MAGS="40X 100X 200X 400X"

for i in $MAGS
do
   export MAGNIFICATION_FACTOR=$i
   jupyter nbconvert --to ipynb --execute --ExecutePreprocessor.timeout=-1 --debug --output VGGINET_${MAGNIFICATION_FACTOR}.ipynb VGGINET_woDA.ipynb
   jupyter nbconvert --to html VGGINET_${MAGNIFICATION_FACTOR}.ipynb
done
