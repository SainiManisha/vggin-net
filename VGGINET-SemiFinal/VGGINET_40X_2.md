<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('40X', '2')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0450 - accuracy: 0.5312WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 14s 2s/step - loss: 4.0288 - accuracy: 0.7289 - val_loss: 25.6950 - val_accuracy: 0.7542
    Epoch 2/100
    9/9 [==============================] - 12s 1s/step - loss: 2.0416 - accuracy: 0.8477 - val_loss: 4.3026 - val_accuracy: 0.8939
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2862 - accuracy: 0.8839 - val_loss: 3.9676 - val_accuracy: 0.8380
    Epoch 4/100
    9/9 [==============================] - 9s 1s/step - loss: 1.3132 - accuracy: 0.8756 - val_loss: 3.1349 - val_accuracy: 0.8715
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4029 - accuracy: 0.8942 - val_loss: 1.2106 - val_accuracy: 0.9106
    Epoch 6/100
    9/9 [==============================] - 9s 1s/step - loss: 1.1683 - accuracy: 0.9090 - val_loss: 1.6021 - val_accuracy: 0.9218
    Epoch 7/100
    9/9 [==============================] - 9s 1s/step - loss: 1.0423 - accuracy: 0.9146 - val_loss: 0.7561 - val_accuracy: 0.9553
    Epoch 8/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8581 - accuracy: 0.9211 - val_loss: 1.1041 - val_accuracy: 0.9330
    Epoch 9/100
    9/9 [==============================] - 12s 1s/step - loss: 0.7297 - accuracy: 0.9285 - val_loss: 1.5695 - val_accuracy: 0.9218
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7842 - accuracy: 0.9146 - val_loss: 2.5549 - val_accuracy: 0.9162
    Epoch 11/100
    9/9 [==============================] - 12s 1s/step - loss: 0.7075 - accuracy: 0.9471 - val_loss: 1.2843 - val_accuracy: 0.9665
    Epoch 12/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5067 - accuracy: 0.9452 - val_loss: 2.9825 - val_accuracy: 0.9050
    Epoch 13/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5643 - accuracy: 0.9443 - val_loss: 0.7372 - val_accuracy: 0.9553
    Epoch 14/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4657 - accuracy: 0.9582 - val_loss: 0.8185 - val_accuracy: 0.9553
    Epoch 15/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4845 - accuracy: 0.9526 - val_loss: 0.6430 - val_accuracy: 0.9609
    Epoch 16/100
    9/9 [==============================] - 9s 985ms/step - loss: 0.4836 - accuracy: 0.9564 - val_loss: 2.5137 - val_accuracy: 0.9106
    Epoch 17/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6381 - accuracy: 0.9415 - val_loss: 1.5819 - val_accuracy: 0.9385
    Epoch 18/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7317 - accuracy: 0.9536 - val_loss: 1.8082 - val_accuracy: 0.9218
    Epoch 19/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4792 - accuracy: 0.9601 - val_loss: 1.0257 - val_accuracy: 0.9385
    Epoch 20/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7236 - accuracy: 0.9471 - val_loss: 0.9859 - val_accuracy: 0.9553
    Epoch 21/100
    9/9 [==============================] - 12s 1s/step - loss: 0.4707 - accuracy: 0.9536 - val_loss: 1.0496 - val_accuracy: 0.9441
    Epoch 22/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5187 - accuracy: 0.9582 - val_loss: 1.2669 - val_accuracy: 0.9385
    Epoch 23/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5619 - accuracy: 0.9601 - val_loss: 0.6835 - val_accuracy: 0.9665
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4699 - accuracy: 0.9619 - val_loss: 1.5367 - val_accuracy: 0.9441
    Epoch 25/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.3350 - accuracy: 0.9656 - val_loss: 0.5496 - val_accuracy: 0.9721
    Epoch 26/100
    9/9 [==============================] - 9s 992ms/step - loss: 0.4559 - accuracy: 0.9601 - val_loss: 0.6101 - val_accuracy: 0.9665
    Epoch 27/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.5666 - accuracy: 0.9582 - val_loss: 0.8804 - val_accuracy: 0.9553
    Epoch 28/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3152 - accuracy: 0.9749 - val_loss: 1.2553 - val_accuracy: 0.9553
    Epoch 29/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3640 - accuracy: 0.9694 - val_loss: 0.8611 - val_accuracy: 0.9665
    Epoch 30/100
    9/9 [==============================] - 9s 994ms/step - loss: 0.4329 - accuracy: 0.9666 - val_loss: 1.0320 - val_accuracy: 0.9441
    Epoch 31/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3344 - accuracy: 0.9731 - val_loss: 0.8943 - val_accuracy: 0.9609
    Epoch 32/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2226 - accuracy: 0.9768 - val_loss: 1.0311 - val_accuracy: 0.9553
    Epoch 33/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2828 - accuracy: 0.9740 - val_loss: 1.1736 - val_accuracy: 0.9553
    Epoch 34/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1626 - accuracy: 0.9833 - val_loss: 1.3900 - val_accuracy: 0.9497
    Epoch 35/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1667 - accuracy: 0.9824 - val_loss: 0.8275 - val_accuracy: 0.9441
    Epoch 36/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4792 - accuracy: 0.9619 - val_loss: 1.4309 - val_accuracy: 0.9553
    Epoch 37/100
    9/9 [==============================] - 9s 987ms/step - loss: 0.3473 - accuracy: 0.9712 - val_loss: 0.5806 - val_accuracy: 0.9609
    Epoch 38/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2937 - accuracy: 0.9786 - val_loss: 0.4427 - val_accuracy: 0.9609
    Epoch 39/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3126 - accuracy: 0.9777 - val_loss: 0.8511 - val_accuracy: 0.9441
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1848 - accuracy: 0.9842 - val_loss: 0.5648 - val_accuracy: 0.9665
    Epoch 41/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5409 - accuracy: 0.9703 - val_loss: 0.7717 - val_accuracy: 0.9609
    Epoch 42/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3333 - accuracy: 0.9814 - val_loss: 0.9967 - val_accuracy: 0.9441
    Epoch 43/100
    9/9 [==============================] - 9s 979ms/step - loss: 0.3381 - accuracy: 0.9694 - val_loss: 0.5816 - val_accuracy: 0.9665
    Epoch 44/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3140 - accuracy: 0.9703 - val_loss: 0.4359 - val_accuracy: 0.9665
    Epoch 45/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3992 - accuracy: 0.9768 - val_loss: 0.5376 - val_accuracy: 0.9721
    Epoch 46/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3980 - accuracy: 0.9749 - val_loss: 1.0804 - val_accuracy: 0.9609
    Epoch 47/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4090 - accuracy: 0.9740 - val_loss: 4.1278 - val_accuracy: 0.9162
    Epoch 48/100
    9/9 [==============================] - 9s 983ms/step - loss: 0.2492 - accuracy: 0.9805 - val_loss: 1.3779 - val_accuracy: 0.9553
    Epoch 49/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3975 - accuracy: 0.9777 - val_loss: 2.5857 - val_accuracy: 0.9441
    Epoch 50/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4054 - accuracy: 0.9796 - val_loss: 1.1326 - val_accuracy: 0.9497
    Epoch 51/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2604 - accuracy: 0.9824 - val_loss: 1.1392 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1493 - accuracy: 0.9851 - val_loss: 1.8951 - val_accuracy: 0.9497
    Epoch 53/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3120 - accuracy: 0.9842 - val_loss: 2.0196 - val_accuracy: 0.9497
    Epoch 54/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1953 - accuracy: 0.9879 - val_loss: 1.0125 - val_accuracy: 0.9665
    Epoch 55/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3828 - accuracy: 0.9768 - val_loss: 0.6739 - val_accuracy: 0.9665
    Epoch 56/100
    9/9 [==============================] - 9s 998ms/step - loss: 0.2969 - accuracy: 0.9833 - val_loss: 1.4437 - val_accuracy: 0.9609
    Epoch 57/100
    9/9 [==============================] - 9s 987ms/step - loss: 0.2566 - accuracy: 0.9870 - val_loss: 1.4353 - val_accuracy: 0.9609
    Epoch 58/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.1756 - accuracy: 0.9889 - val_loss: 0.8494 - val_accuracy: 0.9665
    Epoch 59/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1891 - accuracy: 0.9870 - val_loss: 0.5802 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.2874 - accuracy: 0.9889 - val_loss: 2.3393 - val_accuracy: 0.9441
    Epoch 61/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2935 - accuracy: 0.9833 - val_loss: 1.8191 - val_accuracy: 0.9497
    Epoch 62/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3485 - accuracy: 0.9805 - val_loss: 2.0552 - val_accuracy: 0.9497
    Epoch 63/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3271 - accuracy: 0.9824 - val_loss: 2.0779 - val_accuracy: 0.9553
    Epoch 64/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1104 - accuracy: 0.9889 - val_loss: 0.7537 - val_accuracy: 0.9721
    Epoch 65/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2444 - accuracy: 0.9870 - val_loss: 0.9530 - val_accuracy: 0.9721
    Epoch 66/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2776 - accuracy: 0.9824 - val_loss: 0.9186 - val_accuracy: 0.9777
    Epoch 67/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1649 - accuracy: 0.9898 - val_loss: 1.1361 - val_accuracy: 0.9553
    Epoch 68/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2391 - accuracy: 0.9861 - val_loss: 1.6465 - val_accuracy: 0.9497
    Epoch 69/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.2964 - accuracy: 0.9851 - val_loss: 1.3867 - val_accuracy: 0.9609
    Epoch 70/100
    9/9 [==============================] - 9s 1s/step - loss: 0.0909 - accuracy: 0.9907 - val_loss: 1.4274 - val_accuracy: 0.9609
    Epoch 71/100
    9/9 [==============================] - 9s 984ms/step - loss: 0.2447 - accuracy: 0.9824 - val_loss: 1.6169 - val_accuracy: 0.9497
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1231 - accuracy: 0.9916 - val_loss: 1.3210 - val_accuracy: 0.9330
    Epoch 73/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2604 - accuracy: 0.9842 - val_loss: 1.5556 - val_accuracy: 0.9609
    Epoch 74/100
    9/9 [==============================] - 9s 996ms/step - loss: 0.2441 - accuracy: 0.9833 - val_loss: 2.8305 - val_accuracy: 0.9385
    Epoch 75/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1587 - accuracy: 0.9870 - val_loss: 3.5952 - val_accuracy: 0.9162
    Epoch 76/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1654 - accuracy: 0.9898 - val_loss: 5.4975 - val_accuracy: 0.8939
    Epoch 77/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.2223 - accuracy: 0.9870 - val_loss: 1.7898 - val_accuracy: 0.9665
    Epoch 78/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1761 - accuracy: 0.9870 - val_loss: 1.1701 - val_accuracy: 0.9777
    Epoch 79/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2560 - accuracy: 0.9814 - val_loss: 1.9797 - val_accuracy: 0.9441
    Epoch 80/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1811 - accuracy: 0.9889 - val_loss: 1.1030 - val_accuracy: 0.9721
    Epoch 81/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1901 - accuracy: 0.9861 - val_loss: 1.1962 - val_accuracy: 0.9553
    Epoch 82/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1663 - accuracy: 0.9814 - val_loss: 3.3301 - val_accuracy: 0.9385
    Epoch 83/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3909 - accuracy: 0.9814 - val_loss: 2.3681 - val_accuracy: 0.9441
    Epoch 84/100
    9/9 [==============================] - 9s 989ms/step - loss: 0.2248 - accuracy: 0.9833 - val_loss: 1.6035 - val_accuracy: 0.9497
    Epoch 85/100
    9/9 [==============================] - 9s 986ms/step - loss: 0.2405 - accuracy: 0.9870 - val_loss: 0.5831 - val_accuracy: 0.9609
    Epoch 86/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2319 - accuracy: 0.9870 - val_loss: 1.4986 - val_accuracy: 0.9553
    Epoch 87/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2055 - accuracy: 0.9870 - val_loss: 1.2262 - val_accuracy: 0.9553
    Epoch 88/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3619 - accuracy: 0.9833 - val_loss: 0.5275 - val_accuracy: 0.9777
    Epoch 89/100
    9/9 [==============================] - 9s 985ms/step - loss: 0.1259 - accuracy: 0.9926 - val_loss: 0.7129 - val_accuracy: 0.9721
    Epoch 90/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1089 - accuracy: 0.9916 - val_loss: 2.1827 - val_accuracy: 0.9497
    Epoch 91/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2678 - accuracy: 0.9870 - val_loss: 1.5790 - val_accuracy: 0.9553
    Epoch 92/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5735 - accuracy: 0.9694 - val_loss: 2.1802 - val_accuracy: 0.9441
    Epoch 93/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2525 - accuracy: 0.9907 - val_loss: 2.0759 - val_accuracy: 0.9441
    Epoch 94/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2098 - accuracy: 0.9898 - val_loss: 2.1985 - val_accuracy: 0.9274
    Epoch 95/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2830 - accuracy: 0.9814 - val_loss: 1.0478 - val_accuracy: 0.9553
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3446 - accuracy: 0.9833 - val_loss: 3.5596 - val_accuracy: 0.9274
    Epoch 97/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1727 - accuracy: 0.9870 - val_loss: 1.1529 - val_accuracy: 0.9609
    Epoch 98/100
    9/9 [==============================] - 9s 982ms/step - loss: 0.3916 - accuracy: 0.9814 - val_loss: 0.8326 - val_accuracy: 0.9721
    Epoch 99/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3036 - accuracy: 0.9851 - val_loss: 1.6309 - val_accuracy: 0.9609
    Epoch 100/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2466 - accuracy: 0.9879 - val_loss: 1.3143 - val_accuracy: 0.9385

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/1.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 1.8080 - accuracy: 0.9555

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.8079954385757446, 0.9554731249809265]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9554730983302412
                  precision    recall  f1-score   support

               0       0.88      0.98      0.93       158
               1       0.99      0.94      0.97       381

        accuracy                           0.96       539
       macro avg       0.94      0.96      0.95       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/2.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/3.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/4.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.88      0.98      0.93       158
               1       0.99      0.94      0.97       381

        accuracy                           0.96       539
       macro avg       0.94      0.96      0.95       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.88      0.98      0.94      0.93      0.96      0.93       158
              1       0.99      0.94      0.98      0.97      0.96      0.92       381

    avg / total       0.96      0.96      0.97      0.96      0.96      0.93       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
