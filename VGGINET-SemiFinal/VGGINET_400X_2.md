<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('400X', '2')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.1299 - accuracy: 0.5391WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 15s 2s/step - loss: 2.8389 - accuracy: 0.7559 - val_loss: 20.6628 - val_accuracy: 0.7391
    Epoch 2/100
    8/8 [==============================] - 12s 2s/step - loss: 2.5419 - accuracy: 0.8321 - val_loss: 51.9626 - val_accuracy: 0.3354
    Epoch 3/100
    8/8 [==============================] - 12s 2s/step - loss: 1.9052 - accuracy: 0.8548 - val_loss: 7.2836 - val_accuracy: 0.8137
    Epoch 4/100
    8/8 [==============================] - 13s 2s/step - loss: 1.6503 - accuracy: 0.8568 - val_loss: 12.8582 - val_accuracy: 0.5714
    Epoch 5/100
    8/8 [==============================] - 12s 2s/step - loss: 1.5769 - accuracy: 0.8661 - val_loss: 4.5273 - val_accuracy: 0.8571
    Epoch 6/100
    8/8 [==============================] - 12s 1s/step - loss: 1.2964 - accuracy: 0.8692 - val_loss: 7.4905 - val_accuracy: 0.6522
    Epoch 7/100
    8/8 [==============================] - 13s 2s/step - loss: 1.5104 - accuracy: 0.8702 - val_loss: 1.7939 - val_accuracy: 0.8882
    Epoch 8/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2564 - accuracy: 0.8950 - val_loss: 3.9241 - val_accuracy: 0.7702
    Epoch 9/100
    8/8 [==============================] - 13s 2s/step - loss: 1.3848 - accuracy: 0.8774 - val_loss: 1.1440 - val_accuracy: 0.9068
    Epoch 10/100
    8/8 [==============================] - 13s 2s/step - loss: 1.3546 - accuracy: 0.8867 - val_loss: 1.0609 - val_accuracy: 0.9255
    Epoch 11/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0356 - accuracy: 0.9114 - val_loss: 2.2029 - val_accuracy: 0.9068
    Epoch 12/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2246 - accuracy: 0.9022 - val_loss: 1.4847 - val_accuracy: 0.9068
    Epoch 13/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0077 - accuracy: 0.9145 - val_loss: 1.7022 - val_accuracy: 0.9068
    Epoch 14/100
    8/8 [==============================] - 12s 1s/step - loss: 0.9437 - accuracy: 0.9011 - val_loss: 1.8062 - val_accuracy: 0.9130
    Epoch 15/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1448 - accuracy: 0.9032 - val_loss: 2.0576 - val_accuracy: 0.9255
    Epoch 16/100
    8/8 [==============================] - 12s 1s/step - loss: 1.1329 - accuracy: 0.9042 - val_loss: 1.9524 - val_accuracy: 0.8882
    Epoch 17/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2258 - accuracy: 0.9135 - val_loss: 2.0325 - val_accuracy: 0.8944
    Epoch 18/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0044 - accuracy: 0.9083 - val_loss: 3.0075 - val_accuracy: 0.8758
    Epoch 19/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1578 - accuracy: 0.9073 - val_loss: 2.6122 - val_accuracy: 0.8571
    Epoch 20/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9272 - accuracy: 0.9331 - val_loss: 1.2945 - val_accuracy: 0.9503
    Epoch 21/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8819 - accuracy: 0.9372 - val_loss: 1.8191 - val_accuracy: 0.9130
    Epoch 22/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7690 - accuracy: 0.9320 - val_loss: 1.7091 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8383 - accuracy: 0.9269 - val_loss: 1.2283 - val_accuracy: 0.9441
    Epoch 24/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8929 - accuracy: 0.9351 - val_loss: 2.2788 - val_accuracy: 0.8882
    Epoch 25/100
    8/8 [==============================] - 12s 1s/step - loss: 0.7678 - accuracy: 0.9331 - val_loss: 1.7803 - val_accuracy: 0.8882
    Epoch 26/100
    8/8 [==============================] - 12s 1s/step - loss: 0.9379 - accuracy: 0.9238 - val_loss: 1.2539 - val_accuracy: 0.8944
    Epoch 27/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7558 - accuracy: 0.9361 - val_loss: 1.4315 - val_accuracy: 0.9255
    Epoch 28/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4883 - accuracy: 0.9506 - val_loss: 2.1046 - val_accuracy: 0.8758
    Epoch 29/100
    8/8 [==============================] - 12s 1s/step - loss: 0.7020 - accuracy: 0.9403 - val_loss: 1.6701 - val_accuracy: 0.8571
    Epoch 30/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7054 - accuracy: 0.9331 - val_loss: 1.3935 - val_accuracy: 0.9193
    Epoch 31/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7601 - accuracy: 0.9454 - val_loss: 1.9224 - val_accuracy: 0.8944
    Epoch 32/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8238 - accuracy: 0.9228 - val_loss: 1.3885 - val_accuracy: 0.9317
    Epoch 33/100
    8/8 [==============================] - 8s 1s/step - loss: 0.5468 - accuracy: 0.9506 - val_loss: 1.4099 - val_accuracy: 0.9379
    Epoch 34/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1551 - accuracy: 0.9022 - val_loss: 1.3540 - val_accuracy: 0.9193
    Epoch 35/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0375 - accuracy: 0.9228 - val_loss: 2.6949 - val_accuracy: 0.8820
    Epoch 36/100
    8/8 [==============================] - 12s 1s/step - loss: 1.0676 - accuracy: 0.9331 - val_loss: 1.4807 - val_accuracy: 0.9255
    Epoch 37/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7640 - accuracy: 0.9526 - val_loss: 1.5431 - val_accuracy: 0.9317
    Epoch 38/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7715 - accuracy: 0.9464 - val_loss: 1.5611 - val_accuracy: 0.9006
    Epoch 39/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5560 - accuracy: 0.9516 - val_loss: 1.5855 - val_accuracy: 0.9379
    Epoch 40/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6430 - accuracy: 0.9526 - val_loss: 1.9962 - val_accuracy: 0.9255
    Epoch 41/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6064 - accuracy: 0.9444 - val_loss: 2.1870 - val_accuracy: 0.9317
    Epoch 42/100
    8/8 [==============================] - 9s 1s/step - loss: 0.5433 - accuracy: 0.9547 - val_loss: 1.6386 - val_accuracy: 0.9441
    Epoch 43/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4081 - accuracy: 0.9629 - val_loss: 3.0341 - val_accuracy: 0.9006
    Epoch 44/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5200 - accuracy: 0.9588 - val_loss: 1.5761 - val_accuracy: 0.9317
    Epoch 45/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4760 - accuracy: 0.9598 - val_loss: 1.7129 - val_accuracy: 0.9379
    Epoch 46/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5670 - accuracy: 0.9640 - val_loss: 1.5255 - val_accuracy: 0.9441
    Epoch 47/100
    8/8 [==============================] - 8s 1s/step - loss: 0.5216 - accuracy: 0.9660 - val_loss: 1.9032 - val_accuracy: 0.9255
    Epoch 48/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5525 - accuracy: 0.9588 - val_loss: 1.3639 - val_accuracy: 0.9565
    Epoch 49/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6171 - accuracy: 0.9526 - val_loss: 2.0309 - val_accuracy: 0.9317
    Epoch 50/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7140 - accuracy: 0.9495 - val_loss: 1.7009 - val_accuracy: 0.9379
    Epoch 51/100
    8/8 [==============================] - 9s 1s/step - loss: 0.4386 - accuracy: 0.9578 - val_loss: 1.5052 - val_accuracy: 0.9379
    Epoch 52/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3248 - accuracy: 0.9670 - val_loss: 1.2150 - val_accuracy: 0.9379
    Epoch 53/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6030 - accuracy: 0.9506 - val_loss: 0.9438 - val_accuracy: 0.9565
    Epoch 54/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5694 - accuracy: 0.9640 - val_loss: 1.6586 - val_accuracy: 0.9317
    Epoch 55/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6449 - accuracy: 0.9598 - val_loss: 0.8678 - val_accuracy: 0.9627
    Epoch 56/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6641 - accuracy: 0.9516 - val_loss: 1.4246 - val_accuracy: 0.9441
    Epoch 57/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5251 - accuracy: 0.9578 - val_loss: 1.1482 - val_accuracy: 0.9503
    Epoch 58/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4848 - accuracy: 0.9650 - val_loss: 0.9036 - val_accuracy: 0.9317
    Epoch 59/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6914 - accuracy: 0.9609 - val_loss: 0.9360 - val_accuracy: 0.9441
    Epoch 60/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2787 - accuracy: 0.9732 - val_loss: 1.2326 - val_accuracy: 0.9379
    Epoch 61/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4391 - accuracy: 0.9650 - val_loss: 1.4684 - val_accuracy: 0.9317
    Epoch 62/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5717 - accuracy: 0.9557 - val_loss: 2.6771 - val_accuracy: 0.8447
    Epoch 63/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7091 - accuracy: 0.9434 - val_loss: 2.5104 - val_accuracy: 0.9255
    Epoch 64/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3862 - accuracy: 0.9712 - val_loss: 1.5844 - val_accuracy: 0.9317
    Epoch 65/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5876 - accuracy: 0.9598 - val_loss: 1.7129 - val_accuracy: 0.9441
    Epoch 66/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5838 - accuracy: 0.9609 - val_loss: 1.9737 - val_accuracy: 0.9441
    Epoch 67/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3144 - accuracy: 0.9763 - val_loss: 2.7665 - val_accuracy: 0.9193
    Epoch 68/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3573 - accuracy: 0.9640 - val_loss: 2.3139 - val_accuracy: 0.9441
    Epoch 69/100
    8/8 [==============================] - 9s 1s/step - loss: 0.4485 - accuracy: 0.9712 - val_loss: 1.5041 - val_accuracy: 0.9379
    Epoch 70/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6082 - accuracy: 0.9537 - val_loss: 2.1746 - val_accuracy: 0.9255
    Epoch 71/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2737 - accuracy: 0.9773 - val_loss: 1.9687 - val_accuracy: 0.9193
    Epoch 72/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4990 - accuracy: 0.9743 - val_loss: 2.0974 - val_accuracy: 0.9441
    Epoch 73/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5765 - accuracy: 0.9670 - val_loss: 1.3414 - val_accuracy: 0.9565
    Epoch 74/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3467 - accuracy: 0.9743 - val_loss: 1.7880 - val_accuracy: 0.9441
    Epoch 75/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2712 - accuracy: 0.9784 - val_loss: 1.7165 - val_accuracy: 0.9317
    Epoch 76/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4681 - accuracy: 0.9681 - val_loss: 1.7884 - val_accuracy: 0.9441
    Epoch 77/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4780 - accuracy: 0.9660 - val_loss: 1.6108 - val_accuracy: 0.9441
    Epoch 78/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4186 - accuracy: 0.9650 - val_loss: 2.1361 - val_accuracy: 0.9379
    Epoch 79/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3867 - accuracy: 0.9732 - val_loss: 2.5371 - val_accuracy: 0.9130
    Epoch 80/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3825 - accuracy: 0.9753 - val_loss: 2.0484 - val_accuracy: 0.9255
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3874 - accuracy: 0.9753 - val_loss: 1.8509 - val_accuracy: 0.9441
    Epoch 82/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2671 - accuracy: 0.9825 - val_loss: 2.1188 - val_accuracy: 0.9379
    Epoch 83/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3203 - accuracy: 0.9722 - val_loss: 1.4120 - val_accuracy: 0.9379
    Epoch 84/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5254 - accuracy: 0.9701 - val_loss: 1.4332 - val_accuracy: 0.9379
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4736 - accuracy: 0.9701 - val_loss: 1.6151 - val_accuracy: 0.9130
    Epoch 86/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2700 - accuracy: 0.9732 - val_loss: 1.7492 - val_accuracy: 0.9255
    Epoch 87/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4197 - accuracy: 0.9701 - val_loss: 1.5210 - val_accuracy: 0.9503
    Epoch 88/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5663 - accuracy: 0.9650 - val_loss: 2.1259 - val_accuracy: 0.9503
    Epoch 89/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5744 - accuracy: 0.9691 - val_loss: 1.7629 - val_accuracy: 0.9379
    Epoch 90/100
    8/8 [==============================] - 12s 1s/step - loss: 0.7035 - accuracy: 0.9516 - val_loss: 2.7174 - val_accuracy: 0.9193
    Epoch 91/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3821 - accuracy: 0.9773 - val_loss: 2.9805 - val_accuracy: 0.9130
    Epoch 92/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2617 - accuracy: 0.9753 - val_loss: 2.6526 - val_accuracy: 0.9255
    Epoch 93/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2471 - accuracy: 0.9815 - val_loss: 1.2219 - val_accuracy: 0.9255
    Epoch 94/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5770 - accuracy: 0.9619 - val_loss: 2.3720 - val_accuracy: 0.8944
    Epoch 95/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2682 - accuracy: 0.9784 - val_loss: 3.8530 - val_accuracy: 0.9006
    Epoch 96/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5954 - accuracy: 0.9691 - val_loss: 2.2944 - val_accuracy: 0.9068
    Epoch 97/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2952 - accuracy: 0.9753 - val_loss: 2.4554 - val_accuracy: 0.9379
    Epoch 98/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3503 - accuracy: 0.9753 - val_loss: 2.5286 - val_accuracy: 0.9068
    Epoch 99/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4481 - accuracy: 0.9722 - val_loss: 2.0689 - val_accuracy: 0.9006
    Epoch 100/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2272 - accuracy: 0.9784 - val_loss: 1.9517 - val_accuracy: 0.9130

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/65.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 9s 2s/step - loss: 2.5448 - accuracy: 0.9283

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.544825792312622, 0.9282786846160889]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9282786885245902
                  precision    recall  f1-score   support

               0       0.91      0.84      0.88       148
               1       0.93      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.90      0.91       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/66.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/67.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/68.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.91      0.84      0.88       148
               1       0.93      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.90      0.91       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.91      0.84      0.96      0.88      0.90      0.80       148
              1       0.93      0.96      0.84      0.95      0.90      0.82       340

    avg / total       0.93      0.93      0.88      0.93      0.90      0.82       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
