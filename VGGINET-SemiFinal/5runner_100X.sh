export MAGNIFICATION_FACTOR=100X

for i in {1..5}
do
   export NUM_RUN=$i
   jupyter nbconvert --execute --ExecutePreprocessor.timeout=-1 --debug --output VGGINET_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html VGGINET.ipynb
done
