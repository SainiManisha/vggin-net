<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('200X', '4')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3958 - accuracy: 0.4688WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 17s 2s/step - loss: 2.8567 - accuracy: 0.7663 - val_loss: 5.8750 - val_accuracy: 0.8564
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 2.0653 - accuracy: 0.8602 - val_loss: 11.2002 - val_accuracy: 0.7901
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3748 - accuracy: 0.8767 - val_loss: 1.6443 - val_accuracy: 0.8950
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0646 - accuracy: 0.8914 - val_loss: 1.4168 - val_accuracy: 0.9227
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1275 - accuracy: 0.9089 - val_loss: 1.8492 - val_accuracy: 0.9061
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9635 - accuracy: 0.9062 - val_loss: 0.6754 - val_accuracy: 0.9227
    Epoch 7/100
    9/9 [==============================] - 18s 2s/step - loss: 0.7855 - accuracy: 0.9154 - val_loss: 2.2705 - val_accuracy: 0.8950
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0225 - accuracy: 0.9172 - val_loss: 1.4758 - val_accuracy: 0.9061
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7390 - accuracy: 0.9282 - val_loss: 1.0866 - val_accuracy: 0.9337
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9755 - accuracy: 0.9052 - val_loss: 1.8420 - val_accuracy: 0.9061
    Epoch 11/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6186 - accuracy: 0.9420 - val_loss: 1.1376 - val_accuracy: 0.9503
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9156 - accuracy: 0.9255 - val_loss: 1.2395 - val_accuracy: 0.9337
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8308 - accuracy: 0.9338 - val_loss: 2.0541 - val_accuracy: 0.9006
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7598 - accuracy: 0.9301 - val_loss: 1.2070 - val_accuracy: 0.9116
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6703 - accuracy: 0.9384 - val_loss: 0.5058 - val_accuracy: 0.9503
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7203 - accuracy: 0.9338 - val_loss: 0.9435 - val_accuracy: 0.9227
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0973 - accuracy: 0.9218 - val_loss: 1.2302 - val_accuracy: 0.9061
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8679 - accuracy: 0.9374 - val_loss: 1.2202 - val_accuracy: 0.9337
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7756 - accuracy: 0.9457 - val_loss: 0.8649 - val_accuracy: 0.9392
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9454 - accuracy: 0.9338 - val_loss: 0.7778 - val_accuracy: 0.9558
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6170 - accuracy: 0.9494 - val_loss: 0.9388 - val_accuracy: 0.9448
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6438 - accuracy: 0.9420 - val_loss: 1.0861 - val_accuracy: 0.9061
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6408 - accuracy: 0.9512 - val_loss: 0.9313 - val_accuracy: 0.9448
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5157 - accuracy: 0.9485 - val_loss: 1.2700 - val_accuracy: 0.9392
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9073 - accuracy: 0.9485 - val_loss: 0.7198 - val_accuracy: 0.9448
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4774 - accuracy: 0.9586 - val_loss: 1.4949 - val_accuracy: 0.9171
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4397 - accuracy: 0.9586 - val_loss: 0.9418 - val_accuracy: 0.9392
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5044 - accuracy: 0.9586 - val_loss: 0.9761 - val_accuracy: 0.9503
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5770 - accuracy: 0.9522 - val_loss: 1.4522 - val_accuracy: 0.9337
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5986 - accuracy: 0.9568 - val_loss: 0.7718 - val_accuracy: 0.9558
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5539 - accuracy: 0.9577 - val_loss: 2.8630 - val_accuracy: 0.9171
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4818 - accuracy: 0.9614 - val_loss: 1.3507 - val_accuracy: 0.9337
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4449 - accuracy: 0.9641 - val_loss: 1.1378 - val_accuracy: 0.9503
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5452 - accuracy: 0.9540 - val_loss: 1.1773 - val_accuracy: 0.9448
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4374 - accuracy: 0.9623 - val_loss: 1.3768 - val_accuracy: 0.9503
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4861 - accuracy: 0.9669 - val_loss: 0.8122 - val_accuracy: 0.9448
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4143 - accuracy: 0.9632 - val_loss: 0.5892 - val_accuracy: 0.9613
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6395 - accuracy: 0.9568 - val_loss: 0.9647 - val_accuracy: 0.9392
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4423 - accuracy: 0.9604 - val_loss: 0.5545 - val_accuracy: 0.9558
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4264 - accuracy: 0.9678 - val_loss: 1.1280 - val_accuracy: 0.9282
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4566 - accuracy: 0.9650 - val_loss: 0.8492 - val_accuracy: 0.9558
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3944 - accuracy: 0.9724 - val_loss: 0.9828 - val_accuracy: 0.9448
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5369 - accuracy: 0.9706 - val_loss: 1.2666 - val_accuracy: 0.9448
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3185 - accuracy: 0.9687 - val_loss: 1.2080 - val_accuracy: 0.9448
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3901 - accuracy: 0.9733 - val_loss: 1.7791 - val_accuracy: 0.9392
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3567 - accuracy: 0.9742 - val_loss: 1.3065 - val_accuracy: 0.9448
    Epoch 47/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3110 - accuracy: 0.9724 - val_loss: 1.7161 - val_accuracy: 0.9503
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5214 - accuracy: 0.9687 - val_loss: 2.5737 - val_accuracy: 0.9337
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2443 - accuracy: 0.9798 - val_loss: 2.3053 - val_accuracy: 0.9392
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3146 - accuracy: 0.9742 - val_loss: 2.3453 - val_accuracy: 0.9227
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4333 - accuracy: 0.9752 - val_loss: 1.0693 - val_accuracy: 0.9448
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3230 - accuracy: 0.9761 - val_loss: 1.5071 - val_accuracy: 0.9558
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1182 - accuracy: 0.9853 - val_loss: 2.5864 - val_accuracy: 0.9503
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3194 - accuracy: 0.9788 - val_loss: 2.1200 - val_accuracy: 0.9337
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3334 - accuracy: 0.9816 - val_loss: 1.5268 - val_accuracy: 0.9448
    Epoch 56/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2450 - accuracy: 0.9779 - val_loss: 1.0571 - val_accuracy: 0.9613
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5168 - accuracy: 0.9715 - val_loss: 1.3015 - val_accuracy: 0.9558
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3580 - accuracy: 0.9706 - val_loss: 1.2916 - val_accuracy: 0.9503
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4952 - accuracy: 0.9678 - val_loss: 1.1881 - val_accuracy: 0.9558
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5106 - accuracy: 0.9696 - val_loss: 2.6740 - val_accuracy: 0.9448
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4732 - accuracy: 0.9724 - val_loss: 2.0815 - val_accuracy: 0.9392
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4123 - accuracy: 0.9770 - val_loss: 0.7551 - val_accuracy: 0.9503
    Epoch 63/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3627 - accuracy: 0.9696 - val_loss: 0.7272 - val_accuracy: 0.9558
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4029 - accuracy: 0.9779 - val_loss: 1.0418 - val_accuracy: 0.9503
    Epoch 65/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5836 - accuracy: 0.9733 - val_loss: 1.1926 - val_accuracy: 0.9448
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3073 - accuracy: 0.9798 - val_loss: 0.1980 - val_accuracy: 0.9890
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3736 - accuracy: 0.9788 - val_loss: 0.6085 - val_accuracy: 0.9724
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2397 - accuracy: 0.9853 - val_loss: 1.5676 - val_accuracy: 0.9503
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2247 - accuracy: 0.9807 - val_loss: 2.1549 - val_accuracy: 0.9227
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3195 - accuracy: 0.9788 - val_loss: 0.2961 - val_accuracy: 0.9669
    Epoch 71/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2636 - accuracy: 0.9834 - val_loss: 0.4983 - val_accuracy: 0.9724
    Epoch 72/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2741 - accuracy: 0.9834 - val_loss: 0.5349 - val_accuracy: 0.9669
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2591 - accuracy: 0.9853 - val_loss: 0.7801 - val_accuracy: 0.9558
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3368 - accuracy: 0.9788 - val_loss: 1.0218 - val_accuracy: 0.9613
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2517 - accuracy: 0.9788 - val_loss: 1.3768 - val_accuracy: 0.9669
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1392 - accuracy: 0.9853 - val_loss: 1.6622 - val_accuracy: 0.9503
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1602 - accuracy: 0.9862 - val_loss: 1.7406 - val_accuracy: 0.9613
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1470 - accuracy: 0.9890 - val_loss: 1.6118 - val_accuracy: 0.9558
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3281 - accuracy: 0.9742 - val_loss: 1.4948 - val_accuracy: 0.9613
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1852 - accuracy: 0.9844 - val_loss: 2.1602 - val_accuracy: 0.9503
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2704 - accuracy: 0.9853 - val_loss: 2.6514 - val_accuracy: 0.9503
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2492 - accuracy: 0.9853 - val_loss: 1.3895 - val_accuracy: 0.9834
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1130 - accuracy: 0.9899 - val_loss: 1.4570 - val_accuracy: 0.9558
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2391 - accuracy: 0.9853 - val_loss: 0.5928 - val_accuracy: 0.9613
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1875 - accuracy: 0.9890 - val_loss: 0.6787 - val_accuracy: 0.9613
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3274 - accuracy: 0.9816 - val_loss: 0.3938 - val_accuracy: 0.9779
    Epoch 87/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1336 - accuracy: 0.9890 - val_loss: 0.9893 - val_accuracy: 0.9503
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2896 - accuracy: 0.9807 - val_loss: 1.1244 - val_accuracy: 0.9669
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1779 - accuracy: 0.9825 - val_loss: 1.7382 - val_accuracy: 0.9503
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2016 - accuracy: 0.9853 - val_loss: 1.6248 - val_accuracy: 0.9558
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2593 - accuracy: 0.9807 - val_loss: 1.0949 - val_accuracy: 0.9613
    Epoch 92/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1562 - accuracy: 0.9908 - val_loss: 1.2400 - val_accuracy: 0.9724
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3797 - accuracy: 0.9779 - val_loss: 1.8622 - val_accuracy: 0.9669
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2499 - accuracy: 0.9853 - val_loss: 2.2405 - val_accuracy: 0.9503
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2861 - accuracy: 0.9834 - val_loss: 1.5636 - val_accuracy: 0.9613
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1072 - accuracy: 0.9890 - val_loss: 1.3276 - val_accuracy: 0.9503
    Epoch 97/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2187 - accuracy: 0.9798 - val_loss: 0.5997 - val_accuracy: 0.9613
    Epoch 98/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2740 - accuracy: 0.9853 - val_loss: 0.6185 - val_accuracy: 0.9669
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2418 - accuracy: 0.9862 - val_loss: 0.8858 - val_accuracy: 0.9669
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1684 - accuracy: 0.9871 - val_loss: 1.1777 - val_accuracy: 0.9669

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/49.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 7s 1s/step - loss: 1.9569 - accuracy: 0.9523

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.9568597078323364, 0.9522935748100281]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9522935779816514
                  precision    recall  f1-score   support

               0       0.88      0.97      0.92       158
               1       0.99      0.95      0.97       387

        accuracy                           0.95       545
       macro avg       0.93      0.96      0.94       545
    weighted avg       0.96      0.95      0.95       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/50.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/51.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/52.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.88      0.97      0.92       158
               1       0.99      0.95      0.97       387

        accuracy                           0.95       545
       macro avg       0.93      0.96      0.94       545
    weighted avg       0.96      0.95      0.95       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.88      0.97      0.95      0.92      0.96      0.92       158
              1       0.99      0.95      0.97      0.97      0.96      0.91       387

    avg / total       0.96      0.95      0.96      0.95      0.96      0.91       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
