<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('40X', '4')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.4398 - accuracy: 0.4297WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 18s 2s/step - loss: 3.4266 - accuracy: 0.7400 - val_loss: 5.4764 - val_accuracy: 0.8715
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 1.8543 - accuracy: 0.8644 - val_loss: 4.1775 - val_accuracy: 0.8603
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.3908 - accuracy: 0.8691 - val_loss: 3.7401 - val_accuracy: 0.8715
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 1.2290 - accuracy: 0.8709 - val_loss: 3.1130 - val_accuracy: 0.8715
    Epoch 5/100
    9/9 [==============================] - 15s 2s/step - loss: 1.3384 - accuracy: 0.9053 - val_loss: 1.9462 - val_accuracy: 0.9274
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0136 - accuracy: 0.9099 - val_loss: 2.1820 - val_accuracy: 0.9050
    Epoch 7/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8837 - accuracy: 0.9146 - val_loss: 1.1917 - val_accuracy: 0.9330
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2030 - accuracy: 0.9025 - val_loss: 1.8622 - val_accuracy: 0.9218
    Epoch 9/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8430 - accuracy: 0.9285 - val_loss: 1.5199 - val_accuracy: 0.9330
    Epoch 10/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8182 - accuracy: 0.9285 - val_loss: 1.5525 - val_accuracy: 0.9274
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7600 - accuracy: 0.9406 - val_loss: 1.2138 - val_accuracy: 0.9385
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5951 - accuracy: 0.9406 - val_loss: 1.5698 - val_accuracy: 0.9106
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7575 - accuracy: 0.9415 - val_loss: 2.1046 - val_accuracy: 0.9274
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0741 - accuracy: 0.9331 - val_loss: 1.0931 - val_accuracy: 0.9218
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8729 - accuracy: 0.9461 - val_loss: 2.6631 - val_accuracy: 0.9050
    Epoch 16/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4493 - accuracy: 0.9582 - val_loss: 1.1566 - val_accuracy: 0.9441
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5162 - accuracy: 0.9573 - val_loss: 1.3257 - val_accuracy: 0.9330
    Epoch 18/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5514 - accuracy: 0.9499 - val_loss: 2.3468 - val_accuracy: 0.9218
    Epoch 19/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4929 - accuracy: 0.9601 - val_loss: 1.1559 - val_accuracy: 0.9497
    Epoch 20/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5631 - accuracy: 0.9610 - val_loss: 0.7005 - val_accuracy: 0.9553
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3683 - accuracy: 0.9582 - val_loss: 0.8356 - val_accuracy: 0.9553
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6472 - accuracy: 0.9471 - val_loss: 0.7019 - val_accuracy: 0.9721
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4321 - accuracy: 0.9638 - val_loss: 0.9864 - val_accuracy: 0.9665
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3296 - accuracy: 0.9684 - val_loss: 1.3837 - val_accuracy: 0.9497
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4178 - accuracy: 0.9591 - val_loss: 0.8436 - val_accuracy: 0.9721
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4241 - accuracy: 0.9759 - val_loss: 1.2766 - val_accuracy: 0.9553
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2928 - accuracy: 0.9749 - val_loss: 2.1419 - val_accuracy: 0.9218
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3045 - accuracy: 0.9675 - val_loss: 0.9031 - val_accuracy: 0.9609
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3035 - accuracy: 0.9684 - val_loss: 1.4711 - val_accuracy: 0.9441
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2522 - accuracy: 0.9768 - val_loss: 1.0413 - val_accuracy: 0.9553
    Epoch 31/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3478 - accuracy: 0.9777 - val_loss: 0.9211 - val_accuracy: 0.9553
    Epoch 32/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2390 - accuracy: 0.9851 - val_loss: 1.1091 - val_accuracy: 0.9274
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5255 - accuracy: 0.9619 - val_loss: 2.0791 - val_accuracy: 0.9050
    Epoch 34/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7305 - accuracy: 0.9629 - val_loss: 2.1116 - val_accuracy: 0.9162
    Epoch 35/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4798 - accuracy: 0.9656 - val_loss: 1.7897 - val_accuracy: 0.9218
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4924 - accuracy: 0.9694 - val_loss: 1.2650 - val_accuracy: 0.9441
    Epoch 37/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4446 - accuracy: 0.9694 - val_loss: 1.1025 - val_accuracy: 0.9553
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2814 - accuracy: 0.9749 - val_loss: 1.0439 - val_accuracy: 0.9777
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3860 - accuracy: 0.9647 - val_loss: 0.8329 - val_accuracy: 0.9609
    Epoch 40/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3832 - accuracy: 0.9731 - val_loss: 0.5690 - val_accuracy: 0.9665
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3637 - accuracy: 0.9740 - val_loss: 0.5762 - val_accuracy: 0.9665
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3542 - accuracy: 0.9805 - val_loss: 1.0283 - val_accuracy: 0.9665
    Epoch 43/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3583 - accuracy: 0.9749 - val_loss: 0.7765 - val_accuracy: 0.9553
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4912 - accuracy: 0.9749 - val_loss: 0.9907 - val_accuracy: 0.9665
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2950 - accuracy: 0.9814 - val_loss: 1.3747 - val_accuracy: 0.9497
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4584 - accuracy: 0.9731 - val_loss: 1.1221 - val_accuracy: 0.9609
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2561 - accuracy: 0.9796 - val_loss: 1.3019 - val_accuracy: 0.9609
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3969 - accuracy: 0.9712 - val_loss: 0.7433 - val_accuracy: 0.9609
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4844 - accuracy: 0.9749 - val_loss: 0.4615 - val_accuracy: 0.9665
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2687 - accuracy: 0.9777 - val_loss: 1.7300 - val_accuracy: 0.9441
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1881 - accuracy: 0.9861 - val_loss: 0.6277 - val_accuracy: 0.9609
    Epoch 52/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1504 - accuracy: 0.9851 - val_loss: 0.8028 - val_accuracy: 0.9609
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4720 - accuracy: 0.9703 - val_loss: 1.4206 - val_accuracy: 0.9441
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4300 - accuracy: 0.9703 - val_loss: 2.4593 - val_accuracy: 0.9441
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5164 - accuracy: 0.9703 - val_loss: 1.3206 - val_accuracy: 0.9441
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1685 - accuracy: 0.9796 - val_loss: 2.0921 - val_accuracy: 0.9441
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1308 - accuracy: 0.9879 - val_loss: 1.6621 - val_accuracy: 0.9497
    Epoch 58/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2154 - accuracy: 0.9879 - val_loss: 1.3526 - val_accuracy: 0.9721
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2130 - accuracy: 0.9898 - val_loss: 1.3831 - val_accuracy: 0.9665
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1994 - accuracy: 0.9861 - val_loss: 1.5243 - val_accuracy: 0.9553
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1263 - accuracy: 0.9870 - val_loss: 1.5559 - val_accuracy: 0.9553
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2960 - accuracy: 0.9842 - val_loss: 1.1931 - val_accuracy: 0.9385
    Epoch 63/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1615 - accuracy: 0.9861 - val_loss: 1.6731 - val_accuracy: 0.9497
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3980 - accuracy: 0.9796 - val_loss: 2.3560 - val_accuracy: 0.9385
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1537 - accuracy: 0.9861 - val_loss: 2.6459 - val_accuracy: 0.9385
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1379 - accuracy: 0.9907 - val_loss: 1.6331 - val_accuracy: 0.9441
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2533 - accuracy: 0.9889 - val_loss: 2.2477 - val_accuracy: 0.9385
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2862 - accuracy: 0.9833 - val_loss: 1.3827 - val_accuracy: 0.9609
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2538 - accuracy: 0.9842 - val_loss: 0.9155 - val_accuracy: 0.9665
    Epoch 70/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1925 - accuracy: 0.9889 - val_loss: 0.9484 - val_accuracy: 0.9721
    Epoch 71/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1140 - accuracy: 0.9889 - val_loss: 0.7685 - val_accuracy: 0.9721
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1430 - accuracy: 0.9889 - val_loss: 0.8698 - val_accuracy: 0.9665
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3286 - accuracy: 0.9786 - val_loss: 1.0541 - val_accuracy: 0.9553
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3737 - accuracy: 0.9786 - val_loss: 1.1361 - val_accuracy: 0.9497
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3217 - accuracy: 0.9759 - val_loss: 0.4769 - val_accuracy: 0.9665
    Epoch 76/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2244 - accuracy: 0.9861 - val_loss: 2.0064 - val_accuracy: 0.9609
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1477 - accuracy: 0.9889 - val_loss: 2.1886 - val_accuracy: 0.9441
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1873 - accuracy: 0.9861 - val_loss: 2.8016 - val_accuracy: 0.9497
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2534 - accuracy: 0.9870 - val_loss: 2.1470 - val_accuracy: 0.9553
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2239 - accuracy: 0.9851 - val_loss: 2.0245 - val_accuracy: 0.9553
    Epoch 81/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2594 - accuracy: 0.9842 - val_loss: 1.0746 - val_accuracy: 0.9609
    Epoch 82/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2497 - accuracy: 0.9805 - val_loss: 0.9195 - val_accuracy: 0.9609
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1985 - accuracy: 0.9851 - val_loss: 1.7893 - val_accuracy: 0.9609
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1913 - accuracy: 0.9861 - val_loss: 1.0875 - val_accuracy: 0.9609
    Epoch 85/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2795 - accuracy: 0.9851 - val_loss: 1.4444 - val_accuracy: 0.9609
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1322 - accuracy: 0.9907 - val_loss: 2.6238 - val_accuracy: 0.9609
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1289 - accuracy: 0.9916 - val_loss: 1.3679 - val_accuracy: 0.9721
    Epoch 88/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1533 - accuracy: 0.9907 - val_loss: 0.9618 - val_accuracy: 0.9721
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3216 - accuracy: 0.9879 - val_loss: 1.8238 - val_accuracy: 0.9609
    Epoch 90/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0695 - accuracy: 0.9954 - val_loss: 2.0108 - val_accuracy: 0.9609
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1850 - accuracy: 0.9879 - val_loss: 2.1339 - val_accuracy: 0.9609
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0822 - accuracy: 0.9907 - val_loss: 2.0999 - val_accuracy: 0.9497
    Epoch 93/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1376 - accuracy: 0.9898 - val_loss: 1.3938 - val_accuracy: 0.9665
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3436 - accuracy: 0.9814 - val_loss: 1.4287 - val_accuracy: 0.9721
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0614 - accuracy: 0.9944 - val_loss: 1.9656 - val_accuracy: 0.9721
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1420 - accuracy: 0.9889 - val_loss: 1.3286 - val_accuracy: 0.9721
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1312 - accuracy: 0.9870 - val_loss: 2.8361 - val_accuracy: 0.9385
    Epoch 98/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2401 - accuracy: 0.9879 - val_loss: 3.7220 - val_accuracy: 0.9330
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1163 - accuracy: 0.9898 - val_loss: 3.1685 - val_accuracy: 0.9497
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1325 - accuracy: 0.9935 - val_loss: 3.5344 - val_accuracy: 0.9497

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/25.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 7s 1s/step - loss: 1.7724 - accuracy: 0.9555

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.7723748683929443, 0.9554731249809265]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9554730983302412
                  precision    recall  f1-score   support

               0       0.98      0.87      0.92       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.93      0.94       539
    weighted avg       0.96      0.96      0.95       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/26.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/27.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/28.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.87      0.92       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.93      0.94       539
    weighted avg       0.96      0.96      0.95       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.87      0.99      0.92      0.93      0.85       158
              1       0.95      0.99      0.87      0.97      0.93      0.87       381

    avg / total       0.96      0.96      0.90      0.95      0.93      0.86       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
