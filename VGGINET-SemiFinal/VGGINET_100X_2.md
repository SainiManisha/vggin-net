<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('100X', '2')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3929 - accuracy: 0.4453WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 21s 2s/step - loss: 5.1764 - accuracy: 0.7039 - val_loss: 8.1671 - val_accuracy: 0.8556
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 2.4773 - accuracy: 0.8661 - val_loss: 2.3713 - val_accuracy: 0.9198
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 1.6999 - accuracy: 0.8777 - val_loss: 2.4948 - val_accuracy: 0.9305
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2713 - accuracy: 0.8874 - val_loss: 2.2748 - val_accuracy: 0.8984
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3259 - accuracy: 0.9051 - val_loss: 3.8937 - val_accuracy: 0.8770
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1770 - accuracy: 0.9069 - val_loss: 3.5880 - val_accuracy: 0.8984
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8528 - accuracy: 0.9309 - val_loss: 1.6225 - val_accuracy: 0.9198
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7844 - accuracy: 0.9229 - val_loss: 1.6923 - val_accuracy: 0.9091
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7696 - accuracy: 0.9238 - val_loss: 1.5430 - val_accuracy: 0.8877
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8937 - accuracy: 0.9149 - val_loss: 1.7102 - val_accuracy: 0.9198
    Epoch 11/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0315 - accuracy: 0.9069 - val_loss: 1.2480 - val_accuracy: 0.9091
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8676 - accuracy: 0.9238 - val_loss: 1.1888 - val_accuracy: 0.9305
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7532 - accuracy: 0.9371 - val_loss: 1.2875 - val_accuracy: 0.8930
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7436 - accuracy: 0.9220 - val_loss: 0.8967 - val_accuracy: 0.9144
    Epoch 15/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9386 - accuracy: 0.9122 - val_loss: 1.0132 - val_accuracy: 0.9519
    Epoch 16/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6589 - accuracy: 0.9379 - val_loss: 1.0402 - val_accuracy: 0.9412
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6186 - accuracy: 0.9415 - val_loss: 0.7914 - val_accuracy: 0.9519
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8832 - accuracy: 0.9291 - val_loss: 0.6962 - val_accuracy: 0.9358
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7942 - accuracy: 0.9326 - val_loss: 0.8127 - val_accuracy: 0.9679
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7357 - accuracy: 0.9424 - val_loss: 0.8901 - val_accuracy: 0.9465
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5497 - accuracy: 0.9504 - val_loss: 1.0947 - val_accuracy: 0.9358
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5788 - accuracy: 0.9468 - val_loss: 0.8753 - val_accuracy: 0.9412
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6755 - accuracy: 0.9459 - val_loss: 1.7509 - val_accuracy: 0.9144
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4734 - accuracy: 0.9530 - val_loss: 1.1755 - val_accuracy: 0.9305
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5962 - accuracy: 0.9406 - val_loss: 2.5304 - val_accuracy: 0.9037
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6005 - accuracy: 0.9468 - val_loss: 2.0103 - val_accuracy: 0.9144
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5523 - accuracy: 0.9512 - val_loss: 1.0622 - val_accuracy: 0.9626
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5243 - accuracy: 0.9539 - val_loss: 0.6901 - val_accuracy: 0.9572
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6451 - accuracy: 0.9441 - val_loss: 0.4118 - val_accuracy: 0.9572
    Epoch 30/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6272 - accuracy: 0.9512 - val_loss: 0.5839 - val_accuracy: 0.9465
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8315 - accuracy: 0.9282 - val_loss: 0.5873 - val_accuracy: 0.9572
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0527 - accuracy: 0.9371 - val_loss: 1.0068 - val_accuracy: 0.9412
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3510 - accuracy: 0.9681 - val_loss: 0.7343 - val_accuracy: 0.9519
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5864 - accuracy: 0.9539 - val_loss: 1.4086 - val_accuracy: 0.9198
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5769 - accuracy: 0.9574 - val_loss: 1.2026 - val_accuracy: 0.9412
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5816 - accuracy: 0.9601 - val_loss: 0.5231 - val_accuracy: 0.9626
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4841 - accuracy: 0.9628 - val_loss: 0.6919 - val_accuracy: 0.9626
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5387 - accuracy: 0.9583 - val_loss: 0.4960 - val_accuracy: 0.9519
    Epoch 39/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3502 - accuracy: 0.9681 - val_loss: 0.5410 - val_accuracy: 0.9626
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5144 - accuracy: 0.9699 - val_loss: 0.4458 - val_accuracy: 0.9519
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2870 - accuracy: 0.9699 - val_loss: 0.4896 - val_accuracy: 0.9626
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4024 - accuracy: 0.9672 - val_loss: 0.7059 - val_accuracy: 0.9572
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4590 - accuracy: 0.9601 - val_loss: 0.5313 - val_accuracy: 0.9679
    Epoch 44/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5871 - accuracy: 0.9574 - val_loss: 1.6047 - val_accuracy: 0.9305
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5572 - accuracy: 0.9566 - val_loss: 0.3541 - val_accuracy: 0.9572
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4901 - accuracy: 0.9645 - val_loss: 0.9876 - val_accuracy: 0.9519
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4071 - accuracy: 0.9574 - val_loss: 1.7819 - val_accuracy: 0.9251
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6277 - accuracy: 0.9645 - val_loss: 0.8806 - val_accuracy: 0.9305
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3995 - accuracy: 0.9663 - val_loss: 0.6831 - val_accuracy: 0.9412
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3964 - accuracy: 0.9699 - val_loss: 0.9576 - val_accuracy: 0.9358
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5039 - accuracy: 0.9672 - val_loss: 0.6198 - val_accuracy: 0.9519
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2885 - accuracy: 0.9716 - val_loss: 0.7413 - val_accuracy: 0.9679
    Epoch 53/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4420 - accuracy: 0.9699 - val_loss: 0.8413 - val_accuracy: 0.9733
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2532 - accuracy: 0.9761 - val_loss: 0.5091 - val_accuracy: 0.9679
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4612 - accuracy: 0.9645 - val_loss: 0.7314 - val_accuracy: 0.9733
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3398 - accuracy: 0.9707 - val_loss: 0.7592 - val_accuracy: 0.9786
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4368 - accuracy: 0.9690 - val_loss: 0.3745 - val_accuracy: 0.9893
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6194 - accuracy: 0.9592 - val_loss: 0.6149 - val_accuracy: 0.9733
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4930 - accuracy: 0.9619 - val_loss: 0.6691 - val_accuracy: 0.9679
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3626 - accuracy: 0.9716 - val_loss: 0.5072 - val_accuracy: 0.9626
    Epoch 61/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4285 - accuracy: 0.9761 - val_loss: 0.3838 - val_accuracy: 0.9733
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3001 - accuracy: 0.9725 - val_loss: 1.5558 - val_accuracy: 0.9305
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2811 - accuracy: 0.9725 - val_loss: 0.6146 - val_accuracy: 0.9572
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4671 - accuracy: 0.9725 - val_loss: 0.9128 - val_accuracy: 0.9519
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4028 - accuracy: 0.9734 - val_loss: 1.2651 - val_accuracy: 0.9679
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2383 - accuracy: 0.9787 - val_loss: 1.9937 - val_accuracy: 0.9412
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2787 - accuracy: 0.9778 - val_loss: 0.8691 - val_accuracy: 0.9786
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4196 - accuracy: 0.9770 - val_loss: 0.3348 - val_accuracy: 0.9786
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3128 - accuracy: 0.9761 - val_loss: 0.2362 - val_accuracy: 0.9840
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2202 - accuracy: 0.9867 - val_loss: 0.2634 - val_accuracy: 0.9733
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2675 - accuracy: 0.9796 - val_loss: 0.3939 - val_accuracy: 0.9733
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3076 - accuracy: 0.9770 - val_loss: 0.6907 - val_accuracy: 0.9733
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2832 - accuracy: 0.9752 - val_loss: 1.3444 - val_accuracy: 0.9679
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1662 - accuracy: 0.9849 - val_loss: 0.8626 - val_accuracy: 0.9786
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2221 - accuracy: 0.9787 - val_loss: 0.6101 - val_accuracy: 0.9733
    Epoch 76/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4928 - accuracy: 0.9752 - val_loss: 0.6542 - val_accuracy: 0.9733
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2489 - accuracy: 0.9849 - val_loss: 0.8149 - val_accuracy: 0.9626
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2213 - accuracy: 0.9858 - val_loss: 1.0830 - val_accuracy: 0.9358
    Epoch 79/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1417 - accuracy: 0.9858 - val_loss: 1.1071 - val_accuracy: 0.9465
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3920 - accuracy: 0.9796 - val_loss: 0.5935 - val_accuracy: 0.9786
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2461 - accuracy: 0.9814 - val_loss: 0.5967 - val_accuracy: 0.9733
    Epoch 82/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1723 - accuracy: 0.9867 - val_loss: 0.6523 - val_accuracy: 0.9733
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4695 - accuracy: 0.9672 - val_loss: 1.0254 - val_accuracy: 0.9465
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2287 - accuracy: 0.9823 - val_loss: 0.5331 - val_accuracy: 0.9626
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1670 - accuracy: 0.9849 - val_loss: 0.6123 - val_accuracy: 0.9572
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2320 - accuracy: 0.9814 - val_loss: 0.8079 - val_accuracy: 0.9465
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3394 - accuracy: 0.9734 - val_loss: 1.4869 - val_accuracy: 0.9572
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2415 - accuracy: 0.9840 - val_loss: 0.6157 - val_accuracy: 0.9679
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2851 - accuracy: 0.9823 - val_loss: 1.3705 - val_accuracy: 0.9358
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3578 - accuracy: 0.9743 - val_loss: 0.5586 - val_accuracy: 0.9786
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5063 - accuracy: 0.9734 - val_loss: 1.0902 - val_accuracy: 0.9733
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2060 - accuracy: 0.9823 - val_loss: 1.1883 - val_accuracy: 0.9679
    Epoch 93/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3727 - accuracy: 0.9770 - val_loss: 0.3400 - val_accuracy: 0.9840
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2178 - accuracy: 0.9805 - val_loss: 0.4654 - val_accuracy: 0.9733
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3163 - accuracy: 0.9823 - val_loss: 0.6911 - val_accuracy: 0.9679
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2088 - accuracy: 0.9796 - val_loss: 0.2714 - val_accuracy: 0.9733
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1975 - accuracy: 0.9814 - val_loss: 1.0860 - val_accuracy: 0.9465
    Epoch 98/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2034 - accuracy: 0.9832 - val_loss: 0.5489 - val_accuracy: 0.9626
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1485 - accuracy: 0.9902 - val_loss: 0.9773 - val_accuracy: 0.9465
    Epoch 100/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2743 - accuracy: 0.9849 - val_loss: 0.6008 - val_accuracy: 0.9679

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/17.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 8s 2s/step - loss: 0.9004 - accuracy: 0.9682

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9003804922103882, 0.9681978821754456]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9681978798586572
                  precision    recall  f1-score   support

               0       0.93      0.96      0.95       164
               1       0.98      0.97      0.98       402

        accuracy                           0.97       566
       macro avg       0.96      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/18.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/19.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/20.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.96      0.95       164
               1       0.98      0.97      0.98       402

        accuracy                           0.97       566
       macro avg       0.96      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.96      0.97      0.95      0.97      0.93       164
              1       0.98      0.97      0.96      0.98      0.97      0.94       402

    avg / total       0.97      0.97      0.97      0.97      0.97      0.93       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
