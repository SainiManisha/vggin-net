<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('100X', '5')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0411 - accuracy: 0.6094WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 35s 4s/step - loss: 2.4369 - accuracy: 0.7846 - val_loss: 8.5028 - val_accuracy: 0.8235
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 1.7536 - accuracy: 0.8670 - val_loss: 2.4078 - val_accuracy: 0.9037
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 1.3737 - accuracy: 0.8777 - val_loss: 4.5407 - val_accuracy: 0.8610
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 1.3644 - accuracy: 0.8945 - val_loss: 2.1657 - val_accuracy: 0.8984
    Epoch 5/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1767 - accuracy: 0.8918 - val_loss: 1.2919 - val_accuracy: 0.9412
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1651 - accuracy: 0.8989 - val_loss: 1.6684 - val_accuracy: 0.8877
    Epoch 7/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0121 - accuracy: 0.9096 - val_loss: 1.1289 - val_accuracy: 0.9305
    Epoch 8/100
    9/9 [==============================] - 16s 2s/step - loss: 1.2065 - accuracy: 0.9087 - val_loss: 1.5152 - val_accuracy: 0.9091
    Epoch 9/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0165 - accuracy: 0.9131 - val_loss: 0.5457 - val_accuracy: 0.9519
    Epoch 10/100
    9/9 [==============================] - 18s 2s/step - loss: 1.0718 - accuracy: 0.9034 - val_loss: 0.9734 - val_accuracy: 0.9572
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9905 - accuracy: 0.9273 - val_loss: 2.5082 - val_accuracy: 0.8930
    Epoch 12/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0613 - accuracy: 0.9246 - val_loss: 0.9977 - val_accuracy: 0.9412
    Epoch 13/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9993 - accuracy: 0.9087 - val_loss: 1.0334 - val_accuracy: 0.9198
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0009 - accuracy: 0.9158 - val_loss: 0.8734 - val_accuracy: 0.9251
    Epoch 15/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0053 - accuracy: 0.9335 - val_loss: 1.1102 - val_accuracy: 0.9412
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 1.2018 - accuracy: 0.9167 - val_loss: 0.9335 - val_accuracy: 0.9412
    Epoch 17/100
    9/9 [==============================] - 20s 2s/step - loss: 0.7749 - accuracy: 0.9344 - val_loss: 1.4493 - val_accuracy: 0.9037
    Epoch 18/100
    9/9 [==============================] - 21s 2s/step - loss: 0.8181 - accuracy: 0.9238 - val_loss: 0.9395 - val_accuracy: 0.9519
    Epoch 19/100
    9/9 [==============================] - 18s 2s/step - loss: 0.7254 - accuracy: 0.9433 - val_loss: 2.2575 - val_accuracy: 0.9091
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8027 - accuracy: 0.9388 - val_loss: 0.3309 - val_accuracy: 0.9733
    Epoch 21/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7793 - accuracy: 0.9388 - val_loss: 1.0203 - val_accuracy: 0.9412
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7374 - accuracy: 0.9379 - val_loss: 0.5577 - val_accuracy: 0.9519
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6453 - accuracy: 0.9548 - val_loss: 0.4451 - val_accuracy: 0.9572
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7994 - accuracy: 0.9450 - val_loss: 0.7377 - val_accuracy: 0.9519
    Epoch 25/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8513 - accuracy: 0.9388 - val_loss: 0.7412 - val_accuracy: 0.9572
    Epoch 26/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5829 - accuracy: 0.9521 - val_loss: 1.0201 - val_accuracy: 0.9305
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5668 - accuracy: 0.9486 - val_loss: 0.7262 - val_accuracy: 0.9412
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6242 - accuracy: 0.9486 - val_loss: 0.4974 - val_accuracy: 0.9465
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8727 - accuracy: 0.9353 - val_loss: 0.2441 - val_accuracy: 0.9840
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6438 - accuracy: 0.9504 - val_loss: 0.6416 - val_accuracy: 0.9412
    Epoch 31/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6662 - accuracy: 0.9512 - val_loss: 0.1848 - val_accuracy: 0.9840
    Epoch 32/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7626 - accuracy: 0.9468 - val_loss: 0.9593 - val_accuracy: 0.9198
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5158 - accuracy: 0.9628 - val_loss: 1.1524 - val_accuracy: 0.9305
    Epoch 34/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4774 - accuracy: 0.9566 - val_loss: 0.4346 - val_accuracy: 0.9572
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5542 - accuracy: 0.9628 - val_loss: 0.4673 - val_accuracy: 0.9572
    Epoch 36/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6119 - accuracy: 0.9512 - val_loss: 0.5704 - val_accuracy: 0.9465
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4606 - accuracy: 0.9566 - val_loss: 0.6422 - val_accuracy: 0.9626
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5812 - accuracy: 0.9521 - val_loss: 0.1958 - val_accuracy: 0.9786
    Epoch 39/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8097 - accuracy: 0.9415 - val_loss: 1.1316 - val_accuracy: 0.9465
    Epoch 40/100
    9/9 [==============================] - 19s 2s/step - loss: 0.4233 - accuracy: 0.9592 - val_loss: 0.3816 - val_accuracy: 0.9733
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5320 - accuracy: 0.9619 - val_loss: 0.6021 - val_accuracy: 0.9412
    Epoch 42/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4981 - accuracy: 0.9574 - val_loss: 0.6816 - val_accuracy: 0.9626
    Epoch 43/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6583 - accuracy: 0.9521 - val_loss: 0.5054 - val_accuracy: 0.9572
    Epoch 44/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4663 - accuracy: 0.9637 - val_loss: 0.6815 - val_accuracy: 0.9572
    Epoch 45/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4327 - accuracy: 0.9663 - val_loss: 0.3915 - val_accuracy: 0.9679
    Epoch 46/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6238 - accuracy: 0.9628 - val_loss: 0.7101 - val_accuracy: 0.9679
    Epoch 47/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5314 - accuracy: 0.9592 - val_loss: 0.5174 - val_accuracy: 0.9679
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4534 - accuracy: 0.9743 - val_loss: 0.9092 - val_accuracy: 0.9572
    Epoch 49/100
    9/9 [==============================] - 18s 2s/step - loss: 0.4185 - accuracy: 0.9690 - val_loss: 0.5574 - val_accuracy: 0.9786
    Epoch 50/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4882 - accuracy: 0.9716 - val_loss: 0.6331 - val_accuracy: 0.9679
    Epoch 51/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4360 - accuracy: 0.9690 - val_loss: 0.1999 - val_accuracy: 0.9733
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5849 - accuracy: 0.9619 - val_loss: 0.4013 - val_accuracy: 0.9733
    Epoch 53/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5507 - accuracy: 0.9699 - val_loss: 0.3769 - val_accuracy: 0.9786
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3697 - accuracy: 0.9672 - val_loss: 0.8100 - val_accuracy: 0.9626
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5524 - accuracy: 0.9654 - val_loss: 0.5587 - val_accuracy: 0.9733
    Epoch 56/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4380 - accuracy: 0.9610 - val_loss: 0.8034 - val_accuracy: 0.9733
    Epoch 57/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3711 - accuracy: 0.9716 - val_loss: 0.7449 - val_accuracy: 0.9626
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4424 - accuracy: 0.9681 - val_loss: 0.6793 - val_accuracy: 0.9786
    Epoch 59/100
    9/9 [==============================] - 18s 2s/step - loss: 0.4415 - accuracy: 0.9672 - val_loss: 0.7644 - val_accuracy: 0.9626
    Epoch 60/100
    9/9 [==============================] - 18s 2s/step - loss: 0.4500 - accuracy: 0.9716 - val_loss: 0.5487 - val_accuracy: 0.9733
    Epoch 61/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2917 - accuracy: 0.9805 - val_loss: 0.8647 - val_accuracy: 0.9572
    Epoch 62/100
    9/9 [==============================] - 18s 2s/step - loss: 0.3223 - accuracy: 0.9796 - val_loss: 0.9245 - val_accuracy: 0.9572
    Epoch 63/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4261 - accuracy: 0.9574 - val_loss: 0.2971 - val_accuracy: 0.9840
    Epoch 64/100
    9/9 [==============================] - 19s 2s/step - loss: 0.3482 - accuracy: 0.9752 - val_loss: 1.0643 - val_accuracy: 0.9572
    Epoch 65/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2464 - accuracy: 0.9814 - val_loss: 1.5640 - val_accuracy: 0.9412
    Epoch 66/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5721 - accuracy: 0.9637 - val_loss: 0.4356 - val_accuracy: 0.9840
    Epoch 67/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4083 - accuracy: 0.9681 - val_loss: 0.6489 - val_accuracy: 0.9572
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5384 - accuracy: 0.9699 - val_loss: 0.3484 - val_accuracy: 0.9786
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4887 - accuracy: 0.9725 - val_loss: 3.3350 - val_accuracy: 0.9144
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4516 - accuracy: 0.9707 - val_loss: 1.0172 - val_accuracy: 0.9679
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3275 - accuracy: 0.9805 - val_loss: 0.9678 - val_accuracy: 0.9626
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3382 - accuracy: 0.9770 - val_loss: 0.6556 - val_accuracy: 0.9626
    Epoch 73/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1893 - accuracy: 0.9805 - val_loss: 0.4507 - val_accuracy: 0.9626
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3470 - accuracy: 0.9770 - val_loss: 0.8231 - val_accuracy: 0.9733
    Epoch 75/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4301 - accuracy: 0.9743 - val_loss: 1.2464 - val_accuracy: 0.9626
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3165 - accuracy: 0.9761 - val_loss: 0.7326 - val_accuracy: 0.9786
    Epoch 77/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2219 - accuracy: 0.9840 - val_loss: 0.4560 - val_accuracy: 0.9733
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1990 - accuracy: 0.9796 - val_loss: 0.1386 - val_accuracy: 0.9840
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4700 - accuracy: 0.9761 - val_loss: 0.0294 - val_accuracy: 0.9893
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3367 - accuracy: 0.9761 - val_loss: 0.7406 - val_accuracy: 0.9626
    Epoch 81/100
    9/9 [==============================] - 18s 2s/step - loss: 0.1548 - accuracy: 0.9858 - val_loss: 0.6793 - val_accuracy: 0.9679
    Epoch 82/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3209 - accuracy: 0.9725 - val_loss: 0.3093 - val_accuracy: 0.9786
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1167 - accuracy: 0.9876 - val_loss: 0.1794 - val_accuracy: 0.9733
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2739 - accuracy: 0.9796 - val_loss: 0.2059 - val_accuracy: 0.9893
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3018 - accuracy: 0.9796 - val_loss: 0.2744 - val_accuracy: 0.9786
    Epoch 86/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3381 - accuracy: 0.9858 - val_loss: 0.1661 - val_accuracy: 0.9786
    Epoch 87/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4585 - accuracy: 0.9770 - val_loss: 0.2349 - val_accuracy: 0.9840
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3064 - accuracy: 0.9805 - val_loss: 0.1036 - val_accuracy: 0.9893
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2824 - accuracy: 0.9805 - val_loss: 0.4728 - val_accuracy: 0.9519
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3196 - accuracy: 0.9796 - val_loss: 0.3518 - val_accuracy: 0.9786
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3331 - accuracy: 0.9770 - val_loss: 0.6646 - val_accuracy: 0.9572
    Epoch 92/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2389 - accuracy: 0.9876 - val_loss: 0.7782 - val_accuracy: 0.9733
    Epoch 93/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4971 - accuracy: 0.9761 - val_loss: 0.4373 - val_accuracy: 0.9786
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2508 - accuracy: 0.9858 - val_loss: 0.3655 - val_accuracy: 0.9679
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4031 - accuracy: 0.9716 - val_loss: 0.3128 - val_accuracy: 0.9626
    Epoch 96/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1893 - accuracy: 0.9858 - val_loss: 0.6693 - val_accuracy: 0.9572
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4018 - accuracy: 0.9787 - val_loss: 0.1841 - val_accuracy: 0.9840
    Epoch 98/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3844 - accuracy: 0.9796 - val_loss: 0.1067 - val_accuracy: 0.9840
    Epoch 99/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3241 - accuracy: 0.9752 - val_loss: 0.4744 - val_accuracy: 0.9733
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6443 - accuracy: 0.9681 - val_loss: 0.5861 - val_accuracy: 0.9840

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/29.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 28s 6s/step - loss: 0.8789 - accuracy: 0.9717

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.8788936138153076, 0.971731424331665]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9717314487632509
                  precision    recall  f1-score   support

               0       0.98      0.92      0.95       164
               1       0.97      0.99      0.98       402

        accuracy                           0.97       566
       macro avg       0.97      0.96      0.97       566
    weighted avg       0.97      0.97      0.97       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/30.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/31.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/32.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.92      0.95       164
               1       0.97      0.99      0.98       402

        accuracy                           0.97       566
       macro avg       0.97      0.96      0.97       566
    weighted avg       0.97      0.97      0.97       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.92      0.99      0.95      0.96      0.91       164
              1       0.97      0.99      0.92      0.98      0.96      0.92       402

    avg / total       0.97      0.97      0.94      0.97      0.96      0.92       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
