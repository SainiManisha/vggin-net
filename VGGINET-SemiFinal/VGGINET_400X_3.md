<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('400X', '3')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 0.9958 - accuracy: 0.5781WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 15s 2s/step - loss: 3.3431 - accuracy: 0.7456 - val_loss: 47.1359 - val_accuracy: 0.3975
    Epoch 2/100
    8/8 [==============================] - 12s 1s/step - loss: 1.9513 - accuracy: 0.8332 - val_loss: 12.5815 - val_accuracy: 0.7640
    Epoch 3/100
    8/8 [==============================] - 12s 2s/step - loss: 1.3726 - accuracy: 0.8774 - val_loss: 9.2351 - val_accuracy: 0.6522
    Epoch 4/100
    8/8 [==============================] - 8s 1s/step - loss: 1.4716 - accuracy: 0.8610 - val_loss: 4.7461 - val_accuracy: 0.8385
    Epoch 5/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2735 - accuracy: 0.8661 - val_loss: 2.0321 - val_accuracy: 0.9006
    Epoch 6/100
    8/8 [==============================] - 12s 2s/step - loss: 1.3014 - accuracy: 0.8805 - val_loss: 3.0340 - val_accuracy: 0.8634
    Epoch 7/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0916 - accuracy: 0.9032 - val_loss: 1.7170 - val_accuracy: 0.9379
    Epoch 8/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1197 - accuracy: 0.8867 - val_loss: 2.8465 - val_accuracy: 0.8944
    Epoch 9/100
    8/8 [==============================] - 12s 1s/step - loss: 1.0897 - accuracy: 0.8939 - val_loss: 1.4954 - val_accuracy: 0.8944
    Epoch 10/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8444 - accuracy: 0.9094 - val_loss: 1.8436 - val_accuracy: 0.8696
    Epoch 11/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9700 - accuracy: 0.8980 - val_loss: 1.0815 - val_accuracy: 0.9255
    Epoch 12/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9755 - accuracy: 0.9063 - val_loss: 1.6401 - val_accuracy: 0.9006
    Epoch 13/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1519 - accuracy: 0.9001 - val_loss: 1.1523 - val_accuracy: 0.9255
    Epoch 14/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0296 - accuracy: 0.9125 - val_loss: 0.8868 - val_accuracy: 0.9379
    Epoch 15/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7959 - accuracy: 0.9320 - val_loss: 1.5652 - val_accuracy: 0.9006
    Epoch 16/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1833 - accuracy: 0.9083 - val_loss: 2.3997 - val_accuracy: 0.8882
    Epoch 17/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1140 - accuracy: 0.9114 - val_loss: 1.1264 - val_accuracy: 0.9068
    Epoch 18/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8877 - accuracy: 0.9083 - val_loss: 1.4988 - val_accuracy: 0.8944
    Epoch 19/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8958 - accuracy: 0.9186 - val_loss: 1.1856 - val_accuracy: 0.9006
    Epoch 20/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8216 - accuracy: 0.9300 - val_loss: 2.8754 - val_accuracy: 0.8447
    Epoch 21/100
    8/8 [==============================] - 9s 1s/step - loss: 1.2905 - accuracy: 0.9269 - val_loss: 1.1680 - val_accuracy: 0.9317
    Epoch 22/100
    8/8 [==============================] - 16s 2s/step - loss: 1.3643 - accuracy: 0.9063 - val_loss: 1.2271 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 8s 1s/step - loss: 0.8696 - accuracy: 0.9238 - val_loss: 1.0581 - val_accuracy: 0.9006
    Epoch 24/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1612 - accuracy: 0.9073 - val_loss: 1.2496 - val_accuracy: 0.9068
    Epoch 25/100
    8/8 [==============================] - 9s 1s/step - loss: 0.9541 - accuracy: 0.9331 - val_loss: 1.1362 - val_accuracy: 0.9255
    Epoch 26/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0679 - accuracy: 0.9197 - val_loss: 1.1676 - val_accuracy: 0.9130
    Epoch 27/100
    8/8 [==============================] - 9s 1s/step - loss: 0.7855 - accuracy: 0.9300 - val_loss: 1.1753 - val_accuracy: 0.9130
    Epoch 28/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9921 - accuracy: 0.9372 - val_loss: 0.9994 - val_accuracy: 0.9317
    Epoch 29/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8033 - accuracy: 0.9392 - val_loss: 1.0666 - val_accuracy: 0.9317
    Epoch 30/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8733 - accuracy: 0.9331 - val_loss: 1.5828 - val_accuracy: 0.9255
    Epoch 31/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7343 - accuracy: 0.9351 - val_loss: 1.3992 - val_accuracy: 0.9379
    Epoch 32/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7383 - accuracy: 0.9413 - val_loss: 1.2970 - val_accuracy: 0.9255
    Epoch 33/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7470 - accuracy: 0.9444 - val_loss: 1.6854 - val_accuracy: 0.9379
    Epoch 34/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8322 - accuracy: 0.9392 - val_loss: 1.4797 - val_accuracy: 0.9006
    Epoch 35/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6635 - accuracy: 0.9516 - val_loss: 2.4097 - val_accuracy: 0.8696
    Epoch 36/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7212 - accuracy: 0.9506 - val_loss: 1.8888 - val_accuracy: 0.8385
    Epoch 37/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6578 - accuracy: 0.9392 - val_loss: 1.3827 - val_accuracy: 0.9006
    Epoch 38/100
    8/8 [==============================] - 9s 1s/step - loss: 0.5512 - accuracy: 0.9567 - val_loss: 2.0707 - val_accuracy: 0.8820
    Epoch 39/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6931 - accuracy: 0.9331 - val_loss: 1.8770 - val_accuracy: 0.9006
    Epoch 40/100
    8/8 [==============================] - 9s 1s/step - loss: 0.8632 - accuracy: 0.9495 - val_loss: 2.5496 - val_accuracy: 0.8634
    Epoch 41/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6611 - accuracy: 0.9526 - val_loss: 2.5517 - val_accuracy: 0.8820
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9457 - accuracy: 0.9403 - val_loss: 2.2798 - val_accuracy: 0.9255
    Epoch 43/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5887 - accuracy: 0.9475 - val_loss: 1.7298 - val_accuracy: 0.9503
    Epoch 44/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9728 - accuracy: 0.9392 - val_loss: 2.7159 - val_accuracy: 0.9068
    Epoch 45/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6993 - accuracy: 0.9444 - val_loss: 1.2860 - val_accuracy: 0.9317
    Epoch 46/100
    8/8 [==============================] - 8s 1s/step - loss: 0.8314 - accuracy: 0.9537 - val_loss: 1.5393 - val_accuracy: 0.9193
    Epoch 47/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4299 - accuracy: 0.9567 - val_loss: 3.7253 - val_accuracy: 0.8571
    Epoch 48/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9248 - accuracy: 0.9434 - val_loss: 2.6053 - val_accuracy: 0.9130
    Epoch 49/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7152 - accuracy: 0.9557 - val_loss: 1.4877 - val_accuracy: 0.9068
    Epoch 50/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7223 - accuracy: 0.9537 - val_loss: 1.9253 - val_accuracy: 0.9130
    Epoch 51/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7399 - accuracy: 0.9454 - val_loss: 1.8139 - val_accuracy: 0.9317
    Epoch 52/100
    8/8 [==============================] - 8s 1s/step - loss: 0.5000 - accuracy: 0.9650 - val_loss: 1.3002 - val_accuracy: 0.9317
    Epoch 53/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7485 - accuracy: 0.9537 - val_loss: 1.6404 - val_accuracy: 0.9068
    Epoch 54/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4258 - accuracy: 0.9640 - val_loss: 2.9948 - val_accuracy: 0.8634
    Epoch 55/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5423 - accuracy: 0.9588 - val_loss: 1.8442 - val_accuracy: 0.8882
    Epoch 56/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7104 - accuracy: 0.9537 - val_loss: 1.3198 - val_accuracy: 0.9317
    Epoch 57/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5432 - accuracy: 0.9547 - val_loss: 1.2275 - val_accuracy: 0.9565
    Epoch 58/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4112 - accuracy: 0.9650 - val_loss: 1.7522 - val_accuracy: 0.9379
    Epoch 59/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7042 - accuracy: 0.9598 - val_loss: 1.5588 - val_accuracy: 0.9068
    Epoch 60/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5433 - accuracy: 0.9495 - val_loss: 2.2836 - val_accuracy: 0.8944
    Epoch 61/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3639 - accuracy: 0.9660 - val_loss: 0.9911 - val_accuracy: 0.9317
    Epoch 62/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4859 - accuracy: 0.9660 - val_loss: 1.5858 - val_accuracy: 0.9503
    Epoch 63/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2593 - accuracy: 0.9773 - val_loss: 1.6581 - val_accuracy: 0.9503
    Epoch 64/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3521 - accuracy: 0.9732 - val_loss: 1.5944 - val_accuracy: 0.9441
    Epoch 65/100
    8/8 [==============================] - 9s 1s/step - loss: 0.4329 - accuracy: 0.9753 - val_loss: 1.4856 - val_accuracy: 0.9317
    Epoch 66/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3750 - accuracy: 0.9732 - val_loss: 1.3840 - val_accuracy: 0.9317
    Epoch 67/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3686 - accuracy: 0.9650 - val_loss: 1.8567 - val_accuracy: 0.9317
    Epoch 68/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3074 - accuracy: 0.9670 - val_loss: 1.3854 - val_accuracy: 0.9317
    Epoch 69/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5479 - accuracy: 0.9681 - val_loss: 0.9867 - val_accuracy: 0.9565
    Epoch 70/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4651 - accuracy: 0.9701 - val_loss: 1.0745 - val_accuracy: 0.9565
    Epoch 71/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3021 - accuracy: 0.9732 - val_loss: 1.8106 - val_accuracy: 0.9441
    Epoch 72/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3664 - accuracy: 0.9753 - val_loss: 1.5837 - val_accuracy: 0.9317
    Epoch 73/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3628 - accuracy: 0.9722 - val_loss: 1.1902 - val_accuracy: 0.9689
    Epoch 74/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3722 - accuracy: 0.9794 - val_loss: 1.7845 - val_accuracy: 0.9379
    Epoch 75/100
    8/8 [==============================] - 8s 1s/step - loss: 0.2996 - accuracy: 0.9732 - val_loss: 1.7884 - val_accuracy: 0.9441
    Epoch 76/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3076 - accuracy: 0.9732 - val_loss: 1.5273 - val_accuracy: 0.9503
    Epoch 77/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3559 - accuracy: 0.9773 - val_loss: 1.3300 - val_accuracy: 0.9503
    Epoch 78/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3242 - accuracy: 0.9794 - val_loss: 2.4323 - val_accuracy: 0.9130
    Epoch 79/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2420 - accuracy: 0.9784 - val_loss: 2.4994 - val_accuracy: 0.9441
    Epoch 80/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3043 - accuracy: 0.9784 - val_loss: 2.9304 - val_accuracy: 0.9503
    Epoch 81/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4152 - accuracy: 0.9691 - val_loss: 1.8817 - val_accuracy: 0.9565
    Epoch 82/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2089 - accuracy: 0.9753 - val_loss: 1.6569 - val_accuracy: 0.9441
    Epoch 83/100
    8/8 [==============================] - 12s 1s/step - loss: 0.7433 - accuracy: 0.9629 - val_loss: 1.2004 - val_accuracy: 0.9565
    Epoch 84/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2633 - accuracy: 0.9794 - val_loss: 2.1945 - val_accuracy: 0.9379
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4118 - accuracy: 0.9722 - val_loss: 2.0410 - val_accuracy: 0.9441
    Epoch 86/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3623 - accuracy: 0.9763 - val_loss: 1.4279 - val_accuracy: 0.9379
    Epoch 87/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4691 - accuracy: 0.9732 - val_loss: 1.5796 - val_accuracy: 0.9503
    Epoch 88/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2251 - accuracy: 0.9835 - val_loss: 2.3123 - val_accuracy: 0.9255
    Epoch 89/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3493 - accuracy: 0.9732 - val_loss: 2.5492 - val_accuracy: 0.9130
    Epoch 90/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2082 - accuracy: 0.9763 - val_loss: 1.9001 - val_accuracy: 0.9503
    Epoch 91/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3277 - accuracy: 0.9773 - val_loss: 1.7080 - val_accuracy: 0.9441
    Epoch 92/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3064 - accuracy: 0.9784 - val_loss: 2.0416 - val_accuracy: 0.9255
    Epoch 93/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2752 - accuracy: 0.9815 - val_loss: 2.2744 - val_accuracy: 0.9130
    Epoch 94/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3815 - accuracy: 0.9784 - val_loss: 2.3037 - val_accuracy: 0.9317
    Epoch 95/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3149 - accuracy: 0.9815 - val_loss: 2.3462 - val_accuracy: 0.9193
    Epoch 96/100
    8/8 [==============================] - 8s 1s/step - loss: 0.2844 - accuracy: 0.9846 - val_loss: 1.6559 - val_accuracy: 0.9503
    Epoch 97/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2023 - accuracy: 0.9835 - val_loss: 1.7055 - val_accuracy: 0.9503
    Epoch 98/100
    8/8 [==============================] - 13s 2s/step - loss: 0.1071 - accuracy: 0.9897 - val_loss: 1.1997 - val_accuracy: 0.9503
    Epoch 99/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2807 - accuracy: 0.9815 - val_loss: 1.4388 - val_accuracy: 0.9441
    Epoch 100/100
    8/8 [==============================] - 9s 1s/step - loss: 0.2248 - accuracy: 0.9825 - val_loss: 1.5829 - val_accuracy: 0.9379

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/69.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 9s 2s/step - loss: 2.0208 - accuracy: 0.9324

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.0208239555358887, 0.9323770403862]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9323770491803278
                  precision    recall  f1-score   support

               0       0.87      0.91      0.89       148
               1       0.96      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.93      0.92       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/70.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/71.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/72.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.87      0.91      0.89       148
               1       0.96      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.93      0.92       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.87      0.91      0.94      0.89      0.93      0.86       148
              1       0.96      0.94      0.91      0.95      0.93      0.86       340

    avg / total       0.93      0.93      0.92      0.93      0.93      0.86       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
