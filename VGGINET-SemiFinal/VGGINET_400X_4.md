<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('400X', '4')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.2102 - accuracy: 0.5078WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 14s 2s/step - loss: 3.3618 - accuracy: 0.7333 - val_loss: 61.6646 - val_accuracy: 0.3540
    Epoch 2/100
    8/8 [==============================] - 8s 1s/step - loss: 2.5238 - accuracy: 0.8177 - val_loss: 3.7579 - val_accuracy: 0.8696
    Epoch 3/100
    8/8 [==============================] - 12s 2s/step - loss: 1.5562 - accuracy: 0.8671 - val_loss: 2.0968 - val_accuracy: 0.8944
    Epoch 4/100
    8/8 [==============================] - 12s 2s/step - loss: 1.3459 - accuracy: 0.8630 - val_loss: 1.5535 - val_accuracy: 0.9130
    Epoch 5/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2826 - accuracy: 0.8744 - val_loss: 1.8879 - val_accuracy: 0.8944
    Epoch 6/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2695 - accuracy: 0.8847 - val_loss: 1.4729 - val_accuracy: 0.8882
    Epoch 7/100
    8/8 [==============================] - 8s 1s/step - loss: 1.1184 - accuracy: 0.8713 - val_loss: 0.7735 - val_accuracy: 0.9441
    Epoch 8/100
    8/8 [==============================] - 12s 1s/step - loss: 1.1530 - accuracy: 0.8847 - val_loss: 1.0282 - val_accuracy: 0.9068
    Epoch 9/100
    8/8 [==============================] - 9s 1s/step - loss: 1.0486 - accuracy: 0.8929 - val_loss: 1.3107 - val_accuracy: 0.9130
    Epoch 10/100
    8/8 [==============================] - 8s 1s/step - loss: 1.2217 - accuracy: 0.8908 - val_loss: 1.1178 - val_accuracy: 0.9130
    Epoch 11/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0868 - accuracy: 0.9176 - val_loss: 1.5450 - val_accuracy: 0.9006
    Epoch 12/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2484 - accuracy: 0.8939 - val_loss: 1.4904 - val_accuracy: 0.9255
    Epoch 13/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8067 - accuracy: 0.9320 - val_loss: 1.8628 - val_accuracy: 0.9068
    Epoch 14/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9298 - accuracy: 0.9094 - val_loss: 1.5759 - val_accuracy: 0.9068
    Epoch 15/100
    8/8 [==============================] - 9s 1s/step - loss: 1.0469 - accuracy: 0.9073 - val_loss: 1.8399 - val_accuracy: 0.9006
    Epoch 16/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8882 - accuracy: 0.9104 - val_loss: 1.2610 - val_accuracy: 0.9441
    Epoch 17/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7594 - accuracy: 0.9258 - val_loss: 1.3051 - val_accuracy: 0.9255
    Epoch 18/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9374 - accuracy: 0.9258 - val_loss: 1.7606 - val_accuracy: 0.9255
    Epoch 19/100
    8/8 [==============================] - 8s 1s/step - loss: 0.8716 - accuracy: 0.9341 - val_loss: 2.3854 - val_accuracy: 0.9193
    Epoch 20/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9231 - accuracy: 0.9300 - val_loss: 1.4073 - val_accuracy: 0.9317
    Epoch 21/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2248 - accuracy: 0.9166 - val_loss: 1.7159 - val_accuracy: 0.9193
    Epoch 22/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2336 - accuracy: 0.9022 - val_loss: 1.5909 - val_accuracy: 0.9379
    Epoch 23/100
    8/8 [==============================] - 13s 2s/step - loss: 1.4554 - accuracy: 0.9053 - val_loss: 1.8197 - val_accuracy: 0.9441
    Epoch 24/100
    8/8 [==============================] - 12s 2s/step - loss: 1.2915 - accuracy: 0.9073 - val_loss: 1.2593 - val_accuracy: 0.9317
    Epoch 25/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9976 - accuracy: 0.9269 - val_loss: 1.3456 - val_accuracy: 0.9379
    Epoch 26/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7480 - accuracy: 0.9351 - val_loss: 1.4718 - val_accuracy: 0.9317
    Epoch 27/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9008 - accuracy: 0.9351 - val_loss: 1.1972 - val_accuracy: 0.9379
    Epoch 28/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6098 - accuracy: 0.9320 - val_loss: 1.3268 - val_accuracy: 0.9193
    Epoch 29/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0983 - accuracy: 0.9176 - val_loss: 1.3911 - val_accuracy: 0.9255
    Epoch 30/100
    8/8 [==============================] - 8s 1s/step - loss: 1.1080 - accuracy: 0.9351 - val_loss: 2.6214 - val_accuracy: 0.8820
    Epoch 31/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9569 - accuracy: 0.9320 - val_loss: 2.1461 - val_accuracy: 0.9068
    Epoch 32/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0065 - accuracy: 0.9258 - val_loss: 2.1913 - val_accuracy: 0.9193
    Epoch 33/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8970 - accuracy: 0.9341 - val_loss: 1.2286 - val_accuracy: 0.9379
    Epoch 34/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7012 - accuracy: 0.9526 - val_loss: 1.2947 - val_accuracy: 0.9441
    Epoch 35/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7271 - accuracy: 0.9372 - val_loss: 1.8113 - val_accuracy: 0.9317
    Epoch 36/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9691 - accuracy: 0.9444 - val_loss: 1.1133 - val_accuracy: 0.9193
    Epoch 37/100
    8/8 [==============================] - 8s 1s/step - loss: 0.9739 - accuracy: 0.9351 - val_loss: 1.1824 - val_accuracy: 0.9441
    Epoch 38/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9065 - accuracy: 0.9464 - val_loss: 1.2039 - val_accuracy: 0.9627
    Epoch 39/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5551 - accuracy: 0.9506 - val_loss: 1.8006 - val_accuracy: 0.9255
    Epoch 40/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6667 - accuracy: 0.9475 - val_loss: 1.5491 - val_accuracy: 0.9441
    Epoch 41/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6532 - accuracy: 0.9413 - val_loss: 1.1365 - val_accuracy: 0.9565
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9283 - accuracy: 0.9403 - val_loss: 0.9432 - val_accuracy: 0.9503
    Epoch 43/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6324 - accuracy: 0.9526 - val_loss: 1.2965 - val_accuracy: 0.9193
    Epoch 44/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6183 - accuracy: 0.9464 - val_loss: 1.4519 - val_accuracy: 0.9255
    Epoch 45/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8292 - accuracy: 0.9485 - val_loss: 1.3321 - val_accuracy: 0.9379
    Epoch 46/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6354 - accuracy: 0.9578 - val_loss: 1.4465 - val_accuracy: 0.9379
    Epoch 47/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8977 - accuracy: 0.9516 - val_loss: 1.6969 - val_accuracy: 0.9193
    Epoch 48/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7565 - accuracy: 0.9423 - val_loss: 1.1490 - val_accuracy: 0.9565
    Epoch 49/100
    8/8 [==============================] - 9s 1s/step - loss: 0.5792 - accuracy: 0.9598 - val_loss: 1.1823 - val_accuracy: 0.9503
    Epoch 50/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4982 - accuracy: 0.9609 - val_loss: 1.8451 - val_accuracy: 0.9193
    Epoch 51/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6807 - accuracy: 0.9557 - val_loss: 1.0341 - val_accuracy: 0.9068
    Epoch 52/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5543 - accuracy: 0.9578 - val_loss: 1.4408 - val_accuracy: 0.9255
    Epoch 53/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3264 - accuracy: 0.9660 - val_loss: 2.1216 - val_accuracy: 0.9255
    Epoch 54/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4449 - accuracy: 0.9701 - val_loss: 2.9333 - val_accuracy: 0.9193
    Epoch 55/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5308 - accuracy: 0.9547 - val_loss: 2.1170 - val_accuracy: 0.9255
    Epoch 56/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5580 - accuracy: 0.9660 - val_loss: 1.5790 - val_accuracy: 0.9441
    Epoch 57/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5458 - accuracy: 0.9629 - val_loss: 1.8592 - val_accuracy: 0.9379
    Epoch 58/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8227 - accuracy: 0.9485 - val_loss: 2.1454 - val_accuracy: 0.9068
    Epoch 59/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7743 - accuracy: 0.9434 - val_loss: 1.8713 - val_accuracy: 0.9193
    Epoch 60/100
    8/8 [==============================] - 8s 1s/step - loss: 0.8845 - accuracy: 0.9578 - val_loss: 2.6943 - val_accuracy: 0.9193
    Epoch 61/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8058 - accuracy: 0.9506 - val_loss: 1.6355 - val_accuracy: 0.9503
    Epoch 62/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5634 - accuracy: 0.9609 - val_loss: 2.3304 - val_accuracy: 0.9255
    Epoch 63/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6077 - accuracy: 0.9619 - val_loss: 1.5938 - val_accuracy: 0.9503
    Epoch 64/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5421 - accuracy: 0.9712 - val_loss: 1.4186 - val_accuracy: 0.9503
    Epoch 65/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5681 - accuracy: 0.9557 - val_loss: 2.4485 - val_accuracy: 0.9068
    Epoch 66/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5658 - accuracy: 0.9578 - val_loss: 1.6728 - val_accuracy: 0.9193
    Epoch 67/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4461 - accuracy: 0.9681 - val_loss: 1.3345 - val_accuracy: 0.9565
    Epoch 68/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2750 - accuracy: 0.9763 - val_loss: 1.8398 - val_accuracy: 0.9193
    Epoch 69/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4434 - accuracy: 0.9681 - val_loss: 1.5882 - val_accuracy: 0.9317
    Epoch 70/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6252 - accuracy: 0.9578 - val_loss: 1.0225 - val_accuracy: 0.9627
    Epoch 71/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4307 - accuracy: 0.9681 - val_loss: 1.4322 - val_accuracy: 0.9379
    Epoch 72/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4938 - accuracy: 0.9660 - val_loss: 1.9371 - val_accuracy: 0.9255
    Epoch 73/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6025 - accuracy: 0.9609 - val_loss: 2.4667 - val_accuracy: 0.9130
    Epoch 74/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5135 - accuracy: 0.9640 - val_loss: 1.6735 - val_accuracy: 0.9441
    Epoch 75/100
    8/8 [==============================] - 9s 1s/step - loss: 0.2964 - accuracy: 0.9763 - val_loss: 1.8142 - val_accuracy: 0.9565
    Epoch 76/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4806 - accuracy: 0.9609 - val_loss: 1.5562 - val_accuracy: 0.9565
    Epoch 77/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3629 - accuracy: 0.9732 - val_loss: 1.4547 - val_accuracy: 0.9441
    Epoch 78/100
    8/8 [==============================] - 12s 1s/step - loss: 0.1443 - accuracy: 0.9815 - val_loss: 1.4444 - val_accuracy: 0.9503
    Epoch 79/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4135 - accuracy: 0.9732 - val_loss: 1.4979 - val_accuracy: 0.9441
    Epoch 80/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3251 - accuracy: 0.9670 - val_loss: 2.0154 - val_accuracy: 0.9255
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2208 - accuracy: 0.9825 - val_loss: 4.1625 - val_accuracy: 0.9006
    Epoch 82/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3795 - accuracy: 0.9743 - val_loss: 2.5697 - val_accuracy: 0.9193
    Epoch 83/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2751 - accuracy: 0.9732 - val_loss: 1.9759 - val_accuracy: 0.9441
    Epoch 84/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2995 - accuracy: 0.9743 - val_loss: 1.9861 - val_accuracy: 0.9317
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2562 - accuracy: 0.9825 - val_loss: 2.4076 - val_accuracy: 0.9441
    Epoch 86/100
    8/8 [==============================] - 9s 1s/step - loss: 0.3361 - accuracy: 0.9815 - val_loss: 2.4252 - val_accuracy: 0.9317
    Epoch 87/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4041 - accuracy: 0.9701 - val_loss: 1.6155 - val_accuracy: 0.9503
    Epoch 88/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4293 - accuracy: 0.9701 - val_loss: 1.9463 - val_accuracy: 0.9255
    Epoch 89/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3851 - accuracy: 0.9773 - val_loss: 1.9127 - val_accuracy: 0.9193
    Epoch 90/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3213 - accuracy: 0.9712 - val_loss: 1.8085 - val_accuracy: 0.9565
    Epoch 91/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3541 - accuracy: 0.9804 - val_loss: 2.0940 - val_accuracy: 0.9441
    Epoch 92/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4985 - accuracy: 0.9722 - val_loss: 2.6704 - val_accuracy: 0.9317
    Epoch 93/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4248 - accuracy: 0.9794 - val_loss: 2.3457 - val_accuracy: 0.9255
    Epoch 94/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4232 - accuracy: 0.9722 - val_loss: 2.2050 - val_accuracy: 0.9255
    Epoch 95/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4116 - accuracy: 0.9701 - val_loss: 3.0790 - val_accuracy: 0.9193
    Epoch 96/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3199 - accuracy: 0.9743 - val_loss: 3.7722 - val_accuracy: 0.9193
    Epoch 97/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3666 - accuracy: 0.9712 - val_loss: 1.9265 - val_accuracy: 0.9379
    Epoch 98/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4107 - accuracy: 0.9732 - val_loss: 1.7193 - val_accuracy: 0.9379
    Epoch 99/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3348 - accuracy: 0.9784 - val_loss: 1.8965 - val_accuracy: 0.9565
    Epoch 100/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2560 - accuracy: 0.9825 - val_loss: 2.0514 - val_accuracy: 0.9379

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/41.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 9s 2s/step - loss: 2.6172 - accuracy: 0.9283

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.6171977519989014, 0.9282786846160889]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9282786885245902
                  precision    recall  f1-score   support

               0       0.90      0.86      0.88       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.91      0.91       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/42.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/43.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/44.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.90      0.86      0.88       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.91      0.91       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.90      0.86      0.96      0.88      0.91      0.81       148
              1       0.94      0.96      0.86      0.95      0.91      0.83       340

    avg / total       0.93      0.93      0.89      0.93      0.91      0.83       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
