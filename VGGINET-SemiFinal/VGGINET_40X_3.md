<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('40X', '3')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.4552 - accuracy: 0.4453WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 18s 2s/step - loss: 5.3817 - accuracy: 0.6908 - val_loss: 9.7722 - val_accuracy: 0.8771
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 2.3170 - accuracy: 0.8514 - val_loss: 5.2751 - val_accuracy: 0.8827
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 2.1195 - accuracy: 0.8737 - val_loss: 12.2847 - val_accuracy: 0.7877
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.6791 - accuracy: 0.8858 - val_loss: 1.6578 - val_accuracy: 0.9050
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0954 - accuracy: 0.9090 - val_loss: 1.9955 - val_accuracy: 0.8994
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9158 - accuracy: 0.9164 - val_loss: 3.6930 - val_accuracy: 0.8715
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8393 - accuracy: 0.9146 - val_loss: 1.9195 - val_accuracy: 0.8939
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7368 - accuracy: 0.9266 - val_loss: 5.9683 - val_accuracy: 0.8212
    Epoch 9/100
    9/9 [==============================] - 11s 1s/step - loss: 1.1188 - accuracy: 0.9062 - val_loss: 1.3121 - val_accuracy: 0.8994
    Epoch 10/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7817 - accuracy: 0.9331 - val_loss: 2.1277 - val_accuracy: 0.9106
    Epoch 11/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6660 - accuracy: 0.9443 - val_loss: 1.3660 - val_accuracy: 0.9218
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6880 - accuracy: 0.9294 - val_loss: 1.6047 - val_accuracy: 0.9162
    Epoch 13/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4690 - accuracy: 0.9480 - val_loss: 2.1688 - val_accuracy: 0.9162
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5320 - accuracy: 0.9499 - val_loss: 1.6549 - val_accuracy: 0.9218
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3574 - accuracy: 0.9629 - val_loss: 1.4412 - val_accuracy: 0.9330
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5987 - accuracy: 0.9424 - val_loss: 0.8881 - val_accuracy: 0.9330
    Epoch 17/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3300 - accuracy: 0.9666 - val_loss: 0.6966 - val_accuracy: 0.9441
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3277 - accuracy: 0.9638 - val_loss: 0.6965 - val_accuracy: 0.9553
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3924 - accuracy: 0.9573 - val_loss: 0.9800 - val_accuracy: 0.9385
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5693 - accuracy: 0.9499 - val_loss: 0.8434 - val_accuracy: 0.9050
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6661 - accuracy: 0.9461 - val_loss: 1.9157 - val_accuracy: 0.9162
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4772 - accuracy: 0.9526 - val_loss: 1.2174 - val_accuracy: 0.9385
    Epoch 23/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5425 - accuracy: 0.9619 - val_loss: 1.0958 - val_accuracy: 0.9330
    Epoch 24/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3777 - accuracy: 0.9638 - val_loss: 1.6333 - val_accuracy: 0.9330
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4835 - accuracy: 0.9619 - val_loss: 3.2623 - val_accuracy: 0.9106
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5798 - accuracy: 0.9591 - val_loss: 0.9633 - val_accuracy: 0.9218
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4813 - accuracy: 0.9610 - val_loss: 1.3556 - val_accuracy: 0.9553
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4924 - accuracy: 0.9545 - val_loss: 1.2218 - val_accuracy: 0.9441
    Epoch 29/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4849 - accuracy: 0.9703 - val_loss: 1.9877 - val_accuracy: 0.9385
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4725 - accuracy: 0.9666 - val_loss: 0.8803 - val_accuracy: 0.9553
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2691 - accuracy: 0.9740 - val_loss: 1.3193 - val_accuracy: 0.9441
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4609 - accuracy: 0.9656 - val_loss: 0.6230 - val_accuracy: 0.9553
    Epoch 33/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2473 - accuracy: 0.9768 - val_loss: 0.6349 - val_accuracy: 0.9441
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4132 - accuracy: 0.9675 - val_loss: 1.1356 - val_accuracy: 0.9385
    Epoch 35/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3382 - accuracy: 0.9703 - val_loss: 1.8188 - val_accuracy: 0.9330
    Epoch 36/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2806 - accuracy: 0.9759 - val_loss: 1.4111 - val_accuracy: 0.9441
    Epoch 37/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2702 - accuracy: 0.9703 - val_loss: 0.8417 - val_accuracy: 0.9441
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1685 - accuracy: 0.9805 - val_loss: 1.5186 - val_accuracy: 0.9330
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4820 - accuracy: 0.9647 - val_loss: 2.1697 - val_accuracy: 0.9274
    Epoch 40/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2730 - accuracy: 0.9805 - val_loss: 0.9694 - val_accuracy: 0.9330
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4456 - accuracy: 0.9703 - val_loss: 0.5296 - val_accuracy: 0.9665
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1779 - accuracy: 0.9851 - val_loss: 0.8103 - val_accuracy: 0.9553
    Epoch 43/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4475 - accuracy: 0.9712 - val_loss: 0.7440 - val_accuracy: 0.9609
    Epoch 44/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4587 - accuracy: 0.9656 - val_loss: 0.9172 - val_accuracy: 0.9553
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4968 - accuracy: 0.9610 - val_loss: 1.1310 - val_accuracy: 0.9497
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1538 - accuracy: 0.9786 - val_loss: 1.4968 - val_accuracy: 0.9441
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3073 - accuracy: 0.9759 - val_loss: 0.7135 - val_accuracy: 0.9497
    Epoch 48/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2521 - accuracy: 0.9786 - val_loss: 2.1287 - val_accuracy: 0.9330
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4188 - accuracy: 0.9786 - val_loss: 1.2652 - val_accuracy: 0.9609
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2081 - accuracy: 0.9777 - val_loss: 2.9791 - val_accuracy: 0.9218
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3355 - accuracy: 0.9824 - val_loss: 1.5643 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2424 - accuracy: 0.9824 - val_loss: 2.1503 - val_accuracy: 0.9050
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3060 - accuracy: 0.9749 - val_loss: 1.5993 - val_accuracy: 0.9385
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3183 - accuracy: 0.9786 - val_loss: 0.6122 - val_accuracy: 0.9665
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2898 - accuracy: 0.9805 - val_loss: 0.6997 - val_accuracy: 0.9609
    Epoch 56/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3590 - accuracy: 0.9805 - val_loss: 0.9805 - val_accuracy: 0.9553
    Epoch 57/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2139 - accuracy: 0.9842 - val_loss: 0.7838 - val_accuracy: 0.9721
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2528 - accuracy: 0.9814 - val_loss: 1.6395 - val_accuracy: 0.9497
    Epoch 59/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2324 - accuracy: 0.9814 - val_loss: 1.3159 - val_accuracy: 0.9553
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2757 - accuracy: 0.9759 - val_loss: 2.2386 - val_accuracy: 0.9330
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3026 - accuracy: 0.9870 - val_loss: 2.2714 - val_accuracy: 0.9385
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2011 - accuracy: 0.9842 - val_loss: 1.5386 - val_accuracy: 0.9497
    Epoch 63/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1512 - accuracy: 0.9842 - val_loss: 1.2304 - val_accuracy: 0.9665
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2215 - accuracy: 0.9824 - val_loss: 1.0885 - val_accuracy: 0.9721
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1999 - accuracy: 0.9879 - val_loss: 1.0791 - val_accuracy: 0.9721
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1772 - accuracy: 0.9805 - val_loss: 0.7884 - val_accuracy: 0.9665
    Epoch 67/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3995 - accuracy: 0.9759 - val_loss: 1.0719 - val_accuracy: 0.9609
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3319 - accuracy: 0.9833 - val_loss: 0.8279 - val_accuracy: 0.9609
    Epoch 69/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2056 - accuracy: 0.9851 - val_loss: 1.0398 - val_accuracy: 0.9441
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1784 - accuracy: 0.9851 - val_loss: 1.2923 - val_accuracy: 0.9665
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3181 - accuracy: 0.9814 - val_loss: 0.9390 - val_accuracy: 0.9665
    Epoch 72/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2032 - accuracy: 0.9842 - val_loss: 0.6755 - val_accuracy: 0.9721
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1911 - accuracy: 0.9870 - val_loss: 1.1211 - val_accuracy: 0.9609
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0998 - accuracy: 0.9916 - val_loss: 1.4114 - val_accuracy: 0.9497
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2338 - accuracy: 0.9861 - val_loss: 0.9527 - val_accuracy: 0.9609
    Epoch 76/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1671 - accuracy: 0.9879 - val_loss: 1.0630 - val_accuracy: 0.9665
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2059 - accuracy: 0.9796 - val_loss: 0.8560 - val_accuracy: 0.9721
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3014 - accuracy: 0.9842 - val_loss: 1.1083 - val_accuracy: 0.9665
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1152 - accuracy: 0.9879 - val_loss: 1.3619 - val_accuracy: 0.9553
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4139 - accuracy: 0.9740 - val_loss: 0.9475 - val_accuracy: 0.9721
    Epoch 81/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1839 - accuracy: 0.9879 - val_loss: 0.9445 - val_accuracy: 0.9777
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2296 - accuracy: 0.9833 - val_loss: 0.9104 - val_accuracy: 0.9777
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1398 - accuracy: 0.9879 - val_loss: 0.9275 - val_accuracy: 0.9665
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2370 - accuracy: 0.9842 - val_loss: 1.3233 - val_accuracy: 0.9721
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2270 - accuracy: 0.9842 - val_loss: 1.7705 - val_accuracy: 0.9721
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2100 - accuracy: 0.9879 - val_loss: 0.5386 - val_accuracy: 0.9888
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1688 - accuracy: 0.9851 - val_loss: 0.5285 - val_accuracy: 0.9832
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1810 - accuracy: 0.9889 - val_loss: 1.0007 - val_accuracy: 0.9721
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1426 - accuracy: 0.9879 - val_loss: 1.3233 - val_accuracy: 0.9665
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2039 - accuracy: 0.9879 - val_loss: 1.4768 - val_accuracy: 0.9665
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2947 - accuracy: 0.9870 - val_loss: 2.3718 - val_accuracy: 0.9553
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2533 - accuracy: 0.9879 - val_loss: 0.9666 - val_accuracy: 0.9777
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0810 - accuracy: 0.9916 - val_loss: 0.4906 - val_accuracy: 0.9832
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2502 - accuracy: 0.9879 - val_loss: 1.4297 - val_accuracy: 0.9665
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2175 - accuracy: 0.9870 - val_loss: 2.0793 - val_accuracy: 0.9609
    Epoch 96/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3341 - accuracy: 0.9870 - val_loss: 2.3089 - val_accuracy: 0.9609
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0753 - accuracy: 0.9907 - val_loss: 2.1596 - val_accuracy: 0.9609
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1742 - accuracy: 0.9926 - val_loss: 1.6961 - val_accuracy: 0.9609
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1173 - accuracy: 0.9907 - val_loss: 1.1698 - val_accuracy: 0.9665
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2377 - accuracy: 0.9861 - val_loss: 1.4000 - val_accuracy: 0.9609

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/21.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 14s 3s/step - loss: 1.2509 - accuracy: 0.9592

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.2508755922317505, 0.9591836929321289]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9591836734693877
                  precision    recall  f1-score   support

               0       0.97      0.89      0.93       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.94      0.95       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/22.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/23.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/24.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.89      0.93       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.94      0.95       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.89      0.99      0.93      0.94      0.87       158
              1       0.95      0.99      0.89      0.97      0.94      0.89       381

    avg / total       0.96      0.96      0.92      0.96      0.94      0.88       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
