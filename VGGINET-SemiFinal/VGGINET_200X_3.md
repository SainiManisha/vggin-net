<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('200X', '3')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1332 - accuracy: 0.4922WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 17s 2s/step - loss: 3.6765 - accuracy: 0.7470 - val_loss: 22.9152 - val_accuracy: 0.7624
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 2.1327 - accuracy: 0.8565 - val_loss: 9.3987 - val_accuracy: 0.8066
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 1.6542 - accuracy: 0.8942 - val_loss: 3.4276 - val_accuracy: 0.9116
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.4596 - accuracy: 0.8813 - val_loss: 6.3189 - val_accuracy: 0.8287
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8792 - accuracy: 0.9043 - val_loss: 2.1822 - val_accuracy: 0.8840
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9416 - accuracy: 0.9273 - val_loss: 1.2196 - val_accuracy: 0.9171
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8522 - accuracy: 0.9062 - val_loss: 1.7505 - val_accuracy: 0.9116
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9182 - accuracy: 0.9080 - val_loss: 1.1280 - val_accuracy: 0.9282
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6591 - accuracy: 0.9292 - val_loss: 0.8409 - val_accuracy: 0.9448
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0404 - accuracy: 0.9163 - val_loss: 0.9080 - val_accuracy: 0.9282
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7626 - accuracy: 0.9328 - val_loss: 0.8647 - val_accuracy: 0.9337
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6580 - accuracy: 0.9384 - val_loss: 0.8839 - val_accuracy: 0.9227
    Epoch 13/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8031 - accuracy: 0.9264 - val_loss: 1.0758 - val_accuracy: 0.9227
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4898 - accuracy: 0.9393 - val_loss: 1.4169 - val_accuracy: 0.9227
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7745 - accuracy: 0.9347 - val_loss: 0.8850 - val_accuracy: 0.9392
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7603 - accuracy: 0.9374 - val_loss: 1.1324 - val_accuracy: 0.9282
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6158 - accuracy: 0.9485 - val_loss: 1.0789 - val_accuracy: 0.9227
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6730 - accuracy: 0.9393 - val_loss: 1.0640 - val_accuracy: 0.9171
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7713 - accuracy: 0.9319 - val_loss: 1.1265 - val_accuracy: 0.8950
    Epoch 20/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6816 - accuracy: 0.9420 - val_loss: 0.7496 - val_accuracy: 0.9227
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6740 - accuracy: 0.9411 - val_loss: 0.9458 - val_accuracy: 0.9116
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6800 - accuracy: 0.9476 - val_loss: 1.4606 - val_accuracy: 0.9171
    Epoch 23/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5434 - accuracy: 0.9494 - val_loss: 2.0233 - val_accuracy: 0.9061
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5800 - accuracy: 0.9522 - val_loss: 1.0830 - val_accuracy: 0.9282
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5885 - accuracy: 0.9384 - val_loss: 0.9539 - val_accuracy: 0.9448
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8159 - accuracy: 0.9356 - val_loss: 1.2093 - val_accuracy: 0.9392
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6356 - accuracy: 0.9476 - val_loss: 2.8137 - val_accuracy: 0.9061
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8154 - accuracy: 0.9503 - val_loss: 0.8201 - val_accuracy: 0.9613
    Epoch 29/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6522 - accuracy: 0.9466 - val_loss: 2.1395 - val_accuracy: 0.9171
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6069 - accuracy: 0.9595 - val_loss: 1.5305 - val_accuracy: 0.9337
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6302 - accuracy: 0.9549 - val_loss: 1.5564 - val_accuracy: 0.9392
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3809 - accuracy: 0.9632 - val_loss: 1.3265 - val_accuracy: 0.9227
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4597 - accuracy: 0.9604 - val_loss: 0.9185 - val_accuracy: 0.9503
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5411 - accuracy: 0.9614 - val_loss: 1.0029 - val_accuracy: 0.9337
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4543 - accuracy: 0.9641 - val_loss: 1.3398 - val_accuracy: 0.9337
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4398 - accuracy: 0.9595 - val_loss: 0.8313 - val_accuracy: 0.9558
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3924 - accuracy: 0.9669 - val_loss: 0.5742 - val_accuracy: 0.9558
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4919 - accuracy: 0.9669 - val_loss: 1.2053 - val_accuracy: 0.9503
    Epoch 39/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4354 - accuracy: 0.9696 - val_loss: 0.8158 - val_accuracy: 0.9669
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3659 - accuracy: 0.9669 - val_loss: 0.4486 - val_accuracy: 0.9669
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6154 - accuracy: 0.9614 - val_loss: 0.3720 - val_accuracy: 0.9613
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5664 - accuracy: 0.9558 - val_loss: 0.4419 - val_accuracy: 0.9834
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4838 - accuracy: 0.9632 - val_loss: 0.3881 - val_accuracy: 0.9724
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2136 - accuracy: 0.9779 - val_loss: 0.9744 - val_accuracy: 0.9503
    Epoch 45/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3783 - accuracy: 0.9724 - val_loss: 0.9465 - val_accuracy: 0.9613
    Epoch 46/100
    9/9 [==============================] - 18s 2s/step - loss: 0.3205 - accuracy: 0.9798 - val_loss: 0.3343 - val_accuracy: 0.9669
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2195 - accuracy: 0.9816 - val_loss: 0.2766 - val_accuracy: 0.9890
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3004 - accuracy: 0.9761 - val_loss: 0.4180 - val_accuracy: 0.9669
    Epoch 49/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2911 - accuracy: 0.9779 - val_loss: 1.4953 - val_accuracy: 0.9503
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3339 - accuracy: 0.9788 - val_loss: 1.9744 - val_accuracy: 0.9448
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3790 - accuracy: 0.9770 - val_loss: 0.4729 - val_accuracy: 0.9724
    Epoch 52/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2229 - accuracy: 0.9816 - val_loss: 0.6705 - val_accuracy: 0.9448
    Epoch 53/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2462 - accuracy: 0.9779 - val_loss: 0.8246 - val_accuracy: 0.9613
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3386 - accuracy: 0.9788 - val_loss: 0.5124 - val_accuracy: 0.9724
    Epoch 55/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3450 - accuracy: 0.9761 - val_loss: 1.8592 - val_accuracy: 0.9337
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3434 - accuracy: 0.9696 - val_loss: 1.1181 - val_accuracy: 0.9503
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3692 - accuracy: 0.9779 - val_loss: 1.1507 - val_accuracy: 0.9613
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4689 - accuracy: 0.9595 - val_loss: 0.4391 - val_accuracy: 0.9779
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2257 - accuracy: 0.9779 - val_loss: 1.4106 - val_accuracy: 0.9669
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3076 - accuracy: 0.9761 - val_loss: 0.9785 - val_accuracy: 0.9503
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5363 - accuracy: 0.9632 - val_loss: 1.3546 - val_accuracy: 0.9448
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4149 - accuracy: 0.9733 - val_loss: 0.7179 - val_accuracy: 0.9724
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2998 - accuracy: 0.9724 - val_loss: 1.7771 - val_accuracy: 0.9392
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2986 - accuracy: 0.9816 - val_loss: 0.9992 - val_accuracy: 0.9724
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3462 - accuracy: 0.9742 - val_loss: 2.5263 - val_accuracy: 0.9448
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5038 - accuracy: 0.9715 - val_loss: 1.9813 - val_accuracy: 0.9392
    Epoch 67/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3717 - accuracy: 0.9752 - val_loss: 0.8480 - val_accuracy: 0.9669
    Epoch 68/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4823 - accuracy: 0.9724 - val_loss: 1.0893 - val_accuracy: 0.9558
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3060 - accuracy: 0.9779 - val_loss: 1.6385 - val_accuracy: 0.9337
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5221 - accuracy: 0.9669 - val_loss: 0.5707 - val_accuracy: 0.9779
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2646 - accuracy: 0.9816 - val_loss: 0.5007 - val_accuracy: 0.9834
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4431 - accuracy: 0.9733 - val_loss: 0.5825 - val_accuracy: 0.9724
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7173 - accuracy: 0.9678 - val_loss: 0.3218 - val_accuracy: 0.9669
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4865 - accuracy: 0.9641 - val_loss: 0.7173 - val_accuracy: 0.9669
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3394 - accuracy: 0.9770 - val_loss: 2.7105 - val_accuracy: 0.9337
    Epoch 76/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6525 - accuracy: 0.9733 - val_loss: 1.5196 - val_accuracy: 0.9669
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3226 - accuracy: 0.9834 - val_loss: 1.8238 - val_accuracy: 0.9669
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2031 - accuracy: 0.9844 - val_loss: 2.2991 - val_accuracy: 0.9392
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2890 - accuracy: 0.9798 - val_loss: 0.9730 - val_accuracy: 0.9613
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1599 - accuracy: 0.9862 - val_loss: 0.8402 - val_accuracy: 0.9724
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2316 - accuracy: 0.9834 - val_loss: 0.8673 - val_accuracy: 0.9724
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2136 - accuracy: 0.9834 - val_loss: 0.8596 - val_accuracy: 0.9669
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0694 - accuracy: 0.9926 - val_loss: 0.6095 - val_accuracy: 0.9724
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4970 - accuracy: 0.9798 - val_loss: 0.8875 - val_accuracy: 0.9779
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1990 - accuracy: 0.9844 - val_loss: 1.5668 - val_accuracy: 0.9669
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3680 - accuracy: 0.9779 - val_loss: 1.3946 - val_accuracy: 0.9613
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2138 - accuracy: 0.9871 - val_loss: 0.5933 - val_accuracy: 0.9779
    Epoch 88/100
    9/9 [==============================] - 18s 2s/step - loss: 0.2281 - accuracy: 0.9834 - val_loss: 0.6584 - val_accuracy: 0.9779
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2760 - accuracy: 0.9862 - val_loss: 0.6473 - val_accuracy: 0.9834
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2354 - accuracy: 0.9862 - val_loss: 1.1330 - val_accuracy: 0.9558
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2443 - accuracy: 0.9880 - val_loss: 1.6971 - val_accuracy: 0.9503
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0814 - accuracy: 0.9954 - val_loss: 0.6893 - val_accuracy: 0.9669
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1066 - accuracy: 0.9890 - val_loss: 0.6439 - val_accuracy: 0.9724
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3992 - accuracy: 0.9798 - val_loss: 1.2462 - val_accuracy: 0.9613
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1198 - accuracy: 0.9926 - val_loss: 1.0096 - val_accuracy: 0.9613
    Epoch 96/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1997 - accuracy: 0.9880 - val_loss: 1.4019 - val_accuracy: 0.9613
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3167 - accuracy: 0.9871 - val_loss: 3.1966 - val_accuracy: 0.9392
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3299 - accuracy: 0.9834 - val_loss: 2.0537 - val_accuracy: 0.9613
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3162 - accuracy: 0.9788 - val_loss: 1.1083 - val_accuracy: 0.9613
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1179 - accuracy: 0.9908 - val_loss: 2.5116 - val_accuracy: 0.9448

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/57.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 7s 1s/step - loss: 2.7590 - accuracy: 0.9376

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.7590482234954834, 0.9376146793365479]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9376146788990826
                  precision    recall  f1-score   support

               0       0.98      0.80      0.88       158
               1       0.93      0.99      0.96       387

        accuracy                           0.94       545
       macro avg       0.95      0.90      0.92       545
    weighted avg       0.94      0.94      0.94       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/58.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/59.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/60.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.80      0.88       158
               1       0.93      0.99      0.96       387

        accuracy                           0.94       545
       macro avg       0.95      0.90      0.92       545
    weighted avg       0.94      0.94      0.94       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.80      0.99      0.88      0.89      0.78       158
              1       0.93      0.99      0.80      0.96      0.89      0.81       387

    avg / total       0.94      0.94      0.86      0.94      0.89      0.80       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
