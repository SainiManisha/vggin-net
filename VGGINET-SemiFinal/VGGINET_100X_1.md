<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('100X', '1')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1333 - accuracy: 0.5156WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 4.1518 - accuracy: 0.7207 - val_loss: 33.2394 - val_accuracy: 0.5668
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 2.4135 - accuracy: 0.8475 - val_loss: 5.9705 - val_accuracy: 0.8663
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.5995 - accuracy: 0.8927 - val_loss: 4.1753 - val_accuracy: 0.8396
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9891 - accuracy: 0.8954 - val_loss: 2.4422 - val_accuracy: 0.9198
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 1.4766 - accuracy: 0.9105 - val_loss: 1.5832 - val_accuracy: 0.9251
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0374 - accuracy: 0.9007 - val_loss: 2.0274 - val_accuracy: 0.9144
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8875 - accuracy: 0.9096 - val_loss: 1.3069 - val_accuracy: 0.9305
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7920 - accuracy: 0.9193 - val_loss: 1.0461 - val_accuracy: 0.9358
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1352 - accuracy: 0.9051 - val_loss: 0.7732 - val_accuracy: 0.9465
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0203 - accuracy: 0.9149 - val_loss: 0.7782 - val_accuracy: 0.9465
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7670 - accuracy: 0.9211 - val_loss: 0.8766 - val_accuracy: 0.9465
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6611 - accuracy: 0.9379 - val_loss: 1.0775 - val_accuracy: 0.9305
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8131 - accuracy: 0.9300 - val_loss: 1.3723 - val_accuracy: 0.9251
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7324 - accuracy: 0.9353 - val_loss: 0.7825 - val_accuracy: 0.9465
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7932 - accuracy: 0.9335 - val_loss: 0.9777 - val_accuracy: 0.9358
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8180 - accuracy: 0.9282 - val_loss: 1.0429 - val_accuracy: 0.9519
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7185 - accuracy: 0.9362 - val_loss: 0.9127 - val_accuracy: 0.9519
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6974 - accuracy: 0.9326 - val_loss: 1.2357 - val_accuracy: 0.9251
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7460 - accuracy: 0.9406 - val_loss: 0.6119 - val_accuracy: 0.9679
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5760 - accuracy: 0.9459 - val_loss: 0.8401 - val_accuracy: 0.9572
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5966 - accuracy: 0.9415 - val_loss: 0.9099 - val_accuracy: 0.9519
    Epoch 22/100
    9/9 [==============================] - 18s 2s/step - loss: 0.5017 - accuracy: 0.9530 - val_loss: 0.8561 - val_accuracy: 0.9572
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6432 - accuracy: 0.9459 - val_loss: 0.5409 - val_accuracy: 0.9572
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6298 - accuracy: 0.9557 - val_loss: 0.7351 - val_accuracy: 0.9626
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5496 - accuracy: 0.9557 - val_loss: 0.7909 - val_accuracy: 0.9572
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6772 - accuracy: 0.9486 - val_loss: 0.1855 - val_accuracy: 0.9840
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7116 - accuracy: 0.9415 - val_loss: 0.4914 - val_accuracy: 0.9679
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5549 - accuracy: 0.9468 - val_loss: 0.6891 - val_accuracy: 0.9786
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5212 - accuracy: 0.9548 - val_loss: 0.5867 - val_accuracy: 0.9572
    Epoch 30/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5034 - accuracy: 0.9539 - val_loss: 0.5716 - val_accuracy: 0.9626
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4193 - accuracy: 0.9619 - val_loss: 1.1256 - val_accuracy: 0.9465
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5188 - accuracy: 0.9583 - val_loss: 1.0942 - val_accuracy: 0.9412
    Epoch 33/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4819 - accuracy: 0.9574 - val_loss: 0.5142 - val_accuracy: 0.9786
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4123 - accuracy: 0.9601 - val_loss: 0.4838 - val_accuracy: 0.9679
    Epoch 35/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5063 - accuracy: 0.9583 - val_loss: 0.6469 - val_accuracy: 0.9679
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5412 - accuracy: 0.9601 - val_loss: 0.8127 - val_accuracy: 0.9786
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5944 - accuracy: 0.9610 - val_loss: 1.2283 - val_accuracy: 0.9733
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5258 - accuracy: 0.9574 - val_loss: 1.1750 - val_accuracy: 0.9519
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6103 - accuracy: 0.9566 - val_loss: 0.8022 - val_accuracy: 0.9626
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5222 - accuracy: 0.9574 - val_loss: 0.7850 - val_accuracy: 0.9572
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4129 - accuracy: 0.9654 - val_loss: 0.6017 - val_accuracy: 0.9572
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4790 - accuracy: 0.9654 - val_loss: 0.4300 - val_accuracy: 0.9733
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5370 - accuracy: 0.9619 - val_loss: 0.8560 - val_accuracy: 0.9519
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3572 - accuracy: 0.9734 - val_loss: 0.7034 - val_accuracy: 0.9572
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2983 - accuracy: 0.9743 - val_loss: 0.7372 - val_accuracy: 0.9626
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5227 - accuracy: 0.9583 - val_loss: 1.3908 - val_accuracy: 0.9465
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4985 - accuracy: 0.9583 - val_loss: 0.9340 - val_accuracy: 0.9679
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4076 - accuracy: 0.9699 - val_loss: 1.0723 - val_accuracy: 0.9679
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2629 - accuracy: 0.9725 - val_loss: 0.5706 - val_accuracy: 0.9519
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4513 - accuracy: 0.9699 - val_loss: 0.5870 - val_accuracy: 0.9679
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4325 - accuracy: 0.9672 - val_loss: 0.7227 - val_accuracy: 0.9465
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5796 - accuracy: 0.9645 - val_loss: 1.3783 - val_accuracy: 0.9465
    Epoch 53/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3203 - accuracy: 0.9725 - val_loss: 0.9266 - val_accuracy: 0.9572
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3829 - accuracy: 0.9681 - val_loss: 0.3571 - val_accuracy: 0.9626
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3888 - accuracy: 0.9681 - val_loss: 0.2907 - val_accuracy: 0.9733
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4176 - accuracy: 0.9699 - val_loss: 0.6060 - val_accuracy: 0.9733
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5643 - accuracy: 0.9663 - val_loss: 0.6505 - val_accuracy: 0.9572
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1571 - accuracy: 0.9867 - val_loss: 0.3776 - val_accuracy: 0.9572
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4144 - accuracy: 0.9681 - val_loss: 1.7183 - val_accuracy: 0.9305
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2601 - accuracy: 0.9752 - val_loss: 0.6484 - val_accuracy: 0.9572
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4942 - accuracy: 0.9690 - val_loss: 0.7380 - val_accuracy: 0.9679
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2876 - accuracy: 0.9716 - val_loss: 0.6231 - val_accuracy: 0.9626
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2884 - accuracy: 0.9752 - val_loss: 0.5394 - val_accuracy: 0.9626
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3285 - accuracy: 0.9725 - val_loss: 0.8169 - val_accuracy: 0.9626
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4721 - accuracy: 0.9690 - val_loss: 0.7093 - val_accuracy: 0.9626
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3687 - accuracy: 0.9645 - val_loss: 0.3717 - val_accuracy: 0.9893
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3307 - accuracy: 0.9743 - val_loss: 0.3387 - val_accuracy: 0.9733
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5796 - accuracy: 0.9654 - val_loss: 0.3090 - val_accuracy: 0.9626
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3648 - accuracy: 0.9707 - val_loss: 0.2666 - val_accuracy: 0.9733
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4388 - accuracy: 0.9681 - val_loss: 0.3192 - val_accuracy: 0.9679
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3311 - accuracy: 0.9690 - val_loss: 0.2714 - val_accuracy: 0.9679
    Epoch 72/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5089 - accuracy: 0.9707 - val_loss: 0.4594 - val_accuracy: 0.9786
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2711 - accuracy: 0.9805 - val_loss: 0.4907 - val_accuracy: 0.9786
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3848 - accuracy: 0.9725 - val_loss: 0.4886 - val_accuracy: 0.9786
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2854 - accuracy: 0.9823 - val_loss: 0.2627 - val_accuracy: 0.9786
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3109 - accuracy: 0.9770 - val_loss: 0.2534 - val_accuracy: 0.9840
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2324 - accuracy: 0.9796 - val_loss: 0.2010 - val_accuracy: 0.9840
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2385 - accuracy: 0.9876 - val_loss: 0.4438 - val_accuracy: 0.9679
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3292 - accuracy: 0.9787 - val_loss: 0.3056 - val_accuracy: 0.9733
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3066 - accuracy: 0.9796 - val_loss: 0.2370 - val_accuracy: 0.9733
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2746 - accuracy: 0.9787 - val_loss: 0.7300 - val_accuracy: 0.9519
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2454 - accuracy: 0.9778 - val_loss: 0.4800 - val_accuracy: 0.9840
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3247 - accuracy: 0.9849 - val_loss: 0.5008 - val_accuracy: 0.9786
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3347 - accuracy: 0.9778 - val_loss: 0.4744 - val_accuracy: 0.9733
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4215 - accuracy: 0.9734 - val_loss: 0.0918 - val_accuracy: 0.9893
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3357 - accuracy: 0.9761 - val_loss: 0.5673 - val_accuracy: 0.9679
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2718 - accuracy: 0.9805 - val_loss: 0.4456 - val_accuracy: 0.9679
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2362 - accuracy: 0.9796 - val_loss: 0.4945 - val_accuracy: 0.9733
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3451 - accuracy: 0.9752 - val_loss: 0.4771 - val_accuracy: 0.9626
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3089 - accuracy: 0.9770 - val_loss: 0.6714 - val_accuracy: 0.9519
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2481 - accuracy: 0.9832 - val_loss: 0.2970 - val_accuracy: 0.9786
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3125 - accuracy: 0.9832 - val_loss: 0.4338 - val_accuracy: 0.9786
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2011 - accuracy: 0.9867 - val_loss: 0.8304 - val_accuracy: 0.9626
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1981 - accuracy: 0.9849 - val_loss: 0.8467 - val_accuracy: 0.9519
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3256 - accuracy: 0.9752 - val_loss: 0.4608 - val_accuracy: 0.9679
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2034 - accuracy: 0.9867 - val_loss: 0.3612 - val_accuracy: 0.9626
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3177 - accuracy: 0.9787 - val_loss: 0.4756 - val_accuracy: 0.9679
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2164 - accuracy: 0.9867 - val_loss: 1.1658 - val_accuracy: 0.9465
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3349 - accuracy: 0.9849 - val_loss: 0.7541 - val_accuracy: 0.9626
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3134 - accuracy: 0.9796 - val_loss: 0.5333 - val_accuracy: 0.9786

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/61.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 15s 3s/step - loss: 0.9336 - accuracy: 0.9594

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.933627188205719, 0.9593639373779297]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9593639575971732
                  precision    recall  f1-score   support

               0       0.90      0.97      0.93       164
               1       0.99      0.96      0.97       402

        accuracy                           0.96       566
       macro avg       0.94      0.96      0.95       566
    weighted avg       0.96      0.96      0.96       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/62.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/63.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/64.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.90      0.97      0.93       164
               1       0.99      0.96      0.97       402

        accuracy                           0.96       566
       macro avg       0.94      0.96      0.95       566
    weighted avg       0.96      0.96      0.96       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.90      0.97      0.96      0.93      0.96      0.93       164
              1       0.99      0.96      0.97      0.97      0.96      0.92       402

    avg / total       0.96      0.96      0.97      0.96      0.96      0.93       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
