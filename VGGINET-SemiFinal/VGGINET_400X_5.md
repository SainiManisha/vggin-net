<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('400X', '5')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.4147 - accuracy: 0.4219WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 16s 2s/step - loss: 4.5102 - accuracy: 0.7106 - val_loss: 6.6057 - val_accuracy: 0.8571
    Epoch 2/100
    8/8 [==============================] - 12s 2s/step - loss: 2.9200 - accuracy: 0.8321 - val_loss: 10.9662 - val_accuracy: 0.8012
    Epoch 3/100
    8/8 [==============================] - 8s 1s/step - loss: 2.4874 - accuracy: 0.8095 - val_loss: 3.3714 - val_accuracy: 0.8820
    Epoch 4/100
    8/8 [==============================] - 13s 2s/step - loss: 1.5138 - accuracy: 0.8754 - val_loss: 1.9837 - val_accuracy: 0.9193
    Epoch 5/100
    8/8 [==============================] - 12s 2s/step - loss: 1.3442 - accuracy: 0.8620 - val_loss: 2.8187 - val_accuracy: 0.8758
    Epoch 6/100
    8/8 [==============================] - 12s 2s/step - loss: 1.5987 - accuracy: 0.8733 - val_loss: 2.6623 - val_accuracy: 0.9068
    Epoch 7/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0992 - accuracy: 0.8919 - val_loss: 2.6196 - val_accuracy: 0.8261
    Epoch 8/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1515 - accuracy: 0.8929 - val_loss: 2.0041 - val_accuracy: 0.8882
    Epoch 9/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0766 - accuracy: 0.9063 - val_loss: 1.9626 - val_accuracy: 0.8820
    Epoch 10/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1342 - accuracy: 0.8908 - val_loss: 1.8073 - val_accuracy: 0.9006
    Epoch 11/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9689 - accuracy: 0.9176 - val_loss: 3.8309 - val_accuracy: 0.8634
    Epoch 12/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1829 - accuracy: 0.8847 - val_loss: 1.4675 - val_accuracy: 0.9255
    Epoch 13/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8411 - accuracy: 0.9207 - val_loss: 1.7773 - val_accuracy: 0.9068
    Epoch 14/100
    8/8 [==============================] - 12s 1s/step - loss: 1.0854 - accuracy: 0.8980 - val_loss: 2.0374 - val_accuracy: 0.8571
    Epoch 15/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6107 - accuracy: 0.9279 - val_loss: 1.3791 - val_accuracy: 0.9068
    Epoch 16/100
    8/8 [==============================] - 12s 1s/step - loss: 0.9154 - accuracy: 0.9125 - val_loss: 1.2336 - val_accuracy: 0.9006
    Epoch 17/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8864 - accuracy: 0.9176 - val_loss: 1.3740 - val_accuracy: 0.8944
    Epoch 18/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7867 - accuracy: 0.9207 - val_loss: 1.3595 - val_accuracy: 0.9068
    Epoch 19/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1540 - accuracy: 0.9073 - val_loss: 1.5916 - val_accuracy: 0.9068
    Epoch 20/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9076 - accuracy: 0.9331 - val_loss: 1.0580 - val_accuracy: 0.9193
    Epoch 21/100
    8/8 [==============================] - 8s 1s/step - loss: 1.0728 - accuracy: 0.9156 - val_loss: 0.9282 - val_accuracy: 0.9068
    Epoch 22/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8463 - accuracy: 0.9248 - val_loss: 1.2191 - val_accuracy: 0.9379
    Epoch 23/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7665 - accuracy: 0.9372 - val_loss: 1.2771 - val_accuracy: 0.9317
    Epoch 24/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9096 - accuracy: 0.9228 - val_loss: 1.2047 - val_accuracy: 0.9130
    Epoch 25/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0274 - accuracy: 0.9114 - val_loss: 1.8013 - val_accuracy: 0.9068
    Epoch 26/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5949 - accuracy: 0.9320 - val_loss: 1.7016 - val_accuracy: 0.9193
    Epoch 27/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1002 - accuracy: 0.9207 - val_loss: 2.3773 - val_accuracy: 0.9068
    Epoch 28/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8034 - accuracy: 0.9248 - val_loss: 1.2774 - val_accuracy: 0.9068
    Epoch 29/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9065 - accuracy: 0.9289 - val_loss: 1.3845 - val_accuracy: 0.8882
    Epoch 30/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9732 - accuracy: 0.9238 - val_loss: 1.8581 - val_accuracy: 0.9130
    Epoch 31/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7306 - accuracy: 0.9413 - val_loss: 1.4665 - val_accuracy: 0.9255
    Epoch 32/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6649 - accuracy: 0.9444 - val_loss: 3.2859 - val_accuracy: 0.8696
    Epoch 33/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6531 - accuracy: 0.9434 - val_loss: 3.0326 - val_accuracy: 0.8882
    Epoch 34/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8255 - accuracy: 0.9341 - val_loss: 2.1296 - val_accuracy: 0.9255
    Epoch 35/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7451 - accuracy: 0.9413 - val_loss: 1.6542 - val_accuracy: 0.9255
    Epoch 36/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6026 - accuracy: 0.9495 - val_loss: 1.7316 - val_accuracy: 0.9317
    Epoch 37/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5846 - accuracy: 0.9547 - val_loss: 1.3915 - val_accuracy: 0.9441
    Epoch 38/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3615 - accuracy: 0.9650 - val_loss: 1.7760 - val_accuracy: 0.9255
    Epoch 39/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7849 - accuracy: 0.9382 - val_loss: 2.4883 - val_accuracy: 0.9068
    Epoch 40/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5865 - accuracy: 0.9619 - val_loss: 1.7492 - val_accuracy: 0.9379
    Epoch 41/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5957 - accuracy: 0.9537 - val_loss: 2.8667 - val_accuracy: 0.9006
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8158 - accuracy: 0.9382 - val_loss: 1.8367 - val_accuracy: 0.9130
    Epoch 43/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3955 - accuracy: 0.9691 - val_loss: 1.3361 - val_accuracy: 0.9317
    Epoch 44/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8240 - accuracy: 0.9413 - val_loss: 2.1563 - val_accuracy: 0.9317
    Epoch 45/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6279 - accuracy: 0.9578 - val_loss: 1.9986 - val_accuracy: 0.9317
    Epoch 46/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3851 - accuracy: 0.9629 - val_loss: 2.0695 - val_accuracy: 0.9130
    Epoch 47/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6194 - accuracy: 0.9557 - val_loss: 2.7696 - val_accuracy: 0.9006
    Epoch 48/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8172 - accuracy: 0.9475 - val_loss: 1.7522 - val_accuracy: 0.9130
    Epoch 49/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5541 - accuracy: 0.9619 - val_loss: 2.2928 - val_accuracy: 0.9255
    Epoch 50/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6450 - accuracy: 0.9454 - val_loss: 1.5926 - val_accuracy: 0.9255
    Epoch 51/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6792 - accuracy: 0.9609 - val_loss: 1.4706 - val_accuracy: 0.9255
    Epoch 52/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6051 - accuracy: 0.9588 - val_loss: 1.3800 - val_accuracy: 0.9068
    Epoch 53/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4582 - accuracy: 0.9578 - val_loss: 1.4733 - val_accuracy: 0.9068
    Epoch 54/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5294 - accuracy: 0.9660 - val_loss: 1.9374 - val_accuracy: 0.9068
    Epoch 55/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4683 - accuracy: 0.9609 - val_loss: 2.0099 - val_accuracy: 0.9255
    Epoch 56/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4990 - accuracy: 0.9578 - val_loss: 2.0614 - val_accuracy: 0.9068
    Epoch 57/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4135 - accuracy: 0.9650 - val_loss: 1.7017 - val_accuracy: 0.9441
    Epoch 58/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5248 - accuracy: 0.9650 - val_loss: 1.8274 - val_accuracy: 0.9317
    Epoch 59/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4704 - accuracy: 0.9578 - val_loss: 2.1673 - val_accuracy: 0.9006
    Epoch 60/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4894 - accuracy: 0.9712 - val_loss: 3.0544 - val_accuracy: 0.8882
    Epoch 61/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5203 - accuracy: 0.9619 - val_loss: 2.6525 - val_accuracy: 0.9317
    Epoch 62/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4905 - accuracy: 0.9609 - val_loss: 2.8288 - val_accuracy: 0.9255
    Epoch 63/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5595 - accuracy: 0.9547 - val_loss: 1.5544 - val_accuracy: 0.9565
    Epoch 64/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7214 - accuracy: 0.9609 - val_loss: 1.6677 - val_accuracy: 0.9503
    Epoch 65/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5831 - accuracy: 0.9526 - val_loss: 1.3720 - val_accuracy: 0.9565
    Epoch 66/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5223 - accuracy: 0.9588 - val_loss: 2.5048 - val_accuracy: 0.9317
    Epoch 67/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3984 - accuracy: 0.9732 - val_loss: 2.2411 - val_accuracy: 0.9255
    Epoch 68/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4112 - accuracy: 0.9722 - val_loss: 2.1504 - val_accuracy: 0.9255
    Epoch 69/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6217 - accuracy: 0.9681 - val_loss: 2.2824 - val_accuracy: 0.9255
    Epoch 70/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3941 - accuracy: 0.9691 - val_loss: 2.4081 - val_accuracy: 0.9068
    Epoch 71/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3532 - accuracy: 0.9743 - val_loss: 2.3171 - val_accuracy: 0.9441
    Epoch 72/100
    8/8 [==============================] - 9s 1s/step - loss: 0.3055 - accuracy: 0.9722 - val_loss: 2.0859 - val_accuracy: 0.9317
    Epoch 73/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2527 - accuracy: 0.9763 - val_loss: 2.8363 - val_accuracy: 0.8882
    Epoch 74/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5391 - accuracy: 0.9619 - val_loss: 2.9597 - val_accuracy: 0.8882
    Epoch 75/100
    8/8 [==============================] - 16s 2s/step - loss: 0.2799 - accuracy: 0.9794 - val_loss: 2.9741 - val_accuracy: 0.9068
    Epoch 76/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3607 - accuracy: 0.9722 - val_loss: 2.3512 - val_accuracy: 0.9130
    Epoch 77/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4199 - accuracy: 0.9722 - val_loss: 1.9626 - val_accuracy: 0.9317
    Epoch 78/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4517 - accuracy: 0.9712 - val_loss: 2.3976 - val_accuracy: 0.9317
    Epoch 79/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3475 - accuracy: 0.9732 - val_loss: 1.9896 - val_accuracy: 0.9503
    Epoch 80/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4276 - accuracy: 0.9732 - val_loss: 2.5661 - val_accuracy: 0.9130
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5854 - accuracy: 0.9650 - val_loss: 2.6730 - val_accuracy: 0.9193
    Epoch 82/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3955 - accuracy: 0.9691 - val_loss: 1.9200 - val_accuracy: 0.9379
    Epoch 83/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3374 - accuracy: 0.9794 - val_loss: 2.6640 - val_accuracy: 0.9130
    Epoch 84/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3437 - accuracy: 0.9784 - val_loss: 3.6750 - val_accuracy: 0.8944
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3938 - accuracy: 0.9722 - val_loss: 2.5063 - val_accuracy: 0.9255
    Epoch 86/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4686 - accuracy: 0.9691 - val_loss: 2.2103 - val_accuracy: 0.9379
    Epoch 87/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3678 - accuracy: 0.9794 - val_loss: 2.2694 - val_accuracy: 0.9317
    Epoch 88/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4674 - accuracy: 0.9753 - val_loss: 2.8205 - val_accuracy: 0.9379
    Epoch 89/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4462 - accuracy: 0.9701 - val_loss: 3.5596 - val_accuracy: 0.9193
    Epoch 90/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3992 - accuracy: 0.9743 - val_loss: 3.3231 - val_accuracy: 0.9130
    Epoch 91/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3110 - accuracy: 0.9804 - val_loss: 3.4029 - val_accuracy: 0.9130
    Epoch 92/100
    8/8 [==============================] - 8s 1s/step - loss: 0.2453 - accuracy: 0.9825 - val_loss: 2.5396 - val_accuracy: 0.9193
    Epoch 93/100
    8/8 [==============================] - 12s 1s/step - loss: 0.1082 - accuracy: 0.9887 - val_loss: 1.8694 - val_accuracy: 0.9379
    Epoch 94/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0816 - accuracy: 0.9897 - val_loss: 2.2056 - val_accuracy: 0.9317
    Epoch 95/100
    8/8 [==============================] - 8s 1s/step - loss: 0.2061 - accuracy: 0.9825 - val_loss: 3.2860 - val_accuracy: 0.9255
    Epoch 96/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2889 - accuracy: 0.9825 - val_loss: 2.8211 - val_accuracy: 0.9379
    Epoch 97/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4129 - accuracy: 0.9598 - val_loss: 3.1927 - val_accuracy: 0.9130
    Epoch 98/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2376 - accuracy: 0.9763 - val_loss: 2.6651 - val_accuracy: 0.9317
    Epoch 99/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2240 - accuracy: 0.9804 - val_loss: 2.9154 - val_accuracy: 0.9255
    Epoch 100/100
    8/8 [==============================] - 8s 1s/step - loss: 0.2009 - accuracy: 0.9794 - val_loss: 2.8577 - val_accuracy: 0.9193

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/53.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 9s 2s/step - loss: 2.3709 - accuracy: 0.9324

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.3709206581115723, 0.9323770403862]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9323770491803278
                  precision    recall  f1-score   support

               0       0.88      0.91      0.89       148
               1       0.96      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.92      0.92       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/54.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/55.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/56.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.88      0.91      0.89       148
               1       0.96      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.92      0.92       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.88      0.91      0.94      0.89      0.92      0.85       148
              1       0.96      0.94      0.91      0.95      0.92      0.86       340

    avg / total       0.93      0.93      0.92      0.93      0.92      0.86       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
