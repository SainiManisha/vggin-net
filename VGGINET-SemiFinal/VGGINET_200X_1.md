<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('200X', '1')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.6481 - accuracy: 0.4219WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 3.5853 - accuracy: 0.7525 - val_loss: 4.8139 - val_accuracy: 0.8950
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 2.0712 - accuracy: 0.8638 - val_loss: 12.6313 - val_accuracy: 0.7901
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4631 - accuracy: 0.8721 - val_loss: 5.0511 - val_accuracy: 0.8508
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2447 - accuracy: 0.8822 - val_loss: 3.7521 - val_accuracy: 0.8840
    Epoch 5/100
    9/9 [==============================] - 9s 1s/step - loss: 1.3414 - accuracy: 0.8850 - val_loss: 2.3341 - val_accuracy: 0.8785
    Epoch 6/100
    9/9 [==============================] - 9s 1s/step - loss: 1.2363 - accuracy: 0.8951 - val_loss: 0.8240 - val_accuracy: 0.9227
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8265 - accuracy: 0.9117 - val_loss: 0.9393 - val_accuracy: 0.9282
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5528 - accuracy: 0.9338 - val_loss: 1.0300 - val_accuracy: 0.9392
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7007 - accuracy: 0.9236 - val_loss: 0.8190 - val_accuracy: 0.9448
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7342 - accuracy: 0.9292 - val_loss: 0.6168 - val_accuracy: 0.9392
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7851 - accuracy: 0.9246 - val_loss: 1.1449 - val_accuracy: 0.9337
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7279 - accuracy: 0.9209 - val_loss: 0.5365 - val_accuracy: 0.9558
    Epoch 13/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7696 - accuracy: 0.9236 - val_loss: 1.3226 - val_accuracy: 0.9227
    Epoch 14/100
    9/9 [==============================] - 9s 1s/step - loss: 0.9205 - accuracy: 0.9384 - val_loss: 1.1585 - val_accuracy: 0.9392
    Epoch 15/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0742 - accuracy: 0.9108 - val_loss: 0.8244 - val_accuracy: 0.9282
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6865 - accuracy: 0.9338 - val_loss: 0.8035 - val_accuracy: 0.9448
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9399 - accuracy: 0.9200 - val_loss: 2.6569 - val_accuracy: 0.8950
    Epoch 18/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7888 - accuracy: 0.9430 - val_loss: 1.3891 - val_accuracy: 0.9282
    Epoch 19/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5387 - accuracy: 0.9457 - val_loss: 1.5281 - val_accuracy: 0.9337
    Epoch 20/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6246 - accuracy: 0.9522 - val_loss: 0.8332 - val_accuracy: 0.9558
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7075 - accuracy: 0.9420 - val_loss: 0.8631 - val_accuracy: 0.9448
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6259 - accuracy: 0.9420 - val_loss: 0.4774 - val_accuracy: 0.9613
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5786 - accuracy: 0.9494 - val_loss: 1.7268 - val_accuracy: 0.9171
    Epoch 24/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5643 - accuracy: 0.9485 - val_loss: 0.4623 - val_accuracy: 0.9392
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6545 - accuracy: 0.9485 - val_loss: 0.8666 - val_accuracy: 0.9337
    Epoch 26/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5249 - accuracy: 0.9494 - val_loss: 0.5472 - val_accuracy: 0.9613
    Epoch 27/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5642 - accuracy: 0.9595 - val_loss: 1.3333 - val_accuracy: 0.9227
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6024 - accuracy: 0.9540 - val_loss: 0.8128 - val_accuracy: 0.9503
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5094 - accuracy: 0.9549 - val_loss: 0.6014 - val_accuracy: 0.9669
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6066 - accuracy: 0.9476 - val_loss: 0.2890 - val_accuracy: 0.9779
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4253 - accuracy: 0.9623 - val_loss: 0.5593 - val_accuracy: 0.9669
    Epoch 32/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7389 - accuracy: 0.9512 - val_loss: 0.4030 - val_accuracy: 0.9669
    Epoch 33/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6216 - accuracy: 0.9522 - val_loss: 0.5281 - val_accuracy: 0.9503
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4312 - accuracy: 0.9586 - val_loss: 0.3944 - val_accuracy: 0.9779
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3495 - accuracy: 0.9706 - val_loss: 0.8075 - val_accuracy: 0.9503
    Epoch 36/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4497 - accuracy: 0.9687 - val_loss: 0.8977 - val_accuracy: 0.9503
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3910 - accuracy: 0.9604 - val_loss: 0.7586 - val_accuracy: 0.9558
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3796 - accuracy: 0.9715 - val_loss: 1.9649 - val_accuracy: 0.9392
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4036 - accuracy: 0.9660 - val_loss: 3.0864 - val_accuracy: 0.9006
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6003 - accuracy: 0.9577 - val_loss: 0.9889 - val_accuracy: 0.9503
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6228 - accuracy: 0.9568 - val_loss: 0.6624 - val_accuracy: 0.9558
    Epoch 42/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3777 - accuracy: 0.9650 - val_loss: 0.2156 - val_accuracy: 0.9834
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3819 - accuracy: 0.9650 - val_loss: 0.0620 - val_accuracy: 0.9724
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3655 - accuracy: 0.9724 - val_loss: 0.1397 - val_accuracy: 0.9669
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3230 - accuracy: 0.9779 - val_loss: 0.6847 - val_accuracy: 0.9669
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5329 - accuracy: 0.9641 - val_loss: 1.7601 - val_accuracy: 0.9448
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5810 - accuracy: 0.9531 - val_loss: 1.0679 - val_accuracy: 0.9558
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5032 - accuracy: 0.9623 - val_loss: 2.0338 - val_accuracy: 0.9392
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4027 - accuracy: 0.9715 - val_loss: 1.0414 - val_accuracy: 0.9613
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5533 - accuracy: 0.9660 - val_loss: 0.4170 - val_accuracy: 0.9724
    Epoch 51/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4977 - accuracy: 0.9632 - val_loss: 1.0277 - val_accuracy: 0.9669
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5239 - accuracy: 0.9660 - val_loss: 0.9264 - val_accuracy: 0.9613
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3586 - accuracy: 0.9752 - val_loss: 0.9000 - val_accuracy: 0.9503
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3992 - accuracy: 0.9724 - val_loss: 0.5879 - val_accuracy: 0.9669
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3721 - accuracy: 0.9669 - val_loss: 1.1551 - val_accuracy: 0.9448
    Epoch 56/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4469 - accuracy: 0.9650 - val_loss: 1.5140 - val_accuracy: 0.9503
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4675 - accuracy: 0.9733 - val_loss: 0.8290 - val_accuracy: 0.9613
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2589 - accuracy: 0.9770 - val_loss: 0.6246 - val_accuracy: 0.9613
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4131 - accuracy: 0.9632 - val_loss: 1.2465 - val_accuracy: 0.9669
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3116 - accuracy: 0.9761 - val_loss: 2.3195 - val_accuracy: 0.9282
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5362 - accuracy: 0.9733 - val_loss: 0.6419 - val_accuracy: 0.9834
    Epoch 62/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3701 - accuracy: 0.9706 - val_loss: 1.0054 - val_accuracy: 0.9669
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4799 - accuracy: 0.9687 - val_loss: 1.0744 - val_accuracy: 0.9669
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3031 - accuracy: 0.9724 - val_loss: 1.0755 - val_accuracy: 0.9503
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3289 - accuracy: 0.9752 - val_loss: 0.5243 - val_accuracy: 0.9724
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2265 - accuracy: 0.9761 - val_loss: 0.9472 - val_accuracy: 0.9503
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3126 - accuracy: 0.9779 - val_loss: 0.8485 - val_accuracy: 0.9724
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4259 - accuracy: 0.9770 - val_loss: 0.8595 - val_accuracy: 0.9669
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4564 - accuracy: 0.9687 - val_loss: 1.1866 - val_accuracy: 0.9669
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3255 - accuracy: 0.9770 - val_loss: 3.2368 - val_accuracy: 0.9503
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4653 - accuracy: 0.9788 - val_loss: 1.0796 - val_accuracy: 0.9669
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4413 - accuracy: 0.9742 - val_loss: 0.4712 - val_accuracy: 0.9669
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2366 - accuracy: 0.9871 - val_loss: 0.3593 - val_accuracy: 0.9834
    Epoch 74/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1690 - accuracy: 0.9844 - val_loss: 0.3048 - val_accuracy: 0.9724
    Epoch 75/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2352 - accuracy: 0.9807 - val_loss: 0.5601 - val_accuracy: 0.9558
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2218 - accuracy: 0.9880 - val_loss: 2.5838 - val_accuracy: 0.9006
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2619 - accuracy: 0.9798 - val_loss: 1.0083 - val_accuracy: 0.9613
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2325 - accuracy: 0.9834 - val_loss: 0.5555 - val_accuracy: 0.9669
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3287 - accuracy: 0.9853 - val_loss: 0.2302 - val_accuracy: 0.9779
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1339 - accuracy: 0.9890 - val_loss: 0.3775 - val_accuracy: 0.9779
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1340 - accuracy: 0.9917 - val_loss: 0.5076 - val_accuracy: 0.9613
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2351 - accuracy: 0.9816 - val_loss: 0.2488 - val_accuracy: 0.9779
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3065 - accuracy: 0.9752 - val_loss: 0.3659 - val_accuracy: 0.9724
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2934 - accuracy: 0.9825 - val_loss: 1.0929 - val_accuracy: 0.9669
    Epoch 85/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2961 - accuracy: 0.9816 - val_loss: 1.6841 - val_accuracy: 0.9558
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2556 - accuracy: 0.9862 - val_loss: 1.1376 - val_accuracy: 0.9669
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2357 - accuracy: 0.9798 - val_loss: 0.3850 - val_accuracy: 0.9834
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3694 - accuracy: 0.9807 - val_loss: 0.2938 - val_accuracy: 0.9779
    Epoch 89/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2925 - accuracy: 0.9816 - val_loss: 0.7689 - val_accuracy: 0.9613
    Epoch 90/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2059 - accuracy: 0.9880 - val_loss: 1.2417 - val_accuracy: 0.9613
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2055 - accuracy: 0.9862 - val_loss: 0.8695 - val_accuracy: 0.9724
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3002 - accuracy: 0.9825 - val_loss: 0.7801 - val_accuracy: 0.9613
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1498 - accuracy: 0.9871 - val_loss: 1.5523 - val_accuracy: 0.9669
    Epoch 94/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1995 - accuracy: 0.9853 - val_loss: 1.0796 - val_accuracy: 0.9724
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3616 - accuracy: 0.9742 - val_loss: 0.9256 - val_accuracy: 0.9669
    Epoch 96/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1748 - accuracy: 0.9862 - val_loss: 0.7960 - val_accuracy: 0.9779
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2544 - accuracy: 0.9853 - val_loss: 0.7603 - val_accuracy: 0.9613
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3091 - accuracy: 0.9834 - val_loss: 1.2942 - val_accuracy: 0.9613
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1009 - accuracy: 0.9899 - val_loss: 1.3748 - val_accuracy: 0.9613
    Epoch 100/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2277 - accuracy: 0.9825 - val_loss: 1.5705 - val_accuracy: 0.9669

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/9.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 13s 3s/step - loss: 2.1480 - accuracy: 0.9578

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.1480038166046143, 0.957798182964325]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9577981651376147
                  precision    recall  f1-score   support

               0       0.97      0.88      0.92       158
               1       0.95      0.99      0.97       387

        accuracy                           0.96       545
       macro avg       0.96      0.93      0.95       545
    weighted avg       0.96      0.96      0.96       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/10.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/11.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/12.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.88      0.92       158
               1       0.95      0.99      0.97       387

        accuracy                           0.96       545
       macro avg       0.96      0.93      0.95       545
    weighted avg       0.96      0.96      0.96       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.88      0.99      0.92      0.93      0.86       158
              1       0.95      0.99      0.88      0.97      0.93      0.88       387

    avg / total       0.96      0.96      0.91      0.96      0.93      0.87       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
