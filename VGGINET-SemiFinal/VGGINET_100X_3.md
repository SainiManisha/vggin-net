<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('100X', '3')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0554 - accuracy: 0.5469WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 21s 2s/step - loss: 2.4099 - accuracy: 0.7668 - val_loss: 14.4510 - val_accuracy: 0.8235
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 2.0484 - accuracy: 0.8537 - val_loss: 1.6573 - val_accuracy: 0.9198
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 2.6807 - accuracy: 0.8457 - val_loss: 1.5867 - val_accuracy: 0.9358
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.4880 - accuracy: 0.8998 - val_loss: 1.6157 - val_accuracy: 0.9358
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1218 - accuracy: 0.9113 - val_loss: 3.6266 - val_accuracy: 0.8610
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2648 - accuracy: 0.8998 - val_loss: 1.6434 - val_accuracy: 0.9037
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9153 - accuracy: 0.9255 - val_loss: 1.0865 - val_accuracy: 0.9465
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7940 - accuracy: 0.9184 - val_loss: 1.2829 - val_accuracy: 0.9358
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0905 - accuracy: 0.9034 - val_loss: 1.4676 - val_accuracy: 0.9251
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9945 - accuracy: 0.9273 - val_loss: 1.0975 - val_accuracy: 0.9358
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9577 - accuracy: 0.9034 - val_loss: 1.3961 - val_accuracy: 0.9144
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8449 - accuracy: 0.9264 - val_loss: 1.6088 - val_accuracy: 0.8930
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9060 - accuracy: 0.9246 - val_loss: 0.5162 - val_accuracy: 0.9519
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9563 - accuracy: 0.9246 - val_loss: 0.7412 - val_accuracy: 0.9412
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6150 - accuracy: 0.9317 - val_loss: 0.8963 - val_accuracy: 0.9358
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8608 - accuracy: 0.9291 - val_loss: 1.0279 - val_accuracy: 0.9358
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7973 - accuracy: 0.9362 - val_loss: 0.5856 - val_accuracy: 0.9358
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1067 - accuracy: 0.9211 - val_loss: 0.7842 - val_accuracy: 0.9412
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6963 - accuracy: 0.9379 - val_loss: 0.9446 - val_accuracy: 0.9251
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7876 - accuracy: 0.9406 - val_loss: 1.0064 - val_accuracy: 0.9091
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6689 - accuracy: 0.9415 - val_loss: 0.7980 - val_accuracy: 0.9465
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7433 - accuracy: 0.9353 - val_loss: 0.7757 - val_accuracy: 0.9358
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6767 - accuracy: 0.9521 - val_loss: 0.3184 - val_accuracy: 0.9679
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6585 - accuracy: 0.9477 - val_loss: 0.2663 - val_accuracy: 0.9786
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6906 - accuracy: 0.9539 - val_loss: 0.8372 - val_accuracy: 0.9412
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8967 - accuracy: 0.9300 - val_loss: 0.3950 - val_accuracy: 0.9626
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6534 - accuracy: 0.9574 - val_loss: 0.4455 - val_accuracy: 0.9679
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4152 - accuracy: 0.9548 - val_loss: 0.6046 - val_accuracy: 0.9572
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5476 - accuracy: 0.9548 - val_loss: 0.6260 - val_accuracy: 0.9679
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6284 - accuracy: 0.9548 - val_loss: 0.1631 - val_accuracy: 0.9840
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5920 - accuracy: 0.9592 - val_loss: 0.1294 - val_accuracy: 0.9840
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3741 - accuracy: 0.9637 - val_loss: 0.3485 - val_accuracy: 0.9733
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5067 - accuracy: 0.9583 - val_loss: 0.2690 - val_accuracy: 0.9786
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4199 - accuracy: 0.9690 - val_loss: 0.2673 - val_accuracy: 0.9733
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4641 - accuracy: 0.9672 - val_loss: 0.1765 - val_accuracy: 0.9786
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4819 - accuracy: 0.9690 - val_loss: 0.5625 - val_accuracy: 0.9626
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3397 - accuracy: 0.9690 - val_loss: 0.3557 - val_accuracy: 0.9626
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5768 - accuracy: 0.9628 - val_loss: 0.3467 - val_accuracy: 0.9679
    Epoch 39/100
    9/9 [==============================] - 18s 2s/step - loss: 0.5078 - accuracy: 0.9654 - val_loss: 0.5581 - val_accuracy: 0.9626
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4441 - accuracy: 0.9699 - val_loss: 0.2074 - val_accuracy: 0.9840
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7941 - accuracy: 0.9521 - val_loss: 0.4279 - val_accuracy: 0.9786
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3434 - accuracy: 0.9699 - val_loss: 0.6024 - val_accuracy: 0.9733
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4560 - accuracy: 0.9610 - val_loss: 0.1623 - val_accuracy: 0.9893
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2994 - accuracy: 0.9707 - val_loss: 0.2739 - val_accuracy: 0.9840
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5076 - accuracy: 0.9610 - val_loss: 0.6021 - val_accuracy: 0.9733
    Epoch 46/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4971 - accuracy: 0.9681 - val_loss: 0.1633 - val_accuracy: 0.9786
    Epoch 47/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4930 - accuracy: 0.9610 - val_loss: 0.0274 - val_accuracy: 0.9947
    Epoch 48/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4926 - accuracy: 0.9699 - val_loss: 0.1959 - val_accuracy: 0.9893
    Epoch 49/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3147 - accuracy: 0.9725 - val_loss: 0.3534 - val_accuracy: 0.9786
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5044 - accuracy: 0.9637 - val_loss: 0.2323 - val_accuracy: 0.9840
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3287 - accuracy: 0.9707 - val_loss: 0.2334 - val_accuracy: 0.9786
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6425 - accuracy: 0.9637 - val_loss: 0.3914 - val_accuracy: 0.9786
    Epoch 53/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3547 - accuracy: 0.9725 - val_loss: 0.9609 - val_accuracy: 0.9572
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4652 - accuracy: 0.9690 - val_loss: 0.1693 - val_accuracy: 0.9840
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1676 - accuracy: 0.9805 - val_loss: 0.5058 - val_accuracy: 0.9572
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3344 - accuracy: 0.9734 - val_loss: 1.2124 - val_accuracy: 0.9358
    Epoch 57/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4324 - accuracy: 0.9663 - val_loss: 0.1901 - val_accuracy: 0.9786
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4472 - accuracy: 0.9734 - val_loss: 0.1928 - val_accuracy: 0.9947
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4999 - accuracy: 0.9619 - val_loss: 0.6521 - val_accuracy: 0.9679
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3871 - accuracy: 0.9787 - val_loss: 0.4070 - val_accuracy: 0.9626
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3315 - accuracy: 0.9770 - val_loss: 0.0797 - val_accuracy: 0.9840
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2819 - accuracy: 0.9823 - val_loss: 0.2993 - val_accuracy: 0.9840
    Epoch 63/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5600 - accuracy: 0.9601 - val_loss: 0.5985 - val_accuracy: 0.9733
    Epoch 64/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6322 - accuracy: 0.9574 - val_loss: 1.4570 - val_accuracy: 0.9412
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4583 - accuracy: 0.9681 - val_loss: 0.1694 - val_accuracy: 0.9786
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4167 - accuracy: 0.9681 - val_loss: 0.0872 - val_accuracy: 0.9840
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2720 - accuracy: 0.9823 - val_loss: 0.3324 - val_accuracy: 0.9786
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3764 - accuracy: 0.9725 - val_loss: 0.5493 - val_accuracy: 0.9572
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2876 - accuracy: 0.9761 - val_loss: 0.2363 - val_accuracy: 0.9733
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1985 - accuracy: 0.9787 - val_loss: 0.1598 - val_accuracy: 0.9893
    Epoch 71/100
    9/9 [==============================] - 18s 2s/step - loss: 0.3544 - accuracy: 0.9787 - val_loss: 0.3598 - val_accuracy: 0.9733
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2527 - accuracy: 0.9823 - val_loss: 0.5402 - val_accuracy: 0.9733
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3490 - accuracy: 0.9734 - val_loss: 0.7581 - val_accuracy: 0.9733
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2046 - accuracy: 0.9858 - val_loss: 0.9579 - val_accuracy: 0.9626
    Epoch 75/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2473 - accuracy: 0.9805 - val_loss: 0.3687 - val_accuracy: 0.9679
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3356 - accuracy: 0.9840 - val_loss: 0.3409 - val_accuracy: 0.9893
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1474 - accuracy: 0.9814 - val_loss: 0.1918 - val_accuracy: 0.9840
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4544 - accuracy: 0.9770 - val_loss: 0.1982 - val_accuracy: 0.9893
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2561 - accuracy: 0.9778 - val_loss: 0.6437 - val_accuracy: 0.9733
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3674 - accuracy: 0.9734 - val_loss: 0.0414 - val_accuracy: 0.9947
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2318 - accuracy: 0.9823 - val_loss: 0.0506 - val_accuracy: 0.9893
    Epoch 82/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4192 - accuracy: 0.9752 - val_loss: 0.2971 - val_accuracy: 0.9786
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1546 - accuracy: 0.9840 - val_loss: 0.4017 - val_accuracy: 0.9733
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1876 - accuracy: 0.9876 - val_loss: 0.3014 - val_accuracy: 0.9786
    Epoch 85/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3178 - accuracy: 0.9770 - val_loss: 0.0722 - val_accuracy: 0.9893
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3118 - accuracy: 0.9840 - val_loss: 0.0361 - val_accuracy: 0.9947
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2183 - accuracy: 0.9840 - val_loss: 0.0034 - val_accuracy: 1.0000
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3165 - accuracy: 0.9778 - val_loss: 0.7357 - val_accuracy: 0.9412
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3395 - accuracy: 0.9796 - val_loss: 0.2806 - val_accuracy: 0.9733
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2967 - accuracy: 0.9814 - val_loss: 2.2126e-04 - val_accuracy: 1.0000
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3070 - accuracy: 0.9770 - val_loss: 0.0309 - val_accuracy: 0.9947
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3729 - accuracy: 0.9770 - val_loss: 0.0727 - val_accuracy: 0.9947
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3048 - accuracy: 0.9823 - val_loss: 0.4578 - val_accuracy: 0.9786
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5597 - accuracy: 0.9725 - val_loss: 0.4682 - val_accuracy: 0.9893
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3358 - accuracy: 0.9787 - val_loss: 0.5370 - val_accuracy: 0.9786
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5121 - accuracy: 0.9707 - val_loss: 0.2622 - val_accuracy: 0.9786
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3659 - accuracy: 0.9770 - val_loss: 0.2269 - val_accuracy: 0.9840
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2647 - accuracy: 0.9778 - val_loss: 0.8396 - val_accuracy: 0.9572
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2077 - accuracy: 0.9787 - val_loss: 0.9619 - val_accuracy: 0.9679
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2480 - accuracy: 0.9832 - val_loss: 0.6006 - val_accuracy: 0.9412

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/5.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 8s 2s/step - loss: 1.3955 - accuracy: 0.9541

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3954927921295166, 0.9540635943412781]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9540636042402827
                  precision    recall  f1-score   support

               0       0.89      0.96      0.92       164
               1       0.98      0.95      0.97       402

        accuracy                           0.95       566
       macro avg       0.94      0.96      0.95       566
    weighted avg       0.96      0.95      0.95       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/6.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/7.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/8.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.89      0.96      0.92       164
               1       0.98      0.95      0.97       402

        accuracy                           0.95       566
       macro avg       0.94      0.96      0.95       566
    weighted avg       0.96      0.95      0.95       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.89      0.96      0.95      0.92      0.96      0.91       164
              1       0.98      0.95      0.96      0.97      0.96      0.91       402

    avg / total       0.96      0.95      0.96      0.95      0.96      0.91       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
