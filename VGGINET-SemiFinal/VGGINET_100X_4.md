<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('100X', '4')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.9334 - accuracy: 0.5547WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 3.5961 - accuracy: 0.7562 - val_loss: 4.9295 - val_accuracy: 0.8930
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 1.8156 - accuracy: 0.8617 - val_loss: 4.0234 - val_accuracy: 0.8610
    Epoch 3/100
    9/9 [==============================] - 11s 1s/step - loss: 1.2552 - accuracy: 0.8768 - val_loss: 1.2626 - val_accuracy: 0.9412
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0457 - accuracy: 0.8954 - val_loss: 0.7081 - val_accuracy: 0.9572
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0286 - accuracy: 0.9034 - val_loss: 1.1863 - val_accuracy: 0.9358
    Epoch 6/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1557 - accuracy: 0.8998 - val_loss: 0.7799 - val_accuracy: 0.9412
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8573 - accuracy: 0.9140 - val_loss: 0.6619 - val_accuracy: 0.9626
    Epoch 8/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8686 - accuracy: 0.9184 - val_loss: 0.8525 - val_accuracy: 0.9465
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0911 - accuracy: 0.9043 - val_loss: 1.4346 - val_accuracy: 0.9144
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0936 - accuracy: 0.8998 - val_loss: 0.8773 - val_accuracy: 0.9251
    Epoch 11/100
    9/9 [==============================] - 17s 2s/step - loss: 1.1936 - accuracy: 0.9060 - val_loss: 0.7793 - val_accuracy: 0.9305
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0712 - accuracy: 0.9193 - val_loss: 1.1031 - val_accuracy: 0.9251
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9390 - accuracy: 0.9229 - val_loss: 0.8819 - val_accuracy: 0.9519
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7974 - accuracy: 0.9300 - val_loss: 1.3667 - val_accuracy: 0.9198
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9910 - accuracy: 0.9176 - val_loss: 0.9502 - val_accuracy: 0.9465
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2655 - accuracy: 0.9113 - val_loss: 0.8157 - val_accuracy: 0.9519
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9464 - accuracy: 0.9335 - val_loss: 0.6393 - val_accuracy: 0.9519
    Epoch 18/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7775 - accuracy: 0.9362 - val_loss: 0.4928 - val_accuracy: 0.9626
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7165 - accuracy: 0.9433 - val_loss: 0.4660 - val_accuracy: 0.9626
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6742 - accuracy: 0.9406 - val_loss: 0.7230 - val_accuracy: 0.9412
    Epoch 21/100
    9/9 [==============================] - 18s 2s/step - loss: 0.6259 - accuracy: 0.9504 - val_loss: 0.5051 - val_accuracy: 0.9679
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5490 - accuracy: 0.9495 - val_loss: 0.9254 - val_accuracy: 0.9305
    Epoch 23/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5635 - accuracy: 0.9539 - val_loss: 0.2513 - val_accuracy: 0.9572
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7455 - accuracy: 0.9433 - val_loss: 0.9089 - val_accuracy: 0.9519
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5448 - accuracy: 0.9521 - val_loss: 0.2561 - val_accuracy: 0.9626
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6424 - accuracy: 0.9486 - val_loss: 0.3018 - val_accuracy: 0.9626
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6203 - accuracy: 0.9495 - val_loss: 0.3411 - val_accuracy: 0.9679
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6343 - accuracy: 0.9539 - val_loss: 0.3696 - val_accuracy: 0.9572
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6222 - accuracy: 0.9512 - val_loss: 0.5653 - val_accuracy: 0.9519
    Epoch 30/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8657 - accuracy: 0.9441 - val_loss: 0.3955 - val_accuracy: 0.9733
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5850 - accuracy: 0.9574 - val_loss: 0.6029 - val_accuracy: 0.9572
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7500 - accuracy: 0.9459 - val_loss: 0.2985 - val_accuracy: 0.9679
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4073 - accuracy: 0.9681 - val_loss: 0.2943 - val_accuracy: 0.9679
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5993 - accuracy: 0.9566 - val_loss: 1.0095 - val_accuracy: 0.9091
    Epoch 35/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7346 - accuracy: 0.9459 - val_loss: 0.4196 - val_accuracy: 0.9679
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7552 - accuracy: 0.9530 - val_loss: 1.0217 - val_accuracy: 0.9572
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6501 - accuracy: 0.9557 - val_loss: 1.5810 - val_accuracy: 0.9519
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8286 - accuracy: 0.9450 - val_loss: 0.8387 - val_accuracy: 0.9519
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5015 - accuracy: 0.9690 - val_loss: 1.3947 - val_accuracy: 0.9305
    Epoch 40/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5782 - accuracy: 0.9601 - val_loss: 0.3544 - val_accuracy: 0.9786
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4626 - accuracy: 0.9619 - val_loss: 0.3453 - val_accuracy: 0.9840
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4937 - accuracy: 0.9654 - val_loss: 0.1799 - val_accuracy: 0.9893
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2711 - accuracy: 0.9672 - val_loss: 0.2118 - val_accuracy: 0.9733
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2720 - accuracy: 0.9743 - val_loss: 0.4731 - val_accuracy: 0.9626
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3664 - accuracy: 0.9778 - val_loss: 0.3059 - val_accuracy: 0.9733
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4460 - accuracy: 0.9681 - val_loss: 0.2146 - val_accuracy: 0.9786
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3215 - accuracy: 0.9734 - val_loss: 0.3713 - val_accuracy: 0.9572
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3631 - accuracy: 0.9707 - val_loss: 0.4370 - val_accuracy: 0.9733
    Epoch 49/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4159 - accuracy: 0.9610 - val_loss: 0.5178 - val_accuracy: 0.9840
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5345 - accuracy: 0.9663 - val_loss: 0.5021 - val_accuracy: 0.9572
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6977 - accuracy: 0.9574 - val_loss: 0.3334 - val_accuracy: 0.9679
    Epoch 52/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4859 - accuracy: 0.9619 - val_loss: 0.2988 - val_accuracy: 0.9786
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5052 - accuracy: 0.9654 - val_loss: 0.0984 - val_accuracy: 0.9947
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2713 - accuracy: 0.9725 - val_loss: 0.3400 - val_accuracy: 0.9733
    Epoch 55/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5664 - accuracy: 0.9734 - val_loss: 0.3002 - val_accuracy: 0.9786
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4455 - accuracy: 0.9663 - val_loss: 0.7415 - val_accuracy: 0.9786
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4254 - accuracy: 0.9707 - val_loss: 0.7457 - val_accuracy: 0.9840
    Epoch 58/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4689 - accuracy: 0.9663 - val_loss: 0.6313 - val_accuracy: 0.9679
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3571 - accuracy: 0.9787 - val_loss: 0.4338 - val_accuracy: 0.9786
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4475 - accuracy: 0.9734 - val_loss: 0.3743 - val_accuracy: 0.9733
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5476 - accuracy: 0.9637 - val_loss: 0.0567 - val_accuracy: 0.9893
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3097 - accuracy: 0.9752 - val_loss: 0.4387 - val_accuracy: 0.9786
    Epoch 63/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4862 - accuracy: 0.9690 - val_loss: 0.0645 - val_accuracy: 0.9893
    Epoch 64/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3923 - accuracy: 0.9707 - val_loss: 0.3997 - val_accuracy: 0.9786
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3659 - accuracy: 0.9734 - val_loss: 0.0968 - val_accuracy: 0.9893
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4360 - accuracy: 0.9743 - val_loss: 0.2249 - val_accuracy: 0.9893
    Epoch 67/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4846 - accuracy: 0.9716 - val_loss: 0.3786 - val_accuracy: 0.9786
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4285 - accuracy: 0.9761 - val_loss: 0.2392 - val_accuracy: 0.9840
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3551 - accuracy: 0.9778 - val_loss: 0.2367 - val_accuracy: 0.9840
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2879 - accuracy: 0.9725 - val_loss: 0.4867 - val_accuracy: 0.9679
    Epoch 71/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3923 - accuracy: 0.9787 - val_loss: 0.2074 - val_accuracy: 0.9733
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4116 - accuracy: 0.9610 - val_loss: 0.2529 - val_accuracy: 0.9786
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5291 - accuracy: 0.9672 - val_loss: 0.1746 - val_accuracy: 0.9840
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2758 - accuracy: 0.9805 - val_loss: 1.1941 - val_accuracy: 0.9519
    Epoch 75/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3964 - accuracy: 0.9725 - val_loss: 0.6652 - val_accuracy: 0.9626
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2281 - accuracy: 0.9849 - val_loss: 0.5799 - val_accuracy: 0.9840
    Epoch 77/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2447 - accuracy: 0.9832 - val_loss: 0.9380 - val_accuracy: 0.9840
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2405 - accuracy: 0.9832 - val_loss: 0.6891 - val_accuracy: 0.9786
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3164 - accuracy: 0.9778 - val_loss: 0.2714 - val_accuracy: 0.9786
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2690 - accuracy: 0.9778 - val_loss: 0.0064 - val_accuracy: 0.9947
    Epoch 81/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3720 - accuracy: 0.9796 - val_loss: 0.2447 - val_accuracy: 0.9840
    Epoch 82/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4109 - accuracy: 0.9778 - val_loss: 0.4234 - val_accuracy: 0.9626
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1868 - accuracy: 0.9814 - val_loss: 1.0850 - val_accuracy: 0.9572
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3262 - accuracy: 0.9823 - val_loss: 1.0853 - val_accuracy: 0.9733
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2311 - accuracy: 0.9840 - val_loss: 1.8856 - val_accuracy: 0.9358
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3056 - accuracy: 0.9787 - val_loss: 0.2194 - val_accuracy: 0.9840
    Epoch 87/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3920 - accuracy: 0.9761 - val_loss: 0.3281 - val_accuracy: 0.9679
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4005 - accuracy: 0.9699 - val_loss: 0.1884 - val_accuracy: 0.9840
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2991 - accuracy: 0.9814 - val_loss: 6.0561e-08 - val_accuracy: 1.0000
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1890 - accuracy: 0.9858 - val_loss: 0.0349 - val_accuracy: 0.9947
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2105 - accuracy: 0.9858 - val_loss: 0.1188 - val_accuracy: 0.9893
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3100 - accuracy: 0.9805 - val_loss: 0.2384 - val_accuracy: 0.9893
    Epoch 93/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3332 - accuracy: 0.9805 - val_loss: 2.7086e-05 - val_accuracy: 1.0000
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2794 - accuracy: 0.9840 - val_loss: 0.3399 - val_accuracy: 0.9947
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1632 - accuracy: 0.9867 - val_loss: 0.0784 - val_accuracy: 0.9840
    Epoch 96/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2794 - accuracy: 0.9796 - val_loss: 0.0974 - val_accuracy: 0.9840
    Epoch 97/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1711 - accuracy: 0.9840 - val_loss: 0.3372 - val_accuracy: 0.9840
    Epoch 98/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1660 - accuracy: 0.9832 - val_loss: 0.0213 - val_accuracy: 0.9947
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2804 - accuracy: 0.9752 - val_loss: 0.0474 - val_accuracy: 0.9893
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3551 - accuracy: 0.9832 - val_loss: 4.8192e-07 - val_accuracy: 1.0000

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/33.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 8s 2s/step - loss: 0.8033 - accuracy: 0.9753

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.8033291697502136, 0.9752650260925293]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9752650176678446
                  precision    recall  f1-score   support

               0       0.95      0.96      0.96       164
               1       0.98      0.98      0.98       402

        accuracy                           0.98       566
       macro avg       0.97      0.97      0.97       566
    weighted avg       0.98      0.98      0.98       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/34.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/35.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/36.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.96      0.96       164
               1       0.98      0.98      0.98       402

        accuracy                           0.98       566
       macro avg       0.97      0.97      0.97       566
    weighted avg       0.98      0.98      0.98       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.96      0.98      0.96      0.97      0.94       164
              1       0.98      0.98      0.96      0.98      0.97      0.95       402

    avg / total       0.98      0.98      0.97      0.98      0.97      0.94       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
