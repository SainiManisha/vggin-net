<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('400X', '1')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.2739 - accuracy: 0.5156WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 15s 2s/step - loss: 4.4220 - accuracy: 0.6952 - val_loss: 8.3376 - val_accuracy: 0.8882
    Epoch 2/100
    8/8 [==============================] - 12s 2s/step - loss: 2.5003 - accuracy: 0.8301 - val_loss: 4.3064 - val_accuracy: 0.8696
    Epoch 3/100
    8/8 [==============================] - 8s 1s/step - loss: 1.9416 - accuracy: 0.8558 - val_loss: 4.1648 - val_accuracy: 0.8385
    Epoch 4/100
    8/8 [==============================] - 8s 1s/step - loss: 1.5780 - accuracy: 0.8744 - val_loss: 2.9590 - val_accuracy: 0.8447
    Epoch 5/100
    8/8 [==============================] - 14s 2s/step - loss: 1.4695 - accuracy: 0.8702 - val_loss: 1.8376 - val_accuracy: 0.9193
    Epoch 6/100
    8/8 [==============================] - 8s 1s/step - loss: 1.3336 - accuracy: 0.8764 - val_loss: 2.5088 - val_accuracy: 0.8634
    Epoch 7/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1221 - accuracy: 0.8898 - val_loss: 1.3539 - val_accuracy: 0.9193
    Epoch 8/100
    8/8 [==============================] - 8s 1s/step - loss: 1.0051 - accuracy: 0.8950 - val_loss: 1.4181 - val_accuracy: 0.9379
    Epoch 9/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9740 - accuracy: 0.9114 - val_loss: 2.1322 - val_accuracy: 0.8882
    Epoch 10/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6757 - accuracy: 0.9248 - val_loss: 1.1967 - val_accuracy: 0.9068
    Epoch 11/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0677 - accuracy: 0.9011 - val_loss: 0.9186 - val_accuracy: 0.9255
    Epoch 12/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9207 - accuracy: 0.9114 - val_loss: 1.1598 - val_accuracy: 0.9068
    Epoch 13/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9723 - accuracy: 0.9022 - val_loss: 1.0642 - val_accuracy: 0.9193
    Epoch 14/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9132 - accuracy: 0.9063 - val_loss: 1.0152 - val_accuracy: 0.9255
    Epoch 15/100
    8/8 [==============================] - 8s 1s/step - loss: 1.0935 - accuracy: 0.9094 - val_loss: 1.0477 - val_accuracy: 0.9317
    Epoch 16/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1777 - accuracy: 0.9156 - val_loss: 1.4841 - val_accuracy: 0.9255
    Epoch 17/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1512 - accuracy: 0.8939 - val_loss: 1.4363 - val_accuracy: 0.9193
    Epoch 18/100
    8/8 [==============================] - 9s 1s/step - loss: 1.2013 - accuracy: 0.9207 - val_loss: 0.9264 - val_accuracy: 0.9317
    Epoch 19/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0222 - accuracy: 0.9094 - val_loss: 1.2362 - val_accuracy: 0.9255
    Epoch 20/100
    8/8 [==============================] - 8s 1s/step - loss: 0.8771 - accuracy: 0.9269 - val_loss: 1.1082 - val_accuracy: 0.9317
    Epoch 21/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0928 - accuracy: 0.9135 - val_loss: 0.8958 - val_accuracy: 0.9317
    Epoch 22/100
    8/8 [==============================] - 8s 1s/step - loss: 1.0520 - accuracy: 0.9228 - val_loss: 1.3983 - val_accuracy: 0.9317
    Epoch 23/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8939 - accuracy: 0.9258 - val_loss: 1.5508 - val_accuracy: 0.9130
    Epoch 24/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8534 - accuracy: 0.9217 - val_loss: 1.6674 - val_accuracy: 0.9006
    Epoch 25/100
    8/8 [==============================] - 9s 1s/step - loss: 0.7166 - accuracy: 0.9228 - val_loss: 1.6902 - val_accuracy: 0.9130
    Epoch 26/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6750 - accuracy: 0.9413 - val_loss: 1.3714 - val_accuracy: 0.9255
    Epoch 27/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7148 - accuracy: 0.9423 - val_loss: 1.3249 - val_accuracy: 0.9193
    Epoch 28/100
    8/8 [==============================] - 9s 1s/step - loss: 0.6618 - accuracy: 0.9413 - val_loss: 1.8600 - val_accuracy: 0.9068
    Epoch 29/100
    8/8 [==============================] - 9s 1s/step - loss: 0.6970 - accuracy: 0.9392 - val_loss: 2.5701 - val_accuracy: 0.8820
    Epoch 30/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8268 - accuracy: 0.9300 - val_loss: 2.9485 - val_accuracy: 0.8571
    Epoch 31/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9771 - accuracy: 0.9475 - val_loss: 1.5793 - val_accuracy: 0.9068
    Epoch 32/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7503 - accuracy: 0.9331 - val_loss: 1.3537 - val_accuracy: 0.9130
    Epoch 33/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5140 - accuracy: 0.9537 - val_loss: 1.7085 - val_accuracy: 0.9068
    Epoch 34/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7951 - accuracy: 0.9392 - val_loss: 2.6330 - val_accuracy: 0.9006
    Epoch 35/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7321 - accuracy: 0.9361 - val_loss: 2.1856 - val_accuracy: 0.9006
    Epoch 36/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5508 - accuracy: 0.9495 - val_loss: 5.0571 - val_accuracy: 0.8509
    Epoch 37/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7889 - accuracy: 0.9361 - val_loss: 3.7827 - val_accuracy: 0.8820
    Epoch 38/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7137 - accuracy: 0.9361 - val_loss: 1.9027 - val_accuracy: 0.9068
    Epoch 39/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7101 - accuracy: 0.9506 - val_loss: 1.9904 - val_accuracy: 0.9006
    Epoch 40/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6424 - accuracy: 0.9382 - val_loss: 1.8346 - val_accuracy: 0.8944
    Epoch 41/100
    8/8 [==============================] - 8s 1s/step - loss: 0.5062 - accuracy: 0.9475 - val_loss: 2.0764 - val_accuracy: 0.8944
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6075 - accuracy: 0.9537 - val_loss: 1.6451 - val_accuracy: 0.9193
    Epoch 43/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6533 - accuracy: 0.9516 - val_loss: 1.7446 - val_accuracy: 0.9255
    Epoch 44/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7049 - accuracy: 0.9516 - val_loss: 2.1969 - val_accuracy: 0.9006
    Epoch 45/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4278 - accuracy: 0.9567 - val_loss: 1.8450 - val_accuracy: 0.9317
    Epoch 46/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4252 - accuracy: 0.9701 - val_loss: 1.7839 - val_accuracy: 0.9068
    Epoch 47/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5403 - accuracy: 0.9629 - val_loss: 1.2213 - val_accuracy: 0.9130
    Epoch 48/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5456 - accuracy: 0.9547 - val_loss: 2.1430 - val_accuracy: 0.9255
    Epoch 49/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6051 - accuracy: 0.9588 - val_loss: 1.6251 - val_accuracy: 0.9130
    Epoch 50/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6779 - accuracy: 0.9609 - val_loss: 2.1403 - val_accuracy: 0.8944
    Epoch 51/100
    8/8 [==============================] - 9s 1s/step - loss: 0.5659 - accuracy: 0.9557 - val_loss: 1.6448 - val_accuracy: 0.9068
    Epoch 52/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3411 - accuracy: 0.9691 - val_loss: 1.8822 - val_accuracy: 0.9255
    Epoch 53/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7336 - accuracy: 0.9567 - val_loss: 2.1326 - val_accuracy: 0.9068
    Epoch 54/100
    8/8 [==============================] - 9s 1s/step - loss: 0.5153 - accuracy: 0.9506 - val_loss: 2.1979 - val_accuracy: 0.9130
    Epoch 55/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6282 - accuracy: 0.9598 - val_loss: 2.0300 - val_accuracy: 0.9255
    Epoch 56/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5548 - accuracy: 0.9537 - val_loss: 1.5262 - val_accuracy: 0.9255
    Epoch 57/100
    8/8 [==============================] - 9s 1s/step - loss: 0.7052 - accuracy: 0.9547 - val_loss: 1.4572 - val_accuracy: 0.9441
    Epoch 58/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5627 - accuracy: 0.9526 - val_loss: 1.4101 - val_accuracy: 0.9379
    Epoch 59/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7843 - accuracy: 0.9434 - val_loss: 2.4266 - val_accuracy: 0.9441
    Epoch 60/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6871 - accuracy: 0.9526 - val_loss: 1.8609 - val_accuracy: 0.9006
    Epoch 61/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5637 - accuracy: 0.9588 - val_loss: 1.4489 - val_accuracy: 0.9441
    Epoch 62/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4539 - accuracy: 0.9629 - val_loss: 1.5697 - val_accuracy: 0.9441
    Epoch 63/100
    8/8 [==============================] - 9s 1s/step - loss: 0.4636 - accuracy: 0.9722 - val_loss: 1.5666 - val_accuracy: 0.9379
    Epoch 64/100
    8/8 [==============================] - 12s 2s/step - loss: 0.1938 - accuracy: 0.9732 - val_loss: 1.6778 - val_accuracy: 0.9565
    Epoch 65/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5851 - accuracy: 0.9660 - val_loss: 1.6023 - val_accuracy: 0.9627
    Epoch 66/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4924 - accuracy: 0.9619 - val_loss: 1.9312 - val_accuracy: 0.9317
    Epoch 67/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5134 - accuracy: 0.9691 - val_loss: 2.5965 - val_accuracy: 0.9193
    Epoch 68/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4452 - accuracy: 0.9681 - val_loss: 1.6152 - val_accuracy: 0.9317
    Epoch 69/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5215 - accuracy: 0.9598 - val_loss: 1.6677 - val_accuracy: 0.9379
    Epoch 70/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3999 - accuracy: 0.9640 - val_loss: 3.0643 - val_accuracy: 0.9006
    Epoch 71/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5670 - accuracy: 0.9537 - val_loss: 1.5905 - val_accuracy: 0.9130
    Epoch 72/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4769 - accuracy: 0.9681 - val_loss: 1.0304 - val_accuracy: 0.9441
    Epoch 73/100
    8/8 [==============================] - 9s 1s/step - loss: 0.3472 - accuracy: 0.9753 - val_loss: 1.5321 - val_accuracy: 0.9441
    Epoch 74/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3808 - accuracy: 0.9619 - val_loss: 1.8579 - val_accuracy: 0.9317
    Epoch 75/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4517 - accuracy: 0.9712 - val_loss: 2.4885 - val_accuracy: 0.9193
    Epoch 76/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3924 - accuracy: 0.9681 - val_loss: 2.5142 - val_accuracy: 0.9006
    Epoch 77/100
    8/8 [==============================] - 8s 1s/step - loss: 0.3342 - accuracy: 0.9773 - val_loss: 3.3725 - val_accuracy: 0.8820
    Epoch 78/100
    8/8 [==============================] - 9s 1s/step - loss: 0.3230 - accuracy: 0.9773 - val_loss: 2.3698 - val_accuracy: 0.9068
    Epoch 79/100
    8/8 [==============================] - 9s 1s/step - loss: 0.4613 - accuracy: 0.9722 - val_loss: 2.5777 - val_accuracy: 0.9130
    Epoch 80/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3909 - accuracy: 0.9773 - val_loss: 3.0608 - val_accuracy: 0.9006
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4605 - accuracy: 0.9650 - val_loss: 2.6720 - val_accuracy: 0.9068
    Epoch 82/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3006 - accuracy: 0.9722 - val_loss: 1.8249 - val_accuracy: 0.9255
    Epoch 83/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3566 - accuracy: 0.9743 - val_loss: 1.4237 - val_accuracy: 0.9503
    Epoch 84/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4142 - accuracy: 0.9712 - val_loss: 1.2140 - val_accuracy: 0.9565
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4330 - accuracy: 0.9753 - val_loss: 1.5528 - val_accuracy: 0.9503
    Epoch 86/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2975 - accuracy: 0.9784 - val_loss: 1.6773 - val_accuracy: 0.9441
    Epoch 87/100
    8/8 [==============================] - 12s 1s/step - loss: 0.1581 - accuracy: 0.9773 - val_loss: 1.5250 - val_accuracy: 0.9565
    Epoch 88/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2812 - accuracy: 0.9815 - val_loss: 1.4701 - val_accuracy: 0.9503
    Epoch 89/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3129 - accuracy: 0.9773 - val_loss: 1.1698 - val_accuracy: 0.9565
    Epoch 90/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2614 - accuracy: 0.9773 - val_loss: 1.2176 - val_accuracy: 0.9317
    Epoch 91/100
    8/8 [==============================] - 9s 1s/step - loss: 0.2793 - accuracy: 0.9743 - val_loss: 2.4274 - val_accuracy: 0.9379
    Epoch 92/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2426 - accuracy: 0.9722 - val_loss: 1.3656 - val_accuracy: 0.9565
    Epoch 93/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2945 - accuracy: 0.9763 - val_loss: 1.5352 - val_accuracy: 0.9441
    Epoch 94/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4552 - accuracy: 0.9773 - val_loss: 1.4727 - val_accuracy: 0.9503
    Epoch 95/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4092 - accuracy: 0.9743 - val_loss: 2.3481 - val_accuracy: 0.9503
    Epoch 96/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3054 - accuracy: 0.9773 - val_loss: 3.4624 - val_accuracy: 0.9193
    Epoch 97/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2507 - accuracy: 0.9804 - val_loss: 2.3161 - val_accuracy: 0.9441
    Epoch 98/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2618 - accuracy: 0.9804 - val_loss: 2.1093 - val_accuracy: 0.9379
    Epoch 99/100
    8/8 [==============================] - 12s 2s/step - loss: 0.1906 - accuracy: 0.9866 - val_loss: 2.2766 - val_accuracy: 0.9317
    Epoch 100/100
    8/8 [==============================] - 9s 1s/step - loss: 0.2506 - accuracy: 0.9753 - val_loss: 3.3091 - val_accuracy: 0.9317

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/13.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 14s 4s/step - loss: 2.3688 - accuracy: 0.9365

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.368755578994751, 0.936475396156311]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9364754098360656
                  precision    recall  f1-score   support

               0       0.91      0.87      0.89       148
               1       0.95      0.96      0.95       340

        accuracy                           0.94       488
       macro avg       0.93      0.92      0.92       488
    weighted avg       0.94      0.94      0.94       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/14.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/15.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/16.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.91      0.87      0.89       148
               1       0.95      0.96      0.95       340

        accuracy                           0.94       488
       macro avg       0.93      0.92      0.92       488
    weighted avg       0.94      0.94      0.94       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.91      0.87      0.96      0.89      0.92      0.83       148
              1       0.95      0.96      0.87      0.95      0.92      0.85       340

    avg / total       0.94      0.94      0.90      0.94      0.92      0.84       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
