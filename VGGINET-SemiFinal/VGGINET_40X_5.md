<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = os.environ['NUM_RUN']
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    ('40X', '5')

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'VGGINet/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.4468 - accuracy: 0.4766WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 18s 2s/step - loss: 5.3622 - accuracy: 0.6973 - val_loss: 34.0673 - val_accuracy: 0.5810
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 2.3076 - accuracy: 0.8394 - val_loss: 11.3255 - val_accuracy: 0.8101
    Epoch 3/100
    9/9 [==============================] - 11s 1s/step - loss: 1.3821 - accuracy: 0.8812 - val_loss: 3.6645 - val_accuracy: 0.8603
    Epoch 4/100
    9/9 [==============================] - 11s 1s/step - loss: 1.1043 - accuracy: 0.8942 - val_loss: 4.7166 - val_accuracy: 0.8547
    Epoch 5/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9312 - accuracy: 0.9034 - val_loss: 1.2174 - val_accuracy: 0.9385
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9654 - accuracy: 0.9081 - val_loss: 2.3543 - val_accuracy: 0.9274
    Epoch 7/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8769 - accuracy: 0.9109 - val_loss: 1.6494 - val_accuracy: 0.9274
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 1.3919 - accuracy: 0.9025 - val_loss: 2.6079 - val_accuracy: 0.8994
    Epoch 9/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0405 - accuracy: 0.9201 - val_loss: 2.5941 - val_accuracy: 0.9162
    Epoch 10/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7866 - accuracy: 0.9220 - val_loss: 1.5495 - val_accuracy: 0.9385
    Epoch 11/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6816 - accuracy: 0.9313 - val_loss: 2.0697 - val_accuracy: 0.9330
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6503 - accuracy: 0.9415 - val_loss: 1.4357 - val_accuracy: 0.9441
    Epoch 13/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5059 - accuracy: 0.9508 - val_loss: 1.4691 - val_accuracy: 0.9330
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5057 - accuracy: 0.9536 - val_loss: 1.7863 - val_accuracy: 0.9274
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3428 - accuracy: 0.9573 - val_loss: 1.3486 - val_accuracy: 0.9330
    Epoch 16/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4954 - accuracy: 0.9582 - val_loss: 2.0596 - val_accuracy: 0.9162
    Epoch 17/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4426 - accuracy: 0.9545 - val_loss: 1.1116 - val_accuracy: 0.9497
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4352 - accuracy: 0.9564 - val_loss: 1.8578 - val_accuracy: 0.9385
    Epoch 19/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4591 - accuracy: 0.9582 - val_loss: 1.2864 - val_accuracy: 0.9497
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4620 - accuracy: 0.9619 - val_loss: 1.3060 - val_accuracy: 0.9441
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3622 - accuracy: 0.9656 - val_loss: 1.4632 - val_accuracy: 0.9497
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5708 - accuracy: 0.9601 - val_loss: 1.0392 - val_accuracy: 0.9609
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5227 - accuracy: 0.9564 - val_loss: 0.8929 - val_accuracy: 0.9553
    Epoch 24/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3171 - accuracy: 0.9712 - val_loss: 1.4139 - val_accuracy: 0.9274
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4769 - accuracy: 0.9610 - val_loss: 1.4180 - val_accuracy: 0.9274
    Epoch 26/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3792 - accuracy: 0.9712 - val_loss: 1.0918 - val_accuracy: 0.9274
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5543 - accuracy: 0.9610 - val_loss: 1.4542 - val_accuracy: 0.9218
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3548 - accuracy: 0.9647 - val_loss: 0.9607 - val_accuracy: 0.9330
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3885 - accuracy: 0.9731 - val_loss: 1.3221 - val_accuracy: 0.9385
    Epoch 30/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4512 - accuracy: 0.9694 - val_loss: 1.1167 - val_accuracy: 0.9497
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3279 - accuracy: 0.9740 - val_loss: 1.3193 - val_accuracy: 0.9553
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3751 - accuracy: 0.9684 - val_loss: 1.6349 - val_accuracy: 0.9497
    Epoch 33/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2684 - accuracy: 0.9721 - val_loss: 0.9291 - val_accuracy: 0.9441
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4424 - accuracy: 0.9675 - val_loss: 1.0740 - val_accuracy: 0.9609
    Epoch 35/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2989 - accuracy: 0.9759 - val_loss: 1.0945 - val_accuracy: 0.9553
    Epoch 36/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3112 - accuracy: 0.9703 - val_loss: 1.6310 - val_accuracy: 0.9274
    Epoch 37/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2808 - accuracy: 0.9731 - val_loss: 2.4903 - val_accuracy: 0.9162
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3880 - accuracy: 0.9759 - val_loss: 3.0513 - val_accuracy: 0.9218
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3998 - accuracy: 0.9694 - val_loss: 1.1289 - val_accuracy: 0.9609
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1700 - accuracy: 0.9842 - val_loss: 1.3294 - val_accuracy: 0.9553
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3146 - accuracy: 0.9703 - val_loss: 1.2536 - val_accuracy: 0.9497
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1917 - accuracy: 0.9851 - val_loss: 1.5289 - val_accuracy: 0.9385
    Epoch 43/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2915 - accuracy: 0.9759 - val_loss: 1.3584 - val_accuracy: 0.9497
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2084 - accuracy: 0.9796 - val_loss: 1.2951 - val_accuracy: 0.9553
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4567 - accuracy: 0.9712 - val_loss: 1.0116 - val_accuracy: 0.9441
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4634 - accuracy: 0.9656 - val_loss: 1.6382 - val_accuracy: 0.9385
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3790 - accuracy: 0.9740 - val_loss: 1.5333 - val_accuracy: 0.9330
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3246 - accuracy: 0.9694 - val_loss: 1.8302 - val_accuracy: 0.9330
    Epoch 49/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5367 - accuracy: 0.9666 - val_loss: 2.6495 - val_accuracy: 0.9330
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4550 - accuracy: 0.9703 - val_loss: 1.0735 - val_accuracy: 0.9665
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3642 - accuracy: 0.9694 - val_loss: 1.5144 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4039 - accuracy: 0.9786 - val_loss: 1.7139 - val_accuracy: 0.9553
    Epoch 53/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2626 - accuracy: 0.9814 - val_loss: 1.3799 - val_accuracy: 0.9553
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1292 - accuracy: 0.9879 - val_loss: 1.1811 - val_accuracy: 0.9497
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3642 - accuracy: 0.9777 - val_loss: 1.3979 - val_accuracy: 0.9609
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1299 - accuracy: 0.9851 - val_loss: 2.0492 - val_accuracy: 0.9441
    Epoch 57/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3532 - accuracy: 0.9777 - val_loss: 1.4742 - val_accuracy: 0.9441
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3614 - accuracy: 0.9749 - val_loss: 1.3119 - val_accuracy: 0.9553
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5237 - accuracy: 0.9721 - val_loss: 1.6984 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2059 - accuracy: 0.9879 - val_loss: 1.6233 - val_accuracy: 0.9385
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1629 - accuracy: 0.9851 - val_loss: 1.7652 - val_accuracy: 0.9609
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2680 - accuracy: 0.9805 - val_loss: 0.8906 - val_accuracy: 0.9609
    Epoch 63/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1626 - accuracy: 0.9889 - val_loss: 0.7636 - val_accuracy: 0.9553
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2812 - accuracy: 0.9842 - val_loss: 1.0622 - val_accuracy: 0.9553
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2252 - accuracy: 0.9861 - val_loss: 1.3047 - val_accuracy: 0.9497
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1502 - accuracy: 0.9889 - val_loss: 2.6328 - val_accuracy: 0.9385
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2120 - accuracy: 0.9842 - val_loss: 1.8279 - val_accuracy: 0.9553
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2337 - accuracy: 0.9824 - val_loss: 1.5638 - val_accuracy: 0.9721
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1621 - accuracy: 0.9870 - val_loss: 1.7255 - val_accuracy: 0.9609
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1125 - accuracy: 0.9898 - val_loss: 2.2497 - val_accuracy: 0.9330
    Epoch 71/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2352 - accuracy: 0.9833 - val_loss: 3.2455 - val_accuracy: 0.9274
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3154 - accuracy: 0.9824 - val_loss: 1.6636 - val_accuracy: 0.9553
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1412 - accuracy: 0.9851 - val_loss: 1.1251 - val_accuracy: 0.9721
    Epoch 74/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1977 - accuracy: 0.9851 - val_loss: 1.4283 - val_accuracy: 0.9721
    Epoch 75/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3031 - accuracy: 0.9824 - val_loss: 1.8054 - val_accuracy: 0.9609
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0962 - accuracy: 0.9916 - val_loss: 1.4995 - val_accuracy: 0.9665
    Epoch 77/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2284 - accuracy: 0.9842 - val_loss: 2.6817 - val_accuracy: 0.9385
    Epoch 78/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3087 - accuracy: 0.9861 - val_loss: 3.5247 - val_accuracy: 0.9274
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1067 - accuracy: 0.9907 - val_loss: 2.3265 - val_accuracy: 0.9441
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1753 - accuracy: 0.9842 - val_loss: 1.2092 - val_accuracy: 0.9665
    Epoch 81/100
    9/9 [==============================] - 15s 2s/step - loss: 0.0918 - accuracy: 0.9935 - val_loss: 0.9940 - val_accuracy: 0.9665
    Epoch 82/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2018 - accuracy: 0.9814 - val_loss: 1.5877 - val_accuracy: 0.9665
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2682 - accuracy: 0.9842 - val_loss: 1.9861 - val_accuracy: 0.9497
    Epoch 84/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3008 - accuracy: 0.9805 - val_loss: 1.7973 - val_accuracy: 0.9665
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3551 - accuracy: 0.9731 - val_loss: 1.2877 - val_accuracy: 0.9609
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4074 - accuracy: 0.9814 - val_loss: 1.3274 - val_accuracy: 0.9665
    Epoch 87/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3093 - accuracy: 0.9824 - val_loss: 1.6799 - val_accuracy: 0.9385
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2831 - accuracy: 0.9833 - val_loss: 1.9231 - val_accuracy: 0.9441
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4170 - accuracy: 0.9805 - val_loss: 1.3087 - val_accuracy: 0.9665
    Epoch 90/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2287 - accuracy: 0.9889 - val_loss: 3.1630 - val_accuracy: 0.9106
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3414 - accuracy: 0.9786 - val_loss: 2.1056 - val_accuracy: 0.9218
    Epoch 92/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3051 - accuracy: 0.9833 - val_loss: 1.9620 - val_accuracy: 0.9106
    Epoch 93/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6147 - accuracy: 0.9647 - val_loss: 2.8883 - val_accuracy: 0.9553
    Epoch 94/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2583 - accuracy: 0.9870 - val_loss: 4.8024 - val_accuracy: 0.9330
    Epoch 95/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1353 - accuracy: 0.9907 - val_loss: 4.8565 - val_accuracy: 0.9274
    Epoch 96/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1613 - accuracy: 0.9898 - val_loss: 4.2655 - val_accuracy: 0.9274
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2131 - accuracy: 0.9861 - val_loss: 3.1293 - val_accuracy: 0.9441
    Epoch 98/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2329 - accuracy: 0.9851 - val_loss: 4.4029 - val_accuracy: 0.9162
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1772 - accuracy: 0.9879 - val_loss: 3.7470 - val_accuracy: 0.9218
    Epoch 100/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2415 - accuracy: 0.9889 - val_loss: 2.2341 - val_accuracy: 0.9553

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/37.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 7s 1s/step - loss: 1.8419 - accuracy: 0.9610

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.8419010639190674, 0.9610389471054077]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.961038961038961
                  precision    recall  f1-score   support

               0       0.95      0.92      0.93       158
               1       0.97      0.98      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.95      0.95       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/38.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/39.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/40.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.92      0.93       158
               1       0.97      0.98      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.95      0.95       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.92      0.98      0.93      0.95      0.89       158
              1       0.97      0.98      0.92      0.97      0.95      0.90       381

    avg / total       0.96      0.96      0.94      0.96      0.95      0.90       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
