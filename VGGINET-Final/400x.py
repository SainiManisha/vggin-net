import papermill as pm

magnification_factor = '400X'
trainable_blocks = ['block4']
runs = range(1, 5 + 1)

for irun in runs:
    pm.execute_notebook('VGGINET_FineTuning.ipynb',
                        'VGGINET_FT_%s_%d.ipynb' % (magnification_factor, irun),
                         parameters={
                             'magnification_factor': magnification_factor,
                             'trainable_blocks': trainable_blocks,
                             'irun': irun
                         })