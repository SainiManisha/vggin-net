<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "100X"
trainable_blocks = ["block1", "block2", "block3", "block4"]
irun = 2
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_100X-BREAKHIS-Dataset-60-10-30-VGGINet/2'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/100X/2'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.2968 - accuracy: 0.5156WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 35s 4s/step - loss: 4.9213 - accuracy: 0.7181 - val_loss: 5.7277 - val_accuracy: 0.8663
    Epoch 2/100
    9/9 [==============================] - 16s 2s/step - loss: 3.2093 - accuracy: 0.8493 - val_loss: 1.7941 - val_accuracy: 0.9144
    Epoch 3/100
    9/9 [==============================] - 17s 2s/step - loss: 2.4581 - accuracy: 0.8670 - val_loss: 3.3396 - val_accuracy: 0.9037
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 1.7182 - accuracy: 0.8910 - val_loss: 2.2417 - val_accuracy: 0.9198
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 1.4993 - accuracy: 0.9034 - val_loss: 1.5241 - val_accuracy: 0.9305
    Epoch 6/100
    9/9 [==============================] - 17s 2s/step - loss: 1.2601 - accuracy: 0.9060 - val_loss: 0.6181 - val_accuracy: 0.9465
    Epoch 7/100
    9/9 [==============================] - 17s 2s/step - loss: 1.0033 - accuracy: 0.9087 - val_loss: 0.7600 - val_accuracy: 0.9519
    Epoch 8/100
    9/9 [==============================] - 18s 2s/step - loss: 0.8490 - accuracy: 0.9344 - val_loss: 0.8647 - val_accuracy: 0.9465
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9015 - accuracy: 0.9122 - val_loss: 0.7773 - val_accuracy: 0.9572
    Epoch 10/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0295 - accuracy: 0.9140 - val_loss: 0.6114 - val_accuracy: 0.9519
    Epoch 11/100
    9/9 [==============================] - 17s 2s/step - loss: 0.9265 - accuracy: 0.9184 - val_loss: 0.7125 - val_accuracy: 0.9572
    Epoch 12/100
    9/9 [==============================] - 17s 2s/step - loss: 0.9616 - accuracy: 0.9131 - val_loss: 1.0074 - val_accuracy: 0.9412
    Epoch 13/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6620 - accuracy: 0.9300 - val_loss: 0.6465 - val_accuracy: 0.9572
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0931 - accuracy: 0.9069 - val_loss: 0.7914 - val_accuracy: 0.9465
    Epoch 15/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8021 - accuracy: 0.9300 - val_loss: 1.3160 - val_accuracy: 0.9305
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7120 - accuracy: 0.9353 - val_loss: 1.3850 - val_accuracy: 0.9465
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8875 - accuracy: 0.9309 - val_loss: 1.0888 - val_accuracy: 0.9251
    Epoch 18/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7536 - accuracy: 0.9309 - val_loss: 0.5531 - val_accuracy: 0.9626
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8127 - accuracy: 0.9468 - val_loss: 0.9263 - val_accuracy: 0.9412
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6802 - accuracy: 0.9353 - val_loss: 0.7677 - val_accuracy: 0.9519
    Epoch 21/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6161 - accuracy: 0.9477 - val_loss: 0.6021 - val_accuracy: 0.9626
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6780 - accuracy: 0.9424 - val_loss: 1.4651 - val_accuracy: 0.9412
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6700 - accuracy: 0.9504 - val_loss: 0.2649 - val_accuracy: 0.9465
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4294 - accuracy: 0.9628 - val_loss: 0.3928 - val_accuracy: 0.9679
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5733 - accuracy: 0.9530 - val_loss: 0.8283 - val_accuracy: 0.9465
    Epoch 26/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5456 - accuracy: 0.9477 - val_loss: 0.4902 - val_accuracy: 0.9626
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5351 - accuracy: 0.9530 - val_loss: 0.8766 - val_accuracy: 0.9519
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6425 - accuracy: 0.9539 - val_loss: 0.6924 - val_accuracy: 0.9519
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5145 - accuracy: 0.9504 - val_loss: 1.1793 - val_accuracy: 0.9465
    Epoch 30/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4421 - accuracy: 0.9566 - val_loss: 1.0263 - val_accuracy: 0.9465
    Epoch 31/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3902 - accuracy: 0.9690 - val_loss: 0.9955 - val_accuracy: 0.9465
    Epoch 32/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4918 - accuracy: 0.9645 - val_loss: 1.3581 - val_accuracy: 0.9305
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5104 - accuracy: 0.9637 - val_loss: 0.9104 - val_accuracy: 0.9519
    Epoch 34/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4907 - accuracy: 0.9583 - val_loss: 0.8629 - val_accuracy: 0.9572
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4701 - accuracy: 0.9601 - val_loss: 1.7089 - val_accuracy: 0.9412
    Epoch 36/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5555 - accuracy: 0.9583 - val_loss: 1.2626 - val_accuracy: 0.9465
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4745 - accuracy: 0.9628 - val_loss: 0.9618 - val_accuracy: 0.9412
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3160 - accuracy: 0.9707 - val_loss: 0.5694 - val_accuracy: 0.9679
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6329 - accuracy: 0.9592 - val_loss: 0.7771 - val_accuracy: 0.9626
    Epoch 40/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4153 - accuracy: 0.9637 - val_loss: 1.1486 - val_accuracy: 0.9305
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5255 - accuracy: 0.9672 - val_loss: 0.2788 - val_accuracy: 0.9679
    Epoch 42/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3563 - accuracy: 0.9699 - val_loss: 0.8004 - val_accuracy: 0.9519
    Epoch 43/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5207 - accuracy: 0.9574 - val_loss: 0.6518 - val_accuracy: 0.9465
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3999 - accuracy: 0.9628 - val_loss: 0.7945 - val_accuracy: 0.9358
    Epoch 45/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3780 - accuracy: 0.9707 - val_loss: 0.9172 - val_accuracy: 0.9305
    Epoch 46/100
    9/9 [==============================] - 19s 2s/step - loss: 0.5872 - accuracy: 0.9583 - val_loss: 0.7663 - val_accuracy: 0.9519
    Epoch 47/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3588 - accuracy: 0.9725 - val_loss: 0.5949 - val_accuracy: 0.9572
    Epoch 48/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1726 - accuracy: 0.9823 - val_loss: 0.8861 - val_accuracy: 0.9519
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3428 - accuracy: 0.9725 - val_loss: 0.7927 - val_accuracy: 0.9305
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2638 - accuracy: 0.9787 - val_loss: 0.3905 - val_accuracy: 0.9626
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1983 - accuracy: 0.9823 - val_loss: 0.1565 - val_accuracy: 0.9840
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1967 - accuracy: 0.9778 - val_loss: 0.1016 - val_accuracy: 0.9893
    Epoch 53/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1916 - accuracy: 0.9796 - val_loss: 0.2949 - val_accuracy: 0.9679
    Epoch 54/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2347 - accuracy: 0.9796 - val_loss: 0.4255 - val_accuracy: 0.9733
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2711 - accuracy: 0.9734 - val_loss: 0.6395 - val_accuracy: 0.9679
    Epoch 56/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3470 - accuracy: 0.9743 - val_loss: 0.9794 - val_accuracy: 0.9572
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3456 - accuracy: 0.9707 - val_loss: 1.4724 - val_accuracy: 0.9519
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4542 - accuracy: 0.9699 - val_loss: 0.7320 - val_accuracy: 0.9626
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2583 - accuracy: 0.9805 - val_loss: 1.0197 - val_accuracy: 0.9679
    Epoch 60/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2241 - accuracy: 0.9752 - val_loss: 0.8947 - val_accuracy: 0.9626
    Epoch 61/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2740 - accuracy: 0.9743 - val_loss: 0.8830 - val_accuracy: 0.9465
    Epoch 62/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5420 - accuracy: 0.9654 - val_loss: 0.6105 - val_accuracy: 0.9733
    Epoch 63/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4782 - accuracy: 0.9743 - val_loss: 1.6968 - val_accuracy: 0.9251
    Epoch 64/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3644 - accuracy: 0.9734 - val_loss: 0.9576 - val_accuracy: 0.9626
    Epoch 65/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4086 - accuracy: 0.9778 - val_loss: 0.9053 - val_accuracy: 0.9465
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3967 - accuracy: 0.9725 - val_loss: 0.8300 - val_accuracy: 0.9626
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2841 - accuracy: 0.9805 - val_loss: 0.3689 - val_accuracy: 0.9679
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4591 - accuracy: 0.9690 - val_loss: 0.5922 - val_accuracy: 0.9626
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4712 - accuracy: 0.9645 - val_loss: 0.9887 - val_accuracy: 0.9572
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3301 - accuracy: 0.9761 - val_loss: 0.4445 - val_accuracy: 0.9572
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2713 - accuracy: 0.9805 - val_loss: 0.8329 - val_accuracy: 0.9465
    Epoch 72/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2679 - accuracy: 0.9849 - val_loss: 0.8784 - val_accuracy: 0.9626
    Epoch 73/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3696 - accuracy: 0.9707 - val_loss: 0.5240 - val_accuracy: 0.9786
    Epoch 74/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1055 - accuracy: 0.9876 - val_loss: 0.9183 - val_accuracy: 0.9626
    Epoch 75/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3836 - accuracy: 0.9752 - val_loss: 0.3440 - val_accuracy: 0.9786
    Epoch 76/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1435 - accuracy: 0.9858 - val_loss: 0.4272 - val_accuracy: 0.9679
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3431 - accuracy: 0.9840 - val_loss: 0.6258 - val_accuracy: 0.9519
    Epoch 78/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2252 - accuracy: 0.9805 - val_loss: 0.3152 - val_accuracy: 0.9733
    Epoch 79/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3745 - accuracy: 0.9752 - val_loss: 0.1026 - val_accuracy: 0.9840
    Epoch 80/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4815 - accuracy: 0.9770 - val_loss: 0.6738 - val_accuracy: 0.9679
    Epoch 81/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2722 - accuracy: 0.9840 - val_loss: 1.2913 - val_accuracy: 0.9572
    Epoch 82/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2327 - accuracy: 0.9796 - val_loss: 1.1306 - val_accuracy: 0.9519
    Epoch 83/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3364 - accuracy: 0.9778 - val_loss: 0.6496 - val_accuracy: 0.9572
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3179 - accuracy: 0.9832 - val_loss: 0.5885 - val_accuracy: 0.9626
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2042 - accuracy: 0.9832 - val_loss: 0.7445 - val_accuracy: 0.9572
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1700 - accuracy: 0.9823 - val_loss: 0.2223 - val_accuracy: 0.9786
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2546 - accuracy: 0.9814 - val_loss: 0.3945 - val_accuracy: 0.9572
    Epoch 88/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2643 - accuracy: 0.9796 - val_loss: 0.7801 - val_accuracy: 0.9572
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1538 - accuracy: 0.9858 - val_loss: 0.4204 - val_accuracy: 0.9786
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3171 - accuracy: 0.9770 - val_loss: 0.4978 - val_accuracy: 0.9572
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2113 - accuracy: 0.9796 - val_loss: 0.7548 - val_accuracy: 0.9679
    Epoch 92/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3159 - accuracy: 0.9796 - val_loss: 0.3903 - val_accuracy: 0.9840
    Epoch 93/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2822 - accuracy: 0.9805 - val_loss: 0.9377 - val_accuracy: 0.9626
    Epoch 94/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2884 - accuracy: 0.9832 - val_loss: 0.6292 - val_accuracy: 0.9733
    Epoch 95/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2151 - accuracy: 0.9787 - val_loss: 0.7147 - val_accuracy: 0.9733
    Epoch 96/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2495 - accuracy: 0.9805 - val_loss: 1.2817 - val_accuracy: 0.9626
    Epoch 97/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3768 - accuracy: 0.9778 - val_loss: 0.4357 - val_accuracy: 0.9679
    Epoch 98/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3874 - accuracy: 0.9796 - val_loss: 0.6269 - val_accuracy: 0.9626
    Epoch 99/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1220 - accuracy: 0.9876 - val_loss: 0.5524 - val_accuracy: 0.9572
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2259 - accuracy: 0.9832 - val_loss: 0.4707 - val_accuracy: 0.9733

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/61.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 28s 6s/step - loss: 1.1281 - accuracy: 0.9611

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.1280916929244995, 0.9611307382583618]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/62.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    18/18 [==============================] - 28s 2s/step - loss: 0.4530 - accuracy: 0.9752 - val_loss: 0.4005 - val_accuracy: 0.9733

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    18/18 [==============================] - 25s 1s/step - loss: 0.5104 - accuracy: 0.9716 - val_loss: 0.3400 - val_accuracy: 0.9733

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3645 - accuracy: 0.9814 - val_loss: 0.3337 - val_accuracy: 0.9733

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3879 - accuracy: 0.9805 - val_loss: 0.3225 - val_accuracy: 0.9733

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2077 - accuracy: 0.9823 - val_loss: 0.3067 - val_accuracy: 0.9733

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2635 - accuracy: 0.9849 - val_loss: 0.3013 - val_accuracy: 0.9733

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    18/18 [==============================] - 22s 1s/step - loss: 0.3389 - accuracy: 0.9832 - val_loss: 0.2855 - val_accuracy: 0.9733

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1984 - accuracy: 0.9832 - val_loss: 0.2845 - val_accuracy: 0.9733

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    18/18 [==============================] - 24s 1s/step - loss: 0.3684 - accuracy: 0.9805 - val_loss: 0.2964 - val_accuracy: 0.9786

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2369 - accuracy: 0.9840 - val_loss: 0.3118 - val_accuracy: 0.9786

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    18/18 [==============================] - 24s 1s/step - loss: 0.3192 - accuracy: 0.9867 - val_loss: 0.3093 - val_accuracy: 0.9786

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2199 - accuracy: 0.9858 - val_loss: 0.3147 - val_accuracy: 0.9733

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    18/18 [==============================] - 23s 1s/step - loss: 0.2445 - accuracy: 0.9867 - val_loss: 0.3187 - val_accuracy: 0.9786

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2617 - accuracy: 0.9805 - val_loss: 0.3257 - val_accuracy: 0.9786

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3761 - accuracy: 0.9787 - val_loss: 0.3214 - val_accuracy: 0.9786

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2868 - accuracy: 0.9823 - val_loss: 0.3141 - val_accuracy: 0.9786

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1264 - accuracy: 0.9920 - val_loss: 0.3238 - val_accuracy: 0.9786

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    18/18 [==============================] - 22s 1s/step - loss: 0.2427 - accuracy: 0.9805 - val_loss: 0.3321 - val_accuracy: 0.9786

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    18/18 [==============================] - 25s 1s/step - loss: 0.0914 - accuracy: 0.9894 - val_loss: 0.3300 - val_accuracy: 0.9786

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1741 - accuracy: 0.9911 - val_loss: 0.3348 - val_accuracy: 0.9786

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2467 - accuracy: 0.9849 - val_loss: 0.3429 - val_accuracy: 0.9786

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1616 - accuracy: 0.9849 - val_loss: 0.3505 - val_accuracy: 0.9786

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2018 - accuracy: 0.9867 - val_loss: 0.3537 - val_accuracy: 0.9786

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1931 - accuracy: 0.9885 - val_loss: 0.3469 - val_accuracy: 0.9786

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    18/18 [==============================] - 23s 1s/step - loss: 0.3488 - accuracy: 0.9787 - val_loss: 0.3477 - val_accuracy: 0.9786

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2102 - accuracy: 0.9858 - val_loss: 0.3423 - val_accuracy: 0.9786

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2582 - accuracy: 0.9876 - val_loss: 0.3440 - val_accuracy: 0.9786

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2207 - accuracy: 0.9832 - val_loss: 0.3331 - val_accuracy: 0.9840

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1218 - accuracy: 0.9902 - val_loss: 0.3266 - val_accuracy: 0.9786

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2070 - accuracy: 0.9849 - val_loss: 0.3328 - val_accuracy: 0.9786

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2734 - accuracy: 0.9867 - val_loss: 0.3248 - val_accuracy: 0.9786

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2648 - accuracy: 0.9858 - val_loss: 0.3486 - val_accuracy: 0.9786

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1439 - accuracy: 0.9902 - val_loss: 0.3522 - val_accuracy: 0.9840

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    18/18 [==============================] - 24s 1s/step - loss: 0.0765 - accuracy: 0.9911 - val_loss: 0.3620 - val_accuracy: 0.9786

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1595 - accuracy: 0.9876 - val_loss: 0.3654 - val_accuracy: 0.9786

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1481 - accuracy: 0.9867 - val_loss: 0.3592 - val_accuracy: 0.9786

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1963 - accuracy: 0.9832 - val_loss: 0.3662 - val_accuracy: 0.9786

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    18/18 [==============================] - 23s 1s/step - loss: 0.3023 - accuracy: 0.9840 - val_loss: 0.3757 - val_accuracy: 0.9786

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3178 - accuracy: 0.9796 - val_loss: 0.3810 - val_accuracy: 0.9786

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1649 - accuracy: 0.9849 - val_loss: 0.3837 - val_accuracy: 0.9786

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    18/18 [==============================] - 23s 1s/step - loss: 0.2650 - accuracy: 0.9867 - val_loss: 0.3941 - val_accuracy: 0.9786

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2122 - accuracy: 0.9867 - val_loss: 0.3959 - val_accuracy: 0.9786

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1884 - accuracy: 0.9902 - val_loss: 0.4082 - val_accuracy: 0.9786

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1193 - accuracy: 0.9929 - val_loss: 0.4055 - val_accuracy: 0.9786

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1381 - accuracy: 0.9876 - val_loss: 0.4146 - val_accuracy: 0.9786

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1377 - accuracy: 0.9885 - val_loss: 0.3994 - val_accuracy: 0.9786

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1732 - accuracy: 0.9885 - val_loss: 0.3887 - val_accuracy: 0.9786

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2493 - accuracy: 0.9823 - val_loss: 0.3855 - val_accuracy: 0.9786

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1259 - accuracy: 0.9867 - val_loss: 0.3838 - val_accuracy: 0.9786

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2845 - accuracy: 0.9849 - val_loss: 0.3926 - val_accuracy: 0.9786

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/63.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.8238 - accuracy: 0.9700

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.8237907290458679, 0.9699646830558777]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9699646643109541
                  precision    recall  f1-score   support

               0       0.94      0.96      0.95       164
               1       0.98      0.98      0.98       402

        accuracy                           0.97       566
       macro avg       0.96      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/64.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/65.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/66.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.94      0.96      0.95       164
               1       0.98      0.98      0.98       402

        accuracy                           0.97       566
       macro avg       0.96      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.94      0.96      0.98      0.95      0.97      0.93       164
              1       0.98      0.98      0.96      0.98      0.97      0.94       402

    avg / total       0.97      0.97      0.96      0.97      0.97      0.93       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
