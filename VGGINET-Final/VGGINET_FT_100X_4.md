<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "100X"
trainable_blocks = ["block1", "block2", "block3", "block4"]
irun = 4
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_100X-BREAKHIS-Dataset-60-10-30-VGGINet/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/100X/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0906 - accuracy: 0.5703WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 29s 3s/step - loss: 2.8246 - accuracy: 0.7739 - val_loss: 4.6414 - val_accuracy: 0.8770
    Epoch 2/100
    9/9 [==============================] - 16s 2s/step - loss: 1.9280 - accuracy: 0.8582 - val_loss: 3.6482 - val_accuracy: 0.8930
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 1.3271 - accuracy: 0.8856 - val_loss: 1.1196 - val_accuracy: 0.9305
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1981 - accuracy: 0.8927 - val_loss: 1.0754 - val_accuracy: 0.9412
    Epoch 5/100
    9/9 [==============================] - 17s 2s/step - loss: 1.2740 - accuracy: 0.8785 - val_loss: 1.0390 - val_accuracy: 0.9358
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7384 - accuracy: 0.9176 - val_loss: 1.9459 - val_accuracy: 0.8824
    Epoch 7/100
    9/9 [==============================] - 16s 2s/step - loss: 1.3030 - accuracy: 0.8918 - val_loss: 1.1842 - val_accuracy: 0.9305
    Epoch 8/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0679 - accuracy: 0.9105 - val_loss: 1.2870 - val_accuracy: 0.9305
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1102 - accuracy: 0.9007 - val_loss: 1.2948 - val_accuracy: 0.9251
    Epoch 10/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7636 - accuracy: 0.9273 - val_loss: 0.8666 - val_accuracy: 0.9305
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7143 - accuracy: 0.9255 - val_loss: 1.2738 - val_accuracy: 0.9251
    Epoch 12/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7743 - accuracy: 0.9167 - val_loss: 0.4656 - val_accuracy: 0.9519
    Epoch 13/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0302 - accuracy: 0.9122 - val_loss: 0.7615 - val_accuracy: 0.9412
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0003 - accuracy: 0.9238 - val_loss: 0.6002 - val_accuracy: 0.9465
    Epoch 15/100
    9/9 [==============================] - 17s 2s/step - loss: 1.0548 - accuracy: 0.9158 - val_loss: 1.0862 - val_accuracy: 0.9519
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 1.3020 - accuracy: 0.9051 - val_loss: 0.4011 - val_accuracy: 0.9626
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7043 - accuracy: 0.9317 - val_loss: 1.3167 - val_accuracy: 0.9198
    Epoch 18/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6676 - accuracy: 0.9433 - val_loss: 0.4923 - val_accuracy: 0.9465
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8569 - accuracy: 0.9317 - val_loss: 1.0341 - val_accuracy: 0.9037
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7578 - accuracy: 0.9238 - val_loss: 1.4927 - val_accuracy: 0.9144
    Epoch 21/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8869 - accuracy: 0.9415 - val_loss: 0.6651 - val_accuracy: 0.9519
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8206 - accuracy: 0.9353 - val_loss: 0.6479 - val_accuracy: 0.9572
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5623 - accuracy: 0.9574 - val_loss: 0.7689 - val_accuracy: 0.9465
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7468 - accuracy: 0.9362 - val_loss: 1.1083 - val_accuracy: 0.9465
    Epoch 25/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6918 - accuracy: 0.9379 - val_loss: 0.6550 - val_accuracy: 0.9465
    Epoch 26/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6771 - accuracy: 0.9433 - val_loss: 0.9895 - val_accuracy: 0.9465
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6440 - accuracy: 0.9397 - val_loss: 0.8488 - val_accuracy: 0.9519
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5165 - accuracy: 0.9574 - val_loss: 1.0136 - val_accuracy: 0.9465
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8156 - accuracy: 0.9424 - val_loss: 1.1552 - val_accuracy: 0.9572
    Epoch 30/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7378 - accuracy: 0.9397 - val_loss: 1.3906 - val_accuracy: 0.9358
    Epoch 31/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5744 - accuracy: 0.9557 - val_loss: 1.3363 - val_accuracy: 0.9412
    Epoch 32/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7231 - accuracy: 0.9504 - val_loss: 0.7963 - val_accuracy: 0.9465
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4791 - accuracy: 0.9574 - val_loss: 0.6963 - val_accuracy: 0.9519
    Epoch 34/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6909 - accuracy: 0.9424 - val_loss: 0.3942 - val_accuracy: 0.9626
    Epoch 35/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7058 - accuracy: 0.9424 - val_loss: 1.1596 - val_accuracy: 0.9412
    Epoch 36/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4264 - accuracy: 0.9610 - val_loss: 0.6826 - val_accuracy: 0.9733
    Epoch 37/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4989 - accuracy: 0.9557 - val_loss: 0.3566 - val_accuracy: 0.9733
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5815 - accuracy: 0.9566 - val_loss: 0.5511 - val_accuracy: 0.9626
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5137 - accuracy: 0.9592 - val_loss: 0.8616 - val_accuracy: 0.9519
    Epoch 40/100
    9/9 [==============================] - 19s 2s/step - loss: 0.4028 - accuracy: 0.9619 - val_loss: 1.1288 - val_accuracy: 0.9465
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5110 - accuracy: 0.9557 - val_loss: 0.6439 - val_accuracy: 0.9679
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5153 - accuracy: 0.9628 - val_loss: 0.7504 - val_accuracy: 0.9626
    Epoch 43/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3927 - accuracy: 0.9637 - val_loss: 0.6926 - val_accuracy: 0.9679
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5889 - accuracy: 0.9557 - val_loss: 0.6773 - val_accuracy: 0.9733
    Epoch 45/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4690 - accuracy: 0.9637 - val_loss: 0.7709 - val_accuracy: 0.9412
    Epoch 46/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4782 - accuracy: 0.9725 - val_loss: 0.5744 - val_accuracy: 0.9626
    Epoch 47/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2997 - accuracy: 0.9725 - val_loss: 0.2759 - val_accuracy: 0.9786
    Epoch 48/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5624 - accuracy: 0.9601 - val_loss: 0.5614 - val_accuracy: 0.9519
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5117 - accuracy: 0.9628 - val_loss: 0.3167 - val_accuracy: 0.9733
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5105 - accuracy: 0.9601 - val_loss: 0.6669 - val_accuracy: 0.9679
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6659 - accuracy: 0.9557 - val_loss: 0.4914 - val_accuracy: 0.9733
    Epoch 52/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4982 - accuracy: 0.9663 - val_loss: 0.0520 - val_accuracy: 0.9893
    Epoch 53/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3500 - accuracy: 0.9778 - val_loss: 0.1224 - val_accuracy: 0.9947
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4058 - accuracy: 0.9716 - val_loss: 0.4572 - val_accuracy: 0.9679
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4690 - accuracy: 0.9645 - val_loss: 0.5336 - val_accuracy: 0.9733
    Epoch 56/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3387 - accuracy: 0.9734 - val_loss: 1.7307 - val_accuracy: 0.9412
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4574 - accuracy: 0.9707 - val_loss: 1.4013 - val_accuracy: 0.9465
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6470 - accuracy: 0.9734 - val_loss: 0.7483 - val_accuracy: 0.9626
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5098 - accuracy: 0.9654 - val_loss: 0.4977 - val_accuracy: 0.9679
    Epoch 60/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4283 - accuracy: 0.9663 - val_loss: 1.0388 - val_accuracy: 0.9465
    Epoch 61/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6423 - accuracy: 0.9672 - val_loss: 1.3371 - val_accuracy: 0.9519
    Epoch 62/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3560 - accuracy: 0.9725 - val_loss: 0.8452 - val_accuracy: 0.9572
    Epoch 63/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5840 - accuracy: 0.9699 - val_loss: 1.7921 - val_accuracy: 0.9251
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4692 - accuracy: 0.9725 - val_loss: 2.1769 - val_accuracy: 0.9037
    Epoch 65/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3632 - accuracy: 0.9787 - val_loss: 2.0421 - val_accuracy: 0.9144
    Epoch 66/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3658 - accuracy: 0.9761 - val_loss: 0.5500 - val_accuracy: 0.9572
    Epoch 67/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5258 - accuracy: 0.9672 - val_loss: 0.8393 - val_accuracy: 0.9519
    Epoch 68/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4934 - accuracy: 0.9725 - val_loss: 1.4082 - val_accuracy: 0.9358
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3326 - accuracy: 0.9770 - val_loss: 1.4749 - val_accuracy: 0.9412
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4948 - accuracy: 0.9672 - val_loss: 0.3836 - val_accuracy: 0.9733
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3222 - accuracy: 0.9743 - val_loss: 0.3080 - val_accuracy: 0.9840
    Epoch 72/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3739 - accuracy: 0.9716 - val_loss: 0.0699 - val_accuracy: 0.9947
    Epoch 73/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2305 - accuracy: 0.9805 - val_loss: 0.1291 - val_accuracy: 0.9840
    Epoch 74/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2123 - accuracy: 0.9787 - val_loss: 0.1740 - val_accuracy: 0.9840
    Epoch 75/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3670 - accuracy: 0.9743 - val_loss: 0.3374 - val_accuracy: 0.9733
    Epoch 76/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2596 - accuracy: 0.9787 - val_loss: 0.8359 - val_accuracy: 0.9679
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4990 - accuracy: 0.9699 - val_loss: 0.5442 - val_accuracy: 0.9679
    Epoch 78/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3529 - accuracy: 0.9832 - val_loss: 0.5223 - val_accuracy: 0.9572
    Epoch 79/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3722 - accuracy: 0.9743 - val_loss: 0.7190 - val_accuracy: 0.9679
    Epoch 80/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3130 - accuracy: 0.9796 - val_loss: 0.5203 - val_accuracy: 0.9679
    Epoch 81/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1750 - accuracy: 0.9849 - val_loss: 0.4968 - val_accuracy: 0.9679
    Epoch 82/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2387 - accuracy: 0.9778 - val_loss: 1.9132 - val_accuracy: 0.9198
    Epoch 83/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3249 - accuracy: 0.9805 - val_loss: 2.2952 - val_accuracy: 0.9412
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1572 - accuracy: 0.9876 - val_loss: 1.5590 - val_accuracy: 0.9626
    Epoch 85/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2197 - accuracy: 0.9787 - val_loss: 1.6255 - val_accuracy: 0.9679
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2475 - accuracy: 0.9823 - val_loss: 1.7069 - val_accuracy: 0.9358
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2107 - accuracy: 0.9849 - val_loss: 0.6236 - val_accuracy: 0.9733
    Epoch 88/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2720 - accuracy: 0.9814 - val_loss: 0.7771 - val_accuracy: 0.9679
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5267 - accuracy: 0.9637 - val_loss: 0.9419 - val_accuracy: 0.9679
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4004 - accuracy: 0.9734 - val_loss: 1.5251 - val_accuracy: 0.9626
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3092 - accuracy: 0.9796 - val_loss: 0.8717 - val_accuracy: 0.9572
    Epoch 92/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3934 - accuracy: 0.9734 - val_loss: 0.8562 - val_accuracy: 0.9572
    Epoch 93/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2726 - accuracy: 0.9778 - val_loss: 1.0273 - val_accuracy: 0.9626
    Epoch 94/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3765 - accuracy: 0.9787 - val_loss: 0.8908 - val_accuracy: 0.9733
    Epoch 95/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2320 - accuracy: 0.9823 - val_loss: 1.3958 - val_accuracy: 0.9572
    Epoch 96/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3500 - accuracy: 0.9840 - val_loss: 0.7216 - val_accuracy: 0.9733
    Epoch 97/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4059 - accuracy: 0.9867 - val_loss: 0.8234 - val_accuracy: 0.9679
    Epoch 98/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2296 - accuracy: 0.9832 - val_loss: 1.1266 - val_accuracy: 0.9572
    Epoch 99/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3484 - accuracy: 0.9823 - val_loss: 0.4798 - val_accuracy: 0.9786
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3711 - accuracy: 0.9796 - val_loss: 0.8412 - val_accuracy: 0.9626

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/103.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 11s 2s/step - loss: 1.8820 - accuracy: 0.9382

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.8819584846496582, 0.9381625652313232]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/104.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    18/18 [==============================] - 28s 2s/step - loss: 0.3319 - accuracy: 0.9770 - val_loss: 0.5572 - val_accuracy: 0.9786

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2079 - accuracy: 0.9832 - val_loss: 0.4546 - val_accuracy: 0.9786

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    18/18 [==============================] - 25s 1s/step - loss: 0.4433 - accuracy: 0.9743 - val_loss: 0.3940 - val_accuracy: 0.9786

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2372 - accuracy: 0.9840 - val_loss: 0.3473 - val_accuracy: 0.9840

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2605 - accuracy: 0.9805 - val_loss: 0.3202 - val_accuracy: 0.9840

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2434 - accuracy: 0.9778 - val_loss: 0.2867 - val_accuracy: 0.9840

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3639 - accuracy: 0.9805 - val_loss: 0.2592 - val_accuracy: 0.9840

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1683 - accuracy: 0.9849 - val_loss: 0.2313 - val_accuracy: 0.9840

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2269 - accuracy: 0.9787 - val_loss: 0.2040 - val_accuracy: 0.9840

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2803 - accuracy: 0.9858 - val_loss: 0.1951 - val_accuracy: 0.9840

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2334 - accuracy: 0.9858 - val_loss: 0.1882 - val_accuracy: 0.9840

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    18/18 [==============================] - 23s 1s/step - loss: 0.3268 - accuracy: 0.9814 - val_loss: 0.1890 - val_accuracy: 0.9840

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2378 - accuracy: 0.9814 - val_loss: 0.1814 - val_accuracy: 0.9840

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2325 - accuracy: 0.9849 - val_loss: 0.1681 - val_accuracy: 0.9840

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    18/18 [==============================] - 23s 1s/step - loss: 0.3596 - accuracy: 0.9814 - val_loss: 0.1512 - val_accuracy: 0.9840

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1372 - accuracy: 0.9902 - val_loss: 0.1414 - val_accuracy: 0.9840

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2715 - accuracy: 0.9814 - val_loss: 0.1375 - val_accuracy: 0.9840

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2948 - accuracy: 0.9832 - val_loss: 0.1279 - val_accuracy: 0.9840

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2623 - accuracy: 0.9849 - val_loss: 0.1160 - val_accuracy: 0.9840

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3976 - accuracy: 0.9796 - val_loss: 0.1292 - val_accuracy: 0.9840

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1128 - accuracy: 0.9885 - val_loss: 0.1332 - val_accuracy: 0.9840

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2720 - accuracy: 0.9796 - val_loss: 0.1352 - val_accuracy: 0.9840

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    18/18 [==============================] - 24s 1s/step - loss: 0.0958 - accuracy: 0.9911 - val_loss: 0.1364 - val_accuracy: 0.9840

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2016 - accuracy: 0.9867 - val_loss: 0.1343 - val_accuracy: 0.9840

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1106 - accuracy: 0.9885 - val_loss: 0.1429 - val_accuracy: 0.9840

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2641 - accuracy: 0.9840 - val_loss: 0.1519 - val_accuracy: 0.9786

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    18/18 [==============================] - 25s 1s/step - loss: 0.0998 - accuracy: 0.9920 - val_loss: 0.1562 - val_accuracy: 0.9786

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1939 - accuracy: 0.9840 - val_loss: 0.1646 - val_accuracy: 0.9786

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1706 - accuracy: 0.9832 - val_loss: 0.1509 - val_accuracy: 0.9786

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2364 - accuracy: 0.9894 - val_loss: 0.1646 - val_accuracy: 0.9786

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2419 - accuracy: 0.9858 - val_loss: 0.1717 - val_accuracy: 0.9786

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2459 - accuracy: 0.9894 - val_loss: 0.1583 - val_accuracy: 0.9786

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    18/18 [==============================] - 23s 1s/step - loss: 0.3193 - accuracy: 0.9832 - val_loss: 0.1584 - val_accuracy: 0.9786

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1504 - accuracy: 0.9894 - val_loss: 0.1624 - val_accuracy: 0.9786

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    18/18 [==============================] - 27s 1s/step - loss: 0.1638 - accuracy: 0.9902 - val_loss: 0.1585 - val_accuracy: 0.9786

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1748 - accuracy: 0.9885 - val_loss: 0.1562 - val_accuracy: 0.9786

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1632 - accuracy: 0.9858 - val_loss: 0.1485 - val_accuracy: 0.9786

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2545 - accuracy: 0.9858 - val_loss: 0.1517 - val_accuracy: 0.9786

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2746 - accuracy: 0.9796 - val_loss: 0.1679 - val_accuracy: 0.9786

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    18/18 [==============================] - 24s 1s/step - loss: 0.3104 - accuracy: 0.9814 - val_loss: 0.1609 - val_accuracy: 0.9786

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1388 - accuracy: 0.9920 - val_loss: 0.1496 - val_accuracy: 0.9786

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2336 - accuracy: 0.9814 - val_loss: 0.1209 - val_accuracy: 0.9786

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1808 - accuracy: 0.9902 - val_loss: 0.0973 - val_accuracy: 0.9840

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1494 - accuracy: 0.9867 - val_loss: 0.0885 - val_accuracy: 0.9840

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2325 - accuracy: 0.9858 - val_loss: 0.0991 - val_accuracy: 0.9840

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1741 - accuracy: 0.9867 - val_loss: 0.1213 - val_accuracy: 0.9786

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1652 - accuracy: 0.9894 - val_loss: 0.1387 - val_accuracy: 0.9786

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    18/18 [==============================] - 26s 1s/step - loss: 0.2045 - accuracy: 0.9867 - val_loss: 0.1457 - val_accuracy: 0.9786

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    18/18 [==============================] - 26s 1s/step - loss: 0.0821 - accuracy: 0.9911 - val_loss: 0.1526 - val_accuracy: 0.9786

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2760 - accuracy: 0.9832 - val_loss: 0.1478 - val_accuracy: 0.9786

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/105.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.7476 - accuracy: 0.9664

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7476311922073364, 0.9664310812950134]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9664310954063604
                  precision    recall  f1-score   support

               0       0.92      0.96      0.94       164
               1       0.98      0.97      0.98       402

        accuracy                           0.97       566
       macro avg       0.95      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/106.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/107.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/108.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.92      0.96      0.94       164
               1       0.98      0.97      0.98       402

        accuracy                           0.97       566
       macro avg       0.95      0.97      0.96       566
    weighted avg       0.97      0.97      0.97       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.92      0.96      0.97      0.94      0.97      0.93       164
              1       0.98      0.97      0.96      0.98      0.97      0.93       402

    avg / total       0.97      0.97      0.96      0.97      0.97      0.93       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
