<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "400X"
trainable_blocks = ["block4"]
irun = 5
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_400X-BREAKHIS-Dataset-60-10-30-VGGINet/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/400X/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.4578 - accuracy: 0.4297WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 22s 3s/step - loss: 4.7888 - accuracy: 0.7034 - val_loss: 20.8050 - val_accuracy: 0.7950
    Epoch 2/100
    8/8 [==============================] - 14s 2s/step - loss: 3.2338 - accuracy: 0.8332 - val_loss: 4.6217 - val_accuracy: 0.9006
    Epoch 3/100
    8/8 [==============================] - 14s 2s/step - loss: 2.0599 - accuracy: 0.8589 - val_loss: 2.9523 - val_accuracy: 0.9006
    Epoch 4/100
    8/8 [==============================] - 11s 1s/step - loss: 1.6627 - accuracy: 0.8589 - val_loss: 1.8031 - val_accuracy: 0.9130
    Epoch 5/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2929 - accuracy: 0.8847 - val_loss: 1.8016 - val_accuracy: 0.9255
    Epoch 6/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2396 - accuracy: 0.8877 - val_loss: 1.2017 - val_accuracy: 0.9193
    Epoch 7/100
    8/8 [==============================] - 14s 2s/step - loss: 1.1689 - accuracy: 0.8847 - val_loss: 1.8391 - val_accuracy: 0.8758
    Epoch 8/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2593 - accuracy: 0.9053 - val_loss: 1.5451 - val_accuracy: 0.9255
    Epoch 9/100
    8/8 [==============================] - 14s 2s/step - loss: 0.9608 - accuracy: 0.9186 - val_loss: 2.0953 - val_accuracy: 0.8820
    Epoch 10/100
    8/8 [==============================] - 14s 2s/step - loss: 1.1193 - accuracy: 0.9063 - val_loss: 1.5863 - val_accuracy: 0.9006
    Epoch 11/100
    8/8 [==============================] - 15s 2s/step - loss: 0.9817 - accuracy: 0.9011 - val_loss: 1.2777 - val_accuracy: 0.9317
    Epoch 12/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0748 - accuracy: 0.8960 - val_loss: 1.7205 - val_accuracy: 0.9068
    Epoch 13/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2726 - accuracy: 0.9022 - val_loss: 1.1030 - val_accuracy: 0.9503
    Epoch 14/100
    8/8 [==============================] - 14s 2s/step - loss: 1.1077 - accuracy: 0.9207 - val_loss: 1.0571 - val_accuracy: 0.9379
    Epoch 15/100
    8/8 [==============================] - 14s 2s/step - loss: 1.1637 - accuracy: 0.9011 - val_loss: 1.0841 - val_accuracy: 0.9317
    Epoch 16/100
    8/8 [==============================] - 17s 2s/step - loss: 1.3511 - accuracy: 0.9022 - val_loss: 2.2590 - val_accuracy: 0.8571
    Epoch 17/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0509 - accuracy: 0.9094 - val_loss: 4.9311 - val_accuracy: 0.8199
    Epoch 18/100
    8/8 [==============================] - 14s 2s/step - loss: 1.3100 - accuracy: 0.9011 - val_loss: 1.6883 - val_accuracy: 0.8944
    Epoch 19/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2083 - accuracy: 0.9248 - val_loss: 2.1775 - val_accuracy: 0.9255
    Epoch 20/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0003 - accuracy: 0.9228 - val_loss: 1.5707 - val_accuracy: 0.9317
    Epoch 21/100
    8/8 [==============================] - 14s 2s/step - loss: 0.9297 - accuracy: 0.9300 - val_loss: 1.3631 - val_accuracy: 0.9006
    Epoch 22/100
    8/8 [==============================] - 14s 2s/step - loss: 0.8928 - accuracy: 0.9197 - val_loss: 1.4850 - val_accuracy: 0.9255
    Epoch 23/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2427 - accuracy: 0.9207 - val_loss: 1.0054 - val_accuracy: 0.9317
    Epoch 24/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7957 - accuracy: 0.9341 - val_loss: 0.9860 - val_accuracy: 0.9379
    Epoch 25/100
    8/8 [==============================] - 14s 2s/step - loss: 0.8757 - accuracy: 0.9300 - val_loss: 1.0103 - val_accuracy: 0.9255
    Epoch 26/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7800 - accuracy: 0.9361 - val_loss: 1.6805 - val_accuracy: 0.9130
    Epoch 27/100
    8/8 [==============================] - 14s 2s/step - loss: 0.8108 - accuracy: 0.9454 - val_loss: 1.9407 - val_accuracy: 0.9006
    Epoch 28/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7352 - accuracy: 0.9403 - val_loss: 1.3302 - val_accuracy: 0.9130
    Epoch 29/100
    8/8 [==============================] - 14s 2s/step - loss: 0.8684 - accuracy: 0.9372 - val_loss: 1.3594 - val_accuracy: 0.9193
    Epoch 30/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7155 - accuracy: 0.9413 - val_loss: 1.9851 - val_accuracy: 0.9006
    Epoch 31/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6850 - accuracy: 0.9506 - val_loss: 1.5094 - val_accuracy: 0.9130
    Epoch 32/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7044 - accuracy: 0.9403 - val_loss: 2.2157 - val_accuracy: 0.9193
    Epoch 33/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7727 - accuracy: 0.9444 - val_loss: 1.6970 - val_accuracy: 0.9255
    Epoch 34/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6610 - accuracy: 0.9392 - val_loss: 1.9777 - val_accuracy: 0.8882
    Epoch 35/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7448 - accuracy: 0.9361 - val_loss: 1.3746 - val_accuracy: 0.9193
    Epoch 36/100
    8/8 [==============================] - 15s 2s/step - loss: 0.6814 - accuracy: 0.9403 - val_loss: 2.2759 - val_accuracy: 0.9130
    Epoch 37/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7426 - accuracy: 0.9382 - val_loss: 1.8080 - val_accuracy: 0.9193
    Epoch 38/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5602 - accuracy: 0.9578 - val_loss: 1.7891 - val_accuracy: 0.9068
    Epoch 39/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5927 - accuracy: 0.9537 - val_loss: 1.5780 - val_accuracy: 0.9130
    Epoch 40/100
    8/8 [==============================] - 15s 2s/step - loss: 0.5900 - accuracy: 0.9526 - val_loss: 1.7004 - val_accuracy: 0.9068
    Epoch 41/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3937 - accuracy: 0.9567 - val_loss: 2.0411 - val_accuracy: 0.9193
    Epoch 42/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6600 - accuracy: 0.9485 - val_loss: 1.4833 - val_accuracy: 0.9379
    Epoch 43/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5006 - accuracy: 0.9588 - val_loss: 1.9564 - val_accuracy: 0.9255
    Epoch 44/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5771 - accuracy: 0.9495 - val_loss: 1.6737 - val_accuracy: 0.9255
    Epoch 45/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5383 - accuracy: 0.9506 - val_loss: 1.9471 - val_accuracy: 0.9193
    Epoch 46/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4832 - accuracy: 0.9629 - val_loss: 3.0825 - val_accuracy: 0.9130
    Epoch 47/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6028 - accuracy: 0.9506 - val_loss: 2.5108 - val_accuracy: 0.9130
    Epoch 48/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4773 - accuracy: 0.9588 - val_loss: 2.1336 - val_accuracy: 0.9317
    Epoch 49/100
    8/8 [==============================] - 15s 2s/step - loss: 0.4867 - accuracy: 0.9506 - val_loss: 1.6021 - val_accuracy: 0.9379
    Epoch 50/100
    8/8 [==============================] - 15s 2s/step - loss: 0.5823 - accuracy: 0.9578 - val_loss: 2.2078 - val_accuracy: 0.9255
    Epoch 51/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5434 - accuracy: 0.9588 - val_loss: 2.0288 - val_accuracy: 0.9068
    Epoch 52/100
    8/8 [==============================] - 15s 2s/step - loss: 0.4083 - accuracy: 0.9681 - val_loss: 2.4546 - val_accuracy: 0.9193
    Epoch 53/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6855 - accuracy: 0.9629 - val_loss: 2.4239 - val_accuracy: 0.9193
    Epoch 54/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5950 - accuracy: 0.9537 - val_loss: 2.3004 - val_accuracy: 0.9193
    Epoch 55/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4852 - accuracy: 0.9588 - val_loss: 3.1036 - val_accuracy: 0.9130
    Epoch 56/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6876 - accuracy: 0.9588 - val_loss: 1.9975 - val_accuracy: 0.9255
    Epoch 57/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6366 - accuracy: 0.9629 - val_loss: 1.3647 - val_accuracy: 0.9379
    Epoch 58/100
    8/8 [==============================] - 15s 2s/step - loss: 0.5549 - accuracy: 0.9588 - val_loss: 3.6004 - val_accuracy: 0.8882
    Epoch 59/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4411 - accuracy: 0.9650 - val_loss: 2.4638 - val_accuracy: 0.9255
    Epoch 60/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4065 - accuracy: 0.9650 - val_loss: 2.2751 - val_accuracy: 0.9317
    Epoch 61/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3829 - accuracy: 0.9701 - val_loss: 2.8923 - val_accuracy: 0.9255
    Epoch 62/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5411 - accuracy: 0.9619 - val_loss: 2.7532 - val_accuracy: 0.9317
    Epoch 63/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3832 - accuracy: 0.9619 - val_loss: 2.3223 - val_accuracy: 0.9317
    Epoch 64/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3880 - accuracy: 0.9660 - val_loss: 2.8034 - val_accuracy: 0.9255
    Epoch 65/100
    8/8 [==============================] - 15s 2s/step - loss: 0.2604 - accuracy: 0.9753 - val_loss: 2.2043 - val_accuracy: 0.9130
    Epoch 66/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3641 - accuracy: 0.9743 - val_loss: 1.9867 - val_accuracy: 0.9317
    Epoch 67/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3379 - accuracy: 0.9784 - val_loss: 1.8424 - val_accuracy: 0.9379
    Epoch 68/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4527 - accuracy: 0.9609 - val_loss: 1.8049 - val_accuracy: 0.9441
    Epoch 69/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3130 - accuracy: 0.9712 - val_loss: 1.6788 - val_accuracy: 0.9441
    Epoch 70/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4648 - accuracy: 0.9629 - val_loss: 1.7862 - val_accuracy: 0.9503
    Epoch 71/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5364 - accuracy: 0.9650 - val_loss: 4.9833 - val_accuracy: 0.8944
    Epoch 72/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6178 - accuracy: 0.9629 - val_loss: 2.7121 - val_accuracy: 0.9317
    Epoch 73/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5291 - accuracy: 0.9640 - val_loss: 2.4301 - val_accuracy: 0.9317
    Epoch 74/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2728 - accuracy: 0.9784 - val_loss: 4.2491 - val_accuracy: 0.9068
    Epoch 75/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4869 - accuracy: 0.9691 - val_loss: 3.6502 - val_accuracy: 0.9317
    Epoch 76/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3735 - accuracy: 0.9712 - val_loss: 2.9764 - val_accuracy: 0.9130
    Epoch 77/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3979 - accuracy: 0.9701 - val_loss: 2.6088 - val_accuracy: 0.9441
    Epoch 78/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3257 - accuracy: 0.9712 - val_loss: 2.1378 - val_accuracy: 0.9503
    Epoch 79/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3783 - accuracy: 0.9784 - val_loss: 2.1797 - val_accuracy: 0.9317
    Epoch 80/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2847 - accuracy: 0.9763 - val_loss: 3.6674 - val_accuracy: 0.9006
    Epoch 81/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3587 - accuracy: 0.9743 - val_loss: 2.7087 - val_accuracy: 0.9317
    Epoch 82/100
    8/8 [==============================] - 13s 2s/step - loss: 0.2813 - accuracy: 0.9773 - val_loss: 3.0718 - val_accuracy: 0.9255
    Epoch 83/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3684 - accuracy: 0.9732 - val_loss: 2.7556 - val_accuracy: 0.9317
    Epoch 84/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3138 - accuracy: 0.9732 - val_loss: 3.1447 - val_accuracy: 0.9255
    Epoch 85/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4724 - accuracy: 0.9660 - val_loss: 2.9833 - val_accuracy: 0.9317
    Epoch 86/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3376 - accuracy: 0.9753 - val_loss: 3.1381 - val_accuracy: 0.9379
    Epoch 87/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5179 - accuracy: 0.9650 - val_loss: 3.3654 - val_accuracy: 0.9130
    Epoch 88/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4904 - accuracy: 0.9722 - val_loss: 3.0966 - val_accuracy: 0.9193
    Epoch 89/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2328 - accuracy: 0.9825 - val_loss: 2.2340 - val_accuracy: 0.9255
    Epoch 90/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4211 - accuracy: 0.9629 - val_loss: 3.6064 - val_accuracy: 0.9130
    Epoch 91/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3625 - accuracy: 0.9763 - val_loss: 4.7554 - val_accuracy: 0.9130
    Epoch 92/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3903 - accuracy: 0.9815 - val_loss: 3.9602 - val_accuracy: 0.9317
    Epoch 93/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5401 - accuracy: 0.9691 - val_loss: 2.3460 - val_accuracy: 0.9379
    Epoch 94/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3981 - accuracy: 0.9815 - val_loss: 3.0604 - val_accuracy: 0.9379
    Epoch 95/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3738 - accuracy: 0.9712 - val_loss: 3.6000 - val_accuracy: 0.9255
    Epoch 96/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3670 - accuracy: 0.9743 - val_loss: 3.7186 - val_accuracy: 0.9317
    Epoch 97/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4473 - accuracy: 0.9763 - val_loss: 3.8941 - val_accuracy: 0.9317
    Epoch 98/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3072 - accuracy: 0.9743 - val_loss: 3.0487 - val_accuracy: 0.9255
    Epoch 99/100
    8/8 [==============================] - 14s 2s/step - loss: 0.1880 - accuracy: 0.9846 - val_loss: 4.4158 - val_accuracy: 0.9068
    Epoch 100/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3706 - accuracy: 0.9773 - val_loss: 3.8783 - val_accuracy: 0.8944

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/1.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 13s 3s/step - loss: 3.3403 - accuracy: 0.9098

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [3.340254783630371, 0.9098360538482666]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/2.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4554 - accuracy: 0.9701 - val_loss: 3.6454 - val_accuracy: 0.8944

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    16/16 [==============================] - 19s 1s/step - loss: 0.4453 - accuracy: 0.9732 - val_loss: 3.4427 - val_accuracy: 0.8944

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4348 - accuracy: 0.9763 - val_loss: 3.3306 - val_accuracy: 0.9068

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2817 - accuracy: 0.9743 - val_loss: 3.2518 - val_accuracy: 0.9068

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    16/16 [==============================] - 20s 1s/step - loss: 0.6593 - accuracy: 0.9722 - val_loss: 3.2021 - val_accuracy: 0.9130

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3639 - accuracy: 0.9773 - val_loss: 3.1828 - val_accuracy: 0.9068

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3235 - accuracy: 0.9773 - val_loss: 3.1814 - val_accuracy: 0.9068

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3236 - accuracy: 0.9804 - val_loss: 3.1965 - val_accuracy: 0.9068

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3974 - accuracy: 0.9763 - val_loss: 3.1656 - val_accuracy: 0.9068

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2181 - accuracy: 0.9825 - val_loss: 3.1304 - val_accuracy: 0.9130

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    16/16 [==============================] - 22s 1s/step - loss: 0.4686 - accuracy: 0.9712 - val_loss: 3.0891 - val_accuracy: 0.9193

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2852 - accuracy: 0.9815 - val_loss: 3.0645 - val_accuracy: 0.9193

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2654 - accuracy: 0.9753 - val_loss: 3.0598 - val_accuracy: 0.9193

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2707 - accuracy: 0.9846 - val_loss: 3.0674 - val_accuracy: 0.9130

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1816 - accuracy: 0.9835 - val_loss: 3.0559 - val_accuracy: 0.9130

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2152 - accuracy: 0.9815 - val_loss: 3.0753 - val_accuracy: 0.9130

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2770 - accuracy: 0.9773 - val_loss: 3.0677 - val_accuracy: 0.9193

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2067 - accuracy: 0.9856 - val_loss: 3.0442 - val_accuracy: 0.9130

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3113 - accuracy: 0.9784 - val_loss: 3.0104 - val_accuracy: 0.9130

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2205 - accuracy: 0.9794 - val_loss: 3.0201 - val_accuracy: 0.9130

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    16/16 [==============================] - 19s 1s/step - loss: 0.3457 - accuracy: 0.9794 - val_loss: 2.9763 - val_accuracy: 0.9068

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2049 - accuracy: 0.9825 - val_loss: 2.9709 - val_accuracy: 0.9130

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2022 - accuracy: 0.9835 - val_loss: 2.9850 - val_accuracy: 0.9068

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2342 - accuracy: 0.9804 - val_loss: 2.9749 - val_accuracy: 0.9068

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2051 - accuracy: 0.9804 - val_loss: 2.9797 - val_accuracy: 0.9068

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    16/16 [==============================] - 18s 1s/step - loss: 0.3501 - accuracy: 0.9743 - val_loss: 2.9627 - val_accuracy: 0.9068

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3484 - accuracy: 0.9773 - val_loss: 2.9674 - val_accuracy: 0.9068

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2798 - accuracy: 0.9784 - val_loss: 2.9497 - val_accuracy: 0.9068

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2636 - accuracy: 0.9815 - val_loss: 2.9594 - val_accuracy: 0.9068

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3763 - accuracy: 0.9712 - val_loss: 2.9293 - val_accuracy: 0.9068

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2777 - accuracy: 0.9794 - val_loss: 2.9222 - val_accuracy: 0.9068

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2598 - accuracy: 0.9773 - val_loss: 2.9391 - val_accuracy: 0.9130

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3144 - accuracy: 0.9784 - val_loss: 2.9266 - val_accuracy: 0.9068

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2750 - accuracy: 0.9815 - val_loss: 2.9316 - val_accuracy: 0.9068

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2454 - accuracy: 0.9866 - val_loss: 2.9248 - val_accuracy: 0.9068

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3118 - accuracy: 0.9804 - val_loss: 2.8963 - val_accuracy: 0.9068

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2571 - accuracy: 0.9825 - val_loss: 2.9138 - val_accuracy: 0.9068

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3832 - accuracy: 0.9835 - val_loss: 2.9260 - val_accuracy: 0.9068

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2069 - accuracy: 0.9794 - val_loss: 2.8820 - val_accuracy: 0.9068

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2376 - accuracy: 0.9825 - val_loss: 2.8873 - val_accuracy: 0.9068

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3820 - accuracy: 0.9804 - val_loss: 2.8302 - val_accuracy: 0.9068

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3819 - accuracy: 0.9722 - val_loss: 2.8552 - val_accuracy: 0.9130

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2636 - accuracy: 0.9794 - val_loss: 2.8611 - val_accuracy: 0.9068

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2182 - accuracy: 0.9825 - val_loss: 2.8346 - val_accuracy: 0.9068

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4298 - accuracy: 0.9701 - val_loss: 2.8459 - val_accuracy: 0.9068

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2566 - accuracy: 0.9815 - val_loss: 2.8643 - val_accuracy: 0.9068

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2468 - accuracy: 0.9794 - val_loss: 2.8686 - val_accuracy: 0.9068

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3083 - accuracy: 0.9722 - val_loss: 2.8739 - val_accuracy: 0.9068

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    16/16 [==============================] - 21s 1s/step - loss: 0.0962 - accuracy: 0.9887 - val_loss: 2.8915 - val_accuracy: 0.9068

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1754 - accuracy: 0.9846 - val_loss: 2.9003 - val_accuracy: 0.9068

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/3.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 5s 1s/step - loss: 1.6853 - accuracy: 0.9406

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.6853009462356567, 0.9405737519264221]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9405737704918032
                  precision    recall  f1-score   support

               0       0.93      0.87      0.90       148
               1       0.95      0.97      0.96       340

        accuracy                           0.94       488
       macro avg       0.94      0.92      0.93       488
    weighted avg       0.94      0.94      0.94       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/4.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/5.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/6.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.87      0.90       148
               1       0.95      0.97      0.96       340

        accuracy                           0.94       488
       macro avg       0.94      0.92      0.93       488
    weighted avg       0.94      0.94      0.94       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.87      0.97      0.90      0.92      0.84       148
              1       0.95      0.97      0.87      0.96      0.92      0.85       340

    avg / total       0.94      0.94      0.90      0.94      0.92      0.85       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
