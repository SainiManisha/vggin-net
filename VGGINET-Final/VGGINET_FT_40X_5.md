<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "40X"
trainable_blocks = ["block3", "block4"]
irun = 5
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_40X-BREAKHIS-Dataset-60-10-30-VGGINet/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/40X/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.2497 - accuracy: 0.4297WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 25s 3s/step - loss: 6.2340 - accuracy: 0.6769 - val_loss: 17.1129 - val_accuracy: 0.7263
    Epoch 2/100
    9/9 [==============================] - 16s 2s/step - loss: 3.1384 - accuracy: 0.8394 - val_loss: 7.2201 - val_accuracy: 0.8715
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 2.1281 - accuracy: 0.8654 - val_loss: 3.3828 - val_accuracy: 0.8603
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 1.3497 - accuracy: 0.8830 - val_loss: 2.7407 - val_accuracy: 0.8659
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0528 - accuracy: 0.9211 - val_loss: 3.8887 - val_accuracy: 0.8380
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0630 - accuracy: 0.9025 - val_loss: 1.4117 - val_accuracy: 0.9050
    Epoch 7/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7135 - accuracy: 0.9276 - val_loss: 1.5731 - val_accuracy: 0.9162
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5882 - accuracy: 0.9341 - val_loss: 1.1818 - val_accuracy: 0.9106
    Epoch 9/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6862 - accuracy: 0.9331 - val_loss: 1.4408 - val_accuracy: 0.9162
    Epoch 10/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4831 - accuracy: 0.9452 - val_loss: 1.7657 - val_accuracy: 0.9218
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6766 - accuracy: 0.9304 - val_loss: 1.0178 - val_accuracy: 0.9218
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6009 - accuracy: 0.9341 - val_loss: 1.2904 - val_accuracy: 0.9497
    Epoch 13/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5282 - accuracy: 0.9471 - val_loss: 1.0768 - val_accuracy: 0.9274
    Epoch 14/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5758 - accuracy: 0.9406 - val_loss: 1.7484 - val_accuracy: 0.9330
    Epoch 15/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6808 - accuracy: 0.9406 - val_loss: 1.8850 - val_accuracy: 0.9274
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7180 - accuracy: 0.9443 - val_loss: 1.2684 - val_accuracy: 0.9274
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6868 - accuracy: 0.9434 - val_loss: 1.9498 - val_accuracy: 0.9274
    Epoch 18/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5742 - accuracy: 0.9480 - val_loss: 1.4643 - val_accuracy: 0.9441
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5271 - accuracy: 0.9508 - val_loss: 0.7667 - val_accuracy: 0.9609
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6419 - accuracy: 0.9461 - val_loss: 0.9730 - val_accuracy: 0.9441
    Epoch 21/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6467 - accuracy: 0.9508 - val_loss: 1.7764 - val_accuracy: 0.9385
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3937 - accuracy: 0.9638 - val_loss: 1.4795 - val_accuracy: 0.9385
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4426 - accuracy: 0.9629 - val_loss: 1.0359 - val_accuracy: 0.9497
    Epoch 24/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2614 - accuracy: 0.9731 - val_loss: 1.4061 - val_accuracy: 0.9385
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5149 - accuracy: 0.9601 - val_loss: 1.0343 - val_accuracy: 0.9497
    Epoch 26/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4138 - accuracy: 0.9666 - val_loss: 0.7706 - val_accuracy: 0.9553
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2753 - accuracy: 0.9721 - val_loss: 0.9289 - val_accuracy: 0.9274
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5457 - accuracy: 0.9582 - val_loss: 2.6470 - val_accuracy: 0.9050
    Epoch 29/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3213 - accuracy: 0.9703 - val_loss: 1.3695 - val_accuracy: 0.9385
    Epoch 30/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4934 - accuracy: 0.9601 - val_loss: 1.1585 - val_accuracy: 0.9553
    Epoch 31/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2789 - accuracy: 0.9694 - val_loss: 1.8233 - val_accuracy: 0.9106
    Epoch 32/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2797 - accuracy: 0.9721 - val_loss: 1.0109 - val_accuracy: 0.9553
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3783 - accuracy: 0.9703 - val_loss: 1.4038 - val_accuracy: 0.9441
    Epoch 34/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2637 - accuracy: 0.9759 - val_loss: 1.3936 - val_accuracy: 0.9497
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3576 - accuracy: 0.9721 - val_loss: 1.2605 - val_accuracy: 0.9330
    Epoch 36/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5449 - accuracy: 0.9629 - val_loss: 1.3585 - val_accuracy: 0.9385
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3819 - accuracy: 0.9601 - val_loss: 2.1616 - val_accuracy: 0.9162
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5692 - accuracy: 0.9619 - val_loss: 1.7115 - val_accuracy: 0.9274
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2837 - accuracy: 0.9721 - val_loss: 1.4713 - val_accuracy: 0.9497
    Epoch 40/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5034 - accuracy: 0.9731 - val_loss: 1.5159 - val_accuracy: 0.9553
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3589 - accuracy: 0.9703 - val_loss: 1.1961 - val_accuracy: 0.9609
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3605 - accuracy: 0.9740 - val_loss: 0.9379 - val_accuracy: 0.9497
    Epoch 43/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4479 - accuracy: 0.9740 - val_loss: 0.7085 - val_accuracy: 0.9609
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2470 - accuracy: 0.9768 - val_loss: 1.0328 - val_accuracy: 0.9497
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3762 - accuracy: 0.9749 - val_loss: 1.1666 - val_accuracy: 0.9497
    Epoch 46/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3143 - accuracy: 0.9768 - val_loss: 1.6648 - val_accuracy: 0.9553
    Epoch 47/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2808 - accuracy: 0.9824 - val_loss: 1.2627 - val_accuracy: 0.9609
    Epoch 48/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3126 - accuracy: 0.9777 - val_loss: 1.1906 - val_accuracy: 0.9553
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2483 - accuracy: 0.9786 - val_loss: 1.1611 - val_accuracy: 0.9441
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3693 - accuracy: 0.9712 - val_loss: 1.3667 - val_accuracy: 0.9385
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2244 - accuracy: 0.9861 - val_loss: 1.3646 - val_accuracy: 0.9441
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2081 - accuracy: 0.9833 - val_loss: 1.3799 - val_accuracy: 0.9609
    Epoch 53/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2754 - accuracy: 0.9749 - val_loss: 1.5963 - val_accuracy: 0.9609
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2004 - accuracy: 0.9861 - val_loss: 1.2737 - val_accuracy: 0.9385
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4278 - accuracy: 0.9759 - val_loss: 0.8468 - val_accuracy: 0.9609
    Epoch 56/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1916 - accuracy: 0.9824 - val_loss: 1.7812 - val_accuracy: 0.9497
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2874 - accuracy: 0.9749 - val_loss: 3.6148 - val_accuracy: 0.9218
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3746 - accuracy: 0.9759 - val_loss: 1.5910 - val_accuracy: 0.9497
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2447 - accuracy: 0.9805 - val_loss: 1.1106 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 19s 2s/step - loss: 0.4181 - accuracy: 0.9694 - val_loss: 0.8608 - val_accuracy: 0.9721
    Epoch 61/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1917 - accuracy: 0.9851 - val_loss: 0.9677 - val_accuracy: 0.9497
    Epoch 62/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2237 - accuracy: 0.9861 - val_loss: 1.0568 - val_accuracy: 0.9665
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3033 - accuracy: 0.9833 - val_loss: 1.1952 - val_accuracy: 0.9665
    Epoch 64/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3755 - accuracy: 0.9759 - val_loss: 0.8379 - val_accuracy: 0.9609
    Epoch 65/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1148 - accuracy: 0.9833 - val_loss: 0.9031 - val_accuracy: 0.9609
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2225 - accuracy: 0.9861 - val_loss: 1.2724 - val_accuracy: 0.9497
    Epoch 67/100
    9/9 [==============================] - 19s 2s/step - loss: 0.3386 - accuracy: 0.9861 - val_loss: 1.4408 - val_accuracy: 0.9497
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2325 - accuracy: 0.9842 - val_loss: 1.0285 - val_accuracy: 0.9609
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2663 - accuracy: 0.9786 - val_loss: 1.0695 - val_accuracy: 0.9553
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2302 - accuracy: 0.9851 - val_loss: 1.4018 - val_accuracy: 0.9721
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1431 - accuracy: 0.9907 - val_loss: 1.8733 - val_accuracy: 0.9553
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2615 - accuracy: 0.9824 - val_loss: 1.3518 - val_accuracy: 0.9665
    Epoch 73/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1821 - accuracy: 0.9889 - val_loss: 1.0989 - val_accuracy: 0.9665
    Epoch 74/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2006 - accuracy: 0.9851 - val_loss: 1.0482 - val_accuracy: 0.9609
    Epoch 75/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1946 - accuracy: 0.9851 - val_loss: 1.0853 - val_accuracy: 0.9609
    Epoch 76/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3361 - accuracy: 0.9805 - val_loss: 0.8789 - val_accuracy: 0.9553
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1998 - accuracy: 0.9851 - val_loss: 1.9453 - val_accuracy: 0.9218
    Epoch 78/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1273 - accuracy: 0.9879 - val_loss: 0.6649 - val_accuracy: 0.9553
    Epoch 79/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2086 - accuracy: 0.9889 - val_loss: 0.5115 - val_accuracy: 0.9832
    Epoch 80/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2931 - accuracy: 0.9824 - val_loss: 1.0257 - val_accuracy: 0.9553
    Epoch 81/100
    9/9 [==============================] - 16s 2s/step - loss: 0.0653 - accuracy: 0.9916 - val_loss: 0.6923 - val_accuracy: 0.9609
    Epoch 82/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1231 - accuracy: 0.9889 - val_loss: 0.5848 - val_accuracy: 0.9665
    Epoch 83/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3925 - accuracy: 0.9824 - val_loss: 0.9049 - val_accuracy: 0.9665
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2628 - accuracy: 0.9842 - val_loss: 2.0642 - val_accuracy: 0.9330
    Epoch 85/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3723 - accuracy: 0.9833 - val_loss: 1.2829 - val_accuracy: 0.9497
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2700 - accuracy: 0.9861 - val_loss: 0.2767 - val_accuracy: 0.9721
    Epoch 87/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2424 - accuracy: 0.9833 - val_loss: 0.3585 - val_accuracy: 0.9777
    Epoch 88/100
    9/9 [==============================] - 16s 2s/step - loss: 0.0855 - accuracy: 0.9916 - val_loss: 0.6775 - val_accuracy: 0.9609
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2342 - accuracy: 0.9861 - val_loss: 0.5829 - val_accuracy: 0.9609
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1948 - accuracy: 0.9861 - val_loss: 1.0723 - val_accuracy: 0.9553
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1214 - accuracy: 0.9851 - val_loss: 0.8742 - val_accuracy: 0.9665
    Epoch 92/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3290 - accuracy: 0.9833 - val_loss: 1.0139 - val_accuracy: 0.9721
    Epoch 93/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1649 - accuracy: 0.9889 - val_loss: 2.0162 - val_accuracy: 0.9385
    Epoch 94/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3725 - accuracy: 0.9851 - val_loss: 0.9533 - val_accuracy: 0.9609
    Epoch 95/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3084 - accuracy: 0.9870 - val_loss: 0.5540 - val_accuracy: 0.9832
    Epoch 96/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1523 - accuracy: 0.9907 - val_loss: 0.5938 - val_accuracy: 0.9832
    Epoch 97/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1523 - accuracy: 0.9916 - val_loss: 0.6226 - val_accuracy: 0.9777
    Epoch 98/100
    9/9 [==============================] - 19s 2s/step - loss: 0.2754 - accuracy: 0.9935 - val_loss: 0.3523 - val_accuracy: 0.9832
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1659 - accuracy: 0.9889 - val_loss: 0.5137 - val_accuracy: 0.9777
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2083 - accuracy: 0.9898 - val_loss: 0.5638 - val_accuracy: 0.9721

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/97.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 9s 2s/step - loss: 1.2671 - accuracy: 0.9703

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.2670865058898926, 0.9703153967857361]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/98.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    17/17 [==============================] - 22s 1s/step - loss: 0.3632 - accuracy: 0.9805 - val_loss: 0.3795 - val_accuracy: 0.9888

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 25s 1s/step - loss: 0.2461 - accuracy: 0.9833 - val_loss: 0.3589 - val_accuracy: 0.9832

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1806 - accuracy: 0.9898 - val_loss: 0.3456 - val_accuracy: 0.9832

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1847 - accuracy: 0.9861 - val_loss: 0.3343 - val_accuracy: 0.9832

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2286 - accuracy: 0.9796 - val_loss: 0.3246 - val_accuracy: 0.9832

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 23s 1s/step - loss: 0.3243 - accuracy: 0.9851 - val_loss: 0.3422 - val_accuracy: 0.9832

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1618 - accuracy: 0.9879 - val_loss: 0.3661 - val_accuracy: 0.9832

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1231 - accuracy: 0.9916 - val_loss: 0.3756 - val_accuracy: 0.9832

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0604 - accuracy: 0.9944 - val_loss: 0.3857 - val_accuracy: 0.9832

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1405 - accuracy: 0.9916 - val_loss: 0.3924 - val_accuracy: 0.9832

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2116 - accuracy: 0.9879 - val_loss: 0.3895 - val_accuracy: 0.9832

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 22s 1s/step - loss: 0.1087 - accuracy: 0.9898 - val_loss: 0.4006 - val_accuracy: 0.9832

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0951 - accuracy: 0.9916 - val_loss: 0.4045 - val_accuracy: 0.9832

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0314 - accuracy: 0.9935 - val_loss: 0.4001 - val_accuracy: 0.9832

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1348 - accuracy: 0.9907 - val_loss: 0.3949 - val_accuracy: 0.9832

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2365 - accuracy: 0.9842 - val_loss: 0.4080 - val_accuracy: 0.9832

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1202 - accuracy: 0.9861 - val_loss: 0.4258 - val_accuracy: 0.9832

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0776 - accuracy: 0.9935 - val_loss: 0.4292 - val_accuracy: 0.9832

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1037 - accuracy: 0.9907 - val_loss: 0.4329 - val_accuracy: 0.9832

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1415 - accuracy: 0.9889 - val_loss: 0.4363 - val_accuracy: 0.9832

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1769 - accuracy: 0.9851 - val_loss: 0.4191 - val_accuracy: 0.9832

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0967 - accuracy: 0.9898 - val_loss: 0.4065 - val_accuracy: 0.9832

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 22s 1s/step - loss: 0.1110 - accuracy: 0.9898 - val_loss: 0.4059 - val_accuracy: 0.9832

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1394 - accuracy: 0.9898 - val_loss: 0.4092 - val_accuracy: 0.9832

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1918 - accuracy: 0.9907 - val_loss: 0.4128 - val_accuracy: 0.9832

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1071 - accuracy: 0.9898 - val_loss: 0.4090 - val_accuracy: 0.9832

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1369 - accuracy: 0.9935 - val_loss: 0.3943 - val_accuracy: 0.9832

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0725 - accuracy: 0.9916 - val_loss: 0.3993 - val_accuracy: 0.9832

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0961 - accuracy: 0.9907 - val_loss: 0.4160 - val_accuracy: 0.9832

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0990 - accuracy: 0.9935 - val_loss: 0.4216 - val_accuracy: 0.9832

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1327 - accuracy: 0.9926 - val_loss: 0.4249 - val_accuracy: 0.9832

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1648 - accuracy: 0.9898 - val_loss: 0.4263 - val_accuracy: 0.9832

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0837 - accuracy: 0.9926 - val_loss: 0.4487 - val_accuracy: 0.9832

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1233 - accuracy: 0.9916 - val_loss: 0.4587 - val_accuracy: 0.9832

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2743 - accuracy: 0.9842 - val_loss: 0.4634 - val_accuracy: 0.9832

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1626 - accuracy: 0.9889 - val_loss: 0.4657 - val_accuracy: 0.9832

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0976 - accuracy: 0.9898 - val_loss: 0.4618 - val_accuracy: 0.9832

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0855 - accuracy: 0.9935 - val_loss: 0.4613 - val_accuracy: 0.9832

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0885 - accuracy: 0.9907 - val_loss: 0.4407 - val_accuracy: 0.9832

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 22s 1s/step - loss: 0.0638 - accuracy: 0.9935 - val_loss: 0.4290 - val_accuracy: 0.9832

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0972 - accuracy: 0.9926 - val_loss: 0.4287 - val_accuracy: 0.9832

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0716 - accuracy: 0.9944 - val_loss: 0.4283 - val_accuracy: 0.9832

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1270 - accuracy: 0.9898 - val_loss: 0.4264 - val_accuracy: 0.9832

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1566 - accuracy: 0.9879 - val_loss: 0.4227 - val_accuracy: 0.9832

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1103 - accuracy: 0.9907 - val_loss: 0.4384 - val_accuracy: 0.9832

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0274 - accuracy: 0.9935 - val_loss: 0.4469 - val_accuracy: 0.9832

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1026 - accuracy: 0.9935 - val_loss: 0.4441 - val_accuracy: 0.9832

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1254 - accuracy: 0.9916 - val_loss: 0.4426 - val_accuracy: 0.9832

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1312 - accuracy: 0.9898 - val_loss: 0.4513 - val_accuracy: 0.9832

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0528 - accuracy: 0.9954 - val_loss: 0.4489 - val_accuracy: 0.9832

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/99.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.9882 - accuracy: 0.9722

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9881877303123474, 0.9721707105636597]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9721706864564007
                  precision    recall  f1-score   support

               0       0.97      0.93      0.95       158
               1       0.97      0.99      0.98       381

        accuracy                           0.97       539
       macro avg       0.97      0.96      0.97       539
    weighted avg       0.97      0.97      0.97       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/100.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/101.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/102.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.93      0.95       158
               1       0.97      0.99      0.98       381

        accuracy                           0.97       539
       macro avg       0.97      0.96      0.97       539
    weighted avg       0.97      0.97      0.97       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.93      0.99      0.95      0.96      0.92       158
              1       0.97      0.99      0.93      0.98      0.96      0.93       381

    avg / total       0.97      0.97      0.95      0.97      0.96      0.92       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
