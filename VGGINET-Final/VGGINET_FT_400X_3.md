<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "400X"
trainable_blocks = ["block4"]
irun = 3
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_400X-BREAKHIS-Dataset-60-10-30-VGGINet/3'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/400X/3'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.1245 - accuracy: 0.5938WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 21s 3s/step - loss: 4.5717 - accuracy: 0.7003 - val_loss: 4.4942 - val_accuracy: 0.8820
    Epoch 2/100
    8/8 [==============================] - 13s 2s/step - loss: 2.6778 - accuracy: 0.8198 - val_loss: 8.2837 - val_accuracy: 0.8199
    Epoch 3/100
    8/8 [==============================] - 14s 2s/step - loss: 2.0010 - accuracy: 0.8435 - val_loss: 1.7214 - val_accuracy: 0.9379
    Epoch 4/100
    8/8 [==============================] - 13s 2s/step - loss: 1.4536 - accuracy: 0.8723 - val_loss: 1.8785 - val_accuracy: 0.9193
    Epoch 5/100
    8/8 [==============================] - 11s 1s/step - loss: 1.3983 - accuracy: 0.8826 - val_loss: 2.2566 - val_accuracy: 0.9193
    Epoch 6/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2472 - accuracy: 0.8795 - val_loss: 1.9607 - val_accuracy: 0.8820
    Epoch 7/100
    8/8 [==============================] - 14s 2s/step - loss: 1.2585 - accuracy: 0.8764 - val_loss: 3.8162 - val_accuracy: 0.8634
    Epoch 8/100
    8/8 [==============================] - 11s 1s/step - loss: 1.0891 - accuracy: 0.9083 - val_loss: 2.3405 - val_accuracy: 0.8758
    Epoch 9/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0061 - accuracy: 0.8929 - val_loss: 1.7959 - val_accuracy: 0.9006
    Epoch 10/100
    8/8 [==============================] - 16s 2s/step - loss: 1.1216 - accuracy: 0.8980 - val_loss: 3.6897 - val_accuracy: 0.7702
    Epoch 11/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0251 - accuracy: 0.8960 - val_loss: 1.7812 - val_accuracy: 0.9317
    Epoch 12/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8394 - accuracy: 0.9104 - val_loss: 1.4704 - val_accuracy: 0.9068
    Epoch 13/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8983 - accuracy: 0.9083 - val_loss: 1.6440 - val_accuracy: 0.9068
    Epoch 14/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0872 - accuracy: 0.9063 - val_loss: 2.2702 - val_accuracy: 0.8696
    Epoch 15/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9185 - accuracy: 0.9114 - val_loss: 1.6838 - val_accuracy: 0.9317
    Epoch 16/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1987 - accuracy: 0.9042 - val_loss: 1.3641 - val_accuracy: 0.9379
    Epoch 17/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7936 - accuracy: 0.9289 - val_loss: 1.5057 - val_accuracy: 0.9317
    Epoch 18/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7701 - accuracy: 0.9176 - val_loss: 1.3199 - val_accuracy: 0.9193
    Epoch 19/100
    8/8 [==============================] - 14s 2s/step - loss: 0.9500 - accuracy: 0.9228 - val_loss: 1.2575 - val_accuracy: 0.9006
    Epoch 20/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7830 - accuracy: 0.9207 - val_loss: 1.7940 - val_accuracy: 0.9130
    Epoch 21/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7548 - accuracy: 0.9258 - val_loss: 1.2848 - val_accuracy: 0.9317
    Epoch 22/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7288 - accuracy: 0.9320 - val_loss: 1.4469 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 16s 2s/step - loss: 0.7037 - accuracy: 0.9382 - val_loss: 1.4721 - val_accuracy: 0.9006
    Epoch 24/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7109 - accuracy: 0.9320 - val_loss: 1.3764 - val_accuracy: 0.9068
    Epoch 25/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8269 - accuracy: 0.9289 - val_loss: 1.1690 - val_accuracy: 0.9317
    Epoch 26/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1005 - accuracy: 0.9197 - val_loss: 1.6110 - val_accuracy: 0.9255
    Epoch 27/100
    8/8 [==============================] - 14s 2s/step - loss: 1.0137 - accuracy: 0.9238 - val_loss: 2.6686 - val_accuracy: 0.8447
    Epoch 28/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9942 - accuracy: 0.9217 - val_loss: 1.7386 - val_accuracy: 0.8820
    Epoch 29/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1177 - accuracy: 0.9300 - val_loss: 1.5622 - val_accuracy: 0.8820
    Epoch 30/100
    8/8 [==============================] - 15s 2s/step - loss: 0.6362 - accuracy: 0.9506 - val_loss: 3.4239 - val_accuracy: 0.9006
    Epoch 31/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7149 - accuracy: 0.9361 - val_loss: 1.9360 - val_accuracy: 0.9255
    Epoch 32/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9125 - accuracy: 0.9228 - val_loss: 2.0082 - val_accuracy: 0.9130
    Epoch 33/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5524 - accuracy: 0.9506 - val_loss: 1.2875 - val_accuracy: 0.9379
    Epoch 34/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7867 - accuracy: 0.9351 - val_loss: 1.1420 - val_accuracy: 0.9317
    Epoch 35/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6594 - accuracy: 0.9464 - val_loss: 2.2647 - val_accuracy: 0.9130
    Epoch 36/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7683 - accuracy: 0.9434 - val_loss: 2.1214 - val_accuracy: 0.9006
    Epoch 37/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6053 - accuracy: 0.9506 - val_loss: 1.6741 - val_accuracy: 0.9130
    Epoch 38/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5263 - accuracy: 0.9557 - val_loss: 1.2930 - val_accuracy: 0.9130
    Epoch 39/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6732 - accuracy: 0.9567 - val_loss: 1.5767 - val_accuracy: 0.9193
    Epoch 40/100
    8/8 [==============================] - 15s 2s/step - loss: 0.6253 - accuracy: 0.9526 - val_loss: 2.3916 - val_accuracy: 0.9193
    Epoch 41/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5974 - accuracy: 0.9506 - val_loss: 0.9463 - val_accuracy: 0.9441
    Epoch 42/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4811 - accuracy: 0.9526 - val_loss: 1.1468 - val_accuracy: 0.9130
    Epoch 43/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5420 - accuracy: 0.9444 - val_loss: 1.8441 - val_accuracy: 0.9130
    Epoch 44/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8454 - accuracy: 0.9382 - val_loss: 1.7592 - val_accuracy: 0.9193
    Epoch 45/100
    8/8 [==============================] - 13s 2s/step - loss: 0.6032 - accuracy: 0.9567 - val_loss: 2.4338 - val_accuracy: 0.9130
    Epoch 46/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7071 - accuracy: 0.9475 - val_loss: 1.3076 - val_accuracy: 0.9317
    Epoch 47/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4685 - accuracy: 0.9547 - val_loss: 2.2056 - val_accuracy: 0.9006
    Epoch 48/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6163 - accuracy: 0.9567 - val_loss: 1.0895 - val_accuracy: 0.9379
    Epoch 49/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6039 - accuracy: 0.9516 - val_loss: 1.6006 - val_accuracy: 0.9255
    Epoch 50/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5292 - accuracy: 0.9547 - val_loss: 1.6732 - val_accuracy: 0.9193
    Epoch 51/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5968 - accuracy: 0.9547 - val_loss: 1.3774 - val_accuracy: 0.9255
    Epoch 52/100
    8/8 [==============================] - 13s 2s/step - loss: 0.3562 - accuracy: 0.9640 - val_loss: 1.4922 - val_accuracy: 0.9130
    Epoch 53/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4231 - accuracy: 0.9567 - val_loss: 1.8628 - val_accuracy: 0.9006
    Epoch 54/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4958 - accuracy: 0.9660 - val_loss: 1.6767 - val_accuracy: 0.8944
    Epoch 55/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4283 - accuracy: 0.9712 - val_loss: 3.8737 - val_accuracy: 0.9068
    Epoch 56/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5718 - accuracy: 0.9609 - val_loss: 2.7762 - val_accuracy: 0.8944
    Epoch 57/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5023 - accuracy: 0.9598 - val_loss: 1.2099 - val_accuracy: 0.9317
    Epoch 58/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5621 - accuracy: 0.9640 - val_loss: 1.7571 - val_accuracy: 0.9255
    Epoch 59/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4867 - accuracy: 0.9619 - val_loss: 2.4058 - val_accuracy: 0.9130
    Epoch 60/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4287 - accuracy: 0.9681 - val_loss: 2.3467 - val_accuracy: 0.9068
    Epoch 61/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5681 - accuracy: 0.9619 - val_loss: 1.8202 - val_accuracy: 0.9255
    Epoch 62/100
    8/8 [==============================] - 14s 2s/step - loss: 0.7283 - accuracy: 0.9434 - val_loss: 1.9227 - val_accuracy: 0.9379
    Epoch 63/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5888 - accuracy: 0.9681 - val_loss: 2.1393 - val_accuracy: 0.9130
    Epoch 64/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6358 - accuracy: 0.9547 - val_loss: 2.4883 - val_accuracy: 0.9130
    Epoch 65/100
    8/8 [==============================] - 14s 2s/step - loss: 0.5227 - accuracy: 0.9640 - val_loss: 2.3840 - val_accuracy: 0.9379
    Epoch 66/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4080 - accuracy: 0.9712 - val_loss: 2.6279 - val_accuracy: 0.9441
    Epoch 67/100
    8/8 [==============================] - 13s 2s/step - loss: 0.5413 - accuracy: 0.9598 - val_loss: 2.0721 - val_accuracy: 0.9317
    Epoch 68/100
    8/8 [==============================] - 13s 2s/step - loss: 0.4600 - accuracy: 0.9670 - val_loss: 2.5510 - val_accuracy: 0.9255
    Epoch 69/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3289 - accuracy: 0.9712 - val_loss: 3.0827 - val_accuracy: 0.9255
    Epoch 70/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4253 - accuracy: 0.9660 - val_loss: 2.5360 - val_accuracy: 0.9317
    Epoch 71/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3461 - accuracy: 0.9773 - val_loss: 3.6705 - val_accuracy: 0.9193
    Epoch 72/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3089 - accuracy: 0.9773 - val_loss: 3.9625 - val_accuracy: 0.8944
    Epoch 73/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3069 - accuracy: 0.9763 - val_loss: 2.4486 - val_accuracy: 0.9503
    Epoch 74/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3537 - accuracy: 0.9722 - val_loss: 2.6211 - val_accuracy: 0.9379
    Epoch 75/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4552 - accuracy: 0.9712 - val_loss: 3.3845 - val_accuracy: 0.9130
    Epoch 76/100
    8/8 [==============================] - 17s 2s/step - loss: 0.4473 - accuracy: 0.9743 - val_loss: 2.6177 - val_accuracy: 0.9255
    Epoch 77/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3480 - accuracy: 0.9650 - val_loss: 2.3440 - val_accuracy: 0.9441
    Epoch 78/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3217 - accuracy: 0.9794 - val_loss: 4.1698 - val_accuracy: 0.9193
    Epoch 79/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4566 - accuracy: 0.9681 - val_loss: 2.9014 - val_accuracy: 0.9379
    Epoch 80/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3615 - accuracy: 0.9670 - val_loss: 2.0918 - val_accuracy: 0.9441
    Epoch 81/100
    8/8 [==============================] - 15s 2s/step - loss: 0.4710 - accuracy: 0.9722 - val_loss: 2.0733 - val_accuracy: 0.9255
    Epoch 82/100
    8/8 [==============================] - 15s 2s/step - loss: 0.4484 - accuracy: 0.9732 - val_loss: 2.7169 - val_accuracy: 0.9130
    Epoch 83/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4063 - accuracy: 0.9722 - val_loss: 2.2490 - val_accuracy: 0.9193
    Epoch 84/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4816 - accuracy: 0.9732 - val_loss: 2.3305 - val_accuracy: 0.9255
    Epoch 85/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3810 - accuracy: 0.9784 - val_loss: 2.7917 - val_accuracy: 0.9317
    Epoch 86/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4697 - accuracy: 0.9753 - val_loss: 2.5008 - val_accuracy: 0.9379
    Epoch 87/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3089 - accuracy: 0.9650 - val_loss: 2.5321 - val_accuracy: 0.9441
    Epoch 88/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3555 - accuracy: 0.9773 - val_loss: 1.2786 - val_accuracy: 0.9441
    Epoch 89/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4273 - accuracy: 0.9753 - val_loss: 1.2930 - val_accuracy: 0.9441
    Epoch 90/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3204 - accuracy: 0.9763 - val_loss: 2.0831 - val_accuracy: 0.9317
    Epoch 91/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2861 - accuracy: 0.9784 - val_loss: 1.9451 - val_accuracy: 0.9441
    Epoch 92/100
    8/8 [==============================] - 15s 2s/step - loss: 0.4433 - accuracy: 0.9650 - val_loss: 2.5608 - val_accuracy: 0.9441
    Epoch 93/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3906 - accuracy: 0.9722 - val_loss: 4.6119 - val_accuracy: 0.9255
    Epoch 94/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3103 - accuracy: 0.9794 - val_loss: 3.4805 - val_accuracy: 0.9193
    Epoch 95/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6000 - accuracy: 0.9640 - val_loss: 1.3664 - val_accuracy: 0.9255
    Epoch 96/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2260 - accuracy: 0.9825 - val_loss: 2.4018 - val_accuracy: 0.9193
    Epoch 97/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2593 - accuracy: 0.9835 - val_loss: 2.8097 - val_accuracy: 0.9503
    Epoch 98/100
    8/8 [==============================] - 14s 2s/step - loss: 0.4654 - accuracy: 0.9722 - val_loss: 2.5225 - val_accuracy: 0.9441
    Epoch 99/100
    8/8 [==============================] - 14s 2s/step - loss: 0.2724 - accuracy: 0.9815 - val_loss: 1.9615 - val_accuracy: 0.9441
    Epoch 100/100
    8/8 [==============================] - 14s 2s/step - loss: 0.3300 - accuracy: 0.9804 - val_loss: 1.5718 - val_accuracy: 0.9379

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/37.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 13s 3s/step - loss: 2.5943 - accuracy: 0.9365

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.5943267345428467, 0.936475396156311]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/38.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    16/16 [==============================] - 19s 1s/step - loss: 0.3690 - accuracy: 0.9743 - val_loss: 1.5705 - val_accuracy: 0.9379

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    16/16 [==============================] - 20s 1s/step - loss: 0.4633 - accuracy: 0.9670 - val_loss: 1.5693 - val_accuracy: 0.9441

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    16/16 [==============================] - 19s 1s/step - loss: 0.3338 - accuracy: 0.9794 - val_loss: 1.5857 - val_accuracy: 0.9441

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2821 - accuracy: 0.9784 - val_loss: 1.5786 - val_accuracy: 0.9441

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2849 - accuracy: 0.9856 - val_loss: 1.5802 - val_accuracy: 0.9441

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3063 - accuracy: 0.9763 - val_loss: 1.5882 - val_accuracy: 0.9441

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1307 - accuracy: 0.9835 - val_loss: 1.5872 - val_accuracy: 0.9441

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    16/16 [==============================] - 20s 1s/step - loss: 0.4993 - accuracy: 0.9712 - val_loss: 1.5967 - val_accuracy: 0.9441

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3911 - accuracy: 0.9743 - val_loss: 1.6056 - val_accuracy: 0.9441

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3403 - accuracy: 0.9784 - val_loss: 1.6099 - val_accuracy: 0.9441

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3939 - accuracy: 0.9753 - val_loss: 1.6201 - val_accuracy: 0.9441

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2820 - accuracy: 0.9794 - val_loss: 1.6085 - val_accuracy: 0.9441

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2629 - accuracy: 0.9773 - val_loss: 1.6249 - val_accuracy: 0.9441

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3024 - accuracy: 0.9804 - val_loss: 1.6406 - val_accuracy: 0.9441

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2144 - accuracy: 0.9835 - val_loss: 1.6459 - val_accuracy: 0.9441

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2138 - accuracy: 0.9835 - val_loss: 1.6405 - val_accuracy: 0.9503

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2950 - accuracy: 0.9825 - val_loss: 1.6446 - val_accuracy: 0.9503

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1618 - accuracy: 0.9825 - val_loss: 1.6413 - val_accuracy: 0.9503

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2613 - accuracy: 0.9784 - val_loss: 1.6522 - val_accuracy: 0.9503

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    16/16 [==============================] - 20s 1s/step - loss: 0.1895 - accuracy: 0.9876 - val_loss: 1.6597 - val_accuracy: 0.9503

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4767 - accuracy: 0.9732 - val_loss: 1.6558 - val_accuracy: 0.9503

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3585 - accuracy: 0.9722 - val_loss: 1.6369 - val_accuracy: 0.9503

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2284 - accuracy: 0.9856 - val_loss: 1.6455 - val_accuracy: 0.9503

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2211 - accuracy: 0.9825 - val_loss: 1.6370 - val_accuracy: 0.9503

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2011 - accuracy: 0.9773 - val_loss: 1.6386 - val_accuracy: 0.9503

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4331 - accuracy: 0.9743 - val_loss: 1.6378 - val_accuracy: 0.9503

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2927 - accuracy: 0.9784 - val_loss: 1.6232 - val_accuracy: 0.9441

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1302 - accuracy: 0.9846 - val_loss: 1.6274 - val_accuracy: 0.9441

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3061 - accuracy: 0.9784 - val_loss: 1.6259 - val_accuracy: 0.9441

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1758 - accuracy: 0.9835 - val_loss: 1.6245 - val_accuracy: 0.9441

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2360 - accuracy: 0.9815 - val_loss: 1.6170 - val_accuracy: 0.9441

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2420 - accuracy: 0.9856 - val_loss: 1.6265 - val_accuracy: 0.9441

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2588 - accuracy: 0.9773 - val_loss: 1.6218 - val_accuracy: 0.9441

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2476 - accuracy: 0.9815 - val_loss: 1.6075 - val_accuracy: 0.9441

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1575 - accuracy: 0.9804 - val_loss: 1.6079 - val_accuracy: 0.9441

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1496 - accuracy: 0.9866 - val_loss: 1.6113 - val_accuracy: 0.9441

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3337 - accuracy: 0.9773 - val_loss: 1.6236 - val_accuracy: 0.9441

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1568 - accuracy: 0.9907 - val_loss: 1.6289 - val_accuracy: 0.9441

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    16/16 [==============================] - 20s 1s/step - loss: 0.3649 - accuracy: 0.9773 - val_loss: 1.6383 - val_accuracy: 0.9441

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1597 - accuracy: 0.9876 - val_loss: 1.6442 - val_accuracy: 0.9441

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    16/16 [==============================] - 19s 1s/step - loss: 0.2000 - accuracy: 0.9866 - val_loss: 1.6442 - val_accuracy: 0.9441

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2867 - accuracy: 0.9804 - val_loss: 1.6289 - val_accuracy: 0.9441

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2729 - accuracy: 0.9773 - val_loss: 1.6353 - val_accuracy: 0.9441

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2291 - accuracy: 0.9825 - val_loss: 1.6448 - val_accuracy: 0.9441

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    16/16 [==============================] - 20s 1s/step - loss: 0.2257 - accuracy: 0.9825 - val_loss: 1.6454 - val_accuracy: 0.9441

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2624 - accuracy: 0.9794 - val_loss: 1.6287 - val_accuracy: 0.9441

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    16/16 [==============================] - 20s 1s/step - loss: 0.1136 - accuracy: 0.9876 - val_loss: 1.6129 - val_accuracy: 0.9441

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    16/16 [==============================] - 19s 1s/step - loss: 0.1976 - accuracy: 0.9835 - val_loss: 1.6026 - val_accuracy: 0.9441

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2224 - accuracy: 0.9835 - val_loss: 1.6198 - val_accuracy: 0.9441

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1387 - accuracy: 0.9825 - val_loss: 1.6285 - val_accuracy: 0.9503

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/39.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 5s 1s/step - loss: 2.0692 - accuracy: 0.9426

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.0692458152770996, 0.9426229596138]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9426229508196722
                  precision    recall  f1-score   support

               0       0.91      0.90      0.90       148
               1       0.96      0.96      0.96       340

        accuracy                           0.94       488
       macro avg       0.93      0.93      0.93       488
    weighted avg       0.94      0.94      0.94       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/40.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/41.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/42.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.91      0.90      0.90       148
               1       0.96      0.96      0.96       340

        accuracy                           0.94       488
       macro avg       0.93      0.93      0.93       488
    weighted avg       0.94      0.94      0.94       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.91      0.90      0.96      0.90      0.93      0.86       148
              1       0.96      0.96      0.90      0.96      0.93      0.87       340

    avg / total       0.94      0.94      0.92      0.94      0.93      0.87       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
