<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "200X"
trainable_blocks = ["block1", "block2", "block3", "block4"]
irun = 4
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_200X-BREAKHIS-Dataset-60-10-30-VGGINet/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/200X/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.2227 - accuracy: 0.5156WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 24s 3s/step - loss: 4.1088 - accuracy: 0.7516 - val_loss: 4.9003 - val_accuracy: 0.9116
    Epoch 2/100
    9/9 [==============================] - 17s 2s/step - loss: 2.0772 - accuracy: 0.8721 - val_loss: 24.8192 - val_accuracy: 0.7348
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 1.5732 - accuracy: 0.8740 - val_loss: 2.0948 - val_accuracy: 0.9006
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1957 - accuracy: 0.9006 - val_loss: 3.8735 - val_accuracy: 0.8453
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9533 - accuracy: 0.9117 - val_loss: 1.8954 - val_accuracy: 0.9006
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1336 - accuracy: 0.9034 - val_loss: 2.5492 - val_accuracy: 0.8619
    Epoch 7/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8137 - accuracy: 0.9154 - val_loss: 0.7171 - val_accuracy: 0.9558
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8952 - accuracy: 0.9172 - val_loss: 1.0321 - val_accuracy: 0.9282
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7672 - accuracy: 0.9200 - val_loss: 0.7381 - val_accuracy: 0.9448
    Epoch 10/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7284 - accuracy: 0.9282 - val_loss: 1.4742 - val_accuracy: 0.9116
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8441 - accuracy: 0.9190 - val_loss: 0.7825 - val_accuracy: 0.9558
    Epoch 12/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6468 - accuracy: 0.9338 - val_loss: 3.2147 - val_accuracy: 0.8564
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6855 - accuracy: 0.9328 - val_loss: 0.6447 - val_accuracy: 0.9337
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8051 - accuracy: 0.9356 - val_loss: 0.7495 - val_accuracy: 0.9503
    Epoch 15/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7026 - accuracy: 0.9411 - val_loss: 1.0636 - val_accuracy: 0.9171
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7454 - accuracy: 0.9282 - val_loss: 1.0199 - val_accuracy: 0.9061
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6499 - accuracy: 0.9365 - val_loss: 0.7734 - val_accuracy: 0.9392
    Epoch 18/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7120 - accuracy: 0.9411 - val_loss: 2.5008 - val_accuracy: 0.9171
    Epoch 19/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0688 - accuracy: 0.9200 - val_loss: 1.0271 - val_accuracy: 0.9282
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7514 - accuracy: 0.9485 - val_loss: 0.8630 - val_accuracy: 0.9282
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6493 - accuracy: 0.9448 - val_loss: 2.3166 - val_accuracy: 0.8950
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6080 - accuracy: 0.9485 - val_loss: 1.1128 - val_accuracy: 0.9337
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6384 - accuracy: 0.9448 - val_loss: 0.4897 - val_accuracy: 0.9558
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6099 - accuracy: 0.9466 - val_loss: 0.6531 - val_accuracy: 0.9558
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5883 - accuracy: 0.9430 - val_loss: 1.5333 - val_accuracy: 0.8950
    Epoch 26/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5413 - accuracy: 0.9595 - val_loss: 0.8325 - val_accuracy: 0.9613
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3802 - accuracy: 0.9632 - val_loss: 0.6576 - val_accuracy: 0.9558
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4271 - accuracy: 0.9623 - val_loss: 1.3367 - val_accuracy: 0.9116
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4960 - accuracy: 0.9586 - val_loss: 0.5441 - val_accuracy: 0.9669
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5230 - accuracy: 0.9595 - val_loss: 0.8408 - val_accuracy: 0.9448
    Epoch 31/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4258 - accuracy: 0.9549 - val_loss: 1.1106 - val_accuracy: 0.9448
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6356 - accuracy: 0.9558 - val_loss: 0.5901 - val_accuracy: 0.9448
    Epoch 33/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4830 - accuracy: 0.9614 - val_loss: 1.1374 - val_accuracy: 0.9392
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6942 - accuracy: 0.9476 - val_loss: 0.8204 - val_accuracy: 0.9558
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3312 - accuracy: 0.9687 - val_loss: 0.8190 - val_accuracy: 0.9448
    Epoch 36/100
    9/9 [==============================] - 19s 2s/step - loss: 0.3391 - accuracy: 0.9715 - val_loss: 1.3927 - val_accuracy: 0.9558
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6635 - accuracy: 0.9577 - val_loss: 1.2242 - val_accuracy: 0.9448
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5451 - accuracy: 0.9614 - val_loss: 1.3847 - val_accuracy: 0.9503
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7133 - accuracy: 0.9595 - val_loss: 1.8614 - val_accuracy: 0.9282
    Epoch 40/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3974 - accuracy: 0.9678 - val_loss: 1.3304 - val_accuracy: 0.9282
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4481 - accuracy: 0.9650 - val_loss: 1.2010 - val_accuracy: 0.9448
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5311 - accuracy: 0.9540 - val_loss: 0.7176 - val_accuracy: 0.9613
    Epoch 43/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5573 - accuracy: 0.9687 - val_loss: 0.9626 - val_accuracy: 0.9558
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4167 - accuracy: 0.9752 - val_loss: 1.3215 - val_accuracy: 0.9337
    Epoch 45/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2454 - accuracy: 0.9788 - val_loss: 1.1668 - val_accuracy: 0.9503
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5474 - accuracy: 0.9724 - val_loss: 1.4158 - val_accuracy: 0.9392
    Epoch 47/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4557 - accuracy: 0.9715 - val_loss: 0.6747 - val_accuracy: 0.9558
    Epoch 48/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4383 - accuracy: 0.9733 - val_loss: 1.2304 - val_accuracy: 0.9392
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3344 - accuracy: 0.9733 - val_loss: 2.1005 - val_accuracy: 0.9392
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5338 - accuracy: 0.9614 - val_loss: 1.1980 - val_accuracy: 0.9503
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2623 - accuracy: 0.9752 - val_loss: 1.5001 - val_accuracy: 0.9392
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4911 - accuracy: 0.9669 - val_loss: 0.6439 - val_accuracy: 0.9669
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4260 - accuracy: 0.9733 - val_loss: 0.7749 - val_accuracy: 0.9669
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3841 - accuracy: 0.9752 - val_loss: 0.3821 - val_accuracy: 0.9779
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4947 - accuracy: 0.9678 - val_loss: 0.4136 - val_accuracy: 0.9669
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3877 - accuracy: 0.9779 - val_loss: 0.5591 - val_accuracy: 0.9613
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3829 - accuracy: 0.9770 - val_loss: 1.0600 - val_accuracy: 0.9448
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3337 - accuracy: 0.9798 - val_loss: 1.0124 - val_accuracy: 0.9448
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2756 - accuracy: 0.9798 - val_loss: 0.9202 - val_accuracy: 0.9448
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2246 - accuracy: 0.9816 - val_loss: 0.7418 - val_accuracy: 0.9669
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2546 - accuracy: 0.9798 - val_loss: 0.6382 - val_accuracy: 0.9724
    Epoch 62/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2455 - accuracy: 0.9788 - val_loss: 1.2624 - val_accuracy: 0.9558
    Epoch 63/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4282 - accuracy: 0.9752 - val_loss: 1.2511 - val_accuracy: 0.9558
    Epoch 64/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4777 - accuracy: 0.9724 - val_loss: 0.7770 - val_accuracy: 0.9613
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4147 - accuracy: 0.9687 - val_loss: 1.8349 - val_accuracy: 0.9448
    Epoch 66/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4137 - accuracy: 0.9678 - val_loss: 1.4221 - val_accuracy: 0.9558
    Epoch 67/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1456 - accuracy: 0.9862 - val_loss: 2.3360 - val_accuracy: 0.9448
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4101 - accuracy: 0.9715 - val_loss: 1.0672 - val_accuracy: 0.9448
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2446 - accuracy: 0.9834 - val_loss: 0.6920 - val_accuracy: 0.9779
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1367 - accuracy: 0.9862 - val_loss: 1.0084 - val_accuracy: 0.9669
    Epoch 71/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2540 - accuracy: 0.9825 - val_loss: 1.9454 - val_accuracy: 0.9392
    Epoch 72/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3426 - accuracy: 0.9752 - val_loss: 2.2943 - val_accuracy: 0.9227
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3510 - accuracy: 0.9798 - val_loss: 0.7713 - val_accuracy: 0.9558
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3398 - accuracy: 0.9761 - val_loss: 1.2635 - val_accuracy: 0.9558
    Epoch 75/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2706 - accuracy: 0.9844 - val_loss: 1.0394 - val_accuracy: 0.9503
    Epoch 76/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3286 - accuracy: 0.9798 - val_loss: 0.9102 - val_accuracy: 0.9669
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2164 - accuracy: 0.9853 - val_loss: 2.4417 - val_accuracy: 0.9337
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3880 - accuracy: 0.9752 - val_loss: 0.7016 - val_accuracy: 0.9558
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2947 - accuracy: 0.9742 - val_loss: 0.7483 - val_accuracy: 0.9613
    Epoch 80/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3637 - accuracy: 0.9798 - val_loss: 2.6120 - val_accuracy: 0.9227
    Epoch 81/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2483 - accuracy: 0.9752 - val_loss: 0.9810 - val_accuracy: 0.9448
    Epoch 82/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2301 - accuracy: 0.9752 - val_loss: 1.8975 - val_accuracy: 0.9392
    Epoch 83/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2790 - accuracy: 0.9862 - val_loss: 1.0053 - val_accuracy: 0.9503
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3963 - accuracy: 0.9807 - val_loss: 1.4205 - val_accuracy: 0.9558
    Epoch 85/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1783 - accuracy: 0.9834 - val_loss: 1.3428 - val_accuracy: 0.9503
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1555 - accuracy: 0.9871 - val_loss: 2.5314 - val_accuracy: 0.9282
    Epoch 87/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2158 - accuracy: 0.9816 - val_loss: 0.9836 - val_accuracy: 0.9448
    Epoch 88/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2912 - accuracy: 0.9825 - val_loss: 1.2671 - val_accuracy: 0.9337
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2802 - accuracy: 0.9816 - val_loss: 1.5635 - val_accuracy: 0.9392
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2464 - accuracy: 0.9788 - val_loss: 2.6753 - val_accuracy: 0.9392
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3374 - accuracy: 0.9807 - val_loss: 1.2410 - val_accuracy: 0.9503
    Epoch 92/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1704 - accuracy: 0.9871 - val_loss: 1.0052 - val_accuracy: 0.9613
    Epoch 93/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1567 - accuracy: 0.9890 - val_loss: 0.8087 - val_accuracy: 0.9613
    Epoch 94/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1543 - accuracy: 0.9917 - val_loss: 2.0596 - val_accuracy: 0.9724
    Epoch 95/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1787 - accuracy: 0.9871 - val_loss: 1.8186 - val_accuracy: 0.9558
    Epoch 96/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1782 - accuracy: 0.9880 - val_loss: 1.4340 - val_accuracy: 0.9613
    Epoch 97/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3461 - accuracy: 0.9807 - val_loss: 1.4129 - val_accuracy: 0.9613
    Epoch 98/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1170 - accuracy: 0.9908 - val_loss: 1.1084 - val_accuracy: 0.9613
    Epoch 99/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2297 - accuracy: 0.9853 - val_loss: 0.6742 - val_accuracy: 0.9724
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1658 - accuracy: 0.9908 - val_loss: 1.1511 - val_accuracy: 0.9779

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/7.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 9s 2s/step - loss: 0.9571 - accuracy: 0.9633

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9571134448051453, 0.963302731513977]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/8.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    17/17 [==============================] - 22s 1s/step - loss: 0.2677 - accuracy: 0.9880 - val_loss: 1.0280 - val_accuracy: 0.9779

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 25s 1s/step - loss: 0.2776 - accuracy: 0.9862 - val_loss: 0.9347 - val_accuracy: 0.9779

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2129 - accuracy: 0.9853 - val_loss: 0.8707 - val_accuracy: 0.9834

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1357 - accuracy: 0.9890 - val_loss: 0.8269 - val_accuracy: 0.9834

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2495 - accuracy: 0.9871 - val_loss: 0.7775 - val_accuracy: 0.9834

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1145 - accuracy: 0.9899 - val_loss: 0.7478 - val_accuracy: 0.9834

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 22s 1s/step - loss: 0.1628 - accuracy: 0.9871 - val_loss: 0.7160 - val_accuracy: 0.9834

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1194 - accuracy: 0.9917 - val_loss: 0.6886 - val_accuracy: 0.9834

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1896 - accuracy: 0.9844 - val_loss: 0.6607 - val_accuracy: 0.9834

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0956 - accuracy: 0.9890 - val_loss: 0.6423 - val_accuracy: 0.9779

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1222 - accuracy: 0.9899 - val_loss: 0.6355 - val_accuracy: 0.9779

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1267 - accuracy: 0.9899 - val_loss: 0.6258 - val_accuracy: 0.9779

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2388 - accuracy: 0.9880 - val_loss: 0.6187 - val_accuracy: 0.9724

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1980 - accuracy: 0.9862 - val_loss: 0.6130 - val_accuracy: 0.9724

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0880 - accuracy: 0.9908 - val_loss: 0.6041 - val_accuracy: 0.9724

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2101 - accuracy: 0.9862 - val_loss: 0.6034 - val_accuracy: 0.9779

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1769 - accuracy: 0.9871 - val_loss: 0.6056 - val_accuracy: 0.9779

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2652 - accuracy: 0.9834 - val_loss: 0.6048 - val_accuracy: 0.9779

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 21s 1s/step - loss: 0.2521 - accuracy: 0.9862 - val_loss: 0.5980 - val_accuracy: 0.9779

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1290 - accuracy: 0.9871 - val_loss: 0.5956 - val_accuracy: 0.9779

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 22s 1s/step - loss: 0.1810 - accuracy: 0.9899 - val_loss: 0.5883 - val_accuracy: 0.9779

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1414 - accuracy: 0.9917 - val_loss: 0.5877 - val_accuracy: 0.9779

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2695 - accuracy: 0.9862 - val_loss: 0.5916 - val_accuracy: 0.9779

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1629 - accuracy: 0.9890 - val_loss: 0.5950 - val_accuracy: 0.9779

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 25s 1s/step - loss: 0.2365 - accuracy: 0.9871 - val_loss: 0.5972 - val_accuracy: 0.9779

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1534 - accuracy: 0.9917 - val_loss: 0.6005 - val_accuracy: 0.9724

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1930 - accuracy: 0.9853 - val_loss: 0.5939 - val_accuracy: 0.9724

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1355 - accuracy: 0.9890 - val_loss: 0.5951 - val_accuracy: 0.9724

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1944 - accuracy: 0.9890 - val_loss: 0.5942 - val_accuracy: 0.9724

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1306 - accuracy: 0.9936 - val_loss: 0.5935 - val_accuracy: 0.9724

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0375 - accuracy: 0.9963 - val_loss: 0.5946 - val_accuracy: 0.9724

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1462 - accuracy: 0.9908 - val_loss: 0.5976 - val_accuracy: 0.9724

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1791 - accuracy: 0.9917 - val_loss: 0.6001 - val_accuracy: 0.9724

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 24s 1s/step - loss: 0.3379 - accuracy: 0.9853 - val_loss: 0.6012 - val_accuracy: 0.9724

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0825 - accuracy: 0.9908 - val_loss: 0.6011 - val_accuracy: 0.9724

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0516 - accuracy: 0.9926 - val_loss: 0.6073 - val_accuracy: 0.9724

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1766 - accuracy: 0.9853 - val_loss: 0.6136 - val_accuracy: 0.9724

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1771 - accuracy: 0.9853 - val_loss: 0.6075 - val_accuracy: 0.9724

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1110 - accuracy: 0.9908 - val_loss: 0.5994 - val_accuracy: 0.9724

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1223 - accuracy: 0.9890 - val_loss: 0.6020 - val_accuracy: 0.9724

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1792 - accuracy: 0.9862 - val_loss: 0.6030 - val_accuracy: 0.9724

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1541 - accuracy: 0.9890 - val_loss: 0.5994 - val_accuracy: 0.9724

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1360 - accuracy: 0.9899 - val_loss: 0.5927 - val_accuracy: 0.9724

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1311 - accuracy: 0.9890 - val_loss: 0.5873 - val_accuracy: 0.9724

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1810 - accuracy: 0.9926 - val_loss: 0.5865 - val_accuracy: 0.9724

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0811 - accuracy: 0.9954 - val_loss: 0.5864 - val_accuracy: 0.9724

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1597 - accuracy: 0.9853 - val_loss: 0.5905 - val_accuracy: 0.9724

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1390 - accuracy: 0.9890 - val_loss: 0.5957 - val_accuracy: 0.9724

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1187 - accuracy: 0.9871 - val_loss: 0.5954 - val_accuracy: 0.9724

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1168 - accuracy: 0.9899 - val_loss: 0.5919 - val_accuracy: 0.9724

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/9.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.7757 - accuracy: 0.9725

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7757061123847961, 0.9724770784378052]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9724770642201835
                  precision    recall  f1-score   support

               0       0.97      0.94      0.95       158
               1       0.97      0.99      0.98       387

        accuracy                           0.97       545
       macro avg       0.97      0.96      0.97       545
    weighted avg       0.97      0.97      0.97       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/10.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/11.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/12.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.94      0.95       158
               1       0.97      0.99      0.98       387

        accuracy                           0.97       545
       macro avg       0.97      0.96      0.97       545
    weighted avg       0.97      0.97      0.97       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.94      0.99      0.95      0.96      0.92       158
              1       0.97      0.99      0.94      0.98      0.96      0.93       387

    avg / total       0.97      0.97      0.95      0.97      0.96      0.93       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
