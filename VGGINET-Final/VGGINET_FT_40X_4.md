<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "40X"
trainable_blocks = ["block3", "block4"]
irun = 4
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_40X-BREAKHIS-Dataset-60-10-30-VGGINet/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/40X/4'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.2100 - accuracy: 0.4844WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 34s 4s/step - loss: 4.0886 - accuracy: 0.7344 - val_loss: 16.0888 - val_accuracy: 0.8212
    Epoch 2/100
    9/9 [==============================] - 16s 2s/step - loss: 2.9486 - accuracy: 0.8329 - val_loss: 6.7327 - val_accuracy: 0.8547
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 1.9257 - accuracy: 0.8719 - val_loss: 6.8913 - val_accuracy: 0.8436
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 1.2319 - accuracy: 0.8951 - val_loss: 3.8368 - val_accuracy: 0.8771
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9706 - accuracy: 0.9155 - val_loss: 3.6934 - val_accuracy: 0.8715
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7697 - accuracy: 0.9201 - val_loss: 1.5643 - val_accuracy: 0.9330
    Epoch 7/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7644 - accuracy: 0.9146 - val_loss: 1.5731 - val_accuracy: 0.9106
    Epoch 8/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8410 - accuracy: 0.9378 - val_loss: 1.6766 - val_accuracy: 0.9162
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8245 - accuracy: 0.9276 - val_loss: 1.1902 - val_accuracy: 0.9050
    Epoch 10/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7236 - accuracy: 0.9239 - val_loss: 1.5307 - val_accuracy: 0.9218
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6863 - accuracy: 0.9369 - val_loss: 0.9068 - val_accuracy: 0.9441
    Epoch 12/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6685 - accuracy: 0.9331 - val_loss: 2.0229 - val_accuracy: 0.9385
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5902 - accuracy: 0.9406 - val_loss: 1.0510 - val_accuracy: 0.9274
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8703 - accuracy: 0.9387 - val_loss: 2.5839 - val_accuracy: 0.9106
    Epoch 15/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5723 - accuracy: 0.9480 - val_loss: 1.8304 - val_accuracy: 0.9274
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8609 - accuracy: 0.9322 - val_loss: 1.9635 - val_accuracy: 0.9162
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5964 - accuracy: 0.9471 - val_loss: 1.9040 - val_accuracy: 0.9218
    Epoch 18/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8406 - accuracy: 0.9322 - val_loss: 1.7110 - val_accuracy: 0.9330
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5937 - accuracy: 0.9452 - val_loss: 2.1035 - val_accuracy: 0.9330
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7564 - accuracy: 0.9545 - val_loss: 1.7056 - val_accuracy: 0.9497
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6088 - accuracy: 0.9526 - val_loss: 0.9547 - val_accuracy: 0.9441
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6340 - accuracy: 0.9554 - val_loss: 3.0753 - val_accuracy: 0.8883
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5152 - accuracy: 0.9591 - val_loss: 1.2408 - val_accuracy: 0.9609
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3937 - accuracy: 0.9684 - val_loss: 1.0137 - val_accuracy: 0.9441
    Epoch 25/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6616 - accuracy: 0.9591 - val_loss: 0.9841 - val_accuracy: 0.9497
    Epoch 26/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5670 - accuracy: 0.9629 - val_loss: 1.7963 - val_accuracy: 0.9162
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3954 - accuracy: 0.9656 - val_loss: 1.2912 - val_accuracy: 0.9330
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3743 - accuracy: 0.9703 - val_loss: 0.9531 - val_accuracy: 0.9385
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3309 - accuracy: 0.9740 - val_loss: 0.8664 - val_accuracy: 0.9441
    Epoch 30/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4878 - accuracy: 0.9601 - val_loss: 2.2837 - val_accuracy: 0.9385
    Epoch 31/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4123 - accuracy: 0.9731 - val_loss: 1.0080 - val_accuracy: 0.9274
    Epoch 32/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4033 - accuracy: 0.9694 - val_loss: 1.1559 - val_accuracy: 0.9497
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5779 - accuracy: 0.9712 - val_loss: 0.9250 - val_accuracy: 0.9665
    Epoch 34/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3467 - accuracy: 0.9712 - val_loss: 1.6335 - val_accuracy: 0.9441
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4268 - accuracy: 0.9740 - val_loss: 2.1087 - val_accuracy: 0.9330
    Epoch 36/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3452 - accuracy: 0.9684 - val_loss: 1.2955 - val_accuracy: 0.9553
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3647 - accuracy: 0.9777 - val_loss: 1.8386 - val_accuracy: 0.9497
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3660 - accuracy: 0.9749 - val_loss: 1.2701 - val_accuracy: 0.9441
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3607 - accuracy: 0.9740 - val_loss: 1.8968 - val_accuracy: 0.9330
    Epoch 40/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5275 - accuracy: 0.9629 - val_loss: 1.3864 - val_accuracy: 0.9497
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3516 - accuracy: 0.9759 - val_loss: 1.9688 - val_accuracy: 0.9553
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5023 - accuracy: 0.9684 - val_loss: 2.9508 - val_accuracy: 0.9274
    Epoch 43/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4462 - accuracy: 0.9731 - val_loss: 2.8167 - val_accuracy: 0.9274
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3478 - accuracy: 0.9731 - val_loss: 2.4577 - val_accuracy: 0.9385
    Epoch 45/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4281 - accuracy: 0.9721 - val_loss: 3.4648 - val_accuracy: 0.9274
    Epoch 46/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2165 - accuracy: 0.9851 - val_loss: 1.9512 - val_accuracy: 0.9497
    Epoch 47/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2946 - accuracy: 0.9777 - val_loss: 1.6220 - val_accuracy: 0.9553
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2537 - accuracy: 0.9796 - val_loss: 1.3095 - val_accuracy: 0.9497
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2544 - accuracy: 0.9749 - val_loss: 1.3897 - val_accuracy: 0.9441
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2618 - accuracy: 0.9777 - val_loss: 1.5546 - val_accuracy: 0.9497
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3677 - accuracy: 0.9759 - val_loss: 1.4767 - val_accuracy: 0.9385
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3036 - accuracy: 0.9796 - val_loss: 1.5339 - val_accuracy: 0.9441
    Epoch 53/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4755 - accuracy: 0.9731 - val_loss: 1.2643 - val_accuracy: 0.9330
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2589 - accuracy: 0.9814 - val_loss: 1.2101 - val_accuracy: 0.9609
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3666 - accuracy: 0.9796 - val_loss: 1.0080 - val_accuracy: 0.9553
    Epoch 56/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4504 - accuracy: 0.9796 - val_loss: 1.3054 - val_accuracy: 0.9553
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2304 - accuracy: 0.9833 - val_loss: 1.0904 - val_accuracy: 0.9609
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3112 - accuracy: 0.9842 - val_loss: 1.4352 - val_accuracy: 0.9553
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3269 - accuracy: 0.9805 - val_loss: 1.7620 - val_accuracy: 0.9441
    Epoch 60/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3646 - accuracy: 0.9814 - val_loss: 3.2762 - val_accuracy: 0.9162
    Epoch 61/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2384 - accuracy: 0.9796 - val_loss: 1.1936 - val_accuracy: 0.9609
    Epoch 62/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3426 - accuracy: 0.9796 - val_loss: 2.0338 - val_accuracy: 0.9385
    Epoch 63/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3034 - accuracy: 0.9777 - val_loss: 2.1311 - val_accuracy: 0.9441
    Epoch 64/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2986 - accuracy: 0.9833 - val_loss: 1.9041 - val_accuracy: 0.9609
    Epoch 65/100
    9/9 [==============================] - 16s 2s/step - loss: 0.0440 - accuracy: 0.9935 - val_loss: 1.8937 - val_accuracy: 0.9497
    Epoch 66/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2489 - accuracy: 0.9824 - val_loss: 2.6304 - val_accuracy: 0.9441
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2032 - accuracy: 0.9907 - val_loss: 3.7330 - val_accuracy: 0.9330
    Epoch 68/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3184 - accuracy: 0.9786 - val_loss: 2.6879 - val_accuracy: 0.9441
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.0911 - accuracy: 0.9898 - val_loss: 1.8460 - val_accuracy: 0.9441
    Epoch 70/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1797 - accuracy: 0.9889 - val_loss: 1.4723 - val_accuracy: 0.9553
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1408 - accuracy: 0.9916 - val_loss: 2.3400 - val_accuracy: 0.9441
    Epoch 72/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2396 - accuracy: 0.9861 - val_loss: 1.8574 - val_accuracy: 0.9441
    Epoch 73/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1804 - accuracy: 0.9842 - val_loss: 2.2900 - val_accuracy: 0.9553
    Epoch 74/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2320 - accuracy: 0.9861 - val_loss: 2.7958 - val_accuracy: 0.9497
    Epoch 75/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2901 - accuracy: 0.9824 - val_loss: 1.9283 - val_accuracy: 0.9497
    Epoch 76/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2937 - accuracy: 0.9842 - val_loss: 1.7106 - val_accuracy: 0.9497
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3073 - accuracy: 0.9786 - val_loss: 2.4559 - val_accuracy: 0.9497
    Epoch 78/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2223 - accuracy: 0.9879 - val_loss: 3.5151 - val_accuracy: 0.9497
    Epoch 79/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1237 - accuracy: 0.9898 - val_loss: 3.2237 - val_accuracy: 0.9441
    Epoch 80/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2599 - accuracy: 0.9889 - val_loss: 2.8255 - val_accuracy: 0.9441
    Epoch 81/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2196 - accuracy: 0.9851 - val_loss: 2.4316 - val_accuracy: 0.9553
    Epoch 82/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1844 - accuracy: 0.9879 - val_loss: 1.7159 - val_accuracy: 0.9497
    Epoch 83/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2525 - accuracy: 0.9870 - val_loss: 1.2624 - val_accuracy: 0.9385
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1102 - accuracy: 0.9907 - val_loss: 1.8763 - val_accuracy: 0.9441
    Epoch 85/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1946 - accuracy: 0.9879 - val_loss: 2.3038 - val_accuracy: 0.9441
    Epoch 86/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1174 - accuracy: 0.9889 - val_loss: 3.5435 - val_accuracy: 0.9330
    Epoch 87/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1616 - accuracy: 0.9870 - val_loss: 3.4042 - val_accuracy: 0.9274
    Epoch 88/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1333 - accuracy: 0.9935 - val_loss: 3.8805 - val_accuracy: 0.9330
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1535 - accuracy: 0.9870 - val_loss: 2.4186 - val_accuracy: 0.9441
    Epoch 90/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1376 - accuracy: 0.9916 - val_loss: 2.4605 - val_accuracy: 0.9385
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1149 - accuracy: 0.9889 - val_loss: 2.0780 - val_accuracy: 0.9274
    Epoch 92/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2887 - accuracy: 0.9842 - val_loss: 1.2563 - val_accuracy: 0.9385
    Epoch 93/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2160 - accuracy: 0.9870 - val_loss: 1.8365 - val_accuracy: 0.9553
    Epoch 94/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2658 - accuracy: 0.9824 - val_loss: 2.3105 - val_accuracy: 0.9609
    Epoch 95/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2350 - accuracy: 0.9870 - val_loss: 2.7808 - val_accuracy: 0.9497
    Epoch 96/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2491 - accuracy: 0.9814 - val_loss: 2.6385 - val_accuracy: 0.9497
    Epoch 97/100
    9/9 [==============================] - 19s 2s/step - loss: 0.1818 - accuracy: 0.9889 - val_loss: 3.1705 - val_accuracy: 0.9385
    Epoch 98/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2102 - accuracy: 0.9870 - val_loss: 2.0260 - val_accuracy: 0.9385
    Epoch 99/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3445 - accuracy: 0.9824 - val_loss: 1.6477 - val_accuracy: 0.9553
    Epoch 100/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2184 - accuracy: 0.9851 - val_loss: 3.3150 - val_accuracy: 0.9441

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/115.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 25s 5s/step - loss: 2.0104 - accuracy: 0.9518

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.0104479789733887, 0.9517624974250793]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/116.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    17/17 [==============================] - 21s 1s/step - loss: 0.2889 - accuracy: 0.9833 - val_loss: 3.1763 - val_accuracy: 0.9441

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1510 - accuracy: 0.9879 - val_loss: 3.0326 - val_accuracy: 0.9497

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1300 - accuracy: 0.9879 - val_loss: 2.9292 - val_accuracy: 0.9497

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 22s 1s/step - loss: 0.3863 - accuracy: 0.9851 - val_loss: 2.8384 - val_accuracy: 0.9497

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2853 - accuracy: 0.9833 - val_loss: 2.7768 - val_accuracy: 0.9497

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2711 - accuracy: 0.9824 - val_loss: 2.6528 - val_accuracy: 0.9497

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2501 - accuracy: 0.9842 - val_loss: 2.5449 - val_accuracy: 0.9497

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2144 - accuracy: 0.9851 - val_loss: 2.4715 - val_accuracy: 0.9497

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2390 - accuracy: 0.9861 - val_loss: 2.4160 - val_accuracy: 0.9497

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1556 - accuracy: 0.9898 - val_loss: 2.3628 - val_accuracy: 0.9441

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1663 - accuracy: 0.9879 - val_loss: 2.3136 - val_accuracy: 0.9441

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2397 - accuracy: 0.9851 - val_loss: 2.2893 - val_accuracy: 0.9441

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1847 - accuracy: 0.9842 - val_loss: 2.2745 - val_accuracy: 0.9441

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1973 - accuracy: 0.9898 - val_loss: 2.2612 - val_accuracy: 0.9441

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2600 - accuracy: 0.9842 - val_loss: 2.2136 - val_accuracy: 0.9441

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1039 - accuracy: 0.9926 - val_loss: 2.2181 - val_accuracy: 0.9441

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2605 - accuracy: 0.9814 - val_loss: 2.1923 - val_accuracy: 0.9441

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0976 - accuracy: 0.9907 - val_loss: 2.1740 - val_accuracy: 0.9441

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1445 - accuracy: 0.9916 - val_loss: 2.1137 - val_accuracy: 0.9441

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1640 - accuracy: 0.9916 - val_loss: 2.0864 - val_accuracy: 0.9441

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1511 - accuracy: 0.9907 - val_loss: 2.0735 - val_accuracy: 0.9441

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1450 - accuracy: 0.9898 - val_loss: 2.0356 - val_accuracy: 0.9441

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2732 - accuracy: 0.9916 - val_loss: 2.0143 - val_accuracy: 0.9441

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1890 - accuracy: 0.9861 - val_loss: 2.0412 - val_accuracy: 0.9441

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2446 - accuracy: 0.9814 - val_loss: 2.0209 - val_accuracy: 0.9441

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2371 - accuracy: 0.9916 - val_loss: 1.9859 - val_accuracy: 0.9441

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1078 - accuracy: 0.9907 - val_loss: 1.9861 - val_accuracy: 0.9441

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1705 - accuracy: 0.9851 - val_loss: 2.0001 - val_accuracy: 0.9441

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2433 - accuracy: 0.9898 - val_loss: 1.9632 - val_accuracy: 0.9441

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0307 - accuracy: 0.9954 - val_loss: 1.9379 - val_accuracy: 0.9441

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1659 - accuracy: 0.9907 - val_loss: 1.9700 - val_accuracy: 0.9441

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1440 - accuracy: 0.9926 - val_loss: 1.9675 - val_accuracy: 0.9441

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1291 - accuracy: 0.9898 - val_loss: 2.0120 - val_accuracy: 0.9441

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1554 - accuracy: 0.9898 - val_loss: 2.0115 - val_accuracy: 0.9441

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0690 - accuracy: 0.9935 - val_loss: 1.9864 - val_accuracy: 0.9441

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1017 - accuracy: 0.9916 - val_loss: 1.9979 - val_accuracy: 0.9497

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2007 - accuracy: 0.9861 - val_loss: 1.9847 - val_accuracy: 0.9497

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1930 - accuracy: 0.9907 - val_loss: 1.9738 - val_accuracy: 0.9497

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0802 - accuracy: 0.9935 - val_loss: 1.9367 - val_accuracy: 0.9441

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0608 - accuracy: 0.9935 - val_loss: 1.9051 - val_accuracy: 0.9441

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1546 - accuracy: 0.9926 - val_loss: 1.9131 - val_accuracy: 0.9441

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 24s 1s/step - loss: 0.2232 - accuracy: 0.9870 - val_loss: 1.9156 - val_accuracy: 0.9441

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1576 - accuracy: 0.9926 - val_loss: 1.9084 - val_accuracy: 0.9441

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1220 - accuracy: 0.9926 - val_loss: 1.9331 - val_accuracy: 0.9441

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1379 - accuracy: 0.9926 - val_loss: 1.9178 - val_accuracy: 0.9441

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1833 - accuracy: 0.9907 - val_loss: 1.8705 - val_accuracy: 0.9497

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0987 - accuracy: 0.9935 - val_loss: 1.8527 - val_accuracy: 0.9497

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 22s 1s/step - loss: 0.1338 - accuracy: 0.9916 - val_loss: 1.8558 - val_accuracy: 0.9497

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0882 - accuracy: 0.9907 - val_loss: 1.8952 - val_accuracy: 0.9497

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 23s 1s/step - loss: 0.0957 - accuracy: 0.9944 - val_loss: 1.9278 - val_accuracy: 0.9497

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/117.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.9528 - accuracy: 0.9647

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9527931213378906, 0.9647495150566101]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9647495361781077
                  precision    recall  f1-score   support

               0       0.97      0.91      0.94       158
               1       0.96      0.99      0.98       381

        accuracy                           0.96       539
       macro avg       0.97      0.95      0.96       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/118.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/119.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/120.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.91      0.94       158
               1       0.96      0.99      0.98       381

        accuracy                           0.96       539
       macro avg       0.97      0.95      0.96       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.91      0.99      0.94      0.95      0.89       158
              1       0.96      0.99      0.91      0.98      0.95      0.91       381

    avg / total       0.96      0.96      0.93      0.96      0.95      0.90       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
