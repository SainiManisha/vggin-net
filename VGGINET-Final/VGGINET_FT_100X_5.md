<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered celltag_parameters">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
magnification_factor = ''
trainable_blocks = []
irun = 0
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered celltag_injected-parameters">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# Parameters
magnification_factor = "100X"
trainable_blocks = ["block1", "block2", "block3", "block4"]
irun = 5
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
irun = str(irun)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'FINETUNING_'+ magnification_factor +'-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './VGGINET-FT/%s/%s' % (name, irun)
!mkdir -p {path}

path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[4\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './VGGINET-FT/FINETUNING_100X-BREAKHIS-Dataset-60-10-30-VGGINet/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
tb_path = './tensorboard/' + magnification_factor + '/' + irun
tb_path
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[5\]:

</div>
<div class="output_text output_subarea output_execute_result">

    './tensorboard/100X/5'

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[6\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + magnification_factor + '/train'
val_path = '../../Splitted_' + magnification_factor + '/val'
test_path = '../../Splitted_' + magnification_factor + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3028 - accuracy: 0.4766WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 29s 3s/step - loss: 3.2481 - accuracy: 0.7580 - val_loss: 4.5065 - val_accuracy: 0.8717
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 1.7258 - accuracy: 0.8821 - val_loss: 5.4733 - val_accuracy: 0.8449
    Epoch 3/100
    9/9 [==============================] - 17s 2s/step - loss: 1.4809 - accuracy: 0.8821 - val_loss: 2.6416 - val_accuracy: 0.8449
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 1.7168 - accuracy: 0.8661 - val_loss: 1.5704 - val_accuracy: 0.9251
    Epoch 5/100
    9/9 [==============================] - 16s 2s/step - loss: 1.4173 - accuracy: 0.8892 - val_loss: 1.7552 - val_accuracy: 0.9198
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1641 - accuracy: 0.8989 - val_loss: 1.7584 - val_accuracy: 0.8930
    Epoch 7/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9546 - accuracy: 0.9078 - val_loss: 0.8176 - val_accuracy: 0.9519
    Epoch 8/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8228 - accuracy: 0.9211 - val_loss: 0.9246 - val_accuracy: 0.9358
    Epoch 9/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8179 - accuracy: 0.9167 - val_loss: 1.5274 - val_accuracy: 0.9037
    Epoch 10/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0318 - accuracy: 0.9096 - val_loss: 0.9589 - val_accuracy: 0.9198
    Epoch 11/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9776 - accuracy: 0.9122 - val_loss: 1.0234 - val_accuracy: 0.9251
    Epoch 12/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9521 - accuracy: 0.9202 - val_loss: 1.2668 - val_accuracy: 0.9305
    Epoch 13/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9746 - accuracy: 0.9211 - val_loss: 1.0873 - val_accuracy: 0.9358
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8184 - accuracy: 0.9309 - val_loss: 0.6415 - val_accuracy: 0.9412
    Epoch 15/100
    9/9 [==============================] - 18s 2s/step - loss: 0.8527 - accuracy: 0.9397 - val_loss: 0.5718 - val_accuracy: 0.9572
    Epoch 16/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0872 - accuracy: 0.9202 - val_loss: 0.7581 - val_accuracy: 0.9519
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8237 - accuracy: 0.9255 - val_loss: 1.1773 - val_accuracy: 0.9358
    Epoch 18/100
    9/9 [==============================] - 17s 2s/step - loss: 0.9329 - accuracy: 0.9282 - val_loss: 0.5320 - val_accuracy: 0.9465
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9472 - accuracy: 0.9255 - val_loss: 1.4204 - val_accuracy: 0.9305
    Epoch 20/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8658 - accuracy: 0.9238 - val_loss: 0.6900 - val_accuracy: 0.9519
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9401 - accuracy: 0.9300 - val_loss: 1.0635 - val_accuracy: 0.9412
    Epoch 22/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7297 - accuracy: 0.9379 - val_loss: 0.7078 - val_accuracy: 0.9358
    Epoch 23/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3645 - accuracy: 0.9681 - val_loss: 1.4271 - val_accuracy: 0.9358
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4902 - accuracy: 0.9566 - val_loss: 1.0636 - val_accuracy: 0.9251
    Epoch 25/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6828 - accuracy: 0.9486 - val_loss: 0.7594 - val_accuracy: 0.9519
    Epoch 26/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6261 - accuracy: 0.9468 - val_loss: 0.6780 - val_accuracy: 0.9519
    Epoch 27/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5664 - accuracy: 0.9486 - val_loss: 0.4590 - val_accuracy: 0.9626
    Epoch 28/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5313 - accuracy: 0.9610 - val_loss: 0.7036 - val_accuracy: 0.9412
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7527 - accuracy: 0.9495 - val_loss: 1.2274 - val_accuracy: 0.9465
    Epoch 30/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3764 - accuracy: 0.9672 - val_loss: 0.4381 - val_accuracy: 0.9679
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5114 - accuracy: 0.9619 - val_loss: 0.3071 - val_accuracy: 0.9840
    Epoch 32/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6547 - accuracy: 0.9539 - val_loss: 0.6076 - val_accuracy: 0.9626
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6723 - accuracy: 0.9619 - val_loss: 0.2253 - val_accuracy: 0.9679
    Epoch 34/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4686 - accuracy: 0.9645 - val_loss: 0.7269 - val_accuracy: 0.9626
    Epoch 35/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6630 - accuracy: 0.9566 - val_loss: 0.3456 - val_accuracy: 0.9626
    Epoch 36/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4281 - accuracy: 0.9663 - val_loss: 0.4190 - val_accuracy: 0.9519
    Epoch 37/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4960 - accuracy: 0.9690 - val_loss: 0.3672 - val_accuracy: 0.9679
    Epoch 38/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5191 - accuracy: 0.9583 - val_loss: 0.5687 - val_accuracy: 0.9786
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6832 - accuracy: 0.9566 - val_loss: 0.3485 - val_accuracy: 0.9733
    Epoch 40/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6107 - accuracy: 0.9592 - val_loss: 1.2873 - val_accuracy: 0.9251
    Epoch 41/100
    9/9 [==============================] - 17s 2s/step - loss: 0.5151 - accuracy: 0.9583 - val_loss: 0.1650 - val_accuracy: 0.9893
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5333 - accuracy: 0.9619 - val_loss: 0.2256 - val_accuracy: 0.9893
    Epoch 43/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4124 - accuracy: 0.9699 - val_loss: 0.2538 - val_accuracy: 0.9840
    Epoch 44/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4988 - accuracy: 0.9654 - val_loss: 0.4987 - val_accuracy: 0.9733
    Epoch 45/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4692 - accuracy: 0.9672 - val_loss: 0.4105 - val_accuracy: 0.9840
    Epoch 46/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5129 - accuracy: 0.9663 - val_loss: 0.8246 - val_accuracy: 0.9572
    Epoch 47/100
    9/9 [==============================] - 20s 2s/step - loss: 0.5034 - accuracy: 0.9645 - val_loss: 0.4816 - val_accuracy: 0.9786
    Epoch 48/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5887 - accuracy: 0.9574 - val_loss: 0.4536 - val_accuracy: 0.9733
    Epoch 49/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5698 - accuracy: 0.9699 - val_loss: 0.3036 - val_accuracy: 0.9786
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4826 - accuracy: 0.9619 - val_loss: 0.3633 - val_accuracy: 0.9733
    Epoch 51/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3759 - accuracy: 0.9725 - val_loss: 0.7842 - val_accuracy: 0.9465
    Epoch 52/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5331 - accuracy: 0.9734 - val_loss: 0.8455 - val_accuracy: 0.9572
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3481 - accuracy: 0.9725 - val_loss: 0.6791 - val_accuracy: 0.9679
    Epoch 54/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2298 - accuracy: 0.9796 - val_loss: 0.6629 - val_accuracy: 0.9786
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3892 - accuracy: 0.9690 - val_loss: 0.5788 - val_accuracy: 0.9786
    Epoch 56/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4774 - accuracy: 0.9619 - val_loss: 0.1864 - val_accuracy: 0.9893
    Epoch 57/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6739 - accuracy: 0.9645 - val_loss: 0.0916 - val_accuracy: 0.9893
    Epoch 58/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2380 - accuracy: 0.9752 - val_loss: 0.4220 - val_accuracy: 0.9786
    Epoch 59/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3471 - accuracy: 0.9690 - val_loss: 0.1621 - val_accuracy: 0.9893
    Epoch 60/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3551 - accuracy: 0.9778 - val_loss: 0.2848 - val_accuracy: 0.9786
    Epoch 61/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3196 - accuracy: 0.9770 - val_loss: 0.3255 - val_accuracy: 0.9679
    Epoch 62/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2263 - accuracy: 0.9814 - val_loss: 0.5419 - val_accuracy: 0.9679
    Epoch 63/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3582 - accuracy: 0.9778 - val_loss: 0.8802 - val_accuracy: 0.9733
    Epoch 64/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1992 - accuracy: 0.9823 - val_loss: 0.9356 - val_accuracy: 0.9626
    Epoch 65/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2844 - accuracy: 0.9814 - val_loss: 0.7566 - val_accuracy: 0.9733
    Epoch 66/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2239 - accuracy: 0.9840 - val_loss: 1.0973 - val_accuracy: 0.9519
    Epoch 67/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3123 - accuracy: 0.9787 - val_loss: 0.8371 - val_accuracy: 0.9679
    Epoch 68/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3699 - accuracy: 0.9761 - val_loss: 0.5939 - val_accuracy: 0.9840
    Epoch 69/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2553 - accuracy: 0.9796 - val_loss: 0.2044 - val_accuracy: 0.9786
    Epoch 70/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2664 - accuracy: 0.9805 - val_loss: 0.1849 - val_accuracy: 0.9893
    Epoch 71/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3305 - accuracy: 0.9725 - val_loss: 0.5357 - val_accuracy: 0.9786
    Epoch 72/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2286 - accuracy: 0.9814 - val_loss: 0.3758 - val_accuracy: 0.9840
    Epoch 73/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4496 - accuracy: 0.9707 - val_loss: 0.5079 - val_accuracy: 0.9786
    Epoch 74/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4208 - accuracy: 0.9770 - val_loss: 0.9182 - val_accuracy: 0.9626
    Epoch 75/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4068 - accuracy: 0.9752 - val_loss: 1.4118 - val_accuracy: 0.9519
    Epoch 76/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3400 - accuracy: 0.9770 - val_loss: 0.2327 - val_accuracy: 0.9733
    Epoch 77/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3937 - accuracy: 0.9716 - val_loss: 0.3861 - val_accuracy: 0.9679
    Epoch 78/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1977 - accuracy: 0.9849 - val_loss: 0.3171 - val_accuracy: 0.9733
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4107 - accuracy: 0.9752 - val_loss: 0.9084 - val_accuracy: 0.9679
    Epoch 80/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3015 - accuracy: 0.9761 - val_loss: 0.8284 - val_accuracy: 0.9679
    Epoch 81/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2966 - accuracy: 0.9805 - val_loss: 1.0918 - val_accuracy: 0.9572
    Epoch 82/100
    9/9 [==============================] - 17s 2s/step - loss: 0.4547 - accuracy: 0.9725 - val_loss: 0.3611 - val_accuracy: 0.9840
    Epoch 83/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2105 - accuracy: 0.9849 - val_loss: 0.2205 - val_accuracy: 0.9893
    Epoch 84/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3341 - accuracy: 0.9805 - val_loss: 0.2588 - val_accuracy: 0.9786
    Epoch 85/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1431 - accuracy: 0.9858 - val_loss: 0.2557 - val_accuracy: 0.9733
    Epoch 86/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1769 - accuracy: 0.9849 - val_loss: 0.7535 - val_accuracy: 0.9626
    Epoch 87/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4019 - accuracy: 0.9770 - val_loss: 0.5269 - val_accuracy: 0.9733
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3099 - accuracy: 0.9796 - val_loss: 0.2221 - val_accuracy: 0.9840
    Epoch 89/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3109 - accuracy: 0.9840 - val_loss: 0.2300 - val_accuracy: 0.9733
    Epoch 90/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3336 - accuracy: 0.9796 - val_loss: 0.4060 - val_accuracy: 0.9679
    Epoch 91/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1491 - accuracy: 0.9867 - val_loss: 0.6833 - val_accuracy: 0.9733
    Epoch 92/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1698 - accuracy: 0.9867 - val_loss: 0.7452 - val_accuracy: 0.9786
    Epoch 93/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2742 - accuracy: 0.9823 - val_loss: 0.2482 - val_accuracy: 0.9786
    Epoch 94/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1770 - accuracy: 0.9823 - val_loss: 0.4327 - val_accuracy: 0.9786
    Epoch 95/100
    9/9 [==============================] - 16s 2s/step - loss: 0.2469 - accuracy: 0.9849 - val_loss: 0.2970 - val_accuracy: 0.9840
    Epoch 96/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3378 - accuracy: 0.9840 - val_loss: 0.3013 - val_accuracy: 0.9893
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2189 - accuracy: 0.9849 - val_loss: 0.6201 - val_accuracy: 0.9626
    Epoch 98/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1686 - accuracy: 0.9858 - val_loss: 0.7968 - val_accuracy: 0.9626
    Epoch 99/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1636 - accuracy: 0.9902 - val_loss: 0.7979 - val_accuracy: 0.9572
    Epoch 100/100
    9/9 [==============================] - 17s 2s/step - loss: 0.2615 - accuracy: 0.9823 - val_loss: 0.6621 - val_accuracy: 0.9679

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/109.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 11s 2s/step - loss: 1.8120 - accuracy: 0.9576

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.8120121955871582, 0.9575971961021423]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = False
for layer in new_model.get_layer('vgg16_features').layers:
    for block_name in trainable_blocks:
        if layer.name.startswith(block_name):
            layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(tb_path)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/110.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    18/18 [==============================] - 27s 2s/step - loss: 0.2571 - accuracy: 0.9840 - val_loss: 0.4792 - val_accuracy: 0.9679

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1371 - accuracy: 0.9876 - val_loss: 0.3965 - val_accuracy: 0.9733

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1388 - accuracy: 0.9876 - val_loss: 0.3525 - val_accuracy: 0.9733

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2792 - accuracy: 0.9840 - val_loss: 0.3183 - val_accuracy: 0.9786

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    18/18 [==============================] - 26s 1s/step - loss: 0.5024 - accuracy: 0.9761 - val_loss: 0.3029 - val_accuracy: 0.9786

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2772 - accuracy: 0.9814 - val_loss: 0.2823 - val_accuracy: 0.9786

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2119 - accuracy: 0.9840 - val_loss: 0.2861 - val_accuracy: 0.9733

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2282 - accuracy: 0.9867 - val_loss: 0.2821 - val_accuracy: 0.9733

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    18/18 [==============================] - 26s 1s/step - loss: 0.0588 - accuracy: 0.9911 - val_loss: 0.2802 - val_accuracy: 0.9733

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1627 - accuracy: 0.9849 - val_loss: 0.2831 - val_accuracy: 0.9733

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2054 - accuracy: 0.9867 - val_loss: 0.2890 - val_accuracy: 0.9733

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2844 - accuracy: 0.9796 - val_loss: 0.2925 - val_accuracy: 0.9733

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2177 - accuracy: 0.9867 - val_loss: 0.3025 - val_accuracy: 0.9733

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1370 - accuracy: 0.9894 - val_loss: 0.3010 - val_accuracy: 0.9733

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3596 - accuracy: 0.9823 - val_loss: 0.3063 - val_accuracy: 0.9733

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1288 - accuracy: 0.9858 - val_loss: 0.3118 - val_accuracy: 0.9733

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1172 - accuracy: 0.9858 - val_loss: 0.3174 - val_accuracy: 0.9733

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1042 - accuracy: 0.9902 - val_loss: 0.3158 - val_accuracy: 0.9733

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2645 - accuracy: 0.9832 - val_loss: 0.3084 - val_accuracy: 0.9733

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3990 - accuracy: 0.9787 - val_loss: 0.2985 - val_accuracy: 0.9733

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3439 - accuracy: 0.9832 - val_loss: 0.3042 - val_accuracy: 0.9733

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1409 - accuracy: 0.9911 - val_loss: 0.3198 - val_accuracy: 0.9733

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2385 - accuracy: 0.9814 - val_loss: 0.3255 - val_accuracy: 0.9733

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2567 - accuracy: 0.9823 - val_loss: 0.3416 - val_accuracy: 0.9733

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1395 - accuracy: 0.9885 - val_loss: 0.3362 - val_accuracy: 0.9733

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1087 - accuracy: 0.9902 - val_loss: 0.3381 - val_accuracy: 0.9733

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1925 - accuracy: 0.9876 - val_loss: 0.3305 - val_accuracy: 0.9733

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1699 - accuracy: 0.9911 - val_loss: 0.3297 - val_accuracy: 0.9733

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2306 - accuracy: 0.9858 - val_loss: 0.3275 - val_accuracy: 0.9733

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2113 - accuracy: 0.9858 - val_loss: 0.3362 - val_accuracy: 0.9733

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1493 - accuracy: 0.9885 - val_loss: 0.3422 - val_accuracy: 0.9733

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1302 - accuracy: 0.9885 - val_loss: 0.3403 - val_accuracy: 0.9733

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2343 - accuracy: 0.9849 - val_loss: 0.3431 - val_accuracy: 0.9733

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    18/18 [==============================] - 23s 1s/step - loss: 0.1581 - accuracy: 0.9849 - val_loss: 0.3296 - val_accuracy: 0.9733

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    18/18 [==============================] - 25s 1s/step - loss: 0.3002 - accuracy: 0.9823 - val_loss: 0.3243 - val_accuracy: 0.9733

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2517 - accuracy: 0.9832 - val_loss: 0.3285 - val_accuracy: 0.9733

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1573 - accuracy: 0.9867 - val_loss: 0.3328 - val_accuracy: 0.9733

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1526 - accuracy: 0.9938 - val_loss: 0.3331 - val_accuracy: 0.9733

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1752 - accuracy: 0.9867 - val_loss: 0.3274 - val_accuracy: 0.9733

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1695 - accuracy: 0.9885 - val_loss: 0.3356 - val_accuracy: 0.9733

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1879 - accuracy: 0.9911 - val_loss: 0.3462 - val_accuracy: 0.9733

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1207 - accuracy: 0.9894 - val_loss: 0.3577 - val_accuracy: 0.9733

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1933 - accuracy: 0.9885 - val_loss: 0.3559 - val_accuracy: 0.9733

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2475 - accuracy: 0.9805 - val_loss: 0.3700 - val_accuracy: 0.9733

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    18/18 [==============================] - 24s 1s/step - loss: 0.2151 - accuracy: 0.9805 - val_loss: 0.3656 - val_accuracy: 0.9733

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1789 - accuracy: 0.9867 - val_loss: 0.3697 - val_accuracy: 0.9733

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1682 - accuracy: 0.9867 - val_loss: 0.3661 - val_accuracy: 0.9733

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    18/18 [==============================] - 24s 1s/step - loss: 0.0761 - accuracy: 0.9911 - val_loss: 0.3550 - val_accuracy: 0.9733

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    18/18 [==============================] - 25s 1s/step - loss: 0.1331 - accuracy: 0.9840 - val_loss: 0.3519 - val_accuracy: 0.9733

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    18/18 [==============================] - 24s 1s/step - loss: 0.1471 - accuracy: 0.9858 - val_loss: 0.3553 - val_accuracy: 0.9733

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/111.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 1.1571 - accuracy: 0.9647

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.1571301221847534, 0.9646643400192261]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9646643109540636
                  precision    recall  f1-score   support

               0       0.92      0.96      0.94       164
               1       0.98      0.97      0.97       402

        accuracy                           0.96       566
       macro avg       0.95      0.96      0.96       566
    weighted avg       0.97      0.96      0.96       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[38\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[38\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/112.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/113.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[39\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[39\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/114.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[40\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[41\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.92      0.96      0.94       164
               1       0.98      0.97      0.97       402

        accuracy                           0.96       566
       macro avg       0.95      0.96      0.96       566
    weighted avg       0.97      0.96      0.96       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.92      0.96      0.97      0.94      0.96      0.93       164
              1       0.98      0.97      0.96      0.97      0.96      0.93       402

    avg / total       0.97      0.96      0.96      0.96      0.96      0.93       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
