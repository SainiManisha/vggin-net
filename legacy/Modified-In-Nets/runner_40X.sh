export MAGNIFICATION_FACTOR=40X

# for i in {1..1}
# do
#    export NUM_RUN=$i
#    jupyter nbconvert --execute --to html --ExecutePreprocessor.timeout=-1 --debug --output ModifiedEfficientNet_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html ModifiedEfficientNet.ipynb
# done

# for i in {1..1}
# do
#    export NUM_RUN=$i
#    jupyter nbconvert --execute --to html --ExecutePreprocessor.timeout=-1 --debug --output ModifiedDenseNet_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html ModifiedDenseNet.ipynb
# done

# for i in {1..1}
# do
#    export NUM_RUN=$i
#    jupyter nbconvert --execute --to html --ExecutePreprocessor.timeout=-1 --debug --output ModifiedXception_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html ModifiedXception.ipynb
# done

# for i in {1..1}
# do
#    export NUM_RUN=$i
#    jupyter nbconvert --execute --to html --ExecutePreprocessor.timeout=-1 --debug --output ModifiedMobileNet_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html ModifiedMobileNet.ipynb
# done

for i in {1..1}
do
   export NUM_RUN=$i
   jupyter nbconvert --execute --to html --ExecutePreprocessor.timeout=-1 --debug --output ModifiedResNet_${MAGNIFICATION_FACTOR}_${NUM_RUN}.html ModifiedResNet.ipynb
done
