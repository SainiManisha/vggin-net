<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
from tensorflow.keras.layers import Dropout, Dense, BatchNormalization, Flatten
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def vgginnet(inp_shape, num_classes, name, include_top=False, weights_path=None):
    base_model = VGG16(include_top=False, input_shape=inp_shape)

    feature_ex_model = Model(inputs=base_model.input,
                             outputs=base_model.get_layer('block4_pool').output,
                             name='vgg16_features')
    feature_ex_model.trainable = False

    image_input = Input(inp_shape, name='image_input')
    x = Lambda(vgg_preprocess, name='vgg_preprocess')(image_input)

    x = feature_ex_model(x)
    feature_ex_model = Model(inputs=image_input, outputs=x)
    x = feature_ex_model.output

    def naive_inception_module(layer_in, f1, f2, f3):
        # 1x1 conv
        conv1 = Conv2D(f1, (1,1), padding='same',
                       activation='relu', name='inception_block/conv_1x1')(layer_in)
        # 3x3 conv
        conv3 = Conv2D(f2, (3,3), padding='same',
                       activation='relu', name='inception_block/conv_3x3')(layer_in)
        # 5x5 conv
        conv5 = Conv2D(f3, (5,5), padding='same',
                       activation='relu', name='inception_block/conv_5x5')(layer_in)
        # 3x3 max pooling
        pool = MaxPooling2D((3,3), strides=(1,1), 
                            padding='same', name='inception_block/pool')(layer_in)
        # concatenate filters, assumes filters/channels last
        layer_out = Concatenate(name='inception_block/add')(
            [conv1, conv3, conv5, pool])
        return layer_out

    x = naive_inception_module(x, 64, 128, 32)
    x = BatchNormalization(name='batch_norm')(x)
    
    if include_top:
        x = Flatten(name='flatten')(x)
        x = Dropout(0.4, name='dropout')(x)
        x = Dense(num_classes, activation='softmax', name='preds')(x)
    
    model = Model(inputs=image_input, outputs=x, name=name)
    model.summary()
    
    if weights_path:
        model.load_weights(weights_path)
    
    return model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
wt_path = "../VGGINET-SemiFinal/VGGINet/40X/1/model.h5"

breakhis_vgginnet = vgginnet((224, 224, 3), 2, 
                             'breakhis_vgginnet', include_top=True,
                             weights_path=wt_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "breakhis_vgginnet"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 224, 224, 3)  0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 14, 14, 736)  0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 14, 14, 736)  2944        inception_block/add[0][0]        
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           batch_norm[0][0]                 
    __________________________________________________________________________________________________
    dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    preds (Dense)                   (None, 2)            288514      dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
breakhis_vgginnet_stem = Model(breakhis_vgginnet.inputs,
                               breakhis_vgginnet.get_layer("batch_norm").output,
                               name='vgginnet_breakhis_stem')
breakhis_vgginnet_stem.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgginnet_breakhis_stem"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 224, 224, 3)  0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 14, 14, 736)  0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 14, 14, 736)  2944        inception_block/add[0][0]        
    ==================================================================================================
    Total params: 8,670,624
    Trainable params: 1,033,888
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
pretrained_weights = breakhis_vgginnet_stem.get_weights()
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
image_size = (50, 50)
num_classes = 2

hist_path_vgginet_stem = vgginnet((*image_size, 3), num_classes,
                                 'vgginnet_stem', include_top=False)
hist_path_vgginet_stem.set_weights(pretrained_weights)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgginnet_stem"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 50, 50, 3)]  0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 50, 50, 3)    0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 3, 3, 512)    7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 3, 3, 64)     32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 3, 3, 128)    589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 3, 3, 32)     409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 3, 3, 512)    0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 3, 3, 736)    0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 3, 3, 736)    2944        inception_block/add[0][0]        
    ==================================================================================================
    Total params: 8,670,624
    Trainable params: 1,033,888
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
hist_path_vgginet_stem.trainable = False

net = Sequential([
    hist_path_vgginet_stem,
    Flatten(name='flatten'),
    Dense(num_classes, activation='softmax', name='preds')
], name='hist_path_vgginet')
net.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "hist_path_vgginet"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    vgginnet_stem (Functional)   (None, 3, 3, 736)         8670624   
    _________________________________________________________________
    flatten (Flatten)            (None, 6624)              0         
    _________________________________________________________________
    preds (Dense)                (None, 2)                 13250     
    =================================================================
    Total params: 8,683,874
    Trainable params: 13,250
    Non-trainable params: 8,670,624
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
opt = tf.keras.optimizers.Adam(lr=0.01)
net.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
batch_size = 512

train_path = '../../Splitted_BREAST_HIST/train'
val_path = '../../Splitted_BREAST_HIST/val'
test_path = '../../Splitted_BREAST_HIST/test'

gen = tf.keras.preprocessing.image.ImageDataGenerator(
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    vertical_flip=True,
    shear_range=0.2,
    zoom_range=0.2)
gen1 = tf.keras.preprocessing.image.ImageDataGenerator()

train_ds = gen.flow_from_directory(train_path, batch_size=batch_size,
                                   target_size=image_size, shuffle=True)
val_ds = gen1.flow_from_directory(val_path, batch_size=batch_size,
                                  target_size=image_size, shuffle=False)
test_ds = gen1.flow_from_directory(test_path, batch_size=batch_size,
                                   target_size=image_size, shuffle=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 208142 images belonging to 2 classes.
    Found 13875 images belonging to 2 classes.
    Found 55507 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './TRANSFER_LEARNING_HIST_PATH_VGGINET'
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
callbacks = [tf.keras.callbacks.TensorBoard(path + '/tensorboard')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

**Fixed feature training**

</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs = 50
train_history = net.fit(train_ds, epochs=epochs, 
                        validation_data=val_ds, callbacks=callbacks)
net.save_weights(path + '/weights.h5')
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
      1/407 [..............................] - ETA: 0s - loss: 1.0515 - accuracy: 0.6035WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    407/407 [==============================] - 4625s 11s/step - loss: 1.0244 - accuracy: 0.7929 - val_loss: 0.9060 - val_accuracy: 0.8085
    Epoch 2/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.9355 - accuracy: 0.7975 - val_loss: 0.8063 - val_accuracy: 0.7618
    Epoch 3/50
    407/407 [==============================] - 292s 717ms/step - loss: 0.9635 - accuracy: 0.7978 - val_loss: 0.8905 - val_accuracy: 0.8054
    Epoch 4/50
    407/407 [==============================] - 291s 714ms/step - loss: 0.9974 - accuracy: 0.7969 - val_loss: 1.0886 - val_accuracy: 0.8081
    Epoch 5/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.9461 - accuracy: 0.7985 - val_loss: 0.8482 - val_accuracy: 0.8058
    Epoch 6/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.9159 - accuracy: 0.7997 - val_loss: 1.0523 - val_accuracy: 0.7293
    Epoch 7/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.9470 - accuracy: 0.7984 - val_loss: 1.5761 - val_accuracy: 0.8118
    Epoch 8/50
    407/407 [==============================] - 299s 735ms/step - loss: 0.9867 - accuracy: 0.7967 - val_loss: 0.9325 - val_accuracy: 0.7614
    Epoch 9/50
    407/407 [==============================] - 299s 736ms/step - loss: 0.9171 - accuracy: 0.8006 - val_loss: 1.0339 - val_accuracy: 0.8427
    Epoch 10/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.8783 - accuracy: 0.8018 - val_loss: 0.7550 - val_accuracy: 0.8223
    Epoch 11/50
    407/407 [==============================] - 294s 722ms/step - loss: 0.9319 - accuracy: 0.7980 - val_loss: 1.3027 - val_accuracy: 0.8337
    Epoch 12/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.9222 - accuracy: 0.7994 - val_loss: 0.9891 - val_accuracy: 0.7918
    Epoch 13/50
    407/407 [==============================] - 300s 738ms/step - loss: 0.8990 - accuracy: 0.8015 - val_loss: 0.7807 - val_accuracy: 0.8172
    Epoch 14/50
    407/407 [==============================] - 294s 721ms/step - loss: 0.9992 - accuracy: 0.7973 - val_loss: 1.3783 - val_accuracy: 0.7274
    Epoch 15/50
    407/407 [==============================] - 295s 726ms/step - loss: 0.9504 - accuracy: 0.8010 - val_loss: 0.6789 - val_accuracy: 0.8312
    Epoch 16/50
    407/407 [==============================] - 304s 746ms/step - loss: 0.9760 - accuracy: 0.7996 - val_loss: 0.8139 - val_accuracy: 0.8272
    Epoch 17/50
    407/407 [==============================] - 308s 756ms/step - loss: 0.9385 - accuracy: 0.8001 - val_loss: 0.8289 - val_accuracy: 0.8182
    Epoch 18/50
    407/407 [==============================] - 305s 749ms/step - loss: 0.9285 - accuracy: 0.8015 - val_loss: 1.1034 - val_accuracy: 0.8151
    Epoch 19/50
    407/407 [==============================] - 300s 737ms/step - loss: 0.9375 - accuracy: 0.8002 - val_loss: 0.8091 - val_accuracy: 0.8348
    Epoch 20/50
    407/407 [==============================] - 290s 713ms/step - loss: 0.9539 - accuracy: 0.8000 - val_loss: 0.8455 - val_accuracy: 0.8399
    Epoch 21/50
    407/407 [==============================] - 290s 713ms/step - loss: 1.0032 - accuracy: 0.7976 - val_loss: 0.8180 - val_accuracy: 0.8059
    Epoch 22/50
    407/407 [==============================] - 291s 715ms/step - loss: 0.8947 - accuracy: 0.8002 - val_loss: 0.8367 - val_accuracy: 0.8391
    Epoch 23/50
    407/407 [==============================] - 294s 721ms/step - loss: 0.9203 - accuracy: 0.8003 - val_loss: 0.8294 - val_accuracy: 0.7968
    Epoch 24/50
    407/407 [==============================] - 291s 714ms/step - loss: 0.9466 - accuracy: 0.7994 - val_loss: 1.1472 - val_accuracy: 0.7246
    Epoch 25/50
    407/407 [==============================] - 291s 715ms/step - loss: 0.9387 - accuracy: 0.7988 - val_loss: 1.0744 - val_accuracy: 0.7781
    Epoch 26/50
    407/407 [==============================] - 289s 709ms/step - loss: 0.9628 - accuracy: 0.8003 - val_loss: 0.8031 - val_accuracy: 0.8182
    Epoch 27/50
    407/407 [==============================] - 289s 709ms/step - loss: 0.9953 - accuracy: 0.7987 - val_loss: 0.9941 - val_accuracy: 0.8200
    Epoch 28/50
    407/407 [==============================] - 286s 703ms/step - loss: 0.9169 - accuracy: 0.7998 - val_loss: 0.8526 - val_accuracy: 0.8014
    Epoch 29/50
    407/407 [==============================] - 286s 703ms/step - loss: 0.9615 - accuracy: 0.7978 - val_loss: 0.7648 - val_accuracy: 0.8115
    Epoch 30/50
    407/407 [==============================] - 281s 692ms/step - loss: 0.9571 - accuracy: 0.7992 - val_loss: 1.1368 - val_accuracy: 0.8038
    Epoch 31/50
    407/407 [==============================] - 284s 699ms/step - loss: 0.9086 - accuracy: 0.8015 - val_loss: 0.7795 - val_accuracy: 0.8032
    Epoch 32/50
    407/407 [==============================] - 289s 711ms/step - loss: 0.9325 - accuracy: 0.7998 - val_loss: 1.0164 - val_accuracy: 0.7723
    Epoch 33/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.9309 - accuracy: 0.8000 - val_loss: 1.2662 - val_accuracy: 0.8326
    Epoch 34/50
    407/407 [==============================] - 291s 716ms/step - loss: 0.9661 - accuracy: 0.7994 - val_loss: 0.9578 - val_accuracy: 0.7945
    Epoch 35/50
    407/407 [==============================] - 292s 717ms/step - loss: 0.9646 - accuracy: 0.8000 - val_loss: 0.7332 - val_accuracy: 0.8228
    Epoch 36/50
    407/407 [==============================] - 290s 713ms/step - loss: 0.9297 - accuracy: 0.8013 - val_loss: 0.9369 - val_accuracy: 0.8140
    Epoch 37/50
    407/407 [==============================] - 288s 708ms/step - loss: 0.9246 - accuracy: 0.8021 - val_loss: 0.7970 - val_accuracy: 0.8271
    Epoch 38/50
    407/407 [==============================] - 292s 717ms/step - loss: 0.9314 - accuracy: 0.8004 - val_loss: 0.8544 - val_accuracy: 0.7928
    Epoch 39/50
    407/407 [==============================] - 289s 711ms/step - loss: 1.0202 - accuracy: 0.7986 - val_loss: 0.8415 - val_accuracy: 0.8286
    Epoch 40/50
    407/407 [==============================] - 287s 705ms/step - loss: 0.9369 - accuracy: 0.8008 - val_loss: 0.8861 - val_accuracy: 0.7723
    Epoch 41/50
    407/407 [==============================] - 287s 704ms/step - loss: 0.9359 - accuracy: 0.7998 - val_loss: 1.6264 - val_accuracy: 0.8084
    Epoch 42/50
    407/407 [==============================] - 291s 716ms/step - loss: 0.9445 - accuracy: 0.8009 - val_loss: 0.7903 - val_accuracy: 0.8063
    Epoch 43/50
    407/407 [==============================] - 288s 709ms/step - loss: 0.9491 - accuracy: 0.7992 - val_loss: 0.8150 - val_accuracy: 0.7984
    Epoch 44/50
    407/407 [==============================] - 288s 707ms/step - loss: 0.8985 - accuracy: 0.8003 - val_loss: 0.7736 - val_accuracy: 0.8085
    Epoch 45/50
    407/407 [==============================] - 290s 712ms/step - loss: 0.9122 - accuracy: 0.8009 - val_loss: 0.9650 - val_accuracy: 0.8342
    Epoch 46/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.9770 - accuracy: 0.7991 - val_loss: 0.9231 - val_accuracy: 0.8108
    Epoch 47/50
    407/407 [==============================] - 292s 717ms/step - loss: 0.9490 - accuracy: 0.8007 - val_loss: 0.9570 - val_accuracy: 0.7878
    Epoch 48/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.8541 - accuracy: 0.8022 - val_loss: 0.7608 - val_accuracy: 0.8177
    Epoch 49/50
    407/407 [==============================] - 294s 721ms/step - loss: 0.9423 - accuracy: 0.7995 - val_loss: 0.7704 - val_accuracy: 0.8053
    Epoch 50/50
    407/407 [==============================] - 291s 715ms/step - loss: 0.9474 - accuracy: 0.8000 - val_loss: 1.0525 - val_accuracy: 0.7769

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def train_plot(history, start_ft=0):
    acc = history['accuracy']
    val_acc = history['val_accuracy']

    loss = history['loss']
    val_loss = history['val_loss']

    fig = plt.figure(figsize=(8, 8))
    fig.patch.set_alpha(0.5)

    plt.subplot(2, 1, 1)
    plt.plot(acc)
    plt.plot(val_acc)
    
    legend = ['Training Accuracy', 'Validation Accuracy']
    if start_ft != 0:
        plt.plot([start_ft - 1, start_ft - 1], plt.ylim())
        legend.append('Start Fine Tuning')
    
    plt.legend(legend, loc='lower right')
    plt.ylabel('Accuracy')
    plt.title('Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss)
    plt.plot(val_loss)
    
    legend = ['Training Loss', 'Validation Loss']
    if start_ft != 0:
        plt.plot([start_ft - 1, start_ft - 1], plt.ylim())
        legend.append('Start Fine Tuning')
    
    plt.legend(legend, loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.xlabel('Epochs')
    plt.title('Loss')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_plot(train_history.history)
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/1.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def eval_report(model, ds):
    steps = len(ds)
    
    y_true, y_pred = [], []
    for step, (images, labels) in enumerate(ds):
        if step &gt;= steps:
            break
        preds = model.predict(images)
        y_true.append(labels)
        y_pred.append(preds)

    y_true, y_probas = np.concatenate(y_true), np.concatenate(y_pred)
    y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_probas, axis=1)
    
    plot_confusion_matrix(y_true, y_pred)
    plt.savefig(path + "/confusion_matrix.pdf")
    plt.show()
    
    plot_confusion_matrix(y_true, y_pred, normalize=True)
    plt.savefig(path + "/confusion_matrix_norm.pdf")
    plt.show()
    
    plot_roc(y_true, y_probas)
    plt.savefig(path + "/roc.pdf")
    plt.show()
    
    print('Accuracy: ', accuracy_score(y_true, y_pred))
    print(classification_report(y_true, y_pred))
    print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
eval_report(net, test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/2.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/3.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/4.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.7712000288251932
                  precision    recall  f1-score   support

               0       0.92      0.74      0.82     39749
               1       0.57      0.84      0.68     15758

        accuracy                           0.77     55507
       macro avg       0.74      0.79      0.75     55507
    weighted avg       0.82      0.77      0.78     55507

                       pre       rec       spe        f1       geo       iba       sup

              0       0.92      0.74      0.84      0.82      0.79      0.62     39749
              1       0.57      0.84      0.74      0.68      0.79      0.63     15758

    avg / total       0.82      0.77      0.81      0.78      0.79      0.62     55507

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

**Fine tuning**

</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
net.get_layer('vgginnet_stem').trainable = True

for layer in net.get_layer('vgginnet_stem').layers:
    if layer.name.startswith('inception_block'):
        layer.trainable = True
    else:
        layer.trainable = False
net.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "hist_path_vgginet"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    vgginnet_stem (Functional)   (None, 3, 3, 736)         8670624   
    _________________________________________________________________
    flatten (Flatten)            (None, 6624)              0         
    _________________________________________________________________
    preds (Dense)                (None, 2)                 13250     
    =================================================================
    Total params: 8,683,874
    Trainable params: 1,045,666
    Non-trainable params: 7,638,208
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
net.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
initial_epochs = epochs
epochs = 25
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 15
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

callbacks.append(tf.keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")

plt.savefig(path + "/fine_tune_lr_schedule.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/5.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
ft_history = net.fit(train_ds, epochs=epochs, 
                     validation_data=val_ds,
                     callbacks=callbacks, verbose=1)
net.save_weights(path + '/ft_weights.h5')
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00001: LearningRateScheduler reducing learning rate to 1e-05.
    Epoch 1/25
    407/407 [==============================] - 291s 715ms/step - loss: 0.6751 - accuracy: 0.8250 - val_loss: 0.6306 - val_accuracy: 0.8316

    Epoch 00002: LearningRateScheduler reducing learning rate to 1.2666666666666668e-05.
    Epoch 2/25
    407/407 [==============================] - 293s 720ms/step - loss: 0.5683 - accuracy: 0.8277 - val_loss: 0.5740 - val_accuracy: 0.8304

    Epoch 00003: LearningRateScheduler reducing learning rate to 1.5333333333333334e-05.
    Epoch 3/25
    407/407 [==============================] - 293s 720ms/step - loss: 0.5189 - accuracy: 0.8306 - val_loss: 0.5391 - val_accuracy: 0.8325

    Epoch 00004: LearningRateScheduler reducing learning rate to 1.8000000000000004e-05.
    Epoch 4/25
    407/407 [==============================] - 290s 714ms/step - loss: 0.4941 - accuracy: 0.8326 - val_loss: 0.5071 - val_accuracy: 0.8352

    Epoch 00005: LearningRateScheduler reducing learning rate to 2.066666666666667e-05.
    Epoch 5/25
    407/407 [==============================] - 292s 718ms/step - loss: 0.4696 - accuracy: 0.8344 - val_loss: 0.4861 - val_accuracy: 0.8365

    Epoch 00006: LearningRateScheduler reducing learning rate to 2.3333333333333336e-05.
    Epoch 6/25
    407/407 [==============================] - 291s 714ms/step - loss: 0.4540 - accuracy: 0.8352 - val_loss: 0.4654 - val_accuracy: 0.8401

    Epoch 00007: LearningRateScheduler reducing learning rate to 2.6000000000000002e-05.
    Epoch 7/25
    407/407 [==============================] - 289s 711ms/step - loss: 0.4383 - accuracy: 0.8369 - val_loss: 0.4518 - val_accuracy: 0.8426

    Epoch 00008: LearningRateScheduler reducing learning rate to 2.8666666666666668e-05.
    Epoch 8/25
    407/407 [==============================] - 289s 710ms/step - loss: 0.4211 - accuracy: 0.8402 - val_loss: 0.4399 - val_accuracy: 0.8437

    Epoch 00009: LearningRateScheduler reducing learning rate to 3.1333333333333334e-05.
    Epoch 9/25
    407/407 [==============================] - 291s 715ms/step - loss: 0.4125 - accuracy: 0.8407 - val_loss: 0.4306 - val_accuracy: 0.8438

    Epoch 00010: LearningRateScheduler reducing learning rate to 3.4e-05.
    Epoch 10/25
    407/407 [==============================] - 292s 718ms/step - loss: 0.4023 - accuracy: 0.8416 - val_loss: 0.4225 - val_accuracy: 0.8447

    Epoch 00011: LearningRateScheduler reducing learning rate to 3.6666666666666666e-05.
    Epoch 11/25
    407/407 [==============================] - 292s 718ms/step - loss: 0.3958 - accuracy: 0.8429 - val_loss: 0.4138 - val_accuracy: 0.8452

    Epoch 00012: LearningRateScheduler reducing learning rate to 3.933333333333334e-05.
    Epoch 12/25
    407/407 [==============================] - 293s 720ms/step - loss: 0.3904 - accuracy: 0.8439 - val_loss: 0.3987 - val_accuracy: 0.8500

    Epoch 00013: LearningRateScheduler reducing learning rate to 4.2000000000000004e-05.
    Epoch 13/25
    407/407 [==============================] - 293s 721ms/step - loss: 0.3841 - accuracy: 0.8454 - val_loss: 0.3908 - val_accuracy: 0.8518

    Epoch 00014: LearningRateScheduler reducing learning rate to 4.466666666666667e-05.
    Epoch 14/25
    407/407 [==============================] - 294s 723ms/step - loss: 0.3771 - accuracy: 0.8466 - val_loss: 0.3824 - val_accuracy: 0.8499

    Epoch 00015: LearningRateScheduler reducing learning rate to 4.7333333333333336e-05.
    Epoch 15/25
    407/407 [==============================] - 292s 718ms/step - loss: 0.3721 - accuracy: 0.8478 - val_loss: 0.3767 - val_accuracy: 0.8525

    Epoch 00016: LearningRateScheduler reducing learning rate to 5e-05.
    Epoch 16/25
    407/407 [==============================] - 295s 725ms/step - loss: 0.3694 - accuracy: 0.8486 - val_loss: 0.3735 - val_accuracy: 0.8525

    Epoch 00017: LearningRateScheduler reducing learning rate to 4.2000000000000004e-05.
    Epoch 17/25
    407/407 [==============================] - 297s 730ms/step - loss: 0.3648 - accuracy: 0.8493 - val_loss: 0.3670 - val_accuracy: 0.8535

    Epoch 00018: LearningRateScheduler reducing learning rate to 3.5600000000000005e-05.
    Epoch 18/25
    407/407 [==============================] - 296s 726ms/step - loss: 0.3630 - accuracy: 0.8507 - val_loss: 0.3667 - val_accuracy: 0.8555

    Epoch 00019: LearningRateScheduler reducing learning rate to 3.0480000000000006e-05.
    Epoch 19/25
    407/407 [==============================] - 292s 718ms/step - loss: 0.3577 - accuracy: 0.8519 - val_loss: 0.3661 - val_accuracy: 0.8554

    Epoch 00020: LearningRateScheduler reducing learning rate to 2.6384000000000004e-05.
    Epoch 20/25
    407/407 [==============================] - 294s 722ms/step - loss: 0.3562 - accuracy: 0.8529 - val_loss: 0.3622 - val_accuracy: 0.8567

    Epoch 00021: LearningRateScheduler reducing learning rate to 2.3107200000000005e-05.
    Epoch 21/25
    407/407 [==============================] - 293s 720ms/step - loss: 0.3553 - accuracy: 0.8522 - val_loss: 0.3604 - val_accuracy: 0.8565

    Epoch 00022: LearningRateScheduler reducing learning rate to 2.0485760000000004e-05.
    Epoch 22/25
    407/407 [==============================] - 291s 715ms/step - loss: 0.3537 - accuracy: 0.8538 - val_loss: 0.3571 - val_accuracy: 0.8572

    Epoch 00023: LearningRateScheduler reducing learning rate to 1.8388608000000004e-05.
    Epoch 23/25
    407/407 [==============================] - 290s 712ms/step - loss: 0.3509 - accuracy: 0.8548 - val_loss: 0.3582 - val_accuracy: 0.8585

    Epoch 00024: LearningRateScheduler reducing learning rate to 1.6710886400000004e-05.
    Epoch 24/25
    407/407 [==============================] - 298s 731ms/step - loss: 0.3522 - accuracy: 0.8534 - val_loss: 0.3579 - val_accuracy: 0.8587

    Epoch 00025: LearningRateScheduler reducing learning rate to 1.5368709120000003e-05.
    Epoch 25/25
    407/407 [==============================] - 302s 741ms/step - loss: 0.3498 - accuracy: 0.8541 - val_loss: 0.3611 - val_accuracy: 0.8598

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
history = {}

history['accuracy'] = train_history.history['accuracy'] + ft_history.history['accuracy']
history['val_accuracy'] = train_history.history['val_accuracy'] + ft_history.history['val_accuracy']

history['loss'] = train_history.history['loss'] + ft_history.history['loss']
history['val_loss'] = train_history.history['val_loss'] + ft_history.history['val_loss']
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_plot(history, start_ft=initial_epochs)
plt.savefig(path + "/training_and_fine_tuning_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/6.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
eval_report(net, test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/7.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/8.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/9.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8571711676004828
                  precision    recall  f1-score   support

               0       0.88      0.92      0.90     39749
               1       0.78      0.69      0.73     15758

        accuracy                           0.86     55507
       macro avg       0.83      0.81      0.82     55507
    weighted avg       0.85      0.86      0.85     55507

                       pre       rec       spe        f1       geo       iba       sup

              0       0.88      0.92      0.69      0.90      0.80      0.65     39749
              1       0.78      0.69      0.92      0.73      0.80      0.62     15758

    avg / total       0.85      0.86      0.76      0.85      0.80      0.64     55507

</div>
</div>
</div>
</div>
</div>
</div>
</div>
