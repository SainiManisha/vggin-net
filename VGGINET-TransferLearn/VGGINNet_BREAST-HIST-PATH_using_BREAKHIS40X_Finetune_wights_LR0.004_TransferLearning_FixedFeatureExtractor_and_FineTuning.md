<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
from tensorflow.keras.layers import Dropout, Dense, BatchNormalization, Flatten
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def vgginnet(inp_shape, num_classes, name, include_top=False, weights_path=None):
    base_model = VGG16(include_top=False, input_shape=inp_shape)

    feature_ex_model = Model(inputs=base_model.input,
                             outputs=base_model.get_layer('block4_pool').output,
                             name='vgg16_features')
    feature_ex_model.trainable = False

    image_input = Input(inp_shape, name='image_input')
    x = Lambda(vgg_preprocess, name='vgg_preprocess')(image_input)

    x = feature_ex_model(x)
    feature_ex_model = Model(inputs=image_input, outputs=x)
    x = feature_ex_model.output

    def naive_inception_module(layer_in, f1, f2, f3):
        # 1x1 conv
        conv1 = Conv2D(f1, (1,1), padding='same',
                       activation='relu', name='inception_block/conv_1x1')(layer_in)
        # 3x3 conv
        conv3 = Conv2D(f2, (3,3), padding='same',
                       activation='relu', name='inception_block/conv_3x3')(layer_in)
        # 5x5 conv
        conv5 = Conv2D(f3, (5,5), padding='same',
                       activation='relu', name='inception_block/conv_5x5')(layer_in)
        # 3x3 max pooling
        pool = MaxPooling2D((3,3), strides=(1,1), 
                            padding='same', name='inception_block/pool')(layer_in)
        # concatenate filters, assumes filters/channels last
        layer_out = Concatenate(name='inception_block/add')(
            [conv1, conv3, conv5, pool])
        return layer_out

    x = naive_inception_module(x, 64, 128, 32)
    x = BatchNormalization(name='batch_norm')(x)
    
    if include_top:
        x = Flatten(name='flatten')(x)
        x = Dropout(0.4, name='dropout')(x)
        x = Dense(num_classes, activation='softmax', name='preds')(x)
    
    model = Model(inputs=image_input, outputs=x, name=name)
    model.summary()
    
    if weights_path:
        model.load_weights(weights_path)
    
    return model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
wt_path = "../VGGINET-SemiFinal/VGGINet/40X/1/model.h5"

breakhis_vgginnet = vgginnet((224, 224, 3), 2, 
                             'breakhis_vgginnet', include_top=True,
                             weights_path=wt_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "breakhis_vgginnet"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 224, 224, 3)  0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 14, 14, 736)  0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 14, 14, 736)  2944        inception_block/add[0][0]        
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           batch_norm[0][0]                 
    __________________________________________________________________________________________________
    dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    preds (Dense)                   (None, 2)            288514      dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
breakhis_vgginnet_stem = Model(breakhis_vgginnet.inputs,
                               breakhis_vgginnet.get_layer("batch_norm").output,
                               name='vgginnet_breakhis_stem')
breakhis_vgginnet_stem.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgginnet_breakhis_stem"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 224, 224, 3)  0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 14, 14, 736)  0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 14, 14, 736)  2944        inception_block/add[0][0]        
    ==================================================================================================
    Total params: 8,670,624
    Trainable params: 1,033,888
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
pretrained_weights = breakhis_vgginnet_stem.get_weights()
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
image_size = (50, 50)
num_classes = 2

hist_path_vgginet_stem = vgginnet((*image_size, 3), num_classes,
                                 'vgginnet_stem', include_top=False)
hist_path_vgginet_stem.set_weights(pretrained_weights)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgginnet_stem"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    image_input (InputLayer)        [(None, 50, 50, 3)]  0                                            
    __________________________________________________________________________________________________
    vgg_preprocess (Lambda)         (None, 50, 50, 3)    0           image_input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 3, 3, 512)    7635264     vgg_preprocess[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_1x1 (Conv2 (None, 3, 3, 64)     32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_3x3 (Conv2 (None, 3, 3, 128)    589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/conv_5x5 (Conv2 (None, 3, 3, 32)     409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/pool (MaxPoolin (None, 3, 3, 512)    0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    inception_block/add (Concatenat (None, 3, 3, 736)    0           inception_block/conv_1x1[0][0]   
                                                                     inception_block/conv_3x3[0][0]   
                                                                     inception_block/conv_5x5[0][0]   
                                                                     inception_block/pool[0][0]       
    __________________________________________________________________________________________________
    batch_norm (BatchNormalization) (None, 3, 3, 736)    2944        inception_block/add[0][0]        
    ==================================================================================================
    Total params: 8,670,624
    Trainable params: 1,033,888
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
hist_path_vgginet_stem.trainable = False

net = Sequential([
    hist_path_vgginet_stem,
    Flatten(name='flatten'),
    Dense(num_classes, activation='softmax', name='preds')
], name='hist_path_vgginet')
net.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "hist_path_vgginet"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    vgginnet_stem (Functional)   (None, 3, 3, 736)         8670624   
    _________________________________________________________________
    flatten (Flatten)            (None, 6624)              0         
    _________________________________________________________________
    preds (Dense)                (None, 2)                 13250     
    =================================================================
    Total params: 8,683,874
    Trainable params: 13,250
    Non-trainable params: 8,670,624
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
batch_size = 512

train_path = '../../Splitted_BREAST_HIST/train'
val_path = '../../Splitted_BREAST_HIST/val'
test_path = '../../Splitted_BREAST_HIST/test'

gen = tf.keras.preprocessing.image.ImageDataGenerator(
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    vertical_flip=True,
    shear_range=0.2,
    zoom_range=0.2)
gen1 = tf.keras.preprocessing.image.ImageDataGenerator()

train_ds = gen.flow_from_directory(train_path, batch_size=batch_size,
                                   target_size=image_size, shuffle=True)
val_ds = gen1.flow_from_directory(val_path, batch_size=batch_size,
                                  target_size=image_size, shuffle=False)
test_ds = gen1.flow_from_directory(test_path, batch_size=batch_size,
                                   target_size=image_size, shuffle=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 208142 images belonging to 2 classes.
    Found 13875 images belonging to 2 classes.
    Found 55507 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
opt = tf.keras.optimizers.Adam(lr=(0.001 * (batch_size / 128)))
net.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './TRANSFER_LEARNING_HIST_PATH_VGGINET'
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
callbacks = [tf.keras.callbacks.TensorBoard(path + '/tensorboard')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

**Fixed feature training**

</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs = 50
train_history = net.fit(train_ds, epochs=epochs, 
                        validation_data=val_ds, callbacks=callbacks)
net.save_weights(path + '/weights.h5')
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
      1/407 [..............................] - ETA: 0s - loss: 0.9629 - accuracy: 0.5918WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    407/407 [==============================] - 301s 739ms/step - loss: 0.5813 - accuracy: 0.8102 - val_loss: 0.5255 - val_accuracy: 0.8241
    Epoch 2/50
    407/407 [==============================] - 293s 719ms/step - loss: 0.5179 - accuracy: 0.8167 - val_loss: 0.6899 - val_accuracy: 0.8074
    Epoch 3/50
    407/407 [==============================] - 292s 717ms/step - loss: 0.5045 - accuracy: 0.8183 - val_loss: 0.4893 - val_accuracy: 0.8243
    Epoch 4/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5241 - accuracy: 0.8171 - val_loss: 0.4692 - val_accuracy: 0.8300
    Epoch 5/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.5005 - accuracy: 0.8191 - val_loss: 0.4457 - val_accuracy: 0.8334
    Epoch 6/50
    407/407 [==============================] - 294s 722ms/step - loss: 0.5253 - accuracy: 0.8163 - val_loss: 0.5702 - val_accuracy: 0.8254
    Epoch 7/50
    407/407 [==============================] - 295s 725ms/step - loss: 0.5559 - accuracy: 0.8142 - val_loss: 0.4607 - val_accuracy: 0.8324
    Epoch 8/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.5355 - accuracy: 0.8152 - val_loss: 0.5604 - val_accuracy: 0.8324
    Epoch 9/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5179 - accuracy: 0.8190 - val_loss: 0.4623 - val_accuracy: 0.8291
    Epoch 10/50
    407/407 [==============================] - 295s 724ms/step - loss: 0.5041 - accuracy: 0.8200 - val_loss: 1.0282 - val_accuracy: 0.6726
    Epoch 11/50
    407/407 [==============================] - 291s 715ms/step - loss: 0.5250 - accuracy: 0.8177 - val_loss: 0.4928 - val_accuracy: 0.8323
    Epoch 12/50
    407/407 [==============================] - 291s 714ms/step - loss: 0.5554 - accuracy: 0.8144 - val_loss: 0.4765 - val_accuracy: 0.8288
    Epoch 13/50
    407/407 [==============================] - 293s 720ms/step - loss: 0.5091 - accuracy: 0.8207 - val_loss: 0.5348 - val_accuracy: 0.8038
    Epoch 14/50
    407/407 [==============================] - 292s 716ms/step - loss: 0.5519 - accuracy: 0.8158 - val_loss: 0.4724 - val_accuracy: 0.8201
    Epoch 15/50
    407/407 [==============================] - 292s 716ms/step - loss: 0.5008 - accuracy: 0.8204 - val_loss: 0.4782 - val_accuracy: 0.8379
    Epoch 16/50
    407/407 [==============================] - 308s 756ms/step - loss: 0.5225 - accuracy: 0.8177 - val_loss: 0.4711 - val_accuracy: 0.8406
    Epoch 17/50
    407/407 [==============================] - 304s 748ms/step - loss: 0.5081 - accuracy: 0.8197 - val_loss: 0.4623 - val_accuracy: 0.8416
    Epoch 18/50
    407/407 [==============================] - 299s 734ms/step - loss: 0.5113 - accuracy: 0.8195 - val_loss: 0.4489 - val_accuracy: 0.8326
    Epoch 19/50
    407/407 [==============================] - 292s 716ms/step - loss: 0.4977 - accuracy: 0.8215 - val_loss: 0.5248 - val_accuracy: 0.8045
    Epoch 20/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.5178 - accuracy: 0.8180 - val_loss: 0.5274 - val_accuracy: 0.8320
    Epoch 21/50
    407/407 [==============================] - 290s 714ms/step - loss: 0.5347 - accuracy: 0.8167 - val_loss: 0.4847 - val_accuracy: 0.8200
    Epoch 22/50
    407/407 [==============================] - 293s 719ms/step - loss: 0.5138 - accuracy: 0.8183 - val_loss: 0.4936 - val_accuracy: 0.8339
    Epoch 23/50
    407/407 [==============================] - 292s 718ms/step - loss: 0.5183 - accuracy: 0.8181 - val_loss: 0.4740 - val_accuracy: 0.8408
    Epoch 24/50
    407/407 [==============================] - 295s 726ms/step - loss: 0.5397 - accuracy: 0.8154 - val_loss: 0.4915 - val_accuracy: 0.8368
    Epoch 25/50
    407/407 [==============================] - 295s 725ms/step - loss: 0.5068 - accuracy: 0.8204 - val_loss: 0.4829 - val_accuracy: 0.8224
    Epoch 26/50
    407/407 [==============================] - 295s 726ms/step - loss: 0.5239 - accuracy: 0.8182 - val_loss: 0.4705 - val_accuracy: 0.8288
    Epoch 27/50
    407/407 [==============================] - 296s 727ms/step - loss: 0.5211 - accuracy: 0.8186 - val_loss: 0.5308 - val_accuracy: 0.8135
    Epoch 28/50
    407/407 [==============================] - 294s 722ms/step - loss: 0.5180 - accuracy: 0.8180 - val_loss: 0.4611 - val_accuracy: 0.8373
    Epoch 29/50
    407/407 [==============================] - 300s 737ms/step - loss: 0.5128 - accuracy: 0.8181 - val_loss: 0.5731 - val_accuracy: 0.7846
    Epoch 30/50
    407/407 [==============================] - 297s 730ms/step - loss: 0.5137 - accuracy: 0.8188 - val_loss: 0.5660 - val_accuracy: 0.8347
    Epoch 31/50
    407/407 [==============================] - 293s 721ms/step - loss: 0.5299 - accuracy: 0.8181 - val_loss: 0.5204 - val_accuracy: 0.8021
    Epoch 32/50
    407/407 [==============================] - 293s 721ms/step - loss: 0.5303 - accuracy: 0.8171 - val_loss: 0.6126 - val_accuracy: 0.7813
    Epoch 33/50
    407/407 [==============================] - 293s 721ms/step - loss: 0.5252 - accuracy: 0.8181 - val_loss: 0.6601 - val_accuracy: 0.7669
    Epoch 34/50
    407/407 [==============================] - 295s 724ms/step - loss: 0.5210 - accuracy: 0.8180 - val_loss: 0.6627 - val_accuracy: 0.7875
    Epoch 35/50
    407/407 [==============================] - 295s 724ms/step - loss: 0.5078 - accuracy: 0.8194 - val_loss: 0.4926 - val_accuracy: 0.8346
    Epoch 36/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.4969 - accuracy: 0.8202 - val_loss: 0.4542 - val_accuracy: 0.8275
    Epoch 37/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5185 - accuracy: 0.8188 - val_loss: 0.6564 - val_accuracy: 0.8153
    Epoch 38/50
    407/407 [==============================] - 294s 722ms/step - loss: 0.5218 - accuracy: 0.8180 - val_loss: 0.4386 - val_accuracy: 0.8352
    Epoch 39/50
    407/407 [==============================] - 296s 728ms/step - loss: 0.5643 - accuracy: 0.8149 - val_loss: 0.5607 - val_accuracy: 0.7766
    Epoch 40/50
    407/407 [==============================] - 296s 728ms/step - loss: 0.4924 - accuracy: 0.8219 - val_loss: 0.6131 - val_accuracy: 0.8350
    Epoch 41/50
    407/407 [==============================] - 295s 724ms/step - loss: 0.5244 - accuracy: 0.8181 - val_loss: 0.4807 - val_accuracy: 0.8230
    Epoch 42/50
    407/407 [==============================] - 296s 727ms/step - loss: 0.5053 - accuracy: 0.8211 - val_loss: 0.4556 - val_accuracy: 0.8316
    Epoch 43/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5436 - accuracy: 0.8169 - val_loss: 0.4803 - val_accuracy: 0.8417
    Epoch 44/50
    407/407 [==============================] - 296s 726ms/step - loss: 0.5100 - accuracy: 0.8200 - val_loss: 0.4818 - val_accuracy: 0.8289
    Epoch 45/50
    407/407 [==============================] - 296s 728ms/step - loss: 0.5224 - accuracy: 0.8173 - val_loss: 0.4921 - val_accuracy: 0.8224
    Epoch 46/50
    407/407 [==============================] - 295s 724ms/step - loss: 0.5143 - accuracy: 0.8188 - val_loss: 0.4936 - val_accuracy: 0.8411
    Epoch 47/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5273 - accuracy: 0.8178 - val_loss: 0.4507 - val_accuracy: 0.8367
    Epoch 48/50
    407/407 [==============================] - 294s 723ms/step - loss: 0.5170 - accuracy: 0.8191 - val_loss: 0.6067 - val_accuracy: 0.7937
    Epoch 49/50
    407/407 [==============================] - 295s 725ms/step - loss: 0.5144 - accuracy: 0.8196 - val_loss: 0.6009 - val_accuracy: 0.7947
    Epoch 50/50
    407/407 [==============================] - 294s 722ms/step - loss: 0.5267 - accuracy: 0.8180 - val_loss: 0.5573 - val_accuracy: 0.8131

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def train_plot(history, start_ft=0):
    acc = history['accuracy']
    val_acc = history['val_accuracy']

    loss = history['loss']
    val_loss = history['val_loss']

    fig = plt.figure(figsize=(8, 8))
    fig.patch.set_alpha(0.5)

    plt.subplot(2, 1, 1)
    plt.plot(acc)
    plt.plot(val_acc)
    
    legend = ['Training Accuracy', 'Validation Accuracy']
    if start_ft != 0:
        plt.plot([start_ft - 1, start_ft - 1], plt.ylim())
        legend.append('Start Fine Tuning')
    
    plt.legend(legend, loc='lower right')
    plt.ylabel('Accuracy')
    plt.title('Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss)
    plt.plot(val_loss)
    
    legend = ['Training Loss', 'Validation Loss']
    if start_ft != 0:
        plt.plot([start_ft - 1, start_ft - 1], plt.ylim())
        legend.append('Start Fine Tuning')
    
    plt.legend(legend, loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.xlabel('Epochs')
    plt.title('Loss')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_plot(train_history.history)
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/10.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def eval_report(model, ds):
    steps = len(ds)
    
    y_true, y_pred = [], []
    for step, (images, labels) in enumerate(ds):
        if step &gt;= steps:
            break
        preds = model.predict(images)
        y_true.append(labels)
        y_pred.append(preds)

    y_true, y_probas = np.concatenate(y_true), np.concatenate(y_pred)
    y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_probas, axis=1)
    
    plot_confusion_matrix(y_true, y_pred)
    plt.savefig(path + "/confusion_matrix.pdf")
    plt.show()
    
    plot_confusion_matrix(y_true, y_pred, normalize=True)
    plt.savefig(path + "/confusion_matrix_norm.pdf")
    plt.show()
    
    plot_roc(y_true, y_probas)
    plt.savefig(path + "/roc.pdf")
    plt.show()
    
    print('Accuracy: ', accuracy_score(y_true, y_pred))
    print(classification_report(y_true, y_pred))
    print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
eval_report(net, test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/11.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/12.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/13.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8153205901958311
                  precision    recall  f1-score   support

               0       0.82      0.95      0.88     39749
               1       0.80      0.47      0.59     15758

        accuracy                           0.82     55507
       macro avg       0.81      0.71      0.73     55507
    weighted avg       0.81      0.82      0.80     55507

                       pre       rec       spe        f1       geo       iba       sup

              0       0.82      0.95      0.47      0.88      0.67      0.47     39749
              1       0.80      0.47      0.95      0.59      0.67      0.42     15758

    avg / total       0.81      0.82      0.60      0.80      0.67      0.45     55507

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

**Fine tuning**

</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
net.get_layer('vgginnet_stem').trainable = True

for layer in net.get_layer('vgginnet_stem').layers:
    if layer.name.startswith('inception_block'):
        layer.trainable = True
    else:
        layer.trainable = False
net.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "hist_path_vgginet"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    vgginnet_stem (Functional)   (None, 3, 3, 736)         8670624   
    _________________________________________________________________
    flatten (Flatten)            (None, 6624)              0         
    _________________________________________________________________
    preds (Dense)                (None, 2)                 13250     
    =================================================================
    Total params: 8,683,874
    Trainable params: 1,045,666
    Non-trainable params: 7,638,208
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
net.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
initial_epochs = epochs
epochs = 25
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 15
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

callbacks.append(tf.keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")

plt.savefig(path + "/fine_tune_lr_schedule.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/14.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
ft_history = net.fit(train_ds, epochs=epochs, 
                     validation_data=val_ds,
                     callbacks=callbacks, verbose=1)
net.save_weights(path + '/ft_weights.h5')
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00001: LearningRateScheduler reducing learning rate to 1e-05.
    Epoch 1/25
    407/407 [==============================] - 295s 726ms/step - loss: 0.4190 - accuracy: 0.8356 - val_loss: 0.4139 - val_accuracy: 0.8409

    Epoch 00002: LearningRateScheduler reducing learning rate to 1.2666666666666668e-05.
    Epoch 2/25
    407/407 [==============================] - 296s 726ms/step - loss: 0.3888 - accuracy: 0.8437 - val_loss: 0.3936 - val_accuracy: 0.8447

    Epoch 00003: LearningRateScheduler reducing learning rate to 1.5333333333333334e-05.
    Epoch 3/25
    407/407 [==============================] - 297s 729ms/step - loss: 0.3749 - accuracy: 0.8459 - val_loss: 0.3785 - val_accuracy: 0.8481

    Epoch 00004: LearningRateScheduler reducing learning rate to 1.8000000000000004e-05.
    Epoch 4/25
    407/407 [==============================] - 297s 729ms/step - loss: 0.3682 - accuracy: 0.8479 - val_loss: 0.3674 - val_accuracy: 0.8515

    Epoch 00005: LearningRateScheduler reducing learning rate to 2.066666666666667e-05.
    Epoch 5/25
    407/407 [==============================] - 296s 727ms/step - loss: 0.3599 - accuracy: 0.8503 - val_loss: 0.3616 - val_accuracy: 0.8519

    Epoch 00006: LearningRateScheduler reducing learning rate to 2.3333333333333336e-05.
    Epoch 6/25
    407/407 [==============================] - 302s 742ms/step - loss: 0.3576 - accuracy: 0.8503 - val_loss: 0.3591 - val_accuracy: 0.8517

    Epoch 00007: LearningRateScheduler reducing learning rate to 2.6000000000000002e-05.
    Epoch 7/25
    407/407 [==============================] - 296s 727ms/step - loss: 0.3531 - accuracy: 0.8526 - val_loss: 0.3512 - val_accuracy: 0.8533

    Epoch 00008: LearningRateScheduler reducing learning rate to 2.8666666666666668e-05.
    Epoch 8/25
    407/407 [==============================] - 297s 730ms/step - loss: 0.3493 - accuracy: 0.8537 - val_loss: 0.3476 - val_accuracy: 0.8548

    Epoch 00009: LearningRateScheduler reducing learning rate to 3.1333333333333334e-05.
    Epoch 9/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3475 - accuracy: 0.8534 - val_loss: 0.3449 - val_accuracy: 0.8533

    Epoch 00010: LearningRateScheduler reducing learning rate to 3.4e-05.
    Epoch 10/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3442 - accuracy: 0.8541 - val_loss: 0.3436 - val_accuracy: 0.8543

    Epoch 00011: LearningRateScheduler reducing learning rate to 3.6666666666666666e-05.
    Epoch 11/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3404 - accuracy: 0.8563 - val_loss: 0.3418 - val_accuracy: 0.8562

    Epoch 00012: LearningRateScheduler reducing learning rate to 3.933333333333334e-05.
    Epoch 12/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3406 - accuracy: 0.8566 - val_loss: 0.3385 - val_accuracy: 0.8575

    Epoch 00013: LearningRateScheduler reducing learning rate to 4.2000000000000004e-05.
    Epoch 13/25
    407/407 [==============================] - 297s 729ms/step - loss: 0.3381 - accuracy: 0.8568 - val_loss: 0.3400 - val_accuracy: 0.8561

    Epoch 00014: LearningRateScheduler reducing learning rate to 4.466666666666667e-05.
    Epoch 14/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3362 - accuracy: 0.8580 - val_loss: 0.3353 - val_accuracy: 0.8583

    Epoch 00015: LearningRateScheduler reducing learning rate to 4.7333333333333336e-05.
    Epoch 15/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3328 - accuracy: 0.8593 - val_loss: 0.3322 - val_accuracy: 0.8591

    Epoch 00016: LearningRateScheduler reducing learning rate to 5e-05.
    Epoch 16/25
    407/407 [==============================] - 296s 726ms/step - loss: 0.3322 - accuracy: 0.8588 - val_loss: 0.3298 - val_accuracy: 0.8614

    Epoch 00017: LearningRateScheduler reducing learning rate to 4.2000000000000004e-05.
    Epoch 17/25
    407/407 [==============================] - 298s 731ms/step - loss: 0.3290 - accuracy: 0.8603 - val_loss: 0.3270 - val_accuracy: 0.8626

    Epoch 00018: LearningRateScheduler reducing learning rate to 3.5600000000000005e-05.
    Epoch 18/25
    407/407 [==============================] - 297s 731ms/step - loss: 0.3255 - accuracy: 0.8620 - val_loss: 0.3259 - val_accuracy: 0.8621

    Epoch 00019: LearningRateScheduler reducing learning rate to 3.0480000000000006e-05.
    Epoch 19/25
    407/407 [==============================] - 296s 726ms/step - loss: 0.3266 - accuracy: 0.8618 - val_loss: 0.3252 - val_accuracy: 0.8640

    Epoch 00020: LearningRateScheduler reducing learning rate to 2.6384000000000004e-05.
    Epoch 20/25
    407/407 [==============================] - 298s 732ms/step - loss: 0.3249 - accuracy: 0.8612 - val_loss: 0.3233 - val_accuracy: 0.8621

    Epoch 00021: LearningRateScheduler reducing learning rate to 2.3107200000000005e-05.
    Epoch 21/25
    407/407 [==============================] - 296s 728ms/step - loss: 0.3233 - accuracy: 0.8630 - val_loss: 0.3245 - val_accuracy: 0.8641

    Epoch 00022: LearningRateScheduler reducing learning rate to 2.0485760000000004e-05.
    Epoch 22/25
    407/407 [==============================] - 295s 725ms/step - loss: 0.3230 - accuracy: 0.8627 - val_loss: 0.3209 - val_accuracy: 0.8641

    Epoch 00023: LearningRateScheduler reducing learning rate to 1.8388608000000004e-05.
    Epoch 23/25
    407/407 [==============================] - 295s 724ms/step - loss: 0.3223 - accuracy: 0.8630 - val_loss: 0.3191 - val_accuracy: 0.8655

    Epoch 00024: LearningRateScheduler reducing learning rate to 1.6710886400000004e-05.
    Epoch 24/25
    407/407 [==============================] - 296s 727ms/step - loss: 0.3212 - accuracy: 0.8639 - val_loss: 0.3213 - val_accuracy: 0.8652

    Epoch 00025: LearningRateScheduler reducing learning rate to 1.5368709120000003e-05.
    Epoch 25/25
    407/407 [==============================] - 297s 729ms/step - loss: 0.3204 - accuracy: 0.8640 - val_loss: 0.3192 - val_accuracy: 0.8646

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
history = {}

history['accuracy'] = train_history.history['accuracy'] + ft_history.history['accuracy']
history['val_accuracy'] = train_history.history['val_accuracy'] + ft_history.history['val_accuracy']

history['loss'] = train_history.history['loss'] + ft_history.history['loss']
history['val_loss'] = train_history.history['val_loss'] + ft_history.history['val_loss']
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_plot(history, start_ft=initial_epochs)
plt.savefig(path + "/training_and_fine_tuning_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/15.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
eval_report(net, test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/16.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/17.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/18.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.866863638820329
                  precision    recall  f1-score   support

               0       0.89      0.92      0.91     39749
               1       0.79      0.72      0.76     15758

        accuracy                           0.87     55507
       macro avg       0.84      0.82      0.83     55507
    weighted avg       0.86      0.87      0.87     55507

                       pre       rec       spe        f1       geo       iba       sup

              0       0.89      0.92      0.72      0.91      0.82      0.68     39749
              1       0.79      0.72      0.92      0.76      0.82      0.66     15758

    avg / total       0.86      0.87      0.78      0.87      0.82      0.67     55507

</div>
</div>
</div>
</div>
</div>
</div>
</div>
