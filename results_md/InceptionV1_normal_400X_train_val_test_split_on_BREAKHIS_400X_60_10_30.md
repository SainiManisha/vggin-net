<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/InceptionV1-NORMAL-RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.1030 - accuracy: 0.3281WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    8/8 [==============================] - 25s 3s/step - loss: 0.8256 - accuracy: 0.5757 - val_loss: 0.8064 - val_accuracy: 0.7019
    Epoch 2/50
    8/8 [==============================] - 10s 1s/step - loss: 0.7331 - accuracy: 0.6746 - val_loss: 0.6249 - val_accuracy: 0.6832
    Epoch 3/50
    8/8 [==============================] - 10s 1s/step - loss: 0.6834 - accuracy: 0.6323 - val_loss: 0.5744 - val_accuracy: 0.7081
    Epoch 4/50
    8/8 [==============================] - 7s 864ms/step - loss: 0.5826 - accuracy: 0.7147 - val_loss: 0.5383 - val_accuracy: 0.7143
    Epoch 5/50
    8/8 [==============================] - 10s 1s/step - loss: 0.5407 - accuracy: 0.7271 - val_loss: 0.4988 - val_accuracy: 0.7764
    Epoch 6/50
    8/8 [==============================] - 10s 1s/step - loss: 0.5353 - accuracy: 0.7508 - val_loss: 0.4741 - val_accuracy: 0.8012
    Epoch 7/50
    8/8 [==============================] - 10s 1s/step - loss: 0.5156 - accuracy: 0.7497 - val_loss: 0.4525 - val_accuracy: 0.8012
    Epoch 8/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4879 - accuracy: 0.7837 - val_loss: 0.4375 - val_accuracy: 0.8199
    Epoch 9/50
    8/8 [==============================] - 7s 848ms/step - loss: 0.4633 - accuracy: 0.7909 - val_loss: 0.4244 - val_accuracy: 0.8323
    Epoch 10/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4654 - accuracy: 0.8002 - val_loss: 0.4187 - val_accuracy: 0.8137
    Epoch 11/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4532 - accuracy: 0.8023 - val_loss: 0.4067 - val_accuracy: 0.8261
    Epoch 12/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4531 - accuracy: 0.8033 - val_loss: 0.3966 - val_accuracy: 0.8447
    Epoch 13/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4307 - accuracy: 0.8095 - val_loss: 0.3913 - val_accuracy: 0.8385
    Epoch 14/50
    8/8 [==============================] - 7s 848ms/step - loss: 0.4066 - accuracy: 0.8229 - val_loss: 0.3902 - val_accuracy: 0.8385
    Epoch 15/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4137 - accuracy: 0.8218 - val_loss: 0.3771 - val_accuracy: 0.8385
    Epoch 16/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4141 - accuracy: 0.8229 - val_loss: 0.3734 - val_accuracy: 0.8385
    Epoch 17/50
    8/8 [==============================] - 10s 1s/step - loss: 0.4012 - accuracy: 0.8177 - val_loss: 0.3678 - val_accuracy: 0.8385
    Epoch 18/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3928 - accuracy: 0.8301 - val_loss: 0.3638 - val_accuracy: 0.8385
    Epoch 19/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3833 - accuracy: 0.8465 - val_loss: 0.3568 - val_accuracy: 0.8509
    Epoch 20/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3793 - accuracy: 0.8414 - val_loss: 0.3630 - val_accuracy: 0.8323
    Epoch 21/50
    8/8 [==============================] - 7s 844ms/step - loss: 0.3869 - accuracy: 0.8218 - val_loss: 0.3509 - val_accuracy: 0.8509
    Epoch 22/50
    8/8 [==============================] - 7s 864ms/step - loss: 0.3992 - accuracy: 0.8260 - val_loss: 0.3521 - val_accuracy: 0.8447
    Epoch 23/50
    8/8 [==============================] - 7s 842ms/step - loss: 0.3970 - accuracy: 0.8404 - val_loss: 0.3472 - val_accuracy: 0.8571
    Epoch 24/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3660 - accuracy: 0.8496 - val_loss: 0.3462 - val_accuracy: 0.8509
    Epoch 25/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3769 - accuracy: 0.8414 - val_loss: 0.3454 - val_accuracy: 0.8385
    Epoch 26/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3697 - accuracy: 0.8486 - val_loss: 0.3379 - val_accuracy: 0.8571
    Epoch 27/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3556 - accuracy: 0.8507 - val_loss: 0.3384 - val_accuracy: 0.8509
    Epoch 28/50
    8/8 [==============================] - 7s 845ms/step - loss: 0.3522 - accuracy: 0.8465 - val_loss: 0.3339 - val_accuracy: 0.8571
    Epoch 29/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3513 - accuracy: 0.8496 - val_loss: 0.3320 - val_accuracy: 0.8634
    Epoch 30/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3683 - accuracy: 0.8373 - val_loss: 0.3260 - val_accuracy: 0.8634
    Epoch 31/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3568 - accuracy: 0.8579 - val_loss: 0.3270 - val_accuracy: 0.8634
    Epoch 32/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3459 - accuracy: 0.8579 - val_loss: 0.3269 - val_accuracy: 0.8696
    Epoch 33/50
    8/8 [==============================] - 7s 852ms/step - loss: 0.3607 - accuracy: 0.8548 - val_loss: 0.3189 - val_accuracy: 0.8634
    Epoch 34/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3600 - accuracy: 0.8538 - val_loss: 0.3249 - val_accuracy: 0.8447
    Epoch 35/50
    8/8 [==============================] - 7s 854ms/step - loss: 0.3450 - accuracy: 0.8527 - val_loss: 0.3118 - val_accuracy: 0.8758
    Epoch 36/50
    8/8 [==============================] - 11s 1s/step - loss: 0.3487 - accuracy: 0.8641 - val_loss: 0.3112 - val_accuracy: 0.8882
    Epoch 37/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3601 - accuracy: 0.8486 - val_loss: 0.3085 - val_accuracy: 0.8882
    Epoch 38/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3568 - accuracy: 0.8579 - val_loss: 0.3125 - val_accuracy: 0.8820
    Epoch 39/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3489 - accuracy: 0.8496 - val_loss: 0.3085 - val_accuracy: 0.8820
    Epoch 40/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3409 - accuracy: 0.8568 - val_loss: 0.3052 - val_accuracy: 0.8882
    Epoch 41/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3501 - accuracy: 0.8527 - val_loss: 0.3046 - val_accuracy: 0.8820
    Epoch 42/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3496 - accuracy: 0.8630 - val_loss: 0.3002 - val_accuracy: 0.8820
    Epoch 43/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3538 - accuracy: 0.8465 - val_loss: 0.3034 - val_accuracy: 0.8634
    Epoch 44/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3298 - accuracy: 0.8661 - val_loss: 0.2998 - val_accuracy: 0.8820
    Epoch 45/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3299 - accuracy: 0.8548 - val_loss: 0.2995 - val_accuracy: 0.8820
    Epoch 46/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3403 - accuracy: 0.8682 - val_loss: 0.2999 - val_accuracy: 0.8696
    Epoch 47/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3253 - accuracy: 0.8744 - val_loss: 0.2972 - val_accuracy: 0.8758
    Epoch 48/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3322 - accuracy: 0.8558 - val_loss: 0.2969 - val_accuracy: 0.8696
    Epoch 49/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3333 - accuracy: 0.8723 - val_loss: 0.2938 - val_accuracy: 0.8820
    Epoch 50/50
    8/8 [==============================] - 10s 1s/step - loss: 0.3140 - accuracy: 0.8682 - val_loss: 0.3014 - val_accuracy: 0.8696

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/15.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 26s 6s/step - loss: 0.3234 - accuracy: 0.8668

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.32339346408843994, 0.8668032884597778]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8668032786885246
                  precision    recall  f1-score   support

               0       0.80      0.74      0.77       148
               1       0.89      0.92      0.91       340

        accuracy                           0.87       488
       macro avg       0.85      0.83      0.84       488
    weighted avg       0.86      0.87      0.87       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/16.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/17.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/18.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.80      0.74      0.77       148
               1       0.89      0.92      0.91       340

        accuracy                           0.87       488
       macro avg       0.85      0.83      0.84       488
    weighted avg       0.86      0.87      0.87       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.80      0.74      0.92      0.77      0.83      0.67       148
              1       0.89      0.92      0.74      0.91      0.83      0.70       340

    avg / total       0.86      0.87      0.80      0.87      0.83      0.69       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
