<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/InceptionV1-NORMAL-RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.5298 - accuracy: 0.2734WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 15s 2s/step - loss: 0.8699 - accuracy: 0.5399 - val_loss: 0.7390 - val_accuracy: 0.7112
    Epoch 2/50
    9/9 [==============================] - 5s 556ms/step - loss: 0.7039 - accuracy: 0.7066 - val_loss: 0.5792 - val_accuracy: 0.7166
    Epoch 3/50
    9/9 [==============================] - 5s 559ms/step - loss: 0.5922 - accuracy: 0.6862 - val_loss: 0.5570 - val_accuracy: 0.7487
    Epoch 4/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.5696 - accuracy: 0.7190 - val_loss: 0.5347 - val_accuracy: 0.7219
    Epoch 5/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.5304 - accuracy: 0.7420 - val_loss: 0.5020 - val_accuracy: 0.7647
    Epoch 6/50
    9/9 [==============================] - 5s 559ms/step - loss: 0.4999 - accuracy: 0.7722 - val_loss: 0.4825 - val_accuracy: 0.7701
    Epoch 7/50
    9/9 [==============================] - 5s 560ms/step - loss: 0.4739 - accuracy: 0.7917 - val_loss: 0.4688 - val_accuracy: 0.7701
    Epoch 8/50
    9/9 [==============================] - 5s 560ms/step - loss: 0.4729 - accuracy: 0.7775 - val_loss: 0.4573 - val_accuracy: 0.7861
    Epoch 9/50
    9/9 [==============================] - 5s 563ms/step - loss: 0.4521 - accuracy: 0.8023 - val_loss: 0.4471 - val_accuracy: 0.7914
    Epoch 10/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.4448 - accuracy: 0.7979 - val_loss: 0.4387 - val_accuracy: 0.8021
    Epoch 11/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.4177 - accuracy: 0.8191 - val_loss: 0.4297 - val_accuracy: 0.7968
    Epoch 12/50
    9/9 [==============================] - 5s 553ms/step - loss: 0.4342 - accuracy: 0.8032 - val_loss: 0.4221 - val_accuracy: 0.8021
    Epoch 13/50
    9/9 [==============================] - 5s 559ms/step - loss: 0.4047 - accuracy: 0.8307 - val_loss: 0.4147 - val_accuracy: 0.8021
    Epoch 14/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.4057 - accuracy: 0.8209 - val_loss: 0.4081 - val_accuracy: 0.8021
    Epoch 15/50
    9/9 [==============================] - 5s 559ms/step - loss: 0.3924 - accuracy: 0.8342 - val_loss: 0.4028 - val_accuracy: 0.8075
    Epoch 16/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3983 - accuracy: 0.8360 - val_loss: 0.3980 - val_accuracy: 0.8289
    Epoch 17/50
    9/9 [==============================] - 5s 551ms/step - loss: 0.3893 - accuracy: 0.8493 - val_loss: 0.3895 - val_accuracy: 0.8128
    Epoch 18/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3787 - accuracy: 0.8422 - val_loss: 0.3857 - val_accuracy: 0.8235
    Epoch 19/50
    9/9 [==============================] - 5s 556ms/step - loss: 0.3756 - accuracy: 0.8440 - val_loss: 0.3848 - val_accuracy: 0.8289
    Epoch 20/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3764 - accuracy: 0.8404 - val_loss: 0.3753 - val_accuracy: 0.8128
    Epoch 21/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.3646 - accuracy: 0.8422 - val_loss: 0.3741 - val_accuracy: 0.8289
    Epoch 22/50
    9/9 [==============================] - 5s 545ms/step - loss: 0.3743 - accuracy: 0.8413 - val_loss: 0.3764 - val_accuracy: 0.8503
    Epoch 23/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.3565 - accuracy: 0.8440 - val_loss: 0.3649 - val_accuracy: 0.8342
    Epoch 24/50
    9/9 [==============================] - 5s 561ms/step - loss: 0.3412 - accuracy: 0.8466 - val_loss: 0.3655 - val_accuracy: 0.8396
    Epoch 25/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.3405 - accuracy: 0.8661 - val_loss: 0.3614 - val_accuracy: 0.8396
    Epoch 26/50
    9/9 [==============================] - 5s 556ms/step - loss: 0.3558 - accuracy: 0.8466 - val_loss: 0.3588 - val_accuracy: 0.8396
    Epoch 27/50
    9/9 [==============================] - 5s 552ms/step - loss: 0.3351 - accuracy: 0.8670 - val_loss: 0.3560 - val_accuracy: 0.8342
    Epoch 28/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.3449 - accuracy: 0.8528 - val_loss: 0.3522 - val_accuracy: 0.8342
    Epoch 29/50
    9/9 [==============================] - 5s 562ms/step - loss: 0.3380 - accuracy: 0.8590 - val_loss: 0.3520 - val_accuracy: 0.8396
    Epoch 30/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.3340 - accuracy: 0.8555 - val_loss: 0.3450 - val_accuracy: 0.8396
    Epoch 31/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3346 - accuracy: 0.8582 - val_loss: 0.3499 - val_accuracy: 0.8503
    Epoch 32/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.3214 - accuracy: 0.8768 - val_loss: 0.3459 - val_accuracy: 0.8449
    Epoch 33/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.3255 - accuracy: 0.8688 - val_loss: 0.3376 - val_accuracy: 0.8449
    Epoch 34/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3097 - accuracy: 0.8741 - val_loss: 0.3406 - val_accuracy: 0.8503
    Epoch 35/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.3077 - accuracy: 0.8785 - val_loss: 0.3360 - val_accuracy: 0.8449
    Epoch 36/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.3240 - accuracy: 0.8546 - val_loss: 0.3343 - val_accuracy: 0.8503
    Epoch 37/50
    9/9 [==============================] - 5s 563ms/step - loss: 0.3128 - accuracy: 0.8652 - val_loss: 0.3310 - val_accuracy: 0.8503
    Epoch 38/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.3084 - accuracy: 0.8697 - val_loss: 0.3347 - val_accuracy: 0.8556
    Epoch 39/50
    9/9 [==============================] - 5s 559ms/step - loss: 0.3129 - accuracy: 0.8759 - val_loss: 0.3272 - val_accuracy: 0.8449
    Epoch 40/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.2989 - accuracy: 0.8794 - val_loss: 0.3289 - val_accuracy: 0.8556
    Epoch 41/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.2850 - accuracy: 0.8927 - val_loss: 0.3227 - val_accuracy: 0.8556
    Epoch 42/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.3012 - accuracy: 0.8688 - val_loss: 0.3286 - val_accuracy: 0.8556
    Epoch 43/50
    9/9 [==============================] - 5s 554ms/step - loss: 0.2920 - accuracy: 0.8768 - val_loss: 0.3214 - val_accuracy: 0.8503
    Epoch 44/50
    9/9 [==============================] - 5s 556ms/step - loss: 0.2990 - accuracy: 0.8723 - val_loss: 0.3196 - val_accuracy: 0.8556
    Epoch 45/50
    9/9 [==============================] - 5s 564ms/step - loss: 0.3068 - accuracy: 0.8794 - val_loss: 0.3173 - val_accuracy: 0.8610
    Epoch 46/50
    9/9 [==============================] - 5s 560ms/step - loss: 0.3038 - accuracy: 0.8715 - val_loss: 0.3201 - val_accuracy: 0.8556
    Epoch 47/50
    9/9 [==============================] - 5s 558ms/step - loss: 0.3022 - accuracy: 0.8856 - val_loss: 0.3179 - val_accuracy: 0.8556
    Epoch 48/50
    9/9 [==============================] - 5s 560ms/step - loss: 0.2904 - accuracy: 0.8812 - val_loss: 0.3126 - val_accuracy: 0.8610
    Epoch 49/50
    9/9 [==============================] - 5s 557ms/step - loss: 0.2922 - accuracy: 0.8839 - val_loss: 0.3160 - val_accuracy: 0.8503
    Epoch 50/50
    9/9 [==============================] - 5s 555ms/step - loss: 0.2915 - accuracy: 0.8794 - val_loss: 0.3086 - val_accuracy: 0.8556

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/249.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 30s 6s/step - loss: 0.3051 - accuracy: 0.8498

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.30513226985931396, 0.8498232960700989]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8498233215547704
                  precision    recall  f1-score   support

               0       0.79      0.66      0.72       164
               1       0.87      0.93      0.90       402

        accuracy                           0.85       566
       macro avg       0.83      0.79      0.81       566
    weighted avg       0.85      0.85      0.85       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: numpy&gt;=1.11.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/250.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/251.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/252.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.79      0.66      0.72       164
               1       0.87      0.93      0.90       402

        accuracy                           0.85       566
       macro avg       0.83      0.79      0.81       566
    weighted avg       0.85      0.85      0.85       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.79      0.66      0.93      0.72      0.78      0.59       164
              1       0.87      0.93      0.66      0.90      0.78      0.63       402

    avg / total       0.85      0.85      0.74      0.85      0.78      0.62       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
