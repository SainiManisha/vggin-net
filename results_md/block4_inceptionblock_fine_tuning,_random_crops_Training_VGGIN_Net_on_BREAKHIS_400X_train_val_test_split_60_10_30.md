<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK4-FINETUNING_400X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.1312 - accuracy: 0.5391WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 18s 2s/step - loss: 3.3506 - accuracy: 0.7188 - val_loss: 19.4570 - val_accuracy: 0.7702
    Epoch 2/100
    8/8 [==============================] - 8s 977ms/step - loss: 2.7961 - accuracy: 0.8342 - val_loss: 3.8594 - val_accuracy: 0.8944
    Epoch 3/100
    8/8 [==============================] - 11s 1s/step - loss: 2.1724 - accuracy: 0.8579 - val_loss: 3.1371 - val_accuracy: 0.8758
    Epoch 4/100
    8/8 [==============================] - 8s 985ms/step - loss: 1.5823 - accuracy: 0.8476 - val_loss: 9.2381 - val_accuracy: 0.6646
    Epoch 5/100
    8/8 [==============================] - 8s 940ms/step - loss: 1.2881 - accuracy: 0.8929 - val_loss: 8.0422 - val_accuracy: 0.8012
    Epoch 6/100
    8/8 [==============================] - 11s 1s/step - loss: 1.4956 - accuracy: 0.8857 - val_loss: 1.4556 - val_accuracy: 0.9255
    Epoch 7/100
    8/8 [==============================] - 11s 1s/step - loss: 1.0257 - accuracy: 0.8898 - val_loss: 1.7609 - val_accuracy: 0.9130
    Epoch 8/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9737 - accuracy: 0.9114 - val_loss: 1.2543 - val_accuracy: 0.9006
    Epoch 9/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8813 - accuracy: 0.9032 - val_loss: 1.2265 - val_accuracy: 0.9006
    Epoch 10/100
    8/8 [==============================] - 7s 931ms/step - loss: 0.9254 - accuracy: 0.8919 - val_loss: 1.1701 - val_accuracy: 0.9255
    Epoch 11/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8839 - accuracy: 0.9145 - val_loss: 1.2124 - val_accuracy: 0.9068
    Epoch 12/100
    8/8 [==============================] - 11s 1s/step - loss: 1.1503 - accuracy: 0.9001 - val_loss: 1.1434 - val_accuracy: 0.9130
    Epoch 13/100
    8/8 [==============================] - 11s 1s/step - loss: 1.0337 - accuracy: 0.9032 - val_loss: 1.5928 - val_accuracy: 0.9193
    Epoch 14/100
    8/8 [==============================] - 11s 1s/step - loss: 1.0181 - accuracy: 0.9053 - val_loss: 2.0601 - val_accuracy: 0.9130
    Epoch 15/100
    8/8 [==============================] - 11s 1s/step - loss: 1.1292 - accuracy: 0.9011 - val_loss: 1.7883 - val_accuracy: 0.9130
    Epoch 16/100
    8/8 [==============================] - 8s 959ms/step - loss: 1.0191 - accuracy: 0.9238 - val_loss: 1.3154 - val_accuracy: 0.9130
    Epoch 17/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9325 - accuracy: 0.9207 - val_loss: 0.9815 - val_accuracy: 0.9379
    Epoch 18/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8294 - accuracy: 0.9351 - val_loss: 1.4375 - val_accuracy: 0.9130
    Epoch 19/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7970 - accuracy: 0.9197 - val_loss: 1.2588 - val_accuracy: 0.9006
    Epoch 20/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9080 - accuracy: 0.9279 - val_loss: 1.1790 - val_accuracy: 0.9193
    Epoch 21/100
    8/8 [==============================] - 8s 951ms/step - loss: 0.8368 - accuracy: 0.9228 - val_loss: 0.8293 - val_accuracy: 0.9503
    Epoch 22/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2029 - accuracy: 0.9228 - val_loss: 1.8877 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2671 - accuracy: 0.9176 - val_loss: 1.3002 - val_accuracy: 0.9130
    Epoch 24/100
    8/8 [==============================] - 12s 1s/step - loss: 1.1058 - accuracy: 0.9176 - val_loss: 1.7179 - val_accuracy: 0.9255
    Epoch 25/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9545 - accuracy: 0.9238 - val_loss: 1.4283 - val_accuracy: 0.9441
    Epoch 26/100
    8/8 [==============================] - 11s 1s/step - loss: 1.1500 - accuracy: 0.9238 - val_loss: 1.5619 - val_accuracy: 0.9255
    Epoch 27/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7354 - accuracy: 0.9320 - val_loss: 1.8692 - val_accuracy: 0.9130
    Epoch 28/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6348 - accuracy: 0.9495 - val_loss: 0.9539 - val_accuracy: 0.9379
    Epoch 29/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8793 - accuracy: 0.9197 - val_loss: 1.3809 - val_accuracy: 0.9193
    Epoch 30/100
    8/8 [==============================] - 8s 1s/step - loss: 0.6084 - accuracy: 0.9526 - val_loss: 1.1883 - val_accuracy: 0.9317
    Epoch 31/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6643 - accuracy: 0.9423 - val_loss: 1.5651 - val_accuracy: 0.9068
    Epoch 32/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9444 - accuracy: 0.9331 - val_loss: 1.7621 - val_accuracy: 0.9379
    Epoch 33/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8139 - accuracy: 0.9423 - val_loss: 1.3431 - val_accuracy: 0.9379
    Epoch 34/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6851 - accuracy: 0.9434 - val_loss: 1.0728 - val_accuracy: 0.9317
    Epoch 35/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6865 - accuracy: 0.9403 - val_loss: 1.4508 - val_accuracy: 0.9255
    Epoch 36/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9697 - accuracy: 0.9392 - val_loss: 1.2782 - val_accuracy: 0.9379
    Epoch 37/100
    8/8 [==============================] - 14s 2s/step - loss: 0.6786 - accuracy: 0.9454 - val_loss: 1.2113 - val_accuracy: 0.9317
    Epoch 38/100
    8/8 [==============================] - 8s 989ms/step - loss: 0.5567 - accuracy: 0.9506 - val_loss: 1.3172 - val_accuracy: 0.9193
    Epoch 39/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5620 - accuracy: 0.9516 - val_loss: 1.0810 - val_accuracy: 0.9379
    Epoch 40/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7698 - accuracy: 0.9495 - val_loss: 1.2417 - val_accuracy: 0.9317
    Epoch 41/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6827 - accuracy: 0.9454 - val_loss: 2.0029 - val_accuracy: 0.9006
    Epoch 42/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9392 - accuracy: 0.9485 - val_loss: 1.5511 - val_accuracy: 0.9255
    Epoch 43/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6116 - accuracy: 0.9588 - val_loss: 1.6872 - val_accuracy: 0.8944
    Epoch 44/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4285 - accuracy: 0.9578 - val_loss: 2.9948 - val_accuracy: 0.8820
    Epoch 45/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5513 - accuracy: 0.9506 - val_loss: 2.6359 - val_accuracy: 0.8882
    Epoch 46/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5900 - accuracy: 0.9475 - val_loss: 1.7942 - val_accuracy: 0.9130
    Epoch 47/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6342 - accuracy: 0.9444 - val_loss: 1.7519 - val_accuracy: 0.9006
    Epoch 48/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5313 - accuracy: 0.9547 - val_loss: 3.5534 - val_accuracy: 0.8571
    Epoch 49/100
    8/8 [==============================] - 7s 927ms/step - loss: 0.8227 - accuracy: 0.9413 - val_loss: 2.3489 - val_accuracy: 0.9068
    Epoch 50/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6229 - accuracy: 0.9444 - val_loss: 2.5230 - val_accuracy: 0.9068
    Epoch 51/100
    8/8 [==============================] - 8s 999ms/step - loss: 0.5248 - accuracy: 0.9588 - val_loss: 2.7020 - val_accuracy: 0.9193
    Epoch 52/100
    8/8 [==============================] - 8s 944ms/step - loss: 0.4534 - accuracy: 0.9578 - val_loss: 2.1079 - val_accuracy: 0.9255
    Epoch 53/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4554 - accuracy: 0.9670 - val_loss: 1.8489 - val_accuracy: 0.9441
    Epoch 54/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4028 - accuracy: 0.9609 - val_loss: 3.0855 - val_accuracy: 0.8882
    Epoch 55/100
    8/8 [==============================] - 8s 994ms/step - loss: 0.5011 - accuracy: 0.9578 - val_loss: 3.4161 - val_accuracy: 0.8944
    Epoch 56/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5289 - accuracy: 0.9650 - val_loss: 2.1180 - val_accuracy: 0.9006
    Epoch 57/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2604 - accuracy: 0.9722 - val_loss: 2.1874 - val_accuracy: 0.9317
    Epoch 58/100
    8/8 [==============================] - 8s 997ms/step - loss: 0.7059 - accuracy: 0.9567 - val_loss: 1.3211 - val_accuracy: 0.9379
    Epoch 59/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5195 - accuracy: 0.9629 - val_loss: 1.6537 - val_accuracy: 0.8944
    Epoch 60/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4423 - accuracy: 0.9629 - val_loss: 1.5594 - val_accuracy: 0.9130
    Epoch 61/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6023 - accuracy: 0.9650 - val_loss: 1.5501 - val_accuracy: 0.9441
    Epoch 62/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4132 - accuracy: 0.9691 - val_loss: 2.3887 - val_accuracy: 0.9130
    Epoch 63/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2316 - accuracy: 0.9722 - val_loss: 1.9231 - val_accuracy: 0.9006
    Epoch 64/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3616 - accuracy: 0.9732 - val_loss: 1.4454 - val_accuracy: 0.9255
    Epoch 65/100
    8/8 [==============================] - 8s 994ms/step - loss: 0.4951 - accuracy: 0.9681 - val_loss: 1.4396 - val_accuracy: 0.9379
    Epoch 66/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5065 - accuracy: 0.9588 - val_loss: 3.3716 - val_accuracy: 0.8820
    Epoch 67/100
    8/8 [==============================] - 8s 994ms/step - loss: 0.3952 - accuracy: 0.9650 - val_loss: 1.4676 - val_accuracy: 0.9130
    Epoch 68/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3615 - accuracy: 0.9712 - val_loss: 2.3084 - val_accuracy: 0.9006
    Epoch 69/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3318 - accuracy: 0.9743 - val_loss: 1.8978 - val_accuracy: 0.9193
    Epoch 70/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6362 - accuracy: 0.9609 - val_loss: 1.5701 - val_accuracy: 0.9255
    Epoch 71/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3213 - accuracy: 0.9753 - val_loss: 1.7340 - val_accuracy: 0.9193
    Epoch 72/100
    8/8 [==============================] - 8s 990ms/step - loss: 0.5539 - accuracy: 0.9650 - val_loss: 1.3656 - val_accuracy: 0.9130
    Epoch 73/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5457 - accuracy: 0.9640 - val_loss: 1.3112 - val_accuracy: 0.9441
    Epoch 74/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6653 - accuracy: 0.9598 - val_loss: 1.5485 - val_accuracy: 0.9068
    Epoch 75/100
    8/8 [==============================] - 8s 941ms/step - loss: 0.6371 - accuracy: 0.9629 - val_loss: 2.1340 - val_accuracy: 0.9130
    Epoch 76/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4180 - accuracy: 0.9701 - val_loss: 1.7557 - val_accuracy: 0.9379
    Epoch 77/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4634 - accuracy: 0.9701 - val_loss: 1.3595 - val_accuracy: 0.9193
    Epoch 78/100
    8/8 [==============================] - 8s 984ms/step - loss: 0.3213 - accuracy: 0.9732 - val_loss: 2.2656 - val_accuracy: 0.9255
    Epoch 79/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4540 - accuracy: 0.9763 - val_loss: 2.5734 - val_accuracy: 0.9006
    Epoch 80/100
    8/8 [==============================] - 8s 991ms/step - loss: 0.2583 - accuracy: 0.9784 - val_loss: 2.4178 - val_accuracy: 0.9255
    Epoch 81/100
    8/8 [==============================] - 8s 997ms/step - loss: 0.2950 - accuracy: 0.9763 - val_loss: 1.5785 - val_accuracy: 0.9503
    Epoch 82/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6191 - accuracy: 0.9578 - val_loss: 1.9017 - val_accuracy: 0.9379
    Epoch 83/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4838 - accuracy: 0.9712 - val_loss: 2.7984 - val_accuracy: 0.9130
    Epoch 84/100
    8/8 [==============================] - 7s 920ms/step - loss: 0.4253 - accuracy: 0.9712 - val_loss: 3.4365 - val_accuracy: 0.8882
    Epoch 85/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4570 - accuracy: 0.9701 - val_loss: 2.8341 - val_accuracy: 0.9068
    Epoch 86/100
    8/8 [==============================] - 7s 928ms/step - loss: 0.3543 - accuracy: 0.9743 - val_loss: 1.6929 - val_accuracy: 0.9379
    Epoch 87/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5496 - accuracy: 0.9691 - val_loss: 1.7104 - val_accuracy: 0.9441
    Epoch 88/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3982 - accuracy: 0.9784 - val_loss: 1.9373 - val_accuracy: 0.9068
    Epoch 89/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5167 - accuracy: 0.9640 - val_loss: 2.4187 - val_accuracy: 0.8944
    Epoch 90/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2956 - accuracy: 0.9773 - val_loss: 1.9858 - val_accuracy: 0.9193
    Epoch 91/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2007 - accuracy: 0.9804 - val_loss: 2.3133 - val_accuracy: 0.9317
    Epoch 92/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3675 - accuracy: 0.9743 - val_loss: 3.2374 - val_accuracy: 0.8944
    Epoch 93/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3570 - accuracy: 0.9815 - val_loss: 2.5227 - val_accuracy: 0.8882
    Epoch 94/100
    8/8 [==============================] - 8s 975ms/step - loss: 0.3295 - accuracy: 0.9773 - val_loss: 2.8916 - val_accuracy: 0.9379
    Epoch 95/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5862 - accuracy: 0.9701 - val_loss: 4.5418 - val_accuracy: 0.8509
    Epoch 96/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3409 - accuracy: 0.9743 - val_loss: 3.1889 - val_accuracy: 0.8882
    Epoch 97/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3259 - accuracy: 0.9773 - val_loss: 1.4836 - val_accuracy: 0.9503
    Epoch 98/100
    8/8 [==============================] - 8s 979ms/step - loss: 0.3471 - accuracy: 0.9835 - val_loss: 2.6415 - val_accuracy: 0.9441
    Epoch 99/100
    8/8 [==============================] - 7s 935ms/step - loss: 0.2526 - accuracy: 0.9794 - val_loss: 2.2426 - val_accuracy: 0.9441
    Epoch 100/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4401 - accuracy: 0.9753 - val_loss: 3.3397 - val_accuracy: 0.9068

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/185.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 13s 3s/step - loss: 2.2113 - accuracy: 0.9262

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.211272716522217, 0.9262295365333557]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 7,222,178
    Non-trainable params: 1,736,960
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/186.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 128, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3979 - accuracy: 0.9681 - val_loss: 4.9362 - val_accuracy: 0.8944

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    8/8 [==============================] - 9s 1s/step - loss: 0.3581 - accuracy: 0.9773 - val_loss: 5.2738 - val_accuracy: 0.8820

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2426 - accuracy: 0.9835 - val_loss: 5.9627 - val_accuracy: 0.8820

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1809 - accuracy: 0.9794 - val_loss: 4.3379 - val_accuracy: 0.9130

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1592 - accuracy: 0.9876 - val_loss: 2.6752 - val_accuracy: 0.9379

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1647 - accuracy: 0.9856 - val_loss: 2.4964 - val_accuracy: 0.9441

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1417 - accuracy: 0.9897 - val_loss: 2.7165 - val_accuracy: 0.9441

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1258 - accuracy: 0.9938 - val_loss: 2.4193 - val_accuracy: 0.9441

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    8/8 [==============================] - 12s 1s/step - loss: 0.4042 - accuracy: 0.9825 - val_loss: 2.5194 - val_accuracy: 0.9379

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2303 - accuracy: 0.9866 - val_loss: 2.6442 - val_accuracy: 0.9379

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1719 - accuracy: 0.9825 - val_loss: 1.3974 - val_accuracy: 0.9565

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1644 - accuracy: 0.9846 - val_loss: 1.3497 - val_accuracy: 0.9565

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    8/8 [==============================] - 9s 1s/step - loss: 0.2138 - accuracy: 0.9846 - val_loss: 1.4859 - val_accuracy: 0.9627

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1321 - accuracy: 0.9897 - val_loss: 1.4593 - val_accuracy: 0.9689

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2288 - accuracy: 0.9825 - val_loss: 1.3489 - val_accuracy: 0.9379

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1851 - accuracy: 0.9866 - val_loss: 1.9004 - val_accuracy: 0.9255

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1752 - accuracy: 0.9846 - val_loss: 1.6499 - val_accuracy: 0.9317

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0958 - accuracy: 0.9907 - val_loss: 1.4880 - val_accuracy: 0.9255

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    8/8 [==============================] - 9s 1s/step - loss: 0.2308 - accuracy: 0.9825 - val_loss: 1.8095 - val_accuracy: 0.9379

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1982 - accuracy: 0.9876 - val_loss: 1.9487 - val_accuracy: 0.9441

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2558 - accuracy: 0.9856 - val_loss: 1.5599 - val_accuracy: 0.9441

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2612 - accuracy: 0.9804 - val_loss: 1.5556 - val_accuracy: 0.9317

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1522 - accuracy: 0.9928 - val_loss: 1.3229 - val_accuracy: 0.9441

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2221 - accuracy: 0.9897 - val_loss: 1.4174 - val_accuracy: 0.9317

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    8/8 [==============================] - 12s 1s/step - loss: 0.1036 - accuracy: 0.9907 - val_loss: 1.5304 - val_accuracy: 0.9503

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1794 - accuracy: 0.9866 - val_loss: 1.7351 - val_accuracy: 0.9441

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1860 - accuracy: 0.9897 - val_loss: 2.0580 - val_accuracy: 0.9317

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1541 - accuracy: 0.9897 - val_loss: 2.1385 - val_accuracy: 0.9379

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0561 - accuracy: 0.9928 - val_loss: 2.8876 - val_accuracy: 0.9130

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1709 - accuracy: 0.9897 - val_loss: 3.2764 - val_accuracy: 0.9068

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1732 - accuracy: 0.9866 - val_loss: 2.5375 - val_accuracy: 0.9193

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1073 - accuracy: 0.9856 - val_loss: 1.8615 - val_accuracy: 0.9379

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1605 - accuracy: 0.9866 - val_loss: 2.5685 - val_accuracy: 0.9193

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    8/8 [==============================] - 12s 1s/step - loss: 0.0349 - accuracy: 0.9949 - val_loss: 2.5043 - val_accuracy: 0.9193

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1790 - accuracy: 0.9856 - val_loss: 3.2267 - val_accuracy: 0.9130

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0396 - accuracy: 0.9918 - val_loss: 3.7291 - val_accuracy: 0.9068

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    8/8 [==============================] - 9s 1s/step - loss: 0.0749 - accuracy: 0.9938 - val_loss: 2.4895 - val_accuracy: 0.9255

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0823 - accuracy: 0.9928 - val_loss: 1.3720 - val_accuracy: 0.9503

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1455 - accuracy: 0.9897 - val_loss: 1.6415 - val_accuracy: 0.9379

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    8/8 [==============================] - 9s 1s/step - loss: 0.1917 - accuracy: 0.9835 - val_loss: 2.3027 - val_accuracy: 0.9317

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1128 - accuracy: 0.9918 - val_loss: 1.5980 - val_accuracy: 0.9441

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2148 - accuracy: 0.9835 - val_loss: 1.4767 - val_accuracy: 0.9503

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0817 - accuracy: 0.9918 - val_loss: 2.5537 - val_accuracy: 0.9068

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    8/8 [==============================] - 15s 2s/step - loss: 0.0654 - accuracy: 0.9938 - val_loss: 2.7719 - val_accuracy: 0.9006

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    8/8 [==============================] - 9s 1s/step - loss: 0.2180 - accuracy: 0.9815 - val_loss: 1.6481 - val_accuracy: 0.9379

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0914 - accuracy: 0.9928 - val_loss: 2.2997 - val_accuracy: 0.9379

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    8/8 [==============================] - 9s 1s/step - loss: 0.0987 - accuracy: 0.9866 - val_loss: 2.2846 - val_accuracy: 0.9255

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    8/8 [==============================] - 12s 1s/step - loss: 0.0652 - accuracy: 0.9938 - val_loss: 1.3095 - val_accuracy: 0.9441

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0316 - accuracy: 0.9938 - val_loss: 1.2296 - val_accuracy: 0.9379

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1390 - accuracy: 0.9866 - val_loss: 1.1197 - val_accuracy: 0.9379

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/187.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 4s 1s/step - loss: 1.9726 - accuracy: 0.9529

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.9726225137710571, 0.9528688788414001]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9528688524590164
                  precision    recall  f1-score   support

               0       0.90      0.95      0.92       148
               1       0.98      0.95      0.97       340

        accuracy                           0.95       488
       macro avg       0.94      0.95      0.95       488
    weighted avg       0.95      0.95      0.95       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/188.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/189.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/190.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.90      0.95      0.92       148
               1       0.98      0.95      0.97       340

        accuracy                           0.95       488
       macro avg       0.94      0.95      0.95       488
    weighted avg       0.95      0.95      0.95       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.90      0.95      0.95      0.92      0.95      0.91       148
              1       0.98      0.95      0.95      0.97      0.95      0.91       340

    avg / total       0.95      0.95      0.95      0.95      0.95      0.91       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
