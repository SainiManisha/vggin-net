<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/VGG16-NORMAL-RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_40X/train'
val_path = '../Splitted_40X/val'
test_path = '../Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.5310 - accuracy: 0.7031WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 35s 4s/step - loss: 22.9599 - accuracy: 0.6054 - val_loss: 0.7818 - val_accuracy: 0.7095
    Epoch 2/50
    9/9 [==============================] - 15s 2s/step - loss: 0.8876 - accuracy: 0.6165 - val_loss: 0.5719 - val_accuracy: 0.7095
    Epoch 3/50
    9/9 [==============================] - 18s 2s/step - loss: 0.7915 - accuracy: 0.6555 - val_loss: 1.0795 - val_accuracy: 0.7095
    Epoch 4/50
    9/9 [==============================] - 19s 2s/step - loss: 0.6223 - accuracy: 0.7307 - val_loss: 0.4250 - val_accuracy: 0.8212
    Epoch 5/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4993 - accuracy: 0.7911 - val_loss: 0.3677 - val_accuracy: 0.8380
    Epoch 6/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3931 - accuracy: 0.8347 - val_loss: 0.4295 - val_accuracy: 0.8212
    Epoch 7/50
    9/9 [==============================] - 19s 2s/step - loss: 0.6800 - accuracy: 0.7539 - val_loss: 0.3296 - val_accuracy: 0.8492
    Epoch 8/50
    9/9 [==============================] - 18s 2s/step - loss: 0.5122 - accuracy: 0.7744 - val_loss: 0.4365 - val_accuracy: 0.8547
    Epoch 9/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4117 - accuracy: 0.8227 - val_loss: 0.3313 - val_accuracy: 0.8492
    Epoch 10/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3478 - accuracy: 0.8663 - val_loss: 0.3209 - val_accuracy: 0.8603
    Epoch 11/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4235 - accuracy: 0.8394 - val_loss: 0.3172 - val_accuracy: 0.8715
    Epoch 12/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3103 - accuracy: 0.8774 - val_loss: 0.2958 - val_accuracy: 0.8771
    Epoch 13/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2957 - accuracy: 0.8867 - val_loss: 0.2888 - val_accuracy: 0.8827
    Epoch 14/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2635 - accuracy: 0.8988 - val_loss: 0.2860 - val_accuracy: 0.8827
    Epoch 15/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2808 - accuracy: 0.8821 - val_loss: 0.2802 - val_accuracy: 0.8771
    Epoch 16/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2521 - accuracy: 0.8969 - val_loss: 0.2965 - val_accuracy: 0.8771
    Epoch 17/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2856 - accuracy: 0.8867 - val_loss: 0.2703 - val_accuracy: 0.8939
    Epoch 18/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2445 - accuracy: 0.9044 - val_loss: 0.2730 - val_accuracy: 0.8827
    Epoch 19/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2295 - accuracy: 0.9071 - val_loss: 0.3286 - val_accuracy: 0.8771
    Epoch 20/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2756 - accuracy: 0.8960 - val_loss: 0.2796 - val_accuracy: 0.8771
    Epoch 21/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2248 - accuracy: 0.9109 - val_loss: 0.2968 - val_accuracy: 0.8715
    Epoch 22/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1806 - accuracy: 0.9350 - val_loss: 0.2984 - val_accuracy: 0.8715
    Epoch 23/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1863 - accuracy: 0.9201 - val_loss: 0.2982 - val_accuracy: 0.8771
    Epoch 24/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2168 - accuracy: 0.9211 - val_loss: 0.2908 - val_accuracy: 0.8771
    Epoch 25/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1913 - accuracy: 0.9239 - val_loss: 0.2860 - val_accuracy: 0.8883
    Epoch 26/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1876 - accuracy: 0.9276 - val_loss: 0.3092 - val_accuracy: 0.8939
    Epoch 27/50
    9/9 [==============================] - 17s 2s/step - loss: 0.2102 - accuracy: 0.9192 - val_loss: 0.3367 - val_accuracy: 0.8939
    Epoch 28/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1711 - accuracy: 0.9387 - val_loss: 0.3269 - val_accuracy: 0.8939
    Epoch 29/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1989 - accuracy: 0.9192 - val_loss: 0.2800 - val_accuracy: 0.8939
    Epoch 30/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1994 - accuracy: 0.9304 - val_loss: 0.2739 - val_accuracy: 0.8939
    Epoch 31/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1818 - accuracy: 0.9378 - val_loss: 0.2627 - val_accuracy: 0.8939
    Epoch 32/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1662 - accuracy: 0.9406 - val_loss: 0.2775 - val_accuracy: 0.8994
    Epoch 33/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1411 - accuracy: 0.9480 - val_loss: 0.2712 - val_accuracy: 0.9050
    Epoch 34/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1639 - accuracy: 0.9434 - val_loss: 0.2858 - val_accuracy: 0.9050
    Epoch 35/50
    9/9 [==============================] - 17s 2s/step - loss: 0.1606 - accuracy: 0.9387 - val_loss: 0.2960 - val_accuracy: 0.8994
    Epoch 36/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1920 - accuracy: 0.9350 - val_loss: 0.5651 - val_accuracy: 0.8659
    Epoch 37/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4125 - accuracy: 0.8765 - val_loss: 0.3119 - val_accuracy: 0.8883
    Epoch 38/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2639 - accuracy: 0.9053 - val_loss: 0.2589 - val_accuracy: 0.9106
    Epoch 39/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3487 - accuracy: 0.8747 - val_loss: 0.2426 - val_accuracy: 0.9050
    Epoch 40/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1775 - accuracy: 0.9285 - val_loss: 0.2653 - val_accuracy: 0.8994
    Epoch 41/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2105 - accuracy: 0.9174 - val_loss: 0.3039 - val_accuracy: 0.8939
    Epoch 42/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1708 - accuracy: 0.9406 - val_loss: 0.2695 - val_accuracy: 0.8994
    Epoch 43/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1586 - accuracy: 0.9369 - val_loss: 0.3060 - val_accuracy: 0.8939
    Epoch 44/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1489 - accuracy: 0.9341 - val_loss: 0.2888 - val_accuracy: 0.8939
    Epoch 45/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1367 - accuracy: 0.9508 - val_loss: 0.2842 - val_accuracy: 0.8939
    Epoch 46/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1578 - accuracy: 0.9452 - val_loss: 0.2939 - val_accuracy: 0.8994
    Epoch 47/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1626 - accuracy: 0.9443 - val_loss: 0.2992 - val_accuracy: 0.8939
    Epoch 48/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1609 - accuracy: 0.9424 - val_loss: 0.3354 - val_accuracy: 0.8994
    Epoch 49/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1489 - accuracy: 0.9415 - val_loss: 0.2622 - val_accuracy: 0.9050
    Epoch 50/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1432 - accuracy: 0.9424 - val_loss: 0.2944 - val_accuracy: 0.9050

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/107.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 33s 7s/step - loss: 0.2101 - accuracy: 0.9295

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.21014811098575592, 0.929499089717865]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9294990723562152
                  precision    recall  f1-score   support

               0       0.95      0.80      0.87       158
               1       0.92      0.98      0.95       381

        accuracy                           0.93       539
       macro avg       0.94      0.89      0.91       539
    weighted avg       0.93      0.93      0.93       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: numpy&gt;=1.11.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/108.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/109.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/110.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.80      0.87       158
               1       0.92      0.98      0.95       381

        accuracy                           0.93       539
       macro avg       0.94      0.89      0.91       539
    weighted avg       0.93      0.93      0.93       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.80      0.98      0.87      0.89      0.77       158
              1       0.92      0.98      0.80      0.95      0.89      0.80       381

    avg / total       0.93      0.93      0.85      0.93      0.89      0.79       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
