<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK2andBLOCK3and4-FINETUNING_200X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.9509 - accuracy: 0.6094WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 22s 2s/step - loss: 3.7193 - accuracy: 0.7433 - val_loss: 8.8633 - val_accuracy: 0.8508
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 1.5556 - accuracy: 0.8740 - val_loss: 19.3383 - val_accuracy: 0.7514
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3810 - accuracy: 0.8712 - val_loss: 2.8252 - val_accuracy: 0.9006
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0340 - accuracy: 0.9016 - val_loss: 5.0294 - val_accuracy: 0.8398
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0330 - accuracy: 0.9071 - val_loss: 1.7801 - val_accuracy: 0.9116
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1950 - accuracy: 0.8979 - val_loss: 3.3464 - val_accuracy: 0.8729
    Epoch 7/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7478 - accuracy: 0.9200 - val_loss: 1.6631 - val_accuracy: 0.9337
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6692 - accuracy: 0.9246 - val_loss: 1.9788 - val_accuracy: 0.8950
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8071 - accuracy: 0.9089 - val_loss: 1.1194 - val_accuracy: 0.9282
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8590 - accuracy: 0.9190 - val_loss: 2.8028 - val_accuracy: 0.8950
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5444 - accuracy: 0.9393 - val_loss: 1.4787 - val_accuracy: 0.9171
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7192 - accuracy: 0.9246 - val_loss: 0.4862 - val_accuracy: 0.9503
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0230 - accuracy: 0.9172 - val_loss: 1.5049 - val_accuracy: 0.9116
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7768 - accuracy: 0.9365 - val_loss: 2.1487 - val_accuracy: 0.9116
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7857 - accuracy: 0.9310 - val_loss: 2.7705 - val_accuracy: 0.8840
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5956 - accuracy: 0.9393 - val_loss: 0.5737 - val_accuracy: 0.9503
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9345 - accuracy: 0.9273 - val_loss: 0.6841 - val_accuracy: 0.9227
    Epoch 18/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6066 - accuracy: 0.9448 - val_loss: 1.3461 - val_accuracy: 0.9171
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9940 - accuracy: 0.9347 - val_loss: 1.5701 - val_accuracy: 0.9227
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8296 - accuracy: 0.9356 - val_loss: 0.7890 - val_accuracy: 0.9171
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6334 - accuracy: 0.9494 - val_loss: 0.6459 - val_accuracy: 0.9448
    Epoch 22/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7679 - accuracy: 0.9393 - val_loss: 1.0178 - val_accuracy: 0.9558
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8422 - accuracy: 0.9338 - val_loss: 1.0237 - val_accuracy: 0.9282
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4816 - accuracy: 0.9540 - val_loss: 0.7470 - val_accuracy: 0.9392
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5907 - accuracy: 0.9485 - val_loss: 0.7107 - val_accuracy: 0.9392
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5835 - accuracy: 0.9457 - val_loss: 0.8448 - val_accuracy: 0.9448
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8103 - accuracy: 0.9402 - val_loss: 0.8322 - val_accuracy: 0.9448
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0565 - accuracy: 0.9384 - val_loss: 0.6957 - val_accuracy: 0.9392
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8805 - accuracy: 0.9420 - val_loss: 0.5004 - val_accuracy: 0.9448
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7812 - accuracy: 0.9430 - val_loss: 1.0418 - val_accuracy: 0.9503
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8915 - accuracy: 0.9439 - val_loss: 0.9581 - val_accuracy: 0.9558
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7616 - accuracy: 0.9568 - val_loss: 0.8827 - val_accuracy: 0.9558
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5898 - accuracy: 0.9503 - val_loss: 0.4905 - val_accuracy: 0.9669
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5372 - accuracy: 0.9623 - val_loss: 0.8457 - val_accuracy: 0.9503
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5766 - accuracy: 0.9586 - val_loss: 0.7733 - val_accuracy: 0.9558
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6827 - accuracy: 0.9540 - val_loss: 1.9544 - val_accuracy: 0.9392
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5984 - accuracy: 0.9549 - val_loss: 3.2628 - val_accuracy: 0.9337
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4998 - accuracy: 0.9632 - val_loss: 1.1859 - val_accuracy: 0.9503
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6011 - accuracy: 0.9595 - val_loss: 0.6888 - val_accuracy: 0.9724
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6491 - accuracy: 0.9595 - val_loss: 1.4413 - val_accuracy: 0.9392
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4669 - accuracy: 0.9632 - val_loss: 2.9654 - val_accuracy: 0.9337
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6315 - accuracy: 0.9604 - val_loss: 0.8354 - val_accuracy: 0.9669
    Epoch 43/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4629 - accuracy: 0.9650 - val_loss: 1.1875 - val_accuracy: 0.9558
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3908 - accuracy: 0.9614 - val_loss: 1.3255 - val_accuracy: 0.9558
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3849 - accuracy: 0.9715 - val_loss: 0.6218 - val_accuracy: 0.9779
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4354 - accuracy: 0.9706 - val_loss: 0.8365 - val_accuracy: 0.9613
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4817 - accuracy: 0.9660 - val_loss: 0.9857 - val_accuracy: 0.9724
    Epoch 48/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4726 - accuracy: 0.9660 - val_loss: 0.6706 - val_accuracy: 0.9779
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6273 - accuracy: 0.9632 - val_loss: 0.5309 - val_accuracy: 0.9669
    Epoch 50/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2047 - accuracy: 0.9788 - val_loss: 0.8538 - val_accuracy: 0.9669
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2504 - accuracy: 0.9788 - val_loss: 0.7688 - val_accuracy: 0.9558
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4022 - accuracy: 0.9770 - val_loss: 0.4704 - val_accuracy: 0.9669
    Epoch 53/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6563 - accuracy: 0.9604 - val_loss: 0.4640 - val_accuracy: 0.9724
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4839 - accuracy: 0.9641 - val_loss: 0.4807 - val_accuracy: 0.9779
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6430 - accuracy: 0.9604 - val_loss: 2.1185 - val_accuracy: 0.9392
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6677 - accuracy: 0.9696 - val_loss: 0.8641 - val_accuracy: 0.9613
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4656 - accuracy: 0.9660 - val_loss: 0.5581 - val_accuracy: 0.9613
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5350 - accuracy: 0.9752 - val_loss: 0.8505 - val_accuracy: 0.9613
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3380 - accuracy: 0.9742 - val_loss: 0.9781 - val_accuracy: 0.9779
    Epoch 60/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2938 - accuracy: 0.9752 - val_loss: 0.8780 - val_accuracy: 0.9724
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5136 - accuracy: 0.9641 - val_loss: 0.7225 - val_accuracy: 0.9669
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3270 - accuracy: 0.9733 - val_loss: 0.7855 - val_accuracy: 0.9558
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2812 - accuracy: 0.9788 - val_loss: 1.3241 - val_accuracy: 0.9503
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1889 - accuracy: 0.9816 - val_loss: 0.7093 - val_accuracy: 0.9613
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2112 - accuracy: 0.9816 - val_loss: 0.9065 - val_accuracy: 0.9613
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2135 - accuracy: 0.9798 - val_loss: 1.5336 - val_accuracy: 0.9337
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1694 - accuracy: 0.9825 - val_loss: 0.8162 - val_accuracy: 0.9724
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2584 - accuracy: 0.9807 - val_loss: 0.5342 - val_accuracy: 0.9724
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5155 - accuracy: 0.9715 - val_loss: 0.4882 - val_accuracy: 0.9779
    Epoch 70/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4367 - accuracy: 0.9742 - val_loss: 1.1952 - val_accuracy: 0.9669
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4864 - accuracy: 0.9715 - val_loss: 0.6930 - val_accuracy: 0.9779
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3792 - accuracy: 0.9770 - val_loss: 0.8335 - val_accuracy: 0.9779
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1687 - accuracy: 0.9844 - val_loss: 1.1874 - val_accuracy: 0.9669
    Epoch 74/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3995 - accuracy: 0.9770 - val_loss: 1.1900 - val_accuracy: 0.9724
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3528 - accuracy: 0.9807 - val_loss: 1.3845 - val_accuracy: 0.9724
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3135 - accuracy: 0.9834 - val_loss: 0.6144 - val_accuracy: 0.9724
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1792 - accuracy: 0.9807 - val_loss: 0.3612 - val_accuracy: 0.9724
    Epoch 78/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0979 - accuracy: 0.9908 - val_loss: 0.4429 - val_accuracy: 0.9558
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2230 - accuracy: 0.9798 - val_loss: 1.2290 - val_accuracy: 0.9503
    Epoch 80/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2684 - accuracy: 0.9853 - val_loss: 0.9936 - val_accuracy: 0.9558
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2078 - accuracy: 0.9807 - val_loss: 1.4958 - val_accuracy: 0.9558
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3230 - accuracy: 0.9798 - val_loss: 1.7245 - val_accuracy: 0.9613
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2704 - accuracy: 0.9825 - val_loss: 0.8447 - val_accuracy: 0.9613
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2688 - accuracy: 0.9770 - val_loss: 1.0679 - val_accuracy: 0.9558
    Epoch 85/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2709 - accuracy: 0.9807 - val_loss: 1.7704 - val_accuracy: 0.9337
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2821 - accuracy: 0.9853 - val_loss: 2.0409 - val_accuracy: 0.9392
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4613 - accuracy: 0.9770 - val_loss: 1.1148 - val_accuracy: 0.9724
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2693 - accuracy: 0.9816 - val_loss: 1.8715 - val_accuracy: 0.9613
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3946 - accuracy: 0.9724 - val_loss: 2.0115 - val_accuracy: 0.9669
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3160 - accuracy: 0.9788 - val_loss: 1.1236 - val_accuracy: 0.9613
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3632 - accuracy: 0.9715 - val_loss: 1.0089 - val_accuracy: 0.9558
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3190 - accuracy: 0.9853 - val_loss: 1.6925 - val_accuracy: 0.9613
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2041 - accuracy: 0.9862 - val_loss: 0.7918 - val_accuracy: 0.9724
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1919 - accuracy: 0.9825 - val_loss: 1.0561 - val_accuracy: 0.9558
    Epoch 95/100
    9/9 [==============================] - 18s 2s/step - loss: 0.3001 - accuracy: 0.9825 - val_loss: 0.7763 - val_accuracy: 0.9613
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1858 - accuracy: 0.9899 - val_loss: 0.5009 - val_accuracy: 0.9669
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1312 - accuracy: 0.9890 - val_loss: 0.3286 - val_accuracy: 0.9724
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2166 - accuracy: 0.9899 - val_loss: 0.4774 - val_accuracy: 0.9724
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2542 - accuracy: 0.9853 - val_loss: 1.2468 - val_accuracy: 0.9613
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2667 - accuracy: 0.9779 - val_loss: 2.3313 - val_accuracy: 0.9669

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/169.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 13s 3s/step - loss: 2.1141 - accuracy: 0.9615

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.1140568256378174, 0.9614678621292114]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3') or layer.name.startswith('block2'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,918,946
    Non-trainable params: 40,192
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/170.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
     2/17 [==&gt;...........................] - ETA: 5s - loss: 0.4397 - accuracy: 0.9766WARNING:tensorflow:Callbacks method `on_train_batch_end` is slow compared to the batch time (batch time: 0.2274s vs `on_train_batch_end` time: 0.4654s). Check your callbacks.
    17/17 [==============================] - 28s 2s/step - loss: 0.6745 - accuracy: 0.9715 - val_loss: 2.5208 - val_accuracy: 0.9061

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2586 - accuracy: 0.9890 - val_loss: 0.6586 - val_accuracy: 0.9834

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2850 - accuracy: 0.9853 - val_loss: 4.4012 - val_accuracy: 0.8729

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 28s 2s/step - loss: 0.3597 - accuracy: 0.9788 - val_loss: 1.1932 - val_accuracy: 0.9669

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 25s 1s/step - loss: 0.3104 - accuracy: 0.9816 - val_loss: 2.5356 - val_accuracy: 0.9724

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2792 - accuracy: 0.9834 - val_loss: 2.1314 - val_accuracy: 0.9779

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2646 - accuracy: 0.9844 - val_loss: 1.0905 - val_accuracy: 0.9779

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 26s 2s/step - loss: 0.2184 - accuracy: 0.9825 - val_loss: 2.3587 - val_accuracy: 0.9337

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1703 - accuracy: 0.9908 - val_loss: 1.6420 - val_accuracy: 0.9613

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1423 - accuracy: 0.9880 - val_loss: 2.0174 - val_accuracy: 0.9613

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1893 - accuracy: 0.9899 - val_loss: 0.6233 - val_accuracy: 0.9779

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1739 - accuracy: 0.9880 - val_loss: 4.5440 - val_accuracy: 0.8674

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1686 - accuracy: 0.9890 - val_loss: 0.7527 - val_accuracy: 0.9613

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 25s 1s/step - loss: 0.2487 - accuracy: 0.9899 - val_loss: 1.0027 - val_accuracy: 0.9613

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2231 - accuracy: 0.9890 - val_loss: 1.0933 - val_accuracy: 0.9779

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1624 - accuracy: 0.9880 - val_loss: 0.6572 - val_accuracy: 0.9613

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1027 - accuracy: 0.9945 - val_loss: 1.8058 - val_accuracy: 0.9779

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1039 - accuracy: 0.9926 - val_loss: 0.9897 - val_accuracy: 0.9724

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 28s 2s/step - loss: 0.3890 - accuracy: 0.9834 - val_loss: 2.2861 - val_accuracy: 0.9337

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2324 - accuracy: 0.9880 - val_loss: 1.5805 - val_accuracy: 0.9558

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1662 - accuracy: 0.9880 - val_loss: 1.9740 - val_accuracy: 0.9337

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 26s 2s/step - loss: 0.3739 - accuracy: 0.9816 - val_loss: 3.9722 - val_accuracy: 0.9282

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 26s 2s/step - loss: 0.2679 - accuracy: 0.9807 - val_loss: 0.9528 - val_accuracy: 0.9613

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 27s 2s/step - loss: 0.3130 - accuracy: 0.9816 - val_loss: 0.6816 - val_accuracy: 0.9779

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 28s 2s/step - loss: 0.2289 - accuracy: 0.9871 - val_loss: 1.5114 - val_accuracy: 0.9392

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 27s 2s/step - loss: 0.0463 - accuracy: 0.9945 - val_loss: 0.8671 - val_accuracy: 0.9724

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1084 - accuracy: 0.9926 - val_loss: 1.2055 - val_accuracy: 0.9337

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1023 - accuracy: 0.9899 - val_loss: 0.5333 - val_accuracy: 0.9613

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 28s 2s/step - loss: 0.2065 - accuracy: 0.9899 - val_loss: 1.8129 - val_accuracy: 0.9392

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 25s 1s/step - loss: 0.3089 - accuracy: 0.9807 - val_loss: 1.2589 - val_accuracy: 0.9724

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 28s 2s/step - loss: 0.0642 - accuracy: 0.9963 - val_loss: 1.7925 - val_accuracy: 0.9669

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1407 - accuracy: 0.9890 - val_loss: 1.8051 - val_accuracy: 0.9448

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0809 - accuracy: 0.9926 - val_loss: 1.0736 - val_accuracy: 0.9669

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 29s 2s/step - loss: 0.3023 - accuracy: 0.9890 - val_loss: 1.9208 - val_accuracy: 0.9392

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0434 - accuracy: 0.9926 - val_loss: 2.4077 - val_accuracy: 0.9392

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1751 - accuracy: 0.9908 - val_loss: 2.3171 - val_accuracy: 0.9337

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1987 - accuracy: 0.9890 - val_loss: 1.7696 - val_accuracy: 0.9392

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1310 - accuracy: 0.9917 - val_loss: 0.5962 - val_accuracy: 0.9724

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1627 - accuracy: 0.9926 - val_loss: 0.4011 - val_accuracy: 0.9779

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 24s 1s/step - loss: 0.0742 - accuracy: 0.9926 - val_loss: 1.2048 - val_accuracy: 0.9613

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 28s 2s/step - loss: 0.0640 - accuracy: 0.9963 - val_loss: 0.7923 - val_accuracy: 0.9669

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1073 - accuracy: 0.9936 - val_loss: 0.4488 - val_accuracy: 0.9724

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1194 - accuracy: 0.9945 - val_loss: 1.1245 - val_accuracy: 0.9724

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 27s 2s/step - loss: 0.1896 - accuracy: 0.9899 - val_loss: 4.2315 - val_accuracy: 0.9613

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 28s 2s/step - loss: 0.1474 - accuracy: 0.9899 - val_loss: 0.8551 - val_accuracy: 0.9392

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 27s 2s/step - loss: 0.0983 - accuracy: 0.9926 - val_loss: 0.9361 - val_accuracy: 0.9724

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 27s 2s/step - loss: 0.0715 - accuracy: 0.9936 - val_loss: 0.7871 - val_accuracy: 0.9724

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 28s 2s/step - loss: 0.0742 - accuracy: 0.9936 - val_loss: 1.9932 - val_accuracy: 0.9337

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0589 - accuracy: 0.9936 - val_loss: 1.6071 - val_accuracy: 0.9669

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 27s 2s/step - loss: 0.0630 - accuracy: 0.9945 - val_loss: 1.7884 - val_accuracy: 0.9558

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/171.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 1.9791 - accuracy: 0.9468

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.9790918827056885, 0.9467889666557312]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9467889908256881
                  precision    recall  f1-score   support

               0       0.86      0.97      0.91       158
               1       0.99      0.94      0.96       387

        accuracy                           0.95       545
       macro avg       0.92      0.96      0.94       545
    weighted avg       0.95      0.95      0.95       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/172.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/173.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/174.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.86      0.97      0.91       158
               1       0.99      0.94      0.96       387

        accuracy                           0.95       545
       macro avg       0.92      0.96      0.94       545
    weighted avg       0.95      0.95      0.95       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.86      0.97      0.94      0.91      0.95      0.92       158
              1       0.99      0.94      0.97      0.96      0.95      0.91       387

    avg / total       0.95      0.95      0.96      0.95      0.95      0.91       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
