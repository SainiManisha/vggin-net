<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK2andBLOCK3and4-FINETUNING_100X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3750 - accuracy: 0.4141WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 4.8157 - accuracy: 0.7190 - val_loss: 4.0672 - val_accuracy: 0.9198
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 2.5854 - accuracy: 0.8661 - val_loss: 2.2301 - val_accuracy: 0.9091
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.6800 - accuracy: 0.8697 - val_loss: 2.1974 - val_accuracy: 0.9144
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 1.3197 - accuracy: 0.8839 - val_loss: 2.0777 - val_accuracy: 0.9037
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1992 - accuracy: 0.8998 - val_loss: 3.8471 - val_accuracy: 0.8182
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0202 - accuracy: 0.9069 - val_loss: 2.0983 - val_accuracy: 0.8984
    Epoch 7/100
    9/9 [==============================] - 15s 2s/step - loss: 1.5256 - accuracy: 0.8839 - val_loss: 1.8433 - val_accuracy: 0.9198
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1799 - accuracy: 0.9034 - val_loss: 1.6340 - val_accuracy: 0.9037
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8240 - accuracy: 0.9211 - val_loss: 1.3007 - val_accuracy: 0.9037
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7863 - accuracy: 0.9344 - val_loss: 1.5154 - val_accuracy: 0.8824
    Epoch 11/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7317 - accuracy: 0.9184 - val_loss: 1.3507 - val_accuracy: 0.8984
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6772 - accuracy: 0.9317 - val_loss: 1.0080 - val_accuracy: 0.9358
    Epoch 13/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8990 - accuracy: 0.9149 - val_loss: 1.6239 - val_accuracy: 0.9358
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7744 - accuracy: 0.9353 - val_loss: 0.6482 - val_accuracy: 0.9358
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6142 - accuracy: 0.9353 - val_loss: 1.0329 - val_accuracy: 0.9305
    Epoch 16/100
    9/9 [==============================] - 18s 2s/step - loss: 0.7032 - accuracy: 0.9291 - val_loss: 0.7553 - val_accuracy: 0.9465
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8319 - accuracy: 0.9379 - val_loss: 1.0832 - val_accuracy: 0.9465
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6781 - accuracy: 0.9371 - val_loss: 1.2119 - val_accuracy: 0.9465
    Epoch 19/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4657 - accuracy: 0.9530 - val_loss: 0.9798 - val_accuracy: 0.9465
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6221 - accuracy: 0.9415 - val_loss: 1.2786 - val_accuracy: 0.9251
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5368 - accuracy: 0.9477 - val_loss: 1.0932 - val_accuracy: 0.9251
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7796 - accuracy: 0.9362 - val_loss: 1.0294 - val_accuracy: 0.9519
    Epoch 23/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8343 - accuracy: 0.9344 - val_loss: 1.8844 - val_accuracy: 0.9144
    Epoch 24/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9298 - accuracy: 0.9317 - val_loss: 0.3812 - val_accuracy: 0.9572
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8235 - accuracy: 0.9379 - val_loss: 1.2450 - val_accuracy: 0.9251
    Epoch 26/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8713 - accuracy: 0.9415 - val_loss: 1.5199 - val_accuracy: 0.9305
    Epoch 27/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5464 - accuracy: 0.9468 - val_loss: 0.7568 - val_accuracy: 0.9465
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5804 - accuracy: 0.9468 - val_loss: 0.3330 - val_accuracy: 0.9572
    Epoch 29/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5991 - accuracy: 0.9521 - val_loss: 0.5425 - val_accuracy: 0.9465
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5477 - accuracy: 0.9521 - val_loss: 0.6538 - val_accuracy: 0.9465
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5287 - accuracy: 0.9548 - val_loss: 0.5177 - val_accuracy: 0.9572
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4679 - accuracy: 0.9548 - val_loss: 0.5782 - val_accuracy: 0.9572
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5615 - accuracy: 0.9495 - val_loss: 0.5149 - val_accuracy: 0.9519
    Epoch 34/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4801 - accuracy: 0.9592 - val_loss: 0.8463 - val_accuracy: 0.9251
    Epoch 35/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5892 - accuracy: 0.9548 - val_loss: 1.9106 - val_accuracy: 0.9198
    Epoch 36/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4534 - accuracy: 0.9574 - val_loss: 0.9004 - val_accuracy: 0.9465
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4239 - accuracy: 0.9663 - val_loss: 0.2238 - val_accuracy: 0.9679
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4939 - accuracy: 0.9592 - val_loss: 0.4139 - val_accuracy: 0.9572
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4362 - accuracy: 0.9690 - val_loss: 0.9381 - val_accuracy: 0.9412
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5525 - accuracy: 0.9504 - val_loss: 0.7010 - val_accuracy: 0.9412
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5247 - accuracy: 0.9681 - val_loss: 0.6055 - val_accuracy: 0.9626
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4778 - accuracy: 0.9583 - val_loss: 0.6405 - val_accuracy: 0.9679
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3343 - accuracy: 0.9707 - val_loss: 0.4469 - val_accuracy: 0.9679
    Epoch 44/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4007 - accuracy: 0.9672 - val_loss: 0.4936 - val_accuracy: 0.9733
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4265 - accuracy: 0.9681 - val_loss: 0.7730 - val_accuracy: 0.9572
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3002 - accuracy: 0.9690 - val_loss: 0.6762 - val_accuracy: 0.9519
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5280 - accuracy: 0.9654 - val_loss: 0.5598 - val_accuracy: 0.9626
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3133 - accuracy: 0.9681 - val_loss: 0.2162 - val_accuracy: 0.9733
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3774 - accuracy: 0.9681 - val_loss: 0.3408 - val_accuracy: 0.9572
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4617 - accuracy: 0.9672 - val_loss: 0.1256 - val_accuracy: 0.9840
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3772 - accuracy: 0.9725 - val_loss: 0.1637 - val_accuracy: 0.9626
    Epoch 52/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4456 - accuracy: 0.9716 - val_loss: 0.4566 - val_accuracy: 0.9626
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5380 - accuracy: 0.9574 - val_loss: 0.4371 - val_accuracy: 0.9626
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3790 - accuracy: 0.9716 - val_loss: 0.1644 - val_accuracy: 0.9840
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2308 - accuracy: 0.9770 - val_loss: 0.5312 - val_accuracy: 0.9679
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4559 - accuracy: 0.9645 - val_loss: 0.1750 - val_accuracy: 0.9893
    Epoch 57/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2575 - accuracy: 0.9778 - val_loss: 1.5559 - val_accuracy: 0.9358
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4908 - accuracy: 0.9663 - val_loss: 1.0303 - val_accuracy: 0.9465
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4622 - accuracy: 0.9716 - val_loss: 1.2558 - val_accuracy: 0.9412
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3918 - accuracy: 0.9707 - val_loss: 0.7617 - val_accuracy: 0.9572
    Epoch 61/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4135 - accuracy: 0.9716 - val_loss: 1.0116 - val_accuracy: 0.9572
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4684 - accuracy: 0.9672 - val_loss: 0.7656 - val_accuracy: 0.9519
    Epoch 63/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4424 - accuracy: 0.9663 - val_loss: 0.6654 - val_accuracy: 0.9626
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4891 - accuracy: 0.9672 - val_loss: 2.2889 - val_accuracy: 0.8930
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4391 - accuracy: 0.9699 - val_loss: 0.6489 - val_accuracy: 0.9626
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3460 - accuracy: 0.9787 - val_loss: 1.2388 - val_accuracy: 0.9465
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3618 - accuracy: 0.9707 - val_loss: 1.6883 - val_accuracy: 0.9412
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4051 - accuracy: 0.9725 - val_loss: 0.4515 - val_accuracy: 0.9733
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3291 - accuracy: 0.9787 - val_loss: 0.7384 - val_accuracy: 0.9679
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2744 - accuracy: 0.9778 - val_loss: 0.6633 - val_accuracy: 0.9626
    Epoch 71/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3011 - accuracy: 0.9778 - val_loss: 0.5680 - val_accuracy: 0.9679
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2190 - accuracy: 0.9823 - val_loss: 0.4646 - val_accuracy: 0.9679
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2197 - accuracy: 0.9805 - val_loss: 0.6633 - val_accuracy: 0.9679
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2521 - accuracy: 0.9787 - val_loss: 0.7251 - val_accuracy: 0.9626
    Epoch 75/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3248 - accuracy: 0.9770 - val_loss: 1.2972 - val_accuracy: 0.9412
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3157 - accuracy: 0.9752 - val_loss: 1.3294 - val_accuracy: 0.9733
    Epoch 77/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4020 - accuracy: 0.9761 - val_loss: 0.6260 - val_accuracy: 0.9679
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5102 - accuracy: 0.9672 - val_loss: 0.6977 - val_accuracy: 0.9733
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3811 - accuracy: 0.9770 - val_loss: 0.5373 - val_accuracy: 0.9733
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2580 - accuracy: 0.9805 - val_loss: 0.8291 - val_accuracy: 0.9626
    Epoch 81/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3454 - accuracy: 0.9699 - val_loss: 0.9026 - val_accuracy: 0.9572
    Epoch 82/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2497 - accuracy: 0.9823 - val_loss: 0.5801 - val_accuracy: 0.9572
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2785 - accuracy: 0.9787 - val_loss: 0.6239 - val_accuracy: 0.9626
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2046 - accuracy: 0.9832 - val_loss: 0.4699 - val_accuracy: 0.9733
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2623 - accuracy: 0.9814 - val_loss: 0.8377 - val_accuracy: 0.9626
    Epoch 86/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2662 - accuracy: 0.9867 - val_loss: 0.5231 - val_accuracy: 0.9679
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3390 - accuracy: 0.9778 - val_loss: 0.4095 - val_accuracy: 0.9626
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3238 - accuracy: 0.9787 - val_loss: 0.2443 - val_accuracy: 0.9733
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2607 - accuracy: 0.9787 - val_loss: 0.6131 - val_accuracy: 0.9786
    Epoch 90/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2379 - accuracy: 0.9849 - val_loss: 0.3787 - val_accuracy: 0.9786
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1371 - accuracy: 0.9876 - val_loss: 0.3445 - val_accuracy: 0.9733
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1399 - accuracy: 0.9823 - val_loss: 0.0176 - val_accuracy: 0.9893
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2956 - accuracy: 0.9778 - val_loss: 0.3671 - val_accuracy: 0.9733
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2316 - accuracy: 0.9814 - val_loss: 0.8934 - val_accuracy: 0.9519
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2500 - accuracy: 0.9840 - val_loss: 0.4098 - val_accuracy: 0.9733
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1884 - accuracy: 0.9858 - val_loss: 0.5896 - val_accuracy: 0.9786
    Epoch 97/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1177 - accuracy: 0.9867 - val_loss: 0.6287 - val_accuracy: 0.9679
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2072 - accuracy: 0.9832 - val_loss: 0.3855 - val_accuracy: 0.9679
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2186 - accuracy: 0.9858 - val_loss: 0.5005 - val_accuracy: 0.9733
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3391 - accuracy: 0.9840 - val_loss: 0.4889 - val_accuracy: 0.9786

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/143.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 16s 3s/step - loss: 0.7404 - accuracy: 0.9700

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7404471039772034, 0.9699646830558777]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3') or layer.name.startswith('block2'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,918,946
    Non-trainable params: 40,192
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/144.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
     2/18 [==&gt;...........................] - ETA: 5s - loss: 0.1751 - accuracy: 0.9844WARNING:tensorflow:Callbacks method `on_train_batch_end` is slow compared to the batch time (batch time: 0.2168s vs `on_train_batch_end` time: 0.4678s). Check your callbacks.
    18/18 [==============================] - 30s 2s/step - loss: 0.3856 - accuracy: 0.9752 - val_loss: 3.0024 - val_accuracy: 0.9091

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    18/18 [==============================] - 29s 2s/step - loss: 0.4199 - accuracy: 0.9823 - val_loss: 1.0483 - val_accuracy: 0.9519

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2177 - accuracy: 0.9867 - val_loss: 2.0351 - val_accuracy: 0.9412

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    18/18 [==============================] - 29s 2s/step - loss: 0.5826 - accuracy: 0.9699 - val_loss: 0.9965 - val_accuracy: 0.9679

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    18/18 [==============================] - 29s 2s/step - loss: 0.4093 - accuracy: 0.9778 - val_loss: 0.2730 - val_accuracy: 0.9733

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    18/18 [==============================] - 29s 2s/step - loss: 0.3507 - accuracy: 0.9823 - val_loss: 1.3523 - val_accuracy: 0.9626

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1813 - accuracy: 0.9876 - val_loss: 0.0256 - val_accuracy: 0.9947

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1324 - accuracy: 0.9876 - val_loss: 0.4374 - val_accuracy: 0.9840

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    18/18 [==============================] - 27s 2s/step - loss: 0.2391 - accuracy: 0.9778 - val_loss: 1.3205 - val_accuracy: 0.9679

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    18/18 [==============================] - 28s 2s/step - loss: 0.3445 - accuracy: 0.9840 - val_loss: 1.5640 - val_accuracy: 0.9465

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1492 - accuracy: 0.9840 - val_loss: 0.9323 - val_accuracy: 0.9626

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    18/18 [==============================] - 28s 2s/step - loss: 0.3005 - accuracy: 0.9823 - val_loss: 1.1749 - val_accuracy: 0.9358

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    18/18 [==============================] - 28s 2s/step - loss: 0.2400 - accuracy: 0.9885 - val_loss: 0.3496 - val_accuracy: 0.9893

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1477 - accuracy: 0.9876 - val_loss: 1.2345 - val_accuracy: 0.9572

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    18/18 [==============================] - 28s 2s/step - loss: 0.2230 - accuracy: 0.9814 - val_loss: 1.0237 - val_accuracy: 0.9626

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1065 - accuracy: 0.9894 - val_loss: 1.2128 - val_accuracy: 0.9626

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    18/18 [==============================] - 30s 2s/step - loss: 0.1423 - accuracy: 0.9894 - val_loss: 0.4050 - val_accuracy: 0.9786

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    18/18 [==============================] - 28s 2s/step - loss: 0.2254 - accuracy: 0.9823 - val_loss: 0.9293 - val_accuracy: 0.9840

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    18/18 [==============================] - 29s 2s/step - loss: 0.3697 - accuracy: 0.9770 - val_loss: 0.8840 - val_accuracy: 0.9733

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2506 - accuracy: 0.9876 - val_loss: 0.8528 - val_accuracy: 0.9572

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    18/18 [==============================] - 30s 2s/step - loss: 0.3277 - accuracy: 0.9849 - val_loss: 0.9140 - val_accuracy: 0.9733

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    18/18 [==============================] - 27s 1s/step - loss: 0.1296 - accuracy: 0.9902 - val_loss: 0.9210 - val_accuracy: 0.9786

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1430 - accuracy: 0.9894 - val_loss: 0.5594 - val_accuracy: 0.9840

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    18/18 [==============================] - 28s 2s/step - loss: 0.2227 - accuracy: 0.9876 - val_loss: 0.9362 - val_accuracy: 0.9626

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1468 - accuracy: 0.9920 - val_loss: 0.7962 - val_accuracy: 0.9679

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1607 - accuracy: 0.9885 - val_loss: 0.1770 - val_accuracy: 0.9893

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1061 - accuracy: 0.9929 - val_loss: 0.1873 - val_accuracy: 0.9947

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2941 - accuracy: 0.9823 - val_loss: 0.5261 - val_accuracy: 0.9786

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2749 - accuracy: 0.9840 - val_loss: 0.3815 - val_accuracy: 0.9893

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    18/18 [==============================] - 28s 2s/step - loss: 0.3673 - accuracy: 0.9787 - val_loss: 1.9359 - val_accuracy: 0.9626

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2043 - accuracy: 0.9823 - val_loss: 1.5264 - val_accuracy: 0.9572

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    18/18 [==============================] - 25s 1s/step - loss: 0.2422 - accuracy: 0.9823 - val_loss: 0.7641 - val_accuracy: 0.9733

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    18/18 [==============================] - 29s 2s/step - loss: 0.0815 - accuracy: 0.9929 - val_loss: 0.9385 - val_accuracy: 0.9626

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    18/18 [==============================] - 30s 2s/step - loss: 0.1173 - accuracy: 0.9911 - val_loss: 0.2903 - val_accuracy: 0.9840

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1499 - accuracy: 0.9902 - val_loss: 0.9749 - val_accuracy: 0.9733

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    18/18 [==============================] - 26s 1s/step - loss: 0.1715 - accuracy: 0.9902 - val_loss: 1.1238 - val_accuracy: 0.9572

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    18/18 [==============================] - 29s 2s/step - loss: 0.0526 - accuracy: 0.9938 - val_loss: 0.4610 - val_accuracy: 0.9786

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1601 - accuracy: 0.9929 - val_loss: 0.9687 - val_accuracy: 0.9733

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1256 - accuracy: 0.9911 - val_loss: 1.1588 - val_accuracy: 0.9626

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1829 - accuracy: 0.9867 - val_loss: 0.4583 - val_accuracy: 0.9893

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1071 - accuracy: 0.9911 - val_loss: 0.2395 - val_accuracy: 0.9840

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1046 - accuracy: 0.9885 - val_loss: 0.8719 - val_accuracy: 0.9679

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1671 - accuracy: 0.9885 - val_loss: 0.2402 - val_accuracy: 0.9786

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    18/18 [==============================] - 29s 2s/step - loss: 0.2953 - accuracy: 0.9858 - val_loss: 0.3119 - val_accuracy: 0.9893

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    18/18 [==============================] - 29s 2s/step - loss: 0.3384 - accuracy: 0.9796 - val_loss: 0.8910 - val_accuracy: 0.9465

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    18/18 [==============================] - 28s 2s/step - loss: 0.2001 - accuracy: 0.9894 - val_loss: 1.6462 - val_accuracy: 0.9519

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    18/18 [==============================] - 29s 2s/step - loss: 0.0735 - accuracy: 0.9911 - val_loss: 0.2535 - val_accuracy: 0.9786

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1217 - accuracy: 0.9920 - val_loss: 0.1821 - val_accuracy: 0.9893

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    18/18 [==============================] - 28s 2s/step - loss: 0.1467 - accuracy: 0.9876 - val_loss: 0.0055 - val_accuracy: 0.9947

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    18/18 [==============================] - 29s 2s/step - loss: 0.1211 - accuracy: 0.9894 - val_loss: 6.1904 - val_accuracy: 0.8770

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/145.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 5.1216 - accuracy: 0.8675

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [5.121611595153809, 0.8674911856651306]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8674911660777385
                  precision    recall  f1-score   support

               0       0.98      0.55      0.71       164
               1       0.85      1.00      0.91       402

        accuracy                           0.87       566
       macro avg       0.91      0.77      0.81       566
    weighted avg       0.88      0.87      0.85       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/146.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/147.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/148.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.55      0.71       164
               1       0.85      1.00      0.91       402

        accuracy                           0.87       566
       macro avg       0.91      0.77      0.81       566
    weighted avg       0.88      0.87      0.85       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.55      1.00      0.71      0.74      0.53       164
              1       0.85      1.00      0.55      0.91      0.74      0.58       402

    avg / total       0.88      0.87      0.68      0.85      0.74      0.56       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
