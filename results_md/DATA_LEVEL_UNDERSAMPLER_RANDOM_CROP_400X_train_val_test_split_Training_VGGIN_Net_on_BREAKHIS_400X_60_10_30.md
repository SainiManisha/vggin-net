<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 images belonging to 2 classes.
    Found 161 images belonging to 2 classes.
    Found 488 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.under_sampling import RandomUnderSampler
from tensorflow.keras.utils import to_categorical
ros = RandomUnderSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((584, 224, 340, 3), (584, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(1000).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/5 [=====&gt;........................] - ETA: 0s - loss: 1.0068 - accuracy: 0.5391WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    5/5 [==============================] - 7s 1s/step - loss: 2.3839 - accuracy: 0.6712 - val_loss: 10.4327 - val_accuracy: 0.8385
    Epoch 2/100
    5/5 [==============================] - 3s 576ms/step - loss: 3.8940 - accuracy: 0.7517 - val_loss: 4.7070 - val_accuracy: 0.8882
    Epoch 3/100
    5/5 [==============================] - 3s 646ms/step - loss: 2.2421 - accuracy: 0.8373 - val_loss: 11.1428 - val_accuracy: 0.6832
    Epoch 4/100
    5/5 [==============================] - 3s 638ms/step - loss: 2.0577 - accuracy: 0.8116 - val_loss: 2.5781 - val_accuracy: 0.8820
    Epoch 5/100
    5/5 [==============================] - 3s 601ms/step - loss: 1.5207 - accuracy: 0.8630 - val_loss: 2.1491 - val_accuracy: 0.9068
    Epoch 6/100
    5/5 [==============================] - 3s 552ms/step - loss: 1.1450 - accuracy: 0.8887 - val_loss: 5.5515 - val_accuracy: 0.7702
    Epoch 7/100
    5/5 [==============================] - 3s 584ms/step - loss: 1.1774 - accuracy: 0.8596 - val_loss: 4.9228 - val_accuracy: 0.7640
    Epoch 8/100
    5/5 [==============================] - 3s 570ms/step - loss: 1.2386 - accuracy: 0.8716 - val_loss: 2.3276 - val_accuracy: 0.8385
    Epoch 9/100
    5/5 [==============================] - 3s 556ms/step - loss: 1.1362 - accuracy: 0.8870 - val_loss: 5.4429 - val_accuracy: 0.7329
    Epoch 10/100
    5/5 [==============================] - 3s 617ms/step - loss: 1.1663 - accuracy: 0.8990 - val_loss: 4.9831 - val_accuracy: 0.7516
    Epoch 11/100
    5/5 [==============================] - 3s 570ms/step - loss: 1.2799 - accuracy: 0.8733 - val_loss: 3.1361 - val_accuracy: 0.8012
    Epoch 12/100
    5/5 [==============================] - 3s 568ms/step - loss: 1.3972 - accuracy: 0.8682 - val_loss: 13.2961 - val_accuracy: 0.5093
    Epoch 13/100
    5/5 [==============================] - 3s 626ms/step - loss: 1.6365 - accuracy: 0.8545 - val_loss: 2.3031 - val_accuracy: 0.8509
    Epoch 14/100
    5/5 [==============================] - 3s 572ms/step - loss: 0.9649 - accuracy: 0.9092 - val_loss: 2.0532 - val_accuracy: 0.8634
    Epoch 15/100
    5/5 [==============================] - 3s 555ms/step - loss: 1.1185 - accuracy: 0.8836 - val_loss: 1.8268 - val_accuracy: 0.9130
    Epoch 16/100
    5/5 [==============================] - 3s 575ms/step - loss: 1.1290 - accuracy: 0.8887 - val_loss: 1.4937 - val_accuracy: 0.8944
    Epoch 17/100
    5/5 [==============================] - 3s 669ms/step - loss: 1.2459 - accuracy: 0.8801 - val_loss: 4.5784 - val_accuracy: 0.7826
    Epoch 18/100
    5/5 [==============================] - 3s 628ms/step - loss: 1.1043 - accuracy: 0.9127 - val_loss: 1.6264 - val_accuracy: 0.8820
    Epoch 19/100
    5/5 [==============================] - 3s 629ms/step - loss: 0.9417 - accuracy: 0.9041 - val_loss: 1.9070 - val_accuracy: 0.9006
    Epoch 20/100
    5/5 [==============================] - 3s 632ms/step - loss: 1.0092 - accuracy: 0.9007 - val_loss: 2.2329 - val_accuracy: 0.8634
    Epoch 21/100
    5/5 [==============================] - 3s 552ms/step - loss: 0.9402 - accuracy: 0.9075 - val_loss: 1.9764 - val_accuracy: 0.8758
    Epoch 22/100
    5/5 [==============================] - 3s 534ms/step - loss: 0.9696 - accuracy: 0.9110 - val_loss: 1.5917 - val_accuracy: 0.9068
    Epoch 23/100
    5/5 [==============================] - 3s 557ms/step - loss: 0.9163 - accuracy: 0.9161 - val_loss: 1.8676 - val_accuracy: 0.9068
    Epoch 24/100
    5/5 [==============================] - 3s 553ms/step - loss: 0.7639 - accuracy: 0.9315 - val_loss: 1.6823 - val_accuracy: 0.8696
    Epoch 25/100
    5/5 [==============================] - 3s 581ms/step - loss: 1.0991 - accuracy: 0.9144 - val_loss: 1.8044 - val_accuracy: 0.8571
    Epoch 26/100
    5/5 [==============================] - 3s 568ms/step - loss: 0.7493 - accuracy: 0.9178 - val_loss: 4.7961 - val_accuracy: 0.7453
    Epoch 27/100
    5/5 [==============================] - 3s 625ms/step - loss: 1.2473 - accuracy: 0.9161 - val_loss: 2.4637 - val_accuracy: 0.8571
    Epoch 28/100
    5/5 [==============================] - 3s 557ms/step - loss: 0.5146 - accuracy: 0.9452 - val_loss: 3.0115 - val_accuracy: 0.8199
    Epoch 29/100
    5/5 [==============================] - 3s 612ms/step - loss: 1.2599 - accuracy: 0.8990 - val_loss: 2.5375 - val_accuracy: 0.8758
    Epoch 30/100
    5/5 [==============================] - 3s 600ms/step - loss: 0.8607 - accuracy: 0.9298 - val_loss: 1.8186 - val_accuracy: 0.9193
    Epoch 31/100
    5/5 [==============================] - 3s 569ms/step - loss: 1.3249 - accuracy: 0.8955 - val_loss: 2.0444 - val_accuracy: 0.8944
    Epoch 32/100
    5/5 [==============================] - 3s 629ms/step - loss: 0.9407 - accuracy: 0.9195 - val_loss: 3.4948 - val_accuracy: 0.8385
    Epoch 33/100
    5/5 [==============================] - 3s 591ms/step - loss: 0.8378 - accuracy: 0.9349 - val_loss: 3.2670 - val_accuracy: 0.8261
    Epoch 34/100
    5/5 [==============================] - 3s 576ms/step - loss: 0.8232 - accuracy: 0.9384 - val_loss: 2.2460 - val_accuracy: 0.8820
    Epoch 35/100
    5/5 [==============================] - 3s 663ms/step - loss: 0.6456 - accuracy: 0.9401 - val_loss: 1.7124 - val_accuracy: 0.9006
    Epoch 36/100
    5/5 [==============================] - 3s 648ms/step - loss: 0.8192 - accuracy: 0.9332 - val_loss: 1.6497 - val_accuracy: 0.9130
    Epoch 37/100
    5/5 [==============================] - 3s 672ms/step - loss: 0.5180 - accuracy: 0.9452 - val_loss: 2.3735 - val_accuracy: 0.9068
    Epoch 38/100
    5/5 [==============================] - 3s 642ms/step - loss: 0.7249 - accuracy: 0.9452 - val_loss: 1.6733 - val_accuracy: 0.8944
    Epoch 39/100
    5/5 [==============================] - 3s 654ms/step - loss: 1.0772 - accuracy: 0.9332 - val_loss: 2.1122 - val_accuracy: 0.8882
    Epoch 40/100
    5/5 [==============================] - 3s 581ms/step - loss: 0.6974 - accuracy: 0.9384 - val_loss: 2.4825 - val_accuracy: 0.8944
    Epoch 41/100
    5/5 [==============================] - 3s 565ms/step - loss: 0.5671 - accuracy: 0.9452 - val_loss: 3.5258 - val_accuracy: 0.8634
    Epoch 42/100
    5/5 [==============================] - 3s 628ms/step - loss: 0.5853 - accuracy: 0.9418 - val_loss: 4.2554 - val_accuracy: 0.8075
    Epoch 43/100
    5/5 [==============================] - 3s 600ms/step - loss: 0.7600 - accuracy: 0.9366 - val_loss: 2.7035 - val_accuracy: 0.9068
    Epoch 44/100
    5/5 [==============================] - 3s 628ms/step - loss: 0.8362 - accuracy: 0.9486 - val_loss: 2.3445 - val_accuracy: 0.9130
    Epoch 45/100
    5/5 [==============================] - 3s 623ms/step - loss: 0.4987 - accuracy: 0.9538 - val_loss: 2.3009 - val_accuracy: 0.9193
    Epoch 46/100
    5/5 [==============================] - 3s 573ms/step - loss: 0.7158 - accuracy: 0.9503 - val_loss: 2.6238 - val_accuracy: 0.9068
    Epoch 47/100
    5/5 [==============================] - 3s 641ms/step - loss: 0.6087 - accuracy: 0.9418 - val_loss: 3.0436 - val_accuracy: 0.8820
    Epoch 48/100
    5/5 [==============================] - 3s 549ms/step - loss: 0.5655 - accuracy: 0.9521 - val_loss: 3.1562 - val_accuracy: 0.8571
    Epoch 49/100
    5/5 [==============================] - 3s 551ms/step - loss: 0.7273 - accuracy: 0.9384 - val_loss: 2.9823 - val_accuracy: 0.8820
    Epoch 50/100
    5/5 [==============================] - 3s 553ms/step - loss: 0.5573 - accuracy: 0.9538 - val_loss: 3.0270 - val_accuracy: 0.8820
    Epoch 51/100
    5/5 [==============================] - 3s 551ms/step - loss: 0.7265 - accuracy: 0.9315 - val_loss: 3.0744 - val_accuracy: 0.8820
    Epoch 52/100
    5/5 [==============================] - 7s 1s/step - loss: 0.5210 - accuracy: 0.9589 - val_loss: 3.1255 - val_accuracy: 0.8696
    Epoch 53/100
    5/5 [==============================] - 3s 593ms/step - loss: 0.7627 - accuracy: 0.9521 - val_loss: 2.7465 - val_accuracy: 0.8696
    Epoch 54/100
    5/5 [==============================] - 3s 606ms/step - loss: 0.5619 - accuracy: 0.9589 - val_loss: 2.9212 - val_accuracy: 0.8758
    Epoch 55/100
    5/5 [==============================] - 3s 536ms/step - loss: 0.4653 - accuracy: 0.9486 - val_loss: 2.0956 - val_accuracy: 0.9068
    Epoch 56/100
    5/5 [==============================] - 3s 623ms/step - loss: 0.6384 - accuracy: 0.9572 - val_loss: 1.1738 - val_accuracy: 0.9503
    Epoch 57/100
    5/5 [==============================] - 3s 641ms/step - loss: 0.3032 - accuracy: 0.9726 - val_loss: 1.9057 - val_accuracy: 0.8882
    Epoch 58/100
    5/5 [==============================] - 3s 579ms/step - loss: 0.5124 - accuracy: 0.9555 - val_loss: 1.7892 - val_accuracy: 0.8820
    Epoch 59/100
    5/5 [==============================] - 3s 572ms/step - loss: 0.2262 - accuracy: 0.9760 - val_loss: 2.6830 - val_accuracy: 0.8634
    Epoch 60/100
    5/5 [==============================] - 3s 566ms/step - loss: 0.4848 - accuracy: 0.9555 - val_loss: 2.0438 - val_accuracy: 0.9006
    Epoch 61/100
    5/5 [==============================] - 3s 675ms/step - loss: 0.3732 - accuracy: 0.9743 - val_loss: 2.7246 - val_accuracy: 0.8820
    Epoch 62/100
    5/5 [==============================] - 3s 565ms/step - loss: 0.5086 - accuracy: 0.9538 - val_loss: 2.4511 - val_accuracy: 0.8882
    Epoch 63/100
    5/5 [==============================] - 3s 629ms/step - loss: 0.5288 - accuracy: 0.9623 - val_loss: 2.1390 - val_accuracy: 0.8882
    Epoch 64/100
    5/5 [==============================] - 3s 692ms/step - loss: 0.6506 - accuracy: 0.9658 - val_loss: 2.3062 - val_accuracy: 0.8571
    Epoch 65/100
    5/5 [==============================] - 3s 562ms/step - loss: 0.5582 - accuracy: 0.9503 - val_loss: 2.1700 - val_accuracy: 0.8758
    Epoch 66/100
    5/5 [==============================] - 3s 658ms/step - loss: 0.2388 - accuracy: 0.9726 - val_loss: 2.3572 - val_accuracy: 0.8696
    Epoch 67/100
    5/5 [==============================] - 3s 631ms/step - loss: 0.2423 - accuracy: 0.9726 - val_loss: 3.4219 - val_accuracy: 0.8509
    Epoch 68/100
    5/5 [==============================] - 3s 547ms/step - loss: 0.3156 - accuracy: 0.9640 - val_loss: 3.0146 - val_accuracy: 0.8758
    Epoch 69/100
    5/5 [==============================] - 3s 567ms/step - loss: 0.4512 - accuracy: 0.9606 - val_loss: 2.1741 - val_accuracy: 0.8882
    Epoch 70/100
    5/5 [==============================] - 3s 560ms/step - loss: 0.5416 - accuracy: 0.9623 - val_loss: 2.1356 - val_accuracy: 0.8944
    Epoch 71/100
    5/5 [==============================] - 3s 659ms/step - loss: 0.3337 - accuracy: 0.9795 - val_loss: 2.2594 - val_accuracy: 0.9193
    Epoch 72/100
    5/5 [==============================] - 3s 647ms/step - loss: 0.4252 - accuracy: 0.9726 - val_loss: 2.0289 - val_accuracy: 0.9193
    Epoch 73/100
    5/5 [==============================] - 3s 567ms/step - loss: 0.3068 - accuracy: 0.9640 - val_loss: 1.5165 - val_accuracy: 0.9503
    Epoch 74/100
    5/5 [==============================] - 3s 564ms/step - loss: 0.2755 - accuracy: 0.9777 - val_loss: 1.8295 - val_accuracy: 0.9441
    Epoch 75/100
    5/5 [==============================] - 3s 633ms/step - loss: 0.3411 - accuracy: 0.9760 - val_loss: 1.8955 - val_accuracy: 0.9379
    Epoch 76/100
    5/5 [==============================] - 3s 573ms/step - loss: 0.6395 - accuracy: 0.9606 - val_loss: 2.9243 - val_accuracy: 0.9130
    Epoch 77/100
    5/5 [==============================] - 3s 635ms/step - loss: 0.6019 - accuracy: 0.9589 - val_loss: 2.8008 - val_accuracy: 0.9130
    Epoch 78/100
    5/5 [==============================] - 3s 577ms/step - loss: 0.4253 - accuracy: 0.9692 - val_loss: 3.2637 - val_accuracy: 0.9068
    Epoch 79/100
    5/5 [==============================] - 3s 535ms/step - loss: 0.3107 - accuracy: 0.9726 - val_loss: 3.2293 - val_accuracy: 0.9068
    Epoch 80/100
    5/5 [==============================] - 3s 554ms/step - loss: 0.3657 - accuracy: 0.9692 - val_loss: 2.0107 - val_accuracy: 0.9193
    Epoch 81/100
    5/5 [==============================] - 3s 652ms/step - loss: 0.2269 - accuracy: 0.9829 - val_loss: 1.7804 - val_accuracy: 0.9255
    Epoch 82/100
    5/5 [==============================] - 3s 621ms/step - loss: 0.2312 - accuracy: 0.9846 - val_loss: 1.4969 - val_accuracy: 0.9379
    Epoch 83/100
    5/5 [==============================] - 3s 558ms/step - loss: 0.2717 - accuracy: 0.9726 - val_loss: 1.7817 - val_accuracy: 0.9317
    Epoch 84/100
    5/5 [==============================] - 3s 613ms/step - loss: 0.1843 - accuracy: 0.9777 - val_loss: 2.1334 - val_accuracy: 0.9193
    Epoch 85/100
    5/5 [==============================] - 3s 677ms/step - loss: 0.2960 - accuracy: 0.9726 - val_loss: 2.7548 - val_accuracy: 0.9068
    Epoch 86/100
    5/5 [==============================] - 3s 644ms/step - loss: 0.4457 - accuracy: 0.9658 - val_loss: 2.4095 - val_accuracy: 0.9068
    Epoch 87/100
    5/5 [==============================] - 3s 634ms/step - loss: 0.2248 - accuracy: 0.9675 - val_loss: 2.6432 - val_accuracy: 0.9006
    Epoch 88/100
    5/5 [==============================] - 7s 1s/step - loss: 0.1631 - accuracy: 0.9846 - val_loss: 2.8577 - val_accuracy: 0.8944
    Epoch 89/100
    5/5 [==============================] - 3s 583ms/step - loss: 0.5344 - accuracy: 0.9640 - val_loss: 2.5788 - val_accuracy: 0.9006
    Epoch 90/100
    5/5 [==============================] - 3s 603ms/step - loss: 0.3455 - accuracy: 0.9640 - val_loss: 2.4368 - val_accuracy: 0.8944
    Epoch 91/100
    5/5 [==============================] - 3s 567ms/step - loss: 0.3132 - accuracy: 0.9795 - val_loss: 2.5267 - val_accuracy: 0.9130
    Epoch 92/100
    5/5 [==============================] - 3s 644ms/step - loss: 0.2004 - accuracy: 0.9829 - val_loss: 2.5524 - val_accuracy: 0.9130
    Epoch 93/100
    5/5 [==============================] - 3s 554ms/step - loss: 0.5870 - accuracy: 0.9692 - val_loss: 2.1482 - val_accuracy: 0.9130
    Epoch 94/100
    5/5 [==============================] - 3s 591ms/step - loss: 0.3411 - accuracy: 0.9743 - val_loss: 2.5245 - val_accuracy: 0.9068
    Epoch 95/100
    5/5 [==============================] - 3s 617ms/step - loss: 0.2358 - accuracy: 0.9743 - val_loss: 2.6822 - val_accuracy: 0.9193
    Epoch 96/100
    5/5 [==============================] - 3s 564ms/step - loss: 0.4569 - accuracy: 0.9640 - val_loss: 2.4376 - val_accuracy: 0.9193
    Epoch 97/100
    5/5 [==============================] - 3s 632ms/step - loss: 0.3845 - accuracy: 0.9760 - val_loss: 3.0905 - val_accuracy: 0.9255
    Epoch 98/100
    5/5 [==============================] - 3s 569ms/step - loss: 0.3764 - accuracy: 0.9692 - val_loss: 2.7841 - val_accuracy: 0.9255
    Epoch 99/100
    5/5 [==============================] - 3s 589ms/step - loss: 0.2909 - accuracy: 0.9743 - val_loss: 3.0140 - val_accuracy: 0.9193
    Epoch 100/100
    5/5 [==============================] - 3s 628ms/step - loss: 0.4034 - accuracy: 0.9658 - val_loss: 3.7083 - val_accuracy: 0.8758

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/139.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 4s 1s/step - loss: 2.2521 - accuracy: 0.9262

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[28\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.25211501121521, 0.9262295365333557]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9262295081967213
                  precision    recall  f1-score   support

               0       0.87      0.89      0.88       148
               1       0.95      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.91      0.91      0.91       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/140.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/141.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[32\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/142.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.87      0.89      0.88       148
               1       0.95      0.94      0.95       340

        accuracy                           0.93       488
       macro avg       0.91      0.91      0.91       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.87      0.89      0.94      0.88      0.91      0.83       148
              1       0.95      0.94      0.89      0.95      0.91      0.84       340

    avg / total       0.93      0.93      0.90      0.93      0.91      0.84       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
