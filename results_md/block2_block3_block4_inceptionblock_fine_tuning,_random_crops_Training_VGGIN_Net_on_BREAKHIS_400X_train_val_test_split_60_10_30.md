<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK2andBLOCK3and4-FINETUNING_400X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 0.9810 - accuracy: 0.5781WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 15s 2s/step - loss: 3.5228 - accuracy: 0.7497 - val_loss: 15.6439 - val_accuracy: 0.7081
    Epoch 2/100
    8/8 [==============================] - 8s 994ms/step - loss: 1.9120 - accuracy: 0.8270 - val_loss: 2.8492 - val_accuracy: 0.9068
    Epoch 3/100
    8/8 [==============================] - 8s 994ms/step - loss: 1.5947 - accuracy: 0.8496 - val_loss: 1.8636 - val_accuracy: 0.9068
    Epoch 4/100
    8/8 [==============================] - 8s 948ms/step - loss: 1.4541 - accuracy: 0.8754 - val_loss: 1.7258 - val_accuracy: 0.9006
    Epoch 5/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2306 - accuracy: 0.8785 - val_loss: 2.9245 - val_accuracy: 0.8758
    Epoch 6/100
    8/8 [==============================] - 8s 965ms/step - loss: 1.3042 - accuracy: 0.8888 - val_loss: 5.8461 - val_accuracy: 0.7516
    Epoch 7/100
    8/8 [==============================] - 12s 1s/step - loss: 1.1258 - accuracy: 0.8888 - val_loss: 1.6855 - val_accuracy: 0.9006
    Epoch 8/100
    8/8 [==============================] - 8s 983ms/step - loss: 1.2201 - accuracy: 0.8816 - val_loss: 1.4841 - val_accuracy: 0.9193
    Epoch 9/100
    8/8 [==============================] - 12s 1s/step - loss: 1.2970 - accuracy: 0.8877 - val_loss: 1.0834 - val_accuracy: 0.9441
    Epoch 10/100
    8/8 [==============================] - 8s 947ms/step - loss: 1.3657 - accuracy: 0.8888 - val_loss: 1.6322 - val_accuracy: 0.8509
    Epoch 11/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2428 - accuracy: 0.8774 - val_loss: 1.7784 - val_accuracy: 0.8571
    Epoch 12/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2122 - accuracy: 0.8919 - val_loss: 1.6041 - val_accuracy: 0.9068
    Epoch 13/100
    8/8 [==============================] - 12s 1s/step - loss: 1.0702 - accuracy: 0.9042 - val_loss: 2.5176 - val_accuracy: 0.8323
    Epoch 14/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8837 - accuracy: 0.9186 - val_loss: 1.4023 - val_accuracy: 0.9379
    Epoch 15/100
    8/8 [==============================] - 8s 982ms/step - loss: 1.0360 - accuracy: 0.9083 - val_loss: 1.3643 - val_accuracy: 0.9193
    Epoch 16/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9595 - accuracy: 0.9135 - val_loss: 1.4617 - val_accuracy: 0.8944
    Epoch 17/100
    8/8 [==============================] - 8s 965ms/step - loss: 1.0297 - accuracy: 0.9114 - val_loss: 1.3082 - val_accuracy: 0.9193
    Epoch 18/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8992 - accuracy: 0.9166 - val_loss: 1.2728 - val_accuracy: 0.9317
    Epoch 19/100
    8/8 [==============================] - 11s 1s/step - loss: 1.1600 - accuracy: 0.9135 - val_loss: 1.1924 - val_accuracy: 0.9255
    Epoch 20/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8874 - accuracy: 0.9145 - val_loss: 1.1933 - val_accuracy: 0.9068
    Epoch 21/100
    8/8 [==============================] - 12s 1s/step - loss: 1.0580 - accuracy: 0.9104 - val_loss: 0.7795 - val_accuracy: 0.9130
    Epoch 22/100
    8/8 [==============================] - 8s 997ms/step - loss: 1.0369 - accuracy: 0.9125 - val_loss: 1.1311 - val_accuracy: 0.9193
    Epoch 23/100
    8/8 [==============================] - 8s 979ms/step - loss: 1.1269 - accuracy: 0.9156 - val_loss: 0.8584 - val_accuracy: 0.9255
    Epoch 24/100
    8/8 [==============================] - 11s 1s/step - loss: 1.2100 - accuracy: 0.9176 - val_loss: 1.2187 - val_accuracy: 0.8944
    Epoch 25/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6751 - accuracy: 0.9341 - val_loss: 1.3909 - val_accuracy: 0.8944
    Epoch 26/100
    8/8 [==============================] - 8s 998ms/step - loss: 0.6643 - accuracy: 0.9413 - val_loss: 1.1959 - val_accuracy: 0.9317
    Epoch 27/100
    8/8 [==============================] - 8s 971ms/step - loss: 0.8553 - accuracy: 0.9372 - val_loss: 1.4719 - val_accuracy: 0.9130
    Epoch 28/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8687 - accuracy: 0.9361 - val_loss: 1.8110 - val_accuracy: 0.9193
    Epoch 29/100
    8/8 [==============================] - 8s 992ms/step - loss: 1.0569 - accuracy: 0.9300 - val_loss: 2.8079 - val_accuracy: 0.8820
    Epoch 30/100
    8/8 [==============================] - 8s 991ms/step - loss: 1.0197 - accuracy: 0.9228 - val_loss: 1.5012 - val_accuracy: 0.8944
    Epoch 31/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4667 - accuracy: 0.9475 - val_loss: 1.5476 - val_accuracy: 0.9193
    Epoch 32/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6651 - accuracy: 0.9382 - val_loss: 1.3064 - val_accuracy: 0.9255
    Epoch 33/100
    8/8 [==============================] - 12s 1s/step - loss: 0.6963 - accuracy: 0.9413 - val_loss: 1.7049 - val_accuracy: 0.9068
    Epoch 34/100
    8/8 [==============================] - 12s 1s/step - loss: 0.7186 - accuracy: 0.9454 - val_loss: 2.3134 - val_accuracy: 0.8944
    Epoch 35/100
    8/8 [==============================] - 8s 1s/step - loss: 0.7693 - accuracy: 0.9485 - val_loss: 1.5020 - val_accuracy: 0.9255
    Epoch 36/100
    8/8 [==============================] - 8s 980ms/step - loss: 0.7412 - accuracy: 0.9506 - val_loss: 1.5008 - val_accuracy: 0.9317
    Epoch 37/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8148 - accuracy: 0.9279 - val_loss: 1.3857 - val_accuracy: 0.9193
    Epoch 38/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6761 - accuracy: 0.9372 - val_loss: 1.8347 - val_accuracy: 0.9130
    Epoch 39/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5412 - accuracy: 0.9557 - val_loss: 3.0649 - val_accuracy: 0.8882
    Epoch 40/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5922 - accuracy: 0.9609 - val_loss: 3.1521 - val_accuracy: 0.8820
    Epoch 41/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5949 - accuracy: 0.9516 - val_loss: 2.3451 - val_accuracy: 0.8944
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5375 - accuracy: 0.9567 - val_loss: 1.7921 - val_accuracy: 0.9317
    Epoch 43/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4995 - accuracy: 0.9567 - val_loss: 1.9870 - val_accuracy: 0.9193
    Epoch 44/100
    8/8 [==============================] - 8s 968ms/step - loss: 0.5446 - accuracy: 0.9506 - val_loss: 1.7527 - val_accuracy: 0.9006
    Epoch 45/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5424 - accuracy: 0.9567 - val_loss: 1.6969 - val_accuracy: 0.9193
    Epoch 46/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5533 - accuracy: 0.9598 - val_loss: 2.6129 - val_accuracy: 0.9006
    Epoch 47/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4778 - accuracy: 0.9629 - val_loss: 1.7161 - val_accuracy: 0.9441
    Epoch 48/100
    8/8 [==============================] - 15s 2s/step - loss: 0.3412 - accuracy: 0.9681 - val_loss: 1.8631 - val_accuracy: 0.9379
    Epoch 49/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4601 - accuracy: 0.9557 - val_loss: 2.2450 - val_accuracy: 0.9006
    Epoch 50/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5762 - accuracy: 0.9567 - val_loss: 1.6628 - val_accuracy: 0.9193
    Epoch 51/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4933 - accuracy: 0.9609 - val_loss: 1.5594 - val_accuracy: 0.9317
    Epoch 52/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5652 - accuracy: 0.9475 - val_loss: 1.9196 - val_accuracy: 0.9130
    Epoch 53/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5721 - accuracy: 0.9547 - val_loss: 2.5493 - val_accuracy: 0.8944
    Epoch 54/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5756 - accuracy: 0.9619 - val_loss: 3.7229 - val_accuracy: 0.8882
    Epoch 55/100
    8/8 [==============================] - 15s 2s/step - loss: 0.5908 - accuracy: 0.9516 - val_loss: 1.8374 - val_accuracy: 0.9255
    Epoch 56/100
    8/8 [==============================] - 12s 2s/step - loss: 0.6005 - accuracy: 0.9516 - val_loss: 2.5222 - val_accuracy: 0.8944
    Epoch 57/100
    8/8 [==============================] - 12s 1s/step - loss: 0.8063 - accuracy: 0.9485 - val_loss: 1.9268 - val_accuracy: 0.9379
    Epoch 58/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4154 - accuracy: 0.9660 - val_loss: 1.8214 - val_accuracy: 0.9255
    Epoch 59/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3259 - accuracy: 0.9650 - val_loss: 0.9674 - val_accuracy: 0.9627
    Epoch 60/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4414 - accuracy: 0.9712 - val_loss: 0.9538 - val_accuracy: 0.9627
    Epoch 61/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3516 - accuracy: 0.9701 - val_loss: 1.1635 - val_accuracy: 0.9441
    Epoch 62/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4193 - accuracy: 0.9650 - val_loss: 1.4275 - val_accuracy: 0.9317
    Epoch 63/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3568 - accuracy: 0.9701 - val_loss: 1.3308 - val_accuracy: 0.9379
    Epoch 64/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4621 - accuracy: 0.9732 - val_loss: 1.5192 - val_accuracy: 0.9565
    Epoch 65/100
    8/8 [==============================] - 15s 2s/step - loss: 0.6297 - accuracy: 0.9629 - val_loss: 1.5146 - val_accuracy: 0.9441
    Epoch 66/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4733 - accuracy: 0.9660 - val_loss: 1.6211 - val_accuracy: 0.9379
    Epoch 67/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3365 - accuracy: 0.9722 - val_loss: 2.3042 - val_accuracy: 0.9193
    Epoch 68/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4167 - accuracy: 0.9650 - val_loss: 2.2105 - val_accuracy: 0.9317
    Epoch 69/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3878 - accuracy: 0.9722 - val_loss: 2.1230 - val_accuracy: 0.9565
    Epoch 70/100
    8/8 [==============================] - 8s 983ms/step - loss: 0.5612 - accuracy: 0.9588 - val_loss: 1.8077 - val_accuracy: 0.9565
    Epoch 71/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4158 - accuracy: 0.9732 - val_loss: 2.0261 - val_accuracy: 0.9317
    Epoch 72/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2904 - accuracy: 0.9732 - val_loss: 1.5927 - val_accuracy: 0.9255
    Epoch 73/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2626 - accuracy: 0.9743 - val_loss: 1.8180 - val_accuracy: 0.9255
    Epoch 74/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4048 - accuracy: 0.9763 - val_loss: 1.5096 - val_accuracy: 0.9193
    Epoch 75/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3295 - accuracy: 0.9670 - val_loss: 1.8031 - val_accuracy: 0.9441
    Epoch 76/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3416 - accuracy: 0.9773 - val_loss: 1.2488 - val_accuracy: 0.9503
    Epoch 77/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3626 - accuracy: 0.9753 - val_loss: 2.0511 - val_accuracy: 0.9130
    Epoch 78/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3413 - accuracy: 0.9753 - val_loss: 1.8192 - val_accuracy: 0.9193
    Epoch 79/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3804 - accuracy: 0.9743 - val_loss: 2.5034 - val_accuracy: 0.9255
    Epoch 80/100
    8/8 [==============================] - 12s 1s/step - loss: 0.5117 - accuracy: 0.9701 - val_loss: 2.8125 - val_accuracy: 0.9130
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.5767 - accuracy: 0.9598 - val_loss: 2.5126 - val_accuracy: 0.9193
    Epoch 82/100
    8/8 [==============================] - 12s 2s/step - loss: 0.3066 - accuracy: 0.9722 - val_loss: 1.5950 - val_accuracy: 0.9379
    Epoch 83/100
    8/8 [==============================] - 8s 986ms/step - loss: 0.3081 - accuracy: 0.9784 - val_loss: 3.3208 - val_accuracy: 0.8944
    Epoch 84/100
    8/8 [==============================] - 12s 1s/step - loss: 0.3208 - accuracy: 0.9794 - val_loss: 3.0625 - val_accuracy: 0.9193
    Epoch 85/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4751 - accuracy: 0.9743 - val_loss: 2.5096 - val_accuracy: 0.9317
    Epoch 86/100
    8/8 [==============================] - 12s 1s/step - loss: 0.1870 - accuracy: 0.9784 - val_loss: 2.8784 - val_accuracy: 0.9317
    Epoch 87/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2537 - accuracy: 0.9784 - val_loss: 3.0431 - val_accuracy: 0.9379
    Epoch 88/100
    8/8 [==============================] - 8s 972ms/step - loss: 0.3658 - accuracy: 0.9712 - val_loss: 2.3624 - val_accuracy: 0.9441
    Epoch 89/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2105 - accuracy: 0.9866 - val_loss: 2.1299 - val_accuracy: 0.9441
    Epoch 90/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4124 - accuracy: 0.9835 - val_loss: 2.1392 - val_accuracy: 0.9441
    Epoch 91/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2260 - accuracy: 0.9846 - val_loss: 2.1423 - val_accuracy: 0.9503
    Epoch 92/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2551 - accuracy: 0.9794 - val_loss: 2.1373 - val_accuracy: 0.9503
    Epoch 93/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2124 - accuracy: 0.9856 - val_loss: 2.1959 - val_accuracy: 0.9503
    Epoch 94/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2460 - accuracy: 0.9804 - val_loss: 1.9953 - val_accuracy: 0.9503
    Epoch 95/100
    8/8 [==============================] - 8s 966ms/step - loss: 0.3357 - accuracy: 0.9753 - val_loss: 3.2429 - val_accuracy: 0.9068
    Epoch 96/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4813 - accuracy: 0.9681 - val_loss: 2.1001 - val_accuracy: 0.9255
    Epoch 97/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3492 - accuracy: 0.9804 - val_loss: 2.1011 - val_accuracy: 0.9255
    Epoch 98/100
    8/8 [==============================] - 12s 1s/step - loss: 0.2795 - accuracy: 0.9856 - val_loss: 2.6929 - val_accuracy: 0.9130
    Epoch 99/100
    8/8 [==============================] - 12s 2s/step - loss: 0.2528 - accuracy: 0.9794 - val_loss: 1.8686 - val_accuracy: 0.9565
    Epoch 100/100
    8/8 [==============================] - 8s 987ms/step - loss: 0.1752 - accuracy: 0.9846 - val_loss: 2.1926 - val_accuracy: 0.9503

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/175.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 14s 3s/step - loss: 3.0555 - accuracy: 0.9262

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [3.055514335632324, 0.9262295365333557]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3') or layer.name.startswith('block2'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,918,946
    Non-trainable params: 40,192
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/176.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
     2/16 [==&gt;...........................] - ETA: 5s - loss: 1.7685e-04 - accuracy: 1.0000WARNING:tensorflow:Callbacks method `on_train_batch_end` is slow compared to the batch time (batch time: 0.2254s vs `on_train_batch_end` time: 0.4947s). Check your callbacks.
    16/16 [==============================] - 23s 1s/step - loss: 0.3505 - accuracy: 0.9763 - val_loss: 4.8733 - val_accuracy: 0.9006

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    16/16 [==============================] - 24s 1s/step - loss: 0.4470 - accuracy: 0.9753 - val_loss: 2.2849 - val_accuracy: 0.9565

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    16/16 [==============================] - 23s 1s/step - loss: 0.4176 - accuracy: 0.9629 - val_loss: 1.6780 - val_accuracy: 0.9255

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2468 - accuracy: 0.9794 - val_loss: 1.8387 - val_accuracy: 0.9379

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    16/16 [==============================] - 23s 1s/step - loss: 0.5210 - accuracy: 0.9660 - val_loss: 1.6235 - val_accuracy: 0.9689

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3335 - accuracy: 0.9784 - val_loss: 3.4159 - val_accuracy: 0.9193

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    16/16 [==============================] - 23s 1s/step - loss: 0.5006 - accuracy: 0.9640 - val_loss: 1.5867 - val_accuracy: 0.9317

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2545 - accuracy: 0.9712 - val_loss: 1.9098 - val_accuracy: 0.9379

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    16/16 [==============================] - 23s 1s/step - loss: 0.2061 - accuracy: 0.9876 - val_loss: 2.8723 - val_accuracy: 0.9255

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    16/16 [==============================] - 23s 1s/step - loss: 0.2351 - accuracy: 0.9804 - val_loss: 4.3352 - val_accuracy: 0.8944

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3532 - accuracy: 0.9773 - val_loss: 2.9605 - val_accuracy: 0.9193

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    16/16 [==============================] - 23s 1s/step - loss: 0.2926 - accuracy: 0.9784 - val_loss: 2.1539 - val_accuracy: 0.9193

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    16/16 [==============================] - 24s 1s/step - loss: 0.3315 - accuracy: 0.9794 - val_loss: 2.7019 - val_accuracy: 0.9503

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2026 - accuracy: 0.9825 - val_loss: 3.0840 - val_accuracy: 0.9130

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2522 - accuracy: 0.9773 - val_loss: 2.3131 - val_accuracy: 0.9130

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3551 - accuracy: 0.9773 - val_loss: 3.5210 - val_accuracy: 0.9006

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1458 - accuracy: 0.9928 - val_loss: 3.3687 - val_accuracy: 0.9068

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    16/16 [==============================] - 22s 1s/step - loss: 0.1971 - accuracy: 0.9876 - val_loss: 3.8887 - val_accuracy: 0.9130

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    16/16 [==============================] - 23s 1s/step - loss: 0.4230 - accuracy: 0.9753 - val_loss: 2.1100 - val_accuracy: 0.9317

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    16/16 [==============================] - 21s 1s/step - loss: 0.2308 - accuracy: 0.9804 - val_loss: 2.4169 - val_accuracy: 0.9193

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    16/16 [==============================] - 20s 1s/step - loss: 0.1083 - accuracy: 0.9907 - val_loss: 2.2354 - val_accuracy: 0.9441

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2539 - accuracy: 0.9856 - val_loss: 2.7094 - val_accuracy: 0.9006

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2346 - accuracy: 0.9887 - val_loss: 2.4479 - val_accuracy: 0.9317

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4267 - accuracy: 0.9691 - val_loss: 2.7020 - val_accuracy: 0.9193

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    16/16 [==============================] - 22s 1s/step - loss: 0.5548 - accuracy: 0.9722 - val_loss: 2.7034 - val_accuracy: 0.9317

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3569 - accuracy: 0.9784 - val_loss: 1.7690 - val_accuracy: 0.9317

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    16/16 [==============================] - 22s 1s/step - loss: 0.1303 - accuracy: 0.9866 - val_loss: 2.5049 - val_accuracy: 0.9379

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2052 - accuracy: 0.9784 - val_loss: 2.2921 - val_accuracy: 0.9503

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3078 - accuracy: 0.9835 - val_loss: 3.0462 - val_accuracy: 0.9255

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    16/16 [==============================] - 22s 1s/step - loss: 0.1292 - accuracy: 0.9887 - val_loss: 2.1501 - val_accuracy: 0.9379

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2755 - accuracy: 0.9846 - val_loss: 4.9037 - val_accuracy: 0.8820

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2476 - accuracy: 0.9815 - val_loss: 2.3429 - val_accuracy: 0.9441

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1696 - accuracy: 0.9907 - val_loss: 1.7611 - val_accuracy: 0.9565

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    16/16 [==============================] - 21s 1s/step - loss: 0.3216 - accuracy: 0.9804 - val_loss: 1.8333 - val_accuracy: 0.9441

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    16/16 [==============================] - 23s 1s/step - loss: 0.2314 - accuracy: 0.9866 - val_loss: 3.6156 - val_accuracy: 0.9006

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2720 - accuracy: 0.9835 - val_loss: 2.4087 - val_accuracy: 0.9317

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2060 - accuracy: 0.9835 - val_loss: 1.6843 - val_accuracy: 0.9503

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    16/16 [==============================] - 22s 1s/step - loss: 0.3267 - accuracy: 0.9835 - val_loss: 4.6289 - val_accuracy: 0.9068

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2042 - accuracy: 0.9815 - val_loss: 1.3127 - val_accuracy: 0.9441

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    16/16 [==============================] - 23s 1s/step - loss: 0.3810 - accuracy: 0.9804 - val_loss: 1.6982 - val_accuracy: 0.9379

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2335 - accuracy: 0.9784 - val_loss: 4.7959 - val_accuracy: 0.8385

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    16/16 [==============================] - 23s 1s/step - loss: 0.3309 - accuracy: 0.9794 - val_loss: 1.2012 - val_accuracy: 0.9565

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    16/16 [==============================] - 23s 1s/step - loss: 0.3847 - accuracy: 0.9773 - val_loss: 1.1846 - val_accuracy: 0.9441

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    16/16 [==============================] - 21s 1s/step - loss: 0.4286 - accuracy: 0.9773 - val_loss: 2.1556 - val_accuracy: 0.9379

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    16/16 [==============================] - 22s 1s/step - loss: 0.2828 - accuracy: 0.9866 - val_loss: 1.4638 - val_accuracy: 0.9503

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    16/16 [==============================] - 22s 1s/step - loss: 0.1007 - accuracy: 0.9918 - val_loss: 1.2435 - val_accuracy: 0.9627

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    16/16 [==============================] - 23s 1s/step - loss: 0.1472 - accuracy: 0.9835 - val_loss: 1.5410 - val_accuracy: 0.9503

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    16/16 [==============================] - 21s 1s/step - loss: 0.1800 - accuracy: 0.9815 - val_loss: 1.5084 - val_accuracy: 0.9503

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    16/16 [==============================] - 22s 1s/step - loss: 0.1898 - accuracy: 0.9846 - val_loss: 1.4293 - val_accuracy: 0.9503

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    16/16 [==============================] - 22s 1s/step - loss: 0.0844 - accuracy: 0.9887 - val_loss: 1.3414 - val_accuracy: 0.9503

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/177.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 5s 1s/step - loss: 2.0705 - accuracy: 0.9324

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.0704970359802246, 0.9323770403862]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9323770491803278
                  precision    recall  f1-score   support

               0       0.91      0.86      0.89       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.93      0.91      0.92       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/178.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/179.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/180.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.91      0.86      0.89       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.93      0.91      0.92       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.91      0.86      0.96      0.89      0.91      0.82       148
              1       0.94      0.96      0.86      0.95      0.91      0.84       340

    avg / total       0.93      0.93      0.89      0.93      0.91      0.83       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
