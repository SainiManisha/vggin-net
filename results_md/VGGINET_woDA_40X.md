```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = '1'
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

    ('40X', '1')



```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________



```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________



```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```


```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```


```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```


```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```


```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________



```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```


```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```


```python
name = 'VGGINetWODA/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```


```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```


```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```


```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     # NO DATA AUGMENTATION WILL BE USED FOR THIS EXPERIMENT!
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.



```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

    Epoch 1/100
    1/9 [==>...........................] - ETA: 0s - loss: 1.5235 - accuracy: 0.3516WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 38s 4s/step - loss: 3.1708 - accuracy: 0.7530 - val_loss: 14.5245 - val_accuracy: 0.6592
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4117 - accuracy: 0.9526 - val_loss: 7.9874 - val_accuracy: 0.8212
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1309 - accuracy: 0.9731 - val_loss: 10.7165 - val_accuracy: 0.7542
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1342 - accuracy: 0.9805 - val_loss: 3.3002 - val_accuracy: 0.9106
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0387 - accuracy: 0.9963 - val_loss: 3.5729 - val_accuracy: 0.9106
    Epoch 6/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0044 - accuracy: 0.9981 - val_loss: 2.7192 - val_accuracy: 0.9106
    Epoch 7/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0104 - accuracy: 0.9981 - val_loss: 2.2206 - val_accuracy: 0.9162
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 3.1806e-04 - accuracy: 1.0000 - val_loss: 1.9455 - val_accuracy: 0.9162
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 4.0227e-04 - accuracy: 1.0000 - val_loss: 1.6936 - val_accuracy: 0.9162
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 6.9923e-04 - accuracy: 0.9991 - val_loss: 1.5289 - val_accuracy: 0.9218
    Epoch 11/100
    9/9 [==============================] - 10s 1s/step - loss: 4.2716e-05 - accuracy: 1.0000 - val_loss: 1.4413 - val_accuracy: 0.9162
    Epoch 12/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0026 - accuracy: 0.9991 - val_loss: 1.3952 - val_accuracy: 0.9330
    Epoch 13/100
    9/9 [==============================] - 10s 1s/step - loss: 6.4586e-04 - accuracy: 1.0000 - val_loss: 1.3644 - val_accuracy: 0.9218
    Epoch 14/100
    9/9 [==============================] - 10s 1s/step - loss: 9.6476e-04 - accuracy: 1.0000 - val_loss: 1.3231 - val_accuracy: 0.9106
    Epoch 15/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0010 - accuracy: 0.9991 - val_loss: 1.2659 - val_accuracy: 0.9162
    Epoch 16/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0033 - accuracy: 0.9972 - val_loss: 1.3937 - val_accuracy: 0.9162
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0043 - accuracy: 0.9991 - val_loss: 2.1376 - val_accuracy: 0.8827
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0231 - accuracy: 0.9963 - val_loss: 1.5825 - val_accuracy: 0.8994
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0084 - accuracy: 0.9972 - val_loss: 1.4221 - val_accuracy: 0.8994
    Epoch 20/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0111 - accuracy: 0.9972 - val_loss: 1.7443 - val_accuracy: 0.8994
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0051 - accuracy: 0.9981 - val_loss: 1.4920 - val_accuracy: 0.8994
    Epoch 22/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0064 - accuracy: 0.9981 - val_loss: 1.9396 - val_accuracy: 0.8939
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0235 - accuracy: 0.9972 - val_loss: 1.4540 - val_accuracy: 0.9106
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0316 - accuracy: 0.9935 - val_loss: 1.6959 - val_accuracy: 0.9106
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0236 - accuracy: 0.9981 - val_loss: 1.9706 - val_accuracy: 0.8994
    Epoch 26/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0169 - accuracy: 0.9981 - val_loss: 2.2879 - val_accuracy: 0.8994
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0050 - accuracy: 0.9991 - val_loss: 1.9259 - val_accuracy: 0.8994
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0049 - accuracy: 0.9981 - val_loss: 1.8360 - val_accuracy: 0.8939
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0094 - accuracy: 0.9944 - val_loss: 2.0958 - val_accuracy: 0.8883
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0178 - accuracy: 0.9981 - val_loss: 2.4112 - val_accuracy: 0.8994
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0034 - accuracy: 0.9981 - val_loss: 1.8856 - val_accuracy: 0.8994
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0261 - accuracy: 0.9972 - val_loss: 1.4838 - val_accuracy: 0.9218
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0479 - accuracy: 0.9935 - val_loss: 2.8165 - val_accuracy: 0.8547
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0588 - accuracy: 0.9926 - val_loss: 1.7003 - val_accuracy: 0.8994
    Epoch 35/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1629 - accuracy: 0.9805 - val_loss: 2.2874 - val_accuracy: 0.8771
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0660 - accuracy: 0.9916 - val_loss: 2.2573 - val_accuracy: 0.8659
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0426 - accuracy: 0.9944 - val_loss: 2.6950 - val_accuracy: 0.8883
    Epoch 38/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0293 - accuracy: 0.9935 - val_loss: 1.6183 - val_accuracy: 0.9162
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0058 - accuracy: 0.9972 - val_loss: 1.4686 - val_accuracy: 0.9218
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0243 - accuracy: 0.9963 - val_loss: 1.5621 - val_accuracy: 0.9218
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0151 - accuracy: 0.9972 - val_loss: 1.4772 - val_accuracy: 0.9162
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0013 - accuracy: 0.9991 - val_loss: 1.6195 - val_accuracy: 0.9218
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0044 - accuracy: 0.9991 - val_loss: 1.4757 - val_accuracy: 0.8994
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0067 - accuracy: 0.9991 - val_loss: 1.4039 - val_accuracy: 0.9162
    Epoch 45/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0044 - accuracy: 0.9991 - val_loss: 1.5747 - val_accuracy: 0.9274
    Epoch 46/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0021 - accuracy: 0.9991 - val_loss: 1.3369 - val_accuracy: 0.9218
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 4.9170e-06 - accuracy: 1.0000 - val_loss: 1.3736 - val_accuracy: 0.9274
    Epoch 48/100
    9/9 [==============================] - 10s 1s/step - loss: 8.1131e-08 - accuracy: 1.0000 - val_loss: 1.4208 - val_accuracy: 0.9274
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 8.4753e-05 - accuracy: 1.0000 - val_loss: 1.4262 - val_accuracy: 0.9274
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 3.8413e-05 - accuracy: 1.0000 - val_loss: 1.3974 - val_accuracy: 0.9274
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0015 - accuracy: 0.9991 - val_loss: 1.3638 - val_accuracy: 0.9274
    Epoch 52/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0043 - accuracy: 0.9991 - val_loss: 1.1401 - val_accuracy: 0.9330
    Epoch 53/100
    9/9 [==============================] - 12s 1s/step - loss: 0.0249 - accuracy: 0.9963 - val_loss: 0.9703 - val_accuracy: 0.9274
    Epoch 54/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0500 - accuracy: 0.9926 - val_loss: 2.0170 - val_accuracy: 0.9162
    Epoch 55/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1861 - accuracy: 0.9833 - val_loss: 2.3917 - val_accuracy: 0.8994
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1846 - accuracy: 0.9870 - val_loss: 4.3420 - val_accuracy: 0.8771
    Epoch 57/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1667 - accuracy: 0.9851 - val_loss: 3.9428 - val_accuracy: 0.8883
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0839 - accuracy: 0.9926 - val_loss: 3.6668 - val_accuracy: 0.8827
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0917 - accuracy: 0.9907 - val_loss: 3.4722 - val_accuracy: 0.8715
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0685 - accuracy: 0.9954 - val_loss: 3.2182 - val_accuracy: 0.9106
    Epoch 61/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0115 - accuracy: 0.9981 - val_loss: 2.7613 - val_accuracy: 0.9106
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 9.8230e-04 - accuracy: 0.9991 - val_loss: 2.4982 - val_accuracy: 0.8939
    Epoch 63/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0331 - accuracy: 0.9981 - val_loss: 2.6276 - val_accuracy: 0.8883
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0343 - accuracy: 0.9963 - val_loss: 2.4459 - val_accuracy: 0.9050
    Epoch 65/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0384 - accuracy: 0.9981 - val_loss: 2.4301 - val_accuracy: 0.9050
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 2.4996e-06 - accuracy: 1.0000 - val_loss: 2.1822 - val_accuracy: 0.9162
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 3.2681e-06 - accuracy: 1.0000 - val_loss: 2.1402 - val_accuracy: 0.9162
    Epoch 68/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0017 - accuracy: 0.9991 - val_loss: 2.1183 - val_accuracy: 0.9274
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 2.0886e-06 - accuracy: 1.0000 - val_loss: 2.0010 - val_accuracy: 0.9218
    Epoch 70/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0020 - accuracy: 0.9991 - val_loss: 1.8945 - val_accuracy: 0.9218
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0023 - accuracy: 0.9991 - val_loss: 1.9789 - val_accuracy: 0.9274
    Epoch 72/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0184 - accuracy: 0.9972 - val_loss: 2.7951 - val_accuracy: 0.8994
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0027 - accuracy: 0.9981 - val_loss: 2.9637 - val_accuracy: 0.9050
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 4.4274e-08 - accuracy: 1.0000 - val_loss: 2.8671 - val_accuracy: 0.8939
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0052 - accuracy: 0.9991 - val_loss: 3.0109 - val_accuracy: 0.8994
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 4.9151e-06 - accuracy: 1.0000 - val_loss: 3.2595 - val_accuracy: 0.8939
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0076 - accuracy: 0.9972 - val_loss: 2.8967 - val_accuracy: 0.8994
    Epoch 78/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0161 - accuracy: 0.9972 - val_loss: 3.6551 - val_accuracy: 0.8827
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0102 - accuracy: 0.9972 - val_loss: 3.1387 - val_accuracy: 0.8994
    Epoch 80/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0082 - accuracy: 0.9991 - val_loss: 2.2672 - val_accuracy: 0.8994
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0024 - accuracy: 0.9991 - val_loss: 2.2134 - val_accuracy: 0.9106
    Epoch 82/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0072 - accuracy: 0.9991 - val_loss: 2.6451 - val_accuracy: 0.9050
    Epoch 83/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1733e-08 - accuracy: 1.0000 - val_loss: 2.7668 - val_accuracy: 0.9106
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0053 - accuracy: 0.9981 - val_loss: 2.7814 - val_accuracy: 0.9162
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0085 - accuracy: 0.9972 - val_loss: 3.5982 - val_accuracy: 0.9162
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 9.0229e-04 - accuracy: 0.9991 - val_loss: 3.3727 - val_accuracy: 0.9106
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 9.9413e-04 - accuracy: 0.9991 - val_loss: 3.4045 - val_accuracy: 0.9162
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 4.9774e-06 - accuracy: 1.0000 - val_loss: 3.3664 - val_accuracy: 0.9162
    Epoch 89/100
    9/9 [==============================] - 10s 1s/step - loss: 4.0954e-09 - accuracy: 1.0000 - val_loss: 3.3647 - val_accuracy: 0.9162
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0000e+00 - accuracy: 1.0000 - val_loss: 3.3645 - val_accuracy: 0.9162
    Epoch 91/100
    9/9 [==============================] - 10s 1s/step - loss: 8.8549e-09 - accuracy: 1.0000 - val_loss: 3.3626 - val_accuracy: 0.9162
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0070 - accuracy: 0.9981 - val_loss: 3.3777 - val_accuracy: 0.9218
    Epoch 93/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0242 - accuracy: 0.9991 - val_loss: 4.5232 - val_accuracy: 0.8994
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 4.1640e-04 - accuracy: 1.0000 - val_loss: 4.7116 - val_accuracy: 0.8994
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0068 - accuracy: 0.9981 - val_loss: 3.9155 - val_accuracy: 0.9050
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 3.1435e-08 - accuracy: 1.0000 - val_loss: 3.2862 - val_accuracy: 0.8883
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8325e-05 - accuracy: 1.0000 - val_loss: 3.1100 - val_accuracy: 0.8883
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0058 - accuracy: 0.9991 - val_loss: 3.3906 - val_accuracy: 0.8994
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 1.3503e-07 - accuracy: 1.0000 - val_loss: 3.8059 - val_accuracy: 0.8994
    Epoch 100/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0000e+00 - accuracy: 1.0000 - val_loss: 3.9746 - val_accuracy: 0.8994



```python
new_model.save_weights(path + '/model.h5')
```


```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```


```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```


    
![png](VGGINET_woDA_40X_files/VGGINET_woDA_40X_17_0.png)
    



```python
new_model.evaluate(test_ds)
```

    5/5 [==============================] - 15s 3s/step - loss: 2.4466 - accuracy: 0.9239





    [2.4465999603271484, 0.923933207988739]




```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

    Accuracy:  0.9239332096474954
                  precision    recall  f1-score   support
    
               0       0.91      0.82      0.86       158
               1       0.93      0.97      0.95       381
    
        accuracy                           0.92       539
       macro avg       0.92      0.89      0.91       539
    weighted avg       0.92      0.92      0.92       539
    



```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```


```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```




    <AxesSubplot:title={'center':'Normalized Confusion Matrix'}, xlabel='Predicted label', ylabel='True label'>




    
![png](VGGINET_woDA_40X_files/VGGINET_woDA_40X_21_1.png)
    



    
![png](VGGINET_woDA_40X_files/VGGINET_woDA_40X_21_2.png)
    



```python
plot_roc(y_true, y_probas)
```




    <AxesSubplot:title={'center':'ROC Curves'}, xlabel='False Positive Rate', ylabel='True Positive Rate'>




    
![png](VGGINET_woDA_40X_files/VGGINET_woDA_40X_22_1.png)
    



```python
from imblearn.metrics import classification_report_imbalanced
```


```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.91      0.82      0.86       158
               1       0.93      0.97      0.95       381
    
        accuracy                           0.92       539
       macro avg       0.92      0.89      0.91       539
    weighted avg       0.92      0.92      0.92       539
    
                       pre       rec       spe        f1       geo       iba       sup
    
              0       0.91      0.82      0.97      0.86      0.89      0.78       158
              1       0.93      0.97      0.82      0.95      0.89      0.80       381
    
    avg / total       0.92      0.92      0.86      0.92      0.89      0.80       539
    



```python

```
