<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# REMOVE INCEPTION BLOCK<a class="anchor-link" href="#REMOVE-INCEPTION-BLOCK">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
# def naive_inception_module(layer_in, f1, f2, f3):
#     # 1x1 conv
#     conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
#     # 3x3 conv
#     conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
#     # 5x5 conv
#     conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
#     # 3x3 max pooling
#     pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
#     # concatenate filters, assumes filters/channels last
#     layer_out = Concatenate()([conv1, conv3, conv5, pool])
#     return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = feature_ex_model.output
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    Image_Input (InputLayer)     [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    VGG_Preprocess (Lambda)      (None, 224, 224, 3)       0         
    _________________________________________________________________
    vgg16_features (Functional)  (None, 14, 14, 512)       7635264   
    _________________________________________________________________
    BN (BatchNormalization)      (None, 14, 14, 512)       2048      
    _________________________________________________________________
    flatten (Flatten)            (None, 100352)            0         
    _________________________________________________________________
    Dropout (Dropout)            (None, 100352)            0         
    _________________________________________________________________
    Predictions (Dense)          (None, 2)                 200706    
    =================================================================
    Total params: 7,838,018
    Trainable params: 201,730
    Non-trainable params: 7,636,288
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3453 - accuracy: 0.5078WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 32s 4s/step - loss: 1.9265 - accuracy: 0.7296 - val_loss: 2.4547 - val_accuracy: 0.8930
    Epoch 2/100
    9/9 [==============================] - 17s 2s/step - loss: 1.0822 - accuracy: 0.8475 - val_loss: 1.6164 - val_accuracy: 0.8930
    Epoch 3/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9617 - accuracy: 0.8635 - val_loss: 1.1137 - val_accuracy: 0.8984
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6322 - accuracy: 0.8998 - val_loss: 1.1856 - val_accuracy: 0.8877
    Epoch 5/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7979 - accuracy: 0.8750 - val_loss: 1.4675 - val_accuracy: 0.8717
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7678 - accuracy: 0.8777 - val_loss: 1.1449 - val_accuracy: 0.9037
    Epoch 7/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9104 - accuracy: 0.8670 - val_loss: 0.7964 - val_accuracy: 0.9358
    Epoch 8/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8979 - accuracy: 0.8732 - val_loss: 0.6138 - val_accuracy: 0.9198
    Epoch 9/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8522 - accuracy: 0.8910 - val_loss: 0.6218 - val_accuracy: 0.9358
    Epoch 10/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8658 - accuracy: 0.8980 - val_loss: 0.5062 - val_accuracy: 0.9412
    Epoch 11/100
    9/9 [==============================] - 17s 2s/step - loss: 0.6847 - accuracy: 0.8945 - val_loss: 0.7235 - val_accuracy: 0.9251
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7125 - accuracy: 0.8936 - val_loss: 0.7770 - val_accuracy: 0.9198
    Epoch 13/100
    9/9 [==============================] - 17s 2s/step - loss: 0.9281 - accuracy: 0.8936 - val_loss: 0.8401 - val_accuracy: 0.9251
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7330 - accuracy: 0.8936 - val_loss: 0.7319 - val_accuracy: 0.9305
    Epoch 15/100
    9/9 [==============================] - 17s 2s/step - loss: 1.0291 - accuracy: 0.8821 - val_loss: 0.7824 - val_accuracy: 0.9144
    Epoch 16/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7952 - accuracy: 0.8927 - val_loss: 0.8118 - val_accuracy: 0.9144
    Epoch 17/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9976 - accuracy: 0.8892 - val_loss: 0.6750 - val_accuracy: 0.9305
    Epoch 18/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9511 - accuracy: 0.8918 - val_loss: 0.7881 - val_accuracy: 0.9198
    Epoch 19/100
    9/9 [==============================] - 16s 2s/step - loss: 1.1858 - accuracy: 0.8848 - val_loss: 0.6882 - val_accuracy: 0.9251
    Epoch 20/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9335 - accuracy: 0.8723 - val_loss: 0.7103 - val_accuracy: 0.9358
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1297 - accuracy: 0.9051 - val_loss: 0.9399 - val_accuracy: 0.9144
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7934 - accuracy: 0.9051 - val_loss: 0.5901 - val_accuracy: 0.9198
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9021 - accuracy: 0.9016 - val_loss: 0.5095 - val_accuracy: 0.9198
    Epoch 24/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8186 - accuracy: 0.9149 - val_loss: 0.5485 - val_accuracy: 0.9358
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8315 - accuracy: 0.9060 - val_loss: 0.4793 - val_accuracy: 0.9251
    Epoch 26/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8108 - accuracy: 0.9113 - val_loss: 0.6422 - val_accuracy: 0.9198
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8379 - accuracy: 0.9087 - val_loss: 0.7323 - val_accuracy: 0.9198
    Epoch 28/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8214 - accuracy: 0.9158 - val_loss: 0.5950 - val_accuracy: 0.9198
    Epoch 29/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7484 - accuracy: 0.9113 - val_loss: 0.6515 - val_accuracy: 0.9412
    Epoch 30/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9081 - accuracy: 0.9176 - val_loss: 0.5946 - val_accuracy: 0.9412
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7079 - accuracy: 0.9176 - val_loss: 0.4986 - val_accuracy: 0.9465
    Epoch 32/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9363 - accuracy: 0.9034 - val_loss: 0.5060 - val_accuracy: 0.9358
    Epoch 33/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8291 - accuracy: 0.9096 - val_loss: 0.6083 - val_accuracy: 0.9358
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8124 - accuracy: 0.9087 - val_loss: 0.6522 - val_accuracy: 0.9358
    Epoch 35/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8571 - accuracy: 0.9051 - val_loss: 0.6076 - val_accuracy: 0.9519
    Epoch 36/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7537 - accuracy: 0.9087 - val_loss: 0.5308 - val_accuracy: 0.9358
    Epoch 37/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7638 - accuracy: 0.9193 - val_loss: 0.4968 - val_accuracy: 0.9519
    Epoch 38/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9167 - accuracy: 0.9122 - val_loss: 0.5186 - val_accuracy: 0.9465
    Epoch 39/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8462 - accuracy: 0.9096 - val_loss: 0.5543 - val_accuracy: 0.9519
    Epoch 40/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8996 - accuracy: 0.9158 - val_loss: 0.5505 - val_accuracy: 0.9412
    Epoch 41/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7840 - accuracy: 0.9264 - val_loss: 0.5597 - val_accuracy: 0.9198
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7584 - accuracy: 0.9255 - val_loss: 0.5564 - val_accuracy: 0.9358
    Epoch 43/100
    9/9 [==============================] - 19s 2s/step - loss: 0.6263 - accuracy: 0.9300 - val_loss: 0.6504 - val_accuracy: 0.9144
    Epoch 44/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9388 - accuracy: 0.9060 - val_loss: 0.6120 - val_accuracy: 0.9465
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8329 - accuracy: 0.9131 - val_loss: 0.4919 - val_accuracy: 0.9465
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6962 - accuracy: 0.9317 - val_loss: 0.4263 - val_accuracy: 0.9358
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8036 - accuracy: 0.9220 - val_loss: 0.2910 - val_accuracy: 0.9519
    Epoch 48/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6950 - accuracy: 0.9176 - val_loss: 0.3654 - val_accuracy: 0.9465
    Epoch 49/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7944 - accuracy: 0.9202 - val_loss: 0.3541 - val_accuracy: 0.9465
    Epoch 50/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7191 - accuracy: 0.9246 - val_loss: 0.3704 - val_accuracy: 0.9412
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9254 - accuracy: 0.9113 - val_loss: 0.3736 - val_accuracy: 0.9358
    Epoch 52/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9581 - accuracy: 0.9007 - val_loss: 0.4290 - val_accuracy: 0.9465
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7975 - accuracy: 0.9255 - val_loss: 0.4735 - val_accuracy: 0.9519
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8599 - accuracy: 0.9273 - val_loss: 0.4027 - val_accuracy: 0.9465
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9233 - accuracy: 0.9025 - val_loss: 0.3931 - val_accuracy: 0.9626
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9182 - accuracy: 0.9202 - val_loss: 0.3584 - val_accuracy: 0.9626
    Epoch 57/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9438 - accuracy: 0.9291 - val_loss: 0.4497 - val_accuracy: 0.9519
    Epoch 58/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6690 - accuracy: 0.9362 - val_loss: 0.5158 - val_accuracy: 0.9519
    Epoch 59/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8375 - accuracy: 0.9167 - val_loss: 0.4065 - val_accuracy: 0.9519
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8420 - accuracy: 0.9184 - val_loss: 0.3055 - val_accuracy: 0.9626
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8130 - accuracy: 0.9220 - val_loss: 0.3634 - val_accuracy: 0.9572
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9496 - accuracy: 0.9193 - val_loss: 0.5456 - val_accuracy: 0.9465
    Epoch 63/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9910 - accuracy: 0.9149 - val_loss: 0.5609 - val_accuracy: 0.9572
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7866 - accuracy: 0.9264 - val_loss: 0.7760 - val_accuracy: 0.9412
    Epoch 65/100
    9/9 [==============================] - 18s 2s/step - loss: 0.9652 - accuracy: 0.9149 - val_loss: 0.6721 - val_accuracy: 0.9465
    Epoch 66/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9700 - accuracy: 0.9167 - val_loss: 0.6313 - val_accuracy: 0.9519
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8694 - accuracy: 0.9105 - val_loss: 0.6189 - val_accuracy: 0.9519
    Epoch 68/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7555 - accuracy: 0.9344 - val_loss: 0.5848 - val_accuracy: 0.9465
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8511 - accuracy: 0.9184 - val_loss: 0.6287 - val_accuracy: 0.9465
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8853 - accuracy: 0.9131 - val_loss: 0.6586 - val_accuracy: 0.9358
    Epoch 71/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8154 - accuracy: 0.9282 - val_loss: 0.5304 - val_accuracy: 0.9465
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0197 - accuracy: 0.9060 - val_loss: 0.4984 - val_accuracy: 0.9465
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8133 - accuracy: 0.9335 - val_loss: 0.6249 - val_accuracy: 0.9519
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8149 - accuracy: 0.9211 - val_loss: 0.7273 - val_accuracy: 0.9412
    Epoch 75/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7395 - accuracy: 0.9300 - val_loss: 0.6861 - val_accuracy: 0.9519
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9677 - accuracy: 0.9193 - val_loss: 0.6799 - val_accuracy: 0.9465
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8814 - accuracy: 0.9282 - val_loss: 0.7005 - val_accuracy: 0.9519
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8392 - accuracy: 0.9317 - val_loss: 0.6403 - val_accuracy: 0.9358
    Epoch 79/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7460 - accuracy: 0.9300 - val_loss: 0.5850 - val_accuracy: 0.9465
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8151 - accuracy: 0.9184 - val_loss: 0.5284 - val_accuracy: 0.9519
    Epoch 81/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9291 - accuracy: 0.9246 - val_loss: 0.5063 - val_accuracy: 0.9465
    Epoch 82/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8263 - accuracy: 0.9220 - val_loss: 0.5371 - val_accuracy: 0.9465
    Epoch 83/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6964 - accuracy: 0.9326 - val_loss: 0.6371 - val_accuracy: 0.9412
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6241 - accuracy: 0.9335 - val_loss: 0.7439 - val_accuracy: 0.9412
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0137 - accuracy: 0.9229 - val_loss: 0.7750 - val_accuracy: 0.9412
    Epoch 86/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0197 - accuracy: 0.9167 - val_loss: 0.6869 - val_accuracy: 0.9358
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8581 - accuracy: 0.9282 - val_loss: 0.6214 - val_accuracy: 0.9465
    Epoch 88/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9280 - accuracy: 0.9273 - val_loss: 0.6019 - val_accuracy: 0.9412
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7343 - accuracy: 0.9291 - val_loss: 0.6479 - val_accuracy: 0.9251
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9768 - accuracy: 0.9229 - val_loss: 0.4458 - val_accuracy: 0.9465
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9069 - accuracy: 0.9291 - val_loss: 0.4117 - val_accuracy: 0.9572
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7500 - accuracy: 0.9273 - val_loss: 0.4596 - val_accuracy: 0.9465
    Epoch 93/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9083 - accuracy: 0.9335 - val_loss: 0.5440 - val_accuracy: 0.9305
    Epoch 94/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8125 - accuracy: 0.9273 - val_loss: 0.7723 - val_accuracy: 0.9412
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0088 - accuracy: 0.9291 - val_loss: 0.4610 - val_accuracy: 0.9572
    Epoch 96/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0733 - accuracy: 0.9105 - val_loss: 0.4154 - val_accuracy: 0.9519
    Epoch 97/100
    9/9 [==============================] - 16s 2s/step - loss: 1.0446 - accuracy: 0.9105 - val_loss: 0.4097 - val_accuracy: 0.9626
    Epoch 98/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1049 - accuracy: 0.9220 - val_loss: 0.3703 - val_accuracy: 0.9626
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8025 - accuracy: 0.9300 - val_loss: 0.3531 - val_accuracy: 0.9679
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5809 - accuracy: 0.9379 - val_loss: 0.3992 - val_accuracy: 0.9733
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/23.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 33s 7s/step - loss: 0.7184 - accuracy: 0.9523

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7184486985206604, 0.952296793460846]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9522968197879859
                  precision    recall  f1-score   support

               0       0.90      0.94      0.92       164
               1       0.97      0.96      0.97       402

        accuracy                           0.95       566
       macro avg       0.94      0.95      0.94       566
    weighted avg       0.95      0.95      0.95       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/24.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/25.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/26.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.90      0.94      0.92       164
               1       0.97      0.96      0.97       402

        accuracy                           0.95       566
       macro avg       0.94      0.95      0.94       566
    weighted avg       0.95      0.95      0.95       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.90      0.94      0.96      0.92      0.95      0.90       164
              1       0.97      0.96      0.94      0.97      0.95      0.90       402

    avg / total       0.95      0.95      0.94      0.95      0.95      0.90       566

</div>
</div>
</div>
</div>
</div>
</div>
</div>
