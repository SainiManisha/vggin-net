<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK3and4-FINETUNING_100X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.4685 - accuracy: 0.4688WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 4.4570 - accuracy: 0.7287 - val_loss: 13.7624 - val_accuracy: 0.7914
    Epoch 2/100
    9/9 [==============================] - 11s 1s/step - loss: 2.5283 - accuracy: 0.8564 - val_loss: 6.2960 - val_accuracy: 0.8877
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8403 - accuracy: 0.8794 - val_loss: 2.7406 - val_accuracy: 0.9091
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.5508 - accuracy: 0.8892 - val_loss: 1.6717 - val_accuracy: 0.9251
    Epoch 5/100
    9/9 [==============================] - 11s 1s/step - loss: 1.2790 - accuracy: 0.8936 - val_loss: 2.0054 - val_accuracy: 0.9305
    Epoch 6/100
    9/9 [==============================] - 15s 2s/step - loss: 1.2025 - accuracy: 0.8998 - val_loss: 1.7233 - val_accuracy: 0.9412
    Epoch 7/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8299 - accuracy: 0.9246 - val_loss: 1.0867 - val_accuracy: 0.9412
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9395 - accuracy: 0.9184 - val_loss: 1.2229 - val_accuracy: 0.9198
    Epoch 9/100
    9/9 [==============================] - 11s 1s/step - loss: 0.9545 - accuracy: 0.9149 - val_loss: 1.1813 - val_accuracy: 0.9412
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9498 - accuracy: 0.9060 - val_loss: 1.0307 - val_accuracy: 0.9519
    Epoch 11/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7503 - accuracy: 0.9344 - val_loss: 1.5365 - val_accuracy: 0.9198
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6634 - accuracy: 0.9415 - val_loss: 0.9505 - val_accuracy: 0.9572
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8129 - accuracy: 0.9264 - val_loss: 1.4356 - val_accuracy: 0.9305
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7802 - accuracy: 0.9255 - val_loss: 1.1174 - val_accuracy: 0.9305
    Epoch 15/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7748 - accuracy: 0.9344 - val_loss: 0.7781 - val_accuracy: 0.9412
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2561 - accuracy: 0.9069 - val_loss: 0.9709 - val_accuracy: 0.9465
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2476 - accuracy: 0.9246 - val_loss: 0.7318 - val_accuracy: 0.9465
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1209 - accuracy: 0.9202 - val_loss: 0.7779 - val_accuracy: 0.9465
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9585 - accuracy: 0.9388 - val_loss: 1.3710 - val_accuracy: 0.9412
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7382 - accuracy: 0.9512 - val_loss: 0.7025 - val_accuracy: 0.9412
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8097 - accuracy: 0.9415 - val_loss: 0.8871 - val_accuracy: 0.9626
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7560 - accuracy: 0.9344 - val_loss: 0.6595 - val_accuracy: 0.9572
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6517 - accuracy: 0.9406 - val_loss: 0.4896 - val_accuracy: 0.9626
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7712 - accuracy: 0.9379 - val_loss: 0.5252 - val_accuracy: 0.9572
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5276 - accuracy: 0.9574 - val_loss: 0.5404 - val_accuracy: 0.9626
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3138 - accuracy: 0.9637 - val_loss: 0.7395 - val_accuracy: 0.9572
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4904 - accuracy: 0.9619 - val_loss: 0.2958 - val_accuracy: 0.9626
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3145 - accuracy: 0.9645 - val_loss: 0.5952 - val_accuracy: 0.9626
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5203 - accuracy: 0.9557 - val_loss: 0.7303 - val_accuracy: 0.9572
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6389 - accuracy: 0.9512 - val_loss: 0.6839 - val_accuracy: 0.9412
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5630 - accuracy: 0.9539 - val_loss: 0.9396 - val_accuracy: 0.9412
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6715 - accuracy: 0.9495 - val_loss: 0.5875 - val_accuracy: 0.9679
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4765 - accuracy: 0.9645 - val_loss: 0.7511 - val_accuracy: 0.9572
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3918 - accuracy: 0.9699 - val_loss: 0.7031 - val_accuracy: 0.9626
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5822 - accuracy: 0.9477 - val_loss: 0.7486 - val_accuracy: 0.9626
    Epoch 36/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4839 - accuracy: 0.9654 - val_loss: 0.4338 - val_accuracy: 0.9679
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4878 - accuracy: 0.9583 - val_loss: 0.1889 - val_accuracy: 0.9786
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3664 - accuracy: 0.9699 - val_loss: 0.5520 - val_accuracy: 0.9626
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4738 - accuracy: 0.9619 - val_loss: 0.6235 - val_accuracy: 0.9840
    Epoch 40/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5280 - accuracy: 0.9619 - val_loss: 0.5678 - val_accuracy: 0.9786
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5013 - accuracy: 0.9637 - val_loss: 0.8411 - val_accuracy: 0.9572
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2822 - accuracy: 0.9707 - val_loss: 1.1535 - val_accuracy: 0.9519
    Epoch 43/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3569 - accuracy: 0.9716 - val_loss: 0.7535 - val_accuracy: 0.9572
    Epoch 44/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3297 - accuracy: 0.9699 - val_loss: 0.0606 - val_accuracy: 0.9947
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6566 - accuracy: 0.9566 - val_loss: 0.4767 - val_accuracy: 0.9519
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4800 - accuracy: 0.9663 - val_loss: 1.1839 - val_accuracy: 0.9091
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4666 - accuracy: 0.9628 - val_loss: 0.3754 - val_accuracy: 0.9572
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4574 - accuracy: 0.9681 - val_loss: 0.5457 - val_accuracy: 0.9679
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2375 - accuracy: 0.9796 - val_loss: 0.6340 - val_accuracy: 0.9679
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3804 - accuracy: 0.9743 - val_loss: 0.5852 - val_accuracy: 0.9733
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4957 - accuracy: 0.9645 - val_loss: 1.5482 - val_accuracy: 0.9358
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3882 - accuracy: 0.9699 - val_loss: 1.3898 - val_accuracy: 0.9144
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3444 - accuracy: 0.9725 - val_loss: 1.0603 - val_accuracy: 0.9572
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2738 - accuracy: 0.9734 - val_loss: 1.0241 - val_accuracy: 0.9519
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3792 - accuracy: 0.9716 - val_loss: 0.7090 - val_accuracy: 0.9519
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1763 - accuracy: 0.9805 - val_loss: 0.5538 - val_accuracy: 0.9786
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3089 - accuracy: 0.9743 - val_loss: 0.3788 - val_accuracy: 0.9679
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4101 - accuracy: 0.9716 - val_loss: 1.2726 - val_accuracy: 0.9519
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4629 - accuracy: 0.9743 - val_loss: 1.0443 - val_accuracy: 0.9519
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2729 - accuracy: 0.9787 - val_loss: 0.7863 - val_accuracy: 0.9679
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2649 - accuracy: 0.9849 - val_loss: 0.4699 - val_accuracy: 0.9733
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3071 - accuracy: 0.9690 - val_loss: 0.5368 - val_accuracy: 0.9626
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3520 - accuracy: 0.9770 - val_loss: 0.7844 - val_accuracy: 0.9679
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2711 - accuracy: 0.9814 - val_loss: 1.0810 - val_accuracy: 0.9519
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2194 - accuracy: 0.9796 - val_loss: 0.9585 - val_accuracy: 0.9572
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3637 - accuracy: 0.9690 - val_loss: 0.8491 - val_accuracy: 0.9572
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2252 - accuracy: 0.9770 - val_loss: 0.7084 - val_accuracy: 0.9679
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2364 - accuracy: 0.9814 - val_loss: 0.8393 - val_accuracy: 0.9679
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2469 - accuracy: 0.9778 - val_loss: 0.5850 - val_accuracy: 0.9679
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4177 - accuracy: 0.9699 - val_loss: 0.6130 - val_accuracy: 0.9679
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2791 - accuracy: 0.9787 - val_loss: 0.8581 - val_accuracy: 0.9412
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3814 - accuracy: 0.9743 - val_loss: 0.7075 - val_accuracy: 0.9679
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3953 - accuracy: 0.9716 - val_loss: 0.3577 - val_accuracy: 0.9840
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2972 - accuracy: 0.9796 - val_loss: 1.0345 - val_accuracy: 0.9572
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4882 - accuracy: 0.9672 - val_loss: 0.2741 - val_accuracy: 0.9733
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5002 - accuracy: 0.9690 - val_loss: 0.1038 - val_accuracy: 0.9893
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3411 - accuracy: 0.9734 - val_loss: 0.3770 - val_accuracy: 0.9786
    Epoch 78/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2279 - accuracy: 0.9796 - val_loss: 0.6686 - val_accuracy: 0.9733
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2647 - accuracy: 0.9832 - val_loss: 0.8783 - val_accuracy: 0.9572
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2964 - accuracy: 0.9823 - val_loss: 0.7392 - val_accuracy: 0.9733
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2543 - accuracy: 0.9840 - val_loss: 0.8996 - val_accuracy: 0.9572
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3320 - accuracy: 0.9761 - val_loss: 2.1640 - val_accuracy: 0.9251
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2709 - accuracy: 0.9814 - val_loss: 0.2634 - val_accuracy: 0.9840
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2788 - accuracy: 0.9805 - val_loss: 0.3038 - val_accuracy: 0.9679
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1714 - accuracy: 0.9885 - val_loss: 0.5044 - val_accuracy: 0.9733
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3532 - accuracy: 0.9734 - val_loss: 0.7972 - val_accuracy: 0.9572
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2011 - accuracy: 0.9832 - val_loss: 0.3187 - val_accuracy: 0.9626
    Epoch 88/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4181 - accuracy: 0.9796 - val_loss: 0.2260 - val_accuracy: 0.9840
    Epoch 89/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2852 - accuracy: 0.9823 - val_loss: 1.3442 - val_accuracy: 0.9519
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2087 - accuracy: 0.9858 - val_loss: 1.2005 - val_accuracy: 0.9679
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3340 - accuracy: 0.9849 - val_loss: 0.8735 - val_accuracy: 0.9679
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1963 - accuracy: 0.9867 - val_loss: 0.7770 - val_accuracy: 0.9679
    Epoch 93/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1983 - accuracy: 0.9849 - val_loss: 0.5723 - val_accuracy: 0.9733
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2619 - accuracy: 0.9805 - val_loss: 0.3053 - val_accuracy: 0.9786
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1300 - accuracy: 0.9840 - val_loss: 0.4094 - val_accuracy: 0.9626
    Epoch 96/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2268 - accuracy: 0.9858 - val_loss: 0.0761 - val_accuracy: 0.9947
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4038 - accuracy: 0.9814 - val_loss: 0.2908 - val_accuracy: 0.9786
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3814 - accuracy: 0.9761 - val_loss: 0.3426 - val_accuracy: 0.9733
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2378 - accuracy: 0.9832 - val_loss: 0.2963 - val_accuracy: 0.9733
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2861 - accuracy: 0.9796 - val_loss: 0.4008 - val_accuracy: 0.9786

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/35.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 16s 3s/step - loss: 0.9102 - accuracy: 0.9629

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9102233052253723, 0.962897539138794]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,697,506
    Non-trainable params: 261,632
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/36.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 128, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    9/9 [==============================] - 18s 2s/step - loss: 0.2699 - accuracy: 0.9796 - val_loss: 2.2297 - val_accuracy: 0.9251

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    9/9 [==============================] - 16s 2s/step - loss: 0.3302 - accuracy: 0.9761 - val_loss: 1.5647 - val_accuracy: 0.9465

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2261 - accuracy: 0.9858 - val_loss: 0.3624 - val_accuracy: 0.9893

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1150 - accuracy: 0.9911 - val_loss: 0.1703 - val_accuracy: 0.9840

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    9/9 [==============================] - 13s 1s/step - loss: 0.3056 - accuracy: 0.9805 - val_loss: 0.3012 - val_accuracy: 0.9786

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1659 - accuracy: 0.9867 - val_loss: 0.2329 - val_accuracy: 0.9733

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1795 - accuracy: 0.9885 - val_loss: 0.1692 - val_accuracy: 0.9840

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2332 - accuracy: 0.9858 - val_loss: 0.0858 - val_accuracy: 0.9947

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0991 - accuracy: 0.9858 - val_loss: 0.0794 - val_accuracy: 0.9947

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1519 - accuracy: 0.9867 - val_loss: 0.0458 - val_accuracy: 0.9947

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1937 - accuracy: 0.9894 - val_loss: 0.3190 - val_accuracy: 0.9893

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1660 - accuracy: 0.9823 - val_loss: 0.4034 - val_accuracy: 0.9786

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1739 - accuracy: 0.9840 - val_loss: 0.1034 - val_accuracy: 0.9947

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2347 - accuracy: 0.9858 - val_loss: 0.0090 - val_accuracy: 0.9947

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    9/9 [==============================] - 13s 1s/step - loss: 0.0624 - accuracy: 0.9929 - val_loss: 0.8311 - val_accuracy: 0.9786

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1824 - accuracy: 0.9858 - val_loss: 0.1289 - val_accuracy: 0.9947

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1139 - accuracy: 0.9885 - val_loss: 0.1177 - val_accuracy: 0.9893

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0516 - accuracy: 0.9920 - val_loss: 0.4733 - val_accuracy: 0.9840

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1727 - accuracy: 0.9920 - val_loss: 0.1256 - val_accuracy: 0.9947

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0475 - accuracy: 0.9911 - val_loss: 0.0149 - val_accuracy: 0.9947

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    9/9 [==============================] - 13s 1s/step - loss: 0.0629 - accuracy: 0.9956 - val_loss: 0.1443 - val_accuracy: 0.9893

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    9/9 [==============================] - 17s 2s/step - loss: 0.1298 - accuracy: 0.9911 - val_loss: 0.1019 - val_accuracy: 0.9947

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1221 - accuracy: 0.9929 - val_loss: 0.1175 - val_accuracy: 0.9947

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0753 - accuracy: 0.9920 - val_loss: 0.6949 - val_accuracy: 0.9572

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2528 - accuracy: 0.9849 - val_loss: 1.0672 - val_accuracy: 0.9733

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1321 - accuracy: 0.9902 - val_loss: 0.2647 - val_accuracy: 0.9786

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1302 - accuracy: 0.9911 - val_loss: 0.2071 - val_accuracy: 0.9840

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0929 - accuracy: 0.9920 - val_loss: 0.1691 - val_accuracy: 0.9893

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1236 - accuracy: 0.9938 - val_loss: 0.1441 - val_accuracy: 0.9893

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    9/9 [==============================] - 17s 2s/step - loss: 0.1521 - accuracy: 0.9920 - val_loss: 0.2327 - val_accuracy: 0.9893

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1028 - accuracy: 0.9894 - val_loss: 0.4417 - val_accuracy: 0.9733

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1089 - accuracy: 0.9911 - val_loss: 0.2039 - val_accuracy: 0.9733

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0526 - accuracy: 0.9956 - val_loss: 0.0873 - val_accuracy: 0.9893

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    9/9 [==============================] - 15s 2s/step - loss: 0.2373 - accuracy: 0.9840 - val_loss: 3.8826e-05 - val_accuracy: 1.0000

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1284 - accuracy: 0.9920 - val_loss: 0.0288 - val_accuracy: 0.9947

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1300 - accuracy: 0.9894 - val_loss: 0.0677 - val_accuracy: 0.9893

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0940 - accuracy: 0.9920 - val_loss: 0.0776 - val_accuracy: 0.9893

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0611 - accuracy: 0.9929 - val_loss: 0.1628 - val_accuracy: 0.9893

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0586 - accuracy: 0.9920 - val_loss: 0.1178 - val_accuracy: 0.9947

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0726 - accuracy: 0.9920 - val_loss: 0.1710 - val_accuracy: 0.9947

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    9/9 [==============================] - 12s 1s/step - loss: 0.1527 - accuracy: 0.9902 - val_loss: 0.3343 - val_accuracy: 0.9786

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0947 - accuracy: 0.9902 - val_loss: 0.4524 - val_accuracy: 0.9786

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0727 - accuracy: 0.9947 - val_loss: 0.3182 - val_accuracy: 0.9893

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0562 - accuracy: 0.9956 - val_loss: 0.1991 - val_accuracy: 0.9893

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0757 - accuracy: 0.9920 - val_loss: 0.0785 - val_accuracy: 0.9840

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0805 - accuracy: 0.9929 - val_loss: 0.0770 - val_accuracy: 0.9947

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0845 - accuracy: 0.9920 - val_loss: 0.0336 - val_accuracy: 0.9893

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0834 - accuracy: 0.9929 - val_loss: 0.0209 - val_accuracy: 0.9947

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0851 - accuracy: 0.9929 - val_loss: 0.2978 - val_accuracy: 0.9840

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0979 - accuracy: 0.9902 - val_loss: 0.4670 - val_accuracy: 0.9840

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/37.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 1.1885 - accuracy: 0.9647

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.188529372215271, 0.9646643400192261]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9646643109540636
                  precision    recall  f1-score   support

               0       0.99      0.88      0.94       164
               1       0.95      1.00      0.98       402

        accuracy                           0.96       566
       macro avg       0.97      0.94      0.96       566
    weighted avg       0.97      0.96      0.96       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/38.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/39.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/40.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.99      0.88      0.94       164
               1       0.95      1.00      0.98       402

        accuracy                           0.96       566
       macro avg       0.97      0.94      0.96       566
    weighted avg       0.97      0.96      0.96       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.99      0.88      1.00      0.94      0.94      0.87       164
              1       0.95      1.00      0.88      0.98      0.94      0.89       402

    avg / total       0.97      0.96      0.92      0.96      0.94      0.89       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
