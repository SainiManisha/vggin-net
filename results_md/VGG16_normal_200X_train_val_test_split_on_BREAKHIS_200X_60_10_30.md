<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/VGG16-NORMAL-RANDOMCROP_200X-BREAKHIS-Dataset-60-10-30'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_200X/train'
val_path = '../Splitted_200X/val'
test_path = '../Splitted_200X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.8910 - accuracy: 0.5156WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 36s 4s/step - loss: 30.6712 - accuracy: 0.5943 - val_loss: 0.6389 - val_accuracy: 0.7127
    Epoch 2/50
    9/9 [==============================] - 39s 4s/step - loss: 0.6422 - accuracy: 0.7121 - val_loss: 0.5900 - val_accuracy: 0.7127
    Epoch 3/50
    9/9 [==============================] - 22s 2s/step - loss: 0.5999 - accuracy: 0.7121 - val_loss: 0.6809 - val_accuracy: 0.7127
    Epoch 4/50
    9/9 [==============================] - 20s 2s/step - loss: 0.7112 - accuracy: 0.4388 - val_loss: 0.6946 - val_accuracy: 0.2873
    Epoch 5/50
    9/9 [==============================] - 18s 2s/step - loss: 0.6940 - accuracy: 0.2870 - val_loss: 0.6929 - val_accuracy: 0.7127
    Epoch 6/50
    9/9 [==============================] - 19s 2s/step - loss: 0.6918 - accuracy: 0.7121 - val_loss: 0.6467 - val_accuracy: 0.7127
    Epoch 7/50
    9/9 [==============================] - 20s 2s/step - loss: 0.5786 - accuracy: 0.7121 - val_loss: 0.5580 - val_accuracy: 0.7127
    Epoch 8/50
    9/9 [==============================] - 19s 2s/step - loss: 0.5186 - accuracy: 0.7121 - val_loss: 0.4550 - val_accuracy: 0.7127
    Epoch 9/50
    9/9 [==============================] - 19s 2s/step - loss: 0.4451 - accuracy: 0.7121 - val_loss: 0.4257 - val_accuracy: 0.7127
    Epoch 10/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4365 - accuracy: 0.7121 - val_loss: 0.3971 - val_accuracy: 0.7127
    Epoch 11/50
    9/9 [==============================] - 18s 2s/step - loss: 0.4185 - accuracy: 0.7121 - val_loss: 0.4033 - val_accuracy: 0.7127
    Epoch 12/50
    9/9 [==============================] - 19s 2s/step - loss: 0.4020 - accuracy: 0.7121 - val_loss: 0.3892 - val_accuracy: 0.7127
    Epoch 13/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3736 - accuracy: 0.7259 - val_loss: 0.3827 - val_accuracy: 0.8895
    Epoch 14/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3591 - accuracy: 0.8841 - val_loss: 0.3772 - val_accuracy: 0.8895
    Epoch 15/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3514 - accuracy: 0.8924 - val_loss: 0.3730 - val_accuracy: 0.8950
    Epoch 16/50
    9/9 [==============================] - 20s 2s/step - loss: 0.3375 - accuracy: 0.8924 - val_loss: 0.3966 - val_accuracy: 0.8840
    Epoch 17/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3561 - accuracy: 0.8832 - val_loss: 0.4064 - val_accuracy: 0.8177
    Epoch 18/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3328 - accuracy: 0.8804 - val_loss: 0.3634 - val_accuracy: 0.9006
    Epoch 19/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3338 - accuracy: 0.9062 - val_loss: 0.4619 - val_accuracy: 0.8453
    Epoch 20/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3581 - accuracy: 0.8878 - val_loss: 0.4113 - val_accuracy: 0.8564
    Epoch 21/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3542 - accuracy: 0.8638 - val_loss: 0.3789 - val_accuracy: 0.8785
    Epoch 22/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3611 - accuracy: 0.8749 - val_loss: 0.3191 - val_accuracy: 0.9006
    Epoch 23/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3551 - accuracy: 0.8758 - val_loss: 0.3245 - val_accuracy: 0.9006
    Epoch 24/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3130 - accuracy: 0.9016 - val_loss: 0.3246 - val_accuracy: 0.9006
    Epoch 25/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3106 - accuracy: 0.9154 - val_loss: 0.3125 - val_accuracy: 0.9061
    Epoch 26/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2914 - accuracy: 0.9016 - val_loss: 0.3149 - val_accuracy: 0.9116
    Epoch 27/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3059 - accuracy: 0.9126 - val_loss: 0.3152 - val_accuracy: 0.8950
    Epoch 28/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3196 - accuracy: 0.9025 - val_loss: 0.3611 - val_accuracy: 0.8453
    Epoch 29/50
    9/9 [==============================] - 18s 2s/step - loss: 0.3127 - accuracy: 0.8970 - val_loss: 0.3019 - val_accuracy: 0.9171
    Epoch 30/50
    9/9 [==============================] - 19s 2s/step - loss: 0.4573 - accuracy: 0.8519 - val_loss: 0.2920 - val_accuracy: 0.9116
    Epoch 31/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3455 - accuracy: 0.8730 - val_loss: 0.3464 - val_accuracy: 0.9006
    Epoch 32/50
    9/9 [==============================] - 15s 2s/step - loss: 0.3148 - accuracy: 0.8970 - val_loss: 0.2930 - val_accuracy: 0.9061
    Epoch 33/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2958 - accuracy: 0.9025 - val_loss: 0.2958 - val_accuracy: 0.9171
    Epoch 34/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2957 - accuracy: 0.9062 - val_loss: 0.2976 - val_accuracy: 0.9006
    Epoch 35/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2910 - accuracy: 0.9080 - val_loss: 0.2926 - val_accuracy: 0.9116
    Epoch 36/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2805 - accuracy: 0.9273 - val_loss: 0.3029 - val_accuracy: 0.9116
    Epoch 37/50
    9/9 [==============================] - 20s 2s/step - loss: 0.2879 - accuracy: 0.9209 - val_loss: 0.2884 - val_accuracy: 0.9116
    Epoch 38/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2881 - accuracy: 0.9108 - val_loss: 0.2875 - val_accuracy: 0.9061
    Epoch 39/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2650 - accuracy: 0.9163 - val_loss: 0.2931 - val_accuracy: 0.9116
    Epoch 40/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2993 - accuracy: 0.9052 - val_loss: 0.2984 - val_accuracy: 0.9116
    Epoch 41/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2600 - accuracy: 0.9218 - val_loss: 0.2859 - val_accuracy: 0.9116
    Epoch 42/50
    9/9 [==============================] - 20s 2s/step - loss: 0.2598 - accuracy: 0.9255 - val_loss: 0.2815 - val_accuracy: 0.9171
    Epoch 43/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2705 - accuracy: 0.9301 - val_loss: 0.2818 - val_accuracy: 0.9282
    Epoch 44/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2740 - accuracy: 0.9218 - val_loss: 0.2807 - val_accuracy: 0.9282
    Epoch 45/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2747 - accuracy: 0.9154 - val_loss: 0.2889 - val_accuracy: 0.9227
    Epoch 46/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2587 - accuracy: 0.9328 - val_loss: 0.2809 - val_accuracy: 0.9171
    Epoch 47/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2557 - accuracy: 0.9227 - val_loss: 0.2845 - val_accuracy: 0.9171
    Epoch 48/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2500 - accuracy: 0.9301 - val_loss: 0.2776 - val_accuracy: 0.9282
    Epoch 49/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2644 - accuracy: 0.9273 - val_loss: 0.3204 - val_accuracy: 0.9282
    Epoch 50/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2671 - accuracy: 0.9154 - val_loss: 0.2911 - val_accuracy: 0.9061

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/245.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 34s 7s/step - loss: 0.2886 - accuracy: 0.9119

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.2885512709617615, 0.9119266271591187]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9119266055045872
                  precision    recall  f1-score   support

               0       0.82      0.90      0.86       158
               1       0.96      0.92      0.94       387

        accuracy                           0.91       545
       macro avg       0.89      0.91      0.90       545
    weighted avg       0.92      0.91      0.91       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: numpy&gt;=1.11.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/246.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/247.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/248.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.82      0.90      0.86       158
               1       0.96      0.92      0.94       387

        accuracy                           0.91       545
       macro avg       0.89      0.91      0.90       545
    weighted avg       0.92      0.91      0.91       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.82      0.90      0.92      0.86      0.91      0.82       158
              1       0.96      0.92      0.90      0.94      0.91      0.83       387

    avg / total       0.92      0.91      0.90      0.91      0.91      0.83       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
