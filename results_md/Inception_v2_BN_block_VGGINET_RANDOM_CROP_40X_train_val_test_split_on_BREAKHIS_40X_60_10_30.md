<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate, BatchNormalization, Activation
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same')(layer_in)
    conv1 = BatchNormalization()(conv1)
    conv1 = Activation('relu')(conv1)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same')(layer_in)
    conv3 = BatchNormalization()(conv3)
    conv3 = Activation('relu')(conv3)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same')(layer_in)
    conv5 = BatchNormalization()(conv5)
    conv5 = Activation('relu')(conv5)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


# bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(out)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    batch_normalization (BatchNorma (None, 14, 14, 64)   256         conv2d[0][0]                     
    __________________________________________________________________________________________________
    batch_normalization_1 (BatchNor (None, 14, 14, 128)  512         conv2d_1[0][0]                   
    __________________________________________________________________________________________________
    batch_normalization_2 (BatchNor (None, 14, 14, 32)   128         conv2d_2[0][0]                   
    __________________________________________________________________________________________________
    activation (Activation)         (None, 14, 14, 64)   0           batch_normalization[0][0]        
    __________________________________________________________________________________________________
    activation_1 (Activation)       (None, 14, 14, 128)  0           batch_normalization_1[0][0]      
    __________________________________________________________________________________________________
    activation_2 (Activation)       (None, 14, 14, 32)   0           batch_normalization_2[0][0]      
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           activation[0][0]                 
                                                                     activation_1[0][0]               
                                                                     activation_2[0][0]               
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           concatenate[0][0]                
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,957,090
    Trainable params: 1,321,378
    Non-trainable params: 7,635,712
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'Inception_v2-BN-block-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    mkdir: cannot create directory ‘./experiments/Inception_v2-BN-block-BREAKHIS-Dataset-60-10-30-VGGINet’: File exists

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 106.7330 - accuracy: 0.5156WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 17s 2s/step - loss: 2019.9032 - accuracy: 0.6342 - val_loss: 1843.1438 - val_accuracy: 0.7095
    Epoch 2/100
    9/9 [==============================] - 11s 1s/step - loss: 900.7866 - accuracy: 0.6917 - val_loss: 299.2730 - val_accuracy: 0.8212
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 298.9424 - accuracy: 0.8254 - val_loss: 202.2010 - val_accuracy: 0.8547
    Epoch 4/100
    9/9 [==============================] - 13s 1s/step - loss: 280.8295 - accuracy: 0.8282 - val_loss: 231.5196 - val_accuracy: 0.7821
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 246.9943 - accuracy: 0.8375 - val_loss: 138.0205 - val_accuracy: 0.8603
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 185.9014 - accuracy: 0.8579 - val_loss: 161.1984 - val_accuracy: 0.8715
    Epoch 7/100
    9/9 [==============================] - 13s 1s/step - loss: 97.5985 - accuracy: 0.9034 - val_loss: 139.4467 - val_accuracy: 0.8939
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 89.6187 - accuracy: 0.8932 - val_loss: 110.7818 - val_accuracy: 0.8939
    Epoch 9/100
    9/9 [==============================] - 9s 1s/step - loss: 70.4577 - accuracy: 0.9118 - val_loss: 90.6558 - val_accuracy: 0.9106
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 71.8887 - accuracy: 0.9034 - val_loss: 112.4102 - val_accuracy: 0.9106
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 124.1684 - accuracy: 0.8579 - val_loss: 147.0210 - val_accuracy: 0.8883
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 131.3823 - accuracy: 0.8700 - val_loss: 305.2246 - val_accuracy: 0.7151
    Epoch 13/100
    9/9 [==============================] - 13s 1s/step - loss: 170.2551 - accuracy: 0.8347 - val_loss: 181.6909 - val_accuracy: 0.8771
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 101.2627 - accuracy: 0.8951 - val_loss: 111.8568 - val_accuracy: 0.9162
    Epoch 15/100
    9/9 [==============================] - 10s 1s/step - loss: 144.6009 - accuracy: 0.8830 - val_loss: 64.7179 - val_accuracy: 0.9274
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 100.1464 - accuracy: 0.8951 - val_loss: 276.5392 - val_accuracy: 0.8603
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 141.9010 - accuracy: 0.8821 - val_loss: 202.3004 - val_accuracy: 0.8380
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 104.5004 - accuracy: 0.8839 - val_loss: 328.2416 - val_accuracy: 0.8715
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 179.6639 - accuracy: 0.8570 - val_loss: 75.0895 - val_accuracy: 0.9162
    Epoch 20/100
    9/9 [==============================] - 13s 1s/step - loss: 99.7041 - accuracy: 0.9053 - val_loss: 93.6788 - val_accuracy: 0.9330
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 66.7735 - accuracy: 0.9285 - val_loss: 103.8910 - val_accuracy: 0.9441
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 62.3220 - accuracy: 0.9322 - val_loss: 109.6642 - val_accuracy: 0.9106
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 54.5928 - accuracy: 0.9322 - val_loss: 169.4097 - val_accuracy: 0.8547
    Epoch 24/100
    9/9 [==============================] - 13s 1s/step - loss: 65.7122 - accuracy: 0.9285 - val_loss: 75.1703 - val_accuracy: 0.9330
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 52.2882 - accuracy: 0.9257 - val_loss: 70.2343 - val_accuracy: 0.9274
    Epoch 26/100
    9/9 [==============================] - 13s 1s/step - loss: 52.8711 - accuracy: 0.9276 - val_loss: 229.6394 - val_accuracy: 0.8603
    Epoch 27/100
    9/9 [==============================] - 13s 1s/step - loss: 246.6844 - accuracy: 0.8468 - val_loss: 78.0540 - val_accuracy: 0.9274
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 232.4799 - accuracy: 0.8561 - val_loss: 154.3884 - val_accuracy: 0.8715
    Epoch 29/100
    9/9 [==============================] - 13s 1s/step - loss: 108.0135 - accuracy: 0.9276 - val_loss: 90.2107 - val_accuracy: 0.8939
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 132.1689 - accuracy: 0.9053 - val_loss: 222.5896 - val_accuracy: 0.8994
    Epoch 31/100
    9/9 [==============================] - 9s 1s/step - loss: 108.6145 - accuracy: 0.9146 - val_loss: 265.2508 - val_accuracy: 0.8715
    Epoch 32/100
    9/9 [==============================] - 13s 1s/step - loss: 115.3784 - accuracy: 0.9192 - val_loss: 261.4218 - val_accuracy: 0.8715
    Epoch 33/100
    9/9 [==============================] - 13s 1s/step - loss: 109.9078 - accuracy: 0.9127 - val_loss: 80.6344 - val_accuracy: 0.9106
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 101.3229 - accuracy: 0.9183 - val_loss: 157.6893 - val_accuracy: 0.8659
    Epoch 35/100
    9/9 [==============================] - 13s 1s/step - loss: 104.7054 - accuracy: 0.9155 - val_loss: 129.1070 - val_accuracy: 0.9441
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 86.8597 - accuracy: 0.9322 - val_loss: 203.8571 - val_accuracy: 0.9162
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 101.2921 - accuracy: 0.9155 - val_loss: 181.0219 - val_accuracy: 0.9274
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 73.6490 - accuracy: 0.9369 - val_loss: 88.0988 - val_accuracy: 0.9441
    Epoch 39/100
    9/9 [==============================] - 13s 1s/step - loss: 61.5673 - accuracy: 0.9313 - val_loss: 269.2286 - val_accuracy: 0.8883
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 114.8357 - accuracy: 0.9146 - val_loss: 631.2856 - val_accuracy: 0.8212
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 127.6689 - accuracy: 0.9229 - val_loss: 129.1946 - val_accuracy: 0.9330
    Epoch 42/100
    9/9 [==============================] - 9s 1s/step - loss: 56.8801 - accuracy: 0.9526 - val_loss: 86.3124 - val_accuracy: 0.9330
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 65.4974 - accuracy: 0.9415 - val_loss: 81.3148 - val_accuracy: 0.9441
    Epoch 44/100
    9/9 [==============================] - 17s 2s/step - loss: 49.3792 - accuracy: 0.9480 - val_loss: 116.8211 - val_accuracy: 0.9330
    Epoch 45/100
    9/9 [==============================] - 13s 1s/step - loss: 53.8926 - accuracy: 0.9471 - val_loss: 125.2511 - val_accuracy: 0.9385
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 36.5527 - accuracy: 0.9545 - val_loss: 122.9905 - val_accuracy: 0.9385
    Epoch 47/100
    9/9 [==============================] - 13s 1s/step - loss: 39.7778 - accuracy: 0.9564 - val_loss: 110.1131 - val_accuracy: 0.9385
    Epoch 48/100
    9/9 [==============================] - 13s 1s/step - loss: 51.7431 - accuracy: 0.9443 - val_loss: 76.5670 - val_accuracy: 0.9330
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 47.4984 - accuracy: 0.9508 - val_loss: 203.7699 - val_accuracy: 0.8268
    Epoch 50/100
    9/9 [==============================] - 13s 1s/step - loss: 67.7482 - accuracy: 0.9322 - val_loss: 70.6361 - val_accuracy: 0.9218
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 49.4593 - accuracy: 0.9396 - val_loss: 90.4527 - val_accuracy: 0.9441
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 53.7710 - accuracy: 0.9471 - val_loss: 71.7089 - val_accuracy: 0.9497
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 37.5154 - accuracy: 0.9629 - val_loss: 76.2054 - val_accuracy: 0.9609
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 30.2694 - accuracy: 0.9610 - val_loss: 169.1335 - val_accuracy: 0.9274
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 67.9644 - accuracy: 0.9434 - val_loss: 320.5690 - val_accuracy: 0.8771
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 54.7258 - accuracy: 0.9461 - val_loss: 85.7116 - val_accuracy: 0.9553
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 32.6630 - accuracy: 0.9666 - val_loss: 71.1116 - val_accuracy: 0.9665
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 37.3566 - accuracy: 0.9629 - val_loss: 90.8080 - val_accuracy: 0.9441
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 30.9672 - accuracy: 0.9554 - val_loss: 46.8049 - val_accuracy: 0.9553
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 36.9424 - accuracy: 0.9703 - val_loss: 98.5109 - val_accuracy: 0.9330
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 36.1562 - accuracy: 0.9582 - val_loss: 71.4976 - val_accuracy: 0.9441
    Epoch 62/100
    9/9 [==============================] - 13s 1s/step - loss: 75.3923 - accuracy: 0.9396 - val_loss: 91.5399 - val_accuracy: 0.9497
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 83.9936 - accuracy: 0.9369 - val_loss: 151.4205 - val_accuracy: 0.9274
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 62.4426 - accuracy: 0.9517 - val_loss: 70.1396 - val_accuracy: 0.9609
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 49.7995 - accuracy: 0.9573 - val_loss: 122.3576 - val_accuracy: 0.9385
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 54.9310 - accuracy: 0.9499 - val_loss: 264.7597 - val_accuracy: 0.8994
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 54.7227 - accuracy: 0.9452 - val_loss: 75.3170 - val_accuracy: 0.9385
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 69.0798 - accuracy: 0.9406 - val_loss: 184.5464 - val_accuracy: 0.8939
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 50.9830 - accuracy: 0.9499 - val_loss: 109.8747 - val_accuracy: 0.9106
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 69.9014 - accuracy: 0.9452 - val_loss: 82.4948 - val_accuracy: 0.9497
    Epoch 71/100
    9/9 [==============================] - 13s 1s/step - loss: 53.7149 - accuracy: 0.9526 - val_loss: 169.1384 - val_accuracy: 0.9274
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 60.4845 - accuracy: 0.9480 - val_loss: 59.8946 - val_accuracy: 0.9665
    Epoch 73/100
    9/9 [==============================] - 13s 1s/step - loss: 41.3764 - accuracy: 0.9610 - val_loss: 76.3488 - val_accuracy: 0.9385
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 49.4024 - accuracy: 0.9564 - val_loss: 235.5702 - val_accuracy: 0.9106
    Epoch 75/100
    9/9 [==============================] - 13s 1s/step - loss: 57.8666 - accuracy: 0.9545 - val_loss: 159.5540 - val_accuracy: 0.9330
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 31.3858 - accuracy: 0.9684 - val_loss: 83.2745 - val_accuracy: 0.9553
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 36.4922 - accuracy: 0.9610 - val_loss: 119.1431 - val_accuracy: 0.8939
    Epoch 78/100
    9/9 [==============================] - 9s 1s/step - loss: 47.3688 - accuracy: 0.9517 - val_loss: 78.0000 - val_accuracy: 0.9553
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 77.4411 - accuracy: 0.9369 - val_loss: 192.5053 - val_accuracy: 0.9218
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 77.6739 - accuracy: 0.9378 - val_loss: 129.4467 - val_accuracy: 0.9441
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 83.2877 - accuracy: 0.9387 - val_loss: 68.2425 - val_accuracy: 0.9218
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 26.7261 - accuracy: 0.9703 - val_loss: 112.6992 - val_accuracy: 0.9497
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 33.3438 - accuracy: 0.9656 - val_loss: 147.7975 - val_accuracy: 0.9441
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 32.8196 - accuracy: 0.9619 - val_loss: 159.3693 - val_accuracy: 0.9385
    Epoch 85/100
    9/9 [==============================] - 13s 1s/step - loss: 48.9334 - accuracy: 0.9638 - val_loss: 58.1750 - val_accuracy: 0.9609
    Epoch 86/100
    9/9 [==============================] - 9s 1s/step - loss: 47.6401 - accuracy: 0.9619 - val_loss: 65.1123 - val_accuracy: 0.9441
    Epoch 87/100
    9/9 [==============================] - 13s 1s/step - loss: 40.5136 - accuracy: 0.9638 - val_loss: 101.7101 - val_accuracy: 0.9497
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 28.2834 - accuracy: 0.9666 - val_loss: 73.3867 - val_accuracy: 0.9553
    Epoch 89/100
    9/9 [==============================] - 13s 1s/step - loss: 38.6020 - accuracy: 0.9712 - val_loss: 91.8675 - val_accuracy: 0.9609
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 30.0265 - accuracy: 0.9712 - val_loss: 100.8182 - val_accuracy: 0.9441
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 49.8973 - accuracy: 0.9591 - val_loss: 95.6327 - val_accuracy: 0.9497
    Epoch 92/100
    9/9 [==============================] - 9s 1s/step - loss: 34.8997 - accuracy: 0.9656 - val_loss: 92.7307 - val_accuracy: 0.9218
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 49.4539 - accuracy: 0.9591 - val_loss: 80.8745 - val_accuracy: 0.9497
    Epoch 94/100
    9/9 [==============================] - 13s 1s/step - loss: 54.8317 - accuracy: 0.9461 - val_loss: 121.6746 - val_accuracy: 0.9441
    Epoch 95/100
    9/9 [==============================] - 13s 1s/step - loss: 50.3055 - accuracy: 0.9582 - val_loss: 118.4586 - val_accuracy: 0.9385
    Epoch 96/100
    9/9 [==============================] - 13s 1s/step - loss: 39.2612 - accuracy: 0.9573 - val_loss: 279.0587 - val_accuracy: 0.8827
    Epoch 97/100
    9/9 [==============================] - 13s 1s/step - loss: 46.8092 - accuracy: 0.9591 - val_loss: 105.5327 - val_accuracy: 0.9553
    Epoch 98/100
    9/9 [==============================] - 13s 1s/step - loss: 20.3333 - accuracy: 0.9786 - val_loss: 116.3242 - val_accuracy: 0.9497
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 35.8575 - accuracy: 0.9610 - val_loss: 99.8237 - val_accuracy: 0.9497
    Epoch 100/100
    9/9 [==============================] - 9s 1s/step - loss: 66.9791 - accuracy: 0.9508 - val_loss: 77.6292 - val_accuracy: 0.9497
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/55.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 76.9372 - accuracy: 0.9703

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [76.93721008300781, 0.9703153967857361]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9703153988868275
                  precision    recall  f1-score   support

               0       0.94      0.96      0.95       158
               1       0.98      0.98      0.98       381

        accuracy                           0.97       539
       macro avg       0.96      0.97      0.96       539
    weighted avg       0.97      0.97      0.97       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/56.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/57.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/58.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.94      0.96      0.95       158
               1       0.98      0.98      0.98       381

        accuracy                           0.97       539
       macro avg       0.96      0.97      0.96       539
    weighted avg       0.97      0.97      0.97       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.94      0.96      0.98      0.95      0.97      0.93       158
              1       0.98      0.98      0.96      0.98      0.97      0.94       381

    avg / total       0.97      0.97      0.96      0.97      0.97      0.93       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
