<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 64
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 images belonging to 2 classes.
    Found 161 images belonging to 2 classes.
    Found 488 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.over_sampling import RandomOverSampler
from tensorflow.keras.utils import to_categorical
ros = RandomOverSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((1358, 224, 340, 3), (1358, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_val.shape, y_val.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[17\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((161, 224, 340, 3), (161, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_test.shape, y_test.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((488, 224, 340, 3), (488, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(100).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
     1/22 [&gt;.............................] - ETA: 0s - loss: 1.1980 - accuracy: 0.6094WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    22/22 [==============================] - 21s 965ms/step - loss: 2.7764 - accuracy: 0.7857 - val_loss: 64.1894 - val_accuracy: 0.3106
    Epoch 2/100
    22/22 [==============================] - 21s 957ms/step - loss: 12.4851 - accuracy: 0.6915 - val_loss: 90.4034 - val_accuracy: 0.3230
    Epoch 3/100
    22/22 [==============================] - 21s 949ms/step - loss: 28.8576 - accuracy: 0.5471 - val_loss: 4.4327 - val_accuracy: 0.9130
    Epoch 4/100
    22/22 [==============================] - 21s 964ms/step - loss: 21.0886 - accuracy: 0.5788 - val_loss: 2.9269 - val_accuracy: 0.9193
    Epoch 5/100
    22/22 [==============================] - 21s 939ms/step - loss: 10.4974 - accuracy: 0.6811 - val_loss: 8.3184 - val_accuracy: 0.7143
    Epoch 6/100
    22/22 [==============================] - 21s 949ms/step - loss: 7.5954 - accuracy: 0.7246 - val_loss: 4.9597 - val_accuracy: 0.7702
    Epoch 7/100
    22/22 [==============================] - 21s 932ms/step - loss: 6.8553 - accuracy: 0.7165 - val_loss: 3.6491 - val_accuracy: 0.8012
    Epoch 8/100
    22/22 [==============================] - 21s 939ms/step - loss: 8.1040 - accuracy: 0.6789 - val_loss: 2.5278 - val_accuracy: 0.8261
    Epoch 9/100
    22/22 [==============================] - 21s 939ms/step - loss: 6.5277 - accuracy: 0.7172 - val_loss: 1.5483 - val_accuracy: 0.8882
    Epoch 10/100
    22/22 [==============================] - 20s 930ms/step - loss: 5.3589 - accuracy: 0.7680 - val_loss: 1.5905 - val_accuracy: 0.9068
    Epoch 11/100
    22/22 [==============================] - 23s 1s/step - loss: 3.9521 - accuracy: 0.7835 - val_loss: 2.7281 - val_accuracy: 0.8137
    Epoch 12/100
    22/22 [==============================] - 20s 926ms/step - loss: 4.7721 - accuracy: 0.7599 - val_loss: 2.4213 - val_accuracy: 0.8137
    Epoch 13/100
    22/22 [==============================] - 22s 1s/step - loss: 4.6945 - accuracy: 0.7754 - val_loss: 3.8907 - val_accuracy: 0.7826
    Epoch 14/100
    22/22 [==============================] - 21s 943ms/step - loss: 4.6015 - accuracy: 0.7865 - val_loss: 1.7180 - val_accuracy: 0.8820
    Epoch 15/100
    22/22 [==============================] - 22s 977ms/step - loss: 3.6591 - accuracy: 0.7923 - val_loss: 3.2595 - val_accuracy: 0.7950
    Epoch 16/100
    22/22 [==============================] - 23s 1s/step - loss: 3.4320 - accuracy: 0.8122 - val_loss: 1.1458 - val_accuracy: 0.9068
    Epoch 17/100
    22/22 [==============================] - 21s 968ms/step - loss: 3.1327 - accuracy: 0.8292 - val_loss: 1.1785 - val_accuracy: 0.9006
    Epoch 18/100
    22/22 [==============================] - 21s 973ms/step - loss: 2.8386 - accuracy: 0.8409 - val_loss: 1.2396 - val_accuracy: 0.9193
    Epoch 19/100
    22/22 [==============================] - 21s 946ms/step - loss: 3.0278 - accuracy: 0.8365 - val_loss: 1.4334 - val_accuracy: 0.8571
    Epoch 20/100
    22/22 [==============================] - 21s 949ms/step - loss: 2.8091 - accuracy: 0.8321 - val_loss: 1.1533 - val_accuracy: 0.8944
    Epoch 21/100
    22/22 [==============================] - 21s 941ms/step - loss: 2.9826 - accuracy: 0.8255 - val_loss: 1.8417 - val_accuracy: 0.8509
    Epoch 22/100
    22/22 [==============================] - 21s 944ms/step - loss: 2.5422 - accuracy: 0.8520 - val_loss: 1.7032 - val_accuracy: 0.8882
    Epoch 23/100
    22/22 [==============================] - 21s 968ms/step - loss: 2.1019 - accuracy: 0.8652 - val_loss: 2.7630 - val_accuracy: 0.8137
    Epoch 24/100
    22/22 [==============================] - 21s 946ms/step - loss: 2.4849 - accuracy: 0.8579 - val_loss: 1.6596 - val_accuracy: 0.8509
    Epoch 25/100
    22/22 [==============================] - 21s 954ms/step - loss: 2.3773 - accuracy: 0.8741 - val_loss: 2.2177 - val_accuracy: 0.8571
    Epoch 26/100
    22/22 [==============================] - 21s 935ms/step - loss: 1.8918 - accuracy: 0.8778 - val_loss: 2.5724 - val_accuracy: 0.8696
    Epoch 27/100
    22/22 [==============================] - 23s 1s/step - loss: 2.7223 - accuracy: 0.8594 - val_loss: 2.6037 - val_accuracy: 0.8447
    Epoch 28/100
    22/22 [==============================] - 20s 930ms/step - loss: 2.5064 - accuracy: 0.8697 - val_loss: 2.6192 - val_accuracy: 0.8509
    Epoch 29/100
    22/22 [==============================] - 21s 957ms/step - loss: 2.0287 - accuracy: 0.8741 - val_loss: 2.0325 - val_accuracy: 0.8944
    Epoch 30/100
    22/22 [==============================] - 21s 974ms/step - loss: 1.9478 - accuracy: 0.8837 - val_loss: 1.4532 - val_accuracy: 0.8944
    Epoch 31/100
    22/22 [==============================] - 21s 961ms/step - loss: 2.4116 - accuracy: 0.8667 - val_loss: 3.1954 - val_accuracy: 0.8199
    Epoch 32/100
    22/22 [==============================] - 21s 961ms/step - loss: 2.1489 - accuracy: 0.8829 - val_loss: 2.0675 - val_accuracy: 0.8261
    Epoch 33/100
    22/22 [==============================] - 21s 951ms/step - loss: 1.9037 - accuracy: 0.8667 - val_loss: 2.2632 - val_accuracy: 0.8820
    Epoch 34/100
    22/22 [==============================] - 21s 951ms/step - loss: 3.0960 - accuracy: 0.8343 - val_loss: 1.3591 - val_accuracy: 0.9006
    Epoch 35/100
    22/22 [==============================] - 20s 924ms/step - loss: 2.3858 - accuracy: 0.8719 - val_loss: 1.8097 - val_accuracy: 0.9006
    Epoch 36/100
    22/22 [==============================] - 21s 947ms/step - loss: 1.8973 - accuracy: 0.8918 - val_loss: 1.3909 - val_accuracy: 0.9130
    Epoch 37/100
    22/22 [==============================] - 21s 958ms/step - loss: 1.9443 - accuracy: 0.9013 - val_loss: 2.8835 - val_accuracy: 0.8199
    Epoch 38/100
    22/22 [==============================] - 22s 1s/step - loss: 2.2502 - accuracy: 0.8881 - val_loss: 2.4129 - val_accuracy: 0.8447
    Epoch 39/100
    22/22 [==============================] - 21s 960ms/step - loss: 2.3430 - accuracy: 0.8675 - val_loss: 1.4684 - val_accuracy: 0.9006
    Epoch 40/100
    22/22 [==============================] - 21s 958ms/step - loss: 1.9935 - accuracy: 0.8954 - val_loss: 1.9085 - val_accuracy: 0.9255
    Epoch 41/100
    22/22 [==============================] - 21s 960ms/step - loss: 1.3282 - accuracy: 0.9146 - val_loss: 3.9865 - val_accuracy: 0.8447
    Epoch 42/100
    22/22 [==============================] - 21s 955ms/step - loss: 1.6405 - accuracy: 0.9094 - val_loss: 1.7348 - val_accuracy: 0.8944
    Epoch 43/100
    22/22 [==============================] - 21s 946ms/step - loss: 1.5360 - accuracy: 0.9124 - val_loss: 2.0228 - val_accuracy: 0.9130
    Epoch 44/100
    22/22 [==============================] - 21s 944ms/step - loss: 1.4274 - accuracy: 0.9138 - val_loss: 2.3519 - val_accuracy: 0.8944
    Epoch 45/100
    22/22 [==============================] - 21s 950ms/step - loss: 1.9692 - accuracy: 0.8910 - val_loss: 3.1155 - val_accuracy: 0.8571
    Epoch 46/100
    22/22 [==============================] - 21s 943ms/step - loss: 1.5203 - accuracy: 0.9072 - val_loss: 1.5680 - val_accuracy: 0.8882
    Epoch 47/100
    22/22 [==============================] - 21s 945ms/step - loss: 1.4550 - accuracy: 0.9168 - val_loss: 1.7157 - val_accuracy: 0.9068
    Epoch 48/100
    22/22 [==============================] - 21s 938ms/step - loss: 1.3502 - accuracy: 0.9227 - val_loss: 2.0195 - val_accuracy: 0.8882
    Epoch 49/100
    22/22 [==============================] - 21s 970ms/step - loss: 1.1725 - accuracy: 0.9286 - val_loss: 2.2096 - val_accuracy: 0.9006
    Epoch 50/100
    22/22 [==============================] - 21s 939ms/step - loss: 1.3422 - accuracy: 0.9197 - val_loss: 2.9713 - val_accuracy: 0.8696
    Epoch 51/100
    22/22 [==============================] - 21s 966ms/step - loss: 1.1714 - accuracy: 0.9183 - val_loss: 2.6779 - val_accuracy: 0.8820
    Epoch 52/100
    22/22 [==============================] - 22s 1s/step - loss: 1.0872 - accuracy: 0.9286 - val_loss: 2.6125 - val_accuracy: 0.8882
    Epoch 53/100
    22/22 [==============================] - 23s 1s/step - loss: 0.9541 - accuracy: 0.9529 - val_loss: 1.8254 - val_accuracy: 0.9068
    Epoch 54/100
    22/22 [==============================] - 21s 949ms/step - loss: 1.0139 - accuracy: 0.9396 - val_loss: 2.6778 - val_accuracy: 0.8944
    Epoch 55/100
    22/22 [==============================] - 21s 969ms/step - loss: 0.9923 - accuracy: 0.9315 - val_loss: 2.4971 - val_accuracy: 0.8820
    Epoch 56/100
    22/22 [==============================] - 21s 965ms/step - loss: 1.0586 - accuracy: 0.9242 - val_loss: 1.5804 - val_accuracy: 0.9130
    Epoch 57/100
    22/22 [==============================] - 21s 959ms/step - loss: 1.0208 - accuracy: 0.9529 - val_loss: 2.8675 - val_accuracy: 0.8820
    Epoch 58/100
    22/22 [==============================] - 23s 1s/step - loss: 1.2592 - accuracy: 0.9293 - val_loss: 3.4047 - val_accuracy: 0.8820
    Epoch 59/100
    22/22 [==============================] - 23s 1s/step - loss: 1.0699 - accuracy: 0.9374 - val_loss: 3.1931 - val_accuracy: 0.8820
    Epoch 60/100
    22/22 [==============================] - 21s 955ms/step - loss: 1.0234 - accuracy: 0.9389 - val_loss: 2.1828 - val_accuracy: 0.9068
    Epoch 61/100
    22/22 [==============================] - 21s 963ms/step - loss: 1.4106 - accuracy: 0.9124 - val_loss: 2.1689 - val_accuracy: 0.8820
    Epoch 62/100
    22/22 [==============================] - 21s 945ms/step - loss: 1.6586 - accuracy: 0.9116 - val_loss: 2.4626 - val_accuracy: 0.9068
    Epoch 63/100
    22/22 [==============================] - 21s 955ms/step - loss: 1.4581 - accuracy: 0.9227 - val_loss: 2.2849 - val_accuracy: 0.8882
    Epoch 64/100
    22/22 [==============================] - 21s 967ms/step - loss: 1.2467 - accuracy: 0.9278 - val_loss: 2.7401 - val_accuracy: 0.8882
    Epoch 65/100
    22/22 [==============================] - 21s 976ms/step - loss: 0.9795 - accuracy: 0.9455 - val_loss: 3.2926 - val_accuracy: 0.9068
    Epoch 66/100
    22/22 [==============================] - 22s 1s/step - loss: 0.6995 - accuracy: 0.9602 - val_loss: 2.8048 - val_accuracy: 0.9130
    Epoch 67/100
    22/22 [==============================] - 21s 942ms/step - loss: 0.7521 - accuracy: 0.9492 - val_loss: 3.5883 - val_accuracy: 0.8882
    Epoch 68/100
    22/22 [==============================] - 23s 1s/step - loss: 0.6562 - accuracy: 0.9536 - val_loss: 2.6537 - val_accuracy: 0.8820
    Epoch 69/100
    22/22 [==============================] - 20s 921ms/step - loss: 0.7027 - accuracy: 0.9514 - val_loss: 2.4198 - val_accuracy: 0.9317
    Epoch 70/100
    22/22 [==============================] - 23s 1s/step - loss: 0.8277 - accuracy: 0.9507 - val_loss: 3.3942 - val_accuracy: 0.9006
    Epoch 71/100
    22/22 [==============================] - 20s 929ms/step - loss: 1.2132 - accuracy: 0.9278 - val_loss: 4.0861 - val_accuracy: 0.8571
    Epoch 72/100
    22/22 [==============================] - 21s 945ms/step - loss: 1.1274 - accuracy: 0.9308 - val_loss: 2.7956 - val_accuracy: 0.8944
    Epoch 73/100
    22/22 [==============================] - 21s 950ms/step - loss: 1.0402 - accuracy: 0.9418 - val_loss: 3.5098 - val_accuracy: 0.9068
    Epoch 74/100
    22/22 [==============================] - 21s 964ms/step - loss: 0.8736 - accuracy: 0.9514 - val_loss: 4.1320 - val_accuracy: 0.8820
    Epoch 75/100
    22/22 [==============================] - 21s 942ms/step - loss: 0.9183 - accuracy: 0.9396 - val_loss: 3.4171 - val_accuracy: 0.8882
    Epoch 76/100
    22/22 [==============================] - 21s 941ms/step - loss: 0.8784 - accuracy: 0.9499 - val_loss: 3.7606 - val_accuracy: 0.8758
    Epoch 77/100
    22/22 [==============================] - 21s 946ms/step - loss: 0.7115 - accuracy: 0.9536 - val_loss: 2.9703 - val_accuracy: 0.9006
    Epoch 78/100
    22/22 [==============================] - 21s 941ms/step - loss: 0.7057 - accuracy: 0.9529 - val_loss: 3.3775 - val_accuracy: 0.9193
    Epoch 79/100
    22/22 [==============================] - 21s 939ms/step - loss: 0.6064 - accuracy: 0.9602 - val_loss: 2.7851 - val_accuracy: 0.9193
    Epoch 80/100
    22/22 [==============================] - 21s 965ms/step - loss: 0.6783 - accuracy: 0.9602 - val_loss: 2.7574 - val_accuracy: 0.9068
    Epoch 81/100
    22/22 [==============================] - 21s 977ms/step - loss: 0.5557 - accuracy: 0.9654 - val_loss: 2.9154 - val_accuracy: 0.9130
    Epoch 82/100
    22/22 [==============================] - 21s 969ms/step - loss: 0.8797 - accuracy: 0.9529 - val_loss: 3.0194 - val_accuracy: 0.9006
    Epoch 83/100
    22/22 [==============================] - 21s 944ms/step - loss: 0.7864 - accuracy: 0.9566 - val_loss: 2.3601 - val_accuracy: 0.9317
    Epoch 84/100
    22/22 [==============================] - 22s 985ms/step - loss: 0.6400 - accuracy: 0.9602 - val_loss: 2.8859 - val_accuracy: 0.9130
    Epoch 85/100
    22/22 [==============================] - 22s 979ms/step - loss: 0.7711 - accuracy: 0.9521 - val_loss: 1.8540 - val_accuracy: 0.9130
    Epoch 86/100
    22/22 [==============================] - 21s 952ms/step - loss: 0.6902 - accuracy: 0.9588 - val_loss: 3.1994 - val_accuracy: 0.9130
    Epoch 87/100
    22/22 [==============================] - 21s 943ms/step - loss: 0.5642 - accuracy: 0.9676 - val_loss: 3.4640 - val_accuracy: 0.9193
    Epoch 88/100
    22/22 [==============================] - 21s 964ms/step - loss: 0.7075 - accuracy: 0.9639 - val_loss: 3.7122 - val_accuracy: 0.8882
    Epoch 89/100
    22/22 [==============================] - 21s 956ms/step - loss: 1.0417 - accuracy: 0.9485 - val_loss: 2.8577 - val_accuracy: 0.9068
    Epoch 90/100
    22/22 [==============================] - 22s 987ms/step - loss: 0.8267 - accuracy: 0.9529 - val_loss: 3.2278 - val_accuracy: 0.8944
    Epoch 91/100
    22/22 [==============================] - 21s 970ms/step - loss: 0.9532 - accuracy: 0.9507 - val_loss: 4.2726 - val_accuracy: 0.8758
    Epoch 92/100
    22/22 [==============================] - 21s 945ms/step - loss: 0.7140 - accuracy: 0.9536 - val_loss: 3.1563 - val_accuracy: 0.8820
    Epoch 93/100
    22/22 [==============================] - 21s 956ms/step - loss: 0.8453 - accuracy: 0.9543 - val_loss: 3.5936 - val_accuracy: 0.8696
    Epoch 94/100
    22/22 [==============================] - 22s 978ms/step - loss: 0.7140 - accuracy: 0.9536 - val_loss: 2.0659 - val_accuracy: 0.9255
    Epoch 95/100
    22/22 [==============================] - 21s 956ms/step - loss: 0.7567 - accuracy: 0.9558 - val_loss: 2.6587 - val_accuracy: 0.9130
    Epoch 96/100
    22/22 [==============================] - 21s 976ms/step - loss: 0.6307 - accuracy: 0.9669 - val_loss: 2.4858 - val_accuracy: 0.9317
    Epoch 97/100
    22/22 [==============================] - 21s 939ms/step - loss: 0.5958 - accuracy: 0.9647 - val_loss: 2.2983 - val_accuracy: 0.9441
    Epoch 98/100
    22/22 [==============================] - 21s 958ms/step - loss: 0.8081 - accuracy: 0.9551 - val_loss: 2.5357 - val_accuracy: 0.9006
    Epoch 99/100
    22/22 [==============================] - 23s 1s/step - loss: 0.7702 - accuracy: 0.9566 - val_loss: 2.1749 - val_accuracy: 0.8944
    Epoch 100/100
    22/22 [==============================] - 23s 1s/step - loss: 0.6679 - accuracy: 0.9624 - val_loss: 1.5233 - val_accuracy: 0.9379

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/11.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    8/8 [==============================] - 3s 375ms/step - loss: 2.7397 - accuracy: 0.9303

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[30\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.739748477935791, 0.9303278923034668]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.930327868852459
                  precision    recall  f1-score   support

               0       0.91      0.86      0.88       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.91      0.92       488
    weighted avg       0.93      0.93      0.93       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[33\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/12.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/13.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/14.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.91      0.86      0.88       148
               1       0.94      0.96      0.95       340

        accuracy                           0.93       488
       macro avg       0.92      0.91      0.92       488
    weighted avg       0.93      0.93      0.93       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.91      0.86      0.96      0.88      0.91      0.82       148
              1       0.94      0.96      0.86      0.95      0.91      0.83       340

    avg / total       0.93      0.93      0.89      0.93      0.91      0.83       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
