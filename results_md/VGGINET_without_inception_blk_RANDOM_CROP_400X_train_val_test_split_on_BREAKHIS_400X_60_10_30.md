<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# REMOVE INCEPTION BLOCK<a class="anchor-link" href="#REMOVE-INCEPTION-BLOCK">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
# def naive_inception_module(layer_in, f1, f2, f3):
#     # 1x1 conv
#     conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
#     # 3x3 conv
#     conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
#     # 5x5 conv
#     conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
#     # 3x3 max pooling
#     pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
#     # concatenate filters, assumes filters/channels last
#     layer_out = Concatenate()([conv1, conv3, conv5, pool])
#     return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = feature_ex_model.output
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    Image_Input (InputLayer)     [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    VGG_Preprocess (Lambda)      (None, 224, 224, 3)       0         
    _________________________________________________________________
    vgg16_features (Functional)  (None, 14, 14, 512)       7635264   
    _________________________________________________________________
    BN (BatchNormalization)      (None, 14, 14, 512)       2048      
    _________________________________________________________________
    flatten (Flatten)            (None, 100352)            0         
    _________________________________________________________________
    Dropout (Dropout)            (None, 100352)            0         
    _________________________________________________________________
    Predictions (Dense)          (None, 2)                 200706    
    =================================================================
    Total params: 7,838,018
    Trainable params: 201,730
    Non-trainable params: 7,636,288
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    mkdir: cannot create directory ‘./experiments/00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet’: File exists

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.0983 - accuracy: 0.5312WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 27s 3s/step - loss: 1.9429 - accuracy: 0.7003 - val_loss: 5.9955 - val_accuracy: 0.8199
    Epoch 2/100
    8/8 [==============================] - 10s 1s/step - loss: 1.2757 - accuracy: 0.8208 - val_loss: 2.8113 - val_accuracy: 0.8509
    Epoch 3/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9618 - accuracy: 0.8363 - val_loss: 1.4074 - val_accuracy: 0.9193
    Epoch 4/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9365 - accuracy: 0.8558 - val_loss: 1.3333 - val_accuracy: 0.8882
    Epoch 5/100
    8/8 [==============================] - 10s 1s/step - loss: 0.8707 - accuracy: 0.8465 - val_loss: 1.2298 - val_accuracy: 0.9193
    Epoch 6/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8142 - accuracy: 0.8517 - val_loss: 1.0585 - val_accuracy: 0.9255
    Epoch 7/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9162 - accuracy: 0.8630 - val_loss: 1.2234 - val_accuracy: 0.9068
    Epoch 8/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1475 - accuracy: 0.8682 - val_loss: 1.1802 - val_accuracy: 0.8944
    Epoch 9/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9088 - accuracy: 0.8723 - val_loss: 1.1300 - val_accuracy: 0.9130
    Epoch 10/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0352 - accuracy: 0.8671 - val_loss: 1.0606 - val_accuracy: 0.8820
    Epoch 11/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9472 - accuracy: 0.8548 - val_loss: 0.8803 - val_accuracy: 0.9255
    Epoch 12/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9231 - accuracy: 0.8764 - val_loss: 1.3032 - val_accuracy: 0.8882
    Epoch 13/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0851 - accuracy: 0.8610 - val_loss: 0.8818 - val_accuracy: 0.9193
    Epoch 14/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1378 - accuracy: 0.8558 - val_loss: 1.0146 - val_accuracy: 0.9379
    Epoch 15/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1535 - accuracy: 0.8754 - val_loss: 1.2606 - val_accuracy: 0.8758
    Epoch 16/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0522 - accuracy: 0.8682 - val_loss: 1.0103 - val_accuracy: 0.9130
    Epoch 17/100
    8/8 [==============================] - 14s 2s/step - loss: 0.9403 - accuracy: 0.8713 - val_loss: 1.0826 - val_accuracy: 0.9006
    Epoch 18/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0635 - accuracy: 0.8888 - val_loss: 1.1467 - val_accuracy: 0.8882
    Epoch 19/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9812 - accuracy: 0.8630 - val_loss: 1.0908 - val_accuracy: 0.9255
    Epoch 20/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0005 - accuracy: 0.8898 - val_loss: 1.2704 - val_accuracy: 0.9006
    Epoch 21/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0115 - accuracy: 0.8795 - val_loss: 1.1024 - val_accuracy: 0.9068
    Epoch 22/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2399 - accuracy: 0.8568 - val_loss: 1.0799 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0824 - accuracy: 0.8826 - val_loss: 1.1892 - val_accuracy: 0.9006
    Epoch 24/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1131 - accuracy: 0.8888 - val_loss: 1.2177 - val_accuracy: 0.8882
    Epoch 25/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0007 - accuracy: 0.8867 - val_loss: 1.2227 - val_accuracy: 0.8758
    Epoch 26/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1242 - accuracy: 0.8713 - val_loss: 1.0867 - val_accuracy: 0.9006
    Epoch 27/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1509 - accuracy: 0.8847 - val_loss: 1.0847 - val_accuracy: 0.8944
    Epoch 28/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0380 - accuracy: 0.8898 - val_loss: 1.2392 - val_accuracy: 0.8820
    Epoch 29/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9347 - accuracy: 0.8867 - val_loss: 1.1851 - val_accuracy: 0.8882
    Epoch 30/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9613 - accuracy: 0.8847 - val_loss: 1.2087 - val_accuracy: 0.9130
    Epoch 31/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8619 - accuracy: 0.8991 - val_loss: 1.1954 - val_accuracy: 0.9006
    Epoch 32/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8636 - accuracy: 0.9001 - val_loss: 1.2363 - val_accuracy: 0.8944
    Epoch 33/100
    8/8 [==============================] - 10s 1s/step - loss: 0.9678 - accuracy: 0.9104 - val_loss: 1.1715 - val_accuracy: 0.9130
    Epoch 34/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7479 - accuracy: 0.8908 - val_loss: 1.0917 - val_accuracy: 0.9068
    Epoch 35/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9576 - accuracy: 0.8898 - val_loss: 1.0050 - val_accuracy: 0.8944
    Epoch 36/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1250 - accuracy: 0.8702 - val_loss: 0.9014 - val_accuracy: 0.8944
    Epoch 37/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0617 - accuracy: 0.8805 - val_loss: 0.8459 - val_accuracy: 0.9006
    Epoch 38/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8689 - accuracy: 0.8919 - val_loss: 0.9009 - val_accuracy: 0.8944
    Epoch 39/100
    8/8 [==============================] - 10s 1s/step - loss: 1.0855 - accuracy: 0.8795 - val_loss: 1.0528 - val_accuracy: 0.9255
    Epoch 40/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0800 - accuracy: 0.9001 - val_loss: 0.9650 - val_accuracy: 0.9317
    Epoch 41/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9520 - accuracy: 0.8764 - val_loss: 0.8125 - val_accuracy: 0.9317
    Epoch 42/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0638 - accuracy: 0.8908 - val_loss: 0.9392 - val_accuracy: 0.9317
    Epoch 43/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2620 - accuracy: 0.8702 - val_loss: 0.8982 - val_accuracy: 0.9130
    Epoch 44/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7988 - accuracy: 0.9114 - val_loss: 1.1222 - val_accuracy: 0.9130
    Epoch 45/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9912 - accuracy: 0.8908 - val_loss: 1.2269 - val_accuracy: 0.9006
    Epoch 46/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0163 - accuracy: 0.8980 - val_loss: 1.0263 - val_accuracy: 0.9255
    Epoch 47/100
    8/8 [==============================] - 10s 1s/step - loss: 0.7803 - accuracy: 0.9125 - val_loss: 0.9039 - val_accuracy: 0.9317
    Epoch 48/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2374 - accuracy: 0.8744 - val_loss: 1.0384 - val_accuracy: 0.9006
    Epoch 49/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1425 - accuracy: 0.8877 - val_loss: 0.9886 - val_accuracy: 0.9130
    Epoch 50/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8867 - accuracy: 0.9156 - val_loss: 0.9270 - val_accuracy: 0.9068
    Epoch 51/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9262 - accuracy: 0.8960 - val_loss: 0.8327 - val_accuracy: 0.9317
    Epoch 52/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7842 - accuracy: 0.9063 - val_loss: 0.8354 - val_accuracy: 0.9006
    Epoch 53/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9709 - accuracy: 0.9145 - val_loss: 0.7498 - val_accuracy: 0.9068
    Epoch 54/100
    8/8 [==============================] - 10s 1s/step - loss: 1.0180 - accuracy: 0.9053 - val_loss: 0.9168 - val_accuracy: 0.9006
    Epoch 55/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9197 - accuracy: 0.9042 - val_loss: 0.9664 - val_accuracy: 0.9068
    Epoch 56/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8271 - accuracy: 0.9073 - val_loss: 0.8257 - val_accuracy: 0.9255
    Epoch 57/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9999 - accuracy: 0.8908 - val_loss: 0.8376 - val_accuracy: 0.9255
    Epoch 58/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1539 - accuracy: 0.8991 - val_loss: 0.8425 - val_accuracy: 0.9255
    Epoch 59/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0420 - accuracy: 0.8980 - val_loss: 1.0458 - val_accuracy: 0.9068
    Epoch 60/100
    8/8 [==============================] - 12s 2s/step - loss: 1.0517 - accuracy: 0.8980 - val_loss: 0.9359 - val_accuracy: 0.9068
    Epoch 61/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9863 - accuracy: 0.9032 - val_loss: 0.8903 - val_accuracy: 0.9255
    Epoch 62/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1729 - accuracy: 0.9001 - val_loss: 0.8183 - val_accuracy: 0.9379
    Epoch 63/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9607 - accuracy: 0.9156 - val_loss: 0.8245 - val_accuracy: 0.9130
    Epoch 64/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0852 - accuracy: 0.8908 - val_loss: 0.8836 - val_accuracy: 0.9006
    Epoch 65/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0135 - accuracy: 0.9063 - val_loss: 0.9194 - val_accuracy: 0.8820
    Epoch 66/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1623 - accuracy: 0.8867 - val_loss: 0.8914 - val_accuracy: 0.9255
    Epoch 67/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1524 - accuracy: 0.9053 - val_loss: 0.8321 - val_accuracy: 0.9317
    Epoch 68/100
    8/8 [==============================] - 12s 2s/step - loss: 0.7967 - accuracy: 0.9176 - val_loss: 0.8743 - val_accuracy: 0.9255
    Epoch 69/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8789 - accuracy: 0.9207 - val_loss: 0.8846 - val_accuracy: 0.9317
    Epoch 70/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9135 - accuracy: 0.9011 - val_loss: 0.8397 - val_accuracy: 0.9255
    Epoch 71/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1094 - accuracy: 0.9094 - val_loss: 0.7890 - val_accuracy: 0.9193
    Epoch 72/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9069 - accuracy: 0.9186 - val_loss: 0.9547 - val_accuracy: 0.9006
    Epoch 73/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9669 - accuracy: 0.9156 - val_loss: 0.9480 - val_accuracy: 0.8944
    Epoch 74/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9924 - accuracy: 0.8919 - val_loss: 1.2107 - val_accuracy: 0.9130
    Epoch 75/100
    8/8 [==============================] - 10s 1s/step - loss: 1.0983 - accuracy: 0.8970 - val_loss: 1.2096 - val_accuracy: 0.9193
    Epoch 76/100
    8/8 [==============================] - 13s 2s/step - loss: 0.8006 - accuracy: 0.9186 - val_loss: 1.1022 - val_accuracy: 0.9130
    Epoch 77/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8864 - accuracy: 0.9011 - val_loss: 1.0821 - val_accuracy: 0.9006
    Epoch 78/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9740 - accuracy: 0.9166 - val_loss: 0.9141 - val_accuracy: 0.9193
    Epoch 79/100
    8/8 [==============================] - 13s 2s/step - loss: 0.7815 - accuracy: 0.9310 - val_loss: 0.9847 - val_accuracy: 0.9255
    Epoch 80/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0901 - accuracy: 0.9011 - val_loss: 0.9642 - val_accuracy: 0.9379
    Epoch 81/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8073 - accuracy: 0.9197 - val_loss: 0.9022 - val_accuracy: 0.9317
    Epoch 82/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9253 - accuracy: 0.9063 - val_loss: 0.8201 - val_accuracy: 0.9317
    Epoch 83/100
    8/8 [==============================] - 10s 1s/step - loss: 0.9469 - accuracy: 0.9156 - val_loss: 0.8720 - val_accuracy: 0.9255
    Epoch 84/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8938 - accuracy: 0.9145 - val_loss: 0.8458 - val_accuracy: 0.9193
    Epoch 85/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9795 - accuracy: 0.9022 - val_loss: 0.9685 - val_accuracy: 0.9130
    Epoch 86/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9355 - accuracy: 0.9042 - val_loss: 0.8534 - val_accuracy: 0.9255
    Epoch 87/100
    8/8 [==============================] - 12s 2s/step - loss: 1.1257 - accuracy: 0.9032 - val_loss: 0.7609 - val_accuracy: 0.9565
    Epoch 88/100
    8/8 [==============================] - 13s 2s/step - loss: 0.9882 - accuracy: 0.9114 - val_loss: 0.8331 - val_accuracy: 0.9441
    Epoch 89/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0155 - accuracy: 0.9042 - val_loss: 0.9250 - val_accuracy: 0.9130
    Epoch 90/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0939 - accuracy: 0.9011 - val_loss: 1.1483 - val_accuracy: 0.9255
    Epoch 91/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0332 - accuracy: 0.8991 - val_loss: 1.2406 - val_accuracy: 0.9130
    Epoch 92/100
    8/8 [==============================] - 13s 2s/step - loss: 1.2394 - accuracy: 0.9063 - val_loss: 0.9546 - val_accuracy: 0.9317
    Epoch 93/100
    8/8 [==============================] - 10s 1s/step - loss: 1.1493 - accuracy: 0.9022 - val_loss: 0.9215 - val_accuracy: 0.9317
    Epoch 94/100
    8/8 [==============================] - 12s 2s/step - loss: 0.8861 - accuracy: 0.9166 - val_loss: 1.0151 - val_accuracy: 0.9193
    Epoch 95/100
    8/8 [==============================] - 10s 1s/step - loss: 0.9861 - accuracy: 0.9083 - val_loss: 0.9672 - val_accuracy: 0.9068
    Epoch 96/100
    8/8 [==============================] - 13s 2s/step - loss: 1.1286 - accuracy: 0.8970 - val_loss: 0.9946 - val_accuracy: 0.9317
    Epoch 97/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0012 - accuracy: 0.9248 - val_loss: 0.9417 - val_accuracy: 0.9503
    Epoch 98/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0532 - accuracy: 0.9166 - val_loss: 0.8791 - val_accuracy: 0.9193
    Epoch 99/100
    8/8 [==============================] - 12s 2s/step - loss: 0.9522 - accuracy: 0.9042 - val_loss: 1.0818 - val_accuracy: 0.9317
    Epoch 100/100
    8/8 [==============================] - 13s 2s/step - loss: 1.0713 - accuracy: 0.9207 - val_loss: 0.8159 - val_accuracy: 0.9193
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/191.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 29s 7s/step - loss: 1.3766 - accuracy: 0.8893

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3765809535980225, 0.8893442749977112]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.889344262295082
                  precision    recall  f1-score   support

               0       0.80      0.85      0.82       148
               1       0.93      0.91      0.92       340

        accuracy                           0.89       488
       macro avg       0.87      0.88      0.87       488
    weighted avg       0.89      0.89      0.89       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/192.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/193.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/194.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.80      0.85      0.82       148
               1       0.93      0.91      0.92       340

        accuracy                           0.89       488
       macro avg       0.87      0.88      0.87       488
    weighted avg       0.89      0.89      0.89       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.80      0.85      0.91      0.82      0.88      0.77       148
              1       0.93      0.91      0.85      0.92      0.88      0.78       340

    avg / total       0.89      0.89      0.87      0.89      0.88      0.77       488

</div>
</div>
</div>
</div>
</div>
</div>
</div>
