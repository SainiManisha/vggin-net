<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK4-FINETUNING_40X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0646 - accuracy: 0.5859WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 17s 2s/step - loss: 1.8320 - accuracy: 0.7716 - val_loss: 5.6611 - val_accuracy: 0.8827
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 1.5915 - accuracy: 0.8524 - val_loss: 5.2457 - val_accuracy: 0.8603
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3623 - accuracy: 0.8719 - val_loss: 2.9985 - val_accuracy: 0.8994
    Epoch 4/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4397 - accuracy: 0.8802 - val_loss: 1.6284 - val_accuracy: 0.9218
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2417 - accuracy: 0.9044 - val_loss: 1.6912 - val_accuracy: 0.9218
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8262 - accuracy: 0.9220 - val_loss: 2.8828 - val_accuracy: 0.8771
    Epoch 7/100
    9/9 [==============================] - 12s 1s/step - loss: 0.9245 - accuracy: 0.9127 - val_loss: 1.4021 - val_accuracy: 0.9274
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7981 - accuracy: 0.9174 - val_loss: 2.2807 - val_accuracy: 0.9162
    Epoch 9/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0045 - accuracy: 0.9183 - val_loss: 1.9710 - val_accuracy: 0.8827
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3998 - accuracy: 0.9053 - val_loss: 4.8467 - val_accuracy: 0.8659
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4692 - accuracy: 0.8997 - val_loss: 2.4086 - val_accuracy: 0.9050
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1569 - accuracy: 0.9257 - val_loss: 1.2357 - val_accuracy: 0.9441
    Epoch 13/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2792 - accuracy: 0.9183 - val_loss: 1.2478 - val_accuracy: 0.9274
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9736 - accuracy: 0.9378 - val_loss: 1.3200 - val_accuracy: 0.9218
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9237 - accuracy: 0.9322 - val_loss: 1.1377 - val_accuracy: 0.9385
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7477 - accuracy: 0.9508 - val_loss: 2.0381 - val_accuracy: 0.9274
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5157 - accuracy: 0.9601 - val_loss: 3.6772 - val_accuracy: 0.8883
    Epoch 18/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5753 - accuracy: 0.9573 - val_loss: 1.9975 - val_accuracy: 0.9050
    Epoch 19/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5196 - accuracy: 0.9545 - val_loss: 2.7263 - val_accuracy: 0.8939
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6847 - accuracy: 0.9471 - val_loss: 1.7666 - val_accuracy: 0.9162
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6087 - accuracy: 0.9591 - val_loss: 1.3739 - val_accuracy: 0.9274
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4054 - accuracy: 0.9629 - val_loss: 1.0544 - val_accuracy: 0.9441
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5568 - accuracy: 0.9601 - val_loss: 1.5048 - val_accuracy: 0.9385
    Epoch 24/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5977 - accuracy: 0.9591 - val_loss: 2.4340 - val_accuracy: 0.9218
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6004 - accuracy: 0.9656 - val_loss: 1.5181 - val_accuracy: 0.9441
    Epoch 26/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2218 - accuracy: 0.9749 - val_loss: 1.7408 - val_accuracy: 0.9441
    Epoch 27/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2753 - accuracy: 0.9768 - val_loss: 1.2600 - val_accuracy: 0.9609
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3014 - accuracy: 0.9749 - val_loss: 1.3929 - val_accuracy: 0.9553
    Epoch 29/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4094 - accuracy: 0.9721 - val_loss: 1.2188 - val_accuracy: 0.9665
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3427 - accuracy: 0.9749 - val_loss: 1.0665 - val_accuracy: 0.9609
    Epoch 31/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2806 - accuracy: 0.9796 - val_loss: 1.2333 - val_accuracy: 0.9609
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6607 - accuracy: 0.9564 - val_loss: 1.2564 - val_accuracy: 0.9497
    Epoch 33/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5848 - accuracy: 0.9675 - val_loss: 1.3299 - val_accuracy: 0.9497
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4124 - accuracy: 0.9703 - val_loss: 1.1953 - val_accuracy: 0.9553
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2182 - accuracy: 0.9814 - val_loss: 0.6571 - val_accuracy: 0.9721
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2780 - accuracy: 0.9786 - val_loss: 0.7221 - val_accuracy: 0.9665
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4048 - accuracy: 0.9768 - val_loss: 0.8584 - val_accuracy: 0.9665
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3030 - accuracy: 0.9796 - val_loss: 1.1797 - val_accuracy: 0.9609
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3107 - accuracy: 0.9786 - val_loss: 1.6417 - val_accuracy: 0.9441
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2429 - accuracy: 0.9814 - val_loss: 1.0886 - val_accuracy: 0.9665
    Epoch 41/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3783 - accuracy: 0.9749 - val_loss: 1.5193 - val_accuracy: 0.9330
    Epoch 42/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3986 - accuracy: 0.9712 - val_loss: 1.0061 - val_accuracy: 0.9609
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3791 - accuracy: 0.9749 - val_loss: 2.8048 - val_accuracy: 0.9162
    Epoch 44/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2547 - accuracy: 0.9786 - val_loss: 1.2616 - val_accuracy: 0.9609
    Epoch 45/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4610 - accuracy: 0.9694 - val_loss: 1.0333 - val_accuracy: 0.9609
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2416 - accuracy: 0.9796 - val_loss: 1.0159 - val_accuracy: 0.9553
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2031 - accuracy: 0.9814 - val_loss: 0.7329 - val_accuracy: 0.9609
    Epoch 48/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3038 - accuracy: 0.9786 - val_loss: 0.8125 - val_accuracy: 0.9553
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1884 - accuracy: 0.9814 - val_loss: 1.0076 - val_accuracy: 0.9609
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3308 - accuracy: 0.9777 - val_loss: 1.1421 - val_accuracy: 0.9497
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4415 - accuracy: 0.9759 - val_loss: 0.9839 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1514 - accuracy: 0.9824 - val_loss: 1.4403 - val_accuracy: 0.9665
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2685 - accuracy: 0.9833 - val_loss: 1.8971 - val_accuracy: 0.9497
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1621 - accuracy: 0.9870 - val_loss: 2.2595 - val_accuracy: 0.9497
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2509 - accuracy: 0.9842 - val_loss: 1.4910 - val_accuracy: 0.9665
    Epoch 56/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2545 - accuracy: 0.9786 - val_loss: 1.6287 - val_accuracy: 0.9497
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2635 - accuracy: 0.9851 - val_loss: 2.0444 - val_accuracy: 0.9553
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2851 - accuracy: 0.9833 - val_loss: 1.3197 - val_accuracy: 0.9609
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1803 - accuracy: 0.9861 - val_loss: 1.3791 - val_accuracy: 0.9497
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2141 - accuracy: 0.9796 - val_loss: 1.6452 - val_accuracy: 0.9609
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2007 - accuracy: 0.9842 - val_loss: 0.7267 - val_accuracy: 0.9553
    Epoch 62/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2349 - accuracy: 0.9814 - val_loss: 1.0653 - val_accuracy: 0.9553
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1521 - accuracy: 0.9879 - val_loss: 1.0401 - val_accuracy: 0.9441
    Epoch 64/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2618 - accuracy: 0.9768 - val_loss: 1.3422 - val_accuracy: 0.9441
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1262 - accuracy: 0.9907 - val_loss: 1.7457 - val_accuracy: 0.9330
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3098 - accuracy: 0.9786 - val_loss: 1.3438 - val_accuracy: 0.9609
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3174 - accuracy: 0.9805 - val_loss: 1.0991 - val_accuracy: 0.9553
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1065 - accuracy: 0.9907 - val_loss: 0.9305 - val_accuracy: 0.9609
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2162 - accuracy: 0.9879 - val_loss: 0.7364 - val_accuracy: 0.9609
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1787 - accuracy: 0.9898 - val_loss: 0.6928 - val_accuracy: 0.9609
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0889 - accuracy: 0.9907 - val_loss: 1.0962 - val_accuracy: 0.9553
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2473 - accuracy: 0.9805 - val_loss: 1.4777 - val_accuracy: 0.9441
    Epoch 73/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1976 - accuracy: 0.9851 - val_loss: 1.0557 - val_accuracy: 0.9665
    Epoch 74/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4025 - accuracy: 0.9786 - val_loss: 1.0590 - val_accuracy: 0.9497
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2682 - accuracy: 0.9851 - val_loss: 3.7671 - val_accuracy: 0.9218
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3381 - accuracy: 0.9833 - val_loss: 2.3628 - val_accuracy: 0.9441
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5835 - accuracy: 0.9740 - val_loss: 3.6950 - val_accuracy: 0.9497
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4239 - accuracy: 0.9814 - val_loss: 1.7674 - val_accuracy: 0.9497
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4765 - accuracy: 0.9740 - val_loss: 3.0349 - val_accuracy: 0.9553
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3603 - accuracy: 0.9786 - val_loss: 1.3335 - val_accuracy: 0.9553
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1230 - accuracy: 0.9907 - val_loss: 0.9379 - val_accuracy: 0.9497
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3263 - accuracy: 0.9842 - val_loss: 1.2375 - val_accuracy: 0.9721
    Epoch 83/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2150 - accuracy: 0.9861 - val_loss: 2.5444 - val_accuracy: 0.9609
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3239 - accuracy: 0.9805 - val_loss: 0.6059 - val_accuracy: 0.9665
    Epoch 85/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3766 - accuracy: 0.9833 - val_loss: 1.0962 - val_accuracy: 0.9721
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1412 - accuracy: 0.9851 - val_loss: 3.3393 - val_accuracy: 0.9330
    Epoch 87/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2085 - accuracy: 0.9861 - val_loss: 1.2873 - val_accuracy: 0.9665
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1234 - accuracy: 0.9907 - val_loss: 1.0077 - val_accuracy: 0.9665
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2493 - accuracy: 0.9861 - val_loss: 2.2921 - val_accuracy: 0.9497
    Epoch 90/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3833 - accuracy: 0.9851 - val_loss: 1.7471 - val_accuracy: 0.9553
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2242 - accuracy: 0.9898 - val_loss: 0.6556 - val_accuracy: 0.9665
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3431 - accuracy: 0.9786 - val_loss: 0.5857 - val_accuracy: 0.9609
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3508 - accuracy: 0.9777 - val_loss: 2.1016 - val_accuracy: 0.9497
    Epoch 94/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3135 - accuracy: 0.9814 - val_loss: 2.3376 - val_accuracy: 0.9553
    Epoch 95/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1512 - accuracy: 0.9907 - val_loss: 2.1103 - val_accuracy: 0.9553
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1867 - accuracy: 0.9851 - val_loss: 1.4439 - val_accuracy: 0.9497
    Epoch 97/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1236 - accuracy: 0.9898 - val_loss: 1.7514 - val_accuracy: 0.9553
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1615 - accuracy: 0.9889 - val_loss: 1.8858 - val_accuracy: 0.9721
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1994 - accuracy: 0.9851 - val_loss: 1.9409 - val_accuracy: 0.9609
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1677 - accuracy: 0.9898 - val_loss: 1.6545 - val_accuracy: 0.9497

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/111.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 7s 1s/step - loss: 1.7427 - accuracy: 0.9555

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.7427211999893188, 0.9554731249809265]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 7,222,178
    Non-trainable params: 1,736,960
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/112.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 128, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    9/9 [==============================] - 18s 2s/step - loss: 0.0261 - accuracy: 0.9981 - val_loss: 2.3886 - val_accuracy: 0.9553

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    9/9 [==============================] - 11s 1s/step - loss: 0.2090 - accuracy: 0.9870 - val_loss: 1.3204 - val_accuracy: 0.9665

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0792 - accuracy: 0.9954 - val_loss: 1.5689 - val_accuracy: 0.9721

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0592 - accuracy: 0.9981 - val_loss: 2.7014 - val_accuracy: 0.9609

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    9/9 [==============================] - 11s 1s/step - loss: 0.2191 - accuracy: 0.9861 - val_loss: 2.2145 - val_accuracy: 0.9665

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    9/9 [==============================] - 11s 1s/step - loss: 0.1552 - accuracy: 0.9916 - val_loss: 1.4654 - val_accuracy: 0.9721

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0968 - accuracy: 0.9944 - val_loss: 1.0281 - val_accuracy: 0.9777

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0891 - accuracy: 0.9935 - val_loss: 1.0389 - val_accuracy: 0.9665

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0782 - accuracy: 0.9944 - val_loss: 1.2219 - val_accuracy: 0.9665

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0560 - accuracy: 0.9954 - val_loss: 1.2947 - val_accuracy: 0.9721

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    9/9 [==============================] - 14s 2s/step - loss: 0.1323 - accuracy: 0.9916 - val_loss: 1.3404 - val_accuracy: 0.9777

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0574 - accuracy: 0.9926 - val_loss: 0.9619 - val_accuracy: 0.9609

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0681 - accuracy: 0.9935 - val_loss: 1.1289 - val_accuracy: 0.9609

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0654 - accuracy: 0.9954 - val_loss: 1.1235 - val_accuracy: 0.9609

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    9/9 [==============================] - 14s 2s/step - loss: 0.2436 - accuracy: 0.9870 - val_loss: 1.4464 - val_accuracy: 0.9721

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1050 - accuracy: 0.9889 - val_loss: 1.8919 - val_accuracy: 0.9721

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0202 - accuracy: 0.9972 - val_loss: 1.9855 - val_accuracy: 0.9665

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0885 - accuracy: 0.9944 - val_loss: 2.4030 - val_accuracy: 0.9665

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    9/9 [==============================] - 14s 2s/step - loss: 0.1103 - accuracy: 0.9926 - val_loss: 3.0583 - val_accuracy: 0.9330

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0910 - accuracy: 0.9889 - val_loss: 2.2491 - val_accuracy: 0.9665

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0548 - accuracy: 0.9972 - val_loss: 1.1451 - val_accuracy: 0.9665

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0439 - accuracy: 0.9954 - val_loss: 1.1366 - val_accuracy: 0.9609

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0216 - accuracy: 0.9981 - val_loss: 1.2008 - val_accuracy: 0.9609

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0898 - accuracy: 0.9926 - val_loss: 1.0808 - val_accuracy: 0.9721

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0184 - accuracy: 0.9972 - val_loss: 1.2628 - val_accuracy: 0.9721

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0991 - accuracy: 0.9935 - val_loss: 1.5927 - val_accuracy: 0.9665

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    9/9 [==============================] - 14s 2s/step - loss: 0.1550 - accuracy: 0.9926 - val_loss: 1.9376 - val_accuracy: 0.9665

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    9/9 [==============================] - 10s 1s/step - loss: 0.1218 - accuracy: 0.9935 - val_loss: 1.8413 - val_accuracy: 0.9665

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0756 - accuracy: 0.9954 - val_loss: 2.4577 - val_accuracy: 0.9609

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0450 - accuracy: 0.9944 - val_loss: 2.2896 - val_accuracy: 0.9609

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    9/9 [==============================] - 14s 2s/step - loss: 0.1144 - accuracy: 0.9944 - val_loss: 2.3033 - val_accuracy: 0.9609

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0798 - accuracy: 0.9944 - val_loss: 1.6167 - val_accuracy: 0.9777

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0223 - accuracy: 0.9963 - val_loss: 0.9457 - val_accuracy: 0.9832

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1180 - accuracy: 0.9944 - val_loss: 1.1656 - val_accuracy: 0.9777

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0650 - accuracy: 0.9935 - val_loss: 1.0882 - val_accuracy: 0.9665

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0457 - accuracy: 0.9981 - val_loss: 1.1388 - val_accuracy: 0.9665

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0427 - accuracy: 0.9944 - val_loss: 1.1082 - val_accuracy: 0.9665

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0828 - accuracy: 0.9944 - val_loss: 1.1113 - val_accuracy: 0.9721

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0370 - accuracy: 0.9954 - val_loss: 1.2978 - val_accuracy: 0.9609

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0115 - accuracy: 0.9981 - val_loss: 1.2861 - val_accuracy: 0.9609

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0171 - accuracy: 0.9981 - val_loss: 1.2532 - val_accuracy: 0.9609

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0851 - accuracy: 0.9944 - val_loss: 1.2460 - val_accuracy: 0.9665

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0323 - accuracy: 0.9963 - val_loss: 1.4707 - val_accuracy: 0.9665

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0862 - accuracy: 0.9926 - val_loss: 1.0941 - val_accuracy: 0.9665

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    9/9 [==============================] - 11s 1s/step - loss: 0.0289 - accuracy: 0.9981 - val_loss: 1.2919 - val_accuracy: 0.9553

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0017 - accuracy: 0.9991 - val_loss: 1.5195 - val_accuracy: 0.9497

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0566 - accuracy: 0.9963 - val_loss: 1.1006 - val_accuracy: 0.9721

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    9/9 [==============================] - 11s 1s/step - loss: 0.1158 - accuracy: 0.9926 - val_loss: 2.0514 - val_accuracy: 0.9609

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    9/9 [==============================] - 14s 2s/step - loss: 0.0524 - accuracy: 0.9972 - val_loss: 3.0607 - val_accuracy: 0.9609

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    9/9 [==============================] - 14s 2s/step - loss: 0.1406 - accuracy: 0.9889 - val_loss: 2.4958 - val_accuracy: 0.9553

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/113.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 1.7321 - accuracy: 0.9610

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.7320551872253418, 0.9610389471054077]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.961038961038961
                  precision    recall  f1-score   support

               0       0.98      0.89      0.93       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.97      0.94      0.95       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/114.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/115.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/116.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.89      0.93       158
               1       0.95      0.99      0.97       381

        accuracy                           0.96       539
       macro avg       0.97      0.94      0.95       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.89      0.99      0.93      0.94      0.87       158
              1       0.95      0.99      0.89      0.97      0.94      0.89       381

    avg / total       0.96      0.96      0.92      0.96      0.94      0.88       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
