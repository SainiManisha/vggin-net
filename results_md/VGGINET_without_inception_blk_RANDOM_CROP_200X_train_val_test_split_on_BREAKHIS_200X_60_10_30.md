<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# REMOVE INCEPTION BLOCK<a class="anchor-link" href="#REMOVE-INCEPTION-BLOCK">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
# def naive_inception_module(layer_in, f1, f2, f3):
#     # 1x1 conv
#     conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
#     # 3x3 conv
#     conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
#     # 5x5 conv
#     conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
#     # 3x3 max pooling
#     pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
#     # concatenate filters, assumes filters/channels last
#     layer_out = Concatenate()([conv1, conv3, conv5, pool])
#     return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = feature_ex_model.output
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    Image_Input (InputLayer)     [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    VGG_Preprocess (Lambda)      (None, 224, 224, 3)       0         
    _________________________________________________________________
    vgg16_features (Functional)  (None, 14, 14, 512)       7635264   
    _________________________________________________________________
    BN (BatchNormalization)      (None, 14, 14, 512)       2048      
    _________________________________________________________________
    flatten (Flatten)            (None, 100352)            0         
    _________________________________________________________________
    Dropout (Dropout)            (None, 100352)            0         
    _________________________________________________________________
    Predictions (Dense)          (None, 2)                 200706    
    =================================================================
    Total params: 7,838,018
    Trainable params: 201,730
    Non-trainable params: 7,636,288
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    mkdir: cannot create directory ‘./experiments/00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet’: File exists

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3601 - accuracy: 0.4688WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 28s 3s/step - loss: 1.4283 - accuracy: 0.7571 - val_loss: 10.0173 - val_accuracy: 0.7624
    Epoch 2/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1028 - accuracy: 0.8556 - val_loss: 2.6092 - val_accuracy: 0.8674
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0887 - accuracy: 0.8482 - val_loss: 4.5092 - val_accuracy: 0.7845
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9666 - accuracy: 0.8592 - val_loss: 4.0323 - val_accuracy: 0.7790
    Epoch 5/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8209 - accuracy: 0.8749 - val_loss: 2.1712 - val_accuracy: 0.8453
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7904 - accuracy: 0.8887 - val_loss: 1.4030 - val_accuracy: 0.8840
    Epoch 7/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8575 - accuracy: 0.8703 - val_loss: 1.3143 - val_accuracy: 0.8895
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8210 - accuracy: 0.8887 - val_loss: 0.9678 - val_accuracy: 0.9171
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9450 - accuracy: 0.8887 - val_loss: 1.1940 - val_accuracy: 0.8950
    Epoch 10/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9092 - accuracy: 0.8666 - val_loss: 1.0736 - val_accuracy: 0.8950
    Epoch 11/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8024 - accuracy: 0.9062 - val_loss: 0.8173 - val_accuracy: 0.9282
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7537 - accuracy: 0.8997 - val_loss: 1.0178 - val_accuracy: 0.9061
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7268 - accuracy: 0.8942 - val_loss: 0.9259 - val_accuracy: 0.9227
    Epoch 14/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6665 - accuracy: 0.9117 - val_loss: 0.9211 - val_accuracy: 0.9116
    Epoch 15/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6573 - accuracy: 0.8997 - val_loss: 1.5949 - val_accuracy: 0.8895
    Epoch 16/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8020 - accuracy: 0.9016 - val_loss: 0.8526 - val_accuracy: 0.9227
    Epoch 17/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8464 - accuracy: 0.8951 - val_loss: 1.0578 - val_accuracy: 0.9171
    Epoch 18/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7094 - accuracy: 0.9025 - val_loss: 0.8201 - val_accuracy: 0.9282
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7369 - accuracy: 0.9006 - val_loss: 0.8262 - val_accuracy: 0.9061
    Epoch 20/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7592 - accuracy: 0.8988 - val_loss: 0.7357 - val_accuracy: 0.9282
    Epoch 21/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7543 - accuracy: 0.9062 - val_loss: 0.7026 - val_accuracy: 0.9282
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7762 - accuracy: 0.9016 - val_loss: 0.7112 - val_accuracy: 0.9282
    Epoch 23/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6831 - accuracy: 0.9108 - val_loss: 0.6441 - val_accuracy: 0.9337
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8044 - accuracy: 0.9080 - val_loss: 0.5641 - val_accuracy: 0.9282
    Epoch 25/100
    9/9 [==============================] - 12s 1s/step - loss: 0.7792 - accuracy: 0.9108 - val_loss: 0.6704 - val_accuracy: 0.9227
    Epoch 26/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7035 - accuracy: 0.9200 - val_loss: 0.4856 - val_accuracy: 0.9337
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7209 - accuracy: 0.9172 - val_loss: 0.5684 - val_accuracy: 0.9282
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6347 - accuracy: 0.9108 - val_loss: 0.5434 - val_accuracy: 0.9337
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8152 - accuracy: 0.9172 - val_loss: 0.5259 - val_accuracy: 0.9337
    Epoch 30/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5457 - accuracy: 0.9273 - val_loss: 0.6242 - val_accuracy: 0.9392
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8468 - accuracy: 0.9062 - val_loss: 0.7153 - val_accuracy: 0.9392
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6340 - accuracy: 0.9227 - val_loss: 0.6281 - val_accuracy: 0.9282
    Epoch 33/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8869 - accuracy: 0.9144 - val_loss: 0.6581 - val_accuracy: 0.9006
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8859 - accuracy: 0.9181 - val_loss: 0.6228 - val_accuracy: 0.9171
    Epoch 35/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7060 - accuracy: 0.9200 - val_loss: 0.7193 - val_accuracy: 0.9171
    Epoch 36/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7594 - accuracy: 0.9200 - val_loss: 0.7289 - val_accuracy: 0.9227
    Epoch 37/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7319 - accuracy: 0.9218 - val_loss: 0.7648 - val_accuracy: 0.9171
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7669 - accuracy: 0.9264 - val_loss: 0.6782 - val_accuracy: 0.9061
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9636 - accuracy: 0.9062 - val_loss: 0.7885 - val_accuracy: 0.9116
    Epoch 40/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8585 - accuracy: 0.9135 - val_loss: 0.6633 - val_accuracy: 0.9448
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9207 - accuracy: 0.9144 - val_loss: 0.8219 - val_accuracy: 0.9061
    Epoch 42/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7233 - accuracy: 0.9255 - val_loss: 0.5884 - val_accuracy: 0.9392
    Epoch 43/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5674 - accuracy: 0.9301 - val_loss: 0.6359 - val_accuracy: 0.9227
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6879 - accuracy: 0.9255 - val_loss: 0.7843 - val_accuracy: 0.9061
    Epoch 45/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7228 - accuracy: 0.9209 - val_loss: 0.5422 - val_accuracy: 0.9061
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8812 - accuracy: 0.9089 - val_loss: 0.6568 - val_accuracy: 0.9282
    Epoch 47/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7083 - accuracy: 0.9227 - val_loss: 0.6317 - val_accuracy: 0.9282
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6108 - accuracy: 0.9384 - val_loss: 0.7857 - val_accuracy: 0.9337
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7462 - accuracy: 0.9273 - val_loss: 0.6932 - val_accuracy: 0.9337
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7988 - accuracy: 0.9190 - val_loss: 0.6338 - val_accuracy: 0.9282
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9325 - accuracy: 0.9062 - val_loss: 0.6251 - val_accuracy: 0.9006
    Epoch 52/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0457 - accuracy: 0.9016 - val_loss: 0.6937 - val_accuracy: 0.9227
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9353 - accuracy: 0.9135 - val_loss: 0.4761 - val_accuracy: 0.9558
    Epoch 54/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7133 - accuracy: 0.9255 - val_loss: 0.5222 - val_accuracy: 0.9448
    Epoch 55/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7864 - accuracy: 0.9319 - val_loss: 0.4522 - val_accuracy: 0.9337
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6843 - accuracy: 0.9347 - val_loss: 0.5868 - val_accuracy: 0.9282
    Epoch 57/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7786 - accuracy: 0.9292 - val_loss: 0.7905 - val_accuracy: 0.9006
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7740 - accuracy: 0.9292 - val_loss: 0.6890 - val_accuracy: 0.9282
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9021 - accuracy: 0.9209 - val_loss: 0.5460 - val_accuracy: 0.9227
    Epoch 60/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8702 - accuracy: 0.9135 - val_loss: 0.6559 - val_accuracy: 0.9337
    Epoch 61/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6986 - accuracy: 0.9246 - val_loss: 0.5459 - val_accuracy: 0.9392
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7062 - accuracy: 0.9328 - val_loss: 0.5337 - val_accuracy: 0.9448
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7206 - accuracy: 0.9374 - val_loss: 0.5049 - val_accuracy: 0.9448
    Epoch 64/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7421 - accuracy: 0.9319 - val_loss: 0.6178 - val_accuracy: 0.9392
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8064 - accuracy: 0.9301 - val_loss: 0.5826 - val_accuracy: 0.9392
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7876 - accuracy: 0.9246 - val_loss: 0.6433 - val_accuracy: 0.9448
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8744 - accuracy: 0.9209 - val_loss: 0.6065 - val_accuracy: 0.9392
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7580 - accuracy: 0.9292 - val_loss: 0.5852 - val_accuracy: 0.9337
    Epoch 69/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7448 - accuracy: 0.9236 - val_loss: 0.4812 - val_accuracy: 0.9282
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7853 - accuracy: 0.9236 - val_loss: 0.5472 - val_accuracy: 0.9448
    Epoch 71/100
    9/9 [==============================] - 17s 2s/step - loss: 0.8889 - accuracy: 0.9227 - val_loss: 0.4926 - val_accuracy: 0.9337
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7427 - accuracy: 0.9218 - val_loss: 0.4895 - val_accuracy: 0.9448
    Epoch 73/100
    9/9 [==============================] - 12s 1s/step - loss: 0.7469 - accuracy: 0.9328 - val_loss: 0.5271 - val_accuracy: 0.9171
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8636 - accuracy: 0.9374 - val_loss: 0.5404 - val_accuracy: 0.9282
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7597 - accuracy: 0.9200 - val_loss: 0.8502 - val_accuracy: 0.9116
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7014 - accuracy: 0.9356 - val_loss: 0.5431 - val_accuracy: 0.9282
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5913 - accuracy: 0.9374 - val_loss: 0.6891 - val_accuracy: 0.9227
    Epoch 78/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6638 - accuracy: 0.9393 - val_loss: 0.5480 - val_accuracy: 0.9448
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6563 - accuracy: 0.9374 - val_loss: 0.9340 - val_accuracy: 0.9116
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6782 - accuracy: 0.9328 - val_loss: 0.5637 - val_accuracy: 0.9392
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8157 - accuracy: 0.9365 - val_loss: 0.5829 - val_accuracy: 0.9392
    Epoch 82/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7815 - accuracy: 0.9264 - val_loss: 0.5659 - val_accuracy: 0.9503
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7803 - accuracy: 0.9273 - val_loss: 0.5734 - val_accuracy: 0.9448
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9152 - accuracy: 0.9273 - val_loss: 0.4635 - val_accuracy: 0.9503
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6955 - accuracy: 0.9356 - val_loss: 0.5216 - val_accuracy: 0.9448
    Epoch 86/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5883 - accuracy: 0.9420 - val_loss: 0.4643 - val_accuracy: 0.9448
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6271 - accuracy: 0.9301 - val_loss: 0.5444 - val_accuracy: 0.9448
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7958 - accuracy: 0.9310 - val_loss: 0.4366 - val_accuracy: 0.9448
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8535 - accuracy: 0.9338 - val_loss: 0.3861 - val_accuracy: 0.9503
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6859 - accuracy: 0.9310 - val_loss: 0.4470 - val_accuracy: 0.9282
    Epoch 91/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6377 - accuracy: 0.9384 - val_loss: 0.4702 - val_accuracy: 0.9448
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7851 - accuracy: 0.9356 - val_loss: 0.6342 - val_accuracy: 0.9337
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8491 - accuracy: 0.9319 - val_loss: 0.4924 - val_accuracy: 0.9503
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8429 - accuracy: 0.9209 - val_loss: 0.6018 - val_accuracy: 0.9448
    Epoch 95/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0042 - accuracy: 0.9282 - val_loss: 0.4521 - val_accuracy: 0.9448
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8156 - accuracy: 0.9227 - val_loss: 0.4400 - val_accuracy: 0.9392
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7009 - accuracy: 0.9384 - val_loss: 0.4631 - val_accuracy: 0.9448
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7616 - accuracy: 0.9402 - val_loss: 0.5391 - val_accuracy: 0.9448
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9593 - accuracy: 0.9255 - val_loss: 0.4955 - val_accuracy: 0.9448
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7612 - accuracy: 0.9448 - val_loss: 0.7898 - val_accuracy: 0.9392
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/27.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 31s 6s/step - loss: 1.1048 - accuracy: 0.9358

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.104784607887268, 0.9357798099517822]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9357798165137615
                  precision    recall  f1-score   support

               0       0.93      0.84      0.88       158
               1       0.94      0.97      0.96       387

        accuracy                           0.94       545
       macro avg       0.93      0.91      0.92       545
    weighted avg       0.94      0.94      0.93       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/28.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/29.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/30.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.84      0.88       158
               1       0.94      0.97      0.96       387

        accuracy                           0.94       545
       macro avg       0.93      0.91      0.92       545
    weighted avg       0.94      0.94      0.93       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.84      0.97      0.88      0.91      0.81       158
              1       0.94      0.97      0.84      0.96      0.91      0.83       387

    avg / total       0.94      0.94      0.88      0.93      0.91      0.82       545

</div>
</div>
</div>
</div>
</div>
</div>
</div>
