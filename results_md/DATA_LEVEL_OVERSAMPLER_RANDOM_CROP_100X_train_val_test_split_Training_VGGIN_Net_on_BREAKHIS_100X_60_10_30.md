<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 64
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 images belonging to 2 classes.
    Found 187 images belonging to 2 classes.
    Found 566 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.over_sampling import RandomOverSampler
from tensorflow.keras.utils import to_categorical
ros = RandomOverSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((1604, 224, 340, 3), (1604, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_val.shape, y_val.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[17\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((187, 224, 340, 3), (187, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_test.shape, y_test.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((566, 224, 340, 3), (566, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(100).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
     1/26 [&gt;.............................] - ETA: 0s - loss: 0.9106 - accuracy: 0.5469WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    26/26 [==============================] - 23s 873ms/step - loss: 2.7979 - accuracy: 0.8005 - val_loss: 63.0879 - val_accuracy: 0.2995
    Epoch 2/100
    26/26 [==============================] - 22s 843ms/step - loss: 13.7461 - accuracy: 0.7263 - val_loss: 90.9593 - val_accuracy: 0.3048
    Epoch 3/100
    26/26 [==============================] - 20s 779ms/step - loss: 27.8768 - accuracy: 0.6054 - val_loss: 33.0687 - val_accuracy: 0.4920
    Epoch 4/100
    26/26 [==============================] - 21s 803ms/step - loss: 34.5478 - accuracy: 0.5262 - val_loss: 2.8107 - val_accuracy: 0.9251
    Epoch 5/100
    26/26 [==============================] - 22s 862ms/step - loss: 20.5017 - accuracy: 0.6029 - val_loss: 2.3284 - val_accuracy: 0.9412
    Epoch 6/100
    26/26 [==============================] - 21s 799ms/step - loss: 10.0647 - accuracy: 0.7076 - val_loss: 2.1193 - val_accuracy: 0.9251
    Epoch 7/100
    26/26 [==============================] - 21s 794ms/step - loss: 10.2783 - accuracy: 0.6958 - val_loss: 2.3222 - val_accuracy: 0.8717
    Epoch 8/100
    26/26 [==============================] - 21s 807ms/step - loss: 10.3929 - accuracy: 0.6789 - val_loss: 1.8098 - val_accuracy: 0.8984
    Epoch 9/100
    26/26 [==============================] - 21s 811ms/step - loss: 11.1021 - accuracy: 0.6721 - val_loss: 2.0511 - val_accuracy: 0.8930
    Epoch 10/100
    26/26 [==============================] - 20s 781ms/step - loss: 7.5034 - accuracy: 0.7338 - val_loss: 2.0852 - val_accuracy: 0.8824
    Epoch 11/100
    26/26 [==============================] - 21s 799ms/step - loss: 5.6937 - accuracy: 0.7799 - val_loss: 2.2680 - val_accuracy: 0.8930
    Epoch 12/100
    26/26 [==============================] - 21s 796ms/step - loss: 5.8593 - accuracy: 0.7718 - val_loss: 2.3758 - val_accuracy: 0.8770
    Epoch 13/100
    26/26 [==============================] - 21s 792ms/step - loss: 5.8386 - accuracy: 0.7550 - val_loss: 2.7131 - val_accuracy: 0.8663
    Epoch 14/100
    26/26 [==============================] - 20s 788ms/step - loss: 6.5770 - accuracy: 0.7594 - val_loss: 1.5615 - val_accuracy: 0.9091
    Epoch 15/100
    26/26 [==============================] - 21s 790ms/step - loss: 4.1372 - accuracy: 0.8261 - val_loss: 2.1330 - val_accuracy: 0.8770
    Epoch 16/100
    26/26 [==============================] - 21s 811ms/step - loss: 3.7418 - accuracy: 0.8142 - val_loss: 2.8234 - val_accuracy: 0.8717
    Epoch 17/100
    26/26 [==============================] - 20s 783ms/step - loss: 4.2195 - accuracy: 0.8211 - val_loss: 2.5996 - val_accuracy: 0.8610
    Epoch 18/100
    26/26 [==============================] - 22s 853ms/step - loss: 4.2014 - accuracy: 0.8036 - val_loss: 3.6221 - val_accuracy: 0.8128
    Epoch 19/100
    26/26 [==============================] - 21s 812ms/step - loss: 4.1738 - accuracy: 0.8086 - val_loss: 2.7157 - val_accuracy: 0.8663
    Epoch 20/100
    26/26 [==============================] - 21s 789ms/step - loss: 4.0946 - accuracy: 0.8279 - val_loss: 3.3559 - val_accuracy: 0.8342
    Epoch 21/100
    26/26 [==============================] - 21s 805ms/step - loss: 4.5409 - accuracy: 0.8086 - val_loss: 4.8818 - val_accuracy: 0.7861
    Epoch 22/100
    26/26 [==============================] - 22s 829ms/step - loss: 3.3545 - accuracy: 0.8373 - val_loss: 3.3379 - val_accuracy: 0.8663
    Epoch 23/100
    26/26 [==============================] - 20s 786ms/step - loss: 3.6566 - accuracy: 0.8317 - val_loss: 3.7720 - val_accuracy: 0.8342
    Epoch 24/100
    26/26 [==============================] - 20s 784ms/step - loss: 3.9009 - accuracy: 0.8261 - val_loss: 4.3960 - val_accuracy: 0.8075
    Epoch 25/100
    26/26 [==============================] - 22s 843ms/step - loss: 3.4879 - accuracy: 0.8454 - val_loss: 2.4741 - val_accuracy: 0.8449
    Epoch 26/100
    26/26 [==============================] - 22s 852ms/step - loss: 3.0719 - accuracy: 0.8572 - val_loss: 1.7118 - val_accuracy: 0.8930
    Epoch 27/100
    26/26 [==============================] - 21s 803ms/step - loss: 3.0149 - accuracy: 0.8597 - val_loss: 2.1651 - val_accuracy: 0.8717
    Epoch 28/100
    26/26 [==============================] - 22s 857ms/step - loss: 2.0882 - accuracy: 0.8803 - val_loss: 1.6794 - val_accuracy: 0.9037
    Epoch 29/100
    26/26 [==============================] - 21s 796ms/step - loss: 2.4165 - accuracy: 0.8847 - val_loss: 2.7604 - val_accuracy: 0.8075
    Epoch 30/100
    26/26 [==============================] - 21s 824ms/step - loss: 2.5298 - accuracy: 0.8728 - val_loss: 2.5900 - val_accuracy: 0.8770
    Epoch 31/100
    26/26 [==============================] - 21s 800ms/step - loss: 2.6499 - accuracy: 0.8666 - val_loss: 2.8533 - val_accuracy: 0.8396
    Epoch 32/100
    26/26 [==============================] - 21s 806ms/step - loss: 2.7357 - accuracy: 0.8672 - val_loss: 2.3813 - val_accuracy: 0.8663
    Epoch 33/100
    26/26 [==============================] - 22s 849ms/step - loss: 2.0673 - accuracy: 0.8928 - val_loss: 3.3705 - val_accuracy: 0.7968
    Epoch 34/100
    26/26 [==============================] - 21s 792ms/step - loss: 1.6920 - accuracy: 0.9040 - val_loss: 3.1108 - val_accuracy: 0.8556
    Epoch 35/100
    26/26 [==============================] - 20s 773ms/step - loss: 1.4812 - accuracy: 0.9065 - val_loss: 3.4582 - val_accuracy: 0.8289
    Epoch 36/100
    26/26 [==============================] - 21s 814ms/step - loss: 1.9484 - accuracy: 0.9127 - val_loss: 2.3815 - val_accuracy: 0.8610
    Epoch 37/100
    26/26 [==============================] - 22s 835ms/step - loss: 1.8885 - accuracy: 0.8921 - val_loss: 3.8372 - val_accuracy: 0.8021
    Epoch 38/100
    26/26 [==============================] - 22s 857ms/step - loss: 2.1450 - accuracy: 0.8946 - val_loss: 1.4556 - val_accuracy: 0.9251
    Epoch 39/100
    26/26 [==============================] - 21s 793ms/step - loss: 1.8919 - accuracy: 0.9040 - val_loss: 1.0835 - val_accuracy: 0.9358
    Epoch 40/100
    26/26 [==============================] - 22s 831ms/step - loss: 1.5843 - accuracy: 0.9190 - val_loss: 1.6086 - val_accuracy: 0.8930
    Epoch 41/100
    26/26 [==============================] - 21s 795ms/step - loss: 2.0098 - accuracy: 0.9002 - val_loss: 5.9180 - val_accuracy: 0.7594
    Epoch 42/100
    26/26 [==============================] - 20s 786ms/step - loss: 2.2725 - accuracy: 0.9015 - val_loss: 2.1889 - val_accuracy: 0.8770
    Epoch 43/100
    26/26 [==============================] - 21s 810ms/step - loss: 1.2792 - accuracy: 0.9158 - val_loss: 2.5336 - val_accuracy: 0.8449
    Epoch 44/100
    26/26 [==============================] - 20s 783ms/step - loss: 1.9210 - accuracy: 0.9052 - val_loss: 2.1833 - val_accuracy: 0.8717
    Epoch 45/100
    26/26 [==============================] - 21s 798ms/step - loss: 1.5760 - accuracy: 0.9077 - val_loss: 2.1836 - val_accuracy: 0.8877
    Epoch 46/100
    26/26 [==============================] - 21s 804ms/step - loss: 1.5267 - accuracy: 0.9146 - val_loss: 2.1293 - val_accuracy: 0.8717
    Epoch 47/100
    26/26 [==============================] - 21s 792ms/step - loss: 1.6145 - accuracy: 0.9115 - val_loss: 1.8579 - val_accuracy: 0.8930
    Epoch 48/100
    26/26 [==============================] - 21s 799ms/step - loss: 1.3052 - accuracy: 0.9264 - val_loss: 1.6588 - val_accuracy: 0.9251
    Epoch 49/100
    26/26 [==============================] - 21s 790ms/step - loss: 1.1439 - accuracy: 0.9383 - val_loss: 2.1282 - val_accuracy: 0.8824
    Epoch 50/100
    26/26 [==============================] - 21s 805ms/step - loss: 1.4253 - accuracy: 0.9277 - val_loss: 1.0765 - val_accuracy: 0.9091
    Epoch 51/100
    26/26 [==============================] - 21s 802ms/step - loss: 1.0624 - accuracy: 0.9364 - val_loss: 1.1831 - val_accuracy: 0.9251
    Epoch 52/100
    26/26 [==============================] - 20s 785ms/step - loss: 1.0194 - accuracy: 0.9408 - val_loss: 2.9313 - val_accuracy: 0.8396
    Epoch 53/100
    26/26 [==============================] - 21s 803ms/step - loss: 1.1396 - accuracy: 0.9289 - val_loss: 1.4510 - val_accuracy: 0.9198
    Epoch 54/100
    26/26 [==============================] - 21s 794ms/step - loss: 1.6112 - accuracy: 0.9202 - val_loss: 1.1946 - val_accuracy: 0.9251
    Epoch 55/100
    26/26 [==============================] - 22s 830ms/step - loss: 1.0894 - accuracy: 0.9345 - val_loss: 1.0400 - val_accuracy: 0.9358
    Epoch 56/100
    26/26 [==============================] - 20s 787ms/step - loss: 0.8202 - accuracy: 0.9514 - val_loss: 1.3280 - val_accuracy: 0.9037
    Epoch 57/100
    26/26 [==============================] - 21s 797ms/step - loss: 0.7531 - accuracy: 0.9514 - val_loss: 2.1013 - val_accuracy: 0.8610
    Epoch 58/100
    26/26 [==============================] - 21s 796ms/step - loss: 1.1380 - accuracy: 0.9296 - val_loss: 0.8501 - val_accuracy: 0.9305
    Epoch 59/100
    26/26 [==============================] - 21s 815ms/step - loss: 0.9425 - accuracy: 0.9476 - val_loss: 1.7418 - val_accuracy: 0.9251
    Epoch 60/100
    26/26 [==============================] - 21s 800ms/step - loss: 1.1429 - accuracy: 0.9383 - val_loss: 1.5439 - val_accuracy: 0.8930
    Epoch 61/100
    26/26 [==============================] - 20s 787ms/step - loss: 1.4001 - accuracy: 0.9264 - val_loss: 1.8325 - val_accuracy: 0.8717
    Epoch 62/100
    26/26 [==============================] - 22s 836ms/step - loss: 1.7516 - accuracy: 0.9090 - val_loss: 6.3775 - val_accuracy: 0.7861
    Epoch 63/100
    26/26 [==============================] - 21s 814ms/step - loss: 1.5782 - accuracy: 0.9146 - val_loss: 2.2866 - val_accuracy: 0.8396
    Epoch 64/100
    26/26 [==============================] - 21s 794ms/step - loss: 1.3762 - accuracy: 0.9246 - val_loss: 0.8538 - val_accuracy: 0.9519
    Epoch 65/100
    26/26 [==============================] - 21s 806ms/step - loss: 1.0830 - accuracy: 0.9383 - val_loss: 0.5287 - val_accuracy: 0.9626
    Epoch 66/100
    26/26 [==============================] - 22s 840ms/step - loss: 2.0372 - accuracy: 0.9146 - val_loss: 0.8682 - val_accuracy: 0.9572
    Epoch 67/100
    26/26 [==============================] - 20s 777ms/step - loss: 1.4600 - accuracy: 0.9289 - val_loss: 0.6442 - val_accuracy: 0.9519
    Epoch 68/100
    26/26 [==============================] - 21s 822ms/step - loss: 1.1376 - accuracy: 0.9352 - val_loss: 1.3327 - val_accuracy: 0.9198
    Epoch 69/100
    26/26 [==============================] - 20s 785ms/step - loss: 1.3282 - accuracy: 0.9277 - val_loss: 1.0666 - val_accuracy: 0.9572
    Epoch 70/100
    26/26 [==============================] - 22s 859ms/step - loss: 0.7271 - accuracy: 0.9476 - val_loss: 1.9896 - val_accuracy: 0.9144
    Epoch 71/100
    26/26 [==============================] - 20s 787ms/step - loss: 0.9400 - accuracy: 0.9507 - val_loss: 0.5575 - val_accuracy: 0.9679
    Epoch 72/100
    26/26 [==============================] - 21s 803ms/step - loss: 1.0358 - accuracy: 0.9439 - val_loss: 0.8287 - val_accuracy: 0.9519
    Epoch 73/100
    26/26 [==============================] - 21s 789ms/step - loss: 1.1329 - accuracy: 0.9458 - val_loss: 0.5806 - val_accuracy: 0.9465
    Epoch 74/100
    26/26 [==============================] - 20s 785ms/step - loss: 0.9040 - accuracy: 0.9539 - val_loss: 0.8043 - val_accuracy: 0.9626
    Epoch 75/100
    26/26 [==============================] - 21s 796ms/step - loss: 0.5027 - accuracy: 0.9695 - val_loss: 1.1117 - val_accuracy: 0.9251
    Epoch 76/100
    26/26 [==============================] - 21s 802ms/step - loss: 0.9149 - accuracy: 0.9483 - val_loss: 1.4306 - val_accuracy: 0.8984
    Epoch 77/100
    26/26 [==============================] - 21s 807ms/step - loss: 1.1221 - accuracy: 0.9470 - val_loss: 3.0539 - val_accuracy: 0.8449
    Epoch 78/100
    26/26 [==============================] - 21s 793ms/step - loss: 1.0421 - accuracy: 0.9439 - val_loss: 1.0893 - val_accuracy: 0.9465
    Epoch 79/100
    26/26 [==============================] - 20s 785ms/step - loss: 1.4665 - accuracy: 0.9320 - val_loss: 5.0841 - val_accuracy: 0.8128
    Epoch 80/100
    26/26 [==============================] - 21s 803ms/step - loss: 1.5128 - accuracy: 0.9395 - val_loss: 2.6806 - val_accuracy: 0.9144
    Epoch 81/100
    26/26 [==============================] - 20s 780ms/step - loss: 1.1607 - accuracy: 0.9345 - val_loss: 2.1217 - val_accuracy: 0.9305
    Epoch 82/100
    26/26 [==============================] - 22s 845ms/step - loss: 1.2645 - accuracy: 0.9370 - val_loss: 1.9894 - val_accuracy: 0.9251
    Epoch 83/100
    26/26 [==============================] - 20s 785ms/step - loss: 1.7282 - accuracy: 0.9283 - val_loss: 1.1868 - val_accuracy: 0.9519
    Epoch 84/100
    26/26 [==============================] - 20s 776ms/step - loss: 1.4267 - accuracy: 0.9377 - val_loss: 1.3019 - val_accuracy: 0.9519
    Epoch 85/100
    26/26 [==============================] - 21s 796ms/step - loss: 0.9342 - accuracy: 0.9589 - val_loss: 1.3855 - val_accuracy: 0.9465
    Epoch 86/100
    26/26 [==============================] - 21s 809ms/step - loss: 0.6958 - accuracy: 0.9589 - val_loss: 1.1845 - val_accuracy: 0.9572
    Epoch 87/100
    26/26 [==============================] - 21s 790ms/step - loss: 0.5636 - accuracy: 0.9670 - val_loss: 1.8711 - val_accuracy: 0.9465
    Epoch 88/100
    26/26 [==============================] - 21s 794ms/step - loss: 1.0099 - accuracy: 0.9476 - val_loss: 3.4262 - val_accuracy: 0.8824
    Epoch 89/100
    26/26 [==============================] - 21s 797ms/step - loss: 3.0280 - accuracy: 0.8815 - val_loss: 5.1219 - val_accuracy: 0.8556
    Epoch 90/100
    26/26 [==============================] - 21s 799ms/step - loss: 2.4672 - accuracy: 0.9040 - val_loss: 1.0963 - val_accuracy: 0.9465
    Epoch 91/100
    26/26 [==============================] - 20s 783ms/step - loss: 1.6102 - accuracy: 0.9283 - val_loss: 1.7463 - val_accuracy: 0.9465
    Epoch 92/100
    26/26 [==============================] - 23s 865ms/step - loss: 0.8863 - accuracy: 0.9514 - val_loss: 1.6539 - val_accuracy: 0.9572
    Epoch 93/100
    26/26 [==============================] - 21s 789ms/step - loss: 1.0445 - accuracy: 0.9526 - val_loss: 1.7224 - val_accuracy: 0.9144
    Epoch 94/100
    26/26 [==============================] - 22s 855ms/step - loss: 0.8108 - accuracy: 0.9564 - val_loss: 1.9562 - val_accuracy: 0.9305
    Epoch 95/100
    26/26 [==============================] - 21s 805ms/step - loss: 0.7967 - accuracy: 0.9613 - val_loss: 1.3934 - val_accuracy: 0.9465
    Epoch 96/100
    26/26 [==============================] - 22s 829ms/step - loss: 0.5738 - accuracy: 0.9645 - val_loss: 1.6284 - val_accuracy: 0.9465
    Epoch 97/100
    26/26 [==============================] - 21s 789ms/step - loss: 0.7107 - accuracy: 0.9582 - val_loss: 1.4147 - val_accuracy: 0.9465
    Epoch 98/100
    26/26 [==============================] - 21s 796ms/step - loss: 0.4802 - accuracy: 0.9688 - val_loss: 1.2821 - val_accuracy: 0.9572
    Epoch 99/100
    26/26 [==============================] - 22s 833ms/step - loss: 0.7386 - accuracy: 0.9601 - val_loss: 1.0822 - val_accuracy: 0.9519
    Epoch 100/100
    26/26 [==============================] - 21s 801ms/step - loss: 0.6795 - accuracy: 0.9620 - val_loss: 1.2071 - val_accuracy: 0.9733

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/63.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 4s 454ms/step - loss: 1.3329 - accuracy: 0.9594

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[30\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3329219818115234, 0.9593639373779297]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9593639575971732
                  precision    recall  f1-score   support

               0       0.94      0.91      0.93       164
               1       0.97      0.98      0.97       402

        accuracy                           0.96       566
       macro avg       0.95      0.95      0.95       566
    weighted avg       0.96      0.96      0.96       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[33\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/64.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/65.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/66.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.94      0.91      0.93       164
               1       0.97      0.98      0.97       402

        accuracy                           0.96       566
       macro avg       0.95      0.95      0.95       566
    weighted avg       0.96      0.96      0.96       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.94      0.91      0.98      0.93      0.95      0.89       164
              1       0.97      0.98      0.91      0.97      0.95      0.90       402

    avg / total       0.96      0.96      0.93      0.96      0.95      0.90       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
