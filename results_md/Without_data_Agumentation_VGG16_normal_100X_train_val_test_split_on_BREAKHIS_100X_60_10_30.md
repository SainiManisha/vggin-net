<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/VGG16-NORMAL-RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    #if train:
        #ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    #ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1629 - accuracy: 0.2578WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 15s 2s/step - loss: 27.3463 - accuracy: 0.5629 - val_loss: 2.9652 - val_accuracy: 0.2888
    Epoch 2/50
    9/9 [==============================] - 7s 757ms/step - loss: 1.0687 - accuracy: 0.6915 - val_loss: 0.3652 - val_accuracy: 0.8717
    Epoch 3/50
    9/9 [==============================] - 7s 756ms/step - loss: 0.3513 - accuracy: 0.8865 - val_loss: 0.3063 - val_accuracy: 0.8877
    Epoch 4/50
    9/9 [==============================] - 7s 755ms/step - loss: 0.1708 - accuracy: 0.9353 - val_loss: 0.9334 - val_accuracy: 0.7701
    Epoch 5/50
    9/9 [==============================] - 7s 758ms/step - loss: 0.2166 - accuracy: 0.9291 - val_loss: 0.3270 - val_accuracy: 0.9198
    Epoch 6/50
    9/9 [==============================] - 7s 757ms/step - loss: 0.2168 - accuracy: 0.9388 - val_loss: 0.4705 - val_accuracy: 0.9091
    Epoch 7/50
    9/9 [==============================] - 7s 757ms/step - loss: 0.0252 - accuracy: 0.9920 - val_loss: 0.5066 - val_accuracy: 0.8930
    Epoch 8/50
    9/9 [==============================] - 7s 759ms/step - loss: 0.0095 - accuracy: 0.9956 - val_loss: 0.5042 - val_accuracy: 0.8984
    Epoch 9/50
    9/9 [==============================] - 7s 760ms/step - loss: 0.0018 - accuracy: 1.0000 - val_loss: 0.5199 - val_accuracy: 0.8984
    Epoch 10/50
    9/9 [==============================] - 7s 756ms/step - loss: 1.7763e-04 - accuracy: 1.0000 - val_loss: 0.5681 - val_accuracy: 0.9037
    Epoch 11/50
    9/9 [==============================] - 7s 758ms/step - loss: 1.1580e-04 - accuracy: 1.0000 - val_loss: 0.5557 - val_accuracy: 0.8984
    Epoch 12/50
    9/9 [==============================] - 7s 756ms/step - loss: 7.2002e-05 - accuracy: 1.0000 - val_loss: 0.5472 - val_accuracy: 0.8877
    Epoch 13/50
    9/9 [==============================] - 7s 761ms/step - loss: 5.8193e-05 - accuracy: 1.0000 - val_loss: 0.5423 - val_accuracy: 0.8930
    Epoch 14/50
    9/9 [==============================] - 7s 759ms/step - loss: 4.9048e-05 - accuracy: 1.0000 - val_loss: 0.5415 - val_accuracy: 0.8877
    Epoch 15/50
    9/9 [==============================] - 7s 756ms/step - loss: 4.4812e-05 - accuracy: 1.0000 - val_loss: 0.5416 - val_accuracy: 0.8877
    Epoch 16/50
    9/9 [==============================] - 7s 759ms/step - loss: 4.0610e-05 - accuracy: 1.0000 - val_loss: 0.5420 - val_accuracy: 0.8930
    Epoch 17/50
    9/9 [==============================] - 7s 760ms/step - loss: 3.7764e-05 - accuracy: 1.0000 - val_loss: 0.5426 - val_accuracy: 0.8930
    Epoch 18/50
    9/9 [==============================] - 7s 763ms/step - loss: 3.4816e-05 - accuracy: 1.0000 - val_loss: 0.5428 - val_accuracy: 0.8930
    Epoch 19/50
    9/9 [==============================] - 7s 758ms/step - loss: 3.2821e-05 - accuracy: 1.0000 - val_loss: 0.5430 - val_accuracy: 0.8930
    Epoch 20/50
    9/9 [==============================] - 7s 759ms/step - loss: 3.0394e-05 - accuracy: 1.0000 - val_loss: 0.5424 - val_accuracy: 0.8930
    Epoch 21/50
    9/9 [==============================] - 7s 762ms/step - loss: 2.8611e-05 - accuracy: 1.0000 - val_loss: 0.5424 - val_accuracy: 0.8930
    Epoch 22/50
    9/9 [==============================] - 7s 760ms/step - loss: 2.6852e-05 - accuracy: 1.0000 - val_loss: 0.5422 - val_accuracy: 0.8930
    Epoch 23/50
    9/9 [==============================] - 7s 762ms/step - loss: 2.5260e-05 - accuracy: 1.0000 - val_loss: 0.5416 - val_accuracy: 0.8930
    Epoch 24/50
    9/9 [==============================] - 7s 761ms/step - loss: 2.3565e-05 - accuracy: 1.0000 - val_loss: 0.5412 - val_accuracy: 0.8877
    Epoch 25/50
    9/9 [==============================] - 7s 765ms/step - loss: 2.2210e-05 - accuracy: 1.0000 - val_loss: 0.5398 - val_accuracy: 0.8877
    Epoch 26/50
    9/9 [==============================] - 7s 768ms/step - loss: 2.0598e-05 - accuracy: 1.0000 - val_loss: 0.5359 - val_accuracy: 0.8877
    Epoch 27/50
    9/9 [==============================] - 7s 762ms/step - loss: 1.8976e-05 - accuracy: 1.0000 - val_loss: 0.5308 - val_accuracy: 0.8877
    Epoch 28/50
    9/9 [==============================] - 7s 763ms/step - loss: 1.7311e-05 - accuracy: 1.0000 - val_loss: 0.5264 - val_accuracy: 0.8984
    Epoch 29/50
    9/9 [==============================] - 7s 763ms/step - loss: 1.5272e-05 - accuracy: 1.0000 - val_loss: 0.5239 - val_accuracy: 0.8984
    Epoch 30/50
    9/9 [==============================] - 7s 768ms/step - loss: 1.3768e-05 - accuracy: 1.0000 - val_loss: 0.5221 - val_accuracy: 0.8984
    Epoch 31/50
    9/9 [==============================] - 7s 760ms/step - loss: 1.2172e-05 - accuracy: 1.0000 - val_loss: 0.5223 - val_accuracy: 0.8930
    Epoch 32/50
    9/9 [==============================] - 7s 763ms/step - loss: 1.1165e-05 - accuracy: 1.0000 - val_loss: 0.5209 - val_accuracy: 0.8984
    Epoch 33/50
    9/9 [==============================] - 7s 763ms/step - loss: 9.9268e-06 - accuracy: 1.0000 - val_loss: 0.5188 - val_accuracy: 0.9037
    Epoch 34/50
    9/9 [==============================] - 7s 767ms/step - loss: 9.1067e-06 - accuracy: 1.0000 - val_loss: 0.5177 - val_accuracy: 0.9037
    Epoch 35/50
    9/9 [==============================] - 7s 761ms/step - loss: 8.3195e-06 - accuracy: 1.0000 - val_loss: 0.5175 - val_accuracy: 0.9037
    Epoch 36/50
    9/9 [==============================] - 7s 763ms/step - loss: 7.7094e-06 - accuracy: 1.0000 - val_loss: 0.5175 - val_accuracy: 0.9037
    Epoch 37/50
    9/9 [==============================] - 7s 760ms/step - loss: 7.0847e-06 - accuracy: 1.0000 - val_loss: 0.5156 - val_accuracy: 0.9037
    Epoch 38/50
    9/9 [==============================] - 7s 764ms/step - loss: 6.6343e-06 - accuracy: 1.0000 - val_loss: 0.5150 - val_accuracy: 0.9037
    Epoch 39/50
    9/9 [==============================] - 7s 761ms/step - loss: 6.1900e-06 - accuracy: 1.0000 - val_loss: 0.5160 - val_accuracy: 0.9037
    Epoch 40/50
    9/9 [==============================] - 7s 759ms/step - loss: 5.7927e-06 - accuracy: 1.0000 - val_loss: 0.5145 - val_accuracy: 0.9037
    Epoch 41/50
    9/9 [==============================] - 7s 764ms/step - loss: 5.4321e-06 - accuracy: 1.0000 - val_loss: 0.5150 - val_accuracy: 0.9037
    Epoch 42/50
    9/9 [==============================] - 7s 773ms/step - loss: 5.1079e-06 - accuracy: 1.0000 - val_loss: 0.5140 - val_accuracy: 0.9037
    Epoch 43/50
    9/9 [==============================] - 7s 764ms/step - loss: 4.8173e-06 - accuracy: 1.0000 - val_loss: 0.5137 - val_accuracy: 0.9037
    Epoch 44/50
    9/9 [==============================] - 7s 761ms/step - loss: 4.5373e-06 - accuracy: 1.0000 - val_loss: 0.5140 - val_accuracy: 0.9037
    Epoch 45/50
    9/9 [==============================] - 7s 765ms/step - loss: 4.3180e-06 - accuracy: 1.0000 - val_loss: 0.5141 - val_accuracy: 0.9037
    Epoch 46/50
    9/9 [==============================] - 7s 764ms/step - loss: 4.0624e-06 - accuracy: 1.0000 - val_loss: 0.5140 - val_accuracy: 0.9037
    Epoch 47/50
    9/9 [==============================] - 7s 759ms/step - loss: 3.8206e-06 - accuracy: 1.0000 - val_loss: 0.5131 - val_accuracy: 0.9037
    Epoch 48/50
    9/9 [==============================] - 7s 765ms/step - loss: 3.5917e-06 - accuracy: 1.0000 - val_loss: 0.5129 - val_accuracy: 0.9037
    Epoch 49/50
    9/9 [==============================] - 7s 762ms/step - loss: 3.3929e-06 - accuracy: 1.0000 - val_loss: 0.5132 - val_accuracy: 0.9037
    Epoch 50/50
    9/9 [==============================] - 7s 762ms/step - loss: 3.2258e-06 - accuracy: 1.0000 - val_loss: 0.5127 - val_accuracy: 0.9037

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/237.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 16s 3s/step - loss: 0.7794 - accuracy: 0.8905

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7793562412261963, 0.8904593586921692]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8904593639575972
                  precision    recall  f1-score   support

               0       0.84      0.77      0.80       164
               1       0.91      0.94      0.92       402

        accuracy                           0.89       566
       macro avg       0.87      0.86      0.86       566
    weighted avg       0.89      0.89      0.89       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/238.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/239.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/240.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.84      0.77      0.80       164
               1       0.91      0.94      0.92       402

        accuracy                           0.89       566
       macro avg       0.87      0.86      0.86       566
    weighted avg       0.89      0.89      0.89       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.84      0.77      0.94      0.80      0.85      0.71       164
              1       0.91      0.94      0.77      0.92      0.85      0.74       402

    avg / total       0.89      0.89      0.82      0.89      0.85      0.73       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
