<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/InceptionV1-NORMAL-RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/8 [==&gt;...........................] - ETA: 0s - loss: 0.6604 - accuracy: 0.6562WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    8/8 [==============================] - 8s 943ms/step - loss: 0.6060 - accuracy: 0.6921 - val_loss: 0.5112 - val_accuracy: 0.7453
    Epoch 2/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.5251 - accuracy: 0.7456 - val_loss: 0.4516 - val_accuracy: 0.7950
    Epoch 3/50
    8/8 [==============================] - 4s 542ms/step - loss: 0.4656 - accuracy: 0.7981 - val_loss: 0.4183 - val_accuracy: 0.8199
    Epoch 4/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.4266 - accuracy: 0.8064 - val_loss: 0.3958 - val_accuracy: 0.8323
    Epoch 5/50
    8/8 [==============================] - 4s 518ms/step - loss: 0.4182 - accuracy: 0.8198 - val_loss: 0.3815 - val_accuracy: 0.8261
    Epoch 6/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.4002 - accuracy: 0.8198 - val_loss: 0.3683 - val_accuracy: 0.8571
    Epoch 7/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.4012 - accuracy: 0.8260 - val_loss: 0.3602 - val_accuracy: 0.8447
    Epoch 8/50
    8/8 [==============================] - 4s 527ms/step - loss: 0.3605 - accuracy: 0.8548 - val_loss: 0.3513 - val_accuracy: 0.8509
    Epoch 9/50
    8/8 [==============================] - 4s 513ms/step - loss: 0.3557 - accuracy: 0.8465 - val_loss: 0.3450 - val_accuracy: 0.8509
    Epoch 10/50
    8/8 [==============================] - 4s 515ms/step - loss: 0.3499 - accuracy: 0.8579 - val_loss: 0.3393 - val_accuracy: 0.8509
    Epoch 11/50
    8/8 [==============================] - 4s 518ms/step - loss: 0.3360 - accuracy: 0.8630 - val_loss: 0.3335 - val_accuracy: 0.8571
    Epoch 12/50
    8/8 [==============================] - 4s 519ms/step - loss: 0.3480 - accuracy: 0.8620 - val_loss: 0.3282 - val_accuracy: 0.8634
    Epoch 13/50
    8/8 [==============================] - 4s 520ms/step - loss: 0.3352 - accuracy: 0.8568 - val_loss: 0.3239 - val_accuracy: 0.8696
    Epoch 14/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.3257 - accuracy: 0.8754 - val_loss: 0.3197 - val_accuracy: 0.8758
    Epoch 15/50
    8/8 [==============================] - 4s 510ms/step - loss: 0.3266 - accuracy: 0.8599 - val_loss: 0.3179 - val_accuracy: 0.8696
    Epoch 16/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.3252 - accuracy: 0.8599 - val_loss: 0.3135 - val_accuracy: 0.8696
    Epoch 17/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.3012 - accuracy: 0.8713 - val_loss: 0.3126 - val_accuracy: 0.8696
    Epoch 18/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.3058 - accuracy: 0.8774 - val_loss: 0.3049 - val_accuracy: 0.8758
    Epoch 19/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.3123 - accuracy: 0.8795 - val_loss: 0.3055 - val_accuracy: 0.8696
    Epoch 20/50
    8/8 [==============================] - 4s 524ms/step - loss: 0.2970 - accuracy: 0.8826 - val_loss: 0.3016 - val_accuracy: 0.8696
    Epoch 21/50
    8/8 [==============================] - 4s 521ms/step - loss: 0.3001 - accuracy: 0.8785 - val_loss: 0.2983 - val_accuracy: 0.8758
    Epoch 22/50
    8/8 [==============================] - 4s 526ms/step - loss: 0.3022 - accuracy: 0.8713 - val_loss: 0.2964 - val_accuracy: 0.8820
    Epoch 23/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.2893 - accuracy: 0.8847 - val_loss: 0.2963 - val_accuracy: 0.8758
    Epoch 24/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.3042 - accuracy: 0.8795 - val_loss: 0.2985 - val_accuracy: 0.8696
    Epoch 25/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.2770 - accuracy: 0.9001 - val_loss: 0.2920 - val_accuracy: 0.8820
    Epoch 26/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.2827 - accuracy: 0.8826 - val_loss: 0.2959 - val_accuracy: 0.8758
    Epoch 27/50
    8/8 [==============================] - 4s 524ms/step - loss: 0.2679 - accuracy: 0.8774 - val_loss: 0.2895 - val_accuracy: 0.8820
    Epoch 28/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.2632 - accuracy: 0.9042 - val_loss: 0.2920 - val_accuracy: 0.8696
    Epoch 29/50
    8/8 [==============================] - 4s 528ms/step - loss: 0.2793 - accuracy: 0.8970 - val_loss: 0.2884 - val_accuracy: 0.8758
    Epoch 30/50
    8/8 [==============================] - 4s 526ms/step - loss: 0.2597 - accuracy: 0.8929 - val_loss: 0.2882 - val_accuracy: 0.8758
    Epoch 31/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.2588 - accuracy: 0.8980 - val_loss: 0.2853 - val_accuracy: 0.8820
    Epoch 32/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.2790 - accuracy: 0.8970 - val_loss: 0.2852 - val_accuracy: 0.8820
    Epoch 33/50
    8/8 [==============================] - 4s 521ms/step - loss: 0.2481 - accuracy: 0.9022 - val_loss: 0.2833 - val_accuracy: 0.8820
    Epoch 34/50
    8/8 [==============================] - 4s 524ms/step - loss: 0.2668 - accuracy: 0.8960 - val_loss: 0.2816 - val_accuracy: 0.8820
    Epoch 35/50
    8/8 [==============================] - 4s 536ms/step - loss: 0.2580 - accuracy: 0.9032 - val_loss: 0.2824 - val_accuracy: 0.8820
    Epoch 36/50
    8/8 [==============================] - 4s 528ms/step - loss: 0.2523 - accuracy: 0.9083 - val_loss: 0.2799 - val_accuracy: 0.8882
    Epoch 37/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.2433 - accuracy: 0.9022 - val_loss: 0.2806 - val_accuracy: 0.8882
    Epoch 38/50
    8/8 [==============================] - 4s 529ms/step - loss: 0.2554 - accuracy: 0.8908 - val_loss: 0.2811 - val_accuracy: 0.8820
    Epoch 39/50
    8/8 [==============================] - 4s 527ms/step - loss: 0.2608 - accuracy: 0.8991 - val_loss: 0.2809 - val_accuracy: 0.8820
    Epoch 40/50
    8/8 [==============================] - 4s 523ms/step - loss: 0.2490 - accuracy: 0.9022 - val_loss: 0.2767 - val_accuracy: 0.8820
    Epoch 41/50
    8/8 [==============================] - 4s 527ms/step - loss: 0.2395 - accuracy: 0.9073 - val_loss: 0.2837 - val_accuracy: 0.8882
    Epoch 42/50
    8/8 [==============================] - 4s 526ms/step - loss: 0.2579 - accuracy: 0.9042 - val_loss: 0.2772 - val_accuracy: 0.8882
    Epoch 43/50
    8/8 [==============================] - 4s 526ms/step - loss: 0.2437 - accuracy: 0.9042 - val_loss: 0.2816 - val_accuracy: 0.8944
    Epoch 44/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.2449 - accuracy: 0.8991 - val_loss: 0.2773 - val_accuracy: 0.8882
    Epoch 45/50
    8/8 [==============================] - 4s 522ms/step - loss: 0.2391 - accuracy: 0.9042 - val_loss: 0.2784 - val_accuracy: 0.8882
    Epoch 46/50
    8/8 [==============================] - 4s 519ms/step - loss: 0.2366 - accuracy: 0.9135 - val_loss: 0.2765 - val_accuracy: 0.8882
    Epoch 47/50
    8/8 [==============================] - 4s 526ms/step - loss: 0.2414 - accuracy: 0.9083 - val_loss: 0.2771 - val_accuracy: 0.8882
    Epoch 48/50
    8/8 [==============================] - 4s 525ms/step - loss: 0.2274 - accuracy: 0.9104 - val_loss: 0.2773 - val_accuracy: 0.8882
    Epoch 49/50
    8/8 [==============================] - 4s 518ms/step - loss: 0.2467 - accuracy: 0.9022 - val_loss: 0.2746 - val_accuracy: 0.8820
    Epoch 50/50
    8/8 [==============================] - 4s 524ms/step - loss: 0.2477 - accuracy: 0.9042 - val_loss: 0.2743 - val_accuracy: 0.8820

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/99.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 18s 5s/step - loss: 0.2934 - accuracy: 0.8832

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.29340746998786926, 0.8831967115402222]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8831967213114754
                  precision    recall  f1-score   support

               0       0.86      0.73      0.79       148
               1       0.89      0.95      0.92       340

        accuracy                           0.88       488
       macro avg       0.88      0.84      0.86       488
    weighted avg       0.88      0.88      0.88       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/100.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/101.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/102.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.86      0.73      0.79       148
               1       0.89      0.95      0.92       340

        accuracy                           0.88       488
       macro avg       0.88      0.84      0.86       488
    weighted avg       0.88      0.88      0.88       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.86      0.73      0.95      0.79      0.83      0.68       148
              1       0.89      0.95      0.73      0.92      0.83      0.71       340

    avg / total       0.88      0.88      0.80      0.88      0.83      0.70       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
