<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block3_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    =================================================================
    Total params: 1,735,488
    Trainable params: 0
    Non-trainable params: 1,735,488
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 28, 28, 256)  1735488     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 28, 28, 64)   16448       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 28, 28, 128)  295040      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 28, 28, 32)   204832      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 28, 28, 256)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 28, 28, 480)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 28, 28, 480)  1920        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 376320)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 376320)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            752642      Dropout[0][0]                    
    ==================================================================================================
    Total params: 3,006,370
    Trainable params: 1,269,922
    Non-trainable params: 1,736,448
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00block3RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    mkdir: cannot create directory ‘./experiments/00block3RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet’: File exists

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.6362 - accuracy: 0.3750WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 16s 2s/step - loss: 9.3087 - accuracy: 0.7019 - val_loss: 52.2409 - val_accuracy: 0.7933
    Epoch 2/100
    9/9 [==============================] - 12s 1s/step - loss: 5.7087 - accuracy: 0.8245 - val_loss: 16.0509 - val_accuracy: 0.8268
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 4.5266 - accuracy: 0.8542 - val_loss: 12.2016 - val_accuracy: 0.8603
    Epoch 4/100
    9/9 [==============================] - 16s 2s/step - loss: 3.0913 - accuracy: 0.8728 - val_loss: 7.0109 - val_accuracy: 0.8883
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 2.9238 - accuracy: 0.8737 - val_loss: 4.8048 - val_accuracy: 0.8827
    Epoch 6/100
    9/9 [==============================] - 9s 1s/step - loss: 2.8079 - accuracy: 0.8663 - val_loss: 11.1525 - val_accuracy: 0.8324
    Epoch 7/100
    9/9 [==============================] - 13s 1s/step - loss: 3.7607 - accuracy: 0.8552 - val_loss: 5.0877 - val_accuracy: 0.8771
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 2.3846 - accuracy: 0.8997 - val_loss: 9.9475 - val_accuracy: 0.8101
    Epoch 9/100
    9/9 [==============================] - 13s 1s/step - loss: 2.3893 - accuracy: 0.8914 - val_loss: 5.2050 - val_accuracy: 0.8939
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 2.1129 - accuracy: 0.8886 - val_loss: 3.9815 - val_accuracy: 0.9106
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 2.1610 - accuracy: 0.8960 - val_loss: 10.6116 - val_accuracy: 0.8380
    Epoch 12/100
    9/9 [==============================] - 12s 1s/step - loss: 2.1835 - accuracy: 0.9081 - val_loss: 3.5882 - val_accuracy: 0.8994
    Epoch 13/100
    9/9 [==============================] - 13s 1s/step - loss: 1.8394 - accuracy: 0.9071 - val_loss: 3.2459 - val_accuracy: 0.8994
    Epoch 14/100
    9/9 [==============================] - 9s 1s/step - loss: 2.0432 - accuracy: 0.9183 - val_loss: 4.5122 - val_accuracy: 0.8436
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 2.2032 - accuracy: 0.8969 - val_loss: 4.3206 - val_accuracy: 0.9106
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 2.1759 - accuracy: 0.9127 - val_loss: 8.0760 - val_accuracy: 0.8492
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 1.5713 - accuracy: 0.9331 - val_loss: 9.6184 - val_accuracy: 0.8436
    Epoch 18/100
    9/9 [==============================] - 12s 1s/step - loss: 1.5591 - accuracy: 0.9285 - val_loss: 2.4918 - val_accuracy: 0.9274
    Epoch 19/100
    9/9 [==============================] - 9s 1s/step - loss: 1.3720 - accuracy: 0.9424 - val_loss: 2.4455 - val_accuracy: 0.9385
    Epoch 20/100
    9/9 [==============================] - 10s 1s/step - loss: 1.4317 - accuracy: 0.9378 - val_loss: 4.2278 - val_accuracy: 0.9274
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2113 - accuracy: 0.9369 - val_loss: 1.8088 - val_accuracy: 0.9441
    Epoch 22/100
    9/9 [==============================] - 9s 1s/step - loss: 1.1809 - accuracy: 0.9359 - val_loss: 2.5976 - val_accuracy: 0.9385
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 1.6742 - accuracy: 0.9257 - val_loss: 4.3118 - val_accuracy: 0.8939
    Epoch 24/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4109 - accuracy: 0.9322 - val_loss: 6.5386 - val_accuracy: 0.8883
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3356 - accuracy: 0.9471 - val_loss: 4.2004 - val_accuracy: 0.8883
    Epoch 26/100
    9/9 [==============================] - 13s 1s/step - loss: 1.8157 - accuracy: 0.9331 - val_loss: 3.3871 - val_accuracy: 0.9050
    Epoch 27/100
    9/9 [==============================] - 13s 1s/step - loss: 1.5277 - accuracy: 0.9313 - val_loss: 6.6886 - val_accuracy: 0.8883
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3749 - accuracy: 0.9424 - val_loss: 3.9391 - val_accuracy: 0.9218
    Epoch 29/100
    9/9 [==============================] - 13s 1s/step - loss: 1.7545 - accuracy: 0.9313 - val_loss: 2.6059 - val_accuracy: 0.9274
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 1.1142 - accuracy: 0.9517 - val_loss: 3.6349 - val_accuracy: 0.9162
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9495 - accuracy: 0.9573 - val_loss: 3.5166 - val_accuracy: 0.9330
    Epoch 32/100
    9/9 [==============================] - 9s 1s/step - loss: 1.0689 - accuracy: 0.9517 - val_loss: 5.9338 - val_accuracy: 0.9106
    Epoch 33/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8713 - accuracy: 0.9619 - val_loss: 2.9306 - val_accuracy: 0.9050
    Epoch 34/100
    9/9 [==============================] - 9s 1s/step - loss: 1.4532 - accuracy: 0.9489 - val_loss: 5.3909 - val_accuracy: 0.9106
    Epoch 35/100
    9/9 [==============================] - 9s 1s/step - loss: 1.9240 - accuracy: 0.9350 - val_loss: 1.8290 - val_accuracy: 0.9385
    Epoch 36/100
    9/9 [==============================] - 12s 1s/step - loss: 1.0933 - accuracy: 0.9517 - val_loss: 2.3541 - val_accuracy: 0.9441
    Epoch 37/100
    9/9 [==============================] - 9s 1s/step - loss: 1.6083 - accuracy: 0.9508 - val_loss: 2.3203 - val_accuracy: 0.9441
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9836 - accuracy: 0.9619 - val_loss: 6.4763 - val_accuracy: 0.8603
    Epoch 39/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2495 - accuracy: 0.9545 - val_loss: 1.7822 - val_accuracy: 0.9665
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4544 - accuracy: 0.9480 - val_loss: 2.3729 - val_accuracy: 0.9497
    Epoch 41/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9682 - accuracy: 0.9638 - val_loss: 1.8669 - val_accuracy: 0.9441
    Epoch 42/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8344 - accuracy: 0.9656 - val_loss: 3.2789 - val_accuracy: 0.9162
    Epoch 43/100
    9/9 [==============================] - 12s 1s/step - loss: 1.0060 - accuracy: 0.9573 - val_loss: 2.2718 - val_accuracy: 0.9218
    Epoch 44/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8150 - accuracy: 0.9684 - val_loss: 3.7689 - val_accuracy: 0.9274
    Epoch 45/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6383 - accuracy: 0.9694 - val_loss: 2.2550 - val_accuracy: 0.9385
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8235 - accuracy: 0.9694 - val_loss: 1.7212 - val_accuracy: 0.9385
    Epoch 47/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9150 - accuracy: 0.9601 - val_loss: 4.2662 - val_accuracy: 0.9274
    Epoch 48/100
    9/9 [==============================] - 13s 1s/step - loss: 1.1192 - accuracy: 0.9647 - val_loss: 3.2201 - val_accuracy: 0.9385
    Epoch 49/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9966 - accuracy: 0.9712 - val_loss: 2.9654 - val_accuracy: 0.9218
    Epoch 50/100
    9/9 [==============================] - 12s 1s/step - loss: 0.5045 - accuracy: 0.9749 - val_loss: 2.1616 - val_accuracy: 0.9553
    Epoch 51/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8538 - accuracy: 0.9721 - val_loss: 2.2633 - val_accuracy: 0.9609
    Epoch 52/100
    9/9 [==============================] - 12s 1s/step - loss: 0.8971 - accuracy: 0.9759 - val_loss: 1.8189 - val_accuracy: 0.9330
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0245 - accuracy: 0.9694 - val_loss: 2.8731 - val_accuracy: 0.9553
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8733 - accuracy: 0.9601 - val_loss: 2.7753 - val_accuracy: 0.9497
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8847 - accuracy: 0.9629 - val_loss: 2.2570 - val_accuracy: 0.9441
    Epoch 56/100
    9/9 [==============================] - 9s 1s/step - loss: 1.0167 - accuracy: 0.9647 - val_loss: 2.2246 - val_accuracy: 0.9385
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 1.1008 - accuracy: 0.9554 - val_loss: 2.3387 - val_accuracy: 0.9497
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6228 - accuracy: 0.9712 - val_loss: 2.6176 - val_accuracy: 0.9385
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4697 - accuracy: 0.9749 - val_loss: 2.8407 - val_accuracy: 0.9553
    Epoch 60/100
    9/9 [==============================] - 12s 1s/step - loss: 0.5007 - accuracy: 0.9749 - val_loss: 2.3002 - val_accuracy: 0.9497
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5485 - accuracy: 0.9749 - val_loss: 3.1375 - val_accuracy: 0.9441
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5682 - accuracy: 0.9731 - val_loss: 2.6389 - val_accuracy: 0.9441
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8249 - accuracy: 0.9694 - val_loss: 3.6239 - val_accuracy: 0.9553
    Epoch 64/100
    9/9 [==============================] - 9s 987ms/step - loss: 0.5150 - accuracy: 0.9731 - val_loss: 6.5850 - val_accuracy: 0.9274
    Epoch 65/100
    9/9 [==============================] - 9s 988ms/step - loss: 0.5701 - accuracy: 0.9759 - val_loss: 3.5421 - val_accuracy: 0.9385
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7785 - accuracy: 0.9656 - val_loss: 4.1919 - val_accuracy: 0.9162
    Epoch 67/100
    9/9 [==============================] - 12s 1s/step - loss: 0.8180 - accuracy: 0.9740 - val_loss: 6.6589 - val_accuracy: 0.9330
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6990 - accuracy: 0.9759 - val_loss: 6.2847 - val_accuracy: 0.9385
    Epoch 69/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4998 - accuracy: 0.9786 - val_loss: 7.6163 - val_accuracy: 0.9106
    Epoch 70/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0186 - accuracy: 0.9703 - val_loss: 3.4448 - val_accuracy: 0.9330
    Epoch 71/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8746 - accuracy: 0.9721 - val_loss: 3.5239 - val_accuracy: 0.9330
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0915 - accuracy: 0.9647 - val_loss: 1.9534 - val_accuracy: 0.9497
    Epoch 73/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5135 - accuracy: 0.9731 - val_loss: 3.4995 - val_accuracy: 0.9497
    Epoch 74/100
    9/9 [==============================] - 9s 1s/step - loss: 1.3765 - accuracy: 0.9656 - val_loss: 1.7772 - val_accuracy: 0.9609
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7855 - accuracy: 0.9703 - val_loss: 2.1245 - val_accuracy: 0.9609
    Epoch 76/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2605 - accuracy: 0.9851 - val_loss: 4.7870 - val_accuracy: 0.9106
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7054 - accuracy: 0.9731 - val_loss: 2.0809 - val_accuracy: 0.9609
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6901 - accuracy: 0.9731 - val_loss: 2.0057 - val_accuracy: 0.9609
    Epoch 79/100
    9/9 [==============================] - 9s 985ms/step - loss: 0.5431 - accuracy: 0.9842 - val_loss: 2.8973 - val_accuracy: 0.9218
    Epoch 80/100
    9/9 [==============================] - 12s 1s/step - loss: 0.8714 - accuracy: 0.9675 - val_loss: 3.9554 - val_accuracy: 0.9330
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9625 - accuracy: 0.9638 - val_loss: 2.4155 - val_accuracy: 0.9665
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2343 - accuracy: 0.9675 - val_loss: 1.8280 - val_accuracy: 0.9553
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7675 - accuracy: 0.9684 - val_loss: 4.2705 - val_accuracy: 0.9106
    Epoch 84/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4651 - accuracy: 0.9759 - val_loss: 3.7778 - val_accuracy: 0.9330
    Epoch 85/100
    9/9 [==============================] - 9s 1s/step - loss: 0.9501 - accuracy: 0.9656 - val_loss: 3.5804 - val_accuracy: 0.9385
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6590 - accuracy: 0.9768 - val_loss: 6.4643 - val_accuracy: 0.8939
    Epoch 87/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5829 - accuracy: 0.9805 - val_loss: 7.0537 - val_accuracy: 0.9274
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4470 - accuracy: 0.9805 - val_loss: 6.7770 - val_accuracy: 0.9330
    Epoch 89/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5279 - accuracy: 0.9805 - val_loss: 5.5936 - val_accuracy: 0.9385
    Epoch 90/100
    9/9 [==============================] - 16s 2s/step - loss: 0.7038 - accuracy: 0.9796 - val_loss: 6.0661 - val_accuracy: 0.9330
    Epoch 91/100
    9/9 [==============================] - 9s 1s/step - loss: 1.3016 - accuracy: 0.9573 - val_loss: 16.9269 - val_accuracy: 0.8659
    Epoch 92/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8822 - accuracy: 0.9749 - val_loss: 6.2598 - val_accuracy: 0.8939
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8049 - accuracy: 0.9629 - val_loss: 15.9904 - val_accuracy: 0.8045
    Epoch 94/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6578 - accuracy: 0.9768 - val_loss: 4.3505 - val_accuracy: 0.9218
    Epoch 95/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3763 - accuracy: 0.9842 - val_loss: 6.5688 - val_accuracy: 0.9330
    Epoch 96/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6333 - accuracy: 0.9805 - val_loss: 8.3744 - val_accuracy: 0.8883
    Epoch 97/100
    9/9 [==============================] - 12s 1s/step - loss: 0.5615 - accuracy: 0.9786 - val_loss: 8.5597 - val_accuracy: 0.8994
    Epoch 98/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8073 - accuracy: 0.9731 - val_loss: 5.8476 - val_accuracy: 0.9162
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9761 - accuracy: 0.9777 - val_loss: 4.5158 - val_accuracy: 0.9162
    Epoch 100/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6405 - accuracy: 0.9833 - val_loss: 3.7901 - val_accuracy: 0.9385
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/263.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 13s 3s/step - loss: 2.3310 - accuracy: 0.9536

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.3309850692749023, 0.9536178112030029]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9536178107606679
                  precision    recall  f1-score   support

               0       0.92      0.92      0.92       158
               1       0.97      0.97      0.97       381

        accuracy                           0.95       539
       macro avg       0.94      0.94      0.94       539
    weighted avg       0.95      0.95      0.95       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/264.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/265.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/266.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.92      0.92      0.92       158
               1       0.97      0.97      0.97       381

        accuracy                           0.95       539
       macro avg       0.94      0.94      0.94       539
    weighted avg       0.95      0.95      0.95       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.92      0.92      0.97      0.92      0.94      0.88       158
              1       0.97      0.97      0.92      0.97      0.94      0.89       381

    avg / total       0.95      0.95      0.93      0.95      0.94      0.89       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
