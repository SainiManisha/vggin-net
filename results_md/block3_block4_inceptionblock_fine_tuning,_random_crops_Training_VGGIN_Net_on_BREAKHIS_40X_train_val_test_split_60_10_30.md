<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK3and4-FINETUNING_40X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.4069 - accuracy: 0.4766WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 5.6077 - accuracy: 0.7205 - val_loss: 12.4218 - val_accuracy: 0.8045
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 2.4165 - accuracy: 0.8459 - val_loss: 4.8590 - val_accuracy: 0.8603
    Epoch 3/100
    9/9 [==============================] - 14s 2s/step - loss: 1.8096 - accuracy: 0.8644 - val_loss: 2.7457 - val_accuracy: 0.8939
    Epoch 4/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0521 - accuracy: 0.8951 - val_loss: 2.2743 - val_accuracy: 0.8827
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0140 - accuracy: 0.9025 - val_loss: 1.3902 - val_accuracy: 0.9274
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6498 - accuracy: 0.9211 - val_loss: 0.8975 - val_accuracy: 0.9330
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7723 - accuracy: 0.9174 - val_loss: 3.4309 - val_accuracy: 0.8603
    Epoch 8/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6670 - accuracy: 0.9304 - val_loss: 0.9129 - val_accuracy: 0.9441
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5716 - accuracy: 0.9452 - val_loss: 2.5965 - val_accuracy: 0.8883
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6500 - accuracy: 0.9331 - val_loss: 0.5488 - val_accuracy: 0.9441
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7436 - accuracy: 0.9304 - val_loss: 0.5647 - val_accuracy: 0.9497
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4431 - accuracy: 0.9424 - val_loss: 1.1137 - val_accuracy: 0.9441
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4227 - accuracy: 0.9461 - val_loss: 0.9468 - val_accuracy: 0.9553
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7276 - accuracy: 0.9369 - val_loss: 1.2483 - val_accuracy: 0.9162
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6394 - accuracy: 0.9508 - val_loss: 1.2371 - val_accuracy: 0.9218
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8540 - accuracy: 0.9294 - val_loss: 2.2912 - val_accuracy: 0.8994
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5025 - accuracy: 0.9573 - val_loss: 1.2186 - val_accuracy: 0.9330
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4437 - accuracy: 0.9545 - val_loss: 0.7852 - val_accuracy: 0.9609
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4892 - accuracy: 0.9564 - val_loss: 1.9420 - val_accuracy: 0.9330
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5278 - accuracy: 0.9591 - val_loss: 1.5044 - val_accuracy: 0.8994
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5528 - accuracy: 0.9554 - val_loss: 0.9532 - val_accuracy: 0.9609
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5041 - accuracy: 0.9601 - val_loss: 1.1277 - val_accuracy: 0.9441
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4912 - accuracy: 0.9675 - val_loss: 0.7408 - val_accuracy: 0.9553
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5367 - accuracy: 0.9591 - val_loss: 0.6200 - val_accuracy: 0.9441
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2974 - accuracy: 0.9731 - val_loss: 0.6764 - val_accuracy: 0.9497
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5313 - accuracy: 0.9619 - val_loss: 1.6901 - val_accuracy: 0.9274
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5520 - accuracy: 0.9656 - val_loss: 1.3490 - val_accuracy: 0.9553
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5866 - accuracy: 0.9601 - val_loss: 1.8389 - val_accuracy: 0.9330
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6352 - accuracy: 0.9610 - val_loss: 1.4980 - val_accuracy: 0.9553
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3804 - accuracy: 0.9647 - val_loss: 1.3939 - val_accuracy: 0.9441
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4367 - accuracy: 0.9675 - val_loss: 1.1415 - val_accuracy: 0.9665
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1362 - accuracy: 0.9805 - val_loss: 0.9947 - val_accuracy: 0.9553
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4315 - accuracy: 0.9684 - val_loss: 0.9877 - val_accuracy: 0.9609
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2284 - accuracy: 0.9777 - val_loss: 1.1908 - val_accuracy: 0.9609
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3641 - accuracy: 0.9721 - val_loss: 1.4296 - val_accuracy: 0.9553
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3154 - accuracy: 0.9786 - val_loss: 1.4134 - val_accuracy: 0.9497
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4250 - accuracy: 0.9749 - val_loss: 1.0211 - val_accuracy: 0.9385
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2764 - accuracy: 0.9796 - val_loss: 0.9888 - val_accuracy: 0.9553
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2349 - accuracy: 0.9749 - val_loss: 2.2738 - val_accuracy: 0.9274
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3586 - accuracy: 0.9777 - val_loss: 0.9787 - val_accuracy: 0.9497
    Epoch 41/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4751 - accuracy: 0.9712 - val_loss: 1.7405 - val_accuracy: 0.9441
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3742 - accuracy: 0.9712 - val_loss: 1.8636 - val_accuracy: 0.9441
    Epoch 43/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1968 - accuracy: 0.9796 - val_loss: 1.5288 - val_accuracy: 0.9497
    Epoch 44/100
    9/9 [==============================] - 17s 2s/step - loss: 0.1189 - accuracy: 0.9898 - val_loss: 1.7775 - val_accuracy: 0.9162
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3283 - accuracy: 0.9749 - val_loss: 1.5474 - val_accuracy: 0.9553
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2989 - accuracy: 0.9759 - val_loss: 1.7216 - val_accuracy: 0.9497
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2870 - accuracy: 0.9796 - val_loss: 1.1932 - val_accuracy: 0.9441
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2955 - accuracy: 0.9796 - val_loss: 1.1534 - val_accuracy: 0.9497
    Epoch 49/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4033 - accuracy: 0.9833 - val_loss: 2.2421 - val_accuracy: 0.9330
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3636 - accuracy: 0.9768 - val_loss: 1.6007 - val_accuracy: 0.9385
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3646 - accuracy: 0.9740 - val_loss: 1.4717 - val_accuracy: 0.9441
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3921 - accuracy: 0.9759 - val_loss: 1.6585 - val_accuracy: 0.9330
    Epoch 53/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2719 - accuracy: 0.9824 - val_loss: 1.5265 - val_accuracy: 0.9553
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2750 - accuracy: 0.9814 - val_loss: 1.5909 - val_accuracy: 0.9497
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1805 - accuracy: 0.9851 - val_loss: 1.2669 - val_accuracy: 0.9609
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2762 - accuracy: 0.9759 - val_loss: 1.2689 - val_accuracy: 0.9497
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3728 - accuracy: 0.9721 - val_loss: 0.9924 - val_accuracy: 0.9609
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3452 - accuracy: 0.9796 - val_loss: 1.0983 - val_accuracy: 0.9609
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3744 - accuracy: 0.9777 - val_loss: 0.7388 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2646 - accuracy: 0.9759 - val_loss: 0.8171 - val_accuracy: 0.9553
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2310 - accuracy: 0.9824 - val_loss: 0.8915 - val_accuracy: 0.9497
    Epoch 62/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3599 - accuracy: 0.9805 - val_loss: 1.0275 - val_accuracy: 0.9553
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3594 - accuracy: 0.9759 - val_loss: 1.1300 - val_accuracy: 0.9609
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1974 - accuracy: 0.9833 - val_loss: 1.7517 - val_accuracy: 0.9497
    Epoch 65/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2931 - accuracy: 0.9833 - val_loss: 1.7143 - val_accuracy: 0.9553
    Epoch 66/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3724 - accuracy: 0.9805 - val_loss: 1.8862 - val_accuracy: 0.9441
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2052 - accuracy: 0.9824 - val_loss: 1.8713 - val_accuracy: 0.9218
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4223 - accuracy: 0.9759 - val_loss: 1.3409 - val_accuracy: 0.9497
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2346 - accuracy: 0.9870 - val_loss: 1.4305 - val_accuracy: 0.9497
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3115 - accuracy: 0.9861 - val_loss: 1.1718 - val_accuracy: 0.9497
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1146 - accuracy: 0.9907 - val_loss: 1.3622 - val_accuracy: 0.9609
    Epoch 72/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1631 - accuracy: 0.9861 - val_loss: 2.4136 - val_accuracy: 0.9385
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2125 - accuracy: 0.9870 - val_loss: 2.5899 - val_accuracy: 0.9330
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2190 - accuracy: 0.9879 - val_loss: 2.2500 - val_accuracy: 0.9385
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0497 - accuracy: 0.9944 - val_loss: 1.7238 - val_accuracy: 0.9497
    Epoch 76/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2320 - accuracy: 0.9879 - val_loss: 1.4309 - val_accuracy: 0.9497
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1833 - accuracy: 0.9898 - val_loss: 1.2077 - val_accuracy: 0.9497
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1402 - accuracy: 0.9879 - val_loss: 1.2854 - val_accuracy: 0.9553
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1999 - accuracy: 0.9824 - val_loss: 0.7212 - val_accuracy: 0.9721
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2168 - accuracy: 0.9814 - val_loss: 1.8164 - val_accuracy: 0.9497
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2325 - accuracy: 0.9861 - val_loss: 1.3543 - val_accuracy: 0.9497
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1574 - accuracy: 0.9907 - val_loss: 1.6933 - val_accuracy: 0.9497
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2134 - accuracy: 0.9861 - val_loss: 1.9842 - val_accuracy: 0.9441
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1210 - accuracy: 0.9916 - val_loss: 3.4259 - val_accuracy: 0.9274
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3296 - accuracy: 0.9805 - val_loss: 1.1998 - val_accuracy: 0.9665
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2835 - accuracy: 0.9796 - val_loss: 1.3204 - val_accuracy: 0.9497
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4274 - accuracy: 0.9814 - val_loss: 3.0524 - val_accuracy: 0.9441
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3827 - accuracy: 0.9786 - val_loss: 1.2513 - val_accuracy: 0.9721
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3460 - accuracy: 0.9824 - val_loss: 1.1308 - val_accuracy: 0.9721
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1761 - accuracy: 0.9851 - val_loss: 1.8221 - val_accuracy: 0.9609
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2566 - accuracy: 0.9805 - val_loss: 4.0542 - val_accuracy: 0.9274
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4750 - accuracy: 0.9805 - val_loss: 2.4257 - val_accuracy: 0.9609
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3435 - accuracy: 0.9796 - val_loss: 1.5170 - val_accuracy: 0.9497
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1682 - accuracy: 0.9879 - val_loss: 1.5052 - val_accuracy: 0.9441
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1048 - accuracy: 0.9907 - val_loss: 1.7274 - val_accuracy: 0.9609
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1057 - accuracy: 0.9907 - val_loss: 2.0134 - val_accuracy: 0.9609
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2745 - accuracy: 0.9842 - val_loss: 1.6475 - val_accuracy: 0.9553
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1877 - accuracy: 0.9898 - val_loss: 2.6729 - val_accuracy: 0.9497
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1253 - accuracy: 0.9907 - val_loss: 2.4121 - val_accuracy: 0.9497
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1137 - accuracy: 0.9935 - val_loss: 1.7049 - val_accuracy: 0.9497

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/217.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 14s 3s/step - loss: 1.5212 - accuracy: 0.9647

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.521205186843872, 0.9647495150566101]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,697,506
    Non-trainable params: 261,632
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/218.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 128, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    9/9 [==============================] - 15s 2s/step - loss: 0.3156 - accuracy: 0.9861 - val_loss: 3.3084 - val_accuracy: 0.9497

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0289 - accuracy: 0.9954 - val_loss: 2.1086 - val_accuracy: 0.9441

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2609 - accuracy: 0.9870 - val_loss: 2.6036 - val_accuracy: 0.9497

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1363 - accuracy: 0.9916 - val_loss: 3.3723 - val_accuracy: 0.9497

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    9/9 [==============================] - 13s 1s/step - loss: 0.2046 - accuracy: 0.9879 - val_loss: 2.3233 - val_accuracy: 0.9609

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    9/9 [==============================] - 18s 2s/step - loss: 0.0854 - accuracy: 0.9926 - val_loss: 1.5759 - val_accuracy: 0.9665

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    9/9 [==============================] - 12s 1s/step - loss: 0.2468 - accuracy: 0.9889 - val_loss: 2.1202 - val_accuracy: 0.9441

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    9/9 [==============================] - 13s 1s/step - loss: 0.1158 - accuracy: 0.9935 - val_loss: 1.1788 - val_accuracy: 0.9609

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0870 - accuracy: 0.9898 - val_loss: 0.7351 - val_accuracy: 0.9721

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0639 - accuracy: 0.9954 - val_loss: 0.8915 - val_accuracy: 0.9777

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    9/9 [==============================] - 16s 2s/step - loss: 0.2136 - accuracy: 0.9879 - val_loss: 1.7787 - val_accuracy: 0.9665

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0919 - accuracy: 0.9907 - val_loss: 3.1494 - val_accuracy: 0.9609

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1580 - accuracy: 0.9861 - val_loss: 1.7321 - val_accuracy: 0.9553

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0754 - accuracy: 0.9935 - val_loss: 1.5064 - val_accuracy: 0.9609

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1711 - accuracy: 0.9898 - val_loss: 1.3036 - val_accuracy: 0.9609

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0520 - accuracy: 0.9963 - val_loss: 1.2683 - val_accuracy: 0.9553

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0925 - accuracy: 0.9926 - val_loss: 1.1752 - val_accuracy: 0.9665

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0641 - accuracy: 0.9926 - val_loss: 2.0807 - val_accuracy: 0.9609

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0716 - accuracy: 0.9944 - val_loss: 0.9076 - val_accuracy: 0.9721

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    9/9 [==============================] - 13s 1s/step - loss: 0.0328 - accuracy: 0.9963 - val_loss: 0.9187 - val_accuracy: 0.9721

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1445 - accuracy: 0.9935 - val_loss: 2.2168 - val_accuracy: 0.9497

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0477 - accuracy: 0.9954 - val_loss: 3.7361 - val_accuracy: 0.9441

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0950 - accuracy: 0.9963 - val_loss: 3.0084 - val_accuracy: 0.9441

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0334 - accuracy: 0.9944 - val_loss: 2.2413 - val_accuracy: 0.9553

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0181 - accuracy: 0.9963 - val_loss: 2.4369 - val_accuracy: 0.9721

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    9/9 [==============================] - 13s 1s/step - loss: 0.0920 - accuracy: 0.9935 - val_loss: 2.6379 - val_accuracy: 0.9665

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0989 - accuracy: 0.9916 - val_loss: 2.0560 - val_accuracy: 0.9497

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    9/9 [==============================] - 12s 1s/step - loss: 0.1101 - accuracy: 0.9935 - val_loss: 2.3412 - val_accuracy: 0.9665

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0191 - accuracy: 0.9972 - val_loss: 2.2372 - val_accuracy: 0.9665

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0529 - accuracy: 0.9944 - val_loss: 2.0757 - val_accuracy: 0.9721

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0863 - accuracy: 0.9954 - val_loss: 1.5279 - val_accuracy: 0.9721

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1931 - accuracy: 0.9907 - val_loss: 1.4400 - val_accuracy: 0.9665

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1305 - accuracy: 0.9926 - val_loss: 1.1489 - val_accuracy: 0.9609

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1552 - accuracy: 0.9889 - val_loss: 1.5251 - val_accuracy: 0.9609

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    9/9 [==============================] - 13s 1s/step - loss: 0.0269 - accuracy: 0.9972 - val_loss: 1.7077 - val_accuracy: 0.9609

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0500 - accuracy: 0.9926 - val_loss: 2.0334 - val_accuracy: 0.9553

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1998 - accuracy: 0.9935 - val_loss: 1.9999 - val_accuracy: 0.9497

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0574 - accuracy: 0.9954 - val_loss: 2.2382 - val_accuracy: 0.9441

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    9/9 [==============================] - 12s 1s/step - loss: 0.0680 - accuracy: 0.9944 - val_loss: 1.9338 - val_accuracy: 0.9609

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0625 - accuracy: 0.9935 - val_loss: 1.0536 - val_accuracy: 0.9721

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1086 - accuracy: 0.9935 - val_loss: 0.9728 - val_accuracy: 0.9665

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0595 - accuracy: 0.9954 - val_loss: 1.7900 - val_accuracy: 0.9665

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1198 - accuracy: 0.9954 - val_loss: 2.1285 - val_accuracy: 0.9609

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0655 - accuracy: 0.9935 - val_loss: 2.7147 - val_accuracy: 0.9609

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1152 - accuracy: 0.9944 - val_loss: 1.4005 - val_accuracy: 0.9665

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    9/9 [==============================] - 15s 2s/step - loss: 0.0547 - accuracy: 0.9963 - val_loss: 1.0778 - val_accuracy: 0.9665

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    9/9 [==============================] - 16s 2s/step - loss: 0.0886 - accuracy: 0.9972 - val_loss: 6.0902 - val_accuracy: 0.8547

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    9/9 [==============================] - 15s 2s/step - loss: 0.2065 - accuracy: 0.9879 - val_loss: 2.3556 - val_accuracy: 0.9162

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    9/9 [==============================] - 16s 2s/step - loss: 0.1278 - accuracy: 0.9944 - val_loss: 4.0858 - val_accuracy: 0.9050

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    9/9 [==============================] - 15s 2s/step - loss: 0.1354 - accuracy: 0.9898 - val_loss: 0.8914 - val_accuracy: 0.9721

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/219.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 0.6040 - accuracy: 0.9852

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.604027271270752, 0.9851577281951904]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9851576994434137
                  precision    recall  f1-score   support

               0       0.98      0.97      0.97       158
               1       0.99      0.99      0.99       381

        accuracy                           0.99       539
       macro avg       0.98      0.98      0.98       539
    weighted avg       0.99      0.99      0.99       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/220.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/221.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/222.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.97      0.97       158
               1       0.99      0.99      0.99       381

        accuracy                           0.99       539
       macro avg       0.98      0.98      0.98       539
    weighted avg       0.99      0.99      0.99       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.97      0.99      0.97      0.98      0.96       158
              1       0.99      0.99      0.97      0.99      0.98      0.96       381

    avg / total       0.99      0.99      0.98      0.99      0.98      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
