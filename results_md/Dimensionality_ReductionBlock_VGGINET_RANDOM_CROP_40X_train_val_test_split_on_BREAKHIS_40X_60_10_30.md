<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import concatenate
from tensorflow.keras.utils import plot_model

def inception_module(layer_in, f1, f2_in, f2_out, f3_in, f3_out, f4_out):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2_in, (1,1), padding='same', activation='relu')(layer_in)
    conv3 = Conv2D(f2_out, (3,3), padding='same', activation='relu')(conv3)
    # 5x5 conv
    conv5 = Conv2D(f3_in, (1,1), padding='same', activation='relu')(layer_in)
    conv5 = Conv2D(f3_out, (5,5), padding='same', activation='relu')(conv5)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    pool = Conv2D(f4_out, (1,1), padding='same', activation='relu')(pool)
    # concatenate filters, assumes filters/channels last
    layer_out = concatenate([conv1, conv3, conv5, pool], axis=-1)
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = inception_module(feature_ex_model.output, 64, 96, 128, 16, 32, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 96)   49248       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_3 (Conv2D)               (None, 14, 14, 16)   8208        vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 128)  110720      conv2d_1[0][0]                   
    __________________________________________________________________________________________________
    conv2d_4 (Conv2D)               (None, 14, 14, 32)   12832       conv2d_3[0][0]                   
    __________________________________________________________________________________________________
    conv2d_5 (Conv2D)               (None, 14, 14, 32)   16416       max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 256)  0           conv2d[0][0]                     
                                                                     conv2d_2[0][0]                   
                                                                     conv2d_4[0][0]                   
                                                                     conv2d_5[0][0]                   
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 256)  1024        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 50176)        0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 50176)        0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            100354      Dropout[0][0]                    
    ==================================================================================================
    Total params: 7,966,898
    Trainable params: 331,122
    Non-trainable params: 7,635,776
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DimensionalityReduction_final00RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.6689 - accuracy: 0.4141WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 18s 2s/step - loss: 3.1741 - accuracy: 0.6620 - val_loss: 3.0540 - val_accuracy: 0.8156
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2540 - accuracy: 0.8069 - val_loss: 2.0579 - val_accuracy: 0.8212
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0251 - accuracy: 0.8050 - val_loss: 1.4552 - val_accuracy: 0.8827
    Epoch 4/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6647 - accuracy: 0.8635 - val_loss: 0.9465 - val_accuracy: 0.8715
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5588 - accuracy: 0.8858 - val_loss: 1.4733 - val_accuracy: 0.8547
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6344 - accuracy: 0.8914 - val_loss: 1.4961 - val_accuracy: 0.8771
    Epoch 7/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4187 - accuracy: 0.9201 - val_loss: 0.5631 - val_accuracy: 0.9106
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3843 - accuracy: 0.9118 - val_loss: 1.1875 - val_accuracy: 0.8715
    Epoch 9/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3583 - accuracy: 0.9229 - val_loss: 0.7086 - val_accuracy: 0.8883
    Epoch 10/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2986 - accuracy: 0.9406 - val_loss: 0.5600 - val_accuracy: 0.9330
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3615 - accuracy: 0.9146 - val_loss: 0.6147 - val_accuracy: 0.9218
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3029 - accuracy: 0.9387 - val_loss: 0.4050 - val_accuracy: 0.9162
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2819 - accuracy: 0.9294 - val_loss: 0.8195 - val_accuracy: 0.8883
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2511 - accuracy: 0.9564 - val_loss: 0.4826 - val_accuracy: 0.9441
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1460 - accuracy: 0.9638 - val_loss: 0.5855 - val_accuracy: 0.9385
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2143 - accuracy: 0.9517 - val_loss: 0.7580 - val_accuracy: 0.9106
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2415 - accuracy: 0.9554 - val_loss: 0.7816 - val_accuracy: 0.9050
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2112 - accuracy: 0.9508 - val_loss: 0.3467 - val_accuracy: 0.9441
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1914 - accuracy: 0.9629 - val_loss: 0.3237 - val_accuracy: 0.9497
    Epoch 20/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2523 - accuracy: 0.9536 - val_loss: 0.3797 - val_accuracy: 0.9553
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2119 - accuracy: 0.9573 - val_loss: 0.6437 - val_accuracy: 0.9330
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2778 - accuracy: 0.9424 - val_loss: 0.4694 - val_accuracy: 0.9274
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1690 - accuracy: 0.9601 - val_loss: 1.4761 - val_accuracy: 0.8771
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1543 - accuracy: 0.9675 - val_loss: 0.6281 - val_accuracy: 0.9274
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1988 - accuracy: 0.9591 - val_loss: 0.7585 - val_accuracy: 0.9106
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2514 - accuracy: 0.9573 - val_loss: 1.5716 - val_accuracy: 0.8715
    Epoch 27/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1518 - accuracy: 0.9656 - val_loss: 1.2048 - val_accuracy: 0.9050
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2366 - accuracy: 0.9675 - val_loss: 0.4774 - val_accuracy: 0.9330
    Epoch 29/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1568 - accuracy: 0.9675 - val_loss: 0.8515 - val_accuracy: 0.9330
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1539 - accuracy: 0.9703 - val_loss: 0.7227 - val_accuracy: 0.9441
    Epoch 31/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1613 - accuracy: 0.9656 - val_loss: 0.6919 - val_accuracy: 0.9106
    Epoch 32/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2258 - accuracy: 0.9554 - val_loss: 0.3478 - val_accuracy: 0.9497
    Epoch 33/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1695 - accuracy: 0.9759 - val_loss: 1.8798 - val_accuracy: 0.8659
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1318 - accuracy: 0.9749 - val_loss: 1.0198 - val_accuracy: 0.8994
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1824 - accuracy: 0.9694 - val_loss: 1.6577 - val_accuracy: 0.8659
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1886 - accuracy: 0.9712 - val_loss: 0.3603 - val_accuracy: 0.9553
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1093 - accuracy: 0.9749 - val_loss: 0.7187 - val_accuracy: 0.9385
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1352 - accuracy: 0.9703 - val_loss: 0.3730 - val_accuracy: 0.9497
    Epoch 39/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1445 - accuracy: 0.9759 - val_loss: 1.8687 - val_accuracy: 0.8994
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1153 - accuracy: 0.9740 - val_loss: 0.7434 - val_accuracy: 0.9609
    Epoch 41/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1105 - accuracy: 0.9777 - val_loss: 0.3407 - val_accuracy: 0.9497
    Epoch 42/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1510 - accuracy: 0.9786 - val_loss: 1.0865 - val_accuracy: 0.9050
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1018 - accuracy: 0.9824 - val_loss: 0.9322 - val_accuracy: 0.9162
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1118 - accuracy: 0.9814 - val_loss: 0.4140 - val_accuracy: 0.9665
    Epoch 45/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1002 - accuracy: 0.9749 - val_loss: 0.8871 - val_accuracy: 0.9385
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1561 - accuracy: 0.9759 - val_loss: 0.5219 - val_accuracy: 0.9385
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0981 - accuracy: 0.9833 - val_loss: 0.4603 - val_accuracy: 0.9609
    Epoch 48/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1225 - accuracy: 0.9786 - val_loss: 0.7116 - val_accuracy: 0.9497
    Epoch 49/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1261 - accuracy: 0.9786 - val_loss: 0.6585 - val_accuracy: 0.9385
    Epoch 50/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1311 - accuracy: 0.9814 - val_loss: 1.1266 - val_accuracy: 0.8994
    Epoch 51/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1199 - accuracy: 0.9712 - val_loss: 0.3574 - val_accuracy: 0.9609
    Epoch 52/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1710 - accuracy: 0.9694 - val_loss: 1.2862 - val_accuracy: 0.8939
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1372 - accuracy: 0.9824 - val_loss: 0.3342 - val_accuracy: 0.9497
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0828 - accuracy: 0.9861 - val_loss: 0.6125 - val_accuracy: 0.9441
    Epoch 55/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1187 - accuracy: 0.9786 - val_loss: 2.7386 - val_accuracy: 0.8715
    Epoch 56/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1123 - accuracy: 0.9786 - val_loss: 1.7136 - val_accuracy: 0.9050
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0858 - accuracy: 0.9824 - val_loss: 0.3311 - val_accuracy: 0.9609
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1628 - accuracy: 0.9796 - val_loss: 1.9900 - val_accuracy: 0.8715
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1454 - accuracy: 0.9786 - val_loss: 0.4494 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0489 - accuracy: 0.9879 - val_loss: 0.8253 - val_accuracy: 0.9162
    Epoch 61/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0856 - accuracy: 0.9833 - val_loss: 0.7785 - val_accuracy: 0.9497
    Epoch 62/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1133 - accuracy: 0.9749 - val_loss: 0.4841 - val_accuracy: 0.9609
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1138 - accuracy: 0.9833 - val_loss: 0.3523 - val_accuracy: 0.9553
    Epoch 64/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1680 - accuracy: 0.9768 - val_loss: 0.6033 - val_accuracy: 0.9385
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1615 - accuracy: 0.9712 - val_loss: 0.5754 - val_accuracy: 0.9553
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1123 - accuracy: 0.9786 - val_loss: 1.6676 - val_accuracy: 0.9218
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1066 - accuracy: 0.9814 - val_loss: 2.7786 - val_accuracy: 0.8771
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0670 - accuracy: 0.9851 - val_loss: 0.2788 - val_accuracy: 0.9497
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0369 - accuracy: 0.9926 - val_loss: 1.2568 - val_accuracy: 0.9218
    Epoch 70/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0820 - accuracy: 0.9851 - val_loss: 0.3593 - val_accuracy: 0.9665
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0834 - accuracy: 0.9861 - val_loss: 0.7846 - val_accuracy: 0.9385
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1349 - accuracy: 0.9824 - val_loss: 0.3927 - val_accuracy: 0.9553
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1153 - accuracy: 0.9861 - val_loss: 1.5257 - val_accuracy: 0.8939
    Epoch 74/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0431 - accuracy: 0.9935 - val_loss: 0.3389 - val_accuracy: 0.9441
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0693 - accuracy: 0.9870 - val_loss: 0.4551 - val_accuracy: 0.9497
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0798 - accuracy: 0.9879 - val_loss: 0.5919 - val_accuracy: 0.9609
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0794 - accuracy: 0.9861 - val_loss: 0.5085 - val_accuracy: 0.9665
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1442 - accuracy: 0.9824 - val_loss: 0.4351 - val_accuracy: 0.9665
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0514 - accuracy: 0.9898 - val_loss: 0.3976 - val_accuracy: 0.9609
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0761 - accuracy: 0.9889 - val_loss: 0.4992 - val_accuracy: 0.9609
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0967 - accuracy: 0.9842 - val_loss: 2.0313 - val_accuracy: 0.8994
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1045 - accuracy: 0.9879 - val_loss: 0.6379 - val_accuracy: 0.9441
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1134 - accuracy: 0.9824 - val_loss: 0.6270 - val_accuracy: 0.9497
    Epoch 84/100
    9/9 [==============================] - 17s 2s/step - loss: 0.0972 - accuracy: 0.9805 - val_loss: 0.2659 - val_accuracy: 0.9721
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0968 - accuracy: 0.9898 - val_loss: 0.3123 - val_accuracy: 0.9721
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0717 - accuracy: 0.9889 - val_loss: 0.2115 - val_accuracy: 0.9665
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1055 - accuracy: 0.9870 - val_loss: 0.3305 - val_accuracy: 0.9777
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1322 - accuracy: 0.9814 - val_loss: 0.2287 - val_accuracy: 0.9777
    Epoch 89/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0641 - accuracy: 0.9889 - val_loss: 0.3203 - val_accuracy: 0.9497
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1315 - accuracy: 0.9814 - val_loss: 0.2462 - val_accuracy: 0.9777
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0687 - accuracy: 0.9916 - val_loss: 0.5205 - val_accuracy: 0.9553
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0935 - accuracy: 0.9824 - val_loss: 0.3053 - val_accuracy: 0.9832
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1272 - accuracy: 0.9842 - val_loss: 0.5013 - val_accuracy: 0.9721
    Epoch 94/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0839 - accuracy: 0.9898 - val_loss: 0.3060 - val_accuracy: 0.9665
    Epoch 95/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0564 - accuracy: 0.9889 - val_loss: 0.6486 - val_accuracy: 0.9665
    Epoch 96/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1055 - accuracy: 0.9870 - val_loss: 0.5597 - val_accuracy: 0.9497
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0730 - accuracy: 0.9898 - val_loss: 0.3243 - val_accuracy: 0.9609
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0899 - accuracy: 0.9851 - val_loss: 0.3634 - val_accuracy: 0.9665
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0329 - accuracy: 0.9954 - val_loss: 0.7455 - val_accuracy: 0.9553
    Epoch 100/100
    9/9 [==============================] - 13s 1s/step - loss: 0.0483 - accuracy: 0.9935 - val_loss: 0.9222 - val_accuracy: 0.9609
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/51.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 13s 3s/step - loss: 0.7070 - accuracy: 0.9443

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.7069606184959412, 0.9443413615226746]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9443413729128015
                  precision    recall  f1-score   support

               0       0.98      0.82      0.90       158
               1       0.93      0.99      0.96       381

        accuracy                           0.94       539
       macro avg       0.96      0.91      0.93       539
    weighted avg       0.95      0.94      0.94       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/52.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/53.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/54.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.98      0.82      0.90       158
               1       0.93      0.99      0.96       381

        accuracy                           0.94       539
       macro avg       0.96      0.91      0.93       539
    weighted avg       0.95      0.94      0.94       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.98      0.82      0.99      0.90      0.90      0.80       158
              1       0.93      0.99      0.82      0.96      0.90      0.83       381

    avg / total       0.95      0.94      0.87      0.94      0.90      0.82       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
