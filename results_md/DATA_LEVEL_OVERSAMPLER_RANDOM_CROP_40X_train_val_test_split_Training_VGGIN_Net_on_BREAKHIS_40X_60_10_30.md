<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 64
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 images belonging to 2 classes.
    Found 179 images belonging to 2 classes.
    Found 539 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.over_sampling import RandomOverSampler
from tensorflow.keras.utils import to_categorical
ros = RandomOverSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((1524, 224, 340, 3), (1524, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_val.shape, y_val.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[17\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((179, 224, 340, 3), (179, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_test.shape, y_test.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((539, 224, 340, 3), (539, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(100).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
     1/24 [&gt;.............................] - ETA: 0s - loss: 1.2249 - accuracy: 0.4688WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    24/24 [==============================] - 24s 981ms/step - loss: 3.5039 - accuracy: 0.7867 - val_loss: 26.5944 - val_accuracy: 0.4749
    Epoch 2/100
    24/24 [==============================] - 21s 861ms/step - loss: 16.2474 - accuracy: 0.6693 - val_loss: 37.3679 - val_accuracy: 0.4693
    Epoch 3/100
    24/24 [==============================] - 20s 854ms/step - loss: 39.0884 - accuracy: 0.5046 - val_loss: 6.3755 - val_accuracy: 0.8715
    Epoch 4/100
    24/24 [==============================] - 20s 839ms/step - loss: 25.7229 - accuracy: 0.5551 - val_loss: 5.5716 - val_accuracy: 0.8827
    Epoch 5/100
    24/24 [==============================] - 21s 860ms/step - loss: 11.6443 - accuracy: 0.6791 - val_loss: 3.2717 - val_accuracy: 0.9050
    Epoch 6/100
    24/24 [==============================] - 20s 853ms/step - loss: 7.2418 - accuracy: 0.7343 - val_loss: 2.3132 - val_accuracy: 0.9106
    Epoch 7/100
    24/24 [==============================] - 20s 850ms/step - loss: 9.2927 - accuracy: 0.6804 - val_loss: 2.6950 - val_accuracy: 0.8771
    Epoch 8/100
    24/24 [==============================] - 20s 851ms/step - loss: 9.4636 - accuracy: 0.6804 - val_loss: 1.5613 - val_accuracy: 0.9162
    Epoch 9/100
    24/24 [==============================] - 21s 868ms/step - loss: 7.3317 - accuracy: 0.7198 - val_loss: 1.7447 - val_accuracy: 0.9162
    Epoch 10/100
    24/24 [==============================] - 21s 867ms/step - loss: 5.0196 - accuracy: 0.7789 - val_loss: 1.3811 - val_accuracy: 0.9162
    Epoch 11/100
    24/24 [==============================] - 21s 877ms/step - loss: 5.2638 - accuracy: 0.7625 - val_loss: 2.1384 - val_accuracy: 0.8994
    Epoch 12/100
    24/24 [==============================] - 21s 856ms/step - loss: 4.2064 - accuracy: 0.7900 - val_loss: 1.4006 - val_accuracy: 0.9274
    Epoch 13/100
    24/24 [==============================] - 21s 867ms/step - loss: 3.9256 - accuracy: 0.8084 - val_loss: 1.6984 - val_accuracy: 0.8939
    Epoch 14/100
    24/24 [==============================] - 21s 862ms/step - loss: 3.2565 - accuracy: 0.8202 - val_loss: 1.5249 - val_accuracy: 0.9162
    Epoch 15/100
    24/24 [==============================] - 21s 876ms/step - loss: 3.6529 - accuracy: 0.8268 - val_loss: 1.4321 - val_accuracy: 0.9274
    Epoch 16/100
    24/24 [==============================] - 23s 943ms/step - loss: 2.5726 - accuracy: 0.8609 - val_loss: 1.6802 - val_accuracy: 0.9218
    Epoch 17/100
    24/24 [==============================] - 22s 901ms/step - loss: 2.1782 - accuracy: 0.8635 - val_loss: 1.6674 - val_accuracy: 0.8994
    Epoch 18/100
    24/24 [==============================] - 21s 865ms/step - loss: 2.5696 - accuracy: 0.8675 - val_loss: 1.3589 - val_accuracy: 0.9330
    Epoch 19/100
    24/24 [==============================] - 21s 872ms/step - loss: 1.9922 - accuracy: 0.8904 - val_loss: 1.4415 - val_accuracy: 0.9218
    Epoch 20/100
    24/24 [==============================] - 23s 943ms/step - loss: 2.1005 - accuracy: 0.8766 - val_loss: 1.6640 - val_accuracy: 0.9162
    Epoch 21/100
    24/24 [==============================] - 21s 873ms/step - loss: 1.8038 - accuracy: 0.8812 - val_loss: 1.3707 - val_accuracy: 0.9330
    Epoch 22/100
    24/24 [==============================] - 21s 876ms/step - loss: 1.1350 - accuracy: 0.9088 - val_loss: 1.6198 - val_accuracy: 0.9050
    Epoch 23/100
    24/24 [==============================] - 21s 862ms/step - loss: 1.1900 - accuracy: 0.9127 - val_loss: 1.9853 - val_accuracy: 0.9218
    Epoch 24/100
    24/24 [==============================] - 20s 854ms/step - loss: 1.4121 - accuracy: 0.9094 - val_loss: 2.0619 - val_accuracy: 0.8994
    Epoch 25/100
    24/24 [==============================] - 21s 862ms/step - loss: 1.2909 - accuracy: 0.9167 - val_loss: 1.4564 - val_accuracy: 0.9162
    Epoch 26/100
    24/24 [==============================] - 21s 859ms/step - loss: 1.0537 - accuracy: 0.9232 - val_loss: 1.4504 - val_accuracy: 0.9274
    Epoch 27/100
    24/24 [==============================] - 22s 898ms/step - loss: 1.1789 - accuracy: 0.9154 - val_loss: 1.2524 - val_accuracy: 0.9274
    Epoch 28/100
    24/24 [==============================] - 21s 860ms/step - loss: 1.1094 - accuracy: 0.9206 - val_loss: 1.1366 - val_accuracy: 0.9274
    Epoch 29/100
    24/24 [==============================] - 22s 905ms/step - loss: 1.0095 - accuracy: 0.9285 - val_loss: 1.3199 - val_accuracy: 0.9218
    Epoch 30/100
    24/24 [==============================] - 23s 938ms/step - loss: 1.2549 - accuracy: 0.9239 - val_loss: 3.3853 - val_accuracy: 0.8883
    Epoch 31/100
    24/24 [==============================] - 21s 881ms/step - loss: 1.6135 - accuracy: 0.9016 - val_loss: 1.4176 - val_accuracy: 0.9050
    Epoch 32/100
    24/24 [==============================] - 21s 883ms/step - loss: 1.4231 - accuracy: 0.9094 - val_loss: 1.4958 - val_accuracy: 0.9162
    Epoch 33/100
    24/24 [==============================] - 21s 882ms/step - loss: 0.9816 - accuracy: 0.9344 - val_loss: 0.9957 - val_accuracy: 0.9106
    Epoch 34/100
    24/24 [==============================] - 21s 870ms/step - loss: 0.6083 - accuracy: 0.9528 - val_loss: 1.8915 - val_accuracy: 0.9162
    Epoch 35/100
    24/24 [==============================] - 22s 898ms/step - loss: 0.9734 - accuracy: 0.9409 - val_loss: 2.5432 - val_accuracy: 0.9218
    Epoch 36/100
    24/24 [==============================] - 23s 944ms/step - loss: 0.9765 - accuracy: 0.9370 - val_loss: 1.0759 - val_accuracy: 0.9218
    Epoch 37/100
    24/24 [==============================] - 21s 887ms/step - loss: 0.6520 - accuracy: 0.9521 - val_loss: 1.0254 - val_accuracy: 0.9050
    Epoch 38/100
    24/24 [==============================] - 20s 848ms/step - loss: 0.6438 - accuracy: 0.9501 - val_loss: 1.9604 - val_accuracy: 0.9385
    Epoch 39/100
    24/24 [==============================] - 21s 854ms/step - loss: 0.5772 - accuracy: 0.9619 - val_loss: 1.5108 - val_accuracy: 0.9330
    Epoch 40/100
    24/24 [==============================] - 20s 849ms/step - loss: 0.5623 - accuracy: 0.9600 - val_loss: 1.0610 - val_accuracy: 0.9441
    Epoch 41/100
    24/24 [==============================] - 21s 882ms/step - loss: 0.7410 - accuracy: 0.9528 - val_loss: 1.3226 - val_accuracy: 0.9274
    Epoch 42/100
    24/24 [==============================] - 21s 875ms/step - loss: 0.5353 - accuracy: 0.9672 - val_loss: 0.9592 - val_accuracy: 0.9441
    Epoch 43/100
    24/24 [==============================] - 22s 907ms/step - loss: 0.6399 - accuracy: 0.9573 - val_loss: 1.1314 - val_accuracy: 0.9385
    Epoch 44/100
    24/24 [==============================] - 21s 874ms/step - loss: 0.6292 - accuracy: 0.9573 - val_loss: 1.5982 - val_accuracy: 0.9330
    Epoch 45/100
    24/24 [==============================] - 21s 866ms/step - loss: 0.7664 - accuracy: 0.9488 - val_loss: 1.4279 - val_accuracy: 0.9330
    Epoch 46/100
    24/24 [==============================] - 21s 880ms/step - loss: 0.6381 - accuracy: 0.9475 - val_loss: 0.9288 - val_accuracy: 0.9385
    Epoch 47/100
    24/24 [==============================] - 22s 926ms/step - loss: 0.5763 - accuracy: 0.9593 - val_loss: 1.1809 - val_accuracy: 0.9441
    Epoch 48/100
    24/24 [==============================] - 23s 949ms/step - loss: 0.4676 - accuracy: 0.9626 - val_loss: 1.0941 - val_accuracy: 0.9330
    Epoch 49/100
    24/24 [==============================] - 22s 914ms/step - loss: 0.5986 - accuracy: 0.9580 - val_loss: 1.0965 - val_accuracy: 0.9441
    Epoch 50/100
    24/24 [==============================] - 21s 858ms/step - loss: 0.5454 - accuracy: 0.9567 - val_loss: 0.7661 - val_accuracy: 0.9441
    Epoch 51/100
    24/24 [==============================] - 21s 872ms/step - loss: 0.3342 - accuracy: 0.9639 - val_loss: 1.5196 - val_accuracy: 0.9330
    Epoch 52/100
    24/24 [==============================] - 20s 854ms/step - loss: 0.5220 - accuracy: 0.9639 - val_loss: 1.4419 - val_accuracy: 0.9274
    Epoch 53/100
    24/24 [==============================] - 21s 860ms/step - loss: 0.6209 - accuracy: 0.9613 - val_loss: 1.0136 - val_accuracy: 0.9385
    Epoch 54/100
    24/24 [==============================] - 21s 867ms/step - loss: 0.5446 - accuracy: 0.9587 - val_loss: 1.5302 - val_accuracy: 0.9330
    Epoch 55/100
    24/24 [==============================] - 21s 871ms/step - loss: 0.4794 - accuracy: 0.9698 - val_loss: 1.2653 - val_accuracy: 0.9385
    Epoch 56/100
    24/24 [==============================] - 21s 855ms/step - loss: 0.4457 - accuracy: 0.9672 - val_loss: 1.6519 - val_accuracy: 0.9218
    Epoch 57/100
    24/24 [==============================] - 20s 843ms/step - loss: 0.6962 - accuracy: 0.9613 - val_loss: 1.5431 - val_accuracy: 0.9274
    Epoch 58/100
    24/24 [==============================] - 21s 874ms/step - loss: 0.5286 - accuracy: 0.9646 - val_loss: 0.8236 - val_accuracy: 0.9497
    Epoch 59/100
    24/24 [==============================] - 21s 866ms/step - loss: 0.5237 - accuracy: 0.9659 - val_loss: 0.9694 - val_accuracy: 0.9385
    Epoch 60/100
    24/24 [==============================] - 21s 866ms/step - loss: 0.5880 - accuracy: 0.9672 - val_loss: 1.0700 - val_accuracy: 0.9441
    Epoch 61/100
    24/24 [==============================] - 21s 867ms/step - loss: 0.3541 - accuracy: 0.9692 - val_loss: 1.4293 - val_accuracy: 0.9441
    Epoch 62/100
    24/24 [==============================] - 21s 859ms/step - loss: 0.4405 - accuracy: 0.9705 - val_loss: 1.8682 - val_accuracy: 0.9441
    Epoch 63/100
    24/24 [==============================] - 20s 843ms/step - loss: 0.4768 - accuracy: 0.9751 - val_loss: 1.4028 - val_accuracy: 0.9162
    Epoch 64/100
    24/24 [==============================] - 21s 860ms/step - loss: 0.3730 - accuracy: 0.9744 - val_loss: 2.7820 - val_accuracy: 0.9274
    Epoch 65/100
    24/24 [==============================] - 21s 859ms/step - loss: 0.5023 - accuracy: 0.9659 - val_loss: 0.9077 - val_accuracy: 0.9497
    Epoch 66/100
    24/24 [==============================] - 21s 877ms/step - loss: 0.4259 - accuracy: 0.9705 - val_loss: 1.5335 - val_accuracy: 0.9385
    Epoch 67/100
    24/24 [==============================] - 21s 886ms/step - loss: 0.5310 - accuracy: 0.9705 - val_loss: 1.1231 - val_accuracy: 0.9274
    Epoch 68/100
    24/24 [==============================] - 21s 864ms/step - loss: 0.4651 - accuracy: 0.9633 - val_loss: 1.1737 - val_accuracy: 0.9441
    Epoch 69/100
    24/24 [==============================] - 21s 879ms/step - loss: 0.7763 - accuracy: 0.9495 - val_loss: 2.5134 - val_accuracy: 0.9330
    Epoch 70/100
    24/24 [==============================] - 23s 943ms/step - loss: 0.6092 - accuracy: 0.9633 - val_loss: 0.8036 - val_accuracy: 0.9553
    Epoch 71/100
    24/24 [==============================] - 21s 870ms/step - loss: 0.7467 - accuracy: 0.9547 - val_loss: 0.7375 - val_accuracy: 0.9497
    Epoch 72/100
    24/24 [==============================] - 21s 879ms/step - loss: 0.6635 - accuracy: 0.9547 - val_loss: 0.8005 - val_accuracy: 0.9441
    Epoch 73/100
    24/24 [==============================] - 21s 869ms/step - loss: 0.5389 - accuracy: 0.9619 - val_loss: 1.2618 - val_accuracy: 0.9162
    Epoch 74/100
    24/24 [==============================] - 21s 878ms/step - loss: 0.4991 - accuracy: 0.9698 - val_loss: 0.8000 - val_accuracy: 0.9497
    Epoch 75/100
    24/24 [==============================] - 22s 928ms/step - loss: 0.2839 - accuracy: 0.9810 - val_loss: 1.4585 - val_accuracy: 0.9441
    Epoch 76/100
    24/24 [==============================] - 22s 901ms/step - loss: 0.3566 - accuracy: 0.9738 - val_loss: 0.9745 - val_accuracy: 0.9385
    Epoch 77/100
    24/24 [==============================] - 21s 861ms/step - loss: 0.2927 - accuracy: 0.9751 - val_loss: 1.0946 - val_accuracy: 0.9441
    Epoch 78/100
    24/24 [==============================] - 21s 867ms/step - loss: 0.2933 - accuracy: 0.9764 - val_loss: 0.8932 - val_accuracy: 0.9609
    Epoch 79/100
    24/24 [==============================] - 20s 854ms/step - loss: 0.4333 - accuracy: 0.9744 - val_loss: 1.1047 - val_accuracy: 0.9385
    Epoch 80/100
    24/24 [==============================] - 21s 867ms/step - loss: 0.3493 - accuracy: 0.9829 - val_loss: 1.1312 - val_accuracy: 0.9330
    Epoch 81/100
    24/24 [==============================] - 21s 855ms/step - loss: 0.3465 - accuracy: 0.9718 - val_loss: 1.2688 - val_accuracy: 0.9609
    Epoch 82/100
    24/24 [==============================] - 21s 865ms/step - loss: 0.4118 - accuracy: 0.9770 - val_loss: 1.1638 - val_accuracy: 0.9385
    Epoch 83/100
    24/24 [==============================] - 22s 919ms/step - loss: 0.2631 - accuracy: 0.9810 - val_loss: 0.8763 - val_accuracy: 0.9553
    Epoch 84/100
    24/24 [==============================] - 21s 871ms/step - loss: 0.3905 - accuracy: 0.9764 - val_loss: 0.7901 - val_accuracy: 0.9665
    Epoch 85/100
    24/24 [==============================] - 21s 867ms/step - loss: 0.2829 - accuracy: 0.9810 - val_loss: 1.6105 - val_accuracy: 0.9497
    Epoch 86/100
    24/24 [==============================] - 21s 876ms/step - loss: 0.1562 - accuracy: 0.9875 - val_loss: 1.0195 - val_accuracy: 0.9497
    Epoch 87/100
    24/24 [==============================] - 21s 874ms/step - loss: 0.2221 - accuracy: 0.9836 - val_loss: 0.5539 - val_accuracy: 0.9777
    Epoch 88/100
    24/24 [==============================] - 21s 877ms/step - loss: 0.3120 - accuracy: 0.9803 - val_loss: 1.3796 - val_accuracy: 0.9385
    Epoch 89/100
    24/24 [==============================] - 23s 949ms/step - loss: 0.1418 - accuracy: 0.9843 - val_loss: 1.0443 - val_accuracy: 0.9553
    Epoch 90/100
    24/24 [==============================] - 23s 962ms/step - loss: 0.3006 - accuracy: 0.9816 - val_loss: 0.8456 - val_accuracy: 0.9609
    Epoch 91/100
    24/24 [==============================] - 21s 882ms/step - loss: 0.2537 - accuracy: 0.9823 - val_loss: 1.9202 - val_accuracy: 0.9330
    Epoch 92/100
    24/24 [==============================] - 21s 892ms/step - loss: 0.2564 - accuracy: 0.9829 - val_loss: 0.9011 - val_accuracy: 0.9609
    Epoch 93/100
    24/24 [==============================] - 23s 966ms/step - loss: 0.2954 - accuracy: 0.9770 - val_loss: 1.3448 - val_accuracy: 0.9385
    Epoch 94/100
    24/24 [==============================] - 21s 888ms/step - loss: 0.3215 - accuracy: 0.9797 - val_loss: 1.1454 - val_accuracy: 0.9609
    Epoch 95/100
    24/24 [==============================] - 22s 923ms/step - loss: 0.3577 - accuracy: 0.9777 - val_loss: 0.9187 - val_accuracy: 0.9665
    Epoch 96/100
    24/24 [==============================] - 22s 899ms/step - loss: 0.2243 - accuracy: 0.9856 - val_loss: 1.4085 - val_accuracy: 0.9497
    Epoch 97/100
    24/24 [==============================] - 23s 949ms/step - loss: 0.2255 - accuracy: 0.9829 - val_loss: 0.9409 - val_accuracy: 0.9553
    Epoch 98/100
    24/24 [==============================] - 22s 897ms/step - loss: 0.3710 - accuracy: 0.9803 - val_loss: 2.8356 - val_accuracy: 0.9050
    Epoch 99/100
    24/24 [==============================] - 23s 976ms/step - loss: 0.7710 - accuracy: 0.9639 - val_loss: 4.2511 - val_accuracy: 0.8771
    Epoch 100/100
    24/24 [==============================] - 22s 930ms/step - loss: 0.9332 - accuracy: 0.9639 - val_loss: 1.6845 - val_accuracy: 0.9385

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/131.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 3s 310ms/step - loss: 1.3433 - accuracy: 0.9406

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[30\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3433165550231934, 0.9406307935714722]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9406307977736549
                  precision    recall  f1-score   support

               0       0.86      0.96      0.90       158
               1       0.98      0.93      0.96       381

        accuracy                           0.94       539
       macro avg       0.92      0.95      0.93       539
    weighted avg       0.94      0.94      0.94       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[33\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/132.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/133.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/134.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.86      0.96      0.90       158
               1       0.98      0.93      0.96       381

        accuracy                           0.94       539
       macro avg       0.92      0.95      0.93       539
    weighted avg       0.94      0.94      0.94       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.86      0.96      0.93      0.90      0.94      0.89       158
              1       0.98      0.93      0.96      0.96      0.94      0.89       381

    avg / total       0.94      0.94      0.95      0.94      0.94      0.89       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
