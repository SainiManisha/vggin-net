<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/InceptionV1-NORMAL-RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.0101 - accuracy: 0.3438WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 24s 3s/step - loss: 0.7938 - accuracy: 0.6277 - val_loss: 0.7241 - val_accuracy: 0.7095
    Epoch 2/50
    9/9 [==============================] - 12s 1s/step - loss: 0.6828 - accuracy: 0.6602 - val_loss: 0.5852 - val_accuracy: 0.7263
    Epoch 3/50
    9/9 [==============================] - 12s 1s/step - loss: 0.6560 - accuracy: 0.6862 - val_loss: 0.5563 - val_accuracy: 0.7430
    Epoch 4/50
    9/9 [==============================] - 12s 1s/step - loss: 0.5868 - accuracy: 0.7019 - val_loss: 0.5250 - val_accuracy: 0.7709
    Epoch 5/50
    9/9 [==============================] - 12s 1s/step - loss: 0.5702 - accuracy: 0.7317 - val_loss: 0.4956 - val_accuracy: 0.7989
    Epoch 6/50
    9/9 [==============================] - 12s 1s/step - loss: 0.5345 - accuracy: 0.7502 - val_loss: 0.4908 - val_accuracy: 0.7933
    Epoch 7/50
    9/9 [==============================] - 12s 1s/step - loss: 0.5168 - accuracy: 0.7651 - val_loss: 0.4685 - val_accuracy: 0.8324
    Epoch 8/50
    9/9 [==============================] - 12s 1s/step - loss: 0.5152 - accuracy: 0.7577 - val_loss: 0.4600 - val_accuracy: 0.8268
    Epoch 9/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4814 - accuracy: 0.7725 - val_loss: 0.4491 - val_accuracy: 0.8268
    Epoch 10/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4774 - accuracy: 0.7939 - val_loss: 0.4437 - val_accuracy: 0.8324
    Epoch 11/50
    9/9 [==============================] - 9s 961ms/step - loss: 0.4584 - accuracy: 0.7939 - val_loss: 0.4297 - val_accuracy: 0.8380
    Epoch 12/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4542 - accuracy: 0.8050 - val_loss: 0.4347 - val_accuracy: 0.8156
    Epoch 13/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4532 - accuracy: 0.7994 - val_loss: 0.4236 - val_accuracy: 0.8268
    Epoch 14/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4264 - accuracy: 0.8162 - val_loss: 0.4251 - val_accuracy: 0.8045
    Epoch 15/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4294 - accuracy: 0.8171 - val_loss: 0.4223 - val_accuracy: 0.8101
    Epoch 16/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4306 - accuracy: 0.8087 - val_loss: 0.4249 - val_accuracy: 0.7989
    Epoch 17/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4086 - accuracy: 0.8301 - val_loss: 0.4211 - val_accuracy: 0.7989
    Epoch 18/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4219 - accuracy: 0.8236 - val_loss: 0.4147 - val_accuracy: 0.8156
    Epoch 19/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3810 - accuracy: 0.8403 - val_loss: 0.4048 - val_accuracy: 0.8156
    Epoch 20/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3879 - accuracy: 0.8366 - val_loss: 0.4109 - val_accuracy: 0.8101
    Epoch 21/50
    9/9 [==============================] - 15s 2s/step - loss: 0.4035 - accuracy: 0.8347 - val_loss: 0.4104 - val_accuracy: 0.8045
    Epoch 22/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3869 - accuracy: 0.8431 - val_loss: 0.3969 - val_accuracy: 0.8101
    Epoch 23/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3759 - accuracy: 0.8347 - val_loss: 0.4001 - val_accuracy: 0.8101
    Epoch 24/50
    9/9 [==============================] - 12s 1s/step - loss: 0.4065 - accuracy: 0.8282 - val_loss: 0.3833 - val_accuracy: 0.8268
    Epoch 25/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3842 - accuracy: 0.8347 - val_loss: 0.3983 - val_accuracy: 0.8101
    Epoch 26/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3653 - accuracy: 0.8496 - val_loss: 0.3691 - val_accuracy: 0.8380
    Epoch 27/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3813 - accuracy: 0.8319 - val_loss: 0.3888 - val_accuracy: 0.8268
    Epoch 28/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3853 - accuracy: 0.8329 - val_loss: 0.3940 - val_accuracy: 0.8212
    Epoch 29/50
    9/9 [==============================] - 15s 2s/step - loss: 0.3674 - accuracy: 0.8412 - val_loss: 0.3789 - val_accuracy: 0.8324
    Epoch 30/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3745 - accuracy: 0.8496 - val_loss: 0.4024 - val_accuracy: 0.8101
    Epoch 31/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3521 - accuracy: 0.8570 - val_loss: 0.3601 - val_accuracy: 0.8380
    Epoch 32/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3621 - accuracy: 0.8422 - val_loss: 0.3987 - val_accuracy: 0.8045
    Epoch 33/50
    9/9 [==============================] - 16s 2s/step - loss: 0.3654 - accuracy: 0.8412 - val_loss: 0.3657 - val_accuracy: 0.8436
    Epoch 34/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3653 - accuracy: 0.8505 - val_loss: 0.3726 - val_accuracy: 0.8436
    Epoch 35/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3531 - accuracy: 0.8505 - val_loss: 0.3657 - val_accuracy: 0.8436
    Epoch 36/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3666 - accuracy: 0.8449 - val_loss: 0.3767 - val_accuracy: 0.8380
    Epoch 37/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3728 - accuracy: 0.8487 - val_loss: 0.3620 - val_accuracy: 0.8436
    Epoch 38/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3694 - accuracy: 0.8412 - val_loss: 0.3795 - val_accuracy: 0.8324
    Epoch 39/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3532 - accuracy: 0.8617 - val_loss: 0.3594 - val_accuracy: 0.8436
    Epoch 40/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3545 - accuracy: 0.8496 - val_loss: 0.3676 - val_accuracy: 0.8324
    Epoch 41/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3525 - accuracy: 0.8459 - val_loss: 0.3410 - val_accuracy: 0.8492
    Epoch 42/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3323 - accuracy: 0.8654 - val_loss: 0.3452 - val_accuracy: 0.8547
    Epoch 43/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3451 - accuracy: 0.8552 - val_loss: 0.3724 - val_accuracy: 0.8324
    Epoch 44/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3437 - accuracy: 0.8663 - val_loss: 0.3467 - val_accuracy: 0.8492
    Epoch 45/50
    9/9 [==============================] - 16s 2s/step - loss: 0.3408 - accuracy: 0.8570 - val_loss: 0.3710 - val_accuracy: 0.8380
    Epoch 46/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3329 - accuracy: 0.8570 - val_loss: 0.3489 - val_accuracy: 0.8380
    Epoch 47/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3407 - accuracy: 0.8505 - val_loss: 0.3859 - val_accuracy: 0.8045
    Epoch 48/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3433 - accuracy: 0.8589 - val_loss: 0.3387 - val_accuracy: 0.8547
    Epoch 49/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3439 - accuracy: 0.8570 - val_loss: 0.3653 - val_accuracy: 0.8380
    Epoch 50/50
    9/9 [==============================] - 12s 1s/step - loss: 0.3250 - accuracy: 0.8663 - val_loss: 0.3398 - val_accuracy: 0.8492

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/59.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 28s 6s/step - loss: 0.3027 - accuracy: 0.8683

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.3026564419269562, 0.8682745695114136]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8682745825602969
                  precision    recall  f1-score   support

               0       0.77      0.78      0.78       158
               1       0.91      0.90      0.91       381

        accuracy                           0.87       539
       macro avg       0.84      0.84      0.84       539
    weighted avg       0.87      0.87      0.87       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/60.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/61.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/62.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.77      0.78      0.78       158
               1       0.91      0.90      0.91       381

        accuracy                           0.87       539
       macro avg       0.84      0.84      0.84       539
    weighted avg       0.87      0.87      0.87       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.77      0.78      0.90      0.78      0.84      0.70       158
              1       0.91      0.90      0.78      0.91      0.84      0.72       381

    avg / total       0.87      0.87      0.82      0.87      0.84      0.71       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
