<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 128, 256, 64)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 128)  65664       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 256)  1179904     vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 64)   819264      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 960)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 960)  3840        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 188160)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 188160)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            376322      Dropout[0][0]                    
    ==================================================================================================
    Total params: 10,080,258
    Trainable params: 2,443,074
    Non-trainable params: 7,637,184
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'k2final00RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    mkdir: cannot create directory ‘./experiments/k2final00RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet’: File exists

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1489 - accuracy: 0.5078WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 15s 2s/step - loss: 6.3022 - accuracy: 0.7094 - val_loss: 36.5319 - val_accuracy: 0.7263
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 3.2311 - accuracy: 0.8189 - val_loss: 8.5162 - val_accuracy: 0.7933
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 2.2088 - accuracy: 0.8747 - val_loss: 4.8008 - val_accuracy: 0.8883
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8491 - accuracy: 0.8904 - val_loss: 2.7961 - val_accuracy: 0.8771
    Epoch 5/100
    9/9 [==============================] - 9s 1s/step - loss: 1.4972 - accuracy: 0.8886 - val_loss: 7.1995 - val_accuracy: 0.8101
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0371 - accuracy: 0.9109 - val_loss: 1.4469 - val_accuracy: 0.9050
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9062 - accuracy: 0.9192 - val_loss: 1.8854 - val_accuracy: 0.9274
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6680 - accuracy: 0.9359 - val_loss: 1.3061 - val_accuracy: 0.9385
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7287 - accuracy: 0.9359 - val_loss: 1.4441 - val_accuracy: 0.9162
    Epoch 10/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8747 - accuracy: 0.9331 - val_loss: 1.5604 - val_accuracy: 0.8994
    Epoch 11/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7380 - accuracy: 0.9369 - val_loss: 1.1049 - val_accuracy: 0.9441
    Epoch 12/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7536 - accuracy: 0.9471 - val_loss: 0.9353 - val_accuracy: 0.9385
    Epoch 13/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4864 - accuracy: 0.9610 - val_loss: 3.5276 - val_accuracy: 0.8883
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4176 - accuracy: 0.9666 - val_loss: 2.4003 - val_accuracy: 0.9218
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9397 - accuracy: 0.9396 - val_loss: 2.1451 - val_accuracy: 0.9274
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6013 - accuracy: 0.9601 - val_loss: 1.4337 - val_accuracy: 0.8883
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6052 - accuracy: 0.9517 - val_loss: 0.6439 - val_accuracy: 0.9497
    Epoch 18/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5189 - accuracy: 0.9554 - val_loss: 1.1880 - val_accuracy: 0.9609
    Epoch 19/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3559 - accuracy: 0.9629 - val_loss: 0.6841 - val_accuracy: 0.9330
    Epoch 20/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6602 - accuracy: 0.9452 - val_loss: 0.9301 - val_accuracy: 0.9553
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4387 - accuracy: 0.9684 - val_loss: 1.6026 - val_accuracy: 0.9497
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5332 - accuracy: 0.9666 - val_loss: 1.1218 - val_accuracy: 0.9218
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3566 - accuracy: 0.9731 - val_loss: 2.1126 - val_accuracy: 0.8939
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3959 - accuracy: 0.9721 - val_loss: 2.5892 - val_accuracy: 0.9385
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4463 - accuracy: 0.9684 - val_loss: 2.9518 - val_accuracy: 0.9330
    Epoch 26/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4539 - accuracy: 0.9601 - val_loss: 0.9496 - val_accuracy: 0.9497
    Epoch 27/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4856 - accuracy: 0.9619 - val_loss: 1.9023 - val_accuracy: 0.9162
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7429 - accuracy: 0.9638 - val_loss: 2.1909 - val_accuracy: 0.9106
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5396 - accuracy: 0.9638 - val_loss: 1.9486 - val_accuracy: 0.9218
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5008 - accuracy: 0.9703 - val_loss: 1.7280 - val_accuracy: 0.9609
    Epoch 31/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6943 - accuracy: 0.9638 - val_loss: 3.3505 - val_accuracy: 0.9162
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3322 - accuracy: 0.9731 - val_loss: 1.7390 - val_accuracy: 0.9385
    Epoch 33/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3060 - accuracy: 0.9786 - val_loss: 4.0260 - val_accuracy: 0.9497
    Epoch 34/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5074 - accuracy: 0.9675 - val_loss: 1.4660 - val_accuracy: 0.9609
    Epoch 35/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6554 - accuracy: 0.9629 - val_loss: 1.2536 - val_accuracy: 0.9385
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4470 - accuracy: 0.9675 - val_loss: 1.9724 - val_accuracy: 0.9441
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8634 - accuracy: 0.9656 - val_loss: 1.3659 - val_accuracy: 0.9553
    Epoch 38/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5373 - accuracy: 0.9675 - val_loss: 1.5419 - val_accuracy: 0.9609
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2573 - accuracy: 0.9870 - val_loss: 2.7443 - val_accuracy: 0.9441
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5387 - accuracy: 0.9731 - val_loss: 1.7397 - val_accuracy: 0.9609
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3132 - accuracy: 0.9768 - val_loss: 1.4603 - val_accuracy: 0.9609
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3043 - accuracy: 0.9721 - val_loss: 1.0162 - val_accuracy: 0.9553
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3352 - accuracy: 0.9731 - val_loss: 3.5995 - val_accuracy: 0.9274
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3533 - accuracy: 0.9759 - val_loss: 1.6005 - val_accuracy: 0.9497
    Epoch 45/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3619 - accuracy: 0.9768 - val_loss: 1.7244 - val_accuracy: 0.9441
    Epoch 46/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3969 - accuracy: 0.9796 - val_loss: 2.2867 - val_accuracy: 0.9497
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2338 - accuracy: 0.9842 - val_loss: 0.9734 - val_accuracy: 0.9497
    Epoch 48/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4088 - accuracy: 0.9796 - val_loss: 0.9779 - val_accuracy: 0.9665
    Epoch 49/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3947 - accuracy: 0.9786 - val_loss: 1.3375 - val_accuracy: 0.9441
    Epoch 50/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3090 - accuracy: 0.9861 - val_loss: 3.0025 - val_accuracy: 0.9385
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2933 - accuracy: 0.9814 - val_loss: 3.0686 - val_accuracy: 0.9330
    Epoch 52/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3772 - accuracy: 0.9777 - val_loss: 2.7952 - val_accuracy: 0.9218
    Epoch 53/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4807 - accuracy: 0.9842 - val_loss: 1.8809 - val_accuracy: 0.9441
    Epoch 54/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2547 - accuracy: 0.9814 - val_loss: 1.0934 - val_accuracy: 0.9553
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4404 - accuracy: 0.9749 - val_loss: 1.0456 - val_accuracy: 0.9553
    Epoch 56/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2525 - accuracy: 0.9870 - val_loss: 1.5242 - val_accuracy: 0.9553
    Epoch 57/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2427 - accuracy: 0.9824 - val_loss: 1.7818 - val_accuracy: 0.9497
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2248 - accuracy: 0.9898 - val_loss: 0.9369 - val_accuracy: 0.9665
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2284 - accuracy: 0.9842 - val_loss: 3.1064 - val_accuracy: 0.9274
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3688 - accuracy: 0.9805 - val_loss: 1.0748 - val_accuracy: 0.9721
    Epoch 61/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1823 - accuracy: 0.9898 - val_loss: 0.5340 - val_accuracy: 0.9721
    Epoch 62/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2484 - accuracy: 0.9889 - val_loss: 0.3627 - val_accuracy: 0.9721
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2193 - accuracy: 0.9916 - val_loss: 0.2245 - val_accuracy: 0.9721
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3280 - accuracy: 0.9796 - val_loss: 1.2149 - val_accuracy: 0.9553
    Epoch 65/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2748 - accuracy: 0.9851 - val_loss: 0.8435 - val_accuracy: 0.9497
    Epoch 66/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3314 - accuracy: 0.9777 - val_loss: 1.9329 - val_accuracy: 0.9330
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1837 - accuracy: 0.9833 - val_loss: 1.7906 - val_accuracy: 0.9385
    Epoch 68/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3798 - accuracy: 0.9833 - val_loss: 2.0659 - val_accuracy: 0.9609
    Epoch 69/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3683 - accuracy: 0.9861 - val_loss: 0.9179 - val_accuracy: 0.9665
    Epoch 70/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1896 - accuracy: 0.9842 - val_loss: 1.1103 - val_accuracy: 0.9553
    Epoch 71/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1851 - accuracy: 0.9889 - val_loss: 1.8848 - val_accuracy: 0.9330
    Epoch 72/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4576 - accuracy: 0.9731 - val_loss: 1.1913 - val_accuracy: 0.9553
    Epoch 73/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1344 - accuracy: 0.9907 - val_loss: 1.8224 - val_accuracy: 0.9553
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1833 - accuracy: 0.9861 - val_loss: 1.4314 - val_accuracy: 0.9665
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3891 - accuracy: 0.9824 - val_loss: 0.8459 - val_accuracy: 0.9665
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1848 - accuracy: 0.9824 - val_loss: 1.1709 - val_accuracy: 0.9609
    Epoch 77/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3854 - accuracy: 0.9796 - val_loss: 3.5431 - val_accuracy: 0.9441
    Epoch 78/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1689 - accuracy: 0.9870 - val_loss: 5.0120 - val_accuracy: 0.9162
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3326 - accuracy: 0.9814 - val_loss: 2.9493 - val_accuracy: 0.9385
    Epoch 80/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2378 - accuracy: 0.9814 - val_loss: 2.1131 - val_accuracy: 0.9609
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3740 - accuracy: 0.9824 - val_loss: 2.4431 - val_accuracy: 0.9665
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1936 - accuracy: 0.9889 - val_loss: 2.1799 - val_accuracy: 0.9609
    Epoch 83/100
    9/9 [==============================] - 12s 1s/step - loss: 0.0725 - accuracy: 0.9935 - val_loss: 2.4177 - val_accuracy: 0.9665
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1953 - accuracy: 0.9926 - val_loss: 2.5137 - val_accuracy: 0.9721
    Epoch 85/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1783 - accuracy: 0.9889 - val_loss: 1.4350 - val_accuracy: 0.9665
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2352 - accuracy: 0.9833 - val_loss: 2.4974 - val_accuracy: 0.9665
    Epoch 87/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1929 - accuracy: 0.9898 - val_loss: 3.3848 - val_accuracy: 0.9609
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3353 - accuracy: 0.9879 - val_loss: 1.8125 - val_accuracy: 0.9609
    Epoch 89/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2630 - accuracy: 0.9870 - val_loss: 0.9388 - val_accuracy: 0.9777
    Epoch 90/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2412 - accuracy: 0.9879 - val_loss: 1.6571 - val_accuracy: 0.9721
    Epoch 91/100
    9/9 [==============================] - 12s 1s/step - loss: 0.0923 - accuracy: 0.9916 - val_loss: 1.5810 - val_accuracy: 0.9721
    Epoch 92/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2826 - accuracy: 0.9889 - val_loss: 1.0687 - val_accuracy: 0.9777
    Epoch 93/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2045 - accuracy: 0.9907 - val_loss: 1.1615 - val_accuracy: 0.9721
    Epoch 94/100
    9/9 [==============================] - 12s 1s/step - loss: 0.1471 - accuracy: 0.9944 - val_loss: 1.9422 - val_accuracy: 0.9665
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3994 - accuracy: 0.9851 - val_loss: 1.9777 - val_accuracy: 0.9777
    Epoch 96/100
    9/9 [==============================] - 12s 1s/step - loss: 0.0451 - accuracy: 0.9963 - val_loss: 2.1159 - val_accuracy: 0.9721
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1919 - accuracy: 0.9926 - val_loss: 1.6333 - val_accuracy: 0.9721
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2525 - accuracy: 0.9851 - val_loss: 1.0113 - val_accuracy: 0.9777
    Epoch 99/100
    9/9 [==============================] - 9s 1s/step - loss: 0.1471 - accuracy: 0.9907 - val_loss: 1.7497 - val_accuracy: 0.9665
    Epoch 100/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2141 - accuracy: 0.9907 - val_loss: 2.8788 - val_accuracy: 0.9497
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/135.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 1.8277 - accuracy: 0.9443

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.8276941776275635, 0.9443413615226746]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9443413729128015
                  precision    recall  f1-score   support

               0       0.97      0.84      0.90       158
               1       0.94      0.99      0.96       381

        accuracy                           0.94       539
       macro avg       0.95      0.91      0.93       539
    weighted avg       0.95      0.94      0.94       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/136.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/137.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/138.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.97      0.84      0.90       158
               1       0.94      0.99      0.96       381

        accuracy                           0.94       539
       macro avg       0.95      0.91      0.93       539
    weighted avg       0.95      0.94      0.94       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.97      0.84      0.99      0.90      0.91      0.81       158
              1       0.94      0.99      0.84      0.96      0.91      0.84       381

    avg / total       0.95      0.94      0.88      0.94      0.91      0.83       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
