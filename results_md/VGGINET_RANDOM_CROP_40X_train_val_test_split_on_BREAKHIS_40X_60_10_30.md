<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'final00RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.8417 - accuracy: 0.6484WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 19s 2s/step - loss: 4.1952 - accuracy: 0.7363 - val_loss: 6.7694 - val_accuracy: 0.8827
    Epoch 2/100
    9/9 [==============================] - 14s 2s/step - loss: 2.0953 - accuracy: 0.8533 - val_loss: 4.3984 - val_accuracy: 0.8659
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.5277 - accuracy: 0.8839 - val_loss: 8.0922 - val_accuracy: 0.7989
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2104 - accuracy: 0.8812 - val_loss: 1.3773 - val_accuracy: 0.9274
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1172 - accuracy: 0.8849 - val_loss: 1.3315 - val_accuracy: 0.8939
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0188 - accuracy: 0.9090 - val_loss: 1.1042 - val_accuracy: 0.9330
    Epoch 7/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6771 - accuracy: 0.9322 - val_loss: 0.9307 - val_accuracy: 0.9218
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6627 - accuracy: 0.9257 - val_loss: 1.4628 - val_accuracy: 0.9274
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7257 - accuracy: 0.9276 - val_loss: 1.3673 - val_accuracy: 0.9553
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7300 - accuracy: 0.9359 - val_loss: 3.4185 - val_accuracy: 0.8715
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5550 - accuracy: 0.9350 - val_loss: 1.2020 - val_accuracy: 0.9441
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6453 - accuracy: 0.9369 - val_loss: 0.9869 - val_accuracy: 0.9385
    Epoch 13/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4452 - accuracy: 0.9554 - val_loss: 2.0005 - val_accuracy: 0.9274
    Epoch 14/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5588 - accuracy: 0.9452 - val_loss: 1.1739 - val_accuracy: 0.9497
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4125 - accuracy: 0.9554 - val_loss: 2.0051 - val_accuracy: 0.9274
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5469 - accuracy: 0.9434 - val_loss: 1.6027 - val_accuracy: 0.9330
    Epoch 17/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6017 - accuracy: 0.9378 - val_loss: 2.5704 - val_accuracy: 0.9162
    Epoch 18/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5779 - accuracy: 0.9452 - val_loss: 1.6225 - val_accuracy: 0.9441
    Epoch 19/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5556 - accuracy: 0.9517 - val_loss: 1.7506 - val_accuracy: 0.9218
    Epoch 20/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3989 - accuracy: 0.9582 - val_loss: 2.8220 - val_accuracy: 0.8994
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3579 - accuracy: 0.9629 - val_loss: 1.9918 - val_accuracy: 0.9274
    Epoch 22/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3417 - accuracy: 0.9619 - val_loss: 1.9274 - val_accuracy: 0.9274
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2896 - accuracy: 0.9656 - val_loss: 1.7959 - val_accuracy: 0.9385
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4446 - accuracy: 0.9582 - val_loss: 1.0832 - val_accuracy: 0.9553
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6800 - accuracy: 0.9545 - val_loss: 0.9100 - val_accuracy: 0.9385
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3832 - accuracy: 0.9647 - val_loss: 2.0620 - val_accuracy: 0.9106
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5301 - accuracy: 0.9619 - val_loss: 1.6563 - val_accuracy: 0.9330
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3149 - accuracy: 0.9666 - val_loss: 2.0611 - val_accuracy: 0.9385
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4782 - accuracy: 0.9675 - val_loss: 1.6478 - val_accuracy: 0.9441
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5308 - accuracy: 0.9629 - val_loss: 1.5291 - val_accuracy: 0.9330
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2675 - accuracy: 0.9721 - val_loss: 2.1734 - val_accuracy: 0.9385
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4638 - accuracy: 0.9610 - val_loss: 1.3407 - val_accuracy: 0.9330
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4667 - accuracy: 0.9647 - val_loss: 0.9611 - val_accuracy: 0.9497
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5877 - accuracy: 0.9619 - val_loss: 1.4747 - val_accuracy: 0.9385
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4006 - accuracy: 0.9712 - val_loss: 1.5718 - val_accuracy: 0.9162
    Epoch 36/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6614 - accuracy: 0.9582 - val_loss: 1.2388 - val_accuracy: 0.9385
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4492 - accuracy: 0.9601 - val_loss: 1.0025 - val_accuracy: 0.9553
    Epoch 38/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3498 - accuracy: 0.9712 - val_loss: 1.2870 - val_accuracy: 0.9385
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3781 - accuracy: 0.9694 - val_loss: 0.9834 - val_accuracy: 0.9553
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1915 - accuracy: 0.9861 - val_loss: 0.9159 - val_accuracy: 0.9609
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3047 - accuracy: 0.9768 - val_loss: 0.7433 - val_accuracy: 0.9609
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4279 - accuracy: 0.9703 - val_loss: 1.3354 - val_accuracy: 0.9385
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9152 - accuracy: 0.9610 - val_loss: 1.2194 - val_accuracy: 0.9553
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2273 - accuracy: 0.9721 - val_loss: 1.2929 - val_accuracy: 0.9441
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3806 - accuracy: 0.9731 - val_loss: 1.4839 - val_accuracy: 0.9497
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3209 - accuracy: 0.9768 - val_loss: 1.5390 - val_accuracy: 0.9441
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2380 - accuracy: 0.9777 - val_loss: 1.2654 - val_accuracy: 0.9441
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2542 - accuracy: 0.9814 - val_loss: 1.1287 - val_accuracy: 0.9665
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4163 - accuracy: 0.9731 - val_loss: 1.1014 - val_accuracy: 0.9497
    Epoch 50/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2296 - accuracy: 0.9777 - val_loss: 1.1708 - val_accuracy: 0.9441
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4872 - accuracy: 0.9731 - val_loss: 0.9796 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3612 - accuracy: 0.9796 - val_loss: 1.8523 - val_accuracy: 0.9441
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4653 - accuracy: 0.9777 - val_loss: 1.4465 - val_accuracy: 0.9330
    Epoch 54/100
    9/9 [==============================] - 18s 2s/step - loss: 0.2595 - accuracy: 0.9786 - val_loss: 2.1710 - val_accuracy: 0.9441
    Epoch 55/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2119 - accuracy: 0.9805 - val_loss: 1.5944 - val_accuracy: 0.9497
    Epoch 56/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3080 - accuracy: 0.9759 - val_loss: 1.3711 - val_accuracy: 0.9441
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3467 - accuracy: 0.9786 - val_loss: 1.9364 - val_accuracy: 0.9050
    Epoch 58/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3639 - accuracy: 0.9796 - val_loss: 1.4368 - val_accuracy: 0.9330
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3805 - accuracy: 0.9740 - val_loss: 1.5360 - val_accuracy: 0.9441
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3262 - accuracy: 0.9796 - val_loss: 1.2531 - val_accuracy: 0.9609
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2509 - accuracy: 0.9879 - val_loss: 0.7572 - val_accuracy: 0.9553
    Epoch 62/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2567 - accuracy: 0.9842 - val_loss: 0.9383 - val_accuracy: 0.9609
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4103 - accuracy: 0.9768 - val_loss: 1.1974 - val_accuracy: 0.9553
    Epoch 64/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1850 - accuracy: 0.9879 - val_loss: 1.0624 - val_accuracy: 0.9497
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2748 - accuracy: 0.9796 - val_loss: 1.5548 - val_accuracy: 0.9385
    Epoch 66/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2331 - accuracy: 0.9861 - val_loss: 1.2242 - val_accuracy: 0.9497
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0875 - accuracy: 0.9944 - val_loss: 1.6555 - val_accuracy: 0.9497
    Epoch 68/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1109 - accuracy: 0.9851 - val_loss: 1.2281 - val_accuracy: 0.9553
    Epoch 69/100
    9/9 [==============================] - 18s 2s/step - loss: 0.1214 - accuracy: 0.9907 - val_loss: 0.6827 - val_accuracy: 0.9721
    Epoch 70/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0569 - accuracy: 0.9944 - val_loss: 1.0084 - val_accuracy: 0.9665
    Epoch 71/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2307 - accuracy: 0.9842 - val_loss: 0.8499 - val_accuracy: 0.9665
    Epoch 72/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2131 - accuracy: 0.9833 - val_loss: 0.7324 - val_accuracy: 0.9553
    Epoch 73/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2097 - accuracy: 0.9842 - val_loss: 1.1158 - val_accuracy: 0.9665
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2749 - accuracy: 0.9842 - val_loss: 1.3655 - val_accuracy: 0.9553
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1875 - accuracy: 0.9851 - val_loss: 2.3623 - val_accuracy: 0.9385
    Epoch 76/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2496 - accuracy: 0.9814 - val_loss: 4.0655 - val_accuracy: 0.8994
    Epoch 77/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2900 - accuracy: 0.9814 - val_loss: 1.5812 - val_accuracy: 0.9441
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2451 - accuracy: 0.9879 - val_loss: 1.0011 - val_accuracy: 0.9721
    Epoch 79/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2960 - accuracy: 0.9824 - val_loss: 0.7735 - val_accuracy: 0.9665
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1049 - accuracy: 0.9916 - val_loss: 0.9366 - val_accuracy: 0.9609
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0903 - accuracy: 0.9935 - val_loss: 1.1193 - val_accuracy: 0.9665
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2472 - accuracy: 0.9833 - val_loss: 0.8242 - val_accuracy: 0.9665
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2058 - accuracy: 0.9851 - val_loss: 1.3813 - val_accuracy: 0.9609
    Epoch 84/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2913 - accuracy: 0.9842 - val_loss: 2.0145 - val_accuracy: 0.9497
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1436 - accuracy: 0.9926 - val_loss: 0.9344 - val_accuracy: 0.9609
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2513 - accuracy: 0.9805 - val_loss: 0.7192 - val_accuracy: 0.9665
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0995 - accuracy: 0.9916 - val_loss: 0.7284 - val_accuracy: 0.9609
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1249 - accuracy: 0.9898 - val_loss: 0.8893 - val_accuracy: 0.9665
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2611 - accuracy: 0.9861 - val_loss: 1.3741 - val_accuracy: 0.9609
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2953 - accuracy: 0.9851 - val_loss: 0.6767 - val_accuracy: 0.9777
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1613 - accuracy: 0.9870 - val_loss: 0.5902 - val_accuracy: 0.9665
    Epoch 92/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3579 - accuracy: 0.9749 - val_loss: 1.0936 - val_accuracy: 0.9721
    Epoch 93/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2937 - accuracy: 0.9814 - val_loss: 1.1054 - val_accuracy: 0.9665
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1922 - accuracy: 0.9861 - val_loss: 1.1285 - val_accuracy: 0.9665
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2580 - accuracy: 0.9870 - val_loss: 2.0488 - val_accuracy: 0.9497
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4481 - accuracy: 0.9796 - val_loss: 0.9394 - val_accuracy: 0.9553
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2514 - accuracy: 0.9889 - val_loss: 5.0373 - val_accuracy: 0.8883
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4540 - accuracy: 0.9777 - val_loss: 1.1124 - val_accuracy: 0.9665
    Epoch 99/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1895 - accuracy: 0.9861 - val_loss: 1.4322 - val_accuracy: 0.9553
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2576 - accuracy: 0.9842 - val_loss: 0.7845 - val_accuracy: 0.9665
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/199.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 14s 3s/step - loss: 0.8323 - accuracy: 0.9777

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.8322771787643433, 0.9777365326881409]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9777365491651205
                  precision    recall  f1-score   support

               0       0.95      0.97      0.96       158
               1       0.99      0.98      0.98       381

        accuracy                           0.98       539
       macro avg       0.97      0.98      0.97       539
    weighted avg       0.98      0.98      0.98       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/200.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/201.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/202.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.97      0.96       158
               1       0.99      0.98      0.98       381

        accuracy                           0.98       539
       macro avg       0.97      0.98      0.97       539
    weighted avg       0.98      0.98      0.98       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.97      0.98      0.96      0.98      0.95       158
              1       0.99      0.98      0.97      0.98      0.98      0.95       381

    avg / total       0.98      0.98      0.98      0.98      0.98      0.95       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
