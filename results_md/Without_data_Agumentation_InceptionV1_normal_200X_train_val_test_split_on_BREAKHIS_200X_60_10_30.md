<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/InceptionV1-NORMAL-RANDOMCROP_200X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.8887 - accuracy: 0.4922WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 11s 1s/step - loss: 0.7251 - accuracy: 0.6421 - val_loss: 0.5444 - val_accuracy: 0.7403
    Epoch 2/50
    9/9 [==============================] - 5s 523ms/step - loss: 0.5536 - accuracy: 0.7231 - val_loss: 0.4544 - val_accuracy: 0.7845
    Epoch 3/50
    9/9 [==============================] - 5s 534ms/step - loss: 0.4933 - accuracy: 0.7792 - val_loss: 0.4034 - val_accuracy: 0.8011
    Epoch 4/50
    9/9 [==============================] - 5s 535ms/step - loss: 0.4502 - accuracy: 0.8059 - val_loss: 0.3720 - val_accuracy: 0.8398
    Epoch 5/50
    9/9 [==============================] - 5s 520ms/step - loss: 0.4262 - accuracy: 0.8188 - val_loss: 0.3509 - val_accuracy: 0.8453
    Epoch 6/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.3940 - accuracy: 0.8326 - val_loss: 0.3349 - val_accuracy: 0.8564
    Epoch 7/50
    9/9 [==============================] - 5s 532ms/step - loss: 0.3860 - accuracy: 0.8252 - val_loss: 0.3252 - val_accuracy: 0.8619
    Epoch 8/50
    9/9 [==============================] - 5s 523ms/step - loss: 0.3835 - accuracy: 0.8372 - val_loss: 0.3140 - val_accuracy: 0.8674
    Epoch 9/50
    9/9 [==============================] - 5s 524ms/step - loss: 0.3619 - accuracy: 0.8436 - val_loss: 0.3035 - val_accuracy: 0.8785
    Epoch 10/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.3593 - accuracy: 0.8344 - val_loss: 0.2935 - val_accuracy: 0.8840
    Epoch 11/50
    9/9 [==============================] - 5s 529ms/step - loss: 0.3386 - accuracy: 0.8565 - val_loss: 0.2857 - val_accuracy: 0.8895
    Epoch 12/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.3407 - accuracy: 0.8602 - val_loss: 0.2800 - val_accuracy: 0.8950
    Epoch 13/50
    9/9 [==============================] - 5s 521ms/step - loss: 0.3217 - accuracy: 0.8602 - val_loss: 0.2747 - val_accuracy: 0.8895
    Epoch 14/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.3158 - accuracy: 0.8758 - val_loss: 0.2689 - val_accuracy: 0.9006
    Epoch 15/50
    9/9 [==============================] - 5s 533ms/step - loss: 0.3043 - accuracy: 0.8822 - val_loss: 0.2663 - val_accuracy: 0.8950
    Epoch 16/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.3019 - accuracy: 0.8721 - val_loss: 0.2609 - val_accuracy: 0.9061
    Epoch 17/50
    9/9 [==============================] - 5s 523ms/step - loss: 0.3131 - accuracy: 0.8675 - val_loss: 0.2595 - val_accuracy: 0.9006
    Epoch 18/50
    9/9 [==============================] - 5s 530ms/step - loss: 0.3034 - accuracy: 0.8712 - val_loss: 0.2554 - val_accuracy: 0.9006
    Epoch 19/50
    9/9 [==============================] - 5s 526ms/step - loss: 0.2799 - accuracy: 0.8868 - val_loss: 0.2519 - val_accuracy: 0.9006
    Epoch 20/50
    9/9 [==============================] - 5s 534ms/step - loss: 0.2807 - accuracy: 0.8804 - val_loss: 0.2498 - val_accuracy: 0.8895
    Epoch 21/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2905 - accuracy: 0.8749 - val_loss: 0.2472 - val_accuracy: 0.9006
    Epoch 22/50
    9/9 [==============================] - 5s 533ms/step - loss: 0.2764 - accuracy: 0.8859 - val_loss: 0.2451 - val_accuracy: 0.9006
    Epoch 23/50
    9/9 [==============================] - 5s 532ms/step - loss: 0.2831 - accuracy: 0.8822 - val_loss: 0.2436 - val_accuracy: 0.9006
    Epoch 24/50
    9/9 [==============================] - 5s 531ms/step - loss: 0.2775 - accuracy: 0.8850 - val_loss: 0.2423 - val_accuracy: 0.9006
    Epoch 25/50
    9/9 [==============================] - 5s 534ms/step - loss: 0.2849 - accuracy: 0.8841 - val_loss: 0.2409 - val_accuracy: 0.9006
    Epoch 26/50
    9/9 [==============================] - 5s 529ms/step - loss: 0.2719 - accuracy: 0.8905 - val_loss: 0.2388 - val_accuracy: 0.9006
    Epoch 27/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2606 - accuracy: 0.8905 - val_loss: 0.2372 - val_accuracy: 0.9006
    Epoch 28/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2592 - accuracy: 0.8896 - val_loss: 0.2361 - val_accuracy: 0.9006
    Epoch 29/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.2449 - accuracy: 0.9089 - val_loss: 0.2348 - val_accuracy: 0.9006
    Epoch 30/50
    9/9 [==============================] - 5s 532ms/step - loss: 0.2486 - accuracy: 0.9025 - val_loss: 0.2329 - val_accuracy: 0.9061
    Epoch 31/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.2387 - accuracy: 0.9098 - val_loss: 0.2306 - val_accuracy: 0.9006
    Epoch 32/50
    9/9 [==============================] - 5s 532ms/step - loss: 0.2576 - accuracy: 0.8970 - val_loss: 0.2290 - val_accuracy: 0.9006
    Epoch 33/50
    9/9 [==============================] - 5s 530ms/step - loss: 0.2495 - accuracy: 0.8933 - val_loss: 0.2276 - val_accuracy: 0.9061
    Epoch 34/50
    9/9 [==============================] - 5s 531ms/step - loss: 0.2465 - accuracy: 0.9043 - val_loss: 0.2258 - val_accuracy: 0.9006
    Epoch 35/50
    9/9 [==============================] - 5s 531ms/step - loss: 0.2289 - accuracy: 0.9117 - val_loss: 0.2236 - val_accuracy: 0.9006
    Epoch 36/50
    9/9 [==============================] - 5s 529ms/step - loss: 0.2361 - accuracy: 0.9108 - val_loss: 0.2231 - val_accuracy: 0.9061
    Epoch 37/50
    9/9 [==============================] - 5s 534ms/step - loss: 0.2447 - accuracy: 0.8979 - val_loss: 0.2220 - val_accuracy: 0.9006
    Epoch 38/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2388 - accuracy: 0.8997 - val_loss: 0.2227 - val_accuracy: 0.9006
    Epoch 39/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2363 - accuracy: 0.9025 - val_loss: 0.2206 - val_accuracy: 0.9061
    Epoch 40/50
    9/9 [==============================] - 5s 531ms/step - loss: 0.2396 - accuracy: 0.9025 - val_loss: 0.2204 - val_accuracy: 0.9061
    Epoch 41/50
    9/9 [==============================] - 5s 528ms/step - loss: 0.2431 - accuracy: 0.8951 - val_loss: 0.2210 - val_accuracy: 0.9061
    Epoch 42/50
    9/9 [==============================] - 5s 526ms/step - loss: 0.2331 - accuracy: 0.9080 - val_loss: 0.2206 - val_accuracy: 0.9061
    Epoch 43/50
    9/9 [==============================] - 5s 530ms/step - loss: 0.2258 - accuracy: 0.9108 - val_loss: 0.2210 - val_accuracy: 0.9006
    Epoch 44/50
    9/9 [==============================] - 5s 522ms/step - loss: 0.2189 - accuracy: 0.9218 - val_loss: 0.2204 - val_accuracy: 0.9006
    Epoch 45/50
    9/9 [==============================] - 5s 524ms/step - loss: 0.2322 - accuracy: 0.9043 - val_loss: 0.2183 - val_accuracy: 0.9006
    Epoch 46/50
    9/9 [==============================] - 5s 525ms/step - loss: 0.2257 - accuracy: 0.9089 - val_loss: 0.2184 - val_accuracy: 0.9061
    Epoch 47/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.2239 - accuracy: 0.9135 - val_loss: 0.2169 - val_accuracy: 0.9061
    Epoch 48/50
    9/9 [==============================] - 5s 527ms/step - loss: 0.2311 - accuracy: 0.8970 - val_loss: 0.2161 - val_accuracy: 0.9061
    Epoch 49/50
    9/9 [==============================] - 5s 522ms/step - loss: 0.2351 - accuracy: 0.9034 - val_loss: 0.2163 - val_accuracy: 0.9006
    Epoch 50/50
    9/9 [==============================] - 5s 526ms/step - loss: 0.2352 - accuracy: 0.8988 - val_loss: 0.2159 - val_accuracy: 0.9006

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/203.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 28s 6s/step - loss: 0.2584 - accuracy: 0.9028

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.2583877742290497, 0.9027522802352905]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9027522935779817
                  precision    recall  f1-score   support

               0       0.86      0.80      0.83       158
               1       0.92      0.95      0.93       387

        accuracy                           0.90       545
       macro avg       0.89      0.87      0.88       545
    weighted avg       0.90      0.90      0.90       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: numpy&gt;=1.11.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/204.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/205.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/206.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.86      0.80      0.83       158
               1       0.92      0.95      0.93       387

        accuracy                           0.90       545
       macro avg       0.89      0.87      0.88       545
    weighted avg       0.90      0.90      0.90       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.86      0.80      0.95      0.83      0.87      0.74       158
              1       0.92      0.95      0.80      0.93      0.87      0.77       387

    avg / total       0.90      0.90      0.84      0.90      0.87      0.76       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
