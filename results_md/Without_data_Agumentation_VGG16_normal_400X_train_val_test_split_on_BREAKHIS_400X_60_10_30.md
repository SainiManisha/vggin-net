<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/VGG16-NORMAL-RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    #if train:
       # ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    #ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/8 [==&gt;...........................] - ETA: 0s - loss: 0.9237 - accuracy: 0.4453WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 12s 2s/step - loss: 36.2487 - accuracy: 0.5139 - val_loss: 1.4359 - val_accuracy: 0.7019
    Epoch 2/50
    8/8 [==============================] - 6s 755ms/step - loss: 0.9473 - accuracy: 0.6766 - val_loss: 0.8131 - val_accuracy: 0.4037
    Epoch 3/50
    8/8 [==============================] - 6s 761ms/step - loss: 0.4760 - accuracy: 0.7652 - val_loss: 0.2807 - val_accuracy: 0.9130
    Epoch 4/50
    8/8 [==============================] - 6s 761ms/step - loss: 0.2863 - accuracy: 0.8970 - val_loss: 0.3949 - val_accuracy: 0.8696
    Epoch 5/50
    8/8 [==============================] - 6s 771ms/step - loss: 0.0942 - accuracy: 0.9640 - val_loss: 0.2606 - val_accuracy: 0.9130
    Epoch 6/50
    8/8 [==============================] - 6s 783ms/step - loss: 0.0319 - accuracy: 0.9938 - val_loss: 0.4485 - val_accuracy: 0.8882
    Epoch 7/50
    8/8 [==============================] - 6s 789ms/step - loss: 0.0060 - accuracy: 1.0000 - val_loss: 0.3420 - val_accuracy: 0.9130
    Epoch 8/50
    8/8 [==============================] - 6s 782ms/step - loss: 0.0016 - accuracy: 1.0000 - val_loss: 0.3306 - val_accuracy: 0.9255
    Epoch 9/50
    8/8 [==============================] - 6s 786ms/step - loss: 7.0946e-04 - accuracy: 1.0000 - val_loss: 0.4155 - val_accuracy: 0.9193
    Epoch 10/50
    8/8 [==============================] - 6s 778ms/step - loss: 3.0512e-04 - accuracy: 1.0000 - val_loss: 0.4136 - val_accuracy: 0.9193
    Epoch 11/50
    8/8 [==============================] - 6s 773ms/step - loss: 1.6812e-04 - accuracy: 1.0000 - val_loss: 0.3811 - val_accuracy: 0.9255
    Epoch 12/50
    8/8 [==============================] - 6s 788ms/step - loss: 9.0740e-05 - accuracy: 1.0000 - val_loss: 0.4116 - val_accuracy: 0.9193
    Epoch 13/50
    8/8 [==============================] - 6s 757ms/step - loss: 4.6665e-05 - accuracy: 1.0000 - val_loss: 0.4794 - val_accuracy: 0.9193
    Epoch 14/50
    8/8 [==============================] - 6s 754ms/step - loss: 2.3495e-05 - accuracy: 1.0000 - val_loss: 0.4639 - val_accuracy: 0.9193
    Epoch 15/50
    8/8 [==============================] - 6s 753ms/step - loss: 1.4158e-05 - accuracy: 1.0000 - val_loss: 0.4845 - val_accuracy: 0.9193
    Epoch 16/50
    8/8 [==============================] - 6s 757ms/step - loss: 9.7958e-06 - accuracy: 1.0000 - val_loss: 0.4941 - val_accuracy: 0.9193
    Epoch 17/50
    8/8 [==============================] - 6s 750ms/step - loss: 7.3553e-06 - accuracy: 1.0000 - val_loss: 0.5119 - val_accuracy: 0.9193
    Epoch 18/50
    8/8 [==============================] - 6s 756ms/step - loss: 5.7975e-06 - accuracy: 1.0000 - val_loss: 0.5032 - val_accuracy: 0.9130
    Epoch 19/50
    8/8 [==============================] - 6s 752ms/step - loss: 4.7809e-06 - accuracy: 1.0000 - val_loss: 0.5310 - val_accuracy: 0.9193
    Epoch 20/50
    8/8 [==============================] - 6s 756ms/step - loss: 4.0488e-06 - accuracy: 1.0000 - val_loss: 0.5273 - val_accuracy: 0.9193
    Epoch 21/50
    8/8 [==============================] - 6s 753ms/step - loss: 3.5062e-06 - accuracy: 1.0000 - val_loss: 0.5325 - val_accuracy: 0.9193
    Epoch 22/50
    8/8 [==============================] - 6s 753ms/step - loss: 3.0726e-06 - accuracy: 1.0000 - val_loss: 0.5518 - val_accuracy: 0.9193
    Epoch 23/50
    8/8 [==============================] - 6s 753ms/step - loss: 2.7169e-06 - accuracy: 1.0000 - val_loss: 0.5420 - val_accuracy: 0.9193
    Epoch 24/50
    8/8 [==============================] - 6s 754ms/step - loss: 2.4069e-06 - accuracy: 1.0000 - val_loss: 0.5583 - val_accuracy: 0.9193
    Epoch 25/50
    8/8 [==============================] - 6s 767ms/step - loss: 2.1552e-06 - accuracy: 1.0000 - val_loss: 0.5554 - val_accuracy: 0.9193
    Epoch 26/50
    8/8 [==============================] - 6s 761ms/step - loss: 1.9568e-06 - accuracy: 1.0000 - val_loss: 0.5553 - val_accuracy: 0.9193
    Epoch 27/50
    8/8 [==============================] - 6s 762ms/step - loss: 1.7755e-06 - accuracy: 1.0000 - val_loss: 0.5684 - val_accuracy: 0.9193
    Epoch 28/50
    8/8 [==============================] - 6s 749ms/step - loss: 1.6131e-06 - accuracy: 1.0000 - val_loss: 0.5650 - val_accuracy: 0.9193
    Epoch 29/50
    8/8 [==============================] - 6s 739ms/step - loss: 1.4785e-06 - accuracy: 1.0000 - val_loss: 0.5744 - val_accuracy: 0.9193
    Epoch 30/50
    8/8 [==============================] - 6s 739ms/step - loss: 1.3530e-06 - accuracy: 1.0000 - val_loss: 0.5772 - val_accuracy: 0.9193
    Epoch 31/50
    8/8 [==============================] - 6s 733ms/step - loss: 1.2524e-06 - accuracy: 1.0000 - val_loss: 0.5768 - val_accuracy: 0.9193
    Epoch 32/50
    8/8 [==============================] - 6s 727ms/step - loss: 1.1646e-06 - accuracy: 1.0000 - val_loss: 0.5810 - val_accuracy: 0.9193
    Epoch 33/50
    8/8 [==============================] - 6s 727ms/step - loss: 1.0807e-06 - accuracy: 1.0000 - val_loss: 0.5838 - val_accuracy: 0.9193
    Epoch 34/50
    8/8 [==============================] - 6s 738ms/step - loss: 1.0024e-06 - accuracy: 1.0000 - val_loss: 0.5833 - val_accuracy: 0.9193
    Epoch 35/50
    8/8 [==============================] - 6s 727ms/step - loss: 9.3661e-07 - accuracy: 1.0000 - val_loss: 0.5906 - val_accuracy: 0.9193
    Epoch 36/50
    8/8 [==============================] - 6s 724ms/step - loss: 8.7878e-07 - accuracy: 1.0000 - val_loss: 0.5920 - val_accuracy: 0.9193
    Epoch 37/50
    8/8 [==============================] - 6s 727ms/step - loss: 8.2341e-07 - accuracy: 1.0000 - val_loss: 0.5951 - val_accuracy: 0.9193
    Epoch 38/50
    8/8 [==============================] - 6s 723ms/step - loss: 7.7381e-07 - accuracy: 1.0000 - val_loss: 0.5971 - val_accuracy: 0.9193
    Epoch 39/50
    8/8 [==============================] - 6s 727ms/step - loss: 7.2986e-07 - accuracy: 1.0000 - val_loss: 0.5973 - val_accuracy: 0.9193
    Epoch 40/50
    8/8 [==============================] - 6s 722ms/step - loss: 6.8395e-07 - accuracy: 1.0000 - val_loss: 0.6044 - val_accuracy: 0.9193
    Epoch 41/50
    8/8 [==============================] - 6s 731ms/step - loss: 6.4712e-07 - accuracy: 1.0000 - val_loss: 0.6082 - val_accuracy: 0.9193
    Epoch 42/50
    8/8 [==============================] - 6s 719ms/step - loss: 6.1065e-07 - accuracy: 1.0000 - val_loss: 0.6062 - val_accuracy: 0.9193
    Epoch 43/50
    8/8 [==============================] - 6s 719ms/step - loss: 5.7861e-07 - accuracy: 1.0000 - val_loss: 0.6115 - val_accuracy: 0.9193
    Epoch 44/50
    8/8 [==============================] - 6s 725ms/step - loss: 5.4853e-07 - accuracy: 1.0000 - val_loss: 0.6110 - val_accuracy: 0.9193
    Epoch 45/50
    8/8 [==============================] - 6s 728ms/step - loss: 5.1944e-07 - accuracy: 1.0000 - val_loss: 0.6148 - val_accuracy: 0.9193
    Epoch 46/50
    8/8 [==============================] - 6s 711ms/step - loss: 4.9304e-07 - accuracy: 1.0000 - val_loss: 0.6174 - val_accuracy: 0.9193
    Epoch 47/50
    8/8 [==============================] - 6s 720ms/step - loss: 4.6616e-07 - accuracy: 1.0000 - val_loss: 0.6190 - val_accuracy: 0.9193
    Epoch 48/50
    8/8 [==============================] - 6s 726ms/step - loss: 4.4651e-07 - accuracy: 1.0000 - val_loss: 0.6192 - val_accuracy: 0.9193
    Epoch 49/50
    8/8 [==============================] - 6s 717ms/step - loss: 4.2515e-07 - accuracy: 1.0000 - val_loss: 0.6219 - val_accuracy: 0.9193
    Epoch 50/50
    8/8 [==============================] - 6s 710ms/step - loss: 4.0735e-07 - accuracy: 1.0000 - val_loss: 0.6294 - val_accuracy: 0.9193

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/91.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 13s 3s/step - loss: 1.0732 - accuracy: 0.8607

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.073183298110962, 0.8606557250022888]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.860655737704918
                  precision    recall  f1-score   support

               0       0.85      0.66      0.74       148
               1       0.86      0.95      0.90       340

        accuracy                           0.86       488
       macro avg       0.86      0.80      0.82       488
    weighted avg       0.86      0.86      0.85       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/92.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/93.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/94.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.85      0.66      0.74       148
               1       0.86      0.95      0.90       340

        accuracy                           0.86       488
       macro avg       0.86      0.80      0.82       488
    weighted avg       0.86      0.86      0.85       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.85      0.66      0.95      0.74      0.79      0.60       148
              1       0.86      0.95      0.66      0.90      0.79      0.64       340

    avg / total       0.86      0.86      0.74      0.85      0.79      0.63       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
