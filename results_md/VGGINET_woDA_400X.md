```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = '1'
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

    ('400X', '1')



```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________



```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________



```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```


```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```


```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```


```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```


```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________



```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```


```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```


```python
name = 'VGGINetWODA/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```


```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```


```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```


```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     # NO DATA AUGMENTATION WILL BE USED FOR THIS EXPERIMENT!
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.



```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

    Epoch 1/100
    1/8 [==>...........................] - ETA: 0s - loss: 1.1815 - accuracy: 0.4766WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 36s 5s/step - loss: 2.8510 - accuracy: 0.7467 - val_loss: 5.1316 - val_accuracy: 0.8447
    Epoch 2/100
    8/8 [==============================] - 9s 1s/step - loss: 0.9748 - accuracy: 0.9228 - val_loss: 7.6633 - val_accuracy: 0.8634
    Epoch 3/100
    8/8 [==============================] - 9s 1s/step - loss: 0.2318 - accuracy: 0.9712 - val_loss: 2.8474 - val_accuracy: 0.8820
    Epoch 4/100
    8/8 [==============================] - 9s 1s/step - loss: 0.1167 - accuracy: 0.9876 - val_loss: 4.2122 - val_accuracy: 0.9006
    Epoch 5/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0476 - accuracy: 0.9938 - val_loss: 3.9979 - val_accuracy: 0.9006
    Epoch 6/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0136 - accuracy: 0.9969 - val_loss: 3.5669 - val_accuracy: 0.9068
    Epoch 7/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0038 - accuracy: 0.9979 - val_loss: 3.1926 - val_accuracy: 0.9130
    Epoch 8/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0047 - accuracy: 0.9990 - val_loss: 2.8416 - val_accuracy: 0.9130
    Epoch 9/100
    8/8 [==============================] - 9s 1s/step - loss: 8.9132e-06 - accuracy: 1.0000 - val_loss: 2.7090 - val_accuracy: 0.9193
    Epoch 10/100
    8/8 [==============================] - 9s 1s/step - loss: 7.3986e-06 - accuracy: 1.0000 - val_loss: 2.6095 - val_accuracy: 0.9193
    Epoch 11/100
    8/8 [==============================] - 9s 1s/step - loss: 9.3448e-05 - accuracy: 1.0000 - val_loss: 2.5216 - val_accuracy: 0.9193
    Epoch 12/100
    8/8 [==============================] - 9s 1s/step - loss: 2.7273e-06 - accuracy: 1.0000 - val_loss: 2.4461 - val_accuracy: 0.9193
    Epoch 13/100
    8/8 [==============================] - 9s 1s/step - loss: 1.2501e-04 - accuracy: 1.0000 - val_loss: 2.3614 - val_accuracy: 0.9193
    Epoch 14/100
    8/8 [==============================] - 9s 1s/step - loss: 3.5270e-06 - accuracy: 1.0000 - val_loss: 2.2954 - val_accuracy: 0.9193
    Epoch 15/100
    8/8 [==============================] - 9s 1s/step - loss: 5.9371e-06 - accuracy: 1.0000 - val_loss: 2.2402 - val_accuracy: 0.9193
    Epoch 16/100
    8/8 [==============================] - 9s 1s/step - loss: 7.1608e-05 - accuracy: 1.0000 - val_loss: 2.1900 - val_accuracy: 0.9193
    Epoch 17/100
    8/8 [==============================] - 9s 1s/step - loss: 2.2100e-04 - accuracy: 1.0000 - val_loss: 2.0998 - val_accuracy: 0.9193
    Epoch 18/100
    8/8 [==============================] - 9s 1s/step - loss: 1.8546e-05 - accuracy: 1.0000 - val_loss: 2.0319 - val_accuracy: 0.9193
    Epoch 19/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0017 - accuracy: 0.9990 - val_loss: 2.1753 - val_accuracy: 0.9193
    Epoch 20/100
    8/8 [==============================] - 9s 1s/step - loss: 3.0265e-06 - accuracy: 1.0000 - val_loss: 2.4620 - val_accuracy: 0.9130
    Epoch 21/100
    8/8 [==============================] - 9s 1s/step - loss: 0.0020 - accuracy: 0.9990 - val_loss: 2.5267 - val_accuracy: 0.9068
    Epoch 22/100
    8/8 [==============================] - 9s 1s/step - loss: 5.1189e-05 - accuracy: 1.0000 - val_loss: 2.4049 - val_accuracy: 0.9130
    Epoch 23/100
    8/8 [==============================] - 9s 1s/step - loss: 7.7532e-04 - accuracy: 0.9990 - val_loss: 2.3602 - val_accuracy: 0.9130
    Epoch 24/100
    8/8 [==============================] - 9s 1s/step - loss: 5.2615e-05 - accuracy: 1.0000 - val_loss: 2.3972 - val_accuracy: 0.9130
    Epoch 25/100
    8/8 [==============================] - 9s 1s/step - loss: 9.0762e-04 - accuracy: 0.9990 - val_loss: 2.2222 - val_accuracy: 0.9130
    Epoch 26/100
    8/8 [==============================] - 9s 1s/step - loss: 5.6560e-05 - accuracy: 1.0000 - val_loss: 2.0163 - val_accuracy: 0.9193
    Epoch 27/100
    8/8 [==============================] - 9s 1s/step - loss: 1.3230e-05 - accuracy: 1.0000 - val_loss: 1.9363 - val_accuracy: 0.9193
    Epoch 28/100
    8/8 [==============================] - 9s 1s/step - loss: 4.4160e-06 - accuracy: 1.0000 - val_loss: 1.9005 - val_accuracy: 0.9193
    Epoch 29/100
    8/8 [==============================] - 9s 1s/step - loss: 1.3945e-06 - accuracy: 1.0000 - val_loss: 1.8781 - val_accuracy: 0.9193
    Epoch 30/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7434e-06 - accuracy: 1.0000 - val_loss: 1.8599 - val_accuracy: 0.9193
    Epoch 31/100
    8/8 [==============================] - 9s 1s/step - loss: 8.9983e-06 - accuracy: 1.0000 - val_loss: 1.8456 - val_accuracy: 0.9193
    Epoch 32/100
    8/8 [==============================] - 10s 1s/step - loss: 9.8431e-06 - accuracy: 1.0000 - val_loss: 1.8354 - val_accuracy: 0.9193
    Epoch 33/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7835e-06 - accuracy: 1.0000 - val_loss: 1.8294 - val_accuracy: 0.9193
    Epoch 34/100
    8/8 [==============================] - 9s 1s/step - loss: 2.0093e-05 - accuracy: 1.0000 - val_loss: 1.8195 - val_accuracy: 0.9255
    Epoch 35/100
    8/8 [==============================] - 9s 1s/step - loss: 1.5852e-05 - accuracy: 1.0000 - val_loss: 1.8126 - val_accuracy: 0.9255
    Epoch 36/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1352e-04 - accuracy: 1.0000 - val_loss: 1.8125 - val_accuracy: 0.9193
    Epoch 37/100
    8/8 [==============================] - 9s 1s/step - loss: 1.6969e-04 - accuracy: 1.0000 - val_loss: 1.8538 - val_accuracy: 0.9193
    Epoch 38/100
    8/8 [==============================] - 9s 1s/step - loss: 3.5879e-06 - accuracy: 1.0000 - val_loss: 1.8868 - val_accuracy: 0.9193
    Epoch 39/100
    8/8 [==============================] - 9s 1s/step - loss: 1.4715e-06 - accuracy: 1.0000 - val_loss: 1.9004 - val_accuracy: 0.9255
    Epoch 40/100
    8/8 [==============================] - 9s 1s/step - loss: 2.7193e-06 - accuracy: 1.0000 - val_loss: 1.9021 - val_accuracy: 0.9255
    Epoch 41/100
    8/8 [==============================] - 9s 1s/step - loss: 6.9460e-07 - accuracy: 1.0000 - val_loss: 1.8990 - val_accuracy: 0.9255
    Epoch 42/100
    8/8 [==============================] - 9s 1s/step - loss: 3.9753e-04 - accuracy: 1.0000 - val_loss: 1.8769 - val_accuracy: 0.9255
    Epoch 43/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1061e-06 - accuracy: 1.0000 - val_loss: 1.8128 - val_accuracy: 0.9255
    Epoch 44/100
    8/8 [==============================] - 9s 1s/step - loss: 7.4264e-07 - accuracy: 1.0000 - val_loss: 1.7838 - val_accuracy: 0.9255
    Epoch 45/100
    8/8 [==============================] - 9s 1s/step - loss: 6.3721e-06 - accuracy: 1.0000 - val_loss: 1.7686 - val_accuracy: 0.9255
    Epoch 46/100
    8/8 [==============================] - 9s 1s/step - loss: 5.8665e-07 - accuracy: 1.0000 - val_loss: 1.7615 - val_accuracy: 0.9255
    Epoch 47/100
    8/8 [==============================] - 9s 1s/step - loss: 5.1460e-06 - accuracy: 1.0000 - val_loss: 1.7562 - val_accuracy: 0.9255
    Epoch 48/100
    8/8 [==============================] - 9s 1s/step - loss: 7.6426e-07 - accuracy: 1.0000 - val_loss: 1.7492 - val_accuracy: 0.9255
    Epoch 49/100
    8/8 [==============================] - 9s 1s/step - loss: 7.3634e-07 - accuracy: 1.0000 - val_loss: 1.7460 - val_accuracy: 0.9255
    Epoch 50/100
    8/8 [==============================] - 9s 1s/step - loss: 1.0390e-05 - accuracy: 1.0000 - val_loss: 1.7428 - val_accuracy: 0.9255
    Epoch 51/100
    8/8 [==============================] - 9s 1s/step - loss: 4.9193e-04 - accuracy: 1.0000 - val_loss: 1.7377 - val_accuracy: 0.9193
    Epoch 52/100
    8/8 [==============================] - 9s 1s/step - loss: 7.6452e-06 - accuracy: 1.0000 - val_loss: 1.7420 - val_accuracy: 0.9193
    Epoch 53/100
    8/8 [==============================] - 9s 1s/step - loss: 5.8842e-06 - accuracy: 1.0000 - val_loss: 1.7473 - val_accuracy: 0.9130
    Epoch 54/100
    8/8 [==============================] - 9s 1s/step - loss: 3.2677e-06 - accuracy: 1.0000 - val_loss: 1.7479 - val_accuracy: 0.9130
    Epoch 55/100
    8/8 [==============================] - 9s 1s/step - loss: 3.2579e-06 - accuracy: 1.0000 - val_loss: 1.7474 - val_accuracy: 0.9130
    Epoch 56/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7833e-06 - accuracy: 1.0000 - val_loss: 1.7466 - val_accuracy: 0.9130
    Epoch 57/100
    8/8 [==============================] - 9s 1s/step - loss: 5.1107e-07 - accuracy: 1.0000 - val_loss: 1.7432 - val_accuracy: 0.9130
    Epoch 58/100
    8/8 [==============================] - 9s 1s/step - loss: 2.3874e-06 - accuracy: 1.0000 - val_loss: 1.7411 - val_accuracy: 0.9130
    Epoch 59/100
    8/8 [==============================] - 9s 1s/step - loss: 1.3105e-06 - accuracy: 1.0000 - val_loss: 1.7401 - val_accuracy: 0.9068
    Epoch 60/100
    8/8 [==============================] - 9s 1s/step - loss: 2.1754e-07 - accuracy: 1.0000 - val_loss: 1.7367 - val_accuracy: 0.9068
    Epoch 61/100
    8/8 [==============================] - 9s 1s/step - loss: 1.3681e-04 - accuracy: 1.0000 - val_loss: 1.7141 - val_accuracy: 0.9068
    Epoch 62/100
    8/8 [==============================] - 9s 1s/step - loss: 1.4179e-07 - accuracy: 1.0000 - val_loss: 1.6237 - val_accuracy: 0.9255
    Epoch 63/100
    8/8 [==============================] - 9s 1s/step - loss: 1.3581e-06 - accuracy: 1.0000 - val_loss: 1.5922 - val_accuracy: 0.9255
    Epoch 64/100
    8/8 [==============================] - 9s 1s/step - loss: 1.8324e-06 - accuracy: 1.0000 - val_loss: 1.5776 - val_accuracy: 0.9255
    Epoch 65/100
    8/8 [==============================] - 9s 1s/step - loss: 4.3828e-06 - accuracy: 1.0000 - val_loss: 1.5692 - val_accuracy: 0.9255
    Epoch 66/100
    8/8 [==============================] - 9s 1s/step - loss: 1.6508e-04 - accuracy: 1.0000 - val_loss: 1.6160 - val_accuracy: 0.9193
    Epoch 67/100
    8/8 [==============================] - 9s 1s/step - loss: 7.6975e-08 - accuracy: 1.0000 - val_loss: 1.6725 - val_accuracy: 0.9130
    Epoch 68/100
    8/8 [==============================] - 9s 1s/step - loss: 5.5735e-06 - accuracy: 1.0000 - val_loss: 1.6954 - val_accuracy: 0.9130
    Epoch 69/100
    8/8 [==============================] - 9s 1s/step - loss: 2.3184e-06 - accuracy: 1.0000 - val_loss: 1.7035 - val_accuracy: 0.9130
    Epoch 70/100
    8/8 [==============================] - 9s 1s/step - loss: 3.0715e-07 - accuracy: 1.0000 - val_loss: 1.7059 - val_accuracy: 0.9130
    Epoch 71/100
    8/8 [==============================] - 9s 1s/step - loss: 9.6784e-05 - accuracy: 1.0000 - val_loss: 1.6586 - val_accuracy: 0.9130
    Epoch 72/100
    8/8 [==============================] - 9s 1s/step - loss: 4.3298e-07 - accuracy: 1.0000 - val_loss: 1.6330 - val_accuracy: 0.9193
    Epoch 73/100
    8/8 [==============================] - 9s 1s/step - loss: 3.3516e-08 - accuracy: 1.0000 - val_loss: 1.6226 - val_accuracy: 0.9193
    Epoch 74/100
    8/8 [==============================] - 9s 1s/step - loss: 2.4271e-07 - accuracy: 1.0000 - val_loss: 1.6176 - val_accuracy: 0.9193
    Epoch 75/100
    8/8 [==============================] - 9s 1s/step - loss: 7.5616e-06 - accuracy: 1.0000 - val_loss: 1.6200 - val_accuracy: 0.9193
    Epoch 76/100
    8/8 [==============================] - 9s 1s/step - loss: 8.4291e-07 - accuracy: 1.0000 - val_loss: 1.6194 - val_accuracy: 0.9193
    Epoch 77/100
    8/8 [==============================] - 9s 1s/step - loss: 1.9904e-06 - accuracy: 1.0000 - val_loss: 1.6172 - val_accuracy: 0.9193
    Epoch 78/100
    8/8 [==============================] - 9s 1s/step - loss: 1.9864e-05 - accuracy: 1.0000 - val_loss: 1.6200 - val_accuracy: 0.9193
    Epoch 79/100
    8/8 [==============================] - 9s 1s/step - loss: 5.0416e-07 - accuracy: 1.0000 - val_loss: 1.6324 - val_accuracy: 0.9193
    Epoch 80/100
    8/8 [==============================] - 9s 1s/step - loss: 1.6966e-07 - accuracy: 1.0000 - val_loss: 1.6370 - val_accuracy: 0.9193
    Epoch 81/100
    8/8 [==============================] - 9s 1s/step - loss: 4.1665e-07 - accuracy: 1.0000 - val_loss: 1.6370 - val_accuracy: 0.9193
    Epoch 82/100
    8/8 [==============================] - 9s 1s/step - loss: 7.1040e-06 - accuracy: 1.0000 - val_loss: 1.6342 - val_accuracy: 0.9193
    Epoch 83/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7899e-07 - accuracy: 1.0000 - val_loss: 1.6334 - val_accuracy: 0.9193
    Epoch 84/100
    8/8 [==============================] - 9s 1s/step - loss: 3.1818e-06 - accuracy: 1.0000 - val_loss: 1.6327 - val_accuracy: 0.9193
    Epoch 85/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1270e-07 - accuracy: 1.0000 - val_loss: 1.6328 - val_accuracy: 0.9193
    Epoch 86/100
    8/8 [==============================] - 9s 1s/step - loss: 2.3607e-07 - accuracy: 1.0000 - val_loss: 1.6342 - val_accuracy: 0.9193
    Epoch 87/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1380e-07 - accuracy: 1.0000 - val_loss: 1.6331 - val_accuracy: 0.9193
    Epoch 88/100
    8/8 [==============================] - 9s 1s/step - loss: 5.0670e-07 - accuracy: 1.0000 - val_loss: 1.6337 - val_accuracy: 0.9193
    Epoch 89/100
    8/8 [==============================] - 9s 1s/step - loss: 1.0288e-07 - accuracy: 1.0000 - val_loss: 1.6338 - val_accuracy: 0.9193
    Epoch 90/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1589e-07 - accuracy: 1.0000 - val_loss: 1.6333 - val_accuracy: 0.9193
    Epoch 91/100
    8/8 [==============================] - 9s 1s/step - loss: 1.8423e-06 - accuracy: 1.0000 - val_loss: 1.6326 - val_accuracy: 0.9193
    Epoch 92/100
    8/8 [==============================] - 9s 1s/step - loss: 2.5275e-06 - accuracy: 1.0000 - val_loss: 1.6303 - val_accuracy: 0.9193
    Epoch 93/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7973e-07 - accuracy: 1.0000 - val_loss: 1.6288 - val_accuracy: 0.9193
    Epoch 94/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1907e-06 - accuracy: 1.0000 - val_loss: 1.6275 - val_accuracy: 0.9193
    Epoch 95/100
    8/8 [==============================] - 9s 1s/step - loss: 2.2027e-06 - accuracy: 1.0000 - val_loss: 1.6256 - val_accuracy: 0.9193
    Epoch 96/100
    8/8 [==============================] - 9s 1s/step - loss: 7.6484e-08 - accuracy: 1.0000 - val_loss: 1.6236 - val_accuracy: 0.9193
    Epoch 97/100
    8/8 [==============================] - 9s 1s/step - loss: 2.2285e-05 - accuracy: 1.0000 - val_loss: 1.6285 - val_accuracy: 0.9193
    Epoch 98/100
    8/8 [==============================] - 9s 1s/step - loss: 1.7579e-06 - accuracy: 1.0000 - val_loss: 1.6295 - val_accuracy: 0.9193
    Epoch 99/100
    8/8 [==============================] - 9s 1s/step - loss: 1.1466e-07 - accuracy: 1.0000 - val_loss: 1.6294 - val_accuracy: 0.9193
    Epoch 100/100
    8/8 [==============================] - 9s 1s/step - loss: 3.1599e-05 - accuracy: 1.0000 - val_loss: 1.6344 - val_accuracy: 0.9193



```python
new_model.save_weights(path + '/model.h5')
```


```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```


```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```


    
![png](VGGINET_woDA_400X_files/VGGINET_woDA_400X_17_0.png)
    



```python
new_model.evaluate(test_ds)
```

    4/4 [==============================] - 32s 8s/step - loss: 2.3225 - accuracy: 0.8852





    [2.3225440979003906, 0.8852459192276001]




```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

    Accuracy:  0.8852459016393442
                  precision    recall  f1-score   support
    
               0       0.90      0.70      0.79       148
               1       0.88      0.97      0.92       340
    
        accuracy                           0.89       488
       macro avg       0.89      0.83      0.85       488
    weighted avg       0.89      0.89      0.88       488
    



```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```


```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```




    <AxesSubplot:title={'center':'Normalized Confusion Matrix'}, xlabel='Predicted label', ylabel='True label'>




    
![png](VGGINET_woDA_400X_files/VGGINET_woDA_400X_21_1.png)
    



    
![png](VGGINET_woDA_400X_files/VGGINET_woDA_400X_21_2.png)
    



```python
plot_roc(y_true, y_probas)
```




    <AxesSubplot:title={'center':'ROC Curves'}, xlabel='False Positive Rate', ylabel='True Positive Rate'>




    
![png](VGGINET_woDA_400X_files/VGGINET_woDA_400X_22_1.png)
    



```python
from imblearn.metrics import classification_report_imbalanced
```


```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.90      0.70      0.79       148
               1       0.88      0.97      0.92       340
    
        accuracy                           0.89       488
       macro avg       0.89      0.83      0.85       488
    weighted avg       0.89      0.89      0.88       488
    
                       pre       rec       spe        f1       geo       iba       sup
    
              0       0.90      0.70      0.97      0.79      0.82      0.66       148
              1       0.88      0.97      0.70      0.92      0.82      0.69       340
    
    avg / total       0.89      0.89      0.78      0.88      0.82      0.68       488
    



```python

```
