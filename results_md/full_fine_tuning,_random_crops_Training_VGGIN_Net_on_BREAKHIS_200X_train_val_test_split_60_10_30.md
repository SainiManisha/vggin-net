<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'full-FINETUNING_200X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Downloading data from https://storage.googleapis.com/tensorflow/keras-applications/vgg16/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5
    58892288/58889256 [==============================] - 1s 0us/step
    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.2574 - accuracy: 0.4453WARNING:tensorflow:From /home/manisha.saini/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 31s 3s/step - loss: 4.0902 - accuracy: 0.7305 - val_loss: 5.3362 - val_accuracy: 0.9061
    Epoch 2/100
    9/9 [==============================] - 16s 2s/step - loss: 1.9156 - accuracy: 0.8703 - val_loss: 10.2576 - val_accuracy: 0.8011
    Epoch 3/100
    9/9 [==============================] - 15s 2s/step - loss: 1.1524 - accuracy: 0.8878 - val_loss: 2.1954 - val_accuracy: 0.8840
    Epoch 4/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0978 - accuracy: 0.8942 - val_loss: 7.5476 - val_accuracy: 0.7956
    Epoch 5/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9544 - accuracy: 0.9034 - val_loss: 2.8664 - val_accuracy: 0.8564
    Epoch 6/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8825 - accuracy: 0.9062 - val_loss: 0.6184 - val_accuracy: 0.9282
    Epoch 7/100
    9/9 [==============================] - 15s 2s/step - loss: 0.8331 - accuracy: 0.9144 - val_loss: 2.4484 - val_accuracy: 0.8950
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0579 - accuracy: 0.9080 - val_loss: 0.6538 - val_accuracy: 0.9116
    Epoch 9/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8424 - accuracy: 0.9108 - val_loss: 0.8611 - val_accuracy: 0.9227
    Epoch 10/100
    9/9 [==============================] - 16s 2s/step - loss: 0.8085 - accuracy: 0.9273 - val_loss: 1.7865 - val_accuracy: 0.9116
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8167 - accuracy: 0.9310 - val_loss: 0.3487 - val_accuracy: 0.9613
    Epoch 12/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7981 - accuracy: 0.9338 - val_loss: 0.5441 - val_accuracy: 0.9558
    Epoch 13/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6029 - accuracy: 0.9255 - val_loss: 1.0936 - val_accuracy: 0.9227
    Epoch 14/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6595 - accuracy: 0.9319 - val_loss: 0.9975 - val_accuracy: 0.9448
    Epoch 15/100
    9/9 [==============================] - 16s 2s/step - loss: 0.6953 - accuracy: 0.9328 - val_loss: 1.2281 - val_accuracy: 0.9337
    Epoch 16/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7862 - accuracy: 0.9310 - val_loss: 1.8812 - val_accuracy: 0.9171
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8289 - accuracy: 0.9264 - val_loss: 1.3069 - val_accuracy: 0.9227
    Epoch 18/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5771 - accuracy: 0.9457 - val_loss: 0.3114 - val_accuracy: 0.9669
    Epoch 19/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7774 - accuracy: 0.9356 - val_loss: 2.6033 - val_accuracy: 0.8895
    Epoch 20/100
    9/9 [==============================] - 15s 2s/step - loss: 1.0818 - accuracy: 0.9282 - val_loss: 2.1206 - val_accuracy: 0.8950
    Epoch 21/100
    9/9 [==============================] - 12s 1s/step - loss: 0.5794 - accuracy: 0.9522 - val_loss: 1.4835 - val_accuracy: 0.9061
    Epoch 22/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5060 - accuracy: 0.9503 - val_loss: 1.2747 - val_accuracy: 0.9282
    Epoch 23/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5708 - accuracy: 0.9540 - val_loss: 0.6299 - val_accuracy: 0.9558
    Epoch 24/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6339 - accuracy: 0.9494 - val_loss: 1.2245 - val_accuracy: 0.9282
    Epoch 25/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5288 - accuracy: 0.9512 - val_loss: 0.7289 - val_accuracy: 0.9558
    Epoch 26/100
    9/9 [==============================] - 16s 2s/step - loss: 0.9443 - accuracy: 0.9356 - val_loss: 0.7519 - val_accuracy: 0.9558
    Epoch 27/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5361 - accuracy: 0.9512 - val_loss: 0.5258 - val_accuracy: 0.9392
    Epoch 28/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5307 - accuracy: 0.9503 - val_loss: 2.7339 - val_accuracy: 0.8895
    Epoch 29/100
    9/9 [==============================] - 16s 2s/step - loss: 0.5068 - accuracy: 0.9466 - val_loss: 1.2498 - val_accuracy: 0.9337
    Epoch 30/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6271 - accuracy: 0.9439 - val_loss: 0.2458 - val_accuracy: 0.9724
    Epoch 31/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4171 - accuracy: 0.9678 - val_loss: 0.5682 - val_accuracy: 0.9558
    Epoch 32/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4220 - accuracy: 0.9586 - val_loss: 0.4748 - val_accuracy: 0.9613
    Epoch 33/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3058 - accuracy: 0.9623 - val_loss: 4.0535 - val_accuracy: 0.8950
    Epoch 34/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3929 - accuracy: 0.9660 - val_loss: 2.5859 - val_accuracy: 0.9227
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5070 - accuracy: 0.9586 - val_loss: 1.5386 - val_accuracy: 0.9282
    Epoch 36/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4792 - accuracy: 0.9641 - val_loss: 2.4297 - val_accuracy: 0.9116
    Epoch 37/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4292 - accuracy: 0.9678 - val_loss: 1.2830 - val_accuracy: 0.9448
    Epoch 38/100
    9/9 [==============================] - 15s 2s/step - loss: 0.6460 - accuracy: 0.9641 - val_loss: 1.7689 - val_accuracy: 0.9282
    Epoch 39/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4797 - accuracy: 0.9614 - val_loss: 0.9297 - val_accuracy: 0.9503
    Epoch 40/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4164 - accuracy: 0.9669 - val_loss: 0.3678 - val_accuracy: 0.9613
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4253 - accuracy: 0.9724 - val_loss: 0.8378 - val_accuracy: 0.9337
    Epoch 42/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3336 - accuracy: 0.9706 - val_loss: 0.5097 - val_accuracy: 0.9503
    Epoch 43/100
    9/9 [==============================] - 12s 1s/step - loss: 0.6939 - accuracy: 0.9586 - val_loss: 0.6481 - val_accuracy: 0.9337
    Epoch 44/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5325 - accuracy: 0.9669 - val_loss: 1.1320 - val_accuracy: 0.9227
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3639 - accuracy: 0.9752 - val_loss: 1.3173 - val_accuracy: 0.9337
    Epoch 46/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3990 - accuracy: 0.9669 - val_loss: 0.6947 - val_accuracy: 0.9448
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4989 - accuracy: 0.9595 - val_loss: 0.4881 - val_accuracy: 0.9724
    Epoch 48/100
    9/9 [==============================] - 15s 2s/step - loss: 0.9028 - accuracy: 0.9540 - val_loss: 0.7251 - val_accuracy: 0.9669
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5113 - accuracy: 0.9715 - val_loss: 0.5288 - val_accuracy: 0.9613
    Epoch 50/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4692 - accuracy: 0.9696 - val_loss: 0.6449 - val_accuracy: 0.9779
    Epoch 51/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3616 - accuracy: 0.9715 - val_loss: 1.8534 - val_accuracy: 0.9448
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3539 - accuracy: 0.9742 - val_loss: 2.7954 - val_accuracy: 0.9282
    Epoch 53/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4394 - accuracy: 0.9724 - val_loss: 1.2981 - val_accuracy: 0.9503
    Epoch 54/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5582 - accuracy: 0.9733 - val_loss: 0.9802 - val_accuracy: 0.9613
    Epoch 55/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3706 - accuracy: 0.9752 - val_loss: 1.1836 - val_accuracy: 0.9448
    Epoch 56/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3210 - accuracy: 0.9788 - val_loss: 0.9222 - val_accuracy: 0.9448
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4337 - accuracy: 0.9779 - val_loss: 0.7520 - val_accuracy: 0.9669
    Epoch 58/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5545 - accuracy: 0.9669 - val_loss: 0.6332 - val_accuracy: 0.9669
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3534 - accuracy: 0.9752 - val_loss: 0.9233 - val_accuracy: 0.9503
    Epoch 60/100
    9/9 [==============================] - 15s 2s/step - loss: 0.5729 - accuracy: 0.9706 - val_loss: 0.9972 - val_accuracy: 0.9558
    Epoch 61/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5419 - accuracy: 0.9660 - val_loss: 0.6379 - val_accuracy: 0.9558
    Epoch 62/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2958 - accuracy: 0.9742 - val_loss: 0.7970 - val_accuracy: 0.9779
    Epoch 63/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3199 - accuracy: 0.9807 - val_loss: 0.7990 - val_accuracy: 0.9669
    Epoch 64/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3252 - accuracy: 0.9761 - val_loss: 0.8613 - val_accuracy: 0.9669
    Epoch 65/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1656 - accuracy: 0.9871 - val_loss: 1.1696 - val_accuracy: 0.9613
    Epoch 66/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3504 - accuracy: 0.9788 - val_loss: 1.6691 - val_accuracy: 0.9392
    Epoch 67/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2126 - accuracy: 0.9816 - val_loss: 1.8848 - val_accuracy: 0.9613
    Epoch 68/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3479 - accuracy: 0.9770 - val_loss: 0.9160 - val_accuracy: 0.9724
    Epoch 69/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2210 - accuracy: 0.9779 - val_loss: 1.0577 - val_accuracy: 0.9558
    Epoch 70/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3583 - accuracy: 0.9733 - val_loss: 0.8107 - val_accuracy: 0.9669
    Epoch 71/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3817 - accuracy: 0.9779 - val_loss: 1.3152 - val_accuracy: 0.9392
    Epoch 72/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2383 - accuracy: 0.9798 - val_loss: 0.9169 - val_accuracy: 0.9613
    Epoch 73/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3719 - accuracy: 0.9798 - val_loss: 1.0936 - val_accuracy: 0.9503
    Epoch 74/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3178 - accuracy: 0.9807 - val_loss: 2.7965 - val_accuracy: 0.9282
    Epoch 75/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3137 - accuracy: 0.9807 - val_loss: 0.8812 - val_accuracy: 0.9613
    Epoch 76/100
    9/9 [==============================] - 15s 2s/step - loss: 0.4247 - accuracy: 0.9761 - val_loss: 0.7801 - val_accuracy: 0.9669
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2621 - accuracy: 0.9825 - val_loss: 1.3000 - val_accuracy: 0.9669
    Epoch 78/100
    9/9 [==============================] - 16s 2s/step - loss: 0.1779 - accuracy: 0.9890 - val_loss: 2.3395 - val_accuracy: 0.9392
    Epoch 79/100
    9/9 [==============================] - 16s 2s/step - loss: 0.3953 - accuracy: 0.9788 - val_loss: 1.1213 - val_accuracy: 0.9724
    Epoch 80/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2741 - accuracy: 0.9788 - val_loss: 1.6057 - val_accuracy: 0.9558
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3877 - accuracy: 0.9779 - val_loss: 1.5425 - val_accuracy: 0.9613
    Epoch 82/100
    9/9 [==============================] - 12s 1s/step - loss: 0.3322 - accuracy: 0.9715 - val_loss: 0.6286 - val_accuracy: 0.9724
    Epoch 83/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3263 - accuracy: 0.9770 - val_loss: 1.1709 - val_accuracy: 0.9613
    Epoch 84/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1768 - accuracy: 0.9862 - val_loss: 2.5906 - val_accuracy: 0.9392
    Epoch 85/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1925 - accuracy: 0.9834 - val_loss: 1.9254 - val_accuracy: 0.9613
    Epoch 86/100
    9/9 [==============================] - 16s 2s/step - loss: 0.4443 - accuracy: 0.9788 - val_loss: 1.5033 - val_accuracy: 0.9448
    Epoch 87/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3707 - accuracy: 0.9798 - val_loss: 1.0944 - val_accuracy: 0.9669
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3364 - accuracy: 0.9825 - val_loss: 2.2300 - val_accuracy: 0.9503
    Epoch 89/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2811 - accuracy: 0.9816 - val_loss: 1.4799 - val_accuracy: 0.9613
    Epoch 90/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1548 - accuracy: 0.9862 - val_loss: 2.2257 - val_accuracy: 0.9282
    Epoch 91/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3440 - accuracy: 0.9752 - val_loss: 1.4236 - val_accuracy: 0.9558
    Epoch 92/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1083 - accuracy: 0.9908 - val_loss: 0.9900 - val_accuracy: 0.9613
    Epoch 93/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3601 - accuracy: 0.9807 - val_loss: 1.4016 - val_accuracy: 0.9558
    Epoch 94/100
    9/9 [==============================] - 12s 1s/step - loss: 0.2723 - accuracy: 0.9807 - val_loss: 1.1699 - val_accuracy: 0.9503
    Epoch 95/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2967 - accuracy: 0.9788 - val_loss: 1.0679 - val_accuracy: 0.9669
    Epoch 96/100
    9/9 [==============================] - 15s 2s/step - loss: 0.1486 - accuracy: 0.9926 - val_loss: 1.8310 - val_accuracy: 0.9613
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3364 - accuracy: 0.9816 - val_loss: 1.8669 - val_accuracy: 0.9669
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2044 - accuracy: 0.9880 - val_loss: 1.5562 - val_accuracy: 0.9669
    Epoch 99/100
    9/9 [==============================] - 15s 2s/step - loss: 0.2038 - accuracy: 0.9844 - val_loss: 0.9737 - val_accuracy: 0.9669
    Epoch 100/100
    9/9 [==============================] - 15s 2s/step - loss: 0.3401 - accuracy: 0.9844 - val_loss: 0.8044 - val_accuracy: 0.9669

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/211.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 25s 5s/step - loss: 1.8595 - accuracy: 0.9486

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.859542727470398, 0.9486238360404968]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    layer.trainable = True
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,957,666
    Non-trainable params: 1,472
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/212.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
                  metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
     2/17 [==&gt;...........................] - ETA: 12s - loss: 1.2537 - accuracy: 0.9453WARNING:tensorflow:Callbacks method `on_train_batch_end` is slow compared to the batch time (batch time: 0.4555s vs `on_train_batch_end` time: 1.1463s). Check your callbacks.
    17/17 [==============================] - 38s 2s/step - loss: 0.3336 - accuracy: 0.9761 - val_loss: 3.5757 - val_accuracy: 0.8785

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 33s 2s/step - loss: 0.2887 - accuracy: 0.9825 - val_loss: 2.0413 - val_accuracy: 0.9558

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 34s 2s/step - loss: 0.2059 - accuracy: 0.9844 - val_loss: 1.9154 - val_accuracy: 0.9558

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1184 - accuracy: 0.9880 - val_loss: 0.8610 - val_accuracy: 0.9724

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 33s 2s/step - loss: 0.2552 - accuracy: 0.9880 - val_loss: 0.9557 - val_accuracy: 0.9834

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 34s 2s/step - loss: 0.2283 - accuracy: 0.9871 - val_loss: 1.5447 - val_accuracy: 0.9613

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 30s 2s/step - loss: 0.1978 - accuracy: 0.9890 - val_loss: 1.0700 - val_accuracy: 0.9724

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 33s 2s/step - loss: 0.2862 - accuracy: 0.9844 - val_loss: 1.2715 - val_accuracy: 0.9503

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1940 - accuracy: 0.9899 - val_loss: 2.9985 - val_accuracy: 0.9503

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 31s 2s/step - loss: 0.2179 - accuracy: 0.9862 - val_loss: 1.2512 - val_accuracy: 0.9834

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 34s 2s/step - loss: 0.1276 - accuracy: 0.9908 - val_loss: 0.8784 - val_accuracy: 0.9779

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 32s 2s/step - loss: 0.0894 - accuracy: 0.9936 - val_loss: 1.0801 - val_accuracy: 0.9558

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 32s 2s/step - loss: 0.2514 - accuracy: 0.9862 - val_loss: 0.6508 - val_accuracy: 0.9669

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 33s 2s/step - loss: 0.3135 - accuracy: 0.9834 - val_loss: 0.9362 - val_accuracy: 0.9724

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1683 - accuracy: 0.9890 - val_loss: 0.8493 - val_accuracy: 0.9779

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 34s 2s/step - loss: 0.1431 - accuracy: 0.9890 - val_loss: 0.9218 - val_accuracy: 0.9724

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 31s 2s/step - loss: 0.2913 - accuracy: 0.9816 - val_loss: 1.0644 - val_accuracy: 0.9834

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 34s 2s/step - loss: 0.2698 - accuracy: 0.9862 - val_loss: 1.2761 - val_accuracy: 0.9558

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1613 - accuracy: 0.9899 - val_loss: 1.1565 - val_accuracy: 0.9613

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 31s 2s/step - loss: 0.2650 - accuracy: 0.9871 - val_loss: 0.7551 - val_accuracy: 0.9724

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 33s 2s/step - loss: 0.0962 - accuracy: 0.9926 - val_loss: 1.1096 - val_accuracy: 0.9834

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1754 - accuracy: 0.9880 - val_loss: 1.8375 - val_accuracy: 0.9724

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 31s 2s/step - loss: 0.2013 - accuracy: 0.9880 - val_loss: 0.5836 - val_accuracy: 0.9724

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 33s 2s/step - loss: 0.0489 - accuracy: 0.9936 - val_loss: 0.7843 - val_accuracy: 0.9834

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1726 - accuracy: 0.9899 - val_loss: 0.4960 - val_accuracy: 0.9834

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1129 - accuracy: 0.9945 - val_loss: 2.7358 - val_accuracy: 0.9503

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 31s 2s/step - loss: 0.1534 - accuracy: 0.9890 - val_loss: 3.5630 - val_accuracy: 0.9448

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 33s 2s/step - loss: 0.0914 - accuracy: 0.9908 - val_loss: 2.6238 - val_accuracy: 0.9337

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1251 - accuracy: 0.9917 - val_loss: 2.7264 - val_accuracy: 0.9558

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1103 - accuracy: 0.9908 - val_loss: 1.1965 - val_accuracy: 0.9779

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 31s 2s/step - loss: 0.1471 - accuracy: 0.9890 - val_loss: 4.0607 - val_accuracy: 0.9227

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 31s 2s/step - loss: 0.0860 - accuracy: 0.9917 - val_loss: 1.8744 - val_accuracy: 0.9724

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 32s 2s/step - loss: 0.0595 - accuracy: 0.9982 - val_loss: 1.6898 - val_accuracy: 0.9558

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1493 - accuracy: 0.9908 - val_loss: 2.2816 - val_accuracy: 0.9779

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 32s 2s/step - loss: 0.0691 - accuracy: 0.9917 - val_loss: 0.9405 - val_accuracy: 0.9834

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1682 - accuracy: 0.9908 - val_loss: 0.8243 - val_accuracy: 0.9834

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 32s 2s/step - loss: 0.0960 - accuracy: 0.9917 - val_loss: 1.3442 - val_accuracy: 0.9669

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1937 - accuracy: 0.9871 - val_loss: 5.8728 - val_accuracy: 0.9282

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 33s 2s/step - loss: 0.2316 - accuracy: 0.9880 - val_loss: 0.2681 - val_accuracy: 0.9834

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 33s 2s/step - loss: 0.0374 - accuracy: 0.9954 - val_loss: 0.4343 - val_accuracy: 0.9890

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 31s 2s/step - loss: 0.1476 - accuracy: 0.9926 - val_loss: 0.5113 - val_accuracy: 0.9779

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 31s 2s/step - loss: 0.1793 - accuracy: 0.9908 - val_loss: 0.3648 - val_accuracy: 0.9724

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 32s 2s/step - loss: 0.1421 - accuracy: 0.9890 - val_loss: 0.8453 - val_accuracy: 0.9669

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 33s 2s/step - loss: 0.0499 - accuracy: 0.9954 - val_loss: 1.2377 - val_accuracy: 0.9779

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 29s 2s/step - loss: 0.1797 - accuracy: 0.9890 - val_loss: 0.7286 - val_accuracy: 0.9834

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1307 - accuracy: 0.9908 - val_loss: 1.4609 - val_accuracy: 0.9669

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 33s 2s/step - loss: 0.1431 - accuracy: 0.9908 - val_loss: 2.1738 - val_accuracy: 0.9392

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 32s 2s/step - loss: 0.0971 - accuracy: 0.9936 - val_loss: 1.3495 - val_accuracy: 0.9724

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 31s 2s/step - loss: 0.0437 - accuracy: 0.9972 - val_loss: 1.3252 - val_accuracy: 0.9779

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 32s 2s/step - loss: 0.2472 - accuracy: 0.9890 - val_loss: 2.5536 - val_accuracy: 0.9503

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/213.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 1.3724 - accuracy: 0.9688

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3723750114440918, 0.9688073396682739]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9688073394495413
                  precision    recall  f1-score   support

               0       0.96      0.93      0.95       158
               1       0.97      0.98      0.98       387

        accuracy                           0.97       545
       macro avg       0.97      0.96      0.96       545
    weighted avg       0.97      0.97      0.97       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/214.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/215.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/216.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.96      0.93      0.95       158
               1       0.97      0.98      0.98       387

        accuracy                           0.97       545
       macro avg       0.97      0.96      0.96       545
    weighted avg       0.97      0.97      0.97       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.96      0.93      0.98      0.95      0.96      0.91       158
              1       0.97      0.98      0.93      0.98      0.96      0.92       387

    avg / total       0.97      0.97      0.95      0.97      0.96      0.92       545

</div>
</div>
</div>
</div>
</div>
</div>
</div>
