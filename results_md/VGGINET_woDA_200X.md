```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = '1'
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

    ('200X', '1')



```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________



```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________



```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```


```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```


```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```


```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```


```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________



```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```


```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```


```python
name = 'VGGINetWODA/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```


```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```


```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```


```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     # NO DATA AUGMENTATION WILL BE USED FOR THIS EXPERIMENT!
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

    Found 1087 files belonging to 2 classes.
    Found 181 files belonging to 2 classes.
    Found 545 files belonging to 2 classes.



```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

    Epoch 1/100
    1/9 [==>...........................] - ETA: 0s - loss: 1.1427 - accuracy: 0.5547WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 41s 5s/step - loss: 3.1436 - accuracy: 0.7755 - val_loss: 8.7506 - val_accuracy: 0.8674
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2135 - accuracy: 0.9282 - val_loss: 27.3805 - val_accuracy: 0.7348
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3473 - accuracy: 0.9641 - val_loss: 20.2649 - val_accuracy: 0.7514
    Epoch 4/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1589 - accuracy: 0.9770 - val_loss: 15.2444 - val_accuracy: 0.7680
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0285 - accuracy: 0.9936 - val_loss: 9.7609 - val_accuracy: 0.8122
    Epoch 6/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0028 - accuracy: 0.9982 - val_loss: 7.9716 - val_accuracy: 0.8177
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0069 - accuracy: 0.9991 - val_loss: 6.4813 - val_accuracy: 0.8232
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0038 - accuracy: 0.9991 - val_loss: 5.6860 - val_accuracy: 0.8343
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 2.1265e-04 - accuracy: 1.0000 - val_loss: 6.1398 - val_accuracy: 0.8398
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0690e-05 - accuracy: 1.0000 - val_loss: 5.9946 - val_accuracy: 0.8398
    Epoch 11/100
    9/9 [==============================] - 10s 1s/step - loss: 3.3669e-04 - accuracy: 1.0000 - val_loss: 5.5170 - val_accuracy: 0.8453
    Epoch 12/100
    9/9 [==============================] - 10s 1s/step - loss: 2.7467e-06 - accuracy: 1.0000 - val_loss: 4.9816 - val_accuracy: 0.8564
    Epoch 13/100
    9/9 [==============================] - 10s 1s/step - loss: 5.2136e-04 - accuracy: 1.0000 - val_loss: 4.5410 - val_accuracy: 0.8619
    Epoch 14/100
    9/9 [==============================] - 10s 1s/step - loss: 2.2283e-05 - accuracy: 1.0000 - val_loss: 4.1424 - val_accuracy: 0.8619
    Epoch 15/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0028 - accuracy: 0.9991 - val_loss: 3.7714 - val_accuracy: 0.8619
    Epoch 16/100
    9/9 [==============================] - 10s 1s/step - loss: 9.3828e-04 - accuracy: 0.9991 - val_loss: 3.0913 - val_accuracy: 0.8674
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 8.8564e-06 - accuracy: 1.0000 - val_loss: 2.5415 - val_accuracy: 0.8840
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1858e-04 - accuracy: 1.0000 - val_loss: 2.3402 - val_accuracy: 0.8950
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0037 - accuracy: 0.9982 - val_loss: 2.0848 - val_accuracy: 0.9061
    Epoch 20/100
    9/9 [==============================] - 10s 1s/step - loss: 3.2657e-04 - accuracy: 1.0000 - val_loss: 2.2621 - val_accuracy: 0.9061
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0111 - accuracy: 0.9982 - val_loss: 2.3486 - val_accuracy: 0.9006
    Epoch 22/100
    9/9 [==============================] - 10s 1s/step - loss: 1.6808e-04 - accuracy: 1.0000 - val_loss: 2.7125 - val_accuracy: 0.8840
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0056 - accuracy: 0.9991 - val_loss: 2.2358 - val_accuracy: 0.8895
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 4.2468e-04 - accuracy: 1.0000 - val_loss: 2.0486 - val_accuracy: 0.9006
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 1.6872e-04 - accuracy: 1.0000 - val_loss: 1.9562 - val_accuracy: 0.9061
    Epoch 26/100
    9/9 [==============================] - 10s 1s/step - loss: 3.5277e-05 - accuracy: 1.0000 - val_loss: 1.9103 - val_accuracy: 0.9061
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 7.6229e-06 - accuracy: 1.0000 - val_loss: 1.8728 - val_accuracy: 0.9061
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 4.9909e-06 - accuracy: 1.0000 - val_loss: 1.8357 - val_accuracy: 0.9116
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 5.6989e-04 - accuracy: 1.0000 - val_loss: 1.8248 - val_accuracy: 0.9171
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 3.3457e-05 - accuracy: 1.0000 - val_loss: 1.7936 - val_accuracy: 0.9171
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 8.5128e-04 - accuracy: 0.9991 - val_loss: 1.6408 - val_accuracy: 0.9171
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 9.0065e-07 - accuracy: 1.0000 - val_loss: 1.5641 - val_accuracy: 0.9171
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 2.2224e-05 - accuracy: 1.0000 - val_loss: 1.5342 - val_accuracy: 0.9227
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 3.0841e-05 - accuracy: 1.0000 - val_loss: 1.5159 - val_accuracy: 0.9227
    Epoch 35/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2163e-06 - accuracy: 1.0000 - val_loss: 1.5014 - val_accuracy: 0.9282
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 5.1455e-06 - accuracy: 1.0000 - val_loss: 1.4864 - val_accuracy: 0.9337
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 9.5964e-06 - accuracy: 1.0000 - val_loss: 1.4741 - val_accuracy: 0.9337
    Epoch 38/100
    9/9 [==============================] - 10s 1s/step - loss: 1.3577e-05 - accuracy: 1.0000 - val_loss: 1.4654 - val_accuracy: 0.9337
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 8.9164e-07 - accuracy: 1.0000 - val_loss: 1.4573 - val_accuracy: 0.9282
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 2.8846e-05 - accuracy: 1.0000 - val_loss: 1.4530 - val_accuracy: 0.9282
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 6.2448e-07 - accuracy: 1.0000 - val_loss: 1.4484 - val_accuracy: 0.9282
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 1.3801e-06 - accuracy: 1.0000 - val_loss: 1.4413 - val_accuracy: 0.9282
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 1.7681e-05 - accuracy: 1.0000 - val_loss: 1.4372 - val_accuracy: 0.9282
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 1.3958e-06 - accuracy: 1.0000 - val_loss: 1.4350 - val_accuracy: 0.9282
    Epoch 45/100
    9/9 [==============================] - 10s 1s/step - loss: 1.5183e-05 - accuracy: 1.0000 - val_loss: 1.4317 - val_accuracy: 0.9282
    Epoch 46/100
    9/9 [==============================] - 10s 1s/step - loss: 1.4810e-05 - accuracy: 1.0000 - val_loss: 1.4144 - val_accuracy: 0.9282
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0048 - accuracy: 0.9991 - val_loss: 1.8467 - val_accuracy: 0.9116
    Epoch 48/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1006e-04 - accuracy: 1.0000 - val_loss: 2.1247 - val_accuracy: 0.8950
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 2.2236e-04 - accuracy: 1.0000 - val_loss: 2.1175 - val_accuracy: 0.9006
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 7.2190e-06 - accuracy: 1.0000 - val_loss: 2.0714 - val_accuracy: 0.9061
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 1.2710e-04 - accuracy: 1.0000 - val_loss: 2.0058 - val_accuracy: 0.9061
    Epoch 52/100
    9/9 [==============================] - 10s 1s/step - loss: 2.6451e-05 - accuracy: 1.0000 - val_loss: 1.9745 - val_accuracy: 0.9006
    Epoch 53/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1316e-05 - accuracy: 1.0000 - val_loss: 1.9489 - val_accuracy: 0.9006
    Epoch 54/100
    9/9 [==============================] - 10s 1s/step - loss: 2.6855e-06 - accuracy: 1.0000 - val_loss: 1.9302 - val_accuracy: 0.9006
    Epoch 55/100
    9/9 [==============================] - 10s 1s/step - loss: 5.3330e-06 - accuracy: 1.0000 - val_loss: 1.9165 - val_accuracy: 0.9006
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0852e-05 - accuracy: 1.0000 - val_loss: 1.9078 - val_accuracy: 0.9006
    Epoch 57/100
    9/9 [==============================] - 10s 1s/step - loss: 7.7827e-07 - accuracy: 1.0000 - val_loss: 1.8989 - val_accuracy: 0.9006
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 5.2298e-07 - accuracy: 1.0000 - val_loss: 1.8928 - val_accuracy: 0.9006
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8605e-05 - accuracy: 1.0000 - val_loss: 1.8926 - val_accuracy: 0.9006
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 1.9843e-06 - accuracy: 1.0000 - val_loss: 1.8853 - val_accuracy: 0.9061
    Epoch 61/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1900e-06 - accuracy: 1.0000 - val_loss: 1.8785 - val_accuracy: 0.9061
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 7.4134e-08 - accuracy: 1.0000 - val_loss: 1.8747 - val_accuracy: 0.9061
    Epoch 63/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8757e-05 - accuracy: 1.0000 - val_loss: 1.8638 - val_accuracy: 0.9061
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 2.6085e-05 - accuracy: 1.0000 - val_loss: 1.8702 - val_accuracy: 0.9061
    Epoch 65/100
    9/9 [==============================] - 10s 1s/step - loss: 2.3238e-06 - accuracy: 1.0000 - val_loss: 1.8713 - val_accuracy: 0.9061
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 1.7359e-07 - accuracy: 1.0000 - val_loss: 1.8712 - val_accuracy: 0.9061
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0023 - accuracy: 0.9991 - val_loss: 2.0028 - val_accuracy: 0.9006
    Epoch 68/100
    9/9 [==============================] - 10s 1s/step - loss: 6.2999e-05 - accuracy: 1.0000 - val_loss: 2.4275 - val_accuracy: 0.8840
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 3.8350e-04 - accuracy: 1.0000 - val_loss: 2.6636 - val_accuracy: 0.8785
    Epoch 70/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0036 - accuracy: 0.9991 - val_loss: 2.1096 - val_accuracy: 0.9061
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0036 - accuracy: 0.9991 - val_loss: 2.1655 - val_accuracy: 0.9116
    Epoch 72/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0236 - accuracy: 0.9972 - val_loss: 1.9223 - val_accuracy: 0.9116
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0446 - accuracy: 0.9917 - val_loss: 1.8676 - val_accuracy: 0.8895
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0018 - accuracy: 0.9982 - val_loss: 1.4670 - val_accuracy: 0.9006
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0261 - accuracy: 0.9954 - val_loss: 1.4999 - val_accuracy: 0.8785
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0243 - accuracy: 0.9963 - val_loss: 2.6441 - val_accuracy: 0.9006
    Epoch 77/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0331 - accuracy: 0.9926 - val_loss: 2.8358 - val_accuracy: 0.8619
    Epoch 78/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0427 - accuracy: 0.9936 - val_loss: 3.4634 - val_accuracy: 0.8895
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0538 - accuracy: 0.9963 - val_loss: 2.8241 - val_accuracy: 0.8785
    Epoch 80/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0837 - accuracy: 0.9917 - val_loss: 2.9765 - val_accuracy: 0.9006
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0451 - accuracy: 0.9917 - val_loss: 5.3205 - val_accuracy: 0.8895
    Epoch 82/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0341 - accuracy: 0.9945 - val_loss: 2.4806 - val_accuracy: 0.8674
    Epoch 83/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0358 - accuracy: 0.9954 - val_loss: 3.6768 - val_accuracy: 0.8674
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0386 - accuracy: 0.9917 - val_loss: 3.7363 - val_accuracy: 0.8785
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0458 - accuracy: 0.9936 - val_loss: 3.9632 - val_accuracy: 0.8840
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0609 - accuracy: 0.9936 - val_loss: 5.3089 - val_accuracy: 0.8785
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0663 - accuracy: 0.9917 - val_loss: 4.8664 - val_accuracy: 0.8785
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0422 - accuracy: 0.9954 - val_loss: 3.5425 - val_accuracy: 0.8840
    Epoch 89/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0402 - accuracy: 0.9954 - val_loss: 4.1001 - val_accuracy: 0.8785
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0062 - accuracy: 0.9982 - val_loss: 4.1680 - val_accuracy: 0.8950
    Epoch 91/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0068 - accuracy: 0.9991 - val_loss: 3.3685 - val_accuracy: 0.8950
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0210 - accuracy: 0.9982 - val_loss: 3.0873 - val_accuracy: 0.8785
    Epoch 93/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0396 - accuracy: 0.9945 - val_loss: 5.3500 - val_accuracy: 0.8950
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0140 - accuracy: 0.9963 - val_loss: 7.1564 - val_accuracy: 0.8785
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0440 - accuracy: 0.9954 - val_loss: 4.9109 - val_accuracy: 0.8840
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0258 - accuracy: 0.9972 - val_loss: 4.0623 - val_accuracy: 0.8950
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0132 - accuracy: 0.9982 - val_loss: 3.9601 - val_accuracy: 0.9006
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0394 - accuracy: 0.9982 - val_loss: 2.9260 - val_accuracy: 0.9061
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0069 - accuracy: 0.9991 - val_loss: 2.5311 - val_accuracy: 0.9171
    Epoch 100/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0155 - accuracy: 0.9963 - val_loss: 3.2124 - val_accuracy: 0.9061



```python
new_model.save_weights(path + '/model.h5')
```


```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```


```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```


    
![png](VGGINET_woDA_200X_files/VGGINET_woDA_200X_17_0.png)
    



```python
new_model.evaluate(test_ds)
```

    5/5 [==============================] - 17s 3s/step - loss: 3.6443 - accuracy: 0.9156





    [3.644345998764038, 0.9155963063240051]




```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

    Accuracy:  0.9155963302752294
                  precision    recall  f1-score   support
    
               0       0.89      0.81      0.85       158
               1       0.93      0.96      0.94       387
    
        accuracy                           0.92       545
       macro avg       0.91      0.88      0.89       545
    weighted avg       0.91      0.92      0.91       545
    



```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```


```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```




    <AxesSubplot:title={'center':'Normalized Confusion Matrix'}, xlabel='Predicted label', ylabel='True label'>




    
![png](VGGINET_woDA_200X_files/VGGINET_woDA_200X_21_1.png)
    



    
![png](VGGINET_woDA_200X_files/VGGINET_woDA_200X_21_2.png)
    



```python
plot_roc(y_true, y_probas)
```




    <AxesSubplot:title={'center':'ROC Curves'}, xlabel='False Positive Rate', ylabel='True Positive Rate'>




    
![png](VGGINET_woDA_200X_files/VGGINET_woDA_200X_22_1.png)
    



```python
from imblearn.metrics import classification_report_imbalanced
```


```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.89      0.81      0.85       158
               1       0.93      0.96      0.94       387
    
        accuracy                           0.92       545
       macro avg       0.91      0.88      0.89       545
    weighted avg       0.91      0.92      0.91       545
    
                       pre       rec       spe        f1       geo       iba       sup
    
              0       0.89      0.81      0.96      0.85      0.88      0.77       158
              1       0.93      0.96      0.81      0.94      0.88      0.79       387
    
    avg / total       0.91      0.92      0.85      0.91      0.88      0.78       545
    



```python

```
