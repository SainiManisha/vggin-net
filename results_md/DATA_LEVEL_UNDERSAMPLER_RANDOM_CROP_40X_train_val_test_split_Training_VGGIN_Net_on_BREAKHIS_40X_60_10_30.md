<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 images belonging to 2 classes.
    Found 179 images belonging to 2 classes.
    Found 539 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.under_sampling import RandomUnderSampler
from tensorflow.keras.utils import to_categorical
ros = RandomUnderSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((630, 224, 340, 3), (630, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(1000).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/5 [=====&gt;........................] - ETA: 0s - loss: 1.1064 - accuracy: 0.5000WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    5/5 [==============================] - 9s 2s/step - loss: 5.3848 - accuracy: 0.6381 - val_loss: 31.2217 - val_accuracy: 0.5922
    Epoch 2/100
    5/5 [==============================] - 4s 802ms/step - loss: 1.9936 - accuracy: 0.8127 - val_loss: 6.3307 - val_accuracy: 0.8771
    Epoch 3/100
    5/5 [==============================] - 4s 720ms/step - loss: 1.7170 - accuracy: 0.8603 - val_loss: 5.8915 - val_accuracy: 0.8715
    Epoch 4/100
    5/5 [==============================] - 4s 774ms/step - loss: 1.3631 - accuracy: 0.8762 - val_loss: 5.7089 - val_accuracy: 0.8603
    Epoch 5/100
    5/5 [==============================] - 4s 728ms/step - loss: 1.1590 - accuracy: 0.8667 - val_loss: 6.0385 - val_accuracy: 0.7989
    Epoch 6/100
    5/5 [==============================] - 4s 708ms/step - loss: 1.4402 - accuracy: 0.8778 - val_loss: 3.0303 - val_accuracy: 0.8715
    Epoch 7/100
    5/5 [==============================] - 4s 764ms/step - loss: 0.9449 - accuracy: 0.8889 - val_loss: 2.3798 - val_accuracy: 0.9050
    Epoch 8/100
    5/5 [==============================] - 4s 765ms/step - loss: 0.8445 - accuracy: 0.9143 - val_loss: 2.4915 - val_accuracy: 0.8939
    Epoch 9/100
    5/5 [==============================] - 5s 977ms/step - loss: 0.8191 - accuracy: 0.9048 - val_loss: 2.6813 - val_accuracy: 0.8994
    Epoch 10/100
    5/5 [==============================] - 4s 726ms/step - loss: 0.7365 - accuracy: 0.9222 - val_loss: 4.5879 - val_accuracy: 0.8045
    Epoch 11/100
    5/5 [==============================] - 4s 734ms/step - loss: 0.6738 - accuracy: 0.9317 - val_loss: 1.9511 - val_accuracy: 0.9218
    Epoch 12/100
    5/5 [==============================] - 4s 739ms/step - loss: 0.4998 - accuracy: 0.9302 - val_loss: 2.2052 - val_accuracy: 0.9162
    Epoch 13/100
    5/5 [==============================] - 4s 734ms/step - loss: 0.6323 - accuracy: 0.9444 - val_loss: 2.0817 - val_accuracy: 0.9162
    Epoch 14/100
    5/5 [==============================] - 4s 705ms/step - loss: 0.5434 - accuracy: 0.9286 - val_loss: 2.2046 - val_accuracy: 0.8883
    Epoch 15/100
    5/5 [==============================] - 4s 780ms/step - loss: 0.6081 - accuracy: 0.9349 - val_loss: 2.3066 - val_accuracy: 0.9106
    Epoch 16/100
    5/5 [==============================] - 4s 728ms/step - loss: 0.5314 - accuracy: 0.9444 - val_loss: 2.7169 - val_accuracy: 0.9162
    Epoch 17/100
    5/5 [==============================] - 4s 796ms/step - loss: 0.4331 - accuracy: 0.9508 - val_loss: 2.7197 - val_accuracy: 0.8994
    Epoch 18/100
    5/5 [==============================] - 4s 718ms/step - loss: 0.4602 - accuracy: 0.9571 - val_loss: 1.5365 - val_accuracy: 0.9218
    Epoch 19/100
    5/5 [==============================] - 4s 766ms/step - loss: 0.3941 - accuracy: 0.9524 - val_loss: 1.5655 - val_accuracy: 0.9330
    Epoch 20/100
    5/5 [==============================] - 4s 799ms/step - loss: 0.3753 - accuracy: 0.9603 - val_loss: 1.6208 - val_accuracy: 0.9330
    Epoch 21/100
    5/5 [==============================] - 4s 794ms/step - loss: 0.3363 - accuracy: 0.9556 - val_loss: 1.0510 - val_accuracy: 0.9274
    Epoch 22/100
    5/5 [==============================] - 4s 737ms/step - loss: 0.5284 - accuracy: 0.9508 - val_loss: 1.2932 - val_accuracy: 0.9106
    Epoch 23/100
    5/5 [==============================] - 4s 778ms/step - loss: 0.4446 - accuracy: 0.9571 - val_loss: 1.1694 - val_accuracy: 0.8883
    Epoch 24/100
    5/5 [==============================] - 4s 804ms/step - loss: 0.5625 - accuracy: 0.9476 - val_loss: 1.6038 - val_accuracy: 0.9162
    Epoch 25/100
    5/5 [==============================] - 4s 812ms/step - loss: 0.3858 - accuracy: 0.9619 - val_loss: 1.3211 - val_accuracy: 0.9330
    Epoch 26/100
    5/5 [==============================] - 4s 793ms/step - loss: 0.2281 - accuracy: 0.9698 - val_loss: 1.1539 - val_accuracy: 0.9330
    Epoch 27/100
    5/5 [==============================] - 4s 727ms/step - loss: 0.3182 - accuracy: 0.9651 - val_loss: 1.3986 - val_accuracy: 0.9274
    Epoch 28/100
    5/5 [==============================] - 4s 794ms/step - loss: 0.3119 - accuracy: 0.9667 - val_loss: 2.5400 - val_accuracy: 0.9218
    Epoch 29/100
    5/5 [==============================] - 4s 715ms/step - loss: 0.5440 - accuracy: 0.9460 - val_loss: 2.0237 - val_accuracy: 0.8939
    Epoch 30/100
    5/5 [==============================] - 4s 786ms/step - loss: 0.5678 - accuracy: 0.9460 - val_loss: 1.6143 - val_accuracy: 0.8994
    Epoch 31/100
    5/5 [==============================] - 4s 719ms/step - loss: 0.4275 - accuracy: 0.9556 - val_loss: 2.5066 - val_accuracy: 0.8994
    Epoch 32/100
    5/5 [==============================] - 4s 802ms/step - loss: 0.2972 - accuracy: 0.9714 - val_loss: 2.2428 - val_accuracy: 0.9162
    Epoch 33/100
    5/5 [==============================] - 4s 794ms/step - loss: 0.8942 - accuracy: 0.9444 - val_loss: 1.5890 - val_accuracy: 0.9385
    Epoch 34/100
    5/5 [==============================] - 4s 778ms/step - loss: 0.5441 - accuracy: 0.9635 - val_loss: 2.1516 - val_accuracy: 0.9050
    Epoch 35/100
    5/5 [==============================] - 4s 789ms/step - loss: 0.3783 - accuracy: 0.9698 - val_loss: 1.6951 - val_accuracy: 0.9330
    Epoch 36/100
    5/5 [==============================] - 4s 735ms/step - loss: 0.2928 - accuracy: 0.9730 - val_loss: 2.1946 - val_accuracy: 0.9274
    Epoch 37/100
    5/5 [==============================] - 4s 720ms/step - loss: 0.4219 - accuracy: 0.9651 - val_loss: 1.5723 - val_accuracy: 0.9218
    Epoch 38/100
    5/5 [==============================] - 4s 706ms/step - loss: 0.2957 - accuracy: 0.9746 - val_loss: 1.3765 - val_accuracy: 0.9553
    Epoch 39/100
    5/5 [==============================] - 4s 709ms/step - loss: 0.2623 - accuracy: 0.9730 - val_loss: 1.5669 - val_accuracy: 0.9441
    Epoch 40/100
    5/5 [==============================] - 4s 712ms/step - loss: 0.0979 - accuracy: 0.9810 - val_loss: 3.0698 - val_accuracy: 0.8883
    Epoch 41/100
    5/5 [==============================] - 4s 784ms/step - loss: 0.0966 - accuracy: 0.9794 - val_loss: 3.6984 - val_accuracy: 0.8659
    Epoch 42/100
    5/5 [==============================] - 4s 860ms/step - loss: 0.1401 - accuracy: 0.9873 - val_loss: 2.2476 - val_accuracy: 0.9050
    Epoch 43/100
    5/5 [==============================] - 4s 714ms/step - loss: 0.2646 - accuracy: 0.9746 - val_loss: 2.3052 - val_accuracy: 0.9218
    Epoch 44/100
    5/5 [==============================] - 4s 770ms/step - loss: 0.3701 - accuracy: 0.9698 - val_loss: 3.0731 - val_accuracy: 0.9106
    Epoch 45/100
    5/5 [==============================] - 4s 703ms/step - loss: 0.2944 - accuracy: 0.9714 - val_loss: 1.2339 - val_accuracy: 0.9441
    Epoch 46/100
    5/5 [==============================] - 4s 776ms/step - loss: 0.2631 - accuracy: 0.9730 - val_loss: 1.5984 - val_accuracy: 0.9330
    Epoch 47/100
    5/5 [==============================] - 4s 777ms/step - loss: 0.3296 - accuracy: 0.9683 - val_loss: 1.2820 - val_accuracy: 0.9441
    Epoch 48/100
    5/5 [==============================] - 4s 742ms/step - loss: 0.2415 - accuracy: 0.9794 - val_loss: 1.4044 - val_accuracy: 0.9330
    Epoch 49/100
    5/5 [==============================] - 4s 789ms/step - loss: 0.2109 - accuracy: 0.9810 - val_loss: 1.9434 - val_accuracy: 0.8883
    Epoch 50/100
    5/5 [==============================] - 4s 794ms/step - loss: 0.1811 - accuracy: 0.9841 - val_loss: 2.1221 - val_accuracy: 0.8939
    Epoch 51/100
    5/5 [==============================] - 4s 784ms/step - loss: 0.2454 - accuracy: 0.9762 - val_loss: 1.9607 - val_accuracy: 0.9218
    Epoch 52/100
    5/5 [==============================] - 4s 719ms/step - loss: 0.2681 - accuracy: 0.9762 - val_loss: 1.6964 - val_accuracy: 0.9274
    Epoch 53/100
    5/5 [==============================] - 4s 811ms/step - loss: 0.2497 - accuracy: 0.9730 - val_loss: 1.3142 - val_accuracy: 0.9274
    Epoch 54/100
    5/5 [==============================] - 3s 695ms/step - loss: 0.0923 - accuracy: 0.9905 - val_loss: 1.7037 - val_accuracy: 0.8994
    Epoch 55/100
    5/5 [==============================] - 4s 777ms/step - loss: 0.1158 - accuracy: 0.9873 - val_loss: 2.1874 - val_accuracy: 0.8883
    Epoch 56/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.2272 - accuracy: 0.9810 - val_loss: 1.4552 - val_accuracy: 0.9106
    Epoch 57/100
    5/5 [==============================] - 4s 797ms/step - loss: 0.1824 - accuracy: 0.9857 - val_loss: 1.5577 - val_accuracy: 0.9385
    Epoch 58/100
    5/5 [==============================] - 4s 728ms/step - loss: 0.2801 - accuracy: 0.9825 - val_loss: 1.7061 - val_accuracy: 0.9162
    Epoch 59/100
    5/5 [==============================] - 4s 733ms/step - loss: 0.1450 - accuracy: 0.9810 - val_loss: 1.8916 - val_accuracy: 0.9106
    Epoch 60/100
    5/5 [==============================] - 4s 715ms/step - loss: 0.1305 - accuracy: 0.9841 - val_loss: 1.0679 - val_accuracy: 0.9274
    Epoch 61/100
    5/5 [==============================] - 4s 786ms/step - loss: 0.0993 - accuracy: 0.9841 - val_loss: 1.2305 - val_accuracy: 0.9385
    Epoch 62/100
    5/5 [==============================] - 4s 788ms/step - loss: 0.1438 - accuracy: 0.9857 - val_loss: 1.4027 - val_accuracy: 0.9050
    Epoch 63/100
    5/5 [==============================] - 4s 705ms/step - loss: 0.1964 - accuracy: 0.9778 - val_loss: 1.9586 - val_accuracy: 0.9050
    Epoch 64/100
    5/5 [==============================] - 4s 716ms/step - loss: 0.1956 - accuracy: 0.9857 - val_loss: 1.7104 - val_accuracy: 0.9162
    Epoch 65/100
    5/5 [==============================] - 4s 778ms/step - loss: 0.1646 - accuracy: 0.9873 - val_loss: 1.7992 - val_accuracy: 0.9162
    Epoch 66/100
    5/5 [==============================] - 4s 702ms/step - loss: 0.1621 - accuracy: 0.9794 - val_loss: 1.5926 - val_accuracy: 0.9330
    Epoch 67/100
    5/5 [==============================] - 4s 778ms/step - loss: 0.3240 - accuracy: 0.9762 - val_loss: 2.1377 - val_accuracy: 0.9218
    Epoch 68/100
    5/5 [==============================] - 4s 821ms/step - loss: 0.1857 - accuracy: 0.9730 - val_loss: 2.3770 - val_accuracy: 0.9106
    Epoch 69/100
    5/5 [==============================] - 4s 796ms/step - loss: 0.1642 - accuracy: 0.9794 - val_loss: 2.3594 - val_accuracy: 0.9162
    Epoch 70/100
    5/5 [==============================] - 4s 780ms/step - loss: 0.0674 - accuracy: 0.9905 - val_loss: 1.6429 - val_accuracy: 0.9218
    Epoch 71/100
    5/5 [==============================] - 4s 785ms/step - loss: 0.1316 - accuracy: 0.9873 - val_loss: 1.5667 - val_accuracy: 0.9330
    Epoch 72/100
    5/5 [==============================] - 4s 778ms/step - loss: 0.2810 - accuracy: 0.9857 - val_loss: 2.1929 - val_accuracy: 0.9106
    Epoch 73/100
    5/5 [==============================] - 4s 764ms/step - loss: 0.2642 - accuracy: 0.9857 - val_loss: 2.3083 - val_accuracy: 0.9218
    Epoch 74/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.1226 - accuracy: 0.9905 - val_loss: 2.3434 - val_accuracy: 0.8994
    Epoch 75/100
    5/5 [==============================] - 4s 717ms/step - loss: 0.2042 - accuracy: 0.9762 - val_loss: 3.4953 - val_accuracy: 0.8994
    Epoch 76/100
    5/5 [==============================] - 4s 775ms/step - loss: 0.1216 - accuracy: 0.9873 - val_loss: 2.4087 - val_accuracy: 0.9162
    Epoch 77/100
    5/5 [==============================] - 4s 733ms/step - loss: 0.1698 - accuracy: 0.9873 - val_loss: 1.8070 - val_accuracy: 0.9162
    Epoch 78/100
    5/5 [==============================] - 4s 715ms/step - loss: 0.1909 - accuracy: 0.9857 - val_loss: 1.6721 - val_accuracy: 0.9274
    Epoch 79/100
    5/5 [==============================] - 4s 769ms/step - loss: 0.1360 - accuracy: 0.9857 - val_loss: 2.5990 - val_accuracy: 0.8994
    Epoch 80/100
    5/5 [==============================] - 4s 789ms/step - loss: 0.1758 - accuracy: 0.9857 - val_loss: 2.2849 - val_accuracy: 0.8939
    Epoch 81/100
    5/5 [==============================] - 4s 714ms/step - loss: 0.2146 - accuracy: 0.9889 - val_loss: 2.7920 - val_accuracy: 0.9162
    Epoch 82/100
    5/5 [==============================] - 4s 790ms/step - loss: 0.5358 - accuracy: 0.9730 - val_loss: 1.7803 - val_accuracy: 0.9218
    Epoch 83/100
    5/5 [==============================] - 4s 765ms/step - loss: 0.0895 - accuracy: 0.9825 - val_loss: 3.1026 - val_accuracy: 0.9106
    Epoch 84/100
    5/5 [==============================] - 4s 721ms/step - loss: 0.3001 - accuracy: 0.9841 - val_loss: 3.8901 - val_accuracy: 0.8883
    Epoch 85/100
    5/5 [==============================] - 4s 708ms/step - loss: 0.2409 - accuracy: 0.9857 - val_loss: 3.2774 - val_accuracy: 0.9274
    Epoch 86/100
    5/5 [==============================] - 3s 685ms/step - loss: 0.2037 - accuracy: 0.9825 - val_loss: 3.0080 - val_accuracy: 0.9218
    Epoch 87/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.3052 - accuracy: 0.9714 - val_loss: 3.0046 - val_accuracy: 0.9106
    Epoch 88/100
    5/5 [==============================] - 4s 793ms/step - loss: 0.5312 - accuracy: 0.9762 - val_loss: 2.6185 - val_accuracy: 0.9106
    Epoch 89/100
    5/5 [==============================] - 4s 824ms/step - loss: 0.2658 - accuracy: 0.9794 - val_loss: 3.0687 - val_accuracy: 0.9162
    Epoch 90/100
    5/5 [==============================] - 4s 721ms/step - loss: 0.2015 - accuracy: 0.9810 - val_loss: 4.1669 - val_accuracy: 0.8827
    Epoch 91/100
    5/5 [==============================] - 4s 722ms/step - loss: 0.4889 - accuracy: 0.9714 - val_loss: 2.1243 - val_accuracy: 0.9106
    Epoch 92/100
    5/5 [==============================] - 4s 763ms/step - loss: 0.1964 - accuracy: 0.9778 - val_loss: 2.3496 - val_accuracy: 0.9274
    Epoch 93/100
    5/5 [==============================] - 4s 711ms/step - loss: 0.3033 - accuracy: 0.9762 - val_loss: 2.4109 - val_accuracy: 0.9274
    Epoch 94/100
    5/5 [==============================] - 4s 783ms/step - loss: 0.1243 - accuracy: 0.9889 - val_loss: 1.6615 - val_accuracy: 0.9385
    Epoch 95/100
    5/5 [==============================] - 4s 774ms/step - loss: 0.3115 - accuracy: 0.9810 - val_loss: 1.5270 - val_accuracy: 0.9441
    Epoch 96/100
    5/5 [==============================] - 4s 784ms/step - loss: 0.3141 - accuracy: 0.9825 - val_loss: 2.2376 - val_accuracy: 0.9274
    Epoch 97/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.1603 - accuracy: 0.9857 - val_loss: 3.5500 - val_accuracy: 0.9162
    Epoch 98/100
    5/5 [==============================] - 4s 763ms/step - loss: 0.1341 - accuracy: 0.9873 - val_loss: 4.1111 - val_accuracy: 0.9050
    Epoch 99/100
    5/5 [==============================] - 4s 710ms/step - loss: 0.1209 - accuracy: 0.9921 - val_loss: 4.0337 - val_accuracy: 0.8883
    Epoch 100/100
    5/5 [==============================] - 4s 709ms/step - loss: 0.2063 - accuracy: 0.9841 - val_loss: 3.4419 - val_accuracy: 0.9106

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/7.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 3s 543ms/step - loss: 1.6482 - accuracy: 0.9592

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[29\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.6481999158859253, 0.9591836929321289]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9591836734693877
                  precision    recall  f1-score   support

               0       0.95      0.91      0.93       158
               1       0.96      0.98      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.95      0.95       539
    weighted avg       0.96      0.96      0.96       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[32\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/8.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/9.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[33\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/10.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.91      0.93       158
               1       0.96      0.98      0.97       381

        accuracy                           0.96       539
       macro avg       0.96      0.95      0.95       539
    weighted avg       0.96      0.96      0.96       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.91      0.98      0.93      0.94      0.89       158
              1       0.96      0.98      0.91      0.97      0.94      0.90       381

    avg / total       0.96      0.96      0.93      0.96      0.94      0.89       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
