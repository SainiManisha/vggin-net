<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/VGG16-NORMAL-RANDOMCROP_400X-BREAKHIS-Dataset-60-10-30'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_400X/train'
val_path = '../Splitted_400X/val'
test_path = '../Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/8 [==&gt;...........................] - ETA: 0s - loss: 0.6108 - accuracy: 0.6875WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 35s 4s/step - loss: 20.4388 - accuracy: 0.6138 - val_loss: 2.4991 - val_accuracy: 0.2981
    Epoch 2/50
    8/8 [==============================] - 14s 2s/step - loss: 1.4540 - accuracy: 0.5901 - val_loss: 0.4514 - val_accuracy: 0.7391
    Epoch 3/50
    8/8 [==============================] - 13s 2s/step - loss: 0.6969 - accuracy: 0.7137 - val_loss: 0.3583 - val_accuracy: 0.8944
    Epoch 4/50
    8/8 [==============================] - 14s 2s/step - loss: 0.7615 - accuracy: 0.7147 - val_loss: 0.8334 - val_accuracy: 0.7143
    Epoch 5/50
    8/8 [==============================] - 14s 2s/step - loss: 0.7139 - accuracy: 0.7425 - val_loss: 0.4960 - val_accuracy: 0.7888
    Epoch 6/50
    8/8 [==============================] - 13s 2s/step - loss: 0.5793 - accuracy: 0.7878 - val_loss: 0.4350 - val_accuracy: 0.8137
    Epoch 7/50
    8/8 [==============================] - 14s 2s/step - loss: 0.4913 - accuracy: 0.8033 - val_loss: 0.3013 - val_accuracy: 0.8944
    Epoch 8/50
    8/8 [==============================] - 13s 2s/step - loss: 0.3706 - accuracy: 0.8568 - val_loss: 0.2749 - val_accuracy: 0.9006
    Epoch 9/50
    8/8 [==============================] - 14s 2s/step - loss: 0.4152 - accuracy: 0.8239 - val_loss: 0.2743 - val_accuracy: 0.8944
    Epoch 10/50
    8/8 [==============================] - 14s 2s/step - loss: 0.3511 - accuracy: 0.8589 - val_loss: 0.2663 - val_accuracy: 0.9193
    Epoch 11/50
    8/8 [==============================] - 13s 2s/step - loss: 0.3352 - accuracy: 0.8630 - val_loss: 0.2466 - val_accuracy: 0.9068
    Epoch 12/50
    8/8 [==============================] - 13s 2s/step - loss: 0.3114 - accuracy: 0.8599 - val_loss: 0.2613 - val_accuracy: 0.9006
    Epoch 13/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2985 - accuracy: 0.8816 - val_loss: 0.2284 - val_accuracy: 0.9193
    Epoch 14/50
    8/8 [==============================] - 17s 2s/step - loss: 0.2822 - accuracy: 0.8908 - val_loss: 0.2378 - val_accuracy: 0.9006
    Epoch 15/50
    8/8 [==============================] - 13s 2s/step - loss: 0.3050 - accuracy: 0.8744 - val_loss: 0.2245 - val_accuracy: 0.9193
    Epoch 16/50
    8/8 [==============================] - 14s 2s/step - loss: 0.2570 - accuracy: 0.8980 - val_loss: 0.2329 - val_accuracy: 0.9317
    Epoch 17/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2754 - accuracy: 0.8970 - val_loss: 0.2699 - val_accuracy: 0.8696
    Epoch 18/50
    8/8 [==============================] - 13s 2s/step - loss: 0.3358 - accuracy: 0.8507 - val_loss: 0.2505 - val_accuracy: 0.9006
    Epoch 19/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2722 - accuracy: 0.8877 - val_loss: 0.2276 - val_accuracy: 0.9379
    Epoch 20/50
    8/8 [==============================] - 14s 2s/step - loss: 0.2406 - accuracy: 0.9125 - val_loss: 0.2543 - val_accuracy: 0.9130
    Epoch 21/50
    8/8 [==============================] - 16s 2s/step - loss: 0.2466 - accuracy: 0.9022 - val_loss: 0.2147 - val_accuracy: 0.9379
    Epoch 22/50
    8/8 [==============================] - 18s 2s/step - loss: 0.2383 - accuracy: 0.9001 - val_loss: 0.4510 - val_accuracy: 0.7950
    Epoch 23/50
    8/8 [==============================] - 13s 2s/step - loss: 0.4690 - accuracy: 0.8126 - val_loss: 0.3017 - val_accuracy: 0.8882
    Epoch 24/50
    8/8 [==============================] - 20s 3s/step - loss: 0.4777 - accuracy: 0.8342 - val_loss: 0.2027 - val_accuracy: 0.9255
    Epoch 25/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2412 - accuracy: 0.9063 - val_loss: 0.2396 - val_accuracy: 0.9130
    Epoch 26/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2682 - accuracy: 0.8816 - val_loss: 0.2335 - val_accuracy: 0.9130
    Epoch 27/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2238 - accuracy: 0.9145 - val_loss: 0.2491 - val_accuracy: 0.9193
    Epoch 28/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2347 - accuracy: 0.9032 - val_loss: 0.2542 - val_accuracy: 0.9255
    Epoch 29/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2262 - accuracy: 0.9166 - val_loss: 0.2653 - val_accuracy: 0.9130
    Epoch 30/50
    8/8 [==============================] - 14s 2s/step - loss: 0.1876 - accuracy: 0.9248 - val_loss: 0.2401 - val_accuracy: 0.9130
    Epoch 31/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1969 - accuracy: 0.9279 - val_loss: 0.2330 - val_accuracy: 0.9068
    Epoch 32/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1959 - accuracy: 0.9197 - val_loss: 0.2392 - val_accuracy: 0.9130
    Epoch 33/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2043 - accuracy: 0.9217 - val_loss: 0.2405 - val_accuracy: 0.9130
    Epoch 34/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1940 - accuracy: 0.9186 - val_loss: 0.2255 - val_accuracy: 0.9255
    Epoch 35/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1687 - accuracy: 0.9341 - val_loss: 0.2211 - val_accuracy: 0.9193
    Epoch 36/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1841 - accuracy: 0.9228 - val_loss: 0.2877 - val_accuracy: 0.9006
    Epoch 37/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2172 - accuracy: 0.9135 - val_loss: 0.2977 - val_accuracy: 0.9006
    Epoch 38/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2723 - accuracy: 0.8857 - val_loss: 0.2073 - val_accuracy: 0.9193
    Epoch 39/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2182 - accuracy: 0.9094 - val_loss: 0.2140 - val_accuracy: 0.9379
    Epoch 40/50
    8/8 [==============================] - 17s 2s/step - loss: 0.2227 - accuracy: 0.9073 - val_loss: 0.2630 - val_accuracy: 0.9006
    Epoch 41/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2220 - accuracy: 0.8991 - val_loss: 0.1949 - val_accuracy: 0.9379
    Epoch 42/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1946 - accuracy: 0.9228 - val_loss: 0.1721 - val_accuracy: 0.9379
    Epoch 43/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1922 - accuracy: 0.9197 - val_loss: 0.2126 - val_accuracy: 0.9317
    Epoch 44/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2658 - accuracy: 0.8939 - val_loss: 0.2729 - val_accuracy: 0.8944
    Epoch 45/50
    8/8 [==============================] - 13s 2s/step - loss: 0.2796 - accuracy: 0.8929 - val_loss: 0.1963 - val_accuracy: 0.9379
    Epoch 46/50
    8/8 [==============================] - 14s 2s/step - loss: 0.1974 - accuracy: 0.9197 - val_loss: 0.2098 - val_accuracy: 0.9317
    Epoch 47/50
    8/8 [==============================] - 14s 2s/step - loss: 0.1842 - accuracy: 0.9228 - val_loss: 0.2078 - val_accuracy: 0.9255
    Epoch 48/50
    8/8 [==============================] - 14s 2s/step - loss: 0.1777 - accuracy: 0.9392 - val_loss: 0.1962 - val_accuracy: 0.9317
    Epoch 49/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1670 - accuracy: 0.9310 - val_loss: 0.1895 - val_accuracy: 0.9317
    Epoch 50/50
    8/8 [==============================] - 13s 2s/step - loss: 0.1592 - accuracy: 0.9341 - val_loss: 0.2116 - val_accuracy: 0.9130

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/103.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 31s 8s/step - loss: 0.2994 - accuracy: 0.8914

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.2994438409805298, 0.8913934230804443]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8913934426229508
                  precision    recall  f1-score   support

               0       0.87      0.76      0.81       148
               1       0.90      0.95      0.92       340

        accuracy                           0.89       488
       macro avg       0.88      0.85      0.87       488
    weighted avg       0.89      0.89      0.89       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/104.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/105.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/106.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.87      0.76      0.81       148
               1       0.90      0.95      0.92       340

        accuracy                           0.89       488
       macro avg       0.88      0.85      0.87       488
    weighted avg       0.89      0.89      0.89       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.87      0.76      0.95      0.81      0.85      0.71       148
              1       0.90      0.95      0.76      0.92      0.85      0.73       340

    avg / total       0.89      0.89      0.82      0.89      0.85      0.72       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
