<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 64
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 images belonging to 2 classes.
    Found 181 images belonging to 2 classes.
    Found 545 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.over_sampling import RandomOverSampler
from tensorflow.keras.utils import to_categorical
ros = RandomOverSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((1548, 224, 340, 3), (1548, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_val.shape, y_val.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[17\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((181, 224, 340, 3), (181, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_test.shape, y_test.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((545, 224, 340, 3), (545, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(100).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_200X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
     1/25 [&gt;.............................] - ETA: 0s - loss: 1.0829 - accuracy: 0.5625WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    25/25 [==============================] - 22s 890ms/step - loss: 3.2959 - accuracy: 0.8094 - val_loss: 91.9057 - val_accuracy: 0.3039
    Epoch 2/100
    25/25 [==============================] - 22s 892ms/step - loss: 22.5901 - accuracy: 0.6537 - val_loss: 57.1995 - val_accuracy: 0.3867
    Epoch 3/100
    25/25 [==============================] - 21s 829ms/step - loss: 35.6206 - accuracy: 0.5297 - val_loss: 3.5304 - val_accuracy: 0.9171
    Epoch 4/100
    25/25 [==============================] - 20s 818ms/step - loss: 22.8904 - accuracy: 0.5762 - val_loss: 2.7794 - val_accuracy: 0.9392
    Epoch 5/100
    25/25 [==============================] - 20s 798ms/step - loss: 10.7333 - accuracy: 0.6977 - val_loss: 1.5858 - val_accuracy: 0.9282
    Epoch 6/100
    25/25 [==============================] - 20s 811ms/step - loss: 9.4873 - accuracy: 0.7048 - val_loss: 3.0350 - val_accuracy: 0.8564
    Epoch 7/100
    25/25 [==============================] - 22s 863ms/step - loss: 8.7881 - accuracy: 0.7003 - val_loss: 2.3453 - val_accuracy: 0.8950
    Epoch 8/100
    25/25 [==============================] - 20s 785ms/step - loss: 9.2931 - accuracy: 0.7016 - val_loss: 1.9463 - val_accuracy: 0.9061
    Epoch 9/100
    25/25 [==============================] - 21s 827ms/step - loss: 7.3049 - accuracy: 0.7371 - val_loss: 2.4129 - val_accuracy: 0.8729
    Epoch 10/100
    25/25 [==============================] - 21s 837ms/step - loss: 6.6517 - accuracy: 0.7410 - val_loss: 2.6298 - val_accuracy: 0.8619
    Epoch 11/100
    25/25 [==============================] - 21s 825ms/step - loss: 4.6775 - accuracy: 0.7901 - val_loss: 5.9760 - val_accuracy: 0.7348
    Epoch 12/100
    25/25 [==============================] - 20s 798ms/step - loss: 3.8493 - accuracy: 0.8088 - val_loss: 2.6685 - val_accuracy: 0.8619
    Epoch 13/100
    25/25 [==============================] - 21s 825ms/step - loss: 4.4156 - accuracy: 0.7952 - val_loss: 2.6316 - val_accuracy: 0.8564
    Epoch 14/100
    25/25 [==============================] - 20s 808ms/step - loss: 4.2201 - accuracy: 0.7946 - val_loss: 2.8475 - val_accuracy: 0.8398
    Epoch 15/100
    25/25 [==============================] - 20s 800ms/step - loss: 4.2192 - accuracy: 0.8004 - val_loss: 1.9466 - val_accuracy: 0.8840
    Epoch 16/100
    25/25 [==============================] - 21s 836ms/step - loss: 4.0260 - accuracy: 0.8101 - val_loss: 2.2666 - val_accuracy: 0.8895
    Epoch 17/100
    25/25 [==============================] - 20s 800ms/step - loss: 3.2929 - accuracy: 0.8437 - val_loss: 2.4686 - val_accuracy: 0.8840
    Epoch 18/100
    25/25 [==============================] - 21s 828ms/step - loss: 2.9267 - accuracy: 0.8488 - val_loss: 2.0495 - val_accuracy: 0.8674
    Epoch 19/100
    25/25 [==============================] - 21s 842ms/step - loss: 2.2386 - accuracy: 0.8760 - val_loss: 2.8792 - val_accuracy: 0.8453
    Epoch 20/100
    25/25 [==============================] - 23s 926ms/step - loss: 2.1687 - accuracy: 0.8592 - val_loss: 2.3172 - val_accuracy: 0.8398
    Epoch 21/100
    25/25 [==============================] - 25s 990ms/step - loss: 1.9687 - accuracy: 0.8740 - val_loss: 2.6968 - val_accuracy: 0.8343
    Epoch 22/100
    25/25 [==============================] - 22s 873ms/step - loss: 2.3803 - accuracy: 0.8630 - val_loss: 2.1925 - val_accuracy: 0.8398
    Epoch 23/100
    25/25 [==============================] - 21s 842ms/step - loss: 2.0471 - accuracy: 0.8624 - val_loss: 2.4026 - val_accuracy: 0.8398
    Epoch 24/100
    25/25 [==============================] - 21s 845ms/step - loss: 2.3883 - accuracy: 0.8663 - val_loss: 2.1848 - val_accuracy: 0.8564
    Epoch 25/100
    25/25 [==============================] - 21s 828ms/step - loss: 1.8181 - accuracy: 0.8831 - val_loss: 1.5422 - val_accuracy: 0.8674
    Epoch 26/100
    25/25 [==============================] - 21s 840ms/step - loss: 1.6323 - accuracy: 0.8979 - val_loss: 1.3960 - val_accuracy: 0.9006
    Epoch 27/100
    25/25 [==============================] - 21s 840ms/step - loss: 1.6083 - accuracy: 0.9005 - val_loss: 1.9861 - val_accuracy: 0.8619
    Epoch 28/100
    25/25 [==============================] - 21s 842ms/step - loss: 1.6951 - accuracy: 0.8889 - val_loss: 1.1219 - val_accuracy: 0.9061
    Epoch 29/100
    25/25 [==============================] - 23s 900ms/step - loss: 1.5221 - accuracy: 0.9050 - val_loss: 2.1505 - val_accuracy: 0.8674
    Epoch 30/100
    25/25 [==============================] - 20s 815ms/step - loss: 1.7827 - accuracy: 0.8941 - val_loss: 2.8416 - val_accuracy: 0.8453
    Epoch 31/100
    25/25 [==============================] - 21s 830ms/step - loss: 1.5509 - accuracy: 0.8992 - val_loss: 1.4892 - val_accuracy: 0.9006
    Epoch 32/100
    25/25 [==============================] - 21s 839ms/step - loss: 1.7401 - accuracy: 0.9005 - val_loss: 1.7366 - val_accuracy: 0.9006
    Epoch 33/100
    25/25 [==============================] - 21s 833ms/step - loss: 1.3152 - accuracy: 0.9128 - val_loss: 1.3796 - val_accuracy: 0.9061
    Epoch 34/100
    25/25 [==============================] - 23s 907ms/step - loss: 1.5140 - accuracy: 0.9044 - val_loss: 1.8695 - val_accuracy: 0.8729
    Epoch 35/100
    25/25 [==============================] - 21s 829ms/step - loss: 1.1401 - accuracy: 0.9231 - val_loss: 1.9298 - val_accuracy: 0.8785
    Epoch 36/100
    25/25 [==============================] - 22s 891ms/step - loss: 1.1739 - accuracy: 0.9276 - val_loss: 1.1062 - val_accuracy: 0.9116
    Epoch 37/100
    25/25 [==============================] - 21s 841ms/step - loss: 1.0570 - accuracy: 0.9348 - val_loss: 0.8824 - val_accuracy: 0.9282
    Epoch 38/100
    25/25 [==============================] - 20s 818ms/step - loss: 0.8787 - accuracy: 0.9393 - val_loss: 1.7864 - val_accuracy: 0.9116
    Epoch 39/100
    25/25 [==============================] - 20s 816ms/step - loss: 0.8270 - accuracy: 0.9412 - val_loss: 0.8469 - val_accuracy: 0.9116
    Epoch 40/100
    25/25 [==============================] - 21s 828ms/step - loss: 0.5997 - accuracy: 0.9535 - val_loss: 0.7433 - val_accuracy: 0.9282
    Epoch 41/100
    25/25 [==============================] - 22s 877ms/step - loss: 0.8262 - accuracy: 0.9483 - val_loss: 1.1746 - val_accuracy: 0.9171
    Epoch 42/100
    25/25 [==============================] - 21s 838ms/step - loss: 1.2463 - accuracy: 0.9193 - val_loss: 1.2132 - val_accuracy: 0.9282
    Epoch 43/100
    25/25 [==============================] - 22s 880ms/step - loss: 0.6111 - accuracy: 0.9548 - val_loss: 1.0102 - val_accuracy: 0.8950
    Epoch 44/100
    25/25 [==============================] - 20s 815ms/step - loss: 0.9140 - accuracy: 0.9425 - val_loss: 0.8902 - val_accuracy: 0.9227
    Epoch 45/100
    25/25 [==============================] - 20s 820ms/step - loss: 0.7707 - accuracy: 0.9438 - val_loss: 1.2204 - val_accuracy: 0.9171
    Epoch 46/100
    25/25 [==============================] - 20s 810ms/step - loss: 0.8274 - accuracy: 0.9348 - val_loss: 1.9899 - val_accuracy: 0.8564
    Epoch 47/100
    25/25 [==============================] - 21s 826ms/step - loss: 0.9107 - accuracy: 0.9399 - val_loss: 1.3315 - val_accuracy: 0.9337
    Epoch 48/100
    25/25 [==============================] - 21s 821ms/step - loss: 0.9557 - accuracy: 0.9477 - val_loss: 2.6098 - val_accuracy: 0.8674
    Epoch 49/100
    25/25 [==============================] - 20s 811ms/step - loss: 1.1156 - accuracy: 0.9276 - val_loss: 3.3218 - val_accuracy: 0.8398
    Epoch 50/100
    25/25 [==============================] - 21s 845ms/step - loss: 1.2905 - accuracy: 0.9296 - val_loss: 0.7369 - val_accuracy: 0.9448
    Epoch 51/100
    25/25 [==============================] - 20s 802ms/step - loss: 0.8130 - accuracy: 0.9451 - val_loss: 2.5857 - val_accuracy: 0.9006
    Epoch 52/100
    25/25 [==============================] - 21s 835ms/step - loss: 0.8828 - accuracy: 0.9451 - val_loss: 1.1231 - val_accuracy: 0.9392
    Epoch 53/100
    25/25 [==============================] - 22s 873ms/step - loss: 0.6970 - accuracy: 0.9522 - val_loss: 1.2186 - val_accuracy: 0.9171
    Epoch 54/100
    25/25 [==============================] - 20s 807ms/step - loss: 0.4904 - accuracy: 0.9580 - val_loss: 0.7352 - val_accuracy: 0.9503
    Epoch 55/100
    25/25 [==============================] - 20s 820ms/step - loss: 0.6681 - accuracy: 0.9574 - val_loss: 1.4382 - val_accuracy: 0.9282
    Epoch 56/100
    25/25 [==============================] - 20s 800ms/step - loss: 0.8922 - accuracy: 0.9419 - val_loss: 2.2502 - val_accuracy: 0.8840
    Epoch 57/100
    25/25 [==============================] - 22s 866ms/step - loss: 0.6631 - accuracy: 0.9554 - val_loss: 2.1131 - val_accuracy: 0.9227
    Epoch 58/100
    25/25 [==============================] - 21s 820ms/step - loss: 0.5293 - accuracy: 0.9612 - val_loss: 1.0501 - val_accuracy: 0.9282
    Epoch 59/100
    25/25 [==============================] - 20s 790ms/step - loss: 0.9645 - accuracy: 0.9490 - val_loss: 1.1871 - val_accuracy: 0.9337
    Epoch 60/100
    25/25 [==============================] - 21s 844ms/step - loss: 0.9710 - accuracy: 0.9432 - val_loss: 1.7887 - val_accuracy: 0.9116
    Epoch 61/100
    25/25 [==============================] - 20s 807ms/step - loss: 1.1082 - accuracy: 0.9380 - val_loss: 1.6951 - val_accuracy: 0.8950
    Epoch 62/100
    25/25 [==============================] - 20s 799ms/step - loss: 0.8033 - accuracy: 0.9503 - val_loss: 1.8973 - val_accuracy: 0.9116
    Epoch 63/100
    25/25 [==============================] - 20s 806ms/step - loss: 0.3988 - accuracy: 0.9664 - val_loss: 1.3581 - val_accuracy: 0.9006
    Epoch 64/100
    25/25 [==============================] - 21s 850ms/step - loss: 0.5428 - accuracy: 0.9645 - val_loss: 1.8090 - val_accuracy: 0.8785
    Epoch 65/100
    25/25 [==============================] - 20s 788ms/step - loss: 0.5700 - accuracy: 0.9658 - val_loss: 1.6916 - val_accuracy: 0.9006
    Epoch 66/100
    25/25 [==============================] - 21s 854ms/step - loss: 0.4429 - accuracy: 0.9658 - val_loss: 1.6285 - val_accuracy: 0.9006
    Epoch 67/100
    25/25 [==============================] - 22s 878ms/step - loss: 0.6970 - accuracy: 0.9483 - val_loss: 1.6309 - val_accuracy: 0.9061
    Epoch 68/100
    25/25 [==============================] - 21s 828ms/step - loss: 0.6510 - accuracy: 0.9625 - val_loss: 1.2248 - val_accuracy: 0.9337
    Epoch 69/100
    25/25 [==============================] - 20s 816ms/step - loss: 0.6934 - accuracy: 0.9548 - val_loss: 1.2527 - val_accuracy: 0.9448
    Epoch 70/100
    25/25 [==============================] - 21s 860ms/step - loss: 0.8912 - accuracy: 0.9522 - val_loss: 0.9584 - val_accuracy: 0.9392
    Epoch 71/100
    25/25 [==============================] - 20s 799ms/step - loss: 0.4962 - accuracy: 0.9651 - val_loss: 1.8246 - val_accuracy: 0.9116
    Epoch 72/100
    25/25 [==============================] - 20s 792ms/step - loss: 0.4193 - accuracy: 0.9703 - val_loss: 1.4409 - val_accuracy: 0.9337
    Epoch 73/100
    25/25 [==============================] - 20s 790ms/step - loss: 0.7031 - accuracy: 0.9606 - val_loss: 2.2646 - val_accuracy: 0.9006
    Epoch 74/100
    25/25 [==============================] - 20s 791ms/step - loss: 0.7540 - accuracy: 0.9490 - val_loss: 2.2457 - val_accuracy: 0.8950
    Epoch 75/100
    25/25 [==============================] - 20s 802ms/step - loss: 0.9815 - accuracy: 0.9432 - val_loss: 1.2731 - val_accuracy: 0.9448
    Epoch 76/100
    25/25 [==============================] - 20s 789ms/step - loss: 0.7138 - accuracy: 0.9574 - val_loss: 1.5752 - val_accuracy: 0.9392
    Epoch 77/100
    25/25 [==============================] - 20s 803ms/step - loss: 0.6491 - accuracy: 0.9625 - val_loss: 1.6152 - val_accuracy: 0.9227
    Epoch 78/100
    25/25 [==============================] - 20s 812ms/step - loss: 0.8762 - accuracy: 0.9593 - val_loss: 2.1142 - val_accuracy: 0.9006
    Epoch 79/100
    25/25 [==============================] - 20s 801ms/step - loss: 0.8585 - accuracy: 0.9587 - val_loss: 1.2459 - val_accuracy: 0.9448
    Epoch 80/100
    25/25 [==============================] - 20s 811ms/step - loss: 0.8452 - accuracy: 0.9554 - val_loss: 2.1479 - val_accuracy: 0.9282
    Epoch 81/100
    25/25 [==============================] - 20s 800ms/step - loss: 0.7021 - accuracy: 0.9619 - val_loss: 1.2005 - val_accuracy: 0.9558
    Epoch 82/100
    25/25 [==============================] - 20s 808ms/step - loss: 0.4889 - accuracy: 0.9722 - val_loss: 1.4100 - val_accuracy: 0.9613
    Epoch 83/100
    25/25 [==============================] - 20s 811ms/step - loss: 0.5100 - accuracy: 0.9709 - val_loss: 2.2667 - val_accuracy: 0.9337
    Epoch 84/100
    25/25 [==============================] - 20s 797ms/step - loss: 0.7680 - accuracy: 0.9516 - val_loss: 1.6890 - val_accuracy: 0.9558
    Epoch 85/100
    25/25 [==============================] - 21s 843ms/step - loss: 0.4968 - accuracy: 0.9651 - val_loss: 1.8269 - val_accuracy: 0.9227
    Epoch 86/100
    25/25 [==============================] - 20s 818ms/step - loss: 0.4789 - accuracy: 0.9729 - val_loss: 2.6570 - val_accuracy: 0.8785
    Epoch 87/100
    25/25 [==============================] - 20s 814ms/step - loss: 0.5838 - accuracy: 0.9671 - val_loss: 1.5331 - val_accuracy: 0.9558
    Epoch 88/100
    25/25 [==============================] - 20s 791ms/step - loss: 0.3368 - accuracy: 0.9748 - val_loss: 1.5385 - val_accuracy: 0.9392
    Epoch 89/100
    25/25 [==============================] - 22s 875ms/step - loss: 0.5435 - accuracy: 0.9645 - val_loss: 1.3534 - val_accuracy: 0.9337
    Epoch 90/100
    25/25 [==============================] - 20s 808ms/step - loss: 0.7143 - accuracy: 0.9664 - val_loss: 2.0788 - val_accuracy: 0.9282
    Epoch 91/100
    25/25 [==============================] - 20s 794ms/step - loss: 0.4116 - accuracy: 0.9703 - val_loss: 0.7356 - val_accuracy: 0.9669
    Epoch 92/100
    25/25 [==============================] - 21s 844ms/step - loss: 0.5688 - accuracy: 0.9722 - val_loss: 0.9392 - val_accuracy: 0.9669
    Epoch 93/100
    25/25 [==============================] - 20s 797ms/step - loss: 0.4783 - accuracy: 0.9703 - val_loss: 0.6447 - val_accuracy: 0.9669
    Epoch 94/100
    25/25 [==============================] - 20s 791ms/step - loss: 0.3250 - accuracy: 0.9806 - val_loss: 0.8668 - val_accuracy: 0.9613
    Epoch 95/100
    25/25 [==============================] - 20s 781ms/step - loss: 0.3583 - accuracy: 0.9832 - val_loss: 0.7154 - val_accuracy: 0.9613
    Epoch 96/100
    25/25 [==============================] - 20s 812ms/step - loss: 0.3976 - accuracy: 0.9780 - val_loss: 1.0814 - val_accuracy: 0.9613
    Epoch 97/100
    25/25 [==============================] - 20s 795ms/step - loss: 0.5461 - accuracy: 0.9677 - val_loss: 0.7102 - val_accuracy: 0.9613
    Epoch 98/100
    25/25 [==============================] - 22s 860ms/step - loss: 0.4415 - accuracy: 0.9767 - val_loss: 1.0860 - val_accuracy: 0.9669
    Epoch 99/100
    25/25 [==============================] - 20s 806ms/step - loss: 0.3007 - accuracy: 0.9793 - val_loss: 0.6103 - val_accuracy: 0.9613
    Epoch 100/100
    25/25 [==============================] - 21s 856ms/step - loss: 0.3963 - accuracy: 0.9832 - val_loss: 0.6901 - val_accuracy: 0.9669

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/75.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 3s 385ms/step - loss: 1.1459 - accuracy: 0.9670

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[30\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.145859718322754, 0.9669724702835083]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9669724770642202
                  precision    recall  f1-score   support

               0       0.95      0.93      0.94       158
               1       0.97      0.98      0.98       387

        accuracy                           0.97       545
       macro avg       0.96      0.96      0.96       545
    weighted avg       0.97      0.97      0.97       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[33\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/76.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/77.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/78.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.95      0.93      0.94       158
               1       0.97      0.98      0.98       387

        accuracy                           0.97       545
       macro avg       0.96      0.96      0.96       545
    weighted avg       0.97      0.97      0.97       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.95      0.93      0.98      0.94      0.96      0.91       158
              1       0.97      0.98      0.93      0.98      0.96      0.92       387

    avg / total       0.97      0.97      0.95      0.97      0.96      0.92       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
