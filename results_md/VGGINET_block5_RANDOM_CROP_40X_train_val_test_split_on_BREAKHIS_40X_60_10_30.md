<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block5_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 0
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 7, 7, 512)    14714688    VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 7, 7, 64)     32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 7, 7, 128)    589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 7, 7, 32)     409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 7, 7, 512)    0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 7, 7, 736)    0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 7, 7, 736)    2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 36064)        0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 36064)        0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            72130       Dropout[0][0]                    
    ==================================================================================================
    Total params: 15,822,178
    Trainable params: 1,106,018
    Non-trainable params: 14,716,160
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00block5RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3322 - accuracy: 0.4062WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 15s 2s/step - loss: 1.5935 - accuracy: 0.6760 - val_loss: 3.2585 - val_accuracy: 0.7263
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3757 - accuracy: 0.8208 - val_loss: 1.7071 - val_accuracy: 0.8547
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0086 - accuracy: 0.8347 - val_loss: 1.4172 - val_accuracy: 0.8380
    Epoch 4/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7205 - accuracy: 0.8700 - val_loss: 1.2498 - val_accuracy: 0.8547
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6352 - accuracy: 0.8765 - val_loss: 1.2190 - val_accuracy: 0.8771
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5854 - accuracy: 0.8812 - val_loss: 1.1484 - val_accuracy: 0.8603
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4802 - accuracy: 0.8951 - val_loss: 1.0804 - val_accuracy: 0.8492
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5539 - accuracy: 0.8802 - val_loss: 1.1033 - val_accuracy: 0.8603
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4206 - accuracy: 0.8979 - val_loss: 0.8511 - val_accuracy: 0.8771
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4905 - accuracy: 0.8951 - val_loss: 1.0774 - val_accuracy: 0.8715
    Epoch 11/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4963 - accuracy: 0.8932 - val_loss: 1.1017 - val_accuracy: 0.8715
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5228 - accuracy: 0.9118 - val_loss: 0.9155 - val_accuracy: 0.8547
    Epoch 13/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4960 - accuracy: 0.9006 - val_loss: 1.1270 - val_accuracy: 0.8715
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5047 - accuracy: 0.9053 - val_loss: 0.7870 - val_accuracy: 0.8939
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4198 - accuracy: 0.9136 - val_loss: 1.3007 - val_accuracy: 0.8771
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4463 - accuracy: 0.9127 - val_loss: 1.1253 - val_accuracy: 0.8603
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4094 - accuracy: 0.9155 - val_loss: 0.9782 - val_accuracy: 0.8883
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4944 - accuracy: 0.9099 - val_loss: 0.8985 - val_accuracy: 0.8771
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3978 - accuracy: 0.9146 - val_loss: 0.8892 - val_accuracy: 0.8659
    Epoch 20/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3506 - accuracy: 0.9248 - val_loss: 0.8215 - val_accuracy: 0.8939
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4402 - accuracy: 0.9118 - val_loss: 0.9858 - val_accuracy: 0.8883
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3960 - accuracy: 0.9322 - val_loss: 0.9061 - val_accuracy: 0.8659
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4778 - accuracy: 0.9146 - val_loss: 1.0270 - val_accuracy: 0.8883
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4248 - accuracy: 0.9164 - val_loss: 0.8631 - val_accuracy: 0.9162
    Epoch 25/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4826 - accuracy: 0.9201 - val_loss: 1.1751 - val_accuracy: 0.8883
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4049 - accuracy: 0.9192 - val_loss: 0.9087 - val_accuracy: 0.8715
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3154 - accuracy: 0.9313 - val_loss: 1.1533 - val_accuracy: 0.8324
    Epoch 28/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3347 - accuracy: 0.9304 - val_loss: 0.9179 - val_accuracy: 0.8883
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3878 - accuracy: 0.9229 - val_loss: 0.8217 - val_accuracy: 0.8827
    Epoch 30/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3785 - accuracy: 0.9322 - val_loss: 0.9878 - val_accuracy: 0.8994
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3841 - accuracy: 0.9248 - val_loss: 0.8299 - val_accuracy: 0.9050
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3843 - accuracy: 0.9322 - val_loss: 0.5677 - val_accuracy: 0.9330
    Epoch 33/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2883 - accuracy: 0.9424 - val_loss: 0.6896 - val_accuracy: 0.9162
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3308 - accuracy: 0.9387 - val_loss: 0.7276 - val_accuracy: 0.9274
    Epoch 35/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3638 - accuracy: 0.9387 - val_loss: 1.1376 - val_accuracy: 0.8883
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4233 - accuracy: 0.9322 - val_loss: 0.8894 - val_accuracy: 0.9274
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3292 - accuracy: 0.9378 - val_loss: 0.9715 - val_accuracy: 0.9162
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3340 - accuracy: 0.9424 - val_loss: 1.1575 - val_accuracy: 0.8827
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3273 - accuracy: 0.9396 - val_loss: 0.9273 - val_accuracy: 0.8827
    Epoch 40/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3979 - accuracy: 0.9331 - val_loss: 0.6510 - val_accuracy: 0.9330
    Epoch 41/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4427 - accuracy: 0.9322 - val_loss: 0.7842 - val_accuracy: 0.9050
    Epoch 42/100
    9/9 [==============================] - 9s 1s/step - loss: 0.3546 - accuracy: 0.9359 - val_loss: 0.7959 - val_accuracy: 0.9050
    Epoch 43/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3751 - accuracy: 0.9396 - val_loss: 0.8766 - val_accuracy: 0.8883
    Epoch 44/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2221 - accuracy: 0.9610 - val_loss: 0.8481 - val_accuracy: 0.8827
    Epoch 45/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3130 - accuracy: 0.9415 - val_loss: 0.8337 - val_accuracy: 0.9050
    Epoch 46/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3430 - accuracy: 0.9489 - val_loss: 0.8524 - val_accuracy: 0.9106
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2503 - accuracy: 0.9554 - val_loss: 0.9466 - val_accuracy: 0.8939
    Epoch 48/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3015 - accuracy: 0.9601 - val_loss: 1.1147 - val_accuracy: 0.8827
    Epoch 49/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3009 - accuracy: 0.9406 - val_loss: 0.9301 - val_accuracy: 0.9050
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3142 - accuracy: 0.9424 - val_loss: 1.6160 - val_accuracy: 0.8827
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3333 - accuracy: 0.9536 - val_loss: 1.2980 - val_accuracy: 0.8771
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3060 - accuracy: 0.9461 - val_loss: 0.9744 - val_accuracy: 0.9050
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3080 - accuracy: 0.9517 - val_loss: 0.9667 - val_accuracy: 0.8771
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2606 - accuracy: 0.9508 - val_loss: 0.8827 - val_accuracy: 0.8883
    Epoch 55/100
    9/9 [==============================] - 9s 1s/step - loss: 0.4401 - accuracy: 0.9322 - val_loss: 0.9928 - val_accuracy: 0.8715
    Epoch 56/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2591 - accuracy: 0.9629 - val_loss: 0.7048 - val_accuracy: 0.9218
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2734 - accuracy: 0.9526 - val_loss: 0.6590 - val_accuracy: 0.9106
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3241 - accuracy: 0.9461 - val_loss: 0.9463 - val_accuracy: 0.8939
    Epoch 59/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2643 - accuracy: 0.9573 - val_loss: 1.4142 - val_accuracy: 0.8827
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3675 - accuracy: 0.9508 - val_loss: 0.7891 - val_accuracy: 0.9106
    Epoch 61/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3193 - accuracy: 0.9350 - val_loss: 0.9174 - val_accuracy: 0.9106
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3065 - accuracy: 0.9517 - val_loss: 0.7322 - val_accuracy: 0.8883
    Epoch 63/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3363 - accuracy: 0.9573 - val_loss: 0.8933 - val_accuracy: 0.9050
    Epoch 64/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2111 - accuracy: 0.9582 - val_loss: 1.1146 - val_accuracy: 0.8883
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2098 - accuracy: 0.9526 - val_loss: 1.2608 - val_accuracy: 0.8994
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2583 - accuracy: 0.9647 - val_loss: 1.1885 - val_accuracy: 0.8994
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2726 - accuracy: 0.9573 - val_loss: 1.0778 - val_accuracy: 0.9050
    Epoch 68/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3396 - accuracy: 0.9545 - val_loss: 1.2265 - val_accuracy: 0.8827
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2390 - accuracy: 0.9564 - val_loss: 1.1983 - val_accuracy: 0.8994
    Epoch 70/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2725 - accuracy: 0.9545 - val_loss: 1.2846 - val_accuracy: 0.8939
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3638 - accuracy: 0.9508 - val_loss: 1.3254 - val_accuracy: 0.9050
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3306 - accuracy: 0.9434 - val_loss: 1.4570 - val_accuracy: 0.8883
    Epoch 73/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3601 - accuracy: 0.9526 - val_loss: 1.1836 - val_accuracy: 0.8939
    Epoch 74/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2973 - accuracy: 0.9480 - val_loss: 0.9490 - val_accuracy: 0.9218
    Epoch 75/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2552 - accuracy: 0.9554 - val_loss: 0.8701 - val_accuracy: 0.9218
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3361 - accuracy: 0.9610 - val_loss: 1.0024 - val_accuracy: 0.8939
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2824 - accuracy: 0.9536 - val_loss: 0.9453 - val_accuracy: 0.9050
    Epoch 78/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2693 - accuracy: 0.9489 - val_loss: 1.0332 - val_accuracy: 0.9274
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2339 - accuracy: 0.9619 - val_loss: 0.8610 - val_accuracy: 0.9330
    Epoch 80/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1938 - accuracy: 0.9564 - val_loss: 0.8927 - val_accuracy: 0.9385
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2027 - accuracy: 0.9684 - val_loss: 1.0432 - val_accuracy: 0.9162
    Epoch 82/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2530 - accuracy: 0.9629 - val_loss: 0.7789 - val_accuracy: 0.9218
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2018 - accuracy: 0.9684 - val_loss: 0.8154 - val_accuracy: 0.9218
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2315 - accuracy: 0.9647 - val_loss: 1.0230 - val_accuracy: 0.9162
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2541 - accuracy: 0.9675 - val_loss: 1.4333 - val_accuracy: 0.9106
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1442 - accuracy: 0.9777 - val_loss: 1.7106 - val_accuracy: 0.9050
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2525 - accuracy: 0.9684 - val_loss: 1.8053 - val_accuracy: 0.8994
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2293 - accuracy: 0.9564 - val_loss: 1.1452 - val_accuracy: 0.8939
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2158 - accuracy: 0.9610 - val_loss: 0.9920 - val_accuracy: 0.9050
    Epoch 90/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2195 - accuracy: 0.9647 - val_loss: 0.6756 - val_accuracy: 0.8994
    Epoch 91/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2352 - accuracy: 0.9638 - val_loss: 0.5322 - val_accuracy: 0.9218
    Epoch 92/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2365 - accuracy: 0.9666 - val_loss: 0.7567 - val_accuracy: 0.9218
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1414 - accuracy: 0.9684 - val_loss: 0.9033 - val_accuracy: 0.9385
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2148 - accuracy: 0.9712 - val_loss: 0.9145 - val_accuracy: 0.9385
    Epoch 95/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1995 - accuracy: 0.9647 - val_loss: 0.9411 - val_accuracy: 0.9274
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2084 - accuracy: 0.9749 - val_loss: 0.9800 - val_accuracy: 0.9330
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2257 - accuracy: 0.9619 - val_loss: 1.1376 - val_accuracy: 0.9274
    Epoch 98/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3257 - accuracy: 0.9619 - val_loss: 0.9286 - val_accuracy: 0.9385
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2511 - accuracy: 0.9638 - val_loss: 0.9406 - val_accuracy: 0.9162
    Epoch 100/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2050 - accuracy: 0.9629 - val_loss: 0.7170 - val_accuracy: 0.9218
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/195.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.5428 - accuracy: 0.9536

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.5428140163421631, 0.9536178112030029]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9536178107606679
                  precision    recall  f1-score   support

               0       0.93      0.91      0.92       158
               1       0.96      0.97      0.97       381

        accuracy                           0.95       539
       macro avg       0.95      0.94      0.94       539
    weighted avg       0.95      0.95      0.95       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/196.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/197.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/198.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.91      0.92       158
               1       0.96      0.97      0.97       381

        accuracy                           0.95       539
       macro avg       0.95      0.94      0.94       539
    weighted avg       0.95      0.95      0.95       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.91      0.97      0.92      0.94      0.88       158
              1       0.96      0.97      0.91      0.97      0.94      0.89       381

    avg / total       0.95      0.95      0.93      0.95      0.94      0.89       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
