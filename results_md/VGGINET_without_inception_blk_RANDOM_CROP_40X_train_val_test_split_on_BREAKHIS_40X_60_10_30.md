<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# REMOVE INCEPTION BLOCK<a class="anchor-link" href="#REMOVE-INCEPTION-BLOCK">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
# def naive_inception_module(layer_in, f1, f2, f3):
#     # 1x1 conv
#     conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
#     # 3x3 conv
#     conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
#     # 5x5 conv
#     conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
#     # 3x3 max pooling
#     pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
#     # concatenate filters, assumes filters/channels last
#     layer_out = Concatenate()([conv1, conv3, conv5, pool])
#     return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = feature_ex_model.output
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    Image_Input (InputLayer)     [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    VGG_Preprocess (Lambda)      (None, 224, 224, 3)       0         
    _________________________________________________________________
    vgg16_features (Functional)  (None, 14, 14, 512)       7635264   
    _________________________________________________________________
    BN (BatchNormalization)      (None, 14, 14, 512)       2048      
    _________________________________________________________________
    flatten (Flatten)            (None, 100352)            0         
    _________________________________________________________________
    Dropout (Dropout)            (None, 100352)            0         
    _________________________________________________________________
    Predictions (Dense)          (None, 2)                 200706    
    =================================================================
    Total params: 7,838,018
    Trainable params: 201,730
    Non-trainable params: 7,636,288
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = '00no-inception-blkRANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.9092 - accuracy: 0.6094WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 14s 2s/step - loss: 2.0747 - accuracy: 0.7196 - val_loss: 4.6390 - val_accuracy: 0.8156
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 1.2672 - accuracy: 0.8032 - val_loss: 2.8920 - val_accuracy: 0.8547
    Epoch 3/100
    9/9 [==============================] - 9s 1s/step - loss: 1.1461 - accuracy: 0.8440 - val_loss: 2.9154 - val_accuracy: 0.8324
    Epoch 4/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8809 - accuracy: 0.8533 - val_loss: 2.2279 - val_accuracy: 0.8492
    Epoch 5/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8786 - accuracy: 0.8784 - val_loss: 1.5307 - val_accuracy: 0.8771
    Epoch 6/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6666 - accuracy: 0.8858 - val_loss: 1.9639 - val_accuracy: 0.8547
    Epoch 7/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7975 - accuracy: 0.8756 - val_loss: 1.6865 - val_accuracy: 0.8547
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6987 - accuracy: 0.8979 - val_loss: 1.2447 - val_accuracy: 0.8659
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6458 - accuracy: 0.8969 - val_loss: 1.0559 - val_accuracy: 0.8827
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7516 - accuracy: 0.8895 - val_loss: 0.9767 - val_accuracy: 0.8827
    Epoch 11/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8213 - accuracy: 0.8942 - val_loss: 0.9661 - val_accuracy: 0.9050
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7766 - accuracy: 0.8858 - val_loss: 1.3526 - val_accuracy: 0.8827
    Epoch 13/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6925 - accuracy: 0.8932 - val_loss: 1.0478 - val_accuracy: 0.9050
    Epoch 14/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5856 - accuracy: 0.9099 - val_loss: 0.9399 - val_accuracy: 0.9106
    Epoch 15/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7425 - accuracy: 0.8914 - val_loss: 0.9462 - val_accuracy: 0.9162
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6679 - accuracy: 0.9044 - val_loss: 1.0625 - val_accuracy: 0.9106
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8041 - accuracy: 0.8960 - val_loss: 1.4150 - val_accuracy: 0.8883
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6240 - accuracy: 0.9071 - val_loss: 1.1573 - val_accuracy: 0.8939
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6794 - accuracy: 0.9220 - val_loss: 1.2712 - val_accuracy: 0.8883
    Epoch 20/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6390 - accuracy: 0.9183 - val_loss: 0.9701 - val_accuracy: 0.8939
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6952 - accuracy: 0.9127 - val_loss: 0.7936 - val_accuracy: 0.8994
    Epoch 22/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7592 - accuracy: 0.9071 - val_loss: 0.8973 - val_accuracy: 0.8994
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7789 - accuracy: 0.9136 - val_loss: 0.8226 - val_accuracy: 0.9218
    Epoch 24/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8462 - accuracy: 0.9016 - val_loss: 1.0856 - val_accuracy: 0.8883
    Epoch 25/100
    9/9 [==============================] - 9s 1s/step - loss: 0.9245 - accuracy: 0.8904 - val_loss: 0.7941 - val_accuracy: 0.9385
    Epoch 26/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6726 - accuracy: 0.9183 - val_loss: 0.8046 - val_accuracy: 0.9330
    Epoch 27/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7629 - accuracy: 0.9248 - val_loss: 0.8452 - val_accuracy: 0.9330
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7219 - accuracy: 0.9118 - val_loss: 1.0488 - val_accuracy: 0.9162
    Epoch 29/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7466 - accuracy: 0.9174 - val_loss: 0.7823 - val_accuracy: 0.9330
    Epoch 30/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6255 - accuracy: 0.9239 - val_loss: 0.7926 - val_accuracy: 0.9274
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5733 - accuracy: 0.9248 - val_loss: 0.8084 - val_accuracy: 0.9330
    Epoch 32/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8594 - accuracy: 0.9081 - val_loss: 0.7265 - val_accuracy: 0.9330
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7319 - accuracy: 0.9109 - val_loss: 0.7234 - val_accuracy: 0.9274
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7325 - accuracy: 0.9174 - val_loss: 0.8794 - val_accuracy: 0.9330
    Epoch 35/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9328 - accuracy: 0.9127 - val_loss: 0.8823 - val_accuracy: 0.9385
    Epoch 36/100
    9/9 [==============================] - 9s 1s/step - loss: 0.8305 - accuracy: 0.9155 - val_loss: 0.7533 - val_accuracy: 0.9274
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9992 - accuracy: 0.8969 - val_loss: 0.9807 - val_accuracy: 0.9330
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8329 - accuracy: 0.9118 - val_loss: 0.9347 - val_accuracy: 0.9330
    Epoch 39/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8490 - accuracy: 0.9127 - val_loss: 0.7329 - val_accuracy: 0.9441
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7999 - accuracy: 0.8923 - val_loss: 0.8211 - val_accuracy: 0.9274
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9007 - accuracy: 0.9266 - val_loss: 0.6370 - val_accuracy: 0.9274
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8071 - accuracy: 0.9192 - val_loss: 0.8867 - val_accuracy: 0.9162
    Epoch 43/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6762 - accuracy: 0.9350 - val_loss: 0.5894 - val_accuracy: 0.9441
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7500 - accuracy: 0.9174 - val_loss: 1.1842 - val_accuracy: 0.9106
    Epoch 45/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6629 - accuracy: 0.9304 - val_loss: 0.6266 - val_accuracy: 0.9330
    Epoch 46/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6480 - accuracy: 0.9313 - val_loss: 0.6876 - val_accuracy: 0.9441
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6007 - accuracy: 0.9341 - val_loss: 0.8196 - val_accuracy: 0.9385
    Epoch 48/100
    9/9 [==============================] - 9s 1s/step - loss: 0.7384 - accuracy: 0.9313 - val_loss: 0.8298 - val_accuracy: 0.9385
    Epoch 49/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6788 - accuracy: 0.9369 - val_loss: 0.7371 - val_accuracy: 0.9330
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6902 - accuracy: 0.9220 - val_loss: 0.7736 - val_accuracy: 0.9441
    Epoch 51/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6759 - accuracy: 0.9248 - val_loss: 0.7389 - val_accuracy: 0.9385
    Epoch 52/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5792 - accuracy: 0.9387 - val_loss: 0.8200 - val_accuracy: 0.9441
    Epoch 53/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6965 - accuracy: 0.9322 - val_loss: 0.7031 - val_accuracy: 0.9385
    Epoch 54/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6076 - accuracy: 0.9276 - val_loss: 0.9753 - val_accuracy: 0.9441
    Epoch 55/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7440 - accuracy: 0.9294 - val_loss: 0.8300 - val_accuracy: 0.9497
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7006 - accuracy: 0.9341 - val_loss: 0.6758 - val_accuracy: 0.9385
    Epoch 57/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7609 - accuracy: 0.9201 - val_loss: 0.8084 - val_accuracy: 0.9330
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6089 - accuracy: 0.9396 - val_loss: 0.6987 - val_accuracy: 0.9497
    Epoch 59/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7332 - accuracy: 0.9285 - val_loss: 1.4574 - val_accuracy: 0.9218
    Epoch 60/100
    9/9 [==============================] - 12s 1s/step - loss: 0.8651 - accuracy: 0.9155 - val_loss: 0.9547 - val_accuracy: 0.9385
    Epoch 61/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5491 - accuracy: 0.9434 - val_loss: 1.0092 - val_accuracy: 0.9330
    Epoch 62/100
    9/9 [==============================] - 12s 1s/step - loss: 0.7416 - accuracy: 0.9294 - val_loss: 0.8419 - val_accuracy: 0.9441
    Epoch 63/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6724 - accuracy: 0.9396 - val_loss: 0.8072 - val_accuracy: 0.9385
    Epoch 64/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5116 - accuracy: 0.9341 - val_loss: 0.7483 - val_accuracy: 0.9441
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6073 - accuracy: 0.9424 - val_loss: 1.0388 - val_accuracy: 0.9330
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8141 - accuracy: 0.9276 - val_loss: 1.0552 - val_accuracy: 0.9330
    Epoch 67/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8806 - accuracy: 0.9248 - val_loss: 0.9976 - val_accuracy: 0.9385
    Epoch 68/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6177 - accuracy: 0.9341 - val_loss: 1.0576 - val_accuracy: 0.9330
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8961 - accuracy: 0.9266 - val_loss: 0.8635 - val_accuracy: 0.9274
    Epoch 70/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7052 - accuracy: 0.9406 - val_loss: 0.6788 - val_accuracy: 0.9497
    Epoch 71/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7394 - accuracy: 0.9350 - val_loss: 0.7721 - val_accuracy: 0.9441
    Epoch 72/100
    9/9 [==============================] - 9s 1s/step - loss: 0.9046 - accuracy: 0.9313 - val_loss: 0.8959 - val_accuracy: 0.9441
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8784 - accuracy: 0.9239 - val_loss: 1.1045 - val_accuracy: 0.9385
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8874 - accuracy: 0.9220 - val_loss: 0.8509 - val_accuracy: 0.9609
    Epoch 75/100
    9/9 [==============================] - 9s 1s/step - loss: 0.5046 - accuracy: 0.9406 - val_loss: 0.8524 - val_accuracy: 0.9553
    Epoch 76/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6512 - accuracy: 0.9424 - val_loss: 0.8375 - val_accuracy: 0.9553
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6708 - accuracy: 0.9424 - val_loss: 0.8874 - val_accuracy: 0.9441
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7658 - accuracy: 0.9266 - val_loss: 1.1118 - val_accuracy: 0.9274
    Epoch 79/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7462 - accuracy: 0.9424 - val_loss: 1.0051 - val_accuracy: 0.9218
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6257 - accuracy: 0.9424 - val_loss: 0.9064 - val_accuracy: 0.9274
    Epoch 81/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5909 - accuracy: 0.9387 - val_loss: 1.0708 - val_accuracy: 0.9218
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7587 - accuracy: 0.9369 - val_loss: 0.8311 - val_accuracy: 0.9441
    Epoch 83/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6321 - accuracy: 0.9396 - val_loss: 0.7652 - val_accuracy: 0.9441
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7555 - accuracy: 0.9229 - val_loss: 0.9367 - val_accuracy: 0.9385
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.7857 - accuracy: 0.9276 - val_loss: 0.7800 - val_accuracy: 0.9385
    Epoch 86/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6061 - accuracy: 0.9406 - val_loss: 0.9272 - val_accuracy: 0.9385
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9093 - accuracy: 0.9266 - val_loss: 0.6312 - val_accuracy: 0.9553
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6142 - accuracy: 0.9369 - val_loss: 1.0209 - val_accuracy: 0.9385
    Epoch 89/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6211 - accuracy: 0.9378 - val_loss: 0.8345 - val_accuracy: 0.9441
    Epoch 90/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6363 - accuracy: 0.9434 - val_loss: 1.0366 - val_accuracy: 0.9441
    Epoch 91/100
    9/9 [==============================] - 12s 1s/step - loss: 0.6410 - accuracy: 0.9406 - val_loss: 0.9027 - val_accuracy: 0.9497
    Epoch 92/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6825 - accuracy: 0.9415 - val_loss: 1.0657 - val_accuracy: 0.9441
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6176 - accuracy: 0.9471 - val_loss: 0.9939 - val_accuracy: 0.9497
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6334 - accuracy: 0.9434 - val_loss: 0.9800 - val_accuracy: 0.9441
    Epoch 95/100
    9/9 [==============================] - 9s 1s/step - loss: 0.6946 - accuracy: 0.9434 - val_loss: 1.1508 - val_accuracy: 0.9274
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6240 - accuracy: 0.9415 - val_loss: 1.0837 - val_accuracy: 0.9330
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6383 - accuracy: 0.9387 - val_loss: 0.9113 - val_accuracy: 0.9441
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6358 - accuracy: 0.9331 - val_loss: 1.0262 - val_accuracy: 0.9385
    Epoch 99/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6437 - accuracy: 0.9424 - val_loss: 0.9626 - val_accuracy: 0.9441
    Epoch 100/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8073 - accuracy: 0.9322 - val_loss: 0.8981 - val_accuracy: 0.9441
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/71.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 6s 1s/step - loss: 0.9770 - accuracy: 0.9388

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.9770362973213196, 0.9387755393981934]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9387755102040817
                  precision    recall  f1-score   support

               0       0.93      0.86      0.89       158
               1       0.94      0.97      0.96       381

        accuracy                           0.94       539
       macro avg       0.93      0.92      0.92       539
    weighted avg       0.94      0.94      0.94       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/72.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/73.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[24\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/74.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.86      0.89       158
               1       0.94      0.97      0.96       381

        accuracy                           0.94       539
       macro avg       0.93      0.92      0.92       539
    weighted avg       0.94      0.94      0.94       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.86      0.97      0.89      0.91      0.83       158
              1       0.94      0.97      0.86      0.96      0.91      0.85       381

    avg / total       0.94      0.94      0.89      0.94      0.91      0.84       539

</div>
</div>
</div>
</div>
</div>
</div>
</div>
