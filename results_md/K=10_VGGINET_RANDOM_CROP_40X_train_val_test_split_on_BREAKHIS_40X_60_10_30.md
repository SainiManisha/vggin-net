<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 640, 1280, 320)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2


bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 640)  328320      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 1280) 5899520     vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 320)  4096320     vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 2752) 0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 2752) 11008       concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 539392)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 539392)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            1078786     Dropout[0][0]                    
    ==================================================================================================
    Total params: 19,049,218
    Trainable params: 11,408,450
    Non-trainable params: 7,640,768
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'k10final00RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
path = './experiments/%s' % name
!mkdir {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
!cp -r *.hdf5 {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1562 - accuracy: 0.5312WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 20s 2s/step - loss: 16.6092 - accuracy: 0.7159 - val_loss: 35.8186 - val_accuracy: 0.7821
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 7.2433 - accuracy: 0.8384 - val_loss: 42.4113 - val_accuracy: 0.7654
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 3.6530 - accuracy: 0.8932 - val_loss: 21.7119 - val_accuracy: 0.8436
    Epoch 4/100
    9/9 [==============================] - 11s 1s/step - loss: 3.1827 - accuracy: 0.9127 - val_loss: 8.2489 - val_accuracy: 0.8994
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 2.0888 - accuracy: 0.9183 - val_loss: 4.3596 - val_accuracy: 0.9274
    Epoch 6/100
    9/9 [==============================] - 13s 1s/step - loss: 2.4013 - accuracy: 0.9118 - val_loss: 2.6789 - val_accuracy: 0.9553
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 1.5203 - accuracy: 0.9359 - val_loss: 4.8355 - val_accuracy: 0.8994
    Epoch 8/100
    9/9 [==============================] - 15s 2s/step - loss: 2.1903 - accuracy: 0.9378 - val_loss: 3.7844 - val_accuracy: 0.9441
    Epoch 9/100
    9/9 [==============================] - 14s 2s/step - loss: 2.5058 - accuracy: 0.9304 - val_loss: 9.1512 - val_accuracy: 0.8994
    Epoch 10/100
    9/9 [==============================] - 15s 2s/step - loss: 1.9677 - accuracy: 0.9443 - val_loss: 6.4789 - val_accuracy: 0.9218
    Epoch 11/100
    9/9 [==============================] - 14s 2s/step - loss: 1.4972 - accuracy: 0.9517 - val_loss: 8.1304 - val_accuracy: 0.9162
    Epoch 12/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2241 - accuracy: 0.9647 - val_loss: 1.9924 - val_accuracy: 0.9330
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 2.0480 - accuracy: 0.9480 - val_loss: 2.7779 - val_accuracy: 0.9609
    Epoch 14/100
    9/9 [==============================] - 14s 2s/step - loss: 2.7261 - accuracy: 0.9378 - val_loss: 2.0017 - val_accuracy: 0.9441
    Epoch 15/100
    9/9 [==============================] - 15s 2s/step - loss: 1.7233 - accuracy: 0.9480 - val_loss: 2.2705 - val_accuracy: 0.9497
    Epoch 16/100
    9/9 [==============================] - 14s 2s/step - loss: 1.5624 - accuracy: 0.9536 - val_loss: 4.5746 - val_accuracy: 0.9274
    Epoch 17/100
    9/9 [==============================] - 14s 2s/step - loss: 2.0483 - accuracy: 0.9517 - val_loss: 2.3015 - val_accuracy: 0.9665
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 2.0342 - accuracy: 0.9517 - val_loss: 8.3640 - val_accuracy: 0.8883
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 1.6077 - accuracy: 0.9610 - val_loss: 1.4686 - val_accuracy: 0.9609
    Epoch 20/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2419 - accuracy: 0.9656 - val_loss: 0.9872 - val_accuracy: 0.9777
    Epoch 21/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5562 - accuracy: 0.9786 - val_loss: 5.3854 - val_accuracy: 0.9330
    Epoch 22/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3888 - accuracy: 0.9666 - val_loss: 1.3739 - val_accuracy: 0.9441
    Epoch 23/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1670 - accuracy: 0.9684 - val_loss: 1.9480 - val_accuracy: 0.9553
    Epoch 24/100
    9/9 [==============================] - 11s 1s/step - loss: 1.2733 - accuracy: 0.9656 - val_loss: 4.5791 - val_accuracy: 0.9385
    Epoch 25/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8381 - accuracy: 0.9768 - val_loss: 4.2621 - val_accuracy: 0.9218
    Epoch 26/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2837 - accuracy: 0.9638 - val_loss: 5.8891 - val_accuracy: 0.9385
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9774 - accuracy: 0.9656 - val_loss: 11.4051 - val_accuracy: 0.9050
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7498 - accuracy: 0.9796 - val_loss: 3.5809 - val_accuracy: 0.9553
    Epoch 29/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6788 - accuracy: 0.9786 - val_loss: 3.1394 - val_accuracy: 0.9609
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8249 - accuracy: 0.9777 - val_loss: 3.2677 - val_accuracy: 0.9497
    Epoch 31/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9503 - accuracy: 0.9721 - val_loss: 2.2212 - val_accuracy: 0.9609
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7487 - accuracy: 0.9786 - val_loss: 1.7821 - val_accuracy: 0.9665
    Epoch 33/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5729 - accuracy: 0.9824 - val_loss: 2.3514 - val_accuracy: 0.9497
    Epoch 34/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0137 - accuracy: 0.9786 - val_loss: 4.5074 - val_accuracy: 0.9330
    Epoch 35/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8788 - accuracy: 0.9759 - val_loss: 4.4542 - val_accuracy: 0.9441
    Epoch 36/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5172 - accuracy: 0.9824 - val_loss: 1.1271 - val_accuracy: 0.9553
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8202 - accuracy: 0.9842 - val_loss: 4.4213 - val_accuracy: 0.9441
    Epoch 38/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6989 - accuracy: 0.9814 - val_loss: 3.6535 - val_accuracy: 0.9385
    Epoch 39/100
    9/9 [==============================] - 14s 2s/step - loss: 1.7428 - accuracy: 0.9619 - val_loss: 2.2882 - val_accuracy: 0.9553
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 1.8031 - accuracy: 0.9573 - val_loss: 3.6562 - val_accuracy: 0.9497
    Epoch 41/100
    9/9 [==============================] - 15s 2s/step - loss: 0.7072 - accuracy: 0.9786 - val_loss: 3.1841 - val_accuracy: 0.9441
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3750 - accuracy: 0.9777 - val_loss: 4.7069 - val_accuracy: 0.9497
    Epoch 43/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2086 - accuracy: 0.9777 - val_loss: 2.1435 - val_accuracy: 0.9721
    Epoch 44/100
    9/9 [==============================] - 14s 2s/step - loss: 0.9659 - accuracy: 0.9777 - val_loss: 5.5647 - val_accuracy: 0.9385
    Epoch 45/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5804 - accuracy: 0.9851 - val_loss: 8.1081 - val_accuracy: 0.9106
    Epoch 46/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2821 - accuracy: 0.9889 - val_loss: 5.0101 - val_accuracy: 0.9553
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5230 - accuracy: 0.9861 - val_loss: 3.1393 - val_accuracy: 0.9665
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2678 - accuracy: 0.9861 - val_loss: 2.2724 - val_accuracy: 0.9777
    Epoch 49/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3804 - accuracy: 0.9851 - val_loss: 2.7183 - val_accuracy: 0.9665
    Epoch 50/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4827 - accuracy: 0.9824 - val_loss: 4.8703 - val_accuracy: 0.9553
    Epoch 51/100
    9/9 [==============================] - 14s 2s/step - loss: 1.1187 - accuracy: 0.9814 - val_loss: 3.7588 - val_accuracy: 0.9553
    Epoch 52/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0793 - accuracy: 0.9721 - val_loss: 4.6315 - val_accuracy: 0.9441
    Epoch 53/100
    9/9 [==============================] - 17s 2s/step - loss: 1.2742 - accuracy: 0.9703 - val_loss: 4.5130 - val_accuracy: 0.9441
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 1.0861 - accuracy: 0.9777 - val_loss: 4.3110 - val_accuracy: 0.9385
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 1.3413 - accuracy: 0.9740 - val_loss: 1.3265 - val_accuracy: 0.9665
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1931 - accuracy: 0.9777 - val_loss: 3.6168 - val_accuracy: 0.9441
    Epoch 57/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6279 - accuracy: 0.9879 - val_loss: 9.7991 - val_accuracy: 0.9385
    Epoch 58/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7707 - accuracy: 0.9842 - val_loss: 13.0693 - val_accuracy: 0.9162
    Epoch 59/100
    9/9 [==============================] - 11s 1s/step - loss: 1.0318 - accuracy: 0.9796 - val_loss: 3.5622 - val_accuracy: 0.9721
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0993 - accuracy: 0.9796 - val_loss: 3.5743 - val_accuracy: 0.9441
    Epoch 61/100
    9/9 [==============================] - 11s 1s/step - loss: 1.1001 - accuracy: 0.9703 - val_loss: 6.1454 - val_accuracy: 0.9609
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 1.4922 - accuracy: 0.9712 - val_loss: 1.7223 - val_accuracy: 0.9832
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.9665 - accuracy: 0.9796 - val_loss: 3.4429 - val_accuracy: 0.9721
    Epoch 64/100
    9/9 [==============================] - 17s 2s/step - loss: 0.3604 - accuracy: 0.9916 - val_loss: 10.1798 - val_accuracy: 0.9274
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7306 - accuracy: 0.9851 - val_loss: 6.5965 - val_accuracy: 0.9441
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1740 - accuracy: 0.9898 - val_loss: 8.2742 - val_accuracy: 0.9330
    Epoch 67/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3415 - accuracy: 0.9907 - val_loss: 5.9814 - val_accuracy: 0.9609
    Epoch 68/100
    9/9 [==============================] - 11s 1s/step - loss: 0.8622 - accuracy: 0.9814 - val_loss: 5.9438 - val_accuracy: 0.9497
    Epoch 69/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3353 - accuracy: 0.9870 - val_loss: 6.1734 - val_accuracy: 0.9497
    Epoch 70/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4114 - accuracy: 0.9907 - val_loss: 5.1396 - val_accuracy: 0.9665
    Epoch 71/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6698 - accuracy: 0.9889 - val_loss: 5.9901 - val_accuracy: 0.9497
    Epoch 72/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7690 - accuracy: 0.9861 - val_loss: 3.5147 - val_accuracy: 0.9609
    Epoch 73/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4425 - accuracy: 0.9879 - val_loss: 2.7641 - val_accuracy: 0.9553
    Epoch 74/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8685 - accuracy: 0.9842 - val_loss: 3.2306 - val_accuracy: 0.9497
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4371 - accuracy: 0.9926 - val_loss: 4.0212 - val_accuracy: 0.9609
    Epoch 76/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3277 - accuracy: 0.9889 - val_loss: 3.5440 - val_accuracy: 0.9665
    Epoch 77/100
    9/9 [==============================] - 11s 1s/step - loss: 0.6776 - accuracy: 0.9851 - val_loss: 7.2464 - val_accuracy: 0.9553
    Epoch 78/100
    9/9 [==============================] - 11s 1s/step - loss: 0.2538 - accuracy: 0.9944 - val_loss: 8.2681 - val_accuracy: 0.9385
    Epoch 79/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1895 - accuracy: 0.9954 - val_loss: 3.9488 - val_accuracy: 0.9665
    Epoch 80/100
    9/9 [==============================] - 11s 1s/step - loss: 0.4756 - accuracy: 0.9898 - val_loss: 3.1851 - val_accuracy: 0.9609
    Epoch 81/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4852 - accuracy: 0.9879 - val_loss: 4.2275 - val_accuracy: 0.9553
    Epoch 82/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3870 - accuracy: 0.9926 - val_loss: 3.5773 - val_accuracy: 0.9721
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6239 - accuracy: 0.9907 - val_loss: 2.2524 - val_accuracy: 0.9721
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.5648 - accuracy: 0.9898 - val_loss: 1.4327 - val_accuracy: 0.9721
    Epoch 85/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6382 - accuracy: 0.9861 - val_loss: 3.9628 - val_accuracy: 0.9497
    Epoch 86/100
    9/9 [==============================] - 11s 1s/step - loss: 0.5477 - accuracy: 0.9879 - val_loss: 4.0024 - val_accuracy: 0.9553
    Epoch 87/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1901 - accuracy: 0.9889 - val_loss: 5.7437 - val_accuracy: 0.9609
    Epoch 88/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6160 - accuracy: 0.9889 - val_loss: 3.9027 - val_accuracy: 0.9609
    Epoch 89/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4723 - accuracy: 0.9870 - val_loss: 3.2829 - val_accuracy: 0.9497
    Epoch 90/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4596 - accuracy: 0.9907 - val_loss: 5.2868 - val_accuracy: 0.9441
    Epoch 91/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4768 - accuracy: 0.9851 - val_loss: 9.9313 - val_accuracy: 0.9441
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4404 - accuracy: 0.9926 - val_loss: 20.6865 - val_accuracy: 0.8771
    Epoch 93/100
    9/9 [==============================] - 11s 1s/step - loss: 0.7496 - accuracy: 0.9889 - val_loss: 8.1447 - val_accuracy: 0.9553
    Epoch 94/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5312 - accuracy: 0.9879 - val_loss: 7.6378 - val_accuracy: 0.9553
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4747 - accuracy: 0.9898 - val_loss: 4.1964 - val_accuracy: 0.9497
    Epoch 96/100
    9/9 [==============================] - 14s 2s/step - loss: 0.8299 - accuracy: 0.9851 - val_loss: 6.9456 - val_accuracy: 0.9665
    Epoch 97/100
    9/9 [==============================] - 14s 2s/step - loss: 0.0839 - accuracy: 0.9935 - val_loss: 9.3445 - val_accuracy: 0.9553
    Epoch 98/100
    9/9 [==============================] - 11s 1s/step - loss: 0.3817 - accuracy: 0.9898 - val_loss: 5.2537 - val_accuracy: 0.9777
    Epoch 99/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3953 - accuracy: 0.9898 - val_loss: 6.1017 - val_accuracy: 0.9665
    Epoch 100/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6651 - accuracy: 0.9889 - val_loss: 4.1874 - val_accuracy: 0.9665
    cp: cannot stat '*.hdf5': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/79.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 14s 3s/step - loss: 3.7108 - accuracy: 0.9685

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [3.710801839828491, 0.9684600830078125]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9684601113172542
                  precision    recall  f1-score   support

               0       0.99      0.91      0.94       158
               1       0.96      0.99      0.98       381

        accuracy                           0.97       539
       macro avg       0.97      0.95      0.96       539
    weighted avg       0.97      0.97      0.97       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[22\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/80.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/81.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[23\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/82.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.99      0.91      0.94       158
               1       0.96      0.99      0.98       381

        accuracy                           0.97       539
       macro avg       0.97      0.95      0.96       539
    weighted avg       0.97      0.97      0.97       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.99      0.91      0.99      0.94      0.95      0.89       158
              1       0.96      0.99      0.91      0.98      0.95      0.91       381

    avg / total       0.97      0.97      0.93      0.97      0.95      0.90       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
