<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_200X/train'
val_path = './Splitted_200X/val'
test_path = './Splitted_200X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1087 images belonging to 2 classes.
    Found 181 images belonging to 2 classes.
    Found 545 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.under_sampling import RandomUnderSampler
from tensorflow.keras.utils import to_categorical
ros = RandomUnderSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((626, 224, 340, 3), (626, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(1000).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_200X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/5 [=====&gt;........................] - ETA: 0s - loss: 1.3685 - accuracy: 0.5625WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    5/5 [==============================] - 9s 2s/step - loss: 3.3082 - accuracy: 0.7093 - val_loss: 10.8478 - val_accuracy: 0.8453
    Epoch 2/100
    5/5 [==============================] - 4s 776ms/step - loss: 3.6157 - accuracy: 0.8083 - val_loss: 6.5643 - val_accuracy: 0.8950
    Epoch 3/100
    5/5 [==============================] - 4s 705ms/step - loss: 2.1547 - accuracy: 0.8530 - val_loss: 11.9569 - val_accuracy: 0.7956
    Epoch 4/100
    5/5 [==============================] - 4s 713ms/step - loss: 1.4985 - accuracy: 0.8738 - val_loss: 2.7076 - val_accuracy: 0.9116
    Epoch 5/100
    5/5 [==============================] - 4s 712ms/step - loss: 1.5979 - accuracy: 0.8562 - val_loss: 8.1548 - val_accuracy: 0.8287
    Epoch 6/100
    5/5 [==============================] - 4s 733ms/step - loss: 1.2326 - accuracy: 0.8722 - val_loss: 3.3019 - val_accuracy: 0.8674
    Epoch 7/100
    5/5 [==============================] - 3s 696ms/step - loss: 1.2159 - accuracy: 0.8770 - val_loss: 3.6954 - val_accuracy: 0.8729
    Epoch 8/100
    5/5 [==============================] - 3s 687ms/step - loss: 1.0002 - accuracy: 0.8978 - val_loss: 2.2955 - val_accuracy: 0.9061
    Epoch 9/100
    5/5 [==============================] - 3s 694ms/step - loss: 1.1314 - accuracy: 0.8994 - val_loss: 0.9422 - val_accuracy: 0.9282
    Epoch 10/100
    5/5 [==============================] - 3s 683ms/step - loss: 1.3166 - accuracy: 0.8850 - val_loss: 1.0210 - val_accuracy: 0.9448
    Epoch 11/100
    5/5 [==============================] - 4s 765ms/step - loss: 0.8357 - accuracy: 0.9233 - val_loss: 3.5697 - val_accuracy: 0.8619
    Epoch 12/100
    5/5 [==============================] - 4s 739ms/step - loss: 0.9679 - accuracy: 0.9121 - val_loss: 2.0388 - val_accuracy: 0.8840
    Epoch 13/100
    5/5 [==============================] - 4s 715ms/step - loss: 0.7648 - accuracy: 0.9185 - val_loss: 1.1486 - val_accuracy: 0.9337
    Epoch 14/100
    5/5 [==============================] - 4s 745ms/step - loss: 0.5346 - accuracy: 0.9393 - val_loss: 1.7952 - val_accuracy: 0.9171
    Epoch 15/100
    5/5 [==============================] - 3s 679ms/step - loss: 0.7670 - accuracy: 0.9169 - val_loss: 0.9938 - val_accuracy: 0.9448
    Epoch 16/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.7219 - accuracy: 0.9297 - val_loss: 1.0052 - val_accuracy: 0.9282
    Epoch 17/100
    5/5 [==============================] - 4s 701ms/step - loss: 0.7685 - accuracy: 0.9265 - val_loss: 1.7822 - val_accuracy: 0.8895
    Epoch 18/100
    5/5 [==============================] - 3s 678ms/step - loss: 0.5768 - accuracy: 0.9201 - val_loss: 1.8106 - val_accuracy: 0.8895
    Epoch 19/100
    5/5 [==============================] - 3s 679ms/step - loss: 0.7775 - accuracy: 0.9137 - val_loss: 0.8135 - val_accuracy: 0.9448
    Epoch 20/100
    5/5 [==============================] - 3s 689ms/step - loss: 0.6600 - accuracy: 0.9233 - val_loss: 0.8471 - val_accuracy: 0.9448
    Epoch 21/100
    5/5 [==============================] - 3s 671ms/step - loss: 0.6033 - accuracy: 0.9249 - val_loss: 0.6602 - val_accuracy: 0.9448
    Epoch 22/100
    5/5 [==============================] - 3s 664ms/step - loss: 0.6506 - accuracy: 0.9377 - val_loss: 0.9334 - val_accuracy: 0.9337
    Epoch 23/100
    5/5 [==============================] - 4s 754ms/step - loss: 0.5925 - accuracy: 0.9313 - val_loss: 0.7079 - val_accuracy: 0.9558
    Epoch 24/100
    5/5 [==============================] - 3s 696ms/step - loss: 0.6608 - accuracy: 0.9345 - val_loss: 1.1479 - val_accuracy: 0.9337
    Epoch 25/100
    5/5 [==============================] - 3s 681ms/step - loss: 0.5280 - accuracy: 0.9489 - val_loss: 0.8158 - val_accuracy: 0.9337
    Epoch 26/100
    5/5 [==============================] - 4s 708ms/step - loss: 0.6247 - accuracy: 0.9345 - val_loss: 0.8493 - val_accuracy: 0.9227
    Epoch 27/100
    5/5 [==============================] - 3s 692ms/step - loss: 0.5719 - accuracy: 0.9329 - val_loss: 0.7185 - val_accuracy: 0.9558
    Epoch 28/100
    5/5 [==============================] - 4s 759ms/step - loss: 0.5276 - accuracy: 0.9553 - val_loss: 0.6933 - val_accuracy: 0.9558
    Epoch 29/100
    5/5 [==============================] - 4s 764ms/step - loss: 0.7094 - accuracy: 0.9473 - val_loss: 0.9204 - val_accuracy: 0.9337
    Epoch 30/100
    5/5 [==============================] - 3s 691ms/step - loss: 0.7116 - accuracy: 0.9489 - val_loss: 0.7467 - val_accuracy: 0.9613
    Epoch 31/100
    5/5 [==============================] - 4s 760ms/step - loss: 0.7636 - accuracy: 0.9297 - val_loss: 0.8777 - val_accuracy: 0.9448
    Epoch 32/100
    5/5 [==============================] - 3s 684ms/step - loss: 0.5979 - accuracy: 0.9393 - val_loss: 0.6909 - val_accuracy: 0.9503
    Epoch 33/100
    5/5 [==============================] - 4s 747ms/step - loss: 0.9342 - accuracy: 0.9297 - val_loss: 0.8842 - val_accuracy: 0.9337
    Epoch 34/100
    5/5 [==============================] - 3s 680ms/step - loss: 0.9557 - accuracy: 0.9169 - val_loss: 0.5964 - val_accuracy: 0.9503
    Epoch 35/100
    5/5 [==============================] - 4s 759ms/step - loss: 0.5328 - accuracy: 0.9537 - val_loss: 1.4943 - val_accuracy: 0.9282
    Epoch 36/100
    5/5 [==============================] - 4s 749ms/step - loss: 1.0453 - accuracy: 0.9345 - val_loss: 0.7077 - val_accuracy: 0.9503
    Epoch 37/100
    5/5 [==============================] - 4s 775ms/step - loss: 0.7741 - accuracy: 0.9505 - val_loss: 0.5119 - val_accuracy: 0.9503
    Epoch 38/100
    5/5 [==============================] - 3s 682ms/step - loss: 0.2782 - accuracy: 0.9681 - val_loss: 0.4703 - val_accuracy: 0.9613
    Epoch 39/100
    5/5 [==============================] - 4s 753ms/step - loss: 0.5240 - accuracy: 0.9569 - val_loss: 0.9484 - val_accuracy: 0.9282
    Epoch 40/100
    5/5 [==============================] - 4s 756ms/step - loss: 0.3191 - accuracy: 0.9744 - val_loss: 0.6727 - val_accuracy: 0.9558
    Epoch 41/100
    5/5 [==============================] - 4s 752ms/step - loss: 0.3901 - accuracy: 0.9633 - val_loss: 0.5598 - val_accuracy: 0.9613
    Epoch 42/100
    5/5 [==============================] - 3s 672ms/step - loss: 0.4830 - accuracy: 0.9569 - val_loss: 1.0037 - val_accuracy: 0.9448
    Epoch 43/100
    5/5 [==============================] - 3s 685ms/step - loss: 0.3296 - accuracy: 0.9681 - val_loss: 0.9769 - val_accuracy: 0.9503
    Epoch 44/100
    5/5 [==============================] - 4s 760ms/step - loss: 0.3807 - accuracy: 0.9617 - val_loss: 0.5141 - val_accuracy: 0.9448
    Epoch 45/100
    5/5 [==============================] - 4s 744ms/step - loss: 0.2446 - accuracy: 0.9665 - val_loss: 0.3916 - val_accuracy: 0.9724
    Epoch 46/100
    5/5 [==============================] - 7s 1s/step - loss: 0.3203 - accuracy: 0.9712 - val_loss: 0.6596 - val_accuracy: 0.9724
    Epoch 47/100
    5/5 [==============================] - 3s 676ms/step - loss: 0.4767 - accuracy: 0.9585 - val_loss: 0.5353 - val_accuracy: 0.9669
    Epoch 48/100
    5/5 [==============================] - 3s 690ms/step - loss: 0.2807 - accuracy: 0.9681 - val_loss: 0.4518 - val_accuracy: 0.9669
    Epoch 49/100
    5/5 [==============================] - 3s 685ms/step - loss: 0.2603 - accuracy: 0.9744 - val_loss: 0.5304 - val_accuracy: 0.9613
    Epoch 50/100
    5/5 [==============================] - 4s 752ms/step - loss: 0.3094 - accuracy: 0.9760 - val_loss: 0.8894 - val_accuracy: 0.9503
    Epoch 51/100
    5/5 [==============================] - 4s 701ms/step - loss: 0.3798 - accuracy: 0.9712 - val_loss: 0.6935 - val_accuracy: 0.9558
    Epoch 52/100
    5/5 [==============================] - 3s 678ms/step - loss: 0.2317 - accuracy: 0.9696 - val_loss: 0.4885 - val_accuracy: 0.9558
    Epoch 53/100
    5/5 [==============================] - 3s 654ms/step - loss: 0.3906 - accuracy: 0.9696 - val_loss: 0.5495 - val_accuracy: 0.9282
    Epoch 54/100
    5/5 [==============================] - 7s 1s/step - loss: 0.4586 - accuracy: 0.9617 - val_loss: 0.6074 - val_accuracy: 0.9337
    Epoch 55/100
    5/5 [==============================] - 4s 748ms/step - loss: 0.2770 - accuracy: 0.9712 - val_loss: 0.3665 - val_accuracy: 0.9613
    Epoch 56/100
    5/5 [==============================] - 4s 714ms/step - loss: 0.3965 - accuracy: 0.9649 - val_loss: 1.2712 - val_accuracy: 0.9282
    Epoch 57/100
    5/5 [==============================] - 4s 760ms/step - loss: 0.3475 - accuracy: 0.9665 - val_loss: 0.5766 - val_accuracy: 0.9613
    Epoch 58/100
    5/5 [==============================] - 4s 746ms/step - loss: 0.2465 - accuracy: 0.9728 - val_loss: 0.4961 - val_accuracy: 0.9613
    Epoch 59/100
    5/5 [==============================] - 4s 712ms/step - loss: 0.4243 - accuracy: 0.9696 - val_loss: 0.3725 - val_accuracy: 0.9613
    Epoch 60/100
    5/5 [==============================] - 3s 685ms/step - loss: 0.2298 - accuracy: 0.9760 - val_loss: 0.5428 - val_accuracy: 0.9613
    Epoch 61/100
    5/5 [==============================] - 4s 750ms/step - loss: 0.1686 - accuracy: 0.9792 - val_loss: 0.8199 - val_accuracy: 0.9613
    Epoch 62/100
    5/5 [==============================] - 4s 758ms/step - loss: 0.3403 - accuracy: 0.9696 - val_loss: 0.7031 - val_accuracy: 0.9613
    Epoch 63/100
    5/5 [==============================] - 3s 667ms/step - loss: 0.1764 - accuracy: 0.9824 - val_loss: 0.8541 - val_accuracy: 0.9503
    Epoch 64/100
    5/5 [==============================] - 4s 718ms/step - loss: 0.2660 - accuracy: 0.9776 - val_loss: 0.9657 - val_accuracy: 0.9282
    Epoch 65/100
    5/5 [==============================] - 3s 677ms/step - loss: 0.3812 - accuracy: 0.9712 - val_loss: 1.0435 - val_accuracy: 0.9448
    Epoch 66/100
    5/5 [==============================] - 3s 694ms/step - loss: 0.1829 - accuracy: 0.9776 - val_loss: 1.1163 - val_accuracy: 0.9448
    Epoch 67/100
    5/5 [==============================] - 3s 671ms/step - loss: 0.2624 - accuracy: 0.9808 - val_loss: 1.2535 - val_accuracy: 0.9337
    Epoch 68/100
    5/5 [==============================] - 4s 739ms/step - loss: 0.3578 - accuracy: 0.9808 - val_loss: 1.1120 - val_accuracy: 0.9392
    Epoch 69/100
    5/5 [==============================] - 3s 676ms/step - loss: 0.3148 - accuracy: 0.9744 - val_loss: 0.9712 - val_accuracy: 0.9613
    Epoch 70/100
    5/5 [==============================] - 4s 789ms/step - loss: 0.1155 - accuracy: 0.9888 - val_loss: 1.0834 - val_accuracy: 0.9558
    Epoch 71/100
    5/5 [==============================] - 3s 691ms/step - loss: 0.1756 - accuracy: 0.9840 - val_loss: 0.7091 - val_accuracy: 0.9503
    Epoch 72/100
    5/5 [==============================] - 3s 682ms/step - loss: 0.3015 - accuracy: 0.9760 - val_loss: 1.1957 - val_accuracy: 0.9392
    Epoch 73/100
    5/5 [==============================] - 3s 687ms/step - loss: 0.2701 - accuracy: 0.9760 - val_loss: 0.8878 - val_accuracy: 0.9558
    Epoch 74/100
    5/5 [==============================] - 4s 747ms/step - loss: 0.2478 - accuracy: 0.9840 - val_loss: 1.2006 - val_accuracy: 0.9448
    Epoch 75/100
    5/5 [==============================] - 3s 683ms/step - loss: 0.2922 - accuracy: 0.9712 - val_loss: 1.2182 - val_accuracy: 0.9448
    Epoch 76/100
    5/5 [==============================] - 4s 768ms/step - loss: 0.1681 - accuracy: 0.9808 - val_loss: 0.9258 - val_accuracy: 0.9503
    Epoch 77/100
    5/5 [==============================] - 4s 748ms/step - loss: 0.2430 - accuracy: 0.9728 - val_loss: 0.6872 - val_accuracy: 0.9503
    Epoch 78/100
    5/5 [==============================] - 3s 672ms/step - loss: 0.1217 - accuracy: 0.9904 - val_loss: 0.6076 - val_accuracy: 0.9448
    Epoch 79/100
    5/5 [==============================] - 3s 675ms/step - loss: 0.2306 - accuracy: 0.9760 - val_loss: 0.4981 - val_accuracy: 0.9613
    Epoch 80/100
    5/5 [==============================] - 3s 681ms/step - loss: 0.3621 - accuracy: 0.9760 - val_loss: 0.5068 - val_accuracy: 0.9724
    Epoch 81/100
    5/5 [==============================] - 4s 763ms/step - loss: 0.2446 - accuracy: 0.9824 - val_loss: 1.2020 - val_accuracy: 0.9503
    Epoch 82/100
    5/5 [==============================] - 4s 717ms/step - loss: 0.2052 - accuracy: 0.9824 - val_loss: 1.1294 - val_accuracy: 0.9503
    Epoch 83/100
    5/5 [==============================] - 4s 761ms/step - loss: 0.2616 - accuracy: 0.9744 - val_loss: 0.9202 - val_accuracy: 0.9558
    Epoch 84/100
    5/5 [==============================] - 4s 758ms/step - loss: 0.1321 - accuracy: 0.9840 - val_loss: 1.2002 - val_accuracy: 0.9558
    Epoch 85/100
    5/5 [==============================] - 3s 680ms/step - loss: 0.2271 - accuracy: 0.9840 - val_loss: 1.2532 - val_accuracy: 0.9558
    Epoch 86/100
    5/5 [==============================] - 3s 700ms/step - loss: 0.3559 - accuracy: 0.9760 - val_loss: 1.1411 - val_accuracy: 0.9448
    Epoch 87/100
    5/5 [==============================] - 4s 769ms/step - loss: 0.2562 - accuracy: 0.9776 - val_loss: 0.8882 - val_accuracy: 0.9613
    Epoch 88/100
    5/5 [==============================] - 4s 763ms/step - loss: 0.0682 - accuracy: 0.9920 - val_loss: 1.0214 - val_accuracy: 0.9558
    Epoch 89/100
    5/5 [==============================] - 4s 710ms/step - loss: 0.2829 - accuracy: 0.9808 - val_loss: 1.0829 - val_accuracy: 0.9558
    Epoch 90/100
    5/5 [==============================] - 3s 684ms/step - loss: 0.2649 - accuracy: 0.9856 - val_loss: 0.9134 - val_accuracy: 0.9448
    Epoch 91/100
    5/5 [==============================] - 4s 700ms/step - loss: 0.2725 - accuracy: 0.9776 - val_loss: 0.6815 - val_accuracy: 0.9558
    Epoch 92/100
    5/5 [==============================] - 4s 766ms/step - loss: 0.2101 - accuracy: 0.9776 - val_loss: 0.6197 - val_accuracy: 0.9613
    Epoch 93/100
    5/5 [==============================] - 3s 663ms/step - loss: 0.0880 - accuracy: 0.9856 - val_loss: 0.6683 - val_accuracy: 0.9669
    Epoch 94/100
    5/5 [==============================] - 3s 690ms/step - loss: 0.2028 - accuracy: 0.9760 - val_loss: 0.6652 - val_accuracy: 0.9669
    Epoch 95/100
    5/5 [==============================] - 4s 749ms/step - loss: 0.1048 - accuracy: 0.9888 - val_loss: 0.5772 - val_accuracy: 0.9503
    Epoch 96/100
    5/5 [==============================] - 4s 755ms/step - loss: 0.1696 - accuracy: 0.9824 - val_loss: 0.7009 - val_accuracy: 0.9669
    Epoch 97/100
    5/5 [==============================] - 3s 682ms/step - loss: 0.2727 - accuracy: 0.9792 - val_loss: 1.0121 - val_accuracy: 0.9613
    Epoch 98/100
    5/5 [==============================] - 3s 684ms/step - loss: 0.2245 - accuracy: 0.9808 - val_loss: 0.9423 - val_accuracy: 0.9558
    Epoch 99/100
    5/5 [==============================] - 4s 762ms/step - loss: 0.0796 - accuracy: 0.9920 - val_loss: 0.8874 - val_accuracy: 0.9503
    Epoch 100/100
    5/5 [==============================] - 3s 676ms/step - loss: 0.2353 - accuracy: 0.9856 - val_loss: 0.9772 - val_accuracy: 0.9503

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/87.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 3s 610ms/step - loss: 1.5219 - accuracy: 0.9505

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[28\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.521850824356079, 0.9504587054252625]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9504587155963303
                  precision    recall  f1-score   support

               0       0.96      0.87      0.91       158
               1       0.95      0.98      0.97       387

        accuracy                           0.95       545
       macro avg       0.95      0.93      0.94       545
    weighted avg       0.95      0.95      0.95       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/88.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/89.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[32\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/90.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.96      0.87      0.91       158
               1       0.95      0.98      0.97       387

        accuracy                           0.95       545
       macro avg       0.95      0.93      0.94       545
    weighted avg       0.95      0.95      0.95       545

                       pre       rec       spe        f1       geo       iba       sup

              0       0.96      0.87      0.98      0.91      0.92      0.84       158
              1       0.95      0.98      0.87      0.97      0.92      0.86       387

    avg / total       0.95      0.95      0.90      0.95      0.92      0.86       545

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
