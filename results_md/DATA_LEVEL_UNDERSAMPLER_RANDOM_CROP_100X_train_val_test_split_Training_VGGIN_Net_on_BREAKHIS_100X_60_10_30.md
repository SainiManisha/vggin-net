<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
#Conv2D(filters=100, kernel_size=2, name='C2'),
dropout = Dropout(0.4, name='Dropout')(f)
#GlobalAveragePooling2D(name='G4'),
#     Dropout(0.3, name='D5'),
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard('./tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np

def dataset(path):
    gen = ImageDataGenerator()
    ds = gen.flow_from_directory(path, target_size=image_size)
    X, y = [], []
    for step, (img, label) in enumerate(ds):
        if step &gt;= len(ds):
            break
        X.append(img)
        y.append(label)
    X = np.concatenate(X)
    y = np.concatenate(y)
    return X, y

(X_train, y_train), (X_val, y_val), (X_test, y_test) = dataset(train_path), dataset(val_path), dataset(test_path)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 images belonging to 2 classes.
    Found 187 images belonging to 2 classes.
    Found 566 images belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.under_sampling import RandomUnderSampler
from tensorflow.keras.utils import to_categorical
ros = RandomUnderSampler()
X_train = X_train.reshape((X_train.shape[0], -1))
X_resampled, y_resampled = ros.fit_resample(X_train, y_train)

X_resampled = X_resampled.reshape((X_resampled.shape[0], *image_size, 3))
y_resampled = to_categorical(y_resampled)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    /opt/conda/lib/python3.7/site-packages/sklearn/utils/validation.py:71: FutureWarning: Pass classes=[0 1] as keyword args. From version 0.25 passing these as positional arguments will result in an error
      FutureWarning)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
X_resampled.shape, y_resampled.shape
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ((652, 224, 340, 3), (652, 2))

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train = tf.data.Dataset.from_tensor_slices((X_resampled, y_resampled))
val = tf.data.Dataset.from_tensor_slices((X_val, y_val))
test = tf.data.Dataset.from_tensor_slices((X_test, y_test))
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
def dataset(ds, image_size, crop_size, batch_size, train):
    ds = ds.shuffle(1000).batch(batch_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'DATALEVEL_OVERSAMPLER_RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30-VGGINet'
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
path = './experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/6 [====&gt;.........................] - ETA: 0s - loss: 1.2694 - accuracy: 0.4922WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    6/6 [==============================] - 6s 1s/step - loss: 4.1273 - accuracy: 0.6472 - val_loss: 12.5482 - val_accuracy: 0.8021
    Epoch 2/100
    6/6 [==============================] - 4s 671ms/step - loss: 4.0081 - accuracy: 0.7975 - val_loss: 3.8999 - val_accuracy: 0.9037
    Epoch 3/100
    6/6 [==============================] - 4s 749ms/step - loss: 2.1515 - accuracy: 0.8528 - val_loss: 6.9452 - val_accuracy: 0.8503
    Epoch 4/100
    6/6 [==============================] - 4s 681ms/step - loss: 2.3428 - accuracy: 0.8405 - val_loss: 6.7016 - val_accuracy: 0.7754
    Epoch 5/100
    6/6 [==============================] - 4s 651ms/step - loss: 2.2841 - accuracy: 0.8252 - val_loss: 3.3493 - val_accuracy: 0.8342
    Epoch 6/100
    6/6 [==============================] - 4s 641ms/step - loss: 1.6770 - accuracy: 0.8604 - val_loss: 1.4623 - val_accuracy: 0.9091
    Epoch 7/100
    6/6 [==============================] - 4s 672ms/step - loss: 1.5020 - accuracy: 0.8712 - val_loss: 1.8114 - val_accuracy: 0.8824
    Epoch 8/100
    6/6 [==============================] - 4s 708ms/step - loss: 1.8767 - accuracy: 0.8482 - val_loss: 1.4333 - val_accuracy: 0.9358
    Epoch 9/100
    6/6 [==============================] - 5s 757ms/step - loss: 1.7915 - accuracy: 0.8804 - val_loss: 1.3274 - val_accuracy: 0.9465
    Epoch 10/100
    6/6 [==============================] - 5s 792ms/step - loss: 1.6170 - accuracy: 0.8543 - val_loss: 0.9341 - val_accuracy: 0.9412
    Epoch 11/100
    6/6 [==============================] - 4s 750ms/step - loss: 1.8584 - accuracy: 0.8788 - val_loss: 1.1390 - val_accuracy: 0.9572
    Epoch 12/100
    6/6 [==============================] - 4s 688ms/step - loss: 1.3980 - accuracy: 0.9003 - val_loss: 1.7448 - val_accuracy: 0.9144
    Epoch 13/100
    6/6 [==============================] - 4s 744ms/step - loss: 1.9433 - accuracy: 0.8758 - val_loss: 1.7389 - val_accuracy: 0.9251
    Epoch 14/100
    6/6 [==============================] - 5s 766ms/step - loss: 1.7978 - accuracy: 0.8865 - val_loss: 1.7598 - val_accuracy: 0.9358
    Epoch 15/100
    6/6 [==============================] - 4s 729ms/step - loss: 1.3090 - accuracy: 0.9141 - val_loss: 1.6689 - val_accuracy: 0.9144
    Epoch 16/100
    6/6 [==============================] - 4s 660ms/step - loss: 2.2612 - accuracy: 0.8773 - val_loss: 1.4900 - val_accuracy: 0.9144
    Epoch 17/100
    6/6 [==============================] - 4s 720ms/step - loss: 1.1886 - accuracy: 0.8865 - val_loss: 1.9265 - val_accuracy: 0.8717
    Epoch 18/100
    6/6 [==============================] - 8s 1s/step - loss: 1.4308 - accuracy: 0.8865 - val_loss: 2.3826 - val_accuracy: 0.8824
    Epoch 19/100
    6/6 [==============================] - 4s 745ms/step - loss: 1.2720 - accuracy: 0.9187 - val_loss: 3.2050 - val_accuracy: 0.8930
    Epoch 20/100
    6/6 [==============================] - 4s 694ms/step - loss: 1.8636 - accuracy: 0.8926 - val_loss: 1.7626 - val_accuracy: 0.9251
    Epoch 21/100
    6/6 [==============================] - 4s 665ms/step - loss: 1.1716 - accuracy: 0.9202 - val_loss: 1.6348 - val_accuracy: 0.8984
    Epoch 22/100
    6/6 [==============================] - 4s 646ms/step - loss: 1.2378 - accuracy: 0.9095 - val_loss: 1.0402 - val_accuracy: 0.9144
    Epoch 23/100
    6/6 [==============================] - 4s 717ms/step - loss: 1.4868 - accuracy: 0.8926 - val_loss: 1.1312 - val_accuracy: 0.9305
    Epoch 24/100
    6/6 [==============================] - 4s 657ms/step - loss: 1.3160 - accuracy: 0.9156 - val_loss: 1.3543 - val_accuracy: 0.9144
    Epoch 25/100
    6/6 [==============================] - 4s 677ms/step - loss: 1.1328 - accuracy: 0.9095 - val_loss: 1.5005 - val_accuracy: 0.8824
    Epoch 26/100
    6/6 [==============================] - 4s 736ms/step - loss: 1.3159 - accuracy: 0.9064 - val_loss: 0.8836 - val_accuracy: 0.9465
    Epoch 27/100
    6/6 [==============================] - 4s 656ms/step - loss: 0.7804 - accuracy: 0.9310 - val_loss: 0.9566 - val_accuracy: 0.9465
    Epoch 28/100
    6/6 [==============================] - 4s 723ms/step - loss: 1.1589 - accuracy: 0.9356 - val_loss: 1.8178 - val_accuracy: 0.9144
    Epoch 29/100
    6/6 [==============================] - 4s 721ms/step - loss: 0.8932 - accuracy: 0.9310 - val_loss: 1.4855 - val_accuracy: 0.9198
    Epoch 30/100
    6/6 [==============================] - 4s 748ms/step - loss: 1.0097 - accuracy: 0.9371 - val_loss: 1.2166 - val_accuracy: 0.9251
    Epoch 31/100
    6/6 [==============================] - 4s 686ms/step - loss: 1.1137 - accuracy: 0.9218 - val_loss: 0.8404 - val_accuracy: 0.9412
    Epoch 32/100
    6/6 [==============================] - 4s 680ms/step - loss: 1.6335 - accuracy: 0.8865 - val_loss: 0.8372 - val_accuracy: 0.9465
    Epoch 33/100
    6/6 [==============================] - 5s 751ms/step - loss: 1.2613 - accuracy: 0.9264 - val_loss: 1.7180 - val_accuracy: 0.9251
    Epoch 34/100
    6/6 [==============================] - 4s 729ms/step - loss: 1.3410 - accuracy: 0.9049 - val_loss: 1.8327 - val_accuracy: 0.9144
    Epoch 35/100
    6/6 [==============================] - 4s 730ms/step - loss: 1.6555 - accuracy: 0.9080 - val_loss: 0.9865 - val_accuracy: 0.9358
    Epoch 36/100
    6/6 [==============================] - 4s 722ms/step - loss: 0.8659 - accuracy: 0.9325 - val_loss: 0.6591 - val_accuracy: 0.9519
    Epoch 37/100
    6/6 [==============================] - 4s 679ms/step - loss: 1.0972 - accuracy: 0.9095 - val_loss: 1.0750 - val_accuracy: 0.9251
    Epoch 38/100
    6/6 [==============================] - 5s 760ms/step - loss: 1.6007 - accuracy: 0.8896 - val_loss: 1.0460 - val_accuracy: 0.9412
    Epoch 39/100
    6/6 [==============================] - 4s 662ms/step - loss: 1.4768 - accuracy: 0.9095 - val_loss: 1.6509 - val_accuracy: 0.9305
    Epoch 40/100
    6/6 [==============================] - 4s 662ms/step - loss: 1.4841 - accuracy: 0.9233 - val_loss: 1.8516 - val_accuracy: 0.9091
    Epoch 41/100
    6/6 [==============================] - 4s 696ms/step - loss: 1.9867 - accuracy: 0.8880 - val_loss: 2.5318 - val_accuracy: 0.8717
    Epoch 42/100
    6/6 [==============================] - 4s 730ms/step - loss: 2.4160 - accuracy: 0.9080 - val_loss: 1.5817 - val_accuracy: 0.9519
    Epoch 43/100
    6/6 [==============================] - 4s 697ms/step - loss: 2.9923 - accuracy: 0.8681 - val_loss: 1.5993 - val_accuracy: 0.9465
    Epoch 44/100
    6/6 [==============================] - 5s 767ms/step - loss: 1.7229 - accuracy: 0.9080 - val_loss: 2.5390 - val_accuracy: 0.9091
    Epoch 45/100
    6/6 [==============================] - 4s 725ms/step - loss: 2.1122 - accuracy: 0.9049 - val_loss: 1.5605 - val_accuracy: 0.9465
    Epoch 46/100
    6/6 [==============================] - 5s 753ms/step - loss: 1.1077 - accuracy: 0.9371 - val_loss: 1.4704 - val_accuracy: 0.9465
    Epoch 47/100
    6/6 [==============================] - 4s 731ms/step - loss: 1.5507 - accuracy: 0.9387 - val_loss: 3.2022 - val_accuracy: 0.8717
    Epoch 48/100
    6/6 [==============================] - 5s 757ms/step - loss: 1.4563 - accuracy: 0.9187 - val_loss: 3.4690 - val_accuracy: 0.8556
    Epoch 49/100
    6/6 [==============================] - 4s 676ms/step - loss: 0.7278 - accuracy: 0.9448 - val_loss: 2.0977 - val_accuracy: 0.9305
    Epoch 50/100
    6/6 [==============================] - 8s 1s/step - loss: 0.9499 - accuracy: 0.9463 - val_loss: 1.4728 - val_accuracy: 0.9465
    Epoch 51/100
    6/6 [==============================] - 4s 690ms/step - loss: 0.7022 - accuracy: 0.9555 - val_loss: 0.9740 - val_accuracy: 0.9465
    Epoch 52/100
    6/6 [==============================] - 4s 706ms/step - loss: 0.8915 - accuracy: 0.9340 - val_loss: 1.7708 - val_accuracy: 0.8930
    Epoch 53/100
    6/6 [==============================] - 4s 730ms/step - loss: 0.7908 - accuracy: 0.9540 - val_loss: 1.0856 - val_accuracy: 0.9412
    Epoch 54/100
    6/6 [==============================] - 4s 726ms/step - loss: 0.9914 - accuracy: 0.9479 - val_loss: 1.1676 - val_accuracy: 0.9465
    Epoch 55/100
    6/6 [==============================] - 4s 735ms/step - loss: 0.7182 - accuracy: 0.9555 - val_loss: 1.1607 - val_accuracy: 0.9465
    Epoch 56/100
    6/6 [==============================] - 4s 658ms/step - loss: 1.3827 - accuracy: 0.9402 - val_loss: 1.2524 - val_accuracy: 0.9572
    Epoch 57/100
    6/6 [==============================] - 4s 666ms/step - loss: 1.5260 - accuracy: 0.9233 - val_loss: 0.7386 - val_accuracy: 0.9626
    Epoch 58/100
    6/6 [==============================] - 4s 694ms/step - loss: 1.3962 - accuracy: 0.9202 - val_loss: 0.8882 - val_accuracy: 0.9412
    Epoch 59/100
    6/6 [==============================] - 4s 660ms/step - loss: 1.5010 - accuracy: 0.9233 - val_loss: 1.1476 - val_accuracy: 0.9412
    Epoch 60/100
    6/6 [==============================] - 4s 655ms/step - loss: 0.8980 - accuracy: 0.9525 - val_loss: 0.6987 - val_accuracy: 0.9626
    Epoch 61/100
    6/6 [==============================] - 4s 736ms/step - loss: 1.0203 - accuracy: 0.9540 - val_loss: 2.6110 - val_accuracy: 0.9144
    Epoch 62/100
    6/6 [==============================] - 4s 727ms/step - loss: 1.5525 - accuracy: 0.9356 - val_loss: 2.1495 - val_accuracy: 0.9251
    Epoch 63/100
    6/6 [==============================] - 4s 730ms/step - loss: 0.6898 - accuracy: 0.9509 - val_loss: 1.2464 - val_accuracy: 0.9412
    Epoch 64/100
    6/6 [==============================] - 4s 717ms/step - loss: 1.1549 - accuracy: 0.9417 - val_loss: 3.8248 - val_accuracy: 0.8824
    Epoch 65/100
    6/6 [==============================] - 4s 663ms/step - loss: 1.3869 - accuracy: 0.9371 - val_loss: 14.8253 - val_accuracy: 0.6845
    Epoch 66/100
    6/6 [==============================] - 5s 750ms/step - loss: 1.1132 - accuracy: 0.9294 - val_loss: 16.4479 - val_accuracy: 0.6524
    Epoch 67/100
    6/6 [==============================] - 4s 698ms/step - loss: 0.9458 - accuracy: 0.9463 - val_loss: 7.1363 - val_accuracy: 0.7968
    Epoch 68/100
    6/6 [==============================] - 4s 655ms/step - loss: 1.1491 - accuracy: 0.9371 - val_loss: 2.2851 - val_accuracy: 0.9037
    Epoch 69/100
    6/6 [==============================] - 4s 736ms/step - loss: 0.4186 - accuracy: 0.9693 - val_loss: 1.9434 - val_accuracy: 0.9037
    Epoch 70/100
    6/6 [==============================] - 4s 725ms/step - loss: 0.7159 - accuracy: 0.9540 - val_loss: 1.3842 - val_accuracy: 0.9305
    Epoch 71/100
    6/6 [==============================] - 4s 716ms/step - loss: 0.7768 - accuracy: 0.9494 - val_loss: 1.1762 - val_accuracy: 0.9626
    Epoch 72/100
    6/6 [==============================] - 4s 648ms/step - loss: 1.1334 - accuracy: 0.9417 - val_loss: 1.0574 - val_accuracy: 0.9679
    Epoch 73/100
    6/6 [==============================] - 4s 720ms/step - loss: 0.8632 - accuracy: 0.9463 - val_loss: 1.1757 - val_accuracy: 0.9679
    Epoch 74/100
    6/6 [==============================] - 4s 707ms/step - loss: 0.4369 - accuracy: 0.9724 - val_loss: 1.3148 - val_accuracy: 0.9679
    Epoch 75/100
    6/6 [==============================] - 4s 744ms/step - loss: 1.1661 - accuracy: 0.9356 - val_loss: 1.5782 - val_accuracy: 0.9305
    Epoch 76/100
    6/6 [==============================] - 4s 736ms/step - loss: 0.4874 - accuracy: 0.9663 - val_loss: 1.1320 - val_accuracy: 0.9412
    Epoch 77/100
    6/6 [==============================] - 4s 718ms/step - loss: 0.7891 - accuracy: 0.9509 - val_loss: 1.3577 - val_accuracy: 0.9198
    Epoch 78/100
    6/6 [==============================] - 4s 736ms/step - loss: 0.7481 - accuracy: 0.9601 - val_loss: 1.3631 - val_accuracy: 0.9519
    Epoch 79/100
    6/6 [==============================] - 4s 663ms/step - loss: 0.6225 - accuracy: 0.9693 - val_loss: 1.2881 - val_accuracy: 0.9519
    Epoch 80/100
    6/6 [==============================] - 4s 700ms/step - loss: 0.9735 - accuracy: 0.9525 - val_loss: 2.2885 - val_accuracy: 0.9091
    Epoch 81/100
    6/6 [==============================] - 4s 735ms/step - loss: 1.1420 - accuracy: 0.9402 - val_loss: 1.6889 - val_accuracy: 0.9412
    Epoch 82/100
    6/6 [==============================] - 4s 740ms/step - loss: 0.5685 - accuracy: 0.9709 - val_loss: 1.3463 - val_accuracy: 0.9519
    Epoch 83/100
    6/6 [==============================] - 5s 758ms/step - loss: 0.2878 - accuracy: 0.9709 - val_loss: 1.2097 - val_accuracy: 0.9572
    Epoch 84/100
    6/6 [==============================] - 4s 663ms/step - loss: 0.4034 - accuracy: 0.9709 - val_loss: 1.1999 - val_accuracy: 0.9572
    Epoch 85/100
    6/6 [==============================] - 4s 680ms/step - loss: 0.7112 - accuracy: 0.9601 - val_loss: 1.1955 - val_accuracy: 0.9572
    Epoch 86/100
    6/6 [==============================] - 4s 705ms/step - loss: 0.5355 - accuracy: 0.9663 - val_loss: 0.9096 - val_accuracy: 0.9679
    Epoch 87/100
    6/6 [==============================] - 4s 682ms/step - loss: 0.4537 - accuracy: 0.9709 - val_loss: 0.8245 - val_accuracy: 0.9626
    Epoch 88/100
    6/6 [==============================] - 4s 686ms/step - loss: 0.4677 - accuracy: 0.9632 - val_loss: 0.6913 - val_accuracy: 0.9733
    Epoch 89/100
    6/6 [==============================] - 4s 686ms/step - loss: 0.6291 - accuracy: 0.9647 - val_loss: 0.5552 - val_accuracy: 0.9733
    Epoch 90/100
    6/6 [==============================] - 4s 717ms/step - loss: 0.7344 - accuracy: 0.9509 - val_loss: 0.8124 - val_accuracy: 0.9572
    Epoch 91/100
    6/6 [==============================] - 5s 789ms/step - loss: 1.1014 - accuracy: 0.9463 - val_loss: 1.0060 - val_accuracy: 0.9465
    Epoch 92/100
    6/6 [==============================] - 4s 651ms/step - loss: 0.9432 - accuracy: 0.9494 - val_loss: 0.9250 - val_accuracy: 0.9412
    Epoch 93/100
    6/6 [==============================] - 4s 713ms/step - loss: 2.3884 - accuracy: 0.9018 - val_loss: 7.4121 - val_accuracy: 0.7861
    Epoch 94/100
    6/6 [==============================] - 4s 747ms/step - loss: 1.3345 - accuracy: 0.9371 - val_loss: 2.8603 - val_accuracy: 0.9037
    Epoch 95/100
    6/6 [==============================] - 4s 661ms/step - loss: 1.6444 - accuracy: 0.9340 - val_loss: 1.2558 - val_accuracy: 0.9412
    Epoch 96/100
    6/6 [==============================] - 4s 683ms/step - loss: 1.4365 - accuracy: 0.9479 - val_loss: 1.7982 - val_accuracy: 0.9144
    Epoch 97/100
    6/6 [==============================] - 5s 781ms/step - loss: 1.0597 - accuracy: 0.9463 - val_loss: 2.7727 - val_accuracy: 0.9198
    Epoch 98/100
    6/6 [==============================] - 4s 703ms/step - loss: 1.7095 - accuracy: 0.9479 - val_loss: 1.7690 - val_accuracy: 0.9465
    Epoch 99/100
    6/6 [==============================] - 5s 751ms/step - loss: 1.1733 - accuracy: 0.9433 - val_loss: 1.3763 - val_accuracy: 0.9519
    Epoch 100/100
    6/6 [==============================] - 4s 657ms/step - loss: 1.2551 - accuracy: 0.9387 - val_loss: 1.3747 - val_accuracy: 0.9412

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r tensorboard-logs {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig("training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/117.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 4s 731ms/step - loss: 2.0101 - accuracy: 0.9382

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[28\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [2.0100622177124023, 0.9381625652313232]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9381625441696113
                  precision    recall  f1-score   support

               0       0.85      0.95      0.90       164
               1       0.98      0.93      0.96       402

        accuracy                           0.94       566
       macro avg       0.92      0.94      0.93       566
    weighted avg       0.94      0.94      0.94       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/118.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/119.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[32\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/120.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.85      0.95      0.90       164
               1       0.98      0.93      0.96       402

        accuracy                           0.94       566
       macro avg       0.92      0.94      0.93       566
    weighted avg       0.94      0.94      0.94       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.85      0.95      0.93      0.90      0.94      0.89       164
              1       0.98      0.93      0.95      0.96      0.94      0.89       402

    avg / total       0.94      0.94      0.95      0.94      0.94      0.89       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
