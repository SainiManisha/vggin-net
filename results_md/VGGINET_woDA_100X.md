```python
import os
MAGNIFICATION_FACTOR = os.environ['MAGNIFICATION_FACTOR']
NUM_RUN = '1'
print((MAGNIFICATION_FACTOR, NUM_RUN))
```

    ('100X', '1')



```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________



```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________



```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```


```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate
def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```


```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```


```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)

f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```


```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________



```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```


```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```


```python
name = 'VGGINetWODA/%s' % MAGNIFICATION_FACTOR
path = './%s/%s' % (name, NUM_RUN)
!mkdir -p {path}
```


```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```


```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs/' + NUM_RUN)]
```


```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/train'
val_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/val'
test_path = '../../Splitted_' + MAGNIFICATION_FACTOR + '/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
#     # NO DATA AUGMENTATION WILL BE USED FOR THIS EXPERIMENT!
#     if train:
#         ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
#     ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.



```python
epochs=100
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

    Epoch 1/100
    1/9 [==>...........................] - ETA: 0s - loss: 1.2671 - accuracy: 0.4609WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 53s 6s/step - loss: 3.8363 - accuracy: 0.7482 - val_loss: 35.1075 - val_accuracy: 0.5401
    Epoch 2/100
    9/9 [==============================] - 10s 1s/step - loss: 0.8051 - accuracy: 0.9264 - val_loss: 8.8572 - val_accuracy: 0.8449
    Epoch 3/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1616 - accuracy: 0.9734 - val_loss: 2.3682 - val_accuracy: 0.9037
    Epoch 4/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0523 - accuracy: 0.9894 - val_loss: 1.7591 - val_accuracy: 0.9091
    Epoch 5/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0276 - accuracy: 0.9938 - val_loss: 3.1986 - val_accuracy: 0.8984
    Epoch 6/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0106 - accuracy: 0.9973 - val_loss: 3.0344 - val_accuracy: 0.8877
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0031 - accuracy: 0.9991 - val_loss: 2.2671 - val_accuracy: 0.9037
    Epoch 8/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0030 - accuracy: 0.9982 - val_loss: 1.7853 - val_accuracy: 0.9091
    Epoch 9/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0018 - accuracy: 0.9991 - val_loss: 1.6852 - val_accuracy: 0.9037
    Epoch 10/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0075 - accuracy: 0.9991 - val_loss: 1.6259 - val_accuracy: 0.9037
    Epoch 11/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0017 - accuracy: 0.9982 - val_loss: 1.7392 - val_accuracy: 0.9091
    Epoch 12/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0082 - accuracy: 0.9991 - val_loss: 1.4022 - val_accuracy: 0.8984
    Epoch 13/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0012 - accuracy: 0.9991 - val_loss: 1.4954 - val_accuracy: 0.9091
    Epoch 14/100
    9/9 [==============================] - 10s 1s/step - loss: 5.4269e-04 - accuracy: 1.0000 - val_loss: 1.5271 - val_accuracy: 0.9037
    Epoch 15/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0094 - accuracy: 0.9973 - val_loss: 1.3848 - val_accuracy: 0.9144
    Epoch 16/100
    9/9 [==============================] - 10s 1s/step - loss: 6.1569e-05 - accuracy: 1.0000 - val_loss: 1.4258 - val_accuracy: 0.9144
    Epoch 17/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0019 - accuracy: 0.9991 - val_loss: 1.5273 - val_accuracy: 0.9091
    Epoch 18/100
    9/9 [==============================] - 10s 1s/step - loss: 1.0273e-04 - accuracy: 1.0000 - val_loss: 1.5723 - val_accuracy: 0.9091
    Epoch 19/100
    9/9 [==============================] - 10s 1s/step - loss: 5.2606e-06 - accuracy: 1.0000 - val_loss: 1.5721 - val_accuracy: 0.9091
    Epoch 20/100
    9/9 [==============================] - 10s 1s/step - loss: 9.4160e-04 - accuracy: 0.9991 - val_loss: 1.5382 - val_accuracy: 0.9144
    Epoch 21/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1320e-05 - accuracy: 1.0000 - val_loss: 1.5172 - val_accuracy: 0.9091
    Epoch 22/100
    9/9 [==============================] - 10s 1s/step - loss: 8.4304e-04 - accuracy: 0.9991 - val_loss: 1.4684 - val_accuracy: 0.9091
    Epoch 23/100
    9/9 [==============================] - 10s 1s/step - loss: 1.8271e-06 - accuracy: 1.0000 - val_loss: 1.3112 - val_accuracy: 0.9251
    Epoch 24/100
    9/9 [==============================] - 10s 1s/step - loss: 5.4629e-06 - accuracy: 1.0000 - val_loss: 1.2695 - val_accuracy: 0.9251
    Epoch 25/100
    9/9 [==============================] - 10s 1s/step - loss: 1.9028e-05 - accuracy: 1.0000 - val_loss: 1.2511 - val_accuracy: 0.9251
    Epoch 26/100
    9/9 [==============================] - 10s 1s/step - loss: 6.8384e-06 - accuracy: 1.0000 - val_loss: 1.2415 - val_accuracy: 0.9251
    Epoch 27/100
    9/9 [==============================] - 11s 1s/step - loss: 1.2456e-04 - accuracy: 1.0000 - val_loss: 1.2390 - val_accuracy: 0.9251
    Epoch 28/100
    9/9 [==============================] - 10s 1s/step - loss: 5.2549e-05 - accuracy: 1.0000 - val_loss: 1.2416 - val_accuracy: 0.9198
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 1.3197e-06 - accuracy: 1.0000 - val_loss: 1.2459 - val_accuracy: 0.9198
    Epoch 30/100
    9/9 [==============================] - 11s 1s/step - loss: 4.1720e-06 - accuracy: 1.0000 - val_loss: 1.2458 - val_accuracy: 0.9198
    Epoch 31/100
    9/9 [==============================] - 10s 1s/step - loss: 7.8578e-05 - accuracy: 1.0000 - val_loss: 1.2472 - val_accuracy: 0.9198
    Epoch 32/100
    9/9 [==============================] - 10s 1s/step - loss: 1.5540e-04 - accuracy: 1.0000 - val_loss: 1.2459 - val_accuracy: 0.9251
    Epoch 33/100
    9/9 [==============================] - 10s 1s/step - loss: 8.0740e-08 - accuracy: 1.0000 - val_loss: 1.2459 - val_accuracy: 0.9251
    Epoch 34/100
    9/9 [==============================] - 10s 1s/step - loss: 1.1431e-05 - accuracy: 1.0000 - val_loss: 1.2447 - val_accuracy: 0.9251
    Epoch 35/100
    9/9 [==============================] - 10s 1s/step - loss: 3.6011e-07 - accuracy: 1.0000 - val_loss: 1.2429 - val_accuracy: 0.9251
    Epoch 36/100
    9/9 [==============================] - 11s 1s/step - loss: 1.4245e-06 - accuracy: 1.0000 - val_loss: 1.2412 - val_accuracy: 0.9251
    Epoch 37/100
    9/9 [==============================] - 10s 1s/step - loss: 2.5984e-06 - accuracy: 1.0000 - val_loss: 1.2395 - val_accuracy: 0.9251
    Epoch 38/100
    9/9 [==============================] - 10s 1s/step - loss: 6.4906e-07 - accuracy: 1.0000 - val_loss: 1.2379 - val_accuracy: 0.9251
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 8.8612e-05 - accuracy: 1.0000 - val_loss: 1.2438 - val_accuracy: 0.9251
    Epoch 40/100
    9/9 [==============================] - 10s 1s/step - loss: 1.7754e-07 - accuracy: 1.0000 - val_loss: 1.2462 - val_accuracy: 0.9251
    Epoch 41/100
    9/9 [==============================] - 10s 1s/step - loss: 8.0528e-08 - accuracy: 1.0000 - val_loss: 1.2466 - val_accuracy: 0.9251
    Epoch 42/100
    9/9 [==============================] - 10s 1s/step - loss: 1.5007e-08 - accuracy: 1.0000 - val_loss: 1.2458 - val_accuracy: 0.9251
    Epoch 43/100
    9/9 [==============================] - 10s 1s/step - loss: 1.7453e-06 - accuracy: 1.0000 - val_loss: 1.2446 - val_accuracy: 0.9251
    Epoch 44/100
    9/9 [==============================] - 10s 1s/step - loss: 2.2825e-07 - accuracy: 1.0000 - val_loss: 1.2435 - val_accuracy: 0.9251
    Epoch 45/100
    9/9 [==============================] - 10s 1s/step - loss: 2.0108e-05 - accuracy: 1.0000 - val_loss: 1.2475 - val_accuracy: 0.9251
    Epoch 46/100
    9/9 [==============================] - 11s 1s/step - loss: 2.9069e-07 - accuracy: 1.0000 - val_loss: 1.2485 - val_accuracy: 0.9251
    Epoch 47/100
    9/9 [==============================] - 10s 1s/step - loss: 6.1216e-04 - accuracy: 1.0000 - val_loss: 1.3109 - val_accuracy: 0.9091
    Epoch 48/100
    9/9 [==============================] - 10s 1s/step - loss: 4.7324e-04 - accuracy: 1.0000 - val_loss: 1.3606 - val_accuracy: 0.9091
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0015 - accuracy: 0.9991 - val_loss: 1.4589 - val_accuracy: 0.9198
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 5.6378e-04 - accuracy: 1.0000 - val_loss: 1.6275 - val_accuracy: 0.9144
    Epoch 51/100
    9/9 [==============================] - 10s 1s/step - loss: 7.8744e-05 - accuracy: 1.0000 - val_loss: 1.6341 - val_accuracy: 0.9037
    Epoch 52/100
    9/9 [==============================] - 11s 1s/step - loss: 1.6271e-05 - accuracy: 1.0000 - val_loss: 1.6315 - val_accuracy: 0.9037
    Epoch 53/100
    9/9 [==============================] - 10s 1s/step - loss: 9.9294e-07 - accuracy: 1.0000 - val_loss: 1.6264 - val_accuracy: 0.9037
    Epoch 54/100
    9/9 [==============================] - 10s 1s/step - loss: 8.0040e-06 - accuracy: 1.0000 - val_loss: 1.6191 - val_accuracy: 0.9037
    Epoch 55/100
    9/9 [==============================] - 10s 1s/step - loss: 5.1264e-05 - accuracy: 1.0000 - val_loss: 1.6071 - val_accuracy: 0.9037
    Epoch 56/100
    9/9 [==============================] - 10s 1s/step - loss: 8.6745e-06 - accuracy: 1.0000 - val_loss: 1.5782 - val_accuracy: 0.9037
    Epoch 57/100
    9/9 [==============================] - 10s 1s/step - loss: 5.2684e-06 - accuracy: 1.0000 - val_loss: 1.5637 - val_accuracy: 0.9037
    Epoch 58/100
    9/9 [==============================] - 10s 1s/step - loss: 8.5596e-07 - accuracy: 1.0000 - val_loss: 1.5560 - val_accuracy: 0.9037
    Epoch 59/100
    9/9 [==============================] - 11s 1s/step - loss: 1.5135e-06 - accuracy: 1.0000 - val_loss: 1.5512 - val_accuracy: 0.9037
    Epoch 60/100
    9/9 [==============================] - 10s 1s/step - loss: 1.4256e-05 - accuracy: 1.0000 - val_loss: 1.5470 - val_accuracy: 0.9037
    Epoch 61/100
    9/9 [==============================] - 11s 1s/step - loss: 1.3931e-06 - accuracy: 1.0000 - val_loss: 1.5437 - val_accuracy: 0.9037
    Epoch 62/100
    9/9 [==============================] - 11s 1s/step - loss: 2.2175e-06 - accuracy: 1.0000 - val_loss: 1.5411 - val_accuracy: 0.9037
    Epoch 63/100
    9/9 [==============================] - 11s 1s/step - loss: 4.6686e-06 - accuracy: 1.0000 - val_loss: 1.5391 - val_accuracy: 0.9037
    Epoch 64/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0015 - accuracy: 0.9991 - val_loss: 1.6082 - val_accuracy: 0.9091
    Epoch 65/100
    9/9 [==============================] - 11s 1s/step - loss: 2.0058e-05 - accuracy: 1.0000 - val_loss: 1.7852 - val_accuracy: 0.9198
    Epoch 66/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0106 - accuracy: 0.9973 - val_loss: 1.5719 - val_accuracy: 0.9037
    Epoch 67/100
    9/9 [==============================] - 11s 1s/step - loss: 1.0423e-04 - accuracy: 1.0000 - val_loss: 1.9700 - val_accuracy: 0.8824
    Epoch 68/100
    9/9 [==============================] - 11s 1s/step - loss: 8.6084e-04 - accuracy: 0.9991 - val_loss: 1.9593 - val_accuracy: 0.8877
    Epoch 69/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0014 - accuracy: 0.9991 - val_loss: 1.6677 - val_accuracy: 0.9198
    Epoch 70/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0078 - accuracy: 0.9973 - val_loss: 1.6747 - val_accuracy: 0.9091
    Epoch 71/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0060 - accuracy: 0.9982 - val_loss: 1.7693 - val_accuracy: 0.9037
    Epoch 72/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0039 - accuracy: 0.9991 - val_loss: 1.7651 - val_accuracy: 0.9091
    Epoch 73/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0025 - accuracy: 0.9982 - val_loss: 2.0881 - val_accuracy: 0.9037
    Epoch 74/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0053 - accuracy: 0.9982 - val_loss: 2.2835 - val_accuracy: 0.8930
    Epoch 75/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0087 - accuracy: 0.9982 - val_loss: 1.9515 - val_accuracy: 0.9091
    Epoch 76/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0107 - accuracy: 0.9973 - val_loss: 1.5580 - val_accuracy: 0.8984
    Epoch 77/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0360 - accuracy: 0.9938 - val_loss: 1.3743 - val_accuracy: 0.9091
    Epoch 78/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0400 - accuracy: 0.9938 - val_loss: 1.9384 - val_accuracy: 0.8984
    Epoch 79/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0712 - accuracy: 0.9920 - val_loss: 1.9817 - val_accuracy: 0.8984
    Epoch 80/100
    9/9 [==============================] - 11s 1s/step - loss: 0.1369 - accuracy: 0.9858 - val_loss: 1.6587 - val_accuracy: 0.8930
    Epoch 81/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0267 - accuracy: 0.9938 - val_loss: 1.4549 - val_accuracy: 0.8930
    Epoch 82/100
    9/9 [==============================] - 11s 1s/step - loss: 0.0892 - accuracy: 0.9911 - val_loss: 1.9719 - val_accuracy: 0.9091
    Epoch 83/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0386 - accuracy: 0.9938 - val_loss: 2.0086 - val_accuracy: 0.8984
    Epoch 84/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0786 - accuracy: 0.9885 - val_loss: 2.1324 - val_accuracy: 0.9091
    Epoch 85/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0950 - accuracy: 0.9929 - val_loss: 2.1849 - val_accuracy: 0.9037
    Epoch 86/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0678 - accuracy: 0.9876 - val_loss: 2.1587 - val_accuracy: 0.9037
    Epoch 87/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0591 - accuracy: 0.9902 - val_loss: 1.8504 - val_accuracy: 0.8984
    Epoch 88/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0442 - accuracy: 0.9938 - val_loss: 1.6815 - val_accuracy: 0.9305
    Epoch 89/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0466 - accuracy: 0.9973 - val_loss: 1.6038 - val_accuracy: 0.9251
    Epoch 90/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0379 - accuracy: 0.9929 - val_loss: 2.1094 - val_accuracy: 0.9358
    Epoch 91/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0378 - accuracy: 0.9965 - val_loss: 2.1745 - val_accuracy: 0.9091
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0682 - accuracy: 0.9947 - val_loss: 2.5571 - val_accuracy: 0.8984
    Epoch 93/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0312 - accuracy: 0.9965 - val_loss: 2.8546 - val_accuracy: 0.8930
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0580 - accuracy: 0.9956 - val_loss: 2.2638 - val_accuracy: 0.9144
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0689 - accuracy: 0.9956 - val_loss: 2.2832 - val_accuracy: 0.9251
    Epoch 96/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0356 - accuracy: 0.9929 - val_loss: 2.1212 - val_accuracy: 0.9305
    Epoch 97/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0944 - accuracy: 0.9947 - val_loss: 2.5851 - val_accuracy: 0.9305
    Epoch 98/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0337 - accuracy: 0.9973 - val_loss: 2.8827 - val_accuracy: 0.9144
    Epoch 99/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0035 - accuracy: 0.9991 - val_loss: 3.0352 - val_accuracy: 0.9091
    Epoch 100/100
    9/9 [==============================] - 10s 1s/step - loss: 0.0093 - accuracy: 0.9991 - val_loss: 2.8240 - val_accuracy: 0.9144



```python
new_model.save_weights(path + '/model.h5')
```


```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```


```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot.pdf")
plt.show()
```


    
![png](VGGINET_woDA_100X_files/VGGINET_woDA_100X_17_0.png)
    



```python
new_model.evaluate(test_ds)
```

    5/5 [==============================] - 21s 4s/step - loss: 3.1577 - accuracy: 0.9134





    [3.1576881408691406, 0.9134275913238525]




```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

    Accuracy:  0.9134275618374559
                  precision    recall  f1-score   support
    
               0       0.88      0.82      0.85       164
               1       0.93      0.95      0.94       402
    
        accuracy                           0.91       566
       macro avg       0.90      0.88      0.89       566
    weighted avg       0.91      0.91      0.91       566
    



```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```


```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```




    <AxesSubplot:title={'center':'Normalized Confusion Matrix'}, xlabel='Predicted label', ylabel='True label'>




    
![png](VGGINET_woDA_100X_files/VGGINET_woDA_100X_21_1.png)
    



    
![png](VGGINET_woDA_100X_files/VGGINET_woDA_100X_21_2.png)
    



```python
plot_roc(y_true, y_probas)
```




    <AxesSubplot:title={'center':'ROC Curves'}, xlabel='False Positive Rate', ylabel='True Positive Rate'>




    
![png](VGGINET_woDA_100X_files/VGGINET_woDA_100X_22_1.png)
    



```python
from imblearn.metrics import classification_report_imbalanced
```


```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.88      0.82      0.85       164
               1       0.93      0.95      0.94       402
    
        accuracy                           0.91       566
       macro avg       0.90      0.88      0.89       566
    weighted avg       0.91      0.91      0.91       566
    
                       pre       rec       spe        f1       geo       iba       sup
    
              0       0.88      0.82      0.95      0.85      0.88      0.77       164
              1       0.93      0.95      0.82      0.94      0.88      0.79       402
    
    avg / total       0.91      0.91      0.86      0.91      0.88      0.78       566
    



```python

```
