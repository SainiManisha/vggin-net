<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/VGG16-NORMAL-RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = '../Splitted_100X/train'
val_path = '../Splitted_100X/val'
test_path = '../Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 2.0956 - accuracy: 0.3125WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 39s 4s/step - loss: 36.5535 - accuracy: 0.5177 - val_loss: 0.7679 - val_accuracy: 0.7112
    Epoch 2/50
    9/9 [==============================] - 24s 3s/step - loss: 0.9449 - accuracy: 0.6356 - val_loss: 0.4714 - val_accuracy: 0.7487
    Epoch 3/50
    9/9 [==============================] - 19s 2s/step - loss: 1.8621 - accuracy: 0.6170 - val_loss: 0.4473 - val_accuracy: 0.8075
    Epoch 4/50
    9/9 [==============================] - 18s 2s/step - loss: 0.9515 - accuracy: 0.7314 - val_loss: 0.5465 - val_accuracy: 0.7594
    Epoch 5/50
    9/9 [==============================] - 20s 2s/step - loss: 0.6451 - accuracy: 0.7793 - val_loss: 0.4275 - val_accuracy: 0.8182
    Epoch 6/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3825 - accuracy: 0.8511 - val_loss: 0.2960 - val_accuracy: 0.9037
    Epoch 7/50
    9/9 [==============================] - 20s 2s/step - loss: 0.3151 - accuracy: 0.8759 - val_loss: 0.2709 - val_accuracy: 0.8984
    Epoch 8/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2949 - accuracy: 0.8874 - val_loss: 0.2680 - val_accuracy: 0.8824
    Epoch 9/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2972 - accuracy: 0.8785 - val_loss: 0.2884 - val_accuracy: 0.8984
    Epoch 10/50
    9/9 [==============================] - 20s 2s/step - loss: 0.2984 - accuracy: 0.8741 - val_loss: 0.2522 - val_accuracy: 0.8930
    Epoch 11/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2497 - accuracy: 0.9051 - val_loss: 0.2178 - val_accuracy: 0.9198
    Epoch 12/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2681 - accuracy: 0.8901 - val_loss: 0.2215 - val_accuracy: 0.9144
    Epoch 13/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2561 - accuracy: 0.8980 - val_loss: 0.2204 - val_accuracy: 0.9251
    Epoch 14/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2903 - accuracy: 0.8794 - val_loss: 0.2261 - val_accuracy: 0.9198
    Epoch 15/50
    9/9 [==============================] - 19s 2s/step - loss: 0.3005 - accuracy: 0.8785 - val_loss: 0.3348 - val_accuracy: 0.8877
    Epoch 16/50
    9/9 [==============================] - 20s 2s/step - loss: 0.3060 - accuracy: 0.8794 - val_loss: 0.2056 - val_accuracy: 0.9305
    Epoch 17/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2621 - accuracy: 0.9025 - val_loss: 0.2498 - val_accuracy: 0.9144
    Epoch 18/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2539 - accuracy: 0.9025 - val_loss: 0.2278 - val_accuracy: 0.9091
    Epoch 19/50
    9/9 [==============================] - 18s 2s/step - loss: 0.2157 - accuracy: 0.9149 - val_loss: 0.2165 - val_accuracy: 0.9198
    Epoch 20/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2143 - accuracy: 0.9149 - val_loss: 0.2819 - val_accuracy: 0.9091
    Epoch 21/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2103 - accuracy: 0.9193 - val_loss: 0.2308 - val_accuracy: 0.9198
    Epoch 22/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1939 - accuracy: 0.9246 - val_loss: 0.2315 - val_accuracy: 0.9144
    Epoch 23/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1892 - accuracy: 0.9344 - val_loss: 0.2321 - val_accuracy: 0.9037
    Epoch 24/50
    9/9 [==============================] - 20s 2s/step - loss: 0.2049 - accuracy: 0.9193 - val_loss: 0.2775 - val_accuracy: 0.9091
    Epoch 25/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2002 - accuracy: 0.9273 - val_loss: 0.2316 - val_accuracy: 0.9144
    Epoch 26/50
    9/9 [==============================] - 19s 2s/step - loss: 0.2090 - accuracy: 0.9140 - val_loss: 0.2250 - val_accuracy: 0.9037
    Epoch 27/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1879 - accuracy: 0.9238 - val_loss: 0.2126 - val_accuracy: 0.9037
    Epoch 28/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1880 - accuracy: 0.9264 - val_loss: 0.2139 - val_accuracy: 0.8984
    Epoch 29/50
    9/9 [==============================] - 22s 2s/step - loss: 0.1725 - accuracy: 0.9273 - val_loss: 0.2201 - val_accuracy: 0.8984
    Epoch 30/50
    9/9 [==============================] - 26s 3s/step - loss: 0.1781 - accuracy: 0.9211 - val_loss: 0.2436 - val_accuracy: 0.9037
    Epoch 31/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1876 - accuracy: 0.9273 - val_loss: 0.2445 - val_accuracy: 0.8984
    Epoch 32/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1687 - accuracy: 0.9344 - val_loss: 0.2508 - val_accuracy: 0.8930
    Epoch 33/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1608 - accuracy: 0.9371 - val_loss: 0.2294 - val_accuracy: 0.9198
    Epoch 34/50
    9/9 [==============================] - 16s 2s/step - loss: 0.1611 - accuracy: 0.9335 - val_loss: 0.2550 - val_accuracy: 0.8984
    Epoch 35/50
    9/9 [==============================] - 21s 2s/step - loss: 0.1853 - accuracy: 0.9149 - val_loss: 0.2430 - val_accuracy: 0.9037
    Epoch 36/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1836 - accuracy: 0.9282 - val_loss: 0.2444 - val_accuracy: 0.9037
    Epoch 37/50
    9/9 [==============================] - 18s 2s/step - loss: 0.1813 - accuracy: 0.9246 - val_loss: 0.2633 - val_accuracy: 0.8877
    Epoch 38/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1688 - accuracy: 0.9397 - val_loss: 0.2454 - val_accuracy: 0.9091
    Epoch 39/50
    9/9 [==============================] - 22s 2s/step - loss: 0.1540 - accuracy: 0.9353 - val_loss: 0.2223 - val_accuracy: 0.9144
    Epoch 40/50
    9/9 [==============================] - 16s 2s/step - loss: 0.1524 - accuracy: 0.9362 - val_loss: 0.2077 - val_accuracy: 0.9198
    Epoch 41/50
    9/9 [==============================] - 23s 3s/step - loss: 0.1469 - accuracy: 0.9441 - val_loss: 0.1930 - val_accuracy: 0.9144
    Epoch 42/50
    9/9 [==============================] - 24s 3s/step - loss: 0.1617 - accuracy: 0.9344 - val_loss: 0.1830 - val_accuracy: 0.9251
    Epoch 43/50
    9/9 [==============================] - 17s 2s/step - loss: 0.1908 - accuracy: 0.9229 - val_loss: 0.1850 - val_accuracy: 0.9305
    Epoch 44/50
    9/9 [==============================] - 26s 3s/step - loss: 0.1403 - accuracy: 0.9450 - val_loss: 0.1732 - val_accuracy: 0.9358
    Epoch 45/50
    9/9 [==============================] - 21s 2s/step - loss: 0.1599 - accuracy: 0.9424 - val_loss: 0.1820 - val_accuracy: 0.9144
    Epoch 46/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1442 - accuracy: 0.9433 - val_loss: 0.1939 - val_accuracy: 0.9198
    Epoch 47/50
    9/9 [==============================] - 16s 2s/step - loss: 0.1460 - accuracy: 0.9433 - val_loss: 0.1740 - val_accuracy: 0.9198
    Epoch 48/50
    9/9 [==============================] - 19s 2s/step - loss: 0.1807 - accuracy: 0.9300 - val_loss: 0.2115 - val_accuracy: 0.9144
    Epoch 49/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1446 - accuracy: 0.9424 - val_loss: 0.2006 - val_accuracy: 0.9144
    Epoch 50/50
    9/9 [==============================] - 20s 2s/step - loss: 0.1318 - accuracy: 0.9441 - val_loss: 0.1899 - val_accuracy: 0.9144

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/207.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 33s 7s/step - loss: 0.1935 - accuracy: 0.9240

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.1934501975774765, 0.9240282773971558]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9240282685512368
                  precision    recall  f1-score   support

               0       0.90      0.83      0.86       164
               1       0.93      0.96      0.95       402

        accuracy                           0.92       566
       macro avg       0.92      0.90      0.91       566
    weighted avg       0.92      0.92      0.92       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/208.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/209.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/210.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.90      0.83      0.86       164
               1       0.93      0.96      0.95       402

        accuracy                           0.92       566
       macro avg       0.92      0.90      0.91       566
    weighted avg       0.92      0.92      0.92       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.90      0.83      0.96      0.86      0.89      0.79       164
              1       0.93      0.96      0.83      0.95      0.89      0.81       402

    avg / total       0.92      0.92      0.87      0.92      0.89      0.80       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
