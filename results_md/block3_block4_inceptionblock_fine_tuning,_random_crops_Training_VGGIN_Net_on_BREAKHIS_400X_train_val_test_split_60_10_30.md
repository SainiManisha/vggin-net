<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK3and4-FINETUNING_400X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_400X/train'
val_path = './Splitted_400X/val'
test_path = './Splitted_400X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.
    Found 161 files belonging to 2 classes.
    Found 488 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/8 [==&gt;...........................] - ETA: 0s - loss: 1.1544 - accuracy: 0.5234WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    8/8 [==============================] - 13s 2s/step - loss: 4.7523 - accuracy: 0.6982 - val_loss: 28.5201 - val_accuracy: 0.6335
    Epoch 2/100
    8/8 [==============================] - 7s 862ms/step - loss: 4.2803 - accuracy: 0.8105 - val_loss: 6.2141 - val_accuracy: 0.8634
    Epoch 3/100
    8/8 [==============================] - 7s 893ms/step - loss: 1.9765 - accuracy: 0.8414 - val_loss: 2.4880 - val_accuracy: 0.9255
    Epoch 4/100
    8/8 [==============================] - 10s 1s/step - loss: 1.6159 - accuracy: 0.8857 - val_loss: 1.8626 - val_accuracy: 0.9068
    Epoch 5/100
    8/8 [==============================] - 7s 920ms/step - loss: 1.5372 - accuracy: 0.8713 - val_loss: 1.6458 - val_accuracy: 0.9068
    Epoch 6/100
    8/8 [==============================] - 7s 906ms/step - loss: 1.2122 - accuracy: 0.8744 - val_loss: 1.3945 - val_accuracy: 0.9130
    Epoch 7/100
    8/8 [==============================] - 7s 918ms/step - loss: 1.3713 - accuracy: 0.8816 - val_loss: 1.9138 - val_accuracy: 0.9130
    Epoch 8/100
    8/8 [==============================] - 7s 863ms/step - loss: 1.2601 - accuracy: 0.8908 - val_loss: 2.4599 - val_accuracy: 0.9068
    Epoch 9/100
    8/8 [==============================] - 7s 869ms/step - loss: 1.1236 - accuracy: 0.8908 - val_loss: 1.1056 - val_accuracy: 0.9379
    Epoch 10/100
    8/8 [==============================] - 7s 879ms/step - loss: 1.0763 - accuracy: 0.8980 - val_loss: 1.4385 - val_accuracy: 0.9255
    Epoch 11/100
    8/8 [==============================] - 11s 1s/step - loss: 1.0484 - accuracy: 0.8929 - val_loss: 1.3051 - val_accuracy: 0.9379
    Epoch 12/100
    8/8 [==============================] - 10s 1s/step - loss: 1.0719 - accuracy: 0.8950 - val_loss: 1.1883 - val_accuracy: 0.8882
    Epoch 13/100
    8/8 [==============================] - 10s 1s/step - loss: 0.9990 - accuracy: 0.9114 - val_loss: 1.2293 - val_accuracy: 0.9068
    Epoch 14/100
    8/8 [==============================] - 7s 918ms/step - loss: 1.0820 - accuracy: 0.9011 - val_loss: 0.8832 - val_accuracy: 0.9379
    Epoch 15/100
    8/8 [==============================] - 11s 1s/step - loss: 0.8397 - accuracy: 0.9135 - val_loss: 0.9930 - val_accuracy: 0.9193
    Epoch 16/100
    8/8 [==============================] - 7s 863ms/step - loss: 0.8238 - accuracy: 0.9186 - val_loss: 0.9803 - val_accuracy: 0.9317
    Epoch 17/100
    8/8 [==============================] - 11s 1s/step - loss: 0.7935 - accuracy: 0.9197 - val_loss: 1.1340 - val_accuracy: 0.9193
    Epoch 18/100
    8/8 [==============================] - 10s 1s/step - loss: 0.7214 - accuracy: 0.9289 - val_loss: 0.9593 - val_accuracy: 0.9379
    Epoch 19/100
    8/8 [==============================] - 10s 1s/step - loss: 0.6881 - accuracy: 0.9238 - val_loss: 0.8816 - val_accuracy: 0.9317
    Epoch 20/100
    8/8 [==============================] - 7s 908ms/step - loss: 0.8471 - accuracy: 0.9238 - val_loss: 1.4440 - val_accuracy: 0.9130
    Epoch 21/100
    8/8 [==============================] - 7s 867ms/step - loss: 0.8313 - accuracy: 0.9289 - val_loss: 1.4998 - val_accuracy: 0.9130
    Epoch 22/100
    8/8 [==============================] - 10s 1s/step - loss: 0.7578 - accuracy: 0.9289 - val_loss: 1.3579 - val_accuracy: 0.9193
    Epoch 23/100
    8/8 [==============================] - 10s 1s/step - loss: 0.8350 - accuracy: 0.9217 - val_loss: 1.5986 - val_accuracy: 0.8944
    Epoch 24/100
    8/8 [==============================] - 7s 914ms/step - loss: 0.9814 - accuracy: 0.9114 - val_loss: 1.9275 - val_accuracy: 0.8882
    Epoch 25/100
    8/8 [==============================] - 7s 871ms/step - loss: 0.7761 - accuracy: 0.9331 - val_loss: 1.6828 - val_accuracy: 0.9130
    Epoch 26/100
    8/8 [==============================] - 7s 922ms/step - loss: 0.8665 - accuracy: 0.9197 - val_loss: 1.3973 - val_accuracy: 0.9193
    Epoch 27/100
    8/8 [==============================] - 7s 919ms/step - loss: 0.8234 - accuracy: 0.9258 - val_loss: 2.3943 - val_accuracy: 0.9006
    Epoch 28/100
    8/8 [==============================] - 7s 882ms/step - loss: 0.8450 - accuracy: 0.9289 - val_loss: 1.4873 - val_accuracy: 0.9317
    Epoch 29/100
    8/8 [==============================] - 7s 860ms/step - loss: 0.9506 - accuracy: 0.9269 - val_loss: 1.4576 - val_accuracy: 0.9317
    Epoch 30/100
    8/8 [==============================] - 11s 1s/step - loss: 0.9480 - accuracy: 0.9217 - val_loss: 1.3559 - val_accuracy: 0.9317
    Epoch 31/100
    8/8 [==============================] - 7s 868ms/step - loss: 0.6178 - accuracy: 0.9434 - val_loss: 1.4890 - val_accuracy: 0.9130
    Epoch 32/100
    8/8 [==============================] - 7s 864ms/step - loss: 0.8966 - accuracy: 0.9310 - val_loss: 1.4375 - val_accuracy: 0.9441
    Epoch 33/100
    8/8 [==============================] - 7s 867ms/step - loss: 1.1384 - accuracy: 0.9341 - val_loss: 1.4628 - val_accuracy: 0.9130
    Epoch 34/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4804 - accuracy: 0.9588 - val_loss: 1.2905 - val_accuracy: 0.9255
    Epoch 35/100
    8/8 [==============================] - 10s 1s/step - loss: 0.8291 - accuracy: 0.9289 - val_loss: 1.2065 - val_accuracy: 0.9255
    Epoch 36/100
    8/8 [==============================] - 7s 870ms/step - loss: 0.7133 - accuracy: 0.9413 - val_loss: 1.7967 - val_accuracy: 0.9193
    Epoch 37/100
    8/8 [==============================] - 11s 1s/step - loss: 0.6448 - accuracy: 0.9444 - val_loss: 0.9586 - val_accuracy: 0.9503
    Epoch 38/100
    8/8 [==============================] - 10s 1s/step - loss: 0.9775 - accuracy: 0.9403 - val_loss: 2.2713 - val_accuracy: 0.8696
    Epoch 39/100
    8/8 [==============================] - 10s 1s/step - loss: 0.7325 - accuracy: 0.9341 - val_loss: 1.8514 - val_accuracy: 0.8820
    Epoch 40/100
    8/8 [==============================] - 7s 858ms/step - loss: 0.4813 - accuracy: 0.9619 - val_loss: 1.6778 - val_accuracy: 0.9068
    Epoch 41/100
    8/8 [==============================] - 10s 1s/step - loss: 1.0057 - accuracy: 0.9238 - val_loss: 1.5337 - val_accuracy: 0.9193
    Epoch 42/100
    8/8 [==============================] - 7s 867ms/step - loss: 1.1132 - accuracy: 0.9176 - val_loss: 1.4571 - val_accuracy: 0.9317
    Epoch 43/100
    8/8 [==============================] - 10s 1s/step - loss: 0.8308 - accuracy: 0.9516 - val_loss: 1.5717 - val_accuracy: 0.9565
    Epoch 44/100
    8/8 [==============================] - 7s 871ms/step - loss: 0.5870 - accuracy: 0.9526 - val_loss: 1.3921 - val_accuracy: 0.9317
    Epoch 45/100
    8/8 [==============================] - 7s 855ms/step - loss: 0.4399 - accuracy: 0.9588 - val_loss: 1.3260 - val_accuracy: 0.9193
    Epoch 46/100
    8/8 [==============================] - 7s 911ms/step - loss: 0.6501 - accuracy: 0.9557 - val_loss: 1.2547 - val_accuracy: 0.9193
    Epoch 47/100
    8/8 [==============================] - 10s 1s/step - loss: 0.5333 - accuracy: 0.9475 - val_loss: 1.8445 - val_accuracy: 0.9379
    Epoch 48/100
    8/8 [==============================] - 10s 1s/step - loss: 0.5515 - accuracy: 0.9547 - val_loss: 1.3800 - val_accuracy: 0.9317
    Epoch 49/100
    8/8 [==============================] - 10s 1s/step - loss: 0.6574 - accuracy: 0.9516 - val_loss: 1.2892 - val_accuracy: 0.9255
    Epoch 50/100
    8/8 [==============================] - 7s 853ms/step - loss: 0.4351 - accuracy: 0.9650 - val_loss: 1.5759 - val_accuracy: 0.9255
    Epoch 51/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3999 - accuracy: 0.9640 - val_loss: 1.7724 - val_accuracy: 0.9379
    Epoch 52/100
    8/8 [==============================] - 7s 862ms/step - loss: 0.4827 - accuracy: 0.9660 - val_loss: 1.3449 - val_accuracy: 0.9379
    Epoch 53/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5001 - accuracy: 0.9567 - val_loss: 1.6506 - val_accuracy: 0.9130
    Epoch 54/100
    8/8 [==============================] - 8s 941ms/step - loss: 0.5277 - accuracy: 0.9650 - val_loss: 1.6313 - val_accuracy: 0.9255
    Epoch 55/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5344 - accuracy: 0.9557 - val_loss: 1.6228 - val_accuracy: 0.9006
    Epoch 56/100
    8/8 [==============================] - 18s 2s/step - loss: 0.4831 - accuracy: 0.9650 - val_loss: 1.6136 - val_accuracy: 0.9193
    Epoch 57/100
    8/8 [==============================] - 12s 1s/step - loss: 0.4299 - accuracy: 0.9598 - val_loss: 1.1058 - val_accuracy: 0.9441
    Epoch 58/100
    8/8 [==============================] - 10s 1s/step - loss: 0.5241 - accuracy: 0.9506 - val_loss: 1.4191 - val_accuracy: 0.9255
    Epoch 59/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4972 - accuracy: 0.9578 - val_loss: 1.6639 - val_accuracy: 0.9130
    Epoch 60/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4271 - accuracy: 0.9701 - val_loss: 2.0618 - val_accuracy: 0.9255
    Epoch 61/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4305 - accuracy: 0.9660 - val_loss: 1.4248 - val_accuracy: 0.9130
    Epoch 62/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4460 - accuracy: 0.9557 - val_loss: 1.7112 - val_accuracy: 0.9317
    Epoch 63/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4851 - accuracy: 0.9629 - val_loss: 1.5973 - val_accuracy: 0.9565
    Epoch 64/100
    8/8 [==============================] - 8s 938ms/step - loss: 0.3929 - accuracy: 0.9640 - val_loss: 2.4433 - val_accuracy: 0.9317
    Epoch 65/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3144 - accuracy: 0.9701 - val_loss: 4.0476 - val_accuracy: 0.8696
    Epoch 66/100
    8/8 [==============================] - 11s 1s/step - loss: 0.5305 - accuracy: 0.9629 - val_loss: 2.9383 - val_accuracy: 0.9006
    Epoch 67/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3275 - accuracy: 0.9732 - val_loss: 1.5377 - val_accuracy: 0.9627
    Epoch 68/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4025 - accuracy: 0.9660 - val_loss: 1.7976 - val_accuracy: 0.9317
    Epoch 69/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4901 - accuracy: 0.9701 - val_loss: 2.9615 - val_accuracy: 0.9068
    Epoch 70/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3831 - accuracy: 0.9681 - val_loss: 2.2706 - val_accuracy: 0.9441
    Epoch 71/100
    8/8 [==============================] - 12s 2s/step - loss: 0.4632 - accuracy: 0.9598 - val_loss: 2.7999 - val_accuracy: 0.9379
    Epoch 72/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4275 - accuracy: 0.9598 - val_loss: 2.8867 - val_accuracy: 0.9193
    Epoch 73/100
    8/8 [==============================] - 7s 886ms/step - loss: 0.6538 - accuracy: 0.9588 - val_loss: 2.4687 - val_accuracy: 0.9255
    Epoch 74/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3011 - accuracy: 0.9784 - val_loss: 2.2452 - val_accuracy: 0.9441
    Epoch 75/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4568 - accuracy: 0.9712 - val_loss: 2.8622 - val_accuracy: 0.9317
    Epoch 76/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4653 - accuracy: 0.9650 - val_loss: 2.7048 - val_accuracy: 0.9130
    Epoch 77/100
    8/8 [==============================] - 10s 1s/step - loss: 0.2735 - accuracy: 0.9712 - val_loss: 2.0524 - val_accuracy: 0.9379
    Epoch 78/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3757 - accuracy: 0.9701 - val_loss: 1.8914 - val_accuracy: 0.9503
    Epoch 79/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3189 - accuracy: 0.9722 - val_loss: 2.0985 - val_accuracy: 0.9255
    Epoch 80/100
    8/8 [==============================] - 10s 1s/step - loss: 0.2440 - accuracy: 0.9794 - val_loss: 2.3010 - val_accuracy: 0.9130
    Epoch 81/100
    8/8 [==============================] - 7s 872ms/step - loss: 0.4099 - accuracy: 0.9681 - val_loss: 2.0399 - val_accuracy: 0.9503
    Epoch 82/100
    8/8 [==============================] - 7s 897ms/step - loss: 0.4777 - accuracy: 0.9691 - val_loss: 2.6425 - val_accuracy: 0.9068
    Epoch 83/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3657 - accuracy: 0.9732 - val_loss: 1.7629 - val_accuracy: 0.9193
    Epoch 84/100
    8/8 [==============================] - 8s 967ms/step - loss: 0.4349 - accuracy: 0.9640 - val_loss: 3.1259 - val_accuracy: 0.9068
    Epoch 85/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3254 - accuracy: 0.9773 - val_loss: 2.0821 - val_accuracy: 0.9379
    Epoch 86/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2988 - accuracy: 0.9732 - val_loss: 3.2498 - val_accuracy: 0.9130
    Epoch 87/100
    8/8 [==============================] - 10s 1s/step - loss: 0.4860 - accuracy: 0.9712 - val_loss: 2.7696 - val_accuracy: 0.9068
    Epoch 88/100
    8/8 [==============================] - 8s 1s/step - loss: 0.4796 - accuracy: 0.9650 - val_loss: 2.2703 - val_accuracy: 0.9503
    Epoch 89/100
    8/8 [==============================] - 10s 1s/step - loss: 0.3275 - accuracy: 0.9763 - val_loss: 2.6122 - val_accuracy: 0.9130
    Epoch 90/100
    8/8 [==============================] - 10s 1s/step - loss: 0.5445 - accuracy: 0.9578 - val_loss: 2.8880 - val_accuracy: 0.9379
    Epoch 91/100
    8/8 [==============================] - 10s 1s/step - loss: 0.2804 - accuracy: 0.9825 - val_loss: 3.0752 - val_accuracy: 0.9441
    Epoch 92/100
    8/8 [==============================] - 11s 1s/step - loss: 0.2144 - accuracy: 0.9835 - val_loss: 1.5941 - val_accuracy: 0.9441
    Epoch 93/100
    8/8 [==============================] - 11s 1s/step - loss: 0.3363 - accuracy: 0.9722 - val_loss: 1.6847 - val_accuracy: 0.9379
    Epoch 94/100
    8/8 [==============================] - 10s 1s/step - loss: 0.2754 - accuracy: 0.9794 - val_loss: 2.1142 - val_accuracy: 0.9565
    Epoch 95/100
    8/8 [==============================] - 11s 1s/step - loss: 0.1155 - accuracy: 0.9887 - val_loss: 2.8186 - val_accuracy: 0.9379
    Epoch 96/100
    8/8 [==============================] - 11s 1s/step - loss: 0.1953 - accuracy: 0.9804 - val_loss: 3.4943 - val_accuracy: 0.9193
    Epoch 97/100
    8/8 [==============================] - 7s 878ms/step - loss: 0.3252 - accuracy: 0.9804 - val_loss: 2.8650 - val_accuracy: 0.9379
    Epoch 98/100
    8/8 [==============================] - 8s 946ms/step - loss: 0.2927 - accuracy: 0.9815 - val_loss: 1.9406 - val_accuracy: 0.9689
    Epoch 99/100
    8/8 [==============================] - 10s 1s/step - loss: 0.2473 - accuracy: 0.9856 - val_loss: 2.2107 - val_accuracy: 0.9503
    Epoch 100/100
    8/8 [==============================] - 11s 1s/step - loss: 0.4677 - accuracy: 0.9732 - val_loss: 2.2159 - val_accuracy: 0.9441

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/1.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 13s 3s/step - loss: 1.6107 - accuracy: 0.9447

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.6107115745544434, 0.9446721076965332]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,697,506
    Non-trainable params: 261,632
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/2.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 128, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 971 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2140 - accuracy: 0.9825 - val_loss: 1.9862 - val_accuracy: 0.9317

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1646 - accuracy: 0.9866 - val_loss: 1.7113 - val_accuracy: 0.9503

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1727 - accuracy: 0.9815 - val_loss: 2.3010 - val_accuracy: 0.8758

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3539 - accuracy: 0.9794 - val_loss: 2.1927 - val_accuracy: 0.8944

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2593 - accuracy: 0.9835 - val_loss: 1.9144 - val_accuracy: 0.9379

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2907 - accuracy: 0.9773 - val_loss: 2.8142 - val_accuracy: 0.9193

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3112 - accuracy: 0.9804 - val_loss: 2.1883 - val_accuracy: 0.9255

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3648 - accuracy: 0.9773 - val_loss: 1.9043 - val_accuracy: 0.9068

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1935 - accuracy: 0.9815 - val_loss: 1.9061 - val_accuracy: 0.9255

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1549 - accuracy: 0.9866 - val_loss: 2.2795 - val_accuracy: 0.9317

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    8/8 [==============================] - 10s 1s/step - loss: 0.3625 - accuracy: 0.9794 - val_loss: 4.0894 - val_accuracy: 0.9255

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2092 - accuracy: 0.9835 - val_loss: 1.9740 - val_accuracy: 0.9627

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1958 - accuracy: 0.9815 - val_loss: 2.2174 - val_accuracy: 0.9379

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3232 - accuracy: 0.9753 - val_loss: 4.0355 - val_accuracy: 0.8820

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3190 - accuracy: 0.9753 - val_loss: 3.5486 - val_accuracy: 0.9006

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    8/8 [==============================] - 10s 1s/step - loss: 0.2444 - accuracy: 0.9815 - val_loss: 2.2013 - val_accuracy: 0.9317

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    8/8 [==============================] - 14s 2s/step - loss: 0.2258 - accuracy: 0.9835 - val_loss: 2.5834 - val_accuracy: 0.9565

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    8/8 [==============================] - 13s 2s/step - loss: 0.3134 - accuracy: 0.9773 - val_loss: 1.4737 - val_accuracy: 0.9503

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1277 - accuracy: 0.9897 - val_loss: 1.2240 - val_accuracy: 0.9627

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1750 - accuracy: 0.9856 - val_loss: 2.1569 - val_accuracy: 0.9379

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1519 - accuracy: 0.9856 - val_loss: 1.7966 - val_accuracy: 0.9565

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2125 - accuracy: 0.9866 - val_loss: 1.6140 - val_accuracy: 0.9565

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1036 - accuracy: 0.9897 - val_loss: 1.4293 - val_accuracy: 0.9503

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1327 - accuracy: 0.9866 - val_loss: 1.4771 - val_accuracy: 0.9565

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1202 - accuracy: 0.9876 - val_loss: 1.8767 - val_accuracy: 0.9503

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    8/8 [==============================] - 16s 2s/step - loss: 0.1550 - accuracy: 0.9876 - val_loss: 1.6769 - val_accuracy: 0.9565

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1762 - accuracy: 0.9907 - val_loss: 1.7489 - val_accuracy: 0.9441

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2208 - accuracy: 0.9846 - val_loss: 2.2672 - val_accuracy: 0.9379

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0916 - accuracy: 0.9876 - val_loss: 2.5084 - val_accuracy: 0.9317

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2609 - accuracy: 0.9876 - val_loss: 2.6342 - val_accuracy: 0.9068

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    8/8 [==============================] - 10s 1s/step - loss: 0.1453 - accuracy: 0.9856 - val_loss: 2.7181 - val_accuracy: 0.9068

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2441 - accuracy: 0.9815 - val_loss: 1.8315 - val_accuracy: 0.9441

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2348 - accuracy: 0.9897 - val_loss: 1.9209 - val_accuracy: 0.9441

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    8/8 [==============================] - 10s 1s/step - loss: 0.2522 - accuracy: 0.9835 - val_loss: 2.1995 - val_accuracy: 0.9441

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1179 - accuracy: 0.9897 - val_loss: 2.0028 - val_accuracy: 0.9379

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1117 - accuracy: 0.9907 - val_loss: 1.8907 - val_accuracy: 0.9441

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1352 - accuracy: 0.9866 - val_loss: 1.6956 - val_accuracy: 0.9379

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0501 - accuracy: 0.9928 - val_loss: 1.4993 - val_accuracy: 0.9565

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    8/8 [==============================] - 12s 2s/step - loss: 0.2284 - accuracy: 0.9876 - val_loss: 1.5513 - val_accuracy: 0.9379

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0516 - accuracy: 0.9876 - val_loss: 1.4067 - val_accuracy: 0.9441

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1467 - accuracy: 0.9928 - val_loss: 1.5830 - val_accuracy: 0.9503

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    8/8 [==============================] - 13s 2s/step - loss: 0.0831 - accuracy: 0.9938 - val_loss: 1.8486 - val_accuracy: 0.9565

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2239 - accuracy: 0.9856 - val_loss: 1.3176 - val_accuracy: 0.9441

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    8/8 [==============================] - 15s 2s/step - loss: 0.1915 - accuracy: 0.9887 - val_loss: 1.2789 - val_accuracy: 0.9565

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    8/8 [==============================] - 12s 2s/step - loss: 0.1229 - accuracy: 0.9897 - val_loss: 1.4036 - val_accuracy: 0.9565

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2473 - accuracy: 0.9856 - val_loss: 1.4968 - val_accuracy: 0.9565

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    8/8 [==============================] - 13s 2s/step - loss: 0.2595 - accuracy: 0.9866 - val_loss: 1.5456 - val_accuracy: 0.9503

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1093 - accuracy: 0.9846 - val_loss: 2.0789 - val_accuracy: 0.9565

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    8/8 [==============================] - 13s 2s/step - loss: 0.1421 - accuracy: 0.9866 - val_loss: 2.3326 - val_accuracy: 0.9503

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    8/8 [==============================] - 12s 2s/step - loss: 0.0378 - accuracy: 0.9949 - val_loss: 2.2238 - val_accuracy: 0.9627

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/3.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    4/4 [==============================] - 4s 1s/step - loss: 1.7796 - accuracy: 0.9426

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.7796080112457275, 0.9426229596138]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9426229508196722
                  precision    recall  f1-score   support

               0       0.93      0.87      0.90       148
               1       0.95      0.97      0.96       340

        accuracy                           0.94       488
       macro avg       0.94      0.92      0.93       488
    weighted avg       0.94      0.94      0.94       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title confusion="" matrix="" xlabel="Predicted label" ylabel="True label">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/4.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/5.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<axessubplot:title curves="" xlabel="False Positive Rate" ylabel="True Positive Rate">
</axessubplot:title></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/6.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.93      0.87      0.90       148
               1       0.95      0.97      0.96       340

        accuracy                           0.94       488
       macro avg       0.94      0.92      0.93       488
    weighted avg       0.94      0.94      0.94       488

                       pre       rec       spe        f1       geo       iba       sup

              0       0.93      0.87      0.97      0.90      0.92      0.84       148
              1       0.95      0.97      0.87      0.96      0.92      0.86       340

    avg / total       0.94      0.94      0.90      0.94      0.92      0.85       488

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
