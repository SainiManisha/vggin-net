<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
import tensorflow as tf
import tensorflow_hub as hub

NUM_CLASSES = 2

module_url = 'https://tfhub.dev/google/imagenet/inception_v1/feature_vector/1'
feature_vector = hub.KerasLayer(module_url, trainable=False, name='inception_v1')

model = tf.keras.Sequential([
    tf.keras.Input([224, 224, 3]),
    feature_vector,
    tf.keras.layers.Dropout(rate=0.4, name='dropout'),
    tf.keras.layers.Dense(2, activation='softmax', name='predictions')
])
model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    inception_v1 (KerasLayer)    (None, 1024)              5607184   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 2050      
    =================================================================
    Total params: 5,609,234
    Trainable params: 2,050
    Non-trainable params: 5,607,184
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model  = model
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'experiments/InceptionV1-NORMAL-RANDOMCROP_100X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.
# All of TensorFlow Hub's image modules expect float inputs in the [0, 1] range.

preprocess = lambda x, y: (x / 255., y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_100X/train'
val_path = './Splitted_100X/val'
test_path = './Splitted_100X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1128 files belonging to 2 classes.
    Found 187 files belonging to 2 classes.
    Found 566 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 0.8258 - accuracy: 0.5078WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    WARNING:tensorflow:From /opt/conda/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    9/9 [==============================] - 24s 3s/step - loss: 0.6576 - accuracy: 0.6684 - val_loss: 0.5231 - val_accuracy: 0.7112
    Epoch 2/50
    9/9 [==============================] - 13s 1s/step - loss: 0.6080 - accuracy: 0.6800 - val_loss: 0.4830 - val_accuracy: 0.7326
    Epoch 3/50
    9/9 [==============================] - 13s 1s/step - loss: 0.5552 - accuracy: 0.7199 - val_loss: 0.4550 - val_accuracy: 0.7540
    Epoch 4/50
    9/9 [==============================] - 13s 1s/step - loss: 0.5373 - accuracy: 0.7473 - val_loss: 0.4326 - val_accuracy: 0.7754
    Epoch 5/50
    9/9 [==============================] - 13s 1s/step - loss: 0.5043 - accuracy: 0.7642 - val_loss: 0.4331 - val_accuracy: 0.7968
    Epoch 6/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4925 - accuracy: 0.7801 - val_loss: 0.4274 - val_accuracy: 0.7861
    Epoch 7/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4785 - accuracy: 0.7757 - val_loss: 0.4165 - val_accuracy: 0.7968
    Epoch 8/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4706 - accuracy: 0.7810 - val_loss: 0.4133 - val_accuracy: 0.8021
    Epoch 9/50
    9/9 [==============================] - 14s 2s/step - loss: 0.4536 - accuracy: 0.7979 - val_loss: 0.4002 - val_accuracy: 0.8075
    Epoch 10/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4501 - accuracy: 0.7872 - val_loss: 0.3994 - val_accuracy: 0.8182
    Epoch 11/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4221 - accuracy: 0.8147 - val_loss: 0.3933 - val_accuracy: 0.8182
    Epoch 12/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4215 - accuracy: 0.8059 - val_loss: 0.3851 - val_accuracy: 0.8235
    Epoch 13/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4147 - accuracy: 0.8156 - val_loss: 0.4089 - val_accuracy: 0.8289
    Epoch 14/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4131 - accuracy: 0.8200 - val_loss: 0.3722 - val_accuracy: 0.8289
    Epoch 15/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4225 - accuracy: 0.8076 - val_loss: 0.3669 - val_accuracy: 0.8235
    Epoch 16/50
    9/9 [==============================] - 13s 1s/step - loss: 0.4009 - accuracy: 0.8129 - val_loss: 0.3763 - val_accuracy: 0.8342
    Epoch 17/50
    9/9 [==============================] - 9s 1s/step - loss: 0.3934 - accuracy: 0.8271 - val_loss: 0.3668 - val_accuracy: 0.8342
    Epoch 18/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3879 - accuracy: 0.8324 - val_loss: 0.3798 - val_accuracy: 0.8396
    Epoch 19/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3863 - accuracy: 0.8271 - val_loss: 0.3661 - val_accuracy: 0.8449
    Epoch 20/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3786 - accuracy: 0.8342 - val_loss: 0.3739 - val_accuracy: 0.8449
    Epoch 21/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3801 - accuracy: 0.8378 - val_loss: 0.3622 - val_accuracy: 0.8503
    Epoch 22/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3825 - accuracy: 0.8457 - val_loss: 0.3316 - val_accuracy: 0.8396
    Epoch 23/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3806 - accuracy: 0.8369 - val_loss: 0.3801 - val_accuracy: 0.8449
    Epoch 24/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3750 - accuracy: 0.8431 - val_loss: 0.3417 - val_accuracy: 0.8556
    Epoch 25/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3804 - accuracy: 0.8422 - val_loss: 0.3739 - val_accuracy: 0.8503
    Epoch 26/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3547 - accuracy: 0.8449 - val_loss: 0.3309 - val_accuracy: 0.8556
    Epoch 27/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3630 - accuracy: 0.8351 - val_loss: 0.3645 - val_accuracy: 0.8503
    Epoch 28/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3490 - accuracy: 0.8599 - val_loss: 0.3645 - val_accuracy: 0.8503
    Epoch 29/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3642 - accuracy: 0.8520 - val_loss: 0.3371 - val_accuracy: 0.8717
    Epoch 30/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3450 - accuracy: 0.8475 - val_loss: 0.3602 - val_accuracy: 0.8449
    Epoch 31/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3518 - accuracy: 0.8528 - val_loss: 0.3455 - val_accuracy: 0.8610
    Epoch 32/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3375 - accuracy: 0.8626 - val_loss: 0.3432 - val_accuracy: 0.8610
    Epoch 33/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3675 - accuracy: 0.8404 - val_loss: 0.3684 - val_accuracy: 0.8449
    Epoch 34/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3507 - accuracy: 0.8449 - val_loss: 0.3321 - val_accuracy: 0.8663
    Epoch 35/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3354 - accuracy: 0.8626 - val_loss: 0.3153 - val_accuracy: 0.8717
    Epoch 36/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3526 - accuracy: 0.8590 - val_loss: 0.3405 - val_accuracy: 0.8610
    Epoch 37/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3652 - accuracy: 0.8395 - val_loss: 0.3049 - val_accuracy: 0.8610
    Epoch 38/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3303 - accuracy: 0.8564 - val_loss: 0.3183 - val_accuracy: 0.8717
    Epoch 39/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3691 - accuracy: 0.8395 - val_loss: 0.3403 - val_accuracy: 0.8717
    Epoch 40/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3491 - accuracy: 0.8537 - val_loss: 0.3032 - val_accuracy: 0.8824
    Epoch 41/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3605 - accuracy: 0.8369 - val_loss: 0.3545 - val_accuracy: 0.8503
    Epoch 42/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3390 - accuracy: 0.8608 - val_loss: 0.3027 - val_accuracy: 0.8770
    Epoch 43/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3388 - accuracy: 0.8582 - val_loss: 0.3266 - val_accuracy: 0.8663
    Epoch 44/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3427 - accuracy: 0.8440 - val_loss: 0.3332 - val_accuracy: 0.8503
    Epoch 45/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3278 - accuracy: 0.8652 - val_loss: 0.3124 - val_accuracy: 0.8717
    Epoch 46/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3439 - accuracy: 0.8502 - val_loss: 0.3256 - val_accuracy: 0.8663
    Epoch 47/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3294 - accuracy: 0.8706 - val_loss: 0.3099 - val_accuracy: 0.8556
    Epoch 48/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3320 - accuracy: 0.8706 - val_loss: 0.3048 - val_accuracy: 0.8717
    Epoch 49/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3441 - accuracy: 0.8537 - val_loss: 0.3300 - val_accuracy: 0.8556
    Epoch 50/50
    9/9 [==============================] - 13s 1s/step - loss: 0.3351 - accuracy: 0.8546 - val_loss: 0.3014 - val_accuracy: 0.8824

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/181.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 30s 6s/step - loss: 0.3261 - accuracy: 0.8675

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[16\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.3261060118675232, 0.8674911856651306]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8674911660777385
                  precision    recall  f1-score   support

               0       0.75      0.80      0.78       164
               1       0.92      0.89      0.91       402

        accuracy                           0.87       566
       macro avg       0.84      0.85      0.84       566
    weighted avg       0.87      0.87      0.87       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in /opt/conda/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.14.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.22.2.post1)
    Requirement already satisfied: scipy&gt;=0.9 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.1)
    Requirement already satisfied: numpy&gt;=1.11.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (1.19.2)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: six in /opt/conda/lib/python3.7/site-packages (from cycler&gt;=0.10-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.14.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/182.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/183.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/184.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in /opt/conda/lib/python3.7/site-packages (0.6.2)
    Requirement already satisfied: numpy&gt;=1.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.2)
    Requirement already satisfied: scipy&gt;=0.17 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.22 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.22.2.post1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.14.1)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stderr output_text">

    Using TensorFlow backend.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.75      0.80      0.78       164
               1       0.92      0.89      0.91       402

        accuracy                           0.87       566
       macro avg       0.84      0.85      0.84       566
    weighted avg       0.87      0.87      0.87       566

                       pre       rec       spe        f1       geo       iba       sup

              0       0.75      0.80      0.89      0.78      0.85      0.71       164
              1       0.92      0.89      0.80      0.91      0.85      0.73       402

    avg / total       0.87      0.87      0.83      0.87      0.85      0.72       566

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
