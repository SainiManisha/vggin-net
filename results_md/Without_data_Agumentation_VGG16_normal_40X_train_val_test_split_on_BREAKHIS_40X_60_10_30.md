<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras

NUM_CLASSES = 2

plain_vgg16 = keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

# capture output of FC2 layer
out = plain_vgg16.get_layer('fc2').output
# add a new Dense layer for predictions
output = keras.layers.Dense(NUM_CLASSES, activation='softmax', name='predictions')(out)
# create the model
new_model = keras.models.Model(inputs=plain_vgg16.input, outputs=output)

# freeze the initial layers (feature extraction layers) of the model
for layer in new_model.layers[:-3]:
    layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 25088)             0         
    _________________________________________________________________
    fc1 (Dense)                  (None, 4096)              102764544 
    _________________________________________________________________
    fc2 (Dense)                  (None, 4096)              16781312  
    _________________________________________________________________
    predictions (Dense)          (None, 2)                 8194      
    =================================================================
    Total params: 134,268,738
    Trainable params: 119,554,050
    Non-trainable params: 14,714,688
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'WithoutdataAgumentation_experiments/VGG16-NORMAL-RANDOMCROP_40X-BREAKHIS-Dataset-60-10-30-VGGINet'
!mkdir -p {name}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(name + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.python.keras.applications.imagenet_utils import _preprocess_symbolic_input as pa
preprocess = lambda x, y: (pa(x, data_format='channels_last', mode='caffe'), y)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 224)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    #if train:
        #ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    #ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.map(preprocess)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
train_history = new_model.fit(train_ds, epochs=epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/50
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.1844 - accuracy: 0.3359WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 14s 2s/step - loss: 30.8239 - accuracy: 0.5422 - val_loss: 1.5984 - val_accuracy: 0.7095
    Epoch 2/50
    9/9 [==============================] - 7s 726ms/step - loss: 1.2447 - accuracy: 0.6741 - val_loss: 0.4614 - val_accuracy: 0.7877
    Epoch 3/50
    9/9 [==============================] - 7s 732ms/step - loss: 0.5332 - accuracy: 0.8199 - val_loss: 0.6751 - val_accuracy: 0.7542
    Epoch 4/50
    9/9 [==============================] - 7s 727ms/step - loss: 0.2331 - accuracy: 0.9025 - val_loss: 0.3903 - val_accuracy: 0.8492
    Epoch 5/50
    9/9 [==============================] - 6s 719ms/step - loss: 0.1161 - accuracy: 0.9517 - val_loss: 0.3654 - val_accuracy: 0.8771
    Epoch 6/50
    9/9 [==============================] - 6s 718ms/step - loss: 0.0457 - accuracy: 0.9861 - val_loss: 0.3228 - val_accuracy: 0.8939
    Epoch 7/50
    9/9 [==============================] - 6s 719ms/step - loss: 0.0328 - accuracy: 0.9889 - val_loss: 0.3753 - val_accuracy: 0.8547
    Epoch 8/50
    9/9 [==============================] - 7s 775ms/step - loss: 0.0137 - accuracy: 0.9963 - val_loss: 0.4786 - val_accuracy: 0.8603
    Epoch 9/50
    9/9 [==============================] - 7s 734ms/step - loss: 0.0040 - accuracy: 0.9991 - val_loss: 0.3557 - val_accuracy: 0.8827
    Epoch 10/50
    9/9 [==============================] - 6s 722ms/step - loss: 0.0021 - accuracy: 1.0000 - val_loss: 0.4511 - val_accuracy: 0.8771
    Epoch 11/50
    9/9 [==============================] - 7s 723ms/step - loss: 0.0012 - accuracy: 1.0000 - val_loss: 0.3240 - val_accuracy: 0.8994
    Epoch 12/50
    9/9 [==============================] - 7s 730ms/step - loss: 5.4062e-04 - accuracy: 1.0000 - val_loss: 0.3325 - val_accuracy: 0.8939
    Epoch 13/50
    9/9 [==============================] - 7s 759ms/step - loss: 3.2846e-04 - accuracy: 1.0000 - val_loss: 0.3671 - val_accuracy: 0.8994
    Epoch 14/50
    9/9 [==============================] - 6s 719ms/step - loss: 2.5404e-04 - accuracy: 1.0000 - val_loss: 0.3776 - val_accuracy: 0.8939
    Epoch 15/50
    9/9 [==============================] - 6s 722ms/step - loss: 2.0137e-04 - accuracy: 1.0000 - val_loss: 0.3677 - val_accuracy: 0.8939
    Epoch 16/50
    9/9 [==============================] - 6s 721ms/step - loss: 1.5835e-04 - accuracy: 1.0000 - val_loss: 0.3602 - val_accuracy: 0.8883
    Epoch 17/50
    9/9 [==============================] - 6s 714ms/step - loss: 1.2008e-04 - accuracy: 1.0000 - val_loss: 0.3769 - val_accuracy: 0.8883
    Epoch 18/50
    9/9 [==============================] - 7s 727ms/step - loss: 8.8493e-05 - accuracy: 1.0000 - val_loss: 0.3744 - val_accuracy: 0.8883
    Epoch 19/50
    9/9 [==============================] - 7s 722ms/step - loss: 6.2723e-05 - accuracy: 1.0000 - val_loss: 0.3734 - val_accuracy: 0.8939
    Epoch 20/50
    9/9 [==============================] - 6s 721ms/step - loss: 4.3184e-05 - accuracy: 1.0000 - val_loss: 0.4018 - val_accuracy: 0.8827
    Epoch 21/50
    9/9 [==============================] - 6s 721ms/step - loss: 3.2481e-05 - accuracy: 1.0000 - val_loss: 0.3870 - val_accuracy: 0.8883
    Epoch 22/50
    9/9 [==============================] - 7s 724ms/step - loss: 2.4243e-05 - accuracy: 1.0000 - val_loss: 0.3975 - val_accuracy: 0.8883
    Epoch 23/50
    9/9 [==============================] - 7s 728ms/step - loss: 1.6062e-05 - accuracy: 1.0000 - val_loss: 0.4209 - val_accuracy: 0.8883
    Epoch 24/50
    9/9 [==============================] - 7s 723ms/step - loss: 1.1403e-05 - accuracy: 1.0000 - val_loss: 0.4631 - val_accuracy: 0.8771
    Epoch 25/50
    9/9 [==============================] - 7s 743ms/step - loss: 8.9024e-06 - accuracy: 1.0000 - val_loss: 0.4146 - val_accuracy: 0.8939
    Epoch 26/50
    9/9 [==============================] - 7s 723ms/step - loss: 5.9300e-06 - accuracy: 1.0000 - val_loss: 0.4659 - val_accuracy: 0.8827
    Epoch 27/50
    9/9 [==============================] - 7s 729ms/step - loss: 4.7811e-06 - accuracy: 1.0000 - val_loss: 0.4518 - val_accuracy: 0.8883
    Epoch 28/50
    9/9 [==============================] - 7s 743ms/step - loss: 3.7126e-06 - accuracy: 1.0000 - val_loss: 0.4626 - val_accuracy: 0.8883
    Epoch 29/50
    9/9 [==============================] - 6s 719ms/step - loss: 3.0596e-06 - accuracy: 1.0000 - val_loss: 0.4731 - val_accuracy: 0.8883
    Epoch 30/50
    9/9 [==============================] - 7s 726ms/step - loss: 2.6097e-06 - accuracy: 1.0000 - val_loss: 0.4794 - val_accuracy: 0.8883
    Epoch 31/50
    9/9 [==============================] - 7s 729ms/step - loss: 2.2735e-06 - accuracy: 1.0000 - val_loss: 0.4810 - val_accuracy: 0.8883
    Epoch 32/50
    9/9 [==============================] - 7s 727ms/step - loss: 1.9620e-06 - accuracy: 1.0000 - val_loss: 0.4854 - val_accuracy: 0.8883
    Epoch 33/50
    9/9 [==============================] - 6s 721ms/step - loss: 1.7535e-06 - accuracy: 1.0000 - val_loss: 0.4914 - val_accuracy: 0.8883
    Epoch 34/50
    9/9 [==============================] - 7s 727ms/step - loss: 1.5692e-06 - accuracy: 1.0000 - val_loss: 0.4914 - val_accuracy: 0.8883
    Epoch 35/50
    9/9 [==============================] - 6s 718ms/step - loss: 1.3945e-06 - accuracy: 1.0000 - val_loss: 0.5036 - val_accuracy: 0.8883
    Epoch 36/50
    9/9 [==============================] - 7s 724ms/step - loss: 1.2780e-06 - accuracy: 1.0000 - val_loss: 0.4941 - val_accuracy: 0.8883
    Epoch 37/50
    9/9 [==============================] - 6s 720ms/step - loss: 1.1480e-06 - accuracy: 1.0000 - val_loss: 0.5121 - val_accuracy: 0.8883
    Epoch 38/50
    9/9 [==============================] - 7s 724ms/step - loss: 1.0514e-06 - accuracy: 1.0000 - val_loss: 0.5054 - val_accuracy: 0.8883
    Epoch 39/50
    9/9 [==============================] - 7s 728ms/step - loss: 9.7736e-07 - accuracy: 1.0000 - val_loss: 0.5154 - val_accuracy: 0.8883
    Epoch 40/50
    9/9 [==============================] - 7s 732ms/step - loss: 8.9324e-07 - accuracy: 1.0000 - val_loss: 0.5103 - val_accuracy: 0.8883
    Epoch 41/50
    9/9 [==============================] - 7s 724ms/step - loss: 8.4155e-07 - accuracy: 1.0000 - val_loss: 0.5200 - val_accuracy: 0.8883
    Epoch 42/50
    9/9 [==============================] - 7s 725ms/step - loss: 7.9085e-07 - accuracy: 1.0000 - val_loss: 0.5262 - val_accuracy: 0.8883
    Epoch 43/50
    9/9 [==============================] - 6s 722ms/step - loss: 7.2643e-07 - accuracy: 1.0000 - val_loss: 0.5151 - val_accuracy: 0.8883
    Epoch 44/50
    9/9 [==============================] - 6s 717ms/step - loss: 6.8205e-07 - accuracy: 1.0000 - val_loss: 0.5265 - val_accuracy: 0.8883
    Epoch 45/50
    9/9 [==============================] - 6s 717ms/step - loss: 6.3965e-07 - accuracy: 1.0000 - val_loss: 0.5301 - val_accuracy: 0.8883
    Epoch 46/50
    9/9 [==============================] - 6s 718ms/step - loss: 6.0025e-07 - accuracy: 1.0000 - val_loss: 0.5321 - val_accuracy: 0.8883
    Epoch 47/50
    9/9 [==============================] - 6s 722ms/step - loss: 5.6848e-07 - accuracy: 1.0000 - val_loss: 0.5326 - val_accuracy: 0.8883
    Epoch 48/50
    9/9 [==============================] - 7s 725ms/step - loss: 5.3528e-07 - accuracy: 1.0000 - val_loss: 0.5328 - val_accuracy: 0.8883
    Epoch 49/50
    9/9 [==============================] - 6s 722ms/step - loss: 5.0949e-07 - accuracy: 1.0000 - val_loss: 0.5352 - val_accuracy: 0.8883
    Epoch 50/50
    9/9 [==============================] - 7s 726ms/step - loss: 4.8038e-07 - accuracy: 1.0000 - val_loss: 0.5444 - val_accuracy: 0.8883

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights('./model.h5')
!cp -r model.h5 {name}/
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
# new_model.load_weights('./model-weights.45-0.977901.hdf5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.legend(['Training Accuracy', 'Validation Accuracy'], loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.legend(['Training Loss', 'Validation Loss'], loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(name + "/training_plot.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/259.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!cp -r training_plot.pdf {path}/
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    cp: cannot stat 'training_plot.pdf': No such file or directory

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 17s 3s/step - loss: 0.5124 - accuracy: 0.8887

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[15\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.5124456286430359, 0.8886827230453491]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.8886827458256029
                  precision    recall  f1-score   support

               0       0.86      0.74      0.80       158
               1       0.90      0.95      0.92       381

        accuracy                           0.89       539
       macro avg       0.88      0.85      0.86       539
    weighted avg       0.89      0.89      0.89       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install scikit-plot
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: scikit-plot in ./.local/lib/python3.7/site-packages (0.3.7)
    Requirement already satisfied: scipy&gt;=0.9 in ./.local/lib/python3.7/site-packages (from scikit-plot) (1.4.1)
    Requirement already satisfied: scikit-learn&gt;=0.18 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (0.16.0)
    Requirement already satisfied: matplotlib&gt;=1.4.0 in /opt/conda/lib/python3.7/site-packages (from scikit-plot) (3.2.2)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from scipy&gt;=0.9-&gt;scikit-plot) (1.19.0)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.18-&gt;scikit-plot) (2.1.0)
    Requirement already satisfied: kiwisolver&gt;=1.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.2.0)
    Requirement already satisfied: python-dateutil&gt;=2.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.8.1)
    Requirement already satisfied: cycler&gt;=0.10 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (0.10.0)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,&gt;=2.0.1 in /opt/conda/lib/python3.7/site-packages (from matplotlib&gt;=1.4.0-&gt;scikit-plot) (2.4.7)
    Requirement already satisfied: six&gt;=1.5 in /opt/conda/lib/python3.7/site-packages (from python-dateutil&gt;=2.1-&gt;matplotlib&gt;=1.4.0-&gt;scikit-plot) (1.15.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[18\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/260.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/261.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[19\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/262.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
!pip install imbalanced-learn
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Requirement already satisfied: imbalanced-learn in ./.local/lib/python3.7/site-packages (0.7.0)
    Requirement already satisfied: scikit-learn&gt;=0.23 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.23.1)
    Requirement already satisfied: joblib&gt;=0.11 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (0.16.0)
    Requirement already satisfied: numpy&gt;=1.13.3 in /opt/conda/lib/python3.7/site-packages (from imbalanced-learn) (1.19.0)
    Requirement already satisfied: scipy&gt;=0.19.1 in ./.local/lib/python3.7/site-packages (from imbalanced-learn) (1.4.1)
    Requirement already satisfied: threadpoolctl&gt;=2.0.0 in /opt/conda/lib/python3.7/site-packages (from scikit-learn&gt;=0.23-&gt;imbalanced-learn) (2.1.0)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.86      0.74      0.80       158
               1       0.90      0.95      0.92       381

        accuracy                           0.89       539
       macro avg       0.88      0.85      0.86       539
    weighted avg       0.89      0.89      0.89       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.86      0.74      0.95      0.80      0.84      0.69       158
              1       0.90      0.95      0.74      0.92      0.84      0.72       381

    avg / total       0.89      0.89      0.80      0.89      0.84      0.71       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
