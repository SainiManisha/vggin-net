<div class="border-box-sizing" id="notebook" tabindex="-1">
<div class="container" id="notebook-container">
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[1\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
name = 'BLOCK2andBLOCK3and4-FINETUNING_40X-BREAKHIS-Dataset-60-10-30-VGGINet'

path = './cancer-experiments/%s' % name
!mkdir -p {path}
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Training<a class="anchor-link" href="#Training">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[2\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf
tf.version.VERSION, tf.config.list_physical_devices()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[2\]:

</div>
<div class="output_text output_subarea output_execute_result">

    ('2.3.0-rc1',
     [PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU'),
      PhysicalDevice(name='/physical_device:XLA_CPU:0', device_type='XLA_CPU'),
      PhysicalDevice(name='/physical_device:XLA_GPU:0', device_type='XLA_GPU'),
      PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')])

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[3\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
base_model = VGG16(include_top=False, input_shape=(224, 224, 3))
base_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808   
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0         
    =================================================================
    Total params: 14,714,688
    Trainable params: 14,714,688
    Non-trainable params: 0
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[4\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
layer_name = 'block4_pool'
feature_ex_model = Model(inputs=base_model.input, outputs=base_model.get_layer(layer_name).output, name='vgg16_features')
feature_ex_model.trainable = False
feature_ex_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "vgg16_features"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0         
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792      
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928     
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0         
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856     
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584    
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0         
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168    
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080    
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0         
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160   
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808   
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0         
    =================================================================
    Total params: 7,635,264
    Trainable params: 0
    Non-trainable params: 7,635,264
    _________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[5\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Lambda, Input
from tensorflow.keras.applications.vgg16 import preprocess_input as vgg_preprocess

p1_layer = Lambda(vgg_preprocess, name='VGG_Preprocess')

image_input = Input((224, 224, 3), name='Image_Input')
p1_tensor = p1_layer(image_input)

out =feature_ex_model(p1_tensor)
feature_ex_model = Model(inputs=image_input, outputs=out)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[6\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Concatenate,BatchNormalization,Activation 

def naive_inception_module(layer_in, f1, f2, f3):
    # 1x1 conv
    conv1 = Conv2D(f1, (1,1), padding='same', activation='relu')(layer_in)
    # 3x3 conv
    conv3 = Conv2D(f2, (3,3), padding='same', activation='relu')(layer_in)
    # 5x5 conv
    conv5 = Conv2D(f3, (5,5), padding='same', activation='relu')(layer_in)
    # 3x3 max pooling
    pool = MaxPooling2D((3,3), strides=(1,1), padding='same')(layer_in)
    # concatenate filters, assumes filters/channels last
    layer_out = Concatenate()([conv1, conv3, conv5, pool])
    return layer_out
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[7\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
out = naive_inception_module(feature_ex_model.output, 64, 128, 32)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[8\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dropout, GlobalAveragePooling2D, Dropout, Dense, BatchNormalization, Flatten
num_classes = 2

bn1 = BatchNormalization(name='BN')(out)
f = Flatten()(bn1)
dropout = Dropout(0.4, name='Dropout')(f)
desne = Dense(num_classes, activation='softmax', name='Predictions')(dropout)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[9\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model = Model(inputs=feature_ex_model.input, outputs=desne)
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 1,322,402
    Non-trainable params: 7,636,736
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[10\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow import keras
opt = keras.optimizers.Adam(lr=0.001)
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[11\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[12\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import ModelCheckpoint
callbacks = [
    # ModelCheckpoint(monitor='val_accuracy',filepath="./model-weights.{epoch:02d}-{val_accuracy:.6f}.hdf5", verbose=1, save_best_only=True)
]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[13\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from tensorflow.keras.callbacks import TensorBoard
callbacks += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[14\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import tensorflow as tf

image_size = (224, 340)
crop_size = (224, 224)
batch_size = 128

train_path = './Splitted_40X/train'
val_path = './Splitted_40X/val'
test_path = './Splitted_40X/test'

def dataset(ds_path, image_size, crop_size, batch_size, train):
    ds = tf.keras.preprocessing.image_dataset_from_directory(
        ds_path, shuffle=train, label_mode='categorical',
        batch_size=batch_size, image_size=image_size)
    
    gen = tf.keras.preprocessing.image.ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        shear_range=0.2,
        zoom_range=0.2)
    @tf.function
    def augment(images, labels):
        aug_images = tf.map_fn(lambda image: tf.numpy_function(gen.random_transform,
                                                               [image],
                                                               tf.float32), 
                               images)
        aug_images = tf.ensure_shape(aug_images, images.shape)
        return aug_images, labels
    
    crop_layer = tf.keras.layers.experimental.preprocessing.RandomCrop(*crop_size)
    @tf.function
    def crop(images, labels):
        cropped_images = crop_layer(images, training=train)
        return cropped_images, labels
    
    if train:
        ds = ds.map(augment, tf.data.experimental.AUTOTUNE)
    ds = ds.map(crop, tf.data.experimental.AUTOTUNE)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds

train_ds = dataset(train_path, image_size, crop_size, batch_size, train=True)
val_ds = dataset(val_path, image_size, crop_size, batch_size, train=False)
test_ds = dataset(test_path, image_size, crop_size, batch_size, train=False)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.
    Found 179 files belonging to 2 classes.
    Found 539 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[15\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
init_epochs=100
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[16\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs, 
                              validation_data=val_ds,
                              verbose=1, callbacks=callbacks)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 1/100
    1/9 [==&gt;...........................] - ETA: 0s - loss: 1.3066 - accuracy: 0.4609WARNING:tensorflow:From /home/swg/.local/lib/python3.7/site-packages/tensorflow/python/ops/summary_ops_v2.py:1277: stop (from tensorflow.python.eager.profiler) is deprecated and will be removed after 2020-07-01.
    Instructions for updating:
    use `tf.profiler.experimental.stop` instead.
    9/9 [==============================] - 17s 2s/step - loss: 5.2973 - accuracy: 0.7084 - val_loss: 6.6115 - val_accuracy: 0.8771
    Epoch 2/100
    9/9 [==============================] - 13s 1s/step - loss: 2.1395 - accuracy: 0.8431 - val_loss: 3.4067 - val_accuracy: 0.8827
    Epoch 3/100
    9/9 [==============================] - 13s 1s/step - loss: 1.4743 - accuracy: 0.8784 - val_loss: 20.7584 - val_accuracy: 0.7374
    Epoch 4/100
    9/9 [==============================] - 13s 1s/step - loss: 1.3194 - accuracy: 0.8756 - val_loss: 2.0588 - val_accuracy: 0.8939
    Epoch 5/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2277 - accuracy: 0.9006 - val_loss: 3.8035 - val_accuracy: 0.8547
    Epoch 6/100
    9/9 [==============================] - 14s 2s/step - loss: 1.0229 - accuracy: 0.9044 - val_loss: 1.1735 - val_accuracy: 0.9385
    Epoch 7/100
    9/9 [==============================] - 10s 1s/step - loss: 0.9480 - accuracy: 0.9146 - val_loss: 1.4673 - val_accuracy: 0.9497
    Epoch 8/100
    9/9 [==============================] - 13s 1s/step - loss: 0.8752 - accuracy: 0.9136 - val_loss: 2.2072 - val_accuracy: 0.8883
    Epoch 9/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6370 - accuracy: 0.9378 - val_loss: 1.8481 - val_accuracy: 0.9162
    Epoch 10/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6529 - accuracy: 0.9294 - val_loss: 2.0668 - val_accuracy: 0.9274
    Epoch 11/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6119 - accuracy: 0.9350 - val_loss: 0.8846 - val_accuracy: 0.9330
    Epoch 12/100
    9/9 [==============================] - 13s 1s/step - loss: 0.7051 - accuracy: 0.9229 - val_loss: 1.8560 - val_accuracy: 0.9162
    Epoch 13/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6624 - accuracy: 0.9387 - val_loss: 1.1465 - val_accuracy: 0.9330
    Epoch 14/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6952 - accuracy: 0.9461 - val_loss: 1.2271 - val_accuracy: 0.9274
    Epoch 15/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4628 - accuracy: 0.9499 - val_loss: 2.6091 - val_accuracy: 0.8883
    Epoch 16/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5575 - accuracy: 0.9406 - val_loss: 2.2987 - val_accuracy: 0.9050
    Epoch 17/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5772 - accuracy: 0.9545 - val_loss: 1.8018 - val_accuracy: 0.9218
    Epoch 18/100
    9/9 [==============================] - 14s 2s/step - loss: 0.5599 - accuracy: 0.9499 - val_loss: 1.0245 - val_accuracy: 0.9385
    Epoch 19/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5356 - accuracy: 0.9489 - val_loss: 2.4021 - val_accuracy: 0.8994
    Epoch 20/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6281 - accuracy: 0.9508 - val_loss: 1.6001 - val_accuracy: 0.9218
    Epoch 21/100
    9/9 [==============================] - 13s 1s/step - loss: 0.5342 - accuracy: 0.9499 - val_loss: 4.0501 - val_accuracy: 0.8827
    Epoch 22/100
    9/9 [==============================] - 10s 1s/step - loss: 0.6288 - accuracy: 0.9461 - val_loss: 1.9214 - val_accuracy: 0.8994
    Epoch 23/100
    9/9 [==============================] - 13s 1s/step - loss: 0.6216 - accuracy: 0.9591 - val_loss: 0.9174 - val_accuracy: 0.9497
    Epoch 24/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6929 - accuracy: 0.9443 - val_loss: 2.8186 - val_accuracy: 0.8715
    Epoch 25/100
    9/9 [==============================] - 14s 2s/step - loss: 1.2640 - accuracy: 0.9294 - val_loss: 1.4256 - val_accuracy: 0.9162
    Epoch 26/100
    9/9 [==============================] - 17s 2s/step - loss: 0.7324 - accuracy: 0.9471 - val_loss: 3.0738 - val_accuracy: 0.9050
    Epoch 27/100
    9/9 [==============================] - 14s 2s/step - loss: 0.7062 - accuracy: 0.9517 - val_loss: 1.4211 - val_accuracy: 0.9385
    Epoch 28/100
    9/9 [==============================] - 14s 2s/step - loss: 0.6943 - accuracy: 0.9591 - val_loss: 1.2299 - val_accuracy: 0.9385
    Epoch 29/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4871 - accuracy: 0.9591 - val_loss: 0.7674 - val_accuracy: 0.9441
    Epoch 30/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4892 - accuracy: 0.9749 - val_loss: 0.7006 - val_accuracy: 0.9665
    Epoch 31/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3818 - accuracy: 0.9712 - val_loss: 0.6049 - val_accuracy: 0.9553
    Epoch 32/100
    9/9 [==============================] - 14s 2s/step - loss: 0.4217 - accuracy: 0.9638 - val_loss: 1.3161 - val_accuracy: 0.9497
    Epoch 33/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2480 - accuracy: 0.9814 - val_loss: 0.7672 - val_accuracy: 0.9441
    Epoch 34/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2105 - accuracy: 0.9824 - val_loss: 0.4939 - val_accuracy: 0.9609
    Epoch 35/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2708 - accuracy: 0.9777 - val_loss: 1.3307 - val_accuracy: 0.9330
    Epoch 36/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3536 - accuracy: 0.9740 - val_loss: 2.3337 - val_accuracy: 0.9050
    Epoch 37/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2271 - accuracy: 0.9786 - val_loss: 0.7318 - val_accuracy: 0.9665
    Epoch 38/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2566 - accuracy: 0.9768 - val_loss: 0.7348 - val_accuracy: 0.9497
    Epoch 39/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2059 - accuracy: 0.9824 - val_loss: 0.7033 - val_accuracy: 0.9609
    Epoch 40/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1898 - accuracy: 0.9842 - val_loss: 0.8883 - val_accuracy: 0.9553
    Epoch 41/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1138 - accuracy: 0.9824 - val_loss: 0.9517 - val_accuracy: 0.9609
    Epoch 42/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2423 - accuracy: 0.9861 - val_loss: 1.2692 - val_accuracy: 0.9497
    Epoch 43/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2148 - accuracy: 0.9833 - val_loss: 1.5985 - val_accuracy: 0.9441
    Epoch 44/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3598 - accuracy: 0.9768 - val_loss: 1.2629 - val_accuracy: 0.9609
    Epoch 45/100
    9/9 [==============================] - 10s 1s/step - loss: 0.4421 - accuracy: 0.9638 - val_loss: 0.8733 - val_accuracy: 0.9553
    Epoch 46/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1814 - accuracy: 0.9805 - val_loss: 1.4587 - val_accuracy: 0.9497
    Epoch 47/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2985 - accuracy: 0.9824 - val_loss: 1.4211 - val_accuracy: 0.9553
    Epoch 48/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3585 - accuracy: 0.9703 - val_loss: 1.0015 - val_accuracy: 0.9721
    Epoch 49/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3224 - accuracy: 0.9824 - val_loss: 1.9139 - val_accuracy: 0.9385
    Epoch 50/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3337 - accuracy: 0.9805 - val_loss: 1.0146 - val_accuracy: 0.9609
    Epoch 51/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2166 - accuracy: 0.9796 - val_loss: 0.8148 - val_accuracy: 0.9665
    Epoch 52/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1730 - accuracy: 0.9833 - val_loss: 1.0705 - val_accuracy: 0.9609
    Epoch 53/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1660 - accuracy: 0.9870 - val_loss: 1.5133 - val_accuracy: 0.9441
    Epoch 54/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1197 - accuracy: 0.9907 - val_loss: 1.7297 - val_accuracy: 0.9385
    Epoch 55/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2686 - accuracy: 0.9786 - val_loss: 0.6730 - val_accuracy: 0.9553
    Epoch 56/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3163 - accuracy: 0.9796 - val_loss: 0.7157 - val_accuracy: 0.9721
    Epoch 57/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1778 - accuracy: 0.9870 - val_loss: 0.5918 - val_accuracy: 0.9777
    Epoch 58/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2466 - accuracy: 0.9833 - val_loss: 0.4546 - val_accuracy: 0.9777
    Epoch 59/100
    9/9 [==============================] - 14s 2s/step - loss: 0.1928 - accuracy: 0.9870 - val_loss: 0.5437 - val_accuracy: 0.9609
    Epoch 60/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1775 - accuracy: 0.9879 - val_loss: 1.1350 - val_accuracy: 0.9441
    Epoch 61/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2429 - accuracy: 0.9749 - val_loss: 1.4670 - val_accuracy: 0.9497
    Epoch 62/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3587 - accuracy: 0.9768 - val_loss: 1.5133 - val_accuracy: 0.9553
    Epoch 63/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2218 - accuracy: 0.9870 - val_loss: 0.9065 - val_accuracy: 0.9777
    Epoch 64/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3238 - accuracy: 0.9814 - val_loss: 1.5294 - val_accuracy: 0.9721
    Epoch 65/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3819 - accuracy: 0.9796 - val_loss: 0.3278 - val_accuracy: 0.9777
    Epoch 66/100
    9/9 [==============================] - 10s 1s/step - loss: 0.3678 - accuracy: 0.9786 - val_loss: 0.6130 - val_accuracy: 0.9721
    Epoch 67/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2296 - accuracy: 0.9861 - val_loss: 0.9726 - val_accuracy: 0.9721
    Epoch 68/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2852 - accuracy: 0.9814 - val_loss: 0.8121 - val_accuracy: 0.9777
    Epoch 69/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1745 - accuracy: 0.9842 - val_loss: 0.7215 - val_accuracy: 0.9665
    Epoch 70/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2450 - accuracy: 0.9870 - val_loss: 1.1792 - val_accuracy: 0.9497
    Epoch 71/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2461 - accuracy: 0.9851 - val_loss: 1.2189 - val_accuracy: 0.9553
    Epoch 72/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1801 - accuracy: 0.9814 - val_loss: 1.1401 - val_accuracy: 0.9553
    Epoch 73/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2139 - accuracy: 0.9870 - val_loss: 0.8362 - val_accuracy: 0.9497
    Epoch 74/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1386 - accuracy: 0.9879 - val_loss: 0.6579 - val_accuracy: 0.9721
    Epoch 75/100
    9/9 [==============================] - 14s 2s/step - loss: 0.3367 - accuracy: 0.9861 - val_loss: 0.3375 - val_accuracy: 0.9832
    Epoch 76/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2999 - accuracy: 0.9768 - val_loss: 0.1392 - val_accuracy: 0.9777
    Epoch 77/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4290 - accuracy: 0.9740 - val_loss: 1.0886 - val_accuracy: 0.9497
    Epoch 78/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3911 - accuracy: 0.9768 - val_loss: 0.8556 - val_accuracy: 0.9777
    Epoch 79/100
    9/9 [==============================] - 13s 1s/step - loss: 0.4428 - accuracy: 0.9731 - val_loss: 1.5466 - val_accuracy: 0.9441
    Epoch 80/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2555 - accuracy: 0.9824 - val_loss: 2.5802 - val_accuracy: 0.9274
    Epoch 81/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3551 - accuracy: 0.9740 - val_loss: 1.5323 - val_accuracy: 0.9609
    Epoch 82/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1848 - accuracy: 0.9870 - val_loss: 0.6686 - val_accuracy: 0.9777
    Epoch 83/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3249 - accuracy: 0.9814 - val_loss: 1.3037 - val_accuracy: 0.9665
    Epoch 84/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2235 - accuracy: 0.9898 - val_loss: 2.4718 - val_accuracy: 0.9162
    Epoch 85/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2086 - accuracy: 0.9870 - val_loss: 1.9737 - val_accuracy: 0.9330
    Epoch 86/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2355 - accuracy: 0.9870 - val_loss: 3.9593 - val_accuracy: 0.9218
    Epoch 87/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1851 - accuracy: 0.9851 - val_loss: 1.7067 - val_accuracy: 0.9330
    Epoch 88/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1612 - accuracy: 0.9833 - val_loss: 1.4120 - val_accuracy: 0.9441
    Epoch 89/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2235 - accuracy: 0.9870 - val_loss: 1.8895 - val_accuracy: 0.9330
    Epoch 90/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1443 - accuracy: 0.9879 - val_loss: 0.8829 - val_accuracy: 0.9721
    Epoch 91/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1152 - accuracy: 0.9898 - val_loss: 0.6775 - val_accuracy: 0.9665
    Epoch 92/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1599 - accuracy: 0.9879 - val_loss: 0.7955 - val_accuracy: 0.9609
    Epoch 93/100
    9/9 [==============================] - 13s 1s/step - loss: 0.1917 - accuracy: 0.9861 - val_loss: 3.0103 - val_accuracy: 0.9497
    Epoch 94/100
    9/9 [==============================] - 10s 1s/step - loss: 0.2994 - accuracy: 0.9805 - val_loss: 1.4133 - val_accuracy: 0.9609
    Epoch 95/100
    9/9 [==============================] - 10s 1s/step - loss: 0.1837 - accuracy: 0.9870 - val_loss: 1.6118 - val_accuracy: 0.9441
    Epoch 96/100
    9/9 [==============================] - 13s 1s/step - loss: 0.3011 - accuracy: 0.9805 - val_loss: 2.1822 - val_accuracy: 0.9441
    Epoch 97/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2562 - accuracy: 0.9851 - val_loss: 0.6752 - val_accuracy: 0.9777
    Epoch 98/100
    9/9 [==============================] - 14s 2s/step - loss: 0.2573 - accuracy: 0.9851 - val_loss: 0.7722 - val_accuracy: 0.9777
    Epoch 99/100
    9/9 [==============================] - 13s 1s/step - loss: 0.2487 - accuracy: 0.9833 - val_loss: 0.8803 - val_accuracy: 0.9609
    Epoch 100/100
    9/9 [==============================] - 9s 1s/step - loss: 0.2000 - accuracy: 0.9926 - val_loss: 2.3076 - val_accuracy: 0.9330

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[17\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/init_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[18\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from matplotlib import pyplot as plt
plt.style.use("ggplot")
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[19\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc = train_history.history['accuracy']
val_acc = train_history.history['val_accuracy']

loss = train_history.history['loss']
val_loss = train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot1.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/267.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[20\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 13s 3s/step - loss: 1.3775 - accuracy: 0.9555

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[20\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [1.3775485754013062, 0.9554731249809265]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">

# Fine Tuning<a class="anchor-link" href="#Fine-Tuning">¶</a>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[21\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.get_layer('vgg16_features').trainable = True
for layer in new_model.get_layer('vgg16_features').layers:
    if layer.name.startswith('block4') or layer.name.startswith('block3') or layer.name.startswith('block2'):
        layer.trainable = True
    else:
        layer.trainable = False
new_model.summary()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Model: "functional_3"
    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    Image_Input (InputLayer)        [(None, 224, 224, 3) 0                                            
    __________________________________________________________________________________________________
    VGG_Preprocess (Lambda)         (None, 224, 224, 3)  0           Image_Input[0][0]                
    __________________________________________________________________________________________________
    vgg16_features (Functional)     (None, 14, 14, 512)  7635264     VGG_Preprocess[0][0]             
    __________________________________________________________________________________________________
    conv2d (Conv2D)                 (None, 14, 14, 64)   32832       vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 14, 14, 128)  589952      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 14, 14, 32)   409632      vgg16_features[0][0]             
    __________________________________________________________________________________________________
    max_pooling2d (MaxPooling2D)    (None, 14, 14, 512)  0           vgg16_features[0][0]             
    __________________________________________________________________________________________________
    concatenate (Concatenate)       (None, 14, 14, 736)  0           conv2d[0][0]                     
                                                                     conv2d_1[0][0]                   
                                                                     conv2d_2[0][0]                   
                                                                     max_pooling2d[0][0]              
    __________________________________________________________________________________________________
    BN (BatchNormalization)         (None, 14, 14, 736)  2944        concatenate[0][0]                
    __________________________________________________________________________________________________
    flatten (Flatten)               (None, 144256)       0           BN[0][0]                         
    __________________________________________________________________________________________________
    Dropout (Dropout)               (None, 144256)       0           flatten[0][0]                    
    __________________________________________________________________________________________________
    Predictions (Dense)             (None, 2)            288514      Dropout[0][0]                    
    ==================================================================================================
    Total params: 8,959,138
    Trainable params: 8,918,946
    Non-trainable params: 40,192
    __________________________________________________________________________________________________

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[22\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
lr_start = lr_min = 1e-5
lr_max = 5e-5

lr_warmup_epochs = 25
lr_exp_decay = 0.8

def lr_schedule(epoch):
    if epoch &lt; lr_warmup_epochs:
        lr = (lr_max - lr_start) / lr_warmup_epochs * epoch + lr_start
    else:
        lr = (lr_max - lr_min) * lr_exp_decay ** (epoch - lr_warmup_epochs) + lr_min
    return lr

cbs = [keras.callbacks.LearningRateScheduler(lr_schedule, verbose=True)]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[23\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
cbs += [TensorBoard(path + '/tensorboard-logs')]
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[24\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epochs=50
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[25\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
epx = range(1, epochs + 1)
lrs = [lr_schedule(e) for e in epx]
plt.figure(figsize=(10, 5))
plt.plot(epx, lrs, 'b-')
plt.title("Learning Rate Schedule (for fine tuning)")
plt.xlabel("Epoch")
plt.ylabel("Learning Rate")
plt.savefig(path + '/lr_schedule.pdf')
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/268.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[26\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.compile('adam', 'categorical_crossentropy',
               metrics=['accuracy'])
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[27\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_ds = dataset(train_path, image_size, crop_size, 64, train=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Found 1077 files belonging to 2 classes.

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[28\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
train_history = new_model.fit(train_ds, epochs=init_epochs+epochs, 
                              validation_data=val_ds,
                              initial_epoch=init_epochs,
                              callbacks=cbs,
                              verbose=1)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Epoch 00101: LearningRateScheduler reducing learning rate to 1.0000002156795734e-05.
    Epoch 101/150
     2/17 [==&gt;...........................] - ETA: 5s - loss: 0.0679 - accuracy: 0.9844    WARNING:tensorflow:Callbacks method `on_train_batch_end` is slow compared to the batch time (batch time: 0.2405s vs `on_train_batch_end` time: 0.4773s). Check your callbacks.
    17/17 [==============================] - 25s 1s/step - loss: 0.2664 - accuracy: 0.9870 - val_loss: 5.6933 - val_accuracy: 0.9106

    Epoch 00102: LearningRateScheduler reducing learning rate to 1.0000001725436587e-05.
    Epoch 102/150
    17/17 [==============================] - 23s 1s/step - loss: 0.2331 - accuracy: 0.9861 - val_loss: 1.5260 - val_accuracy: 0.9330

    Epoch 00103: LearningRateScheduler reducing learning rate to 1.0000001380349271e-05.
    Epoch 103/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1831 - accuracy: 0.9889 - val_loss: 1.1579 - val_accuracy: 0.9553

    Epoch 00104: LearningRateScheduler reducing learning rate to 1.0000001104279416e-05.
    Epoch 104/150
    17/17 [==============================] - 26s 2s/step - loss: 0.3021 - accuracy: 0.9833 - val_loss: 0.8309 - val_accuracy: 0.9609

    Epoch 00105: LearningRateScheduler reducing learning rate to 1.0000000883423533e-05.
    Epoch 105/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1255 - accuracy: 0.9889 - val_loss: 0.5210 - val_accuracy: 0.9609

    Epoch 00106: LearningRateScheduler reducing learning rate to 1.0000000706738827e-05.
    Epoch 106/150
    17/17 [==============================] - 25s 1s/step - loss: 0.3029 - accuracy: 0.9786 - val_loss: 2.7337 - val_accuracy: 0.8994

    Epoch 00107: LearningRateScheduler reducing learning rate to 1.0000000565391061e-05.
    Epoch 107/150
    17/17 [==============================] - 25s 1s/step - loss: 0.2059 - accuracy: 0.9889 - val_loss: 6.0504 - val_accuracy: 0.8268

    Epoch 00108: LearningRateScheduler reducing learning rate to 1.0000000452312849e-05.
    Epoch 108/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1620 - accuracy: 0.9889 - val_loss: 1.4850 - val_accuracy: 0.9330

    Epoch 00109: LearningRateScheduler reducing learning rate to 1.000000036185028e-05.
    Epoch 109/150
    17/17 [==============================] - 26s 2s/step - loss: 0.3731 - accuracy: 0.9814 - val_loss: 2.1632 - val_accuracy: 0.9330

    Epoch 00110: LearningRateScheduler reducing learning rate to 1.0000000289480224e-05.
    Epoch 110/150
    17/17 [==============================] - 26s 2s/step - loss: 0.2214 - accuracy: 0.9861 - val_loss: 1.0188 - val_accuracy: 0.9441

    Epoch 00111: LearningRateScheduler reducing learning rate to 1.0000000231584179e-05.
    Epoch 111/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0831 - accuracy: 0.9907 - val_loss: 0.9497 - val_accuracy: 0.9609

    Epoch 00112: LearningRateScheduler reducing learning rate to 1.0000000185267343e-05.
    Epoch 112/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1176 - accuracy: 0.9907 - val_loss: 0.5377 - val_accuracy: 0.9721

    Epoch 00113: LearningRateScheduler reducing learning rate to 1.0000000148213875e-05.
    Epoch 113/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1715 - accuracy: 0.9907 - val_loss: 0.8345 - val_accuracy: 0.9385

    Epoch 00114: LearningRateScheduler reducing learning rate to 1.00000001185711e-05.
    Epoch 114/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1912 - accuracy: 0.9879 - val_loss: 5.6878 - val_accuracy: 0.8827

    Epoch 00115: LearningRateScheduler reducing learning rate to 1.0000000094856881e-05.
    Epoch 115/150
    17/17 [==============================] - 25s 1s/step - loss: 0.3097 - accuracy: 0.9842 - val_loss: 0.7967 - val_accuracy: 0.9665

    Epoch 00116: LearningRateScheduler reducing learning rate to 1.0000000075885505e-05.
    Epoch 116/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1859 - accuracy: 0.9870 - val_loss: 1.6165 - val_accuracy: 0.9218

    Epoch 00117: LearningRateScheduler reducing learning rate to 1.0000000060708404e-05.
    Epoch 117/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1079 - accuracy: 0.9935 - val_loss: 1.5373 - val_accuracy: 0.9330

    Epoch 00118: LearningRateScheduler reducing learning rate to 1.0000000048566724e-05.
    Epoch 118/150
    17/17 [==============================] - 26s 2s/step - loss: 0.3050 - accuracy: 0.9898 - val_loss: 0.6748 - val_accuracy: 0.9665

    Epoch 00119: LearningRateScheduler reducing learning rate to 1.0000000038853378e-05.
    Epoch 119/150
    17/17 [==============================] - 25s 1s/step - loss: 0.3153 - accuracy: 0.9879 - val_loss: 3.8683 - val_accuracy: 0.9218

    Epoch 00120: LearningRateScheduler reducing learning rate to 1.0000000031082703e-05.
    Epoch 120/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0852 - accuracy: 0.9926 - val_loss: 1.0528 - val_accuracy: 0.9385

    Epoch 00121: LearningRateScheduler reducing learning rate to 1.0000000024866162e-05.
    Epoch 121/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1488 - accuracy: 0.9898 - val_loss: 1.2513 - val_accuracy: 0.9497

    Epoch 00122: LearningRateScheduler reducing learning rate to 1.000000001989293e-05.
    Epoch 122/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1520 - accuracy: 0.9870 - val_loss: 3.6274 - val_accuracy: 0.9330

    Epoch 00123: LearningRateScheduler reducing learning rate to 1.0000000015914344e-05.
    Epoch 123/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1929 - accuracy: 0.9935 - val_loss: 3.4042 - val_accuracy: 0.9330

    Epoch 00124: LearningRateScheduler reducing learning rate to 1.0000000012731475e-05.
    Epoch 124/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1309 - accuracy: 0.9935 - val_loss: 1.0069 - val_accuracy: 0.9553

    Epoch 00125: LearningRateScheduler reducing learning rate to 1.000000001018518e-05.
    Epoch 125/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1734 - accuracy: 0.9889 - val_loss: 1.7614 - val_accuracy: 0.9385

    Epoch 00126: LearningRateScheduler reducing learning rate to 1.0000000008148144e-05.
    Epoch 126/150
    17/17 [==============================] - 26s 2s/step - loss: 0.2942 - accuracy: 0.9851 - val_loss: 1.3888 - val_accuracy: 0.9553

    Epoch 00127: LearningRateScheduler reducing learning rate to 1.0000000006518516e-05.
    Epoch 127/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0310 - accuracy: 0.9963 - val_loss: 2.3647 - val_accuracy: 0.9330

    Epoch 00128: LearningRateScheduler reducing learning rate to 1.0000000005214813e-05.
    Epoch 128/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1919 - accuracy: 0.9870 - val_loss: 0.8963 - val_accuracy: 0.9609

    Epoch 00129: LearningRateScheduler reducing learning rate to 1.000000000417185e-05.
    Epoch 129/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1765 - accuracy: 0.9898 - val_loss: 2.0573 - val_accuracy: 0.9441

    Epoch 00130: LearningRateScheduler reducing learning rate to 1.000000000333748e-05.
    Epoch 130/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0767 - accuracy: 0.9954 - val_loss: 2.3622 - val_accuracy: 0.9385

    Epoch 00131: LearningRateScheduler reducing learning rate to 1.0000000002669984e-05.
    Epoch 131/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1918 - accuracy: 0.9879 - val_loss: 3.8913 - val_accuracy: 0.9330

    Epoch 00132: LearningRateScheduler reducing learning rate to 1.0000000002135988e-05.
    Epoch 132/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1101 - accuracy: 0.9944 - val_loss: 4.7867 - val_accuracy: 0.9162

    Epoch 00133: LearningRateScheduler reducing learning rate to 1.000000000170879e-05.
    Epoch 133/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1431 - accuracy: 0.9889 - val_loss: 1.8622 - val_accuracy: 0.9553

    Epoch 00134: LearningRateScheduler reducing learning rate to 1.0000000001367032e-05.
    Epoch 134/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1330 - accuracy: 0.9916 - val_loss: 0.9748 - val_accuracy: 0.9665

    Epoch 00135: LearningRateScheduler reducing learning rate to 1.0000000001093625e-05.
    Epoch 135/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1134 - accuracy: 0.9926 - val_loss: 0.6754 - val_accuracy: 0.9609

    Epoch 00136: LearningRateScheduler reducing learning rate to 1.0000000000874901e-05.
    Epoch 136/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0172 - accuracy: 0.9981 - val_loss: 0.5462 - val_accuracy: 0.9832

    Epoch 00137: LearningRateScheduler reducing learning rate to 1.0000000000699921e-05.
    Epoch 137/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1037 - accuracy: 0.9916 - val_loss: 0.6725 - val_accuracy: 0.9553

    Epoch 00138: LearningRateScheduler reducing learning rate to 1.0000000000559937e-05.
    Epoch 138/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1600 - accuracy: 0.9898 - val_loss: 0.6593 - val_accuracy: 0.9609

    Epoch 00139: LearningRateScheduler reducing learning rate to 1.0000000000447949e-05.
    Epoch 139/150
    17/17 [==============================] - 27s 2s/step - loss: 0.2865 - accuracy: 0.9833 - val_loss: 2.1002 - val_accuracy: 0.9385

    Epoch 00140: LearningRateScheduler reducing learning rate to 1.000000000035836e-05.
    Epoch 140/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0865 - accuracy: 0.9944 - val_loss: 2.0522 - val_accuracy: 0.9106

    Epoch 00141: LearningRateScheduler reducing learning rate to 1.0000000000286688e-05.
    Epoch 141/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1654 - accuracy: 0.9898 - val_loss: 0.7158 - val_accuracy: 0.9385

    Epoch 00142: LearningRateScheduler reducing learning rate to 1.000000000022935e-05.
    Epoch 142/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1716 - accuracy: 0.9916 - val_loss: 1.5499 - val_accuracy: 0.9497

    Epoch 00143: LearningRateScheduler reducing learning rate to 1.000000000018348e-05.
    Epoch 143/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1458 - accuracy: 0.9898 - val_loss: 1.0347 - val_accuracy: 0.9609

    Epoch 00144: LearningRateScheduler reducing learning rate to 1.0000000000146785e-05.
    Epoch 144/150
    17/17 [==============================] - 23s 1s/step - loss: 0.1494 - accuracy: 0.9916 - val_loss: 0.5733 - val_accuracy: 0.9609

    Epoch 00145: LearningRateScheduler reducing learning rate to 1.0000000000117428e-05.
    Epoch 145/150
    17/17 [==============================] - 25s 1s/step - loss: 0.0492 - accuracy: 0.9954 - val_loss: 0.3828 - val_accuracy: 0.9721

    Epoch 00146: LearningRateScheduler reducing learning rate to 1.0000000000093942e-05.
    Epoch 146/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1582 - accuracy: 0.9916 - val_loss: 1.0573 - val_accuracy: 0.9609

    Epoch 00147: LearningRateScheduler reducing learning rate to 1.0000000000075155e-05.
    Epoch 147/150
    17/17 [==============================] - 26s 2s/step - loss: 0.0086 - accuracy: 0.9991 - val_loss: 5.2620 - val_accuracy: 0.9106

    Epoch 00148: LearningRateScheduler reducing learning rate to 1.0000000000060123e-05.
    Epoch 148/150
    17/17 [==============================] - 25s 1s/step - loss: 0.1544 - accuracy: 0.9926 - val_loss: 0.9024 - val_accuracy: 0.9721

    Epoch 00149: LearningRateScheduler reducing learning rate to 1.0000000000048099e-05.
    Epoch 149/150
    17/17 [==============================] - 24s 1s/step - loss: 0.1895 - accuracy: 0.9879 - val_loss: 0.7565 - val_accuracy: 0.9721

    Epoch 00150: LearningRateScheduler reducing learning rate to 1.000000000003848e-05.
    Epoch 150/150
    17/17 [==============================] - 26s 2s/step - loss: 0.1023 - accuracy: 0.9935 - val_loss: 0.5398 - val_accuracy: 0.9777

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[29\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.save_weights(path + '/finetuned_model.h5')
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[30\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
acc += train_history.history['accuracy']
val_acc += train_history.history['val_accuracy']

loss += train_history.history['loss']
val_loss += train_history.history['val_loss']

fig = plt.figure(figsize=(8, 8))
fig.patch.set_alpha(0.5)

plt.subplot(2, 1, 1)
plt.plot(acc)
plt.plot(val_acc)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Accuracy', 'Validation Accuracy', 'Start Fine Tuning'],
           loc='lower right')
plt.ylabel('Accuracy')
plt.title('Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss)
plt.plot(val_loss)
plt.plot([init_epochs - 1, init_epochs - 1], plt.ylim())
plt.legend(['Training Loss', 'Validation Loss', 'Start Fine Tuning'],
           loc='upper right')
plt.ylabel('Cross Entropy')
plt.xlabel('Epochs')
plt.title('Loss')
plt.savefig(path + "/training_plot2.pdf")
plt.show()
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/269.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[31\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
new_model.evaluate(test_ds)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    5/5 [==============================] - 5s 1s/step - loss: 0.6958 - accuracy: 0.9777

</div>
</div>
<div class="output_area">
<div class="prompt output_prompt">

Out\[31\]:

</div>
<div class="output_text output_subarea output_execute_result">

    [0.6958262324333191, 0.9777365326881409]

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[32\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

y_true, y_pred = [], []
for images, labels in test_ds:
    preds = new_model.predict(images)
    y_true.append(labels.numpy())
    y_pred.append(preds)

y_true, y_pred = np.concatenate(y_true), np.concatenate(y_pred)
y_probas = y_pred
y_true, y_pred = np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)
print('Accuracy: ', accuracy_score(y_true, y_pred))
print(classification_report(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

    Accuracy:  0.9777365491651205
                  precision    recall  f1-score   support

               0       0.96      0.96      0.96       158
               1       0.98      0.98      0.98       381

        accuracy                           0.98       539
       macro avg       0.97      0.97      0.97       539
    weighted avg       0.98      0.98      0.98       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[33\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from scikitplot.metrics import plot_confusion_matrix, plot_roc
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[34\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_confusion_matrix(y_true, y_pred)
plot_confusion_matrix(y_true, y_pred, normalize=True)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[34\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/270.png)

</div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/271.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[35\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
plot_roc(y_true, y_probas)
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt output_prompt">

Out\[35\]:

</div>
<div class="output_text output_subarea output_execute_result">
<matplotlib.axes._subplots.axessubplot at="">
</matplotlib.axes._subplots.axessubplot></div>
</div>
<div class="output_area">
<div class="prompt">
</div>
<div class="output_png output_subarea">

![](./figures/272.png)

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[36\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
from imblearn.metrics import classification_report_imbalanced
```

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[37\]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python
print(classification_report(y_true, y_pred))
print(classification_report_imbalanced(y_true, y_pred))
```

</div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt">
</div>
<div class="output_subarea output_stream output_stdout output_text">

                  precision    recall  f1-score   support

               0       0.96      0.96      0.96       158
               1       0.98      0.98      0.98       381

        accuracy                           0.98       539
       macro avg       0.97      0.97      0.97       539
    weighted avg       0.98      0.98      0.98       539

                       pre       rec       spe        f1       geo       iba       sup

              0       0.96      0.96      0.98      0.96      0.97      0.94       158
              1       0.98      0.98      0.96      0.98      0.97      0.95       381

    avg / total       0.98      0.98      0.97      0.98      0.97      0.95       539

</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered">
<div class="input">
<div class="prompt input_prompt">

In \[ \]:

</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3">

```python

```

</div>
</div>
</div>
</div>
</div>
</div>
</div>
