import os
import re
import base64
import glob
import bs4

os.mkdir('figures') if not os.path.exists('figures') else _

html_paths = glob.glob("*.html")

x = re.compile(r'\(data:[a-z]+\/[^;]+;base64[^"]+\)')

img_ctr = 1

def replacement_fn(base64_txt):
    b = base64_txt.group(0).split('base64,')[1].rstrip(')')
    b = base64.b64decode(b + '===')
    
    global img_ctr
    image_path = './figures/' + str(img_ctr) + '.png'
    img_ctr += 1
    with open(image_path, 'wb') as image_file:
        image_file.write(b)
    return '(%s)' % image_path


for html_path in html_paths:
    print(html_path)
    md_path = os.path.basename(html_path).rstrip('.html') + '.md'
    os.system(f'pandoc -s {html_path} -r html -t gfm -o {md_path}')
    
    with open(md_path) as md_file:
        md_contents = md_file.read()
        
    docx = bs4.BeautifulSoup(md_contents)
    
    for eg in docx.find_all('div', class_="highlight hl-ipython3"):
        last_eg_text = eg.text
        last_eg_text = last_eg_text.replace('\n    ', '\n')
        eg.string.replace_with("\n\n```python\n" + last_eg_text.lstrip('\n').rstrip('\n') + "\n```\n\n")
    
    docx_txt = str(docx.contents[0]).replace('<html><body>', '').replace('</body></html>', '')
    docx_txt = re.sub(x, replacement_fn, docx_txt)
    
    with open(md_path, 'w') as md_file:
        md_file.write(docx_txt)
    print(md_path)
