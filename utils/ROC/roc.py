import papermill as pm

magnification_factors = ['40X', '100X', '200X', '400X']

for mag_fac in magnification_factors:
    pm.execute_notebook('ROC.ipynb',
                        'ROC_%s.ipynb' % mag_fac,
                         parameters={
                             'magnification_factor': mag_fac
                         })
