import h5py
import numpy as np


def port_vgg_ft_weights(src_filename, dst_filename):
    ft_weights = h5py.File(src_filename, 'r')
    new_vgg_weights = h5py.File(dst_filename, 'w')

    layer_names_set = set()
    ft_weights['vgg16_features'].visit(
        lambda name: layer_names_set.add(name.split('/')[0]))
    layer_names = np.array(
        list(layer_names_set), dtype='bytes')

    print(ft_weights['vgg16_features'].attrs['weight_names'])

    weight_names = ft_weights['vgg16_features'].attrs['weight_names']

    layer_wise_weight_names = {}

    for weight_name in weight_names:
        weight_name = weight_name.decode('utf8')
        layer_name = weight_name.split('/')[0]

        if layer_name not in layer_wise_weight_names:
            layer_wise_weight_names[layer_name] = [weight_name]
        else:
            layer_wise_weight_names[layer_name].append(weight_name)

    layer_wise_weight_names = {
        key: np.array(layer_wise_weight_names[key], dtype='bytes')
        for key in layer_wise_weight_names
    }

    for key in ft_weights.attrs.keys():
        if key == 'layer_names':
            new_vgg_weights.attrs[key] = layer_names
        else:
            new_vgg_weights.attrs[key] = ft_weights.attrs[key]

    for layer_name in layer_wise_weight_names:
        new_vgg_weights.create_group(layer_name)
        new_vgg_weights[layer_name].attrs['weight_names'] = layer_wise_weight_names[layer_name]

    for layer_name in layer_wise_weight_names:
        weight_names = layer_wise_weight_names[layer_name]
        for weight_name in weight_names:
            src_path = 'vgg16_features' + '/' + weight_name.decode('utf-8')
            dst_path = layer_name + '/' + weight_name.decode('utf-8')

            dataset_as_np_arr = ft_weights.get(src_path).value
            new_vgg_weights.create_dataset(dst_path, data=dataset_as_np_arr)

    new_vgg_weights.close()

    print('Ported weights from "%s" to "%s"' % (
        src_filename, dst_filename
    ))
