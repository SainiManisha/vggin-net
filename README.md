# VGGIN-Net for Imbalanced Breast Cancer Classification

![vggin-graphical-abstract](./utils/vggin-net-graphical-architecture.png)

![vggin-blocks-net diagram](./utils/vgginnet-blocks.png)

## Proposed Approach

Proposed **VGGIN-Net** for breast cancer classification on **BREAKHIS**:

> Notebook for code (w/o results): [[ipynb]](./VGGINET-SemiFinal/VGGINET.ipynb)
>
> [TensorBoard.dev](https://tensorboard.dev) aggregating all training and validation metrics: https://tensorboard.dev/experiment/PSnYyZUWTBem4dLvQYNm2g/

| Result | Run | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| [[md]](./VGGINET-SemiFinal/VGGINET_40X_1.md) | 1 | BREAKHIS | 40X |
| [[md]](./VGGINET-SemiFinal/VGGINET_40X_2.md) | 2 | BREAKHIS | 40X |
| [[md]](./VGGINET-SemiFinal/VGGINET_40X_3.md) | 3 | BREAKHIS | 40X |
| [[md]](./VGGINET-SemiFinal/VGGINET_40X_4.md) | 4 | BREAKHIS | 40X |
| [[md]](./VGGINET-SemiFinal/VGGINET_40X_5.md) | 5 | BREAKHIS | 40X |
| [[md]](./VGGINET-SemiFinal/VGGINET_100X_1.md) | 1 | BREAKHIS | 100X |
| [[md]](./VGGINET-SemiFinal/VGGINET_100X_2.md) | 2 | BREAKHIS | 100X |
| [[md]](./VGGINET-SemiFinal/VGGINET_100X_3.md) | 3 | BREAKHIS | 100X |
| [[md]](./VGGINET-SemiFinal/VGGINET_100X_4.md) | 4 | BREAKHIS | 100X |
| [[md]](./VGGINET-SemiFinal/VGGINET_100X_5.md) | 5 | BREAKHIS | 100X |
| [[md]](./VGGINET-SemiFinal/VGGINET_200X_1.md) | 1 | BREAKHIS | 200X |
| [[md]](./VGGINET-SemiFinal/VGGINET_200X_2.md) | 2 | BREAKHIS | 200X |
| [[md]](./VGGINET-SemiFinal/VGGINET_200X_3.md) | 3 | BREAKHIS | 200X |
| [[md]](./VGGINET-SemiFinal/VGGINET_200X_4.md) | 4 | BREAKHIS | 200X |
| [[md]](./VGGINET-SemiFinal/VGGINET_200X_5.md) | 5 | BREAKHIS | 200X |
| [[md]](./VGGINET-SemiFinal/VGGINET_400X_1.md) | 1 | BREAKHIS | 400X |
| [[md]](./VGGINET-SemiFinal/VGGINET_400X_2.md) | 2 | BREAKHIS | 400X |
| [[md]](./VGGINET-SemiFinal/VGGINET_400X_3.md) | 3 | BREAKHIS | 400X |
| [[md]](./VGGINET-SemiFinal/VGGINET_400X_4.md) | 4 | BREAKHIS | 400X |
| [[md]](./VGGINET-SemiFinal/VGGINET_400X_5.md) | 5 | BREAKHIS | 400X |

Proposed **VGGIN-Net** with **Fine Tuning** for breast cancer classification on **BREAKHIS**:

> Notebook for code (w/o results): [[ipynb]](./VGGINET-Final/VGGINET_FineTuning.ipynb)
>
> [TensorBoard.dev](https://tensorboard.dev) aggregating all training and validation metrics: https://tensorboard.dev/experiment/P7EkvFxPQgSWGqAPhE7w2A/

> Learning curves on validation set: ![Learning Curves on Validation Set](./utils/validation_plot.png)

| Result | Run | Dataset | Magnification Factor | Fine Tuning Blocks |
| --- | --- | --- | --- | --- |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_40X_1.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_40X_1.md) | 1 | BREAKHIS | 40X | block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_40X_2.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_40X_2.md) | 2 | BREAKHIS | 40X | block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_40X_3.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_40X_3.md) | 3 | BREAKHIS | 40X | block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_40X_4.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_40X_4.md) | 4 | BREAKHIS | 40X | block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_40X_5.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_40X_5.md) | 5 | BREAKHIS | 40X | block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_100X_1.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_100X_1.md) | 1 | BREAKHIS | 100X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_100X_2.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_100X_2.md) | 2 | BREAKHIS | 100X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_100X_3.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_100X_3.md) | 3 | BREAKHIS | 100X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_100X_4.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_100X_4.md) | 4 | BREAKHIS | 100X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_100X_5.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_100X_5.md) | 5 | BREAKHIS | 100X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_200X_1.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_200X_1.md) | 1 | BREAKHIS | 200X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_200X_2.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_200X_2.md) | 2 | BREAKHIS | 200X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_200X_3.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_200X_3.md) | 3 | BREAKHIS | 200X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_200X_4.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_200X_4.md) | 4 | BREAKHIS | 200X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_200X_5.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_200X_5.md) | 5 | BREAKHIS | 200X | block1, block2, block3, block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_400X_1.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_400X_1.md) | 1 | BREAKHIS | 400X | block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_400X_2.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_400X_2.md) | 2 | BREAKHIS | 400X | block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_400X_3.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_400X_3.md) | 3 | BREAKHIS | 400X | block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_400X_4.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_400X_4.md) | 4 | BREAKHIS | 400X | block4, inception_block |
| [[ipynb]](./VGGINET-Final/VGGINET_FT_400X_5.ipynb) [[md]](./VGGINET-Final/VGGINET_FT_400X_5.md) | 5 | BREAKHIS | 400X | block4, inception_block |

## Background and Ablation Experiments

Most experiments that form a part of the **background study** or **ablation**:

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGG16 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_VGG16_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_VGG16_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| VGG16 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_VGG16_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_VGG16_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| VGG16 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_VGG16_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_VGG16_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| VGG16 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_VGG16_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_VGG16_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| GoogLeNet (Inception v1) w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_InceptionV1_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_InceptionV1_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| GoogLeNet (Inception v1) w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_InceptionV1_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_InceptionV1_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| GoogLeNet (Inception v1) w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_InceptionV1_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_InceptionV1_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| GoogLeNet (Inception v1) w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_InceptionV1_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_InceptionV1_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGG16 w/ Data Augmentation | [[ipynb]](./ipynb/VGG16_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/VGG16_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| VGG16 w/ Data Augmentation | [[ipynb]](./ipynb/VGG16_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/VGG16_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| VGG16 w/ Data Augmentation | [[ipynb]](./ipynb/VGG16_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/VGG16_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| VGG16 w/ Data Augmentation | [[ipynb]](./ipynb/VGG16_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/VGG16_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| GoogLeNet (Inception v1) w/ Data Augmentation | [[ipynb]](./ipynb/InceptionV1_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/InceptionV1_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| GoogLeNet (Inception v1) w/ Data Augmentation | [[ipynb]](./ipynb/InceptionV1_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/InceptionV1_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| GoogLeNet (Inception v1) w/ Data Augmentation | [[ipynb]](./ipynb/InceptionV1_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/InceptionV1_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| GoogLeNet (Inception v1) w/ Data Augmentation | [[ipynb]](./ipynb/InceptionV1_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/InceptionV1_normal_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|

<!--
| ResNet50 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_ResNet50_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_ResNet50_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| ResNet50 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_ResNet50_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_ResNet50_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| ResNet50 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_ResNet50_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_ResNet50_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| ResNet50 w/o Data Augmentation | [[ipynb]](./ipynb/Without_data_Agumentation_ResNet50_normal_40X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/Without_data_Agumentation_ResNet50_normal_40X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |40X|
-->

<!-- 
| ResNet50 w/ Data Augmentation | [[ipynb]](./ipynb/ResNet50_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/ResNet50_normal_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| ResNet50 w/ Data Augmentation | [[ipynb]](./ipynb/ResNet50_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/ResNet50_normal_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| ResNet50 w/ Data Augmentation | [[ipynb]](./ipynb/ResNet50_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/ResNet50_normal_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| ResNet50 w/ Data Augmentation | [[ipynb]](./ipynb/ResNet50_normal_40X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/ResNet50_normal_40X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |40X|
-->


| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGGIN-Net w/o Data Augmentation | [[ipynb]](./ipynb/VGGINET_woDA.ipynb)[[md]](./results_md/VGGINET_woDA_100X.md) | BREAKHIS |100X|
| VGGIN-Net w/o Data Augmentation | [[ipynb]](./ipynb/VGGINET_woDA.ipynb)[[md]](./results_md/VGGINET_woDA_200X.md) | BREAKHIS |200X|
| VGGIN-Net w/o Data Augmentation | [[ipynb]](./ipynb/VGGINET_woDA.ipynb)[[md]](./results_md/VGGINET_woDA_400X.md) | BREAKHIS |400X|
| VGGIN-Net w/o Data Augmentation | [[ipynb]](./ipynb/VGGINET_woDA.ipynb)[[md]](./results_md/VGGINET_woDA_40X.md) | BREAKHIS |40X|

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGGIN-Net w/o Inception Block | [[ipynb]](./ipynb/VGGINET_without_inception_blk_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/VGGINET_without_inception_blk_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/o Inception Block | [[ipynb]](./ipynb/VGGINET_without_inception_blk_RANDOM_CROP_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/VGGINET_without_inception_blk_RANDOM_CROP_100X_train_val_test_split_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/o Inception Block | [[ipynb]](./ipynb/VGGINET_without_inception_blk_RANDOM_CROP_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/VGGINET_without_inception_blk_RANDOM_CROP_200X_train_val_test_split_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/o Inception Block | [[ipynb]](./ipynb/VGGINET_without_inception_blk_RANDOM_CROP_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/VGGINET_without_inception_blk_RANDOM_CROP_400X_train_val_test_split_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net | [[ipynb]](./ipynb/VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGGIN-Net w/ Oversampling | [[ipynb]](./ipynb/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_100X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_100X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Oversampling | [[ipynb]](./ipynb/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_200X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_200X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Oversampling | [[ipynb]](./ipynb/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_400X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_400X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Oversampling | [[ipynb]](./ipynb/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_40X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_OVERSAMPLER_RANDOM_CROP_40X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Undersampling | [[ipynb]](./ipynb/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_100X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_100X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_100X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_100X_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Undersampling | [[ipynb]](./ipynb/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_200X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_200X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_200X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_200X_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Undersampling | [[ipynb]](./ipynb/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_400X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_400X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_400X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_400X_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Undersampling | [[ipynb]](./ipynb/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_40X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/DATA_LEVEL_UNDERSAMPLER_RANDOM_CROP_40X_train_val_test_split_Training_VGGIN_Net_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGGIN-Net w/ Block 3 | [[ipynb]](./ipynb/VGGINET_block3_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/VGGINET_block3_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Block 5 | [[ipynb]](./ipynb/VGGINET_block5_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/VGGINET_block5_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Dimensionality Reduction Block | [[ipynb]](./ipynb/Dimensionality_ReductionBlock_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/Dimensionality_ReductionBlock_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Inception v2 (Batch Normalization) Block | [[ipynb]](./ipynb/Inception_v2_BN_block_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/Inception_v2_BN_block_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Filters k=10 | [[ipynb]](./ipynb/K=10_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/K=10_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Filters k=2 | [[ipynb]](./ipynb/K=2_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/K=2_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Filters k=5 | [[ipynb]](./ipynb/K=5_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.ipynb)[[md]](./results_md/K=5_VGGINET_RANDOM_CROP_40X_train_val_test_split_on_BREAKHIS_40X_60_10_30.md) | BREAKHIS |40X|

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Magnification Factor |
| --- | --- | --- | --- |
| VGGIN-Net w/ Fine Tuning of Block 1, 2, 3, 4 (Full Fine Tuning) | [[ipynb]](./ipynb/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Fine Tuning of Block 1, 2, 3, 4 (Full Fine Tuning) | [[ipynb]](./ipynb/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Fine Tuning of Block 1, 2, 3, 4 (Full Fine Tuning) | [[ipynb]](./ipynb/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Fine Tuning of Block 1, 2, 3, 4 (Full Fine Tuning) | [[ipynb]](./ipynb/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/full_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Fine Tuning of Block 2, 3, 4 | [[ipynb]](./ipynb/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Fine Tuning of Block 2, 3, 4 | [[ipynb]](./ipynb/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Fine Tuning of Block 2, 3, 4 | [[ipynb]](./ipynb/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Fine Tuning of Block 2, 3, 4 | [[ipynb]](./ipynb/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block2_block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Fine Tuning of Block 3, 4 | [[ipynb]](./ipynb/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Fine Tuning of Block 3, 4 | [[ipynb]](./ipynb/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Fine Tuning of Block 3, 4 | [[ipynb]](./ipynb/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Fine Tuning of Block 3, 4 | [[ipynb]](./ipynb/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block3_block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.md) | BREAKHIS |40X|
| VGGIN-Net w/ Fine Tuning of Block 4 | [[ipynb]](./ipynb/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_100X_train_val_test_split_60_10_30.md) | BREAKHIS |100X|
| VGGIN-Net w/ Fine Tuning of Block 4 | [[ipynb]](./ipynb/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_200X_train_val_test_split_60_10_30.md) | BREAKHIS |200X|
| VGGIN-Net w/ Fine Tuning of Block 4 | [[ipynb]](./ipynb/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_400X_train_val_test_split_60_10_30.md) | BREAKHIS |400X|
| VGGIN-Net w/ Fine Tuning of Block 4 | [[ipynb]](./ipynb/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.ipynb)[[md]](./results_md/block4_inceptionblock_fine_tuning,_random_crops_Training_VGGIN_Net_on_BREAKHIS_40X_train_val_test_split_60_10_30.md) | BREAKHIS |40X|

## Transfer Learning Experiments

Experiments demonstrating that architecture can be used for **transfer learning**:

| Experiment Description | Notebook (IPYNB w/o Results & MD w/ Results) | Dataset | Learning Rate | Pre-trained Weights Used |
| --- | --- | --- | --- | --- |
| TransferLearning with FixedFeatureExtractor and FineTuning VGGIN-Net | [[pdf]](./VGGINET-TransferLearn/_BREAKHIS40X_train_weights-LR_0.001_Fixed_and_FineTune_Results.pdf) | Breast-Histopathology-Images | 0.001 (training), \[1e-5, 5e-5\] (fine tuning) | BREAKHIS-40X Trained VGGIN-Net |
| TransferLearning with FixedFeatureExtractor and FineTuning VGGIN-Net | [[ipynb]](./VGGINET-TransferLearn/VGGINNet_BREAST-HIST-PATH_using_BREAKHIS40X_Finetune_wights_LR0.004_TransferLearning_FixedFeatureExtractor_and_FineTuning.ipynb) [[md]](./VGGINET-TransferLearn/VGGINNet_BREAST-HIST-PATH_using_BREAKHIS40X_Finetune_wights_LR0.004_TransferLearning_FixedFeatureExtractor_and_FineTuning.md) | Breast-Histopathology-Images | 0.01 (training), \[1e-5, 5e-5\] (fine tuning) | BREAKHIS-40X Trained and **FineTuned** VGGIN-Net |
| TransferLearning with FixedFeatureExtractor and FineTuning VGGIN-Net | [[ipynb]](./VGGINET-TransferLearn/VGGINNet_BREAST-HIST-PATH_usingBREAKHIS40X_Finetune_wights_LR0.01_TransferLearning_FixedFeatureExtractor_and_FineTuning_.ipynb) [[md]](./VGGINET-TransferLearn/VGGINNet_BREAST-HIST-PATH_using_BREAKHIS40X_Finetune_wights_LR0.01_TransferLearning_FixedFeatureExtractor_and_FineTuning.md) | Breast-Histopathology-Images | 0.004 (training), \[1e-5, 5e-5\] (fine tuning) | BREAKHIS-40X Trained and **FineTuned** VGGIN-Net |
